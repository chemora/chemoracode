
ActiveThorns = "CoordBase SymBase Boundary NanChecker CartGrid3d Time
                MoL CarpetIOBasic CarpetIOScalar IOUtil Carpet
                CarpetLib CarpetIOASCII ADMBase CarpetReduce
                StaticConformal SpaceMask GenericFD CoordGauge
                ADMCoupling LoopControl ML_BSSN ML_BSSN_Helper
                TMuNuBase SphericalSurface ADMMacros TimerReport
                CaKernel Accelerator CarpetIOHDF5 TwoPunctures
                SystemStatistics NewRad ChemoraDevice"

#############################################################
# Evolution equations
#############################################################

ADMBase::evolution_method         = "ML_BSSN"
ADMBase::lapse_evolution_method   = "ML_BSSN"
ADMBase::shift_evolution_method   = "ML_BSSN"
ADMBase::dtlapse_evolution_method = "ML_BSSN"
ADMBase::dtshift_evolution_method = "ML_BSSN"

ML_BSSN::harmonicN           = 1      # 1+log
ML_BSSN::harmonicF           = 2.0    # 1+log
ML_BSSN::ShiftGammaCoeff     = 0.75
ML_BSSN::BetaDriver          = 1.0
ML_BSSN::LapseAdvectionCoeff = 1.0
ML_BSSN::ShiftAdvectionCoeff = 1.0

ML_BSSN::MinimumLapse                  = 1.0e-8
ML_BSSN::conformalMethod               = 1 # 1 for W, 0 for phi
ML_BSSN::my_initial_boundary_condition = "extrapolate-gammas"
ML_BSSN::my_rhs_boundary_condition     = "static" # Radiative does not work with CaKernel yet
ML_BSSN::apply_dissipation             = "never"
ML_BSSN::fdOrder                       = 8

Boundary::radpower                     = 2
ADMBase::metric_type = "physical"

#############################################################
# Grid
#############################################################

CoordBase::domainsize                   = minmax

CoordBase::boundary_size_x_lower        = 5
CoordBase::boundary_size_y_lower        = 5
CoordBase::boundary_size_z_lower        = 5
CoordBase::boundary_shiftout_x_lower    = 1
CoordBase::boundary_shiftout_y_lower    = 1
CoordBase::boundary_shiftout_z_lower    = 1

CoordBase::boundary_size_x_upper        = 5
CoordBase::boundary_size_y_upper        = 5
CoordBase::boundary_size_z_upper        = 5
CoordBase::boundary_shiftout_x_upper    = 0
CoordBase::boundary_shiftout_y_upper    = 0
CoordBase::boundary_shiftout_z_upper    = 0

CartGrid3D::type                        = "coordbase"
CartGrid3D::domain                      = "full"
CartGrid3D::avoid_origin                = "no"

CoordBase::xmin                         = -4
CoordBase::ymin                         = -4
CoordBase::zmin                         = -4

CoordBase::xmax                         = 4
CoordBase::ymax                         = 4
CoordBase::zmax                         = 4

CoordBase::dx                           = 0.1
CoordBase::dy                           = 0.1
CoordBase::dz                           = 0.1

#############################################################
# Carpet
#############################################################

Carpet::domain_from_coordbase           = "yes"
Carpet::ghost_size                      = 5
Carpet::init_fill_timelevels            = "yes"
Carpet::poison_new_timelevels           = yes
Carpet::poison_value                    = 113

#############################################################
# Timers
#############################################################

TimerReport::output_all_timers_readable = yes
TimerReport::output_schedule_timers     = no
Carpet::output_initialise_timer_tree    = yes
Carpet::output_timer_tree_every         = 16

#############################################################
# Time integration
#############################################################

# Cactus::terminate                         = "iteration"
# Cactus::cctk_itlast                       = 16

Cactus::terminate                         = "time"
Cactus::cctk_final_time                   = 25

Time::dtfac                               = 0.25

MethodOfLines::ode_method             = "RK3"
MethodOfLines::MoL_Intermediate_Steps = 3
MethodOfLines::MoL_Num_Scratch_Levels = 1

#############################################################
# Initial data
#############################################################

ADMBase::initial_data    = "twopunctures"
ADMBase::initial_lapse   = "twopunctures-averaged"
ADMBase::initial_shift   = "zero"
ADMBase::initial_dtlapse = "zero"
ADMBase::initial_dtshift = "zero"

TwoPunctures::par_b          =  1.168642873
TwoPunctures::par_m_plus     =  0.453
TwoPunctures::par_m_minus    =  0.453
TwoPunctures::par_P_plus [1] = +0 #.3331917498
TwoPunctures::par_P_minus[1] = -0 #.3331917498
TwoPunctures::grid_setup_method         = "Taylor expansion"
TwoPunctures::verbose = yes

# Uncomment these to have low accuracy initial data for fast testing
# TwoPunctures::npoints_A                 = 16
# TwoPunctures::npoints_B                 = 16
# TwoPunctures::npoints_phi               = 8

#############################################################
# Output
#############################################################

IO::out_dir                   = $parfile
IO::out_fileinfo              = "none"
CarpetIOBasic::outInfo_every  = 1
CarpetIOBasic::outInfo_vars   = "Carpet::physical_time_per_hour ML_BSSN::phi ML_BSSN::trK SystemStatistics::maxrss_mb"

IOASCII::out1D_every          = 4
IOASCII::out_precision        = 19
IOASCII::out1D_x              = yes
IOASCII::out1D_y              = no
IOASCII::out1D_z              = no
IOASCII::out1D_d              = no
IOASCII::out1D_vars           = "ML_BSSN::phi"

CarpetIOASCII::out0d_vars     = "ML_BSSN::phi"
CarpetIOASCII::out0d_every    = 1

IOHDF5::out2D_every              = 4
IOHDF5::out2D_vars               = "ML_BSSN::phi ML_BSSN::trK" # ML_BSSN::phirhs ML_BSSN::trKrhs

# Enable this for 3D output in HDF5 format
IOHDF5::out_every              = 0
IOHDF5::out_vars               = "ML_BSSN::phi" 
IOHDF5::compression_level      = 9

#############################################################
# Debugging
#############################################################

# Accelerator::verbose     = yes
# Accelerator::veryverbose = yes
# CaKernel::verbose        = yes
# CaKernel::veryverbose    = yes
# Carpet::verbose          = yes
# Carpet::veryverbose      = yes
# LoopControl::printstats  = yes
