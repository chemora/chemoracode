
# This was: https://bitbucket.org/chemora/benchmark/raw/094d25746c58c0c5471edba7c8f03a0570c471e8/sc12bench/weak/cane-mclachlan/cane-100-nox-optmpi/weak_autotopo_1/cakernel_1.par

ActiveThorns = "CoordBase SymBase Boundary NanChecker CartGrid3d Time
                MoL CarpetIOBasic CarpetIOScalar IOUtil Carpet
                CarpetLib CarpetIOASCII ADMBase CarpetReduce
                StaticConformal SpaceMask GenericFD CoordGauge
                ADMCoupling LoopControl ML_BSSN_Host ML_BSSN_Host_Helper
                TMuNuBase SphericalSurface ADMMacros TimerReport
                SystemStatistics NewRad"
#Formaline CarpetIOHDF5"

#############################################################
# Evolution equations
#############################################################

ADMBase::evolution_method         = "ML_BSSN_Host"
ADMBase::lapse_evolution_method   = "ML_BSSN_Host"
ADMBase::shift_evolution_method   = "ML_BSSN_Host"
ADMBase::dtlapse_evolution_method = "ML_BSSN_Host"
ADMBase::dtshift_evolution_method = "ML_BSSN_Host"

ML_BSSN_Host::harmonicN           = 1      # 1+log
ML_BSSN_Host::harmonicF           = 2.0    # 1+log
ML_BSSN_Host::ShiftGammaCoeff     = 0.75
ML_BSSN_Host::BetaDriver          = 1.0
ML_BSSN_Host::LapseAdvectionCoeff = 1.0
ML_BSSN_Host::ShiftAdvectionCoeff = 1.0

ML_BSSN_Host::MinimumLapse                  = 1.0e-8
ML_BSSN_Host::conformalMethod               = 1 # 1 for W, 0 for phi
ML_BSSN_Host::my_initial_boundary_condition = "extrapolate-gammas"
ML_BSSN_Host::my_rhs_boundary_condition     = "static" # Radiative does not work with CaKernel yet
ML_BSSN_Host::apply_dissipation             = "never"

# This thorn has been built only with 8th order finite differencing

Boundary::radpower                     = 2
ADMBase::metric_type = "physical"

#############################################################
# Grid
#############################################################

CoordBase::domainsize                   = minmax

CoordBase::boundary_size_x_lower        = 5
CoordBase::boundary_size_y_lower        = 5
CoordBase::boundary_size_z_lower        = 5
CoordBase::boundary_shiftout_x_lower    = 1
CoordBase::boundary_shiftout_y_lower    = 1
CoordBase::boundary_shiftout_z_lower    = 1

CoordBase::boundary_size_x_upper        = 5
CoordBase::boundary_size_y_upper        = 5
CoordBase::boundary_size_z_upper        = 5
CoordBase::boundary_shiftout_x_upper    = 0
CoordBase::boundary_shiftout_y_upper    = 0
CoordBase::boundary_shiftout_z_upper    = 0

CartGrid3D::type                        = "coordbase"
CartGrid3D::domain                      = "full"
CartGrid3D::avoid_origin                = "no"

CoordBase::xmin                         = 0
CoordBase::ymin                         = 0
CoordBase::zmin                         = 0

CoordBase::xmax                         = 100
CoordBase::ymax                         = 100
CoordBase::zmax                         = 100
# CoordBase::xmax                         = 136
# CoordBase::ymax                         = 136
# CoordBase::zmax                         = 136

CoordBase::dx                           = 1
CoordBase::dy                           = 1
CoordBase::dz                           = 1

#############################################################
# Carpet
#############################################################

Carpet::domain_from_coordbase           = "yes"
Carpet::ghost_size                      = 5
Carpet::init_fill_timelevels            = "yes"
Carpet::poison_new_timelevels           = no
#Carpet::poison_value                    = 113

#############################################################
# Timers
#############################################################

TimerReport::output_all_timers_readable = yes
TimerReport::output_schedule_timers     = no
Carpet::output_initialise_timer_tree    = yes
Carpet::output_timer_tree_every         = 200
#Carpet::zero_timer_tree_every           = 1
#Carpet::timer_tree_threshold_percentage = 0.01
#Carpet::timer_tree_output_precision     = 6
Carpet::output_xml_timer_tree           = yes
Carpet::grid_structure_filename         = "carpet-grid.asc"

#############################################################
# Time integration
#############################################################

Cactus::terminate                         = "iteration"
#Cactus::cctk_itlast                       = 201
Cactus::cctk_itlast                       = 10

# Cactus::terminate                         = "time"
# Cactus::cctk_final_time                   = 25

Time::dtfac                               = 0.25

MethodOfLines::ode_method             = "RK3"
MethodOfLines::MoL_Intermediate_Steps = 3
MethodOfLines::MoL_Num_Scratch_Levels = 1

#############################################################
# Initial data
#############################################################

ADMBase::initial_shift   = "zero"
ADMBase::initial_dtlapse = "zero"
ADMBase::initial_dtshift = "zero"

#############################################################
# Output
#############################################################

IO::out_fileinfo              = "none"
CarpetIOBasic::outInfo_every  = 1
CarpetIOBasic::outInfo_vars   = "Carpet::physical_time_per_hour SystemStatistics::maxrss_mb"

IOASCII::out1D_every          = 201
IOASCII::out_precision        = 19
IOASCII::out1D_x              = yes
IOASCII::out1D_y              = no
IOASCII::out1D_z              = no
IOASCII::out1D_d              = no
IOASCII::out1D_vars           = "ML_BSSN_Host::phi"

CarpetIOASCII::out0d_every    = 201
CarpetIOASCII::out0d_vars     = "ML_BSSN_Host::phi Carpet::timing"

#IOHDF5::out2D_every              = 0
#IOHDF5::out2D_vars               = "ML_BSSN_Host::phi ML_BSSN_Host::trK" # ML_BSSN_Host::phirhs ML_BSSN_Host::trKrhs

# Enable this for 3D output in HDF5 format
#IOHDF5::out_every              = 0
#IOHDF5::out_vars               = "ML_BSSN_Host::phi" 
#IOHDF5::compression_level      = 9

#############################################################
# Debugging
#############################################################

# Carpet::verbose          = yes
# Carpet::veryverbose      = yes
# LoopControl::printstats  = yes
# output dir
IO::out_dir = $parfile

Cactus::highlight_warning_messages = no
