begin thorn mb_ldg

use cakernel

# For more about the code below,
# please refer: http://people.csail.mit.edu/cychan/papers/ipdps10.pdf

begin parameters
  ALPHA : real, default: 0.1
  BETA  : real, default: 0.2
  GAMMA : real, default: 0.3
end parameters

begin variables
  u uNext1               # GFs for Laplacian
  x0 y0 z0 uNext2           # GFs for Divergence
  xNext yNext zNext   # GFs for Gradient stencils
end variables

begin calculation initialize scheduled at initial
  u = sin( x + y + z )
  x0 = sin( 0.1 * x + 0.1 * y + 0.1 * z + 0.1 )
  y0 = sin( 0.2 * x + 0.2 * y + 0.2 * z + 0.2 )
  z0 = sin( 0.12 * x + 0.12 * y + 0.12 * z + 0.12 )
end calculation

begin calculation lap1 scheduled at MoL_CalcRHS
  # Laplacian
  uNext1 = ALPHA u
     + BETA ( Dpl_1 u + Dmn_1 u + Dpl_2 u + Dmn_2 u + Dpl_3 u + Dmn_3 u )
end calculation

begin calculation div1 scheduled at MoL_CalcRHS
  # Divergence
  uNext2 = ALPHA ( Dpl_1 x0 - Dmn_1 x0 )
           + BETA ( Dpl_2 y0 - Dmn_2 y0 )
           + GAMMA ( Dpl_3 z0 - Dmn_3 z0 )

end calculation

begin calculation grad1 scheduled at MoL_CalcRHS
  # Gradient stencils
  xNext = ALPHA ( Dpl_1 u - Dmn_1 u )
  yNext = BETA  ( Dpl_2 u - Dmn_2 u )
  zNext = GAMMA ( Dpl_3 u - Dmn_3 u )
end calculation

begin calculation lap2 scheduled at MoL_CalcRHS
  # Laplacian
  uNext1 = ALPHA u
     + BETA ( Dpl_1 u  + Dmn_1 u  + Dpl_2 u  + Dmn_2 u  + Dpl_3 u  + Dmn_3 u
            + Dpl2_1 u + Dmn2_1 u + Dpl2_2 u + Dmn2_2 u + Dpl2_3 u + Dmn2_3 u )

end calculation

begin calculation div2 scheduled at MoL_CalcRHS

  # Divergence
  uNext2 = ALPHA ( Dpl_1 x0 + Dpl2_1 x0 - Dmn_1 x0 - Dmn2_1 x0 )
           + BETA ( Dpl_2 y0 + Dpl2_2 y0 - Dmn_2 y0 - Dmn2_2 y0 )
           + GAMMA ( Dpl_3 z0 + Dpl2_3 z0 - Dmn_3 z0 - Dmn2_3 z0 )

end calculation

begin calculation grad2 scheduled at MoL_CalcRHS
  # Gradient stencils
  xNext = ALPHA ( Dpl_1 u + Dpl2_1 u - Dmn_1 u - Dmn2_1 u )
  yNext = BETA  ( Dpl_2 u + Dpl2_2 u - Dmn_2 u - Dmn2_2 u )
  zNext = GAMMA ( Dpl_3 u + Dpl2_3 u - Dmn_3 u - Dmn2_3 u )
end calculation

begin operators
  Dpl_i foo = foo[i+1]
  Dmn_i foo = foo[i-1]
  Dpl2_i foo = foo[i+2]
  Dmn2_i foo = foo[i-2]
end operators

end thorn
