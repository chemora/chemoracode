#undef KRANC_DIFF_FUNCTIONS
#define KRANC_C
#include "Differencing.h"
#include "Kranc.hh"
using namespace CCTK_THORN;

#undef KRANC_GFOFFSET3D
#define KRANC_GFOFFSET3D(u,i,j,k) I3D(u,i,j,k)


CAKERNEL_DEVICE_ML_BSSN_InitRHS_Begin
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  #define ConditionExpression(x) (x)
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = params.cagh_dx;
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = params.cagh_dy;
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = params.cagh_dz;
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = params.cagh_dt;
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = params.cagh_time;
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = 1.0/dx;
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = 1.0/dy;
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = 1.0/dz;
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 1/3.0;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 2.0/3.0;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 4.0/3.0;
  const CCTK_REAL keightthird CCTK_ATTRIBUTE_UNUSED = 8.0/3.0;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5 * dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5 * dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5 * dzi;
  /* Initialize predefined quantities */
  const CCTK_REAL p1o1024dx CCTK_ATTRIBUTE_UNUSED = 0.0009765625*pow(dx,-1);
  const CCTK_REAL p1o1024dy CCTK_ATTRIBUTE_UNUSED = 0.0009765625*pow(dy,-1);
  const CCTK_REAL p1o1024dz CCTK_ATTRIBUTE_UNUSED = 0.0009765625*pow(dz,-1);
  const CCTK_REAL p1o1680dx CCTK_ATTRIBUTE_UNUSED = 0.000595238095238095238095238095238*pow(dx,-1);
  const CCTK_REAL p1o1680dy CCTK_ATTRIBUTE_UNUSED = 0.000595238095238095238095238095238*pow(dy,-1);
  const CCTK_REAL p1o1680dz CCTK_ATTRIBUTE_UNUSED = 0.000595238095238095238095238095238*pow(dz,-1);
  const CCTK_REAL p1o2dx CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dx,-1);
  const CCTK_REAL p1o2dy CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dy,-1);
  const CCTK_REAL p1o2dz CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dz,-1);
  const CCTK_REAL p1o5040dx2 CCTK_ATTRIBUTE_UNUSED = 0.000198412698412698412698412698413*pow(dx,-2);
  const CCTK_REAL p1o5040dy2 CCTK_ATTRIBUTE_UNUSED = 0.000198412698412698412698412698413*pow(dy,-2);
  const CCTK_REAL p1o5040dz2 CCTK_ATTRIBUTE_UNUSED = 0.000198412698412698412698412698413*pow(dz,-2);
  const CCTK_REAL p1o560dx CCTK_ATTRIBUTE_UNUSED = 0.00178571428571428571428571428571*pow(dx,-1);
  const CCTK_REAL p1o560dy CCTK_ATTRIBUTE_UNUSED = 0.00178571428571428571428571428571*pow(dy,-1);
  const CCTK_REAL p1o560dz CCTK_ATTRIBUTE_UNUSED = 0.00178571428571428571428571428571*pow(dz,-1);
  const CCTK_REAL p1o705600dxdy CCTK_ATTRIBUTE_UNUSED = 1.41723356009070294784580498866e-6*pow(dx,-1)*pow(dy,-1);
  const CCTK_REAL p1o705600dxdz CCTK_ATTRIBUTE_UNUSED = 1.41723356009070294784580498866e-6*pow(dx,-1)*pow(dz,-1);
  const CCTK_REAL p1o705600dydz CCTK_ATTRIBUTE_UNUSED = 1.41723356009070294784580498866e-6*pow(dy,-1)*pow(dz,-1);
  const CCTK_REAL p1o840dx CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dx,-1);
  const CCTK_REAL p1o840dy CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dy,-1);
  const CCTK_REAL p1o840dz CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dz,-1);
  const CCTK_REAL p1odx CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL p1ody CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL p1odz CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL pm1o2dx CCTK_ATTRIBUTE_UNUSED = -0.5*pow(dx,-1);
  const CCTK_REAL pm1o2dy CCTK_ATTRIBUTE_UNUSED = -0.5*pow(dy,-1);
  const CCTK_REAL pm1o2dz CCTK_ATTRIBUTE_UNUSED = -0.5*pow(dz,-1);
  const CCTK_REAL pm1o840dx CCTK_ATTRIBUTE_UNUSED = -0.00119047619047619047619047619048*pow(dx,-1);
  const CCTK_REAL pm1o840dy CCTK_ATTRIBUTE_UNUSED = -0.00119047619047619047619047619048*pow(dy,-1);
  const CCTK_REAL pm1o840dz CCTK_ATTRIBUTE_UNUSED = -0.00119047619047619047619047619048*pow(dz,-1);
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  CAKERNEL_DEVICE_ML_BSSN_InitRHS_Computations_Begin
    /* Assign local copies of grid functions */
    
    
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL phirhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL gt11rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL gt12rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL gt13rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL gt22rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL gt23rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL gt33rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL trKrhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL At11rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL At12rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL At13rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL At22rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL At23rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL At33rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL Xt1rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL Xt2rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL Xt3rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL alpharhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ArhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL beta1rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL beta2rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL beta3rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL B1rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL B2rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL B3rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    /* Copy local copies back to grid functions */
    I3D(alpharhs,0,0,0) = alpharhsL;
    I3D(Arhs,0,0,0) = ArhsL;
    I3D(At11rhs,0,0,0) = At11rhsL;
    I3D(At12rhs,0,0,0) = At12rhsL;
    I3D(At13rhs,0,0,0) = At13rhsL;
    I3D(At22rhs,0,0,0) = At22rhsL;
    I3D(At23rhs,0,0,0) = At23rhsL;
    I3D(At33rhs,0,0,0) = At33rhsL;
    I3D(B1rhs,0,0,0) = B1rhsL;
    I3D(B2rhs,0,0,0) = B2rhsL;
    I3D(B3rhs,0,0,0) = B3rhsL;
    I3D(beta1rhs,0,0,0) = beta1rhsL;
    I3D(beta2rhs,0,0,0) = beta2rhsL;
    I3D(beta3rhs,0,0,0) = beta3rhsL;
    I3D(gt11rhs,0,0,0) = gt11rhsL;
    I3D(gt12rhs,0,0,0) = gt12rhsL;
    I3D(gt13rhs,0,0,0) = gt13rhsL;
    I3D(gt22rhs,0,0,0) = gt22rhsL;
    I3D(gt23rhs,0,0,0) = gt23rhsL;
    I3D(gt33rhs,0,0,0) = gt33rhsL;
    I3D(phirhs,0,0,0) = phirhsL;
    I3D(trKrhs,0,0,0) = trKrhsL;
    I3D(Xt1rhs,0,0,0) = Xt1rhsL;
    I3D(Xt2rhs,0,0,0) = Xt2rhsL;
    I3D(Xt3rhs,0,0,0) = Xt3rhsL;
    
  CAKERNEL_DEVICE_ML_BSSN_InitRHS_Computations_End
  
CAKERNEL_DEVICE_ML_BSSN_InitRHS_End
