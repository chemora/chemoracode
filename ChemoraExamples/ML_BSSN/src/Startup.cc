/*  File produced by Kranc */

#include "cctk.h"

#include "Chemora.h"

extern "C" int ML_BSSN_Startup(void)
{
  const char* banner CCTK_ATTRIBUTE_UNUSED = "ML_BSSN";
  CCTK_RegisterBanner(banner);
  chemora_cg_thorn_startup();
  return 0;
}
