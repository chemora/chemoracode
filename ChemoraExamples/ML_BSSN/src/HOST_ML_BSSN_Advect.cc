/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_BSSN {

extern "C" void HOST_ML_BSSN_Advect_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % HOST_ML_BSSN_Advect_calc_every != HOST_ML_BSSN_Advect_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN::ML_curvrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN::ML_curvrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN::ML_dtlapserhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN::ML_dtlapserhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN::ML_dtshiftrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN::ML_dtshiftrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN::ML_Gammarhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN::ML_Gammarhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN::ML_lapserhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN::ML_lapserhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN::ML_log_confacrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN::ML_log_confacrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN::ML_metricrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN::ML_metricrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN::ML_shiftrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN::ML_shiftrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN::ML_trace_curvrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN::ML_trace_curvrhs.");
  return;
}

static void HOST_ML_BSSN_Advect_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  const CCTK_REAL p1o1024dx CCTK_ATTRIBUTE_UNUSED = 0.0009765625*pow(dx,-1);
  const CCTK_REAL p1o1024dy CCTK_ATTRIBUTE_UNUSED = 0.0009765625*pow(dy,-1);
  const CCTK_REAL p1o1024dz CCTK_ATTRIBUTE_UNUSED = 0.0009765625*pow(dz,-1);
  const CCTK_REAL p1o1680dx CCTK_ATTRIBUTE_UNUSED = 0.000595238095238095238095238095238*pow(dx,-1);
  const CCTK_REAL p1o1680dy CCTK_ATTRIBUTE_UNUSED = 0.000595238095238095238095238095238*pow(dy,-1);
  const CCTK_REAL p1o1680dz CCTK_ATTRIBUTE_UNUSED = 0.000595238095238095238095238095238*pow(dz,-1);
  const CCTK_REAL p1o2dx CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dx,-1);
  const CCTK_REAL p1o2dy CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dy,-1);
  const CCTK_REAL p1o2dz CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dz,-1);
  const CCTK_REAL p1o5040dx2 CCTK_ATTRIBUTE_UNUSED = 0.000198412698412698412698412698413*pow(dx,-2);
  const CCTK_REAL p1o5040dy2 CCTK_ATTRIBUTE_UNUSED = 0.000198412698412698412698412698413*pow(dy,-2);
  const CCTK_REAL p1o5040dz2 CCTK_ATTRIBUTE_UNUSED = 0.000198412698412698412698412698413*pow(dz,-2);
  const CCTK_REAL p1o560dx CCTK_ATTRIBUTE_UNUSED = 0.00178571428571428571428571428571*pow(dx,-1);
  const CCTK_REAL p1o560dy CCTK_ATTRIBUTE_UNUSED = 0.00178571428571428571428571428571*pow(dy,-1);
  const CCTK_REAL p1o560dz CCTK_ATTRIBUTE_UNUSED = 0.00178571428571428571428571428571*pow(dz,-1);
  const CCTK_REAL p1o705600dxdy CCTK_ATTRIBUTE_UNUSED = 1.41723356009070294784580498866e-6*pow(dx,-1)*pow(dy,-1);
  const CCTK_REAL p1o705600dxdz CCTK_ATTRIBUTE_UNUSED = 1.41723356009070294784580498866e-6*pow(dx,-1)*pow(dz,-1);
  const CCTK_REAL p1o705600dydz CCTK_ATTRIBUTE_UNUSED = 1.41723356009070294784580498866e-6*pow(dy,-1)*pow(dz,-1);
  const CCTK_REAL p1o840dx CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dx,-1);
  const CCTK_REAL p1o840dy CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dy,-1);
  const CCTK_REAL p1o840dz CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dz,-1);
  const CCTK_REAL p1odx CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL p1ody CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL p1odz CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL pm1o2dx CCTK_ATTRIBUTE_UNUSED = -0.5*pow(dx,-1);
  const CCTK_REAL pm1o2dy CCTK_ATTRIBUTE_UNUSED = -0.5*pow(dy,-1);
  const CCTK_REAL pm1o2dz CCTK_ATTRIBUTE_UNUSED = -0.5*pow(dz,-1);
  const CCTK_REAL pm1o840dx CCTK_ATTRIBUTE_UNUSED = -0.00119047619047619047619047619048*pow(dx,-1);
  const CCTK_REAL pm1o840dy CCTK_ATTRIBUTE_UNUSED = -0.00119047619047619047619047619048*pow(dy,-1);
  const CCTK_REAL pm1o840dz CCTK_ATTRIBUTE_UNUSED = -0.00119047619047619047619047619048*pow(dz,-1);
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(HOST_ML_BSSN_Advect,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL AL CCTK_ATTRIBUTE_UNUSED = A[index];
    CCTK_REAL alphaL CCTK_ATTRIBUTE_UNUSED = alpha[index];
    CCTK_REAL alpharhsL CCTK_ATTRIBUTE_UNUSED = alpharhs[index];
    CCTK_REAL ArhsL CCTK_ATTRIBUTE_UNUSED = Arhs[index];
    CCTK_REAL At11L CCTK_ATTRIBUTE_UNUSED = At11[index];
    CCTK_REAL At11rhsL CCTK_ATTRIBUTE_UNUSED = At11rhs[index];
    CCTK_REAL At12L CCTK_ATTRIBUTE_UNUSED = At12[index];
    CCTK_REAL At12rhsL CCTK_ATTRIBUTE_UNUSED = At12rhs[index];
    CCTK_REAL At13L CCTK_ATTRIBUTE_UNUSED = At13[index];
    CCTK_REAL At13rhsL CCTK_ATTRIBUTE_UNUSED = At13rhs[index];
    CCTK_REAL At22L CCTK_ATTRIBUTE_UNUSED = At22[index];
    CCTK_REAL At22rhsL CCTK_ATTRIBUTE_UNUSED = At22rhs[index];
    CCTK_REAL At23L CCTK_ATTRIBUTE_UNUSED = At23[index];
    CCTK_REAL At23rhsL CCTK_ATTRIBUTE_UNUSED = At23rhs[index];
    CCTK_REAL At33L CCTK_ATTRIBUTE_UNUSED = At33[index];
    CCTK_REAL At33rhsL CCTK_ATTRIBUTE_UNUSED = At33rhs[index];
    CCTK_REAL B1L CCTK_ATTRIBUTE_UNUSED = B1[index];
    CCTK_REAL B1rhsL CCTK_ATTRIBUTE_UNUSED = B1rhs[index];
    CCTK_REAL B2L CCTK_ATTRIBUTE_UNUSED = B2[index];
    CCTK_REAL B2rhsL CCTK_ATTRIBUTE_UNUSED = B2rhs[index];
    CCTK_REAL B3L CCTK_ATTRIBUTE_UNUSED = B3[index];
    CCTK_REAL B3rhsL CCTK_ATTRIBUTE_UNUSED = B3rhs[index];
    CCTK_REAL beta1L CCTK_ATTRIBUTE_UNUSED = beta1[index];
    CCTK_REAL beta1rhsL CCTK_ATTRIBUTE_UNUSED = beta1rhs[index];
    CCTK_REAL beta2L CCTK_ATTRIBUTE_UNUSED = beta2[index];
    CCTK_REAL beta2rhsL CCTK_ATTRIBUTE_UNUSED = beta2rhs[index];
    CCTK_REAL beta3L CCTK_ATTRIBUTE_UNUSED = beta3[index];
    CCTK_REAL beta3rhsL CCTK_ATTRIBUTE_UNUSED = beta3rhs[index];
    CCTK_REAL gt11L CCTK_ATTRIBUTE_UNUSED = gt11[index];
    CCTK_REAL gt11rhsL CCTK_ATTRIBUTE_UNUSED = gt11rhs[index];
    CCTK_REAL gt12L CCTK_ATTRIBUTE_UNUSED = gt12[index];
    CCTK_REAL gt12rhsL CCTK_ATTRIBUTE_UNUSED = gt12rhs[index];
    CCTK_REAL gt13L CCTK_ATTRIBUTE_UNUSED = gt13[index];
    CCTK_REAL gt13rhsL CCTK_ATTRIBUTE_UNUSED = gt13rhs[index];
    CCTK_REAL gt22L CCTK_ATTRIBUTE_UNUSED = gt22[index];
    CCTK_REAL gt22rhsL CCTK_ATTRIBUTE_UNUSED = gt22rhs[index];
    CCTK_REAL gt23L CCTK_ATTRIBUTE_UNUSED = gt23[index];
    CCTK_REAL gt23rhsL CCTK_ATTRIBUTE_UNUSED = gt23rhs[index];
    CCTK_REAL gt33L CCTK_ATTRIBUTE_UNUSED = gt33[index];
    CCTK_REAL gt33rhsL CCTK_ATTRIBUTE_UNUSED = gt33rhs[index];
    CCTK_REAL phiL CCTK_ATTRIBUTE_UNUSED = phi[index];
    CCTK_REAL phirhsL CCTK_ATTRIBUTE_UNUSED = phirhs[index];
    CCTK_REAL trKL CCTK_ATTRIBUTE_UNUSED = trK[index];
    CCTK_REAL trKrhsL CCTK_ATTRIBUTE_UNUSED = trKrhs[index];
    CCTK_REAL Xt1L CCTK_ATTRIBUTE_UNUSED = Xt1[index];
    CCTK_REAL Xt1rhsL CCTK_ATTRIBUTE_UNUSED = Xt1rhs[index];
    CCTK_REAL Xt2L CCTK_ATTRIBUTE_UNUSED = Xt2[index];
    CCTK_REAL Xt2rhsL CCTK_ATTRIBUTE_UNUSED = Xt2rhs[index];
    CCTK_REAL Xt3L CCTK_ATTRIBUTE_UNUSED = Xt3[index];
    CCTK_REAL Xt3rhsL CCTK_ATTRIBUTE_UNUSED = Xt3rhs[index];
    
    /* Include user supplied include files */
    /* Precompute derivatives */
    const CCTK_REAL PDupwindNthAnti1A CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&A[index]);
    const CCTK_REAL PDupwindNthSymm1A CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&A[index]);
    const CCTK_REAL PDupwindNthAnti2A CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&A[index]);
    const CCTK_REAL PDupwindNthSymm2A CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&A[index]);
    const CCTK_REAL PDupwindNthAnti3A CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&A[index]);
    const CCTK_REAL PDupwindNthSymm3A CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&A[index]);
    const CCTK_REAL PDupwindNthAnti1alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&alpha[index]);
    const CCTK_REAL PDupwindNthSymm1alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&alpha[index]);
    const CCTK_REAL PDupwindNthAnti2alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&alpha[index]);
    const CCTK_REAL PDupwindNthSymm2alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&alpha[index]);
    const CCTK_REAL PDupwindNthAnti3alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&alpha[index]);
    const CCTK_REAL PDupwindNthSymm3alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&alpha[index]);
    const CCTK_REAL PDupwindNthAnti1At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At11[index]);
    const CCTK_REAL PDupwindNthSymm1At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At11[index]);
    const CCTK_REAL PDupwindNthAnti2At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At11[index]);
    const CCTK_REAL PDupwindNthSymm2At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At11[index]);
    const CCTK_REAL PDupwindNthAnti3At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At11[index]);
    const CCTK_REAL PDupwindNthSymm3At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At11[index]);
    const CCTK_REAL PDupwindNthAnti1At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At12[index]);
    const CCTK_REAL PDupwindNthSymm1At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At12[index]);
    const CCTK_REAL PDupwindNthAnti2At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At12[index]);
    const CCTK_REAL PDupwindNthSymm2At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At12[index]);
    const CCTK_REAL PDupwindNthAnti3At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At12[index]);
    const CCTK_REAL PDupwindNthSymm3At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At12[index]);
    const CCTK_REAL PDupwindNthAnti1At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At13[index]);
    const CCTK_REAL PDupwindNthSymm1At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At13[index]);
    const CCTK_REAL PDupwindNthAnti2At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At13[index]);
    const CCTK_REAL PDupwindNthSymm2At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At13[index]);
    const CCTK_REAL PDupwindNthAnti3At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At13[index]);
    const CCTK_REAL PDupwindNthSymm3At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At13[index]);
    const CCTK_REAL PDupwindNthAnti1At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At22[index]);
    const CCTK_REAL PDupwindNthSymm1At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At22[index]);
    const CCTK_REAL PDupwindNthAnti2At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At22[index]);
    const CCTK_REAL PDupwindNthSymm2At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At22[index]);
    const CCTK_REAL PDupwindNthAnti3At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At22[index]);
    const CCTK_REAL PDupwindNthSymm3At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At22[index]);
    const CCTK_REAL PDupwindNthAnti1At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At23[index]);
    const CCTK_REAL PDupwindNthSymm1At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At23[index]);
    const CCTK_REAL PDupwindNthAnti2At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At23[index]);
    const CCTK_REAL PDupwindNthSymm2At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At23[index]);
    const CCTK_REAL PDupwindNthAnti3At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At23[index]);
    const CCTK_REAL PDupwindNthSymm3At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At23[index]);
    const CCTK_REAL PDupwindNthAnti1At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At33[index]);
    const CCTK_REAL PDupwindNthSymm1At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At33[index]);
    const CCTK_REAL PDupwindNthAnti2At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At33[index]);
    const CCTK_REAL PDupwindNthSymm2At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At33[index]);
    const CCTK_REAL PDupwindNthAnti3At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At33[index]);
    const CCTK_REAL PDupwindNthSymm3At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At33[index]);
    const CCTK_REAL PDupwindNthAnti1B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&B1[index]);
    const CCTK_REAL PDupwindNthSymm1B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&B1[index]);
    const CCTK_REAL PDupwindNthAnti2B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&B1[index]);
    const CCTK_REAL PDupwindNthSymm2B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&B1[index]);
    const CCTK_REAL PDupwindNthAnti3B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&B1[index]);
    const CCTK_REAL PDupwindNthSymm3B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&B1[index]);
    const CCTK_REAL PDupwindNthAnti1B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&B2[index]);
    const CCTK_REAL PDupwindNthSymm1B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&B2[index]);
    const CCTK_REAL PDupwindNthAnti2B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&B2[index]);
    const CCTK_REAL PDupwindNthSymm2B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&B2[index]);
    const CCTK_REAL PDupwindNthAnti3B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&B2[index]);
    const CCTK_REAL PDupwindNthSymm3B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&B2[index]);
    const CCTK_REAL PDupwindNthAnti1B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&B3[index]);
    const CCTK_REAL PDupwindNthSymm1B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&B3[index]);
    const CCTK_REAL PDupwindNthAnti2B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&B3[index]);
    const CCTK_REAL PDupwindNthSymm2B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&B3[index]);
    const CCTK_REAL PDupwindNthAnti3B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&B3[index]);
    const CCTK_REAL PDupwindNthSymm3B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&B3[index]);
    const CCTK_REAL PDupwindNthAnti1beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&beta1[index]);
    const CCTK_REAL PDupwindNthSymm1beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&beta1[index]);
    const CCTK_REAL PDupwindNthAnti2beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&beta1[index]);
    const CCTK_REAL PDupwindNthSymm2beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&beta1[index]);
    const CCTK_REAL PDupwindNthAnti3beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&beta1[index]);
    const CCTK_REAL PDupwindNthSymm3beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&beta1[index]);
    const CCTK_REAL PDupwindNthAnti1beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&beta2[index]);
    const CCTK_REAL PDupwindNthSymm1beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&beta2[index]);
    const CCTK_REAL PDupwindNthAnti2beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&beta2[index]);
    const CCTK_REAL PDupwindNthSymm2beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&beta2[index]);
    const CCTK_REAL PDupwindNthAnti3beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&beta2[index]);
    const CCTK_REAL PDupwindNthSymm3beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&beta2[index]);
    const CCTK_REAL PDupwindNthAnti1beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&beta3[index]);
    const CCTK_REAL PDupwindNthSymm1beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&beta3[index]);
    const CCTK_REAL PDupwindNthAnti2beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&beta3[index]);
    const CCTK_REAL PDupwindNthSymm2beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&beta3[index]);
    const CCTK_REAL PDupwindNthAnti3beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&beta3[index]);
    const CCTK_REAL PDupwindNthSymm3beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&beta3[index]);
    const CCTK_REAL PDupwindNthAnti1gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt11[index]);
    const CCTK_REAL PDupwindNthSymm1gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt11[index]);
    const CCTK_REAL PDupwindNthAnti2gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt11[index]);
    const CCTK_REAL PDupwindNthSymm2gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt11[index]);
    const CCTK_REAL PDupwindNthAnti3gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt11[index]);
    const CCTK_REAL PDupwindNthSymm3gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt11[index]);
    const CCTK_REAL PDupwindNthAnti1gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt12[index]);
    const CCTK_REAL PDupwindNthSymm1gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt12[index]);
    const CCTK_REAL PDupwindNthAnti2gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt12[index]);
    const CCTK_REAL PDupwindNthSymm2gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt12[index]);
    const CCTK_REAL PDupwindNthAnti3gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt12[index]);
    const CCTK_REAL PDupwindNthSymm3gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt12[index]);
    const CCTK_REAL PDupwindNthAnti1gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt13[index]);
    const CCTK_REAL PDupwindNthSymm1gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt13[index]);
    const CCTK_REAL PDupwindNthAnti2gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt13[index]);
    const CCTK_REAL PDupwindNthSymm2gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt13[index]);
    const CCTK_REAL PDupwindNthAnti3gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt13[index]);
    const CCTK_REAL PDupwindNthSymm3gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt13[index]);
    const CCTK_REAL PDupwindNthAnti1gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt22[index]);
    const CCTK_REAL PDupwindNthSymm1gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt22[index]);
    const CCTK_REAL PDupwindNthAnti2gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt22[index]);
    const CCTK_REAL PDupwindNthSymm2gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt22[index]);
    const CCTK_REAL PDupwindNthAnti3gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt22[index]);
    const CCTK_REAL PDupwindNthSymm3gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt22[index]);
    const CCTK_REAL PDupwindNthAnti1gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt23[index]);
    const CCTK_REAL PDupwindNthSymm1gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt23[index]);
    const CCTK_REAL PDupwindNthAnti2gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt23[index]);
    const CCTK_REAL PDupwindNthSymm2gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt23[index]);
    const CCTK_REAL PDupwindNthAnti3gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt23[index]);
    const CCTK_REAL PDupwindNthSymm3gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt23[index]);
    const CCTK_REAL PDupwindNthAnti1gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt33[index]);
    const CCTK_REAL PDupwindNthSymm1gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt33[index]);
    const CCTK_REAL PDupwindNthAnti2gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt33[index]);
    const CCTK_REAL PDupwindNthSymm2gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt33[index]);
    const CCTK_REAL PDupwindNthAnti3gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt33[index]);
    const CCTK_REAL PDupwindNthSymm3gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt33[index]);
    const CCTK_REAL PDupwindNthAnti1phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&phi[index]);
    const CCTK_REAL PDupwindNthSymm1phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&phi[index]);
    const CCTK_REAL PDupwindNthAnti2phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&phi[index]);
    const CCTK_REAL PDupwindNthSymm2phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&phi[index]);
    const CCTK_REAL PDupwindNthAnti3phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&phi[index]);
    const CCTK_REAL PDupwindNthSymm3phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&phi[index]);
    const CCTK_REAL PDupwindNthAnti1trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&trK[index]);
    const CCTK_REAL PDupwindNthSymm1trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&trK[index]);
    const CCTK_REAL PDupwindNthAnti2trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&trK[index]);
    const CCTK_REAL PDupwindNthSymm2trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&trK[index]);
    const CCTK_REAL PDupwindNthAnti3trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&trK[index]);
    const CCTK_REAL PDupwindNthSymm3trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&trK[index]);
    const CCTK_REAL PDupwindNthAnti1Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&Xt1[index]);
    const CCTK_REAL PDupwindNthSymm1Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&Xt1[index]);
    const CCTK_REAL PDupwindNthAnti2Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&Xt1[index]);
    const CCTK_REAL PDupwindNthSymm2Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&Xt1[index]);
    const CCTK_REAL PDupwindNthAnti3Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&Xt1[index]);
    const CCTK_REAL PDupwindNthSymm3Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&Xt1[index]);
    const CCTK_REAL PDupwindNthAnti1Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&Xt2[index]);
    const CCTK_REAL PDupwindNthSymm1Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&Xt2[index]);
    const CCTK_REAL PDupwindNthAnti2Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&Xt2[index]);
    const CCTK_REAL PDupwindNthSymm2Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&Xt2[index]);
    const CCTK_REAL PDupwindNthAnti3Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&Xt2[index]);
    const CCTK_REAL PDupwindNthSymm3Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&Xt2[index]);
    const CCTK_REAL PDupwindNthAnti1Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&Xt3[index]);
    const CCTK_REAL PDupwindNthSymm1Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&Xt3[index]);
    const CCTK_REAL PDupwindNthAnti2Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&Xt3[index]);
    const CCTK_REAL PDupwindNthSymm2Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&Xt3[index]);
    const CCTK_REAL PDupwindNthAnti3Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&Xt3[index]);
    const CCTK_REAL PDupwindNthSymm3Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&Xt3[index]);
    /* Calculate temporaries and grid functions */
    ptrdiff_t dir1 CCTK_ATTRIBUTE_UNUSED = isgn(beta1L);
    
    ptrdiff_t dir2 CCTK_ATTRIBUTE_UNUSED = isgn(beta2L);
    
    ptrdiff_t dir3 CCTK_ATTRIBUTE_UNUSED = isgn(beta3L);
    
    phirhsL = phirhsL + beta1L*PDupwindNthAnti1phi + 
      beta2L*PDupwindNthAnti2phi + beta3L*PDupwindNthAnti3phi + 
      PDupwindNthSymm1phi*fabs(beta1L) + PDupwindNthSymm2phi*fabs(beta2L) + 
      PDupwindNthSymm3phi*fabs(beta3L);
    
    gt11rhsL = gt11rhsL + beta1L*PDupwindNthAnti1gt11 + 
      beta2L*PDupwindNthAnti2gt11 + beta3L*PDupwindNthAnti3gt11 + 
      PDupwindNthSymm1gt11*fabs(beta1L) + PDupwindNthSymm2gt11*fabs(beta2L) + 
      PDupwindNthSymm3gt11*fabs(beta3L);
    
    gt12rhsL = gt12rhsL + beta1L*PDupwindNthAnti1gt12 + 
      beta2L*PDupwindNthAnti2gt12 + beta3L*PDupwindNthAnti3gt12 + 
      PDupwindNthSymm1gt12*fabs(beta1L) + PDupwindNthSymm2gt12*fabs(beta2L) + 
      PDupwindNthSymm3gt12*fabs(beta3L);
    
    gt13rhsL = gt13rhsL + beta1L*PDupwindNthAnti1gt13 + 
      beta2L*PDupwindNthAnti2gt13 + beta3L*PDupwindNthAnti3gt13 + 
      PDupwindNthSymm1gt13*fabs(beta1L) + PDupwindNthSymm2gt13*fabs(beta2L) + 
      PDupwindNthSymm3gt13*fabs(beta3L);
    
    gt22rhsL = gt22rhsL + beta1L*PDupwindNthAnti1gt22 + 
      beta2L*PDupwindNthAnti2gt22 + beta3L*PDupwindNthAnti3gt22 + 
      PDupwindNthSymm1gt22*fabs(beta1L) + PDupwindNthSymm2gt22*fabs(beta2L) + 
      PDupwindNthSymm3gt22*fabs(beta3L);
    
    gt23rhsL = gt23rhsL + beta1L*PDupwindNthAnti1gt23 + 
      beta2L*PDupwindNthAnti2gt23 + beta3L*PDupwindNthAnti3gt23 + 
      PDupwindNthSymm1gt23*fabs(beta1L) + PDupwindNthSymm2gt23*fabs(beta2L) + 
      PDupwindNthSymm3gt23*fabs(beta3L);
    
    gt33rhsL = gt33rhsL + beta1L*PDupwindNthAnti1gt33 + 
      beta2L*PDupwindNthAnti2gt33 + beta3L*PDupwindNthAnti3gt33 + 
      PDupwindNthSymm1gt33*fabs(beta1L) + PDupwindNthSymm2gt33*fabs(beta2L) + 
      PDupwindNthSymm3gt33*fabs(beta3L);
    
    Xt1rhsL = Xt1rhsL + beta1L*PDupwindNthAnti1Xt1 + 
      beta2L*PDupwindNthAnti2Xt1 + beta3L*PDupwindNthAnti3Xt1 + 
      PDupwindNthSymm1Xt1*fabs(beta1L) + PDupwindNthSymm2Xt1*fabs(beta2L) + 
      PDupwindNthSymm3Xt1*fabs(beta3L);
    
    Xt2rhsL = Xt2rhsL + beta1L*PDupwindNthAnti1Xt2 + 
      beta2L*PDupwindNthAnti2Xt2 + beta3L*PDupwindNthAnti3Xt2 + 
      PDupwindNthSymm1Xt2*fabs(beta1L) + PDupwindNthSymm2Xt2*fabs(beta2L) + 
      PDupwindNthSymm3Xt2*fabs(beta3L);
    
    Xt3rhsL = Xt3rhsL + beta1L*PDupwindNthAnti1Xt3 + 
      beta2L*PDupwindNthAnti2Xt3 + beta3L*PDupwindNthAnti3Xt3 + 
      PDupwindNthSymm1Xt3*fabs(beta1L) + PDupwindNthSymm2Xt3*fabs(beta2L) + 
      PDupwindNthSymm3Xt3*fabs(beta3L);
    
    trKrhsL = trKrhsL + beta1L*PDupwindNthAnti1trK + 
      beta2L*PDupwindNthAnti2trK + beta3L*PDupwindNthAnti3trK + 
      PDupwindNthSymm1trK*fabs(beta1L) + PDupwindNthSymm2trK*fabs(beta2L) + 
      PDupwindNthSymm3trK*fabs(beta3L);
    
    At11rhsL = At11rhsL + beta1L*PDupwindNthAnti1At11 + 
      beta2L*PDupwindNthAnti2At11 + beta3L*PDupwindNthAnti3At11 + 
      PDupwindNthSymm1At11*fabs(beta1L) + PDupwindNthSymm2At11*fabs(beta2L) + 
      PDupwindNthSymm3At11*fabs(beta3L);
    
    At12rhsL = At12rhsL + beta1L*PDupwindNthAnti1At12 + 
      beta2L*PDupwindNthAnti2At12 + beta3L*PDupwindNthAnti3At12 + 
      PDupwindNthSymm1At12*fabs(beta1L) + PDupwindNthSymm2At12*fabs(beta2L) + 
      PDupwindNthSymm3At12*fabs(beta3L);
    
    At13rhsL = At13rhsL + beta1L*PDupwindNthAnti1At13 + 
      beta2L*PDupwindNthAnti2At13 + beta3L*PDupwindNthAnti3At13 + 
      PDupwindNthSymm1At13*fabs(beta1L) + PDupwindNthSymm2At13*fabs(beta2L) + 
      PDupwindNthSymm3At13*fabs(beta3L);
    
    At22rhsL = At22rhsL + beta1L*PDupwindNthAnti1At22 + 
      beta2L*PDupwindNthAnti2At22 + beta3L*PDupwindNthAnti3At22 + 
      PDupwindNthSymm1At22*fabs(beta1L) + PDupwindNthSymm2At22*fabs(beta2L) + 
      PDupwindNthSymm3At22*fabs(beta3L);
    
    At23rhsL = At23rhsL + beta1L*PDupwindNthAnti1At23 + 
      beta2L*PDupwindNthAnti2At23 + beta3L*PDupwindNthAnti3At23 + 
      PDupwindNthSymm1At23*fabs(beta1L) + PDupwindNthSymm2At23*fabs(beta2L) + 
      PDupwindNthSymm3At23*fabs(beta3L);
    
    At33rhsL = At33rhsL + beta1L*PDupwindNthAnti1At33 + 
      beta2L*PDupwindNthAnti2At33 + beta3L*PDupwindNthAnti3At33 + 
      PDupwindNthSymm1At33*fabs(beta1L) + PDupwindNthSymm2At33*fabs(beta2L) + 
      PDupwindNthSymm3At33*fabs(beta3L);
    
    alpharhsL = alpharhsL + 
      LapseAdvectionCoeff*(beta1L*PDupwindNthAnti1alpha + 
      beta2L*PDupwindNthAnti2alpha + beta3L*PDupwindNthAnti3alpha + 
      PDupwindNthSymm1alpha*fabs(beta1L) + PDupwindNthSymm2alpha*fabs(beta2L) 
      + PDupwindNthSymm3alpha*fabs(beta3L));
    
    ArhsL = ArhsL + 
      LapseACoeff*(LapseAdvectionCoeff*(beta1L*PDupwindNthAnti1A + 
      beta2L*PDupwindNthAnti2A + beta3L*PDupwindNthAnti3A + 
      PDupwindNthSymm1A*fabs(beta1L) + PDupwindNthSymm2A*fabs(beta2L) + 
      PDupwindNthSymm3A*fabs(beta3L)) - (-1 + 
      LapseAdvectionCoeff)*(beta1L*PDupwindNthAnti1trK + 
      beta2L*PDupwindNthAnti2trK + beta3L*PDupwindNthAnti3trK + 
      PDupwindNthSymm1trK*fabs(beta1L) + PDupwindNthSymm2trK*fabs(beta2L) + 
      PDupwindNthSymm3trK*fabs(beta3L)));
    
    beta1rhsL = beta1rhsL + 
      ShiftAdvectionCoeff*(beta1L*PDupwindNthAnti1beta1 + 
      beta2L*PDupwindNthAnti2beta1 + beta3L*PDupwindNthAnti3beta1 + 
      PDupwindNthSymm1beta1*fabs(beta1L) + PDupwindNthSymm2beta1*fabs(beta2L) 
      + PDupwindNthSymm3beta1*fabs(beta3L));
    
    beta2rhsL = beta2rhsL + 
      ShiftAdvectionCoeff*(beta1L*PDupwindNthAnti1beta2 + 
      beta2L*PDupwindNthAnti2beta2 + beta3L*PDupwindNthAnti3beta2 + 
      PDupwindNthSymm1beta2*fabs(beta1L) + PDupwindNthSymm2beta2*fabs(beta2L) 
      + PDupwindNthSymm3beta2*fabs(beta3L));
    
    beta3rhsL = beta3rhsL + 
      ShiftAdvectionCoeff*(beta1L*PDupwindNthAnti1beta3 + 
      beta2L*PDupwindNthAnti2beta3 + beta3L*PDupwindNthAnti3beta3 + 
      PDupwindNthSymm1beta3*fabs(beta1L) + PDupwindNthSymm2beta3*fabs(beta2L) 
      + PDupwindNthSymm3beta3*fabs(beta3L));
    
    B1rhsL = B1rhsL + 
      ShiftBCoeff*(ShiftAdvectionCoeff*(beta1L*PDupwindNthAnti1B1 + 
      beta2L*PDupwindNthAnti2B1 + beta3L*PDupwindNthAnti3B1 + 
      PDupwindNthSymm1B1*fabs(beta1L) + PDupwindNthSymm2B1*fabs(beta2L) + 
      PDupwindNthSymm3B1*fabs(beta3L)) - (-1 + 
      ShiftAdvectionCoeff)*(beta1L*PDupwindNthAnti1Xt1 + 
      beta2L*PDupwindNthAnti2Xt1 + beta3L*PDupwindNthAnti3Xt1 + 
      PDupwindNthSymm1Xt1*fabs(beta1L) + PDupwindNthSymm2Xt1*fabs(beta2L) + 
      PDupwindNthSymm3Xt1*fabs(beta3L)));
    
    B2rhsL = B2rhsL + 
      ShiftBCoeff*(ShiftAdvectionCoeff*(beta1L*PDupwindNthAnti1B2 + 
      beta2L*PDupwindNthAnti2B2 + beta3L*PDupwindNthAnti3B2 + 
      PDupwindNthSymm1B2*fabs(beta1L) + PDupwindNthSymm2B2*fabs(beta2L) + 
      PDupwindNthSymm3B2*fabs(beta3L)) - (-1 + 
      ShiftAdvectionCoeff)*(beta1L*PDupwindNthAnti1Xt2 + 
      beta2L*PDupwindNthAnti2Xt2 + beta3L*PDupwindNthAnti3Xt2 + 
      PDupwindNthSymm1Xt2*fabs(beta1L) + PDupwindNthSymm2Xt2*fabs(beta2L) + 
      PDupwindNthSymm3Xt2*fabs(beta3L)));
    
    B3rhsL = B3rhsL + 
      ShiftBCoeff*(ShiftAdvectionCoeff*(beta1L*PDupwindNthAnti1B3 + 
      beta2L*PDupwindNthAnti2B3 + beta3L*PDupwindNthAnti3B3 + 
      PDupwindNthSymm1B3*fabs(beta1L) + PDupwindNthSymm2B3*fabs(beta2L) + 
      PDupwindNthSymm3B3*fabs(beta3L)) - (-1 + 
      ShiftAdvectionCoeff)*(beta1L*PDupwindNthAnti1Xt3 + 
      beta2L*PDupwindNthAnti2Xt3 + beta3L*PDupwindNthAnti3Xt3 + 
      PDupwindNthSymm1Xt3*fabs(beta1L) + PDupwindNthSymm2Xt3*fabs(beta2L) + 
      PDupwindNthSymm3Xt3*fabs(beta3L)));
    /* Copy local copies back to grid functions */
    alpharhs[index] = alpharhsL;
    Arhs[index] = ArhsL;
    At11rhs[index] = At11rhsL;
    At12rhs[index] = At12rhsL;
    At13rhs[index] = At13rhsL;
    At22rhs[index] = At22rhsL;
    At23rhs[index] = At23rhsL;
    At33rhs[index] = At33rhsL;
    B1rhs[index] = B1rhsL;
    B2rhs[index] = B2rhsL;
    B3rhs[index] = B3rhsL;
    beta1rhs[index] = beta1rhsL;
    beta2rhs[index] = beta2rhsL;
    beta3rhs[index] = beta3rhsL;
    gt11rhs[index] = gt11rhsL;
    gt12rhs[index] = gt12rhsL;
    gt13rhs[index] = gt13rhsL;
    gt22rhs[index] = gt22rhsL;
    gt23rhs[index] = gt23rhsL;
    gt33rhs[index] = gt33rhsL;
    phirhs[index] = phirhsL;
    trKrhs[index] = trKrhsL;
    Xt1rhs[index] = Xt1rhsL;
    Xt2rhs[index] = Xt2rhsL;
    Xt3rhs[index] = Xt3rhsL;
  }
  CCTK_ENDLOOP3(HOST_ML_BSSN_Advect);
}
extern "C" void HOST_ML_BSSN_Advect(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering HOST_ML_BSSN_Advect_Body");
  }
  if (cctk_iteration % HOST_ML_BSSN_Advect_calc_every != HOST_ML_BSSN_Advect_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_BSSN::ML_curv",
    "ML_BSSN::ML_curvrhs",
    "ML_BSSN::ML_dtlapse",
    "ML_BSSN::ML_dtlapserhs",
    "ML_BSSN::ML_dtshift",
    "ML_BSSN::ML_dtshiftrhs",
    "ML_BSSN::ML_Gamma",
    "ML_BSSN::ML_Gammarhs",
    "ML_BSSN::ML_lapse",
    "ML_BSSN::ML_lapserhs",
    "ML_BSSN::ML_log_confac",
    "ML_BSSN::ML_log_confacrhs",
    "ML_BSSN::ML_metric",
    "ML_BSSN::ML_metricrhs",
    "ML_BSSN::ML_shift",
    "ML_BSSN::ML_shiftrhs",
    "ML_BSSN::ML_trace_curv",
    "ML_BSSN::ML_trace_curvrhs"};
  AssertGroupStorage(cctkGH, "HOST_ML_BSSN_Advect", 18, groups);
  
  EnsureStencilFits(cctkGH, "HOST_ML_BSSN_Advect", 5, 5, 5);
  
  LoopOverInterior(cctkGH, HOST_ML_BSSN_Advect_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving HOST_ML_BSSN_Advect_Body");
  }
}

} // namespace ML_BSSN
