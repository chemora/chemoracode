#undef KRANC_DIFF_FUNCTIONS
#define KRANC_C
#include "Differencing.h"
#include "Kranc.hh"
using namespace CCTK_THORN;

#undef KRANC_GFOFFSET3D
#define KRANC_GFOFFSET3D(u,i,j,k) I3D(u,i,j,k)


CAKERNEL_DEVICE_ML_BSSN_RHS_Dalpha3_2_etc_Begin
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  #define ConditionExpression(x) (x)
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = params.cagh_dx;
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = params.cagh_dy;
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = params.cagh_dz;
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = params.cagh_dt;
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = params.cagh_time;
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = 1.0/dx;
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = 1.0/dy;
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = 1.0/dz;
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 1/3.0;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 2.0/3.0;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 4.0/3.0;
  const CCTK_REAL keightthird CCTK_ATTRIBUTE_UNUSED = 8.0/3.0;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5 * dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5 * dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5 * dzi;
  /* Initialize predefined quantities */
  const CCTK_REAL p1o1024dx CCTK_ATTRIBUTE_UNUSED = 0.0009765625*pow(dx,-1);
  const CCTK_REAL p1o1024dy CCTK_ATTRIBUTE_UNUSED = 0.0009765625*pow(dy,-1);
  const CCTK_REAL p1o1024dz CCTK_ATTRIBUTE_UNUSED = 0.0009765625*pow(dz,-1);
  const CCTK_REAL p1o1680dx CCTK_ATTRIBUTE_UNUSED = 0.000595238095238095238095238095238*pow(dx,-1);
  const CCTK_REAL p1o1680dy CCTK_ATTRIBUTE_UNUSED = 0.000595238095238095238095238095238*pow(dy,-1);
  const CCTK_REAL p1o1680dz CCTK_ATTRIBUTE_UNUSED = 0.000595238095238095238095238095238*pow(dz,-1);
  const CCTK_REAL p1o2dx CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dx,-1);
  const CCTK_REAL p1o2dy CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dy,-1);
  const CCTK_REAL p1o2dz CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dz,-1);
  const CCTK_REAL p1o5040dx2 CCTK_ATTRIBUTE_UNUSED = 0.000198412698412698412698412698413*pow(dx,-2);
  const CCTK_REAL p1o5040dy2 CCTK_ATTRIBUTE_UNUSED = 0.000198412698412698412698412698413*pow(dy,-2);
  const CCTK_REAL p1o5040dz2 CCTK_ATTRIBUTE_UNUSED = 0.000198412698412698412698412698413*pow(dz,-2);
  const CCTK_REAL p1o560dx CCTK_ATTRIBUTE_UNUSED = 0.00178571428571428571428571428571*pow(dx,-1);
  const CCTK_REAL p1o560dy CCTK_ATTRIBUTE_UNUSED = 0.00178571428571428571428571428571*pow(dy,-1);
  const CCTK_REAL p1o560dz CCTK_ATTRIBUTE_UNUSED = 0.00178571428571428571428571428571*pow(dz,-1);
  const CCTK_REAL p1o705600dxdy CCTK_ATTRIBUTE_UNUSED = 1.41723356009070294784580498866e-6*pow(dx,-1)*pow(dy,-1);
  const CCTK_REAL p1o705600dxdz CCTK_ATTRIBUTE_UNUSED = 1.41723356009070294784580498866e-6*pow(dx,-1)*pow(dz,-1);
  const CCTK_REAL p1o705600dydz CCTK_ATTRIBUTE_UNUSED = 1.41723356009070294784580498866e-6*pow(dy,-1)*pow(dz,-1);
  const CCTK_REAL p1o840dx CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dx,-1);
  const CCTK_REAL p1o840dy CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dy,-1);
  const CCTK_REAL p1o840dz CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dz,-1);
  const CCTK_REAL p1odx CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL p1ody CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL p1odz CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL pm1o2dx CCTK_ATTRIBUTE_UNUSED = -0.5*pow(dx,-1);
  const CCTK_REAL pm1o2dy CCTK_ATTRIBUTE_UNUSED = -0.5*pow(dy,-1);
  const CCTK_REAL pm1o2dz CCTK_ATTRIBUTE_UNUSED = -0.5*pow(dz,-1);
  const CCTK_REAL pm1o840dx CCTK_ATTRIBUTE_UNUSED = -0.00119047619047619047619047619048*pow(dx,-1);
  const CCTK_REAL pm1o840dy CCTK_ATTRIBUTE_UNUSED = -0.00119047619047619047619047619048*pow(dy,-1);
  const CCTK_REAL pm1o840dz CCTK_ATTRIBUTE_UNUSED = -0.00119047619047619047619047619048*pow(dz,-1);
  CAKERNEL_DEVICE_ML_BSSN_RHS_Dalpha3_2_etc_Computations_Begin
    /* Calculate temporaries and grid functions */
    I3D(DPDstandardNthalpha32,0,0,0) = 
      PDstandardNth2(DPDstandardNthalpha3);
    I3D(DPDstandardNthbeta132,0,0,0) = 
      PDstandardNth2(DPDstandardNthbeta13);
    I3D(DPDstandardNthbeta232,0,0,0) = 
      PDstandardNth2(DPDstandardNthbeta23);
    I3D(DPDstandardNthbeta332,0,0,0) = 
      PDstandardNth2(DPDstandardNthbeta33);
    I3D(DPDstandardNthgt1132,0,0,0) = PDstandardNth2(DPDstandardNthgt113);
    I3D(DPDstandardNthgt1232,0,0,0) = PDstandardNth2(DPDstandardNthgt123);
    I3D(DPDstandardNthgt1332,0,0,0) = PDstandardNth2(DPDstandardNthgt133);
    I3D(DPDstandardNthgt2232,0,0,0) = PDstandardNth2(DPDstandardNthgt223);
    I3D(DPDstandardNthgt2332,0,0,0) = PDstandardNth2(DPDstandardNthgt233);
    I3D(DPDstandardNthgt3332,0,0,0) = PDstandardNth2(DPDstandardNthgt333);
    I3D(DPDstandardNthphi32,0,0,0) = PDstandardNth2(DPDstandardNthphi3);
    
  CAKERNEL_DEVICE_ML_BSSN_RHS_Dalpha3_2_etc_Computations_End
  
CAKERNEL_DEVICE_ML_BSSN_RHS_Dalpha3_2_etc_End
