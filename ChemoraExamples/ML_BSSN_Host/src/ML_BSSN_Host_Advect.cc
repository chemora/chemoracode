/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"
#include "loopcontrol.h"
#include "vectors.h"

namespace ML_BSSN_Host {

extern "C" void ML_BSSN_Host_Advect_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_Host_Advect_calc_every != ML_BSSN_Host_Advect_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_curvrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_curvrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_dtlapserhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_dtlapserhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_dtshiftrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_dtshiftrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_Gammarhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_Gammarhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_lapserhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_lapserhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_log_confacrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_log_confacrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_metricrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_metricrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_shiftrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_shiftrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_trace_curvrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_trace_curvrhs.");
  return;
}

static void ML_BSSN_Host_Advect_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL_VEC t CCTK_ATTRIBUTE_UNUSED = ToReal(cctk_time);
  const CCTK_REAL_VEC cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_ORIGIN_SPACE(0));
  const CCTK_REAL_VEC cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_ORIGIN_SPACE(1));
  const CCTK_REAL_VEC cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_ORIGIN_SPACE(2));
  const CCTK_REAL_VEC dt CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_DELTA_TIME);
  const CCTK_REAL_VEC dx CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_DELTA_SPACE(0));
  const CCTK_REAL_VEC dy CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_DELTA_SPACE(1));
  const CCTK_REAL_VEC dz CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_DELTA_SPACE(2));
  const CCTK_REAL_VEC dxi CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dx);
  const CCTK_REAL_VEC dyi CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dy);
  const CCTK_REAL_VEC dzi CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dz);
  const CCTK_REAL_VEC khalf CCTK_ATTRIBUTE_UNUSED = ToReal(0.5);
  const CCTK_REAL_VEC kthird CCTK_ATTRIBUTE_UNUSED = 
    ToReal(0.333333333333333333333333333333);
  const CCTK_REAL_VEC ktwothird CCTK_ATTRIBUTE_UNUSED = 
    ToReal(0.666666666666666666666666666667);
  const CCTK_REAL_VEC kfourthird CCTK_ATTRIBUTE_UNUSED = 
    ToReal(1.33333333333333333333333333333);
  const CCTK_REAL_VEC hdxi CCTK_ATTRIBUTE_UNUSED = 
    kmul(dxi,ToReal(0.5));
  const CCTK_REAL_VEC hdyi CCTK_ATTRIBUTE_UNUSED = 
    kmul(dyi,ToReal(0.5));
  const CCTK_REAL_VEC hdzi CCTK_ATTRIBUTE_UNUSED = 
    kmul(dzi,ToReal(0.5));
  /* Initialize predefined quantities */
  const CCTK_REAL_VEC p1o1024dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0009765625),dx);
  const CCTK_REAL_VEC p1o1024dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0009765625),dy);
  const CCTK_REAL_VEC p1o1024dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0009765625),dz);
  const CCTK_REAL_VEC p1o1680dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000595238095238095238095238095238),dx);
  const CCTK_REAL_VEC p1o1680dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000595238095238095238095238095238),dy);
  const CCTK_REAL_VEC p1o1680dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000595238095238095238095238095238),dz);
  const CCTK_REAL_VEC p1o2dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.5),dx);
  const CCTK_REAL_VEC p1o2dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.5),dy);
  const CCTK_REAL_VEC p1o2dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.5),dz);
  const CCTK_REAL_VEC p1o5040dx2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000198412698412698412698412698413),kmul(dx,dx));
  const CCTK_REAL_VEC p1o5040dy2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000198412698412698412698412698413),kmul(dy,dy));
  const CCTK_REAL_VEC p1o5040dz2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000198412698412698412698412698413),kmul(dz,dz));
  const CCTK_REAL_VEC p1o560dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00178571428571428571428571428571),dx);
  const CCTK_REAL_VEC p1o560dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00178571428571428571428571428571),dy);
  const CCTK_REAL_VEC p1o560dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00178571428571428571428571428571),dz);
  const CCTK_REAL_VEC p1o705600dxdy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1.41723356009070294784580498866e-6),kmul(dx,dy));
  const CCTK_REAL_VEC p1o705600dxdz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1.41723356009070294784580498866e-6),kmul(dx,dz));
  const CCTK_REAL_VEC p1o705600dydz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1.41723356009070294784580498866e-6),kmul(dy,dz));
  const CCTK_REAL_VEC p1o840dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00119047619047619047619047619048),dx);
  const CCTK_REAL_VEC p1o840dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00119047619047619047619047619048),dy);
  const CCTK_REAL_VEC p1o840dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00119047619047619047619047619048),dz);
  const CCTK_REAL_VEC p1odx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dx);
  const CCTK_REAL_VEC p1ody CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dy);
  const CCTK_REAL_VEC p1odz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dz);
  const CCTK_REAL_VEC pm1o2dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(-0.5),dx);
  const CCTK_REAL_VEC pm1o2dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(-0.5),dy);
  const CCTK_REAL_VEC pm1o2dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(-0.5),dz);
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3STR(ML_BSSN_Host_Advect,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2],
    vecimin,vecimax, CCTK_REAL_VEC_SIZE)
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL_VEC AL CCTK_ATTRIBUTE_UNUSED = vec_load(A[index]);
    CCTK_REAL_VEC alphaL CCTK_ATTRIBUTE_UNUSED = vec_load(alpha[index]);
    CCTK_REAL_VEC alpharhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(alpharhs[index]);
    CCTK_REAL_VEC ArhsL CCTK_ATTRIBUTE_UNUSED = vec_load(Arhs[index]);
    CCTK_REAL_VEC At11L CCTK_ATTRIBUTE_UNUSED = vec_load(At11[index]);
    CCTK_REAL_VEC At11rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(At11rhs[index]);
    CCTK_REAL_VEC At12L CCTK_ATTRIBUTE_UNUSED = vec_load(At12[index]);
    CCTK_REAL_VEC At12rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(At12rhs[index]);
    CCTK_REAL_VEC At13L CCTK_ATTRIBUTE_UNUSED = vec_load(At13[index]);
    CCTK_REAL_VEC At13rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(At13rhs[index]);
    CCTK_REAL_VEC At22L CCTK_ATTRIBUTE_UNUSED = vec_load(At22[index]);
    CCTK_REAL_VEC At22rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(At22rhs[index]);
    CCTK_REAL_VEC At23L CCTK_ATTRIBUTE_UNUSED = vec_load(At23[index]);
    CCTK_REAL_VEC At23rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(At23rhs[index]);
    CCTK_REAL_VEC At33L CCTK_ATTRIBUTE_UNUSED = vec_load(At33[index]);
    CCTK_REAL_VEC At33rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(At33rhs[index]);
    CCTK_REAL_VEC B1L CCTK_ATTRIBUTE_UNUSED = vec_load(B1[index]);
    CCTK_REAL_VEC B1rhsL CCTK_ATTRIBUTE_UNUSED = vec_load(B1rhs[index]);
    CCTK_REAL_VEC B2L CCTK_ATTRIBUTE_UNUSED = vec_load(B2[index]);
    CCTK_REAL_VEC B2rhsL CCTK_ATTRIBUTE_UNUSED = vec_load(B2rhs[index]);
    CCTK_REAL_VEC B3L CCTK_ATTRIBUTE_UNUSED = vec_load(B3[index]);
    CCTK_REAL_VEC B3rhsL CCTK_ATTRIBUTE_UNUSED = vec_load(B3rhs[index]);
    CCTK_REAL_VEC beta1L CCTK_ATTRIBUTE_UNUSED = vec_load(beta1[index]);
    CCTK_REAL_VEC beta1rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(beta1rhs[index]);
    CCTK_REAL_VEC beta2L CCTK_ATTRIBUTE_UNUSED = vec_load(beta2[index]);
    CCTK_REAL_VEC beta2rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(beta2rhs[index]);
    CCTK_REAL_VEC beta3L CCTK_ATTRIBUTE_UNUSED = vec_load(beta3[index]);
    CCTK_REAL_VEC beta3rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(beta3rhs[index]);
    CCTK_REAL_VEC gt11L CCTK_ATTRIBUTE_UNUSED = vec_load(gt11[index]);
    CCTK_REAL_VEC gt11rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(gt11rhs[index]);
    CCTK_REAL_VEC gt12L CCTK_ATTRIBUTE_UNUSED = vec_load(gt12[index]);
    CCTK_REAL_VEC gt12rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(gt12rhs[index]);
    CCTK_REAL_VEC gt13L CCTK_ATTRIBUTE_UNUSED = vec_load(gt13[index]);
    CCTK_REAL_VEC gt13rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(gt13rhs[index]);
    CCTK_REAL_VEC gt22L CCTK_ATTRIBUTE_UNUSED = vec_load(gt22[index]);
    CCTK_REAL_VEC gt22rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(gt22rhs[index]);
    CCTK_REAL_VEC gt23L CCTK_ATTRIBUTE_UNUSED = vec_load(gt23[index]);
    CCTK_REAL_VEC gt23rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(gt23rhs[index]);
    CCTK_REAL_VEC gt33L CCTK_ATTRIBUTE_UNUSED = vec_load(gt33[index]);
    CCTK_REAL_VEC gt33rhsL CCTK_ATTRIBUTE_UNUSED = 
      vec_load(gt33rhs[index]);
    CCTK_REAL_VEC phiL CCTK_ATTRIBUTE_UNUSED = vec_load(phi[index]);
    CCTK_REAL_VEC phirhsL CCTK_ATTRIBUTE_UNUSED = vec_load(phirhs[index]);
    CCTK_REAL_VEC trKL CCTK_ATTRIBUTE_UNUSED = vec_load(trK[index]);
    CCTK_REAL_VEC trKrhsL CCTK_ATTRIBUTE_UNUSED = vec_load(trKrhs[index]);
    CCTK_REAL_VEC Xt1L CCTK_ATTRIBUTE_UNUSED = vec_load(Xt1[index]);
    CCTK_REAL_VEC Xt1rhsL CCTK_ATTRIBUTE_UNUSED = vec_load(Xt1rhs[index]);
    CCTK_REAL_VEC Xt2L CCTK_ATTRIBUTE_UNUSED = vec_load(Xt2[index]);
    CCTK_REAL_VEC Xt2rhsL CCTK_ATTRIBUTE_UNUSED = vec_load(Xt2rhs[index]);
    CCTK_REAL_VEC Xt3L CCTK_ATTRIBUTE_UNUSED = vec_load(Xt3[index]);
    CCTK_REAL_VEC Xt3rhsL CCTK_ATTRIBUTE_UNUSED = vec_load(Xt3rhs[index]);
    
    /* Include user supplied include files */
    /* Precompute derivatives */
    const CCTK_REAL_VEC PDupwindNthAnti1A CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&A[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1A CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&A[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2A CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&A[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2A CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&A[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3A CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&A[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3A CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&A[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&alpha[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&alpha[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&alpha[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&alpha[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&alpha[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3alpha CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&alpha[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At11[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At11[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At11[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At11[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At11[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3At11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At11[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At12[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At12[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At12[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At12[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At12[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3At12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At12[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At13[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At13[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At13[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At13[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At13[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3At13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At13[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At22[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At22[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At22[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At22[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At22[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3At22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At22[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At23[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At23[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At23[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At23[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At23[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3At23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At23[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&At33[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&At33[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&At33[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&At33[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&At33[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3At33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&At33[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&B1[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&B1[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&B1[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&B1[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&B1[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3B1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&B1[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&B2[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&B2[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&B2[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&B2[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&B2[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3B2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&B2[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&B3[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&B3[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&B3[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&B3[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&B3[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3B3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&B3[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&beta1[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&beta1[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&beta1[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&beta1[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&beta1[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3beta1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&beta1[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&beta2[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&beta2[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&beta2[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&beta2[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&beta2[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3beta2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&beta2[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&beta3[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&beta3[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&beta3[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&beta3[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&beta3[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3beta3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&beta3[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt11[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt11[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt11[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt11[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt11[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3gt11 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt11[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt12[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt12[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt12[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt12[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt12[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3gt12 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt12[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt13[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt13[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt13[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt13[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt13[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3gt13 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt13[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt22[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt22[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt22[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt22[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt22[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3gt22 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt22[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt23[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt23[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt23[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt23[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt23[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3gt23 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt23[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&gt33[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&gt33[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&gt33[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&gt33[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&gt33[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3gt33 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&gt33[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&phi[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&phi[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&phi[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&phi[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&phi[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3phi CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&phi[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&trK[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&trK[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&trK[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&trK[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&trK[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3trK CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&trK[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&Xt1[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&Xt1[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&Xt1[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&Xt1[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&Xt1[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3Xt1 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&Xt1[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&Xt2[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&Xt2[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&Xt2[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&Xt2[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&Xt2[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3Xt2 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&Xt2[index]);
    const CCTK_REAL_VEC PDupwindNthAnti1Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti1(&Xt3[index]);
    const CCTK_REAL_VEC PDupwindNthSymm1Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm1(&Xt3[index]);
    const CCTK_REAL_VEC PDupwindNthAnti2Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti2(&Xt3[index]);
    const CCTK_REAL_VEC PDupwindNthSymm2Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm2(&Xt3[index]);
    const CCTK_REAL_VEC PDupwindNthAnti3Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthAnti3(&Xt3[index]);
    const CCTK_REAL_VEC PDupwindNthSymm3Xt3 CCTK_ATTRIBUTE_UNUSED = PDupwindNthSymm3(&Xt3[index]);
    /* Calculate temporaries and grid functions */
    ptrdiff_t dir1 CCTK_ATTRIBUTE_UNUSED = kisgn(beta1L);
    
    ptrdiff_t dir2 CCTK_ATTRIBUTE_UNUSED = kisgn(beta2L);
    
    ptrdiff_t dir3 CCTK_ATTRIBUTE_UNUSED = kisgn(beta3L);
    
    phirhsL = 
      kadd(phirhsL,kmadd(beta1L,PDupwindNthAnti1phi,kmadd(beta2L,PDupwindNthAnti2phi,kmadd(beta3L,PDupwindNthAnti3phi,kmadd(PDupwindNthSymm1phi,kfabs(beta1L),kmadd(PDupwindNthSymm2phi,kfabs(beta2L),kmul(PDupwindNthSymm3phi,kfabs(beta3L))))))));
    
    gt11rhsL = 
      kadd(gt11rhsL,kmadd(beta1L,PDupwindNthAnti1gt11,kmadd(beta2L,PDupwindNthAnti2gt11,kmadd(beta3L,PDupwindNthAnti3gt11,kmadd(PDupwindNthSymm1gt11,kfabs(beta1L),kmadd(PDupwindNthSymm2gt11,kfabs(beta2L),kmul(PDupwindNthSymm3gt11,kfabs(beta3L))))))));
    
    gt12rhsL = 
      kadd(gt12rhsL,kmadd(beta1L,PDupwindNthAnti1gt12,kmadd(beta2L,PDupwindNthAnti2gt12,kmadd(beta3L,PDupwindNthAnti3gt12,kmadd(PDupwindNthSymm1gt12,kfabs(beta1L),kmadd(PDupwindNthSymm2gt12,kfabs(beta2L),kmul(PDupwindNthSymm3gt12,kfabs(beta3L))))))));
    
    gt13rhsL = 
      kadd(gt13rhsL,kmadd(beta1L,PDupwindNthAnti1gt13,kmadd(beta2L,PDupwindNthAnti2gt13,kmadd(beta3L,PDupwindNthAnti3gt13,kmadd(PDupwindNthSymm1gt13,kfabs(beta1L),kmadd(PDupwindNthSymm2gt13,kfabs(beta2L),kmul(PDupwindNthSymm3gt13,kfabs(beta3L))))))));
    
    gt22rhsL = 
      kadd(gt22rhsL,kmadd(beta1L,PDupwindNthAnti1gt22,kmadd(beta2L,PDupwindNthAnti2gt22,kmadd(beta3L,PDupwindNthAnti3gt22,kmadd(PDupwindNthSymm1gt22,kfabs(beta1L),kmadd(PDupwindNthSymm2gt22,kfabs(beta2L),kmul(PDupwindNthSymm3gt22,kfabs(beta3L))))))));
    
    gt23rhsL = 
      kadd(gt23rhsL,kmadd(beta1L,PDupwindNthAnti1gt23,kmadd(beta2L,PDupwindNthAnti2gt23,kmadd(beta3L,PDupwindNthAnti3gt23,kmadd(PDupwindNthSymm1gt23,kfabs(beta1L),kmadd(PDupwindNthSymm2gt23,kfabs(beta2L),kmul(PDupwindNthSymm3gt23,kfabs(beta3L))))))));
    
    gt33rhsL = 
      kadd(gt33rhsL,kmadd(beta1L,PDupwindNthAnti1gt33,kmadd(beta2L,PDupwindNthAnti2gt33,kmadd(beta3L,PDupwindNthAnti3gt33,kmadd(PDupwindNthSymm1gt33,kfabs(beta1L),kmadd(PDupwindNthSymm2gt33,kfabs(beta2L),kmul(PDupwindNthSymm3gt33,kfabs(beta3L))))))));
    
    Xt1rhsL = 
      kadd(Xt1rhsL,kmadd(beta1L,PDupwindNthAnti1Xt1,kmadd(beta2L,PDupwindNthAnti2Xt1,kmadd(beta3L,PDupwindNthAnti3Xt1,kmadd(PDupwindNthSymm1Xt1,kfabs(beta1L),kmadd(PDupwindNthSymm2Xt1,kfabs(beta2L),kmul(PDupwindNthSymm3Xt1,kfabs(beta3L))))))));
    
    Xt2rhsL = 
      kadd(Xt2rhsL,kmadd(beta1L,PDupwindNthAnti1Xt2,kmadd(beta2L,PDupwindNthAnti2Xt2,kmadd(beta3L,PDupwindNthAnti3Xt2,kmadd(PDupwindNthSymm1Xt2,kfabs(beta1L),kmadd(PDupwindNthSymm2Xt2,kfabs(beta2L),kmul(PDupwindNthSymm3Xt2,kfabs(beta3L))))))));
    
    Xt3rhsL = 
      kadd(Xt3rhsL,kmadd(beta1L,PDupwindNthAnti1Xt3,kmadd(beta2L,PDupwindNthAnti2Xt3,kmadd(beta3L,PDupwindNthAnti3Xt3,kmadd(PDupwindNthSymm1Xt3,kfabs(beta1L),kmadd(PDupwindNthSymm2Xt3,kfabs(beta2L),kmul(PDupwindNthSymm3Xt3,kfabs(beta3L))))))));
    
    trKrhsL = 
      kadd(trKrhsL,kmadd(beta1L,PDupwindNthAnti1trK,kmadd(beta2L,PDupwindNthAnti2trK,kmadd(beta3L,PDupwindNthAnti3trK,kmadd(PDupwindNthSymm1trK,kfabs(beta1L),kmadd(PDupwindNthSymm2trK,kfabs(beta2L),kmul(PDupwindNthSymm3trK,kfabs(beta3L))))))));
    
    At11rhsL = 
      kadd(At11rhsL,kmadd(beta1L,PDupwindNthAnti1At11,kmadd(beta2L,PDupwindNthAnti2At11,kmadd(beta3L,PDupwindNthAnti3At11,kmadd(PDupwindNthSymm1At11,kfabs(beta1L),kmadd(PDupwindNthSymm2At11,kfabs(beta2L),kmul(PDupwindNthSymm3At11,kfabs(beta3L))))))));
    
    At12rhsL = 
      kadd(At12rhsL,kmadd(beta1L,PDupwindNthAnti1At12,kmadd(beta2L,PDupwindNthAnti2At12,kmadd(beta3L,PDupwindNthAnti3At12,kmadd(PDupwindNthSymm1At12,kfabs(beta1L),kmadd(PDupwindNthSymm2At12,kfabs(beta2L),kmul(PDupwindNthSymm3At12,kfabs(beta3L))))))));
    
    At13rhsL = 
      kadd(At13rhsL,kmadd(beta1L,PDupwindNthAnti1At13,kmadd(beta2L,PDupwindNthAnti2At13,kmadd(beta3L,PDupwindNthAnti3At13,kmadd(PDupwindNthSymm1At13,kfabs(beta1L),kmadd(PDupwindNthSymm2At13,kfabs(beta2L),kmul(PDupwindNthSymm3At13,kfabs(beta3L))))))));
    
    At22rhsL = 
      kadd(At22rhsL,kmadd(beta1L,PDupwindNthAnti1At22,kmadd(beta2L,PDupwindNthAnti2At22,kmadd(beta3L,PDupwindNthAnti3At22,kmadd(PDupwindNthSymm1At22,kfabs(beta1L),kmadd(PDupwindNthSymm2At22,kfabs(beta2L),kmul(PDupwindNthSymm3At22,kfabs(beta3L))))))));
    
    At23rhsL = 
      kadd(At23rhsL,kmadd(beta1L,PDupwindNthAnti1At23,kmadd(beta2L,PDupwindNthAnti2At23,kmadd(beta3L,PDupwindNthAnti3At23,kmadd(PDupwindNthSymm1At23,kfabs(beta1L),kmadd(PDupwindNthSymm2At23,kfabs(beta2L),kmul(PDupwindNthSymm3At23,kfabs(beta3L))))))));
    
    At33rhsL = 
      kadd(At33rhsL,kmadd(beta1L,PDupwindNthAnti1At33,kmadd(beta2L,PDupwindNthAnti2At33,kmadd(beta3L,PDupwindNthAnti3At33,kmadd(PDupwindNthSymm1At33,kfabs(beta1L),kmadd(PDupwindNthSymm2At33,kfabs(beta2L),kmul(PDupwindNthSymm3At33,kfabs(beta3L))))))));
    
    alpharhsL = 
      kmadd(ToReal(LapseAdvectionCoeff),kmadd(beta1L,PDupwindNthAnti1alpha,kmadd(beta2L,PDupwindNthAnti2alpha,kmadd(beta3L,PDupwindNthAnti3alpha,kmadd(PDupwindNthSymm1alpha,kfabs(beta1L),kmadd(PDupwindNthSymm2alpha,kfabs(beta2L),kmul(PDupwindNthSymm3alpha,kfabs(beta3L))))))),alpharhsL);
    
    ArhsL = 
      kmadd(ToReal(LapseACoeff),kmsub(ToReal(LapseAdvectionCoeff),kmadd(beta1L,PDupwindNthAnti1A,kmadd(beta2L,PDupwindNthAnti2A,kmadd(beta3L,PDupwindNthAnti3A,kmadd(PDupwindNthSymm1A,kfabs(beta1L),kmadd(PDupwindNthSymm2A,kfabs(beta2L),kmul(PDupwindNthSymm3A,kfabs(beta3L))))))),kmul(kmadd(beta1L,PDupwindNthAnti1trK,kmadd(beta2L,PDupwindNthAnti2trK,kmadd(beta3L,PDupwindNthAnti3trK,kmadd(PDupwindNthSymm1trK,kfabs(beta1L),kmadd(PDupwindNthSymm2trK,kfabs(beta2L),kmul(PDupwindNthSymm3trK,kfabs(beta3L))))))),ToReal(-1 
      + LapseAdvectionCoeff))),ArhsL);
    
    beta1rhsL = 
      kmadd(ToReal(ShiftAdvectionCoeff),kmadd(beta1L,PDupwindNthAnti1beta1,kmadd(beta2L,PDupwindNthAnti2beta1,kmadd(beta3L,PDupwindNthAnti3beta1,kmadd(PDupwindNthSymm1beta1,kfabs(beta1L),kmadd(PDupwindNthSymm2beta1,kfabs(beta2L),kmul(PDupwindNthSymm3beta1,kfabs(beta3L))))))),beta1rhsL);
    
    beta2rhsL = 
      kmadd(ToReal(ShiftAdvectionCoeff),kmadd(beta1L,PDupwindNthAnti1beta2,kmadd(beta2L,PDupwindNthAnti2beta2,kmadd(beta3L,PDupwindNthAnti3beta2,kmadd(PDupwindNthSymm1beta2,kfabs(beta1L),kmadd(PDupwindNthSymm2beta2,kfabs(beta2L),kmul(PDupwindNthSymm3beta2,kfabs(beta3L))))))),beta2rhsL);
    
    beta3rhsL = 
      kmadd(ToReal(ShiftAdvectionCoeff),kmadd(beta1L,PDupwindNthAnti1beta3,kmadd(beta2L,PDupwindNthAnti2beta3,kmadd(beta3L,PDupwindNthAnti3beta3,kmadd(PDupwindNthSymm1beta3,kfabs(beta1L),kmadd(PDupwindNthSymm2beta3,kfabs(beta2L),kmul(PDupwindNthSymm3beta3,kfabs(beta3L))))))),beta3rhsL);
    
    B1rhsL = 
      kmadd(ToReal(ShiftBCoeff),kmsub(ToReal(ShiftAdvectionCoeff),kmadd(beta1L,PDupwindNthAnti1B1,kmadd(beta2L,PDupwindNthAnti2B1,kmadd(beta3L,PDupwindNthAnti3B1,kmadd(PDupwindNthSymm1B1,kfabs(beta1L),kmadd(PDupwindNthSymm2B1,kfabs(beta2L),kmul(PDupwindNthSymm3B1,kfabs(beta3L))))))),kmul(kmadd(beta1L,PDupwindNthAnti1Xt1,kmadd(beta2L,PDupwindNthAnti2Xt1,kmadd(beta3L,PDupwindNthAnti3Xt1,kmadd(PDupwindNthSymm1Xt1,kfabs(beta1L),kmadd(PDupwindNthSymm2Xt1,kfabs(beta2L),kmul(PDupwindNthSymm3Xt1,kfabs(beta3L))))))),ToReal(-1 
      + ShiftAdvectionCoeff))),B1rhsL);
    
    B2rhsL = 
      kmadd(ToReal(ShiftBCoeff),kmsub(ToReal(ShiftAdvectionCoeff),kmadd(beta1L,PDupwindNthAnti1B2,kmadd(beta2L,PDupwindNthAnti2B2,kmadd(beta3L,PDupwindNthAnti3B2,kmadd(PDupwindNthSymm1B2,kfabs(beta1L),kmadd(PDupwindNthSymm2B2,kfabs(beta2L),kmul(PDupwindNthSymm3B2,kfabs(beta3L))))))),kmul(kmadd(beta1L,PDupwindNthAnti1Xt2,kmadd(beta2L,PDupwindNthAnti2Xt2,kmadd(beta3L,PDupwindNthAnti3Xt2,kmadd(PDupwindNthSymm1Xt2,kfabs(beta1L),kmadd(PDupwindNthSymm2Xt2,kfabs(beta2L),kmul(PDupwindNthSymm3Xt2,kfabs(beta3L))))))),ToReal(-1 
      + ShiftAdvectionCoeff))),B2rhsL);
    
    B3rhsL = 
      kmadd(ToReal(ShiftBCoeff),kmsub(ToReal(ShiftAdvectionCoeff),kmadd(beta1L,PDupwindNthAnti1B3,kmadd(beta2L,PDupwindNthAnti2B3,kmadd(beta3L,PDupwindNthAnti3B3,kmadd(PDupwindNthSymm1B3,kfabs(beta1L),kmadd(PDupwindNthSymm2B3,kfabs(beta2L),kmul(PDupwindNthSymm3B3,kfabs(beta3L))))))),kmul(kmadd(beta1L,PDupwindNthAnti1Xt3,kmadd(beta2L,PDupwindNthAnti2Xt3,kmadd(beta3L,PDupwindNthAnti3Xt3,kmadd(PDupwindNthSymm1Xt3,kfabs(beta1L),kmadd(PDupwindNthSymm2Xt3,kfabs(beta2L),kmul(PDupwindNthSymm3Xt3,kfabs(beta3L))))))),ToReal(-1 
      + ShiftAdvectionCoeff))),B3rhsL);
    /* Copy local copies back to grid functions */
    vec_store_partial_prepare(i,vecimin,vecimax);
    vec_store_nta_partial(alpharhs[index],alpharhsL);
    vec_store_nta_partial(Arhs[index],ArhsL);
    vec_store_nta_partial(At11rhs[index],At11rhsL);
    vec_store_nta_partial(At12rhs[index],At12rhsL);
    vec_store_nta_partial(At13rhs[index],At13rhsL);
    vec_store_nta_partial(At22rhs[index],At22rhsL);
    vec_store_nta_partial(At23rhs[index],At23rhsL);
    vec_store_nta_partial(At33rhs[index],At33rhsL);
    vec_store_nta_partial(B1rhs[index],B1rhsL);
    vec_store_nta_partial(B2rhs[index],B2rhsL);
    vec_store_nta_partial(B3rhs[index],B3rhsL);
    vec_store_nta_partial(beta1rhs[index],beta1rhsL);
    vec_store_nta_partial(beta2rhs[index],beta2rhsL);
    vec_store_nta_partial(beta3rhs[index],beta3rhsL);
    vec_store_nta_partial(gt11rhs[index],gt11rhsL);
    vec_store_nta_partial(gt12rhs[index],gt12rhsL);
    vec_store_nta_partial(gt13rhs[index],gt13rhsL);
    vec_store_nta_partial(gt22rhs[index],gt22rhsL);
    vec_store_nta_partial(gt23rhs[index],gt23rhsL);
    vec_store_nta_partial(gt33rhs[index],gt33rhsL);
    vec_store_nta_partial(phirhs[index],phirhsL);
    vec_store_nta_partial(trKrhs[index],trKrhsL);
    vec_store_nta_partial(Xt1rhs[index],Xt1rhsL);
    vec_store_nta_partial(Xt2rhs[index],Xt2rhsL);
    vec_store_nta_partial(Xt3rhs[index],Xt3rhsL);
  }
  CCTK_ENDLOOP3STR(ML_BSSN_Host_Advect);
}
extern "C" void ML_BSSN_Host_Advect(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_Host_Advect_Body");
  }
  if (cctk_iteration % ML_BSSN_Host_Advect_calc_every != ML_BSSN_Host_Advect_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_BSSN_Host::ML_curv",
    "ML_BSSN_Host::ML_curvrhs",
    "ML_BSSN_Host::ML_dtlapse",
    "ML_BSSN_Host::ML_dtlapserhs",
    "ML_BSSN_Host::ML_dtshift",
    "ML_BSSN_Host::ML_dtshiftrhs",
    "ML_BSSN_Host::ML_Gamma",
    "ML_BSSN_Host::ML_Gammarhs",
    "ML_BSSN_Host::ML_lapse",
    "ML_BSSN_Host::ML_lapserhs",
    "ML_BSSN_Host::ML_log_confac",
    "ML_BSSN_Host::ML_log_confacrhs",
    "ML_BSSN_Host::ML_metric",
    "ML_BSSN_Host::ML_metricrhs",
    "ML_BSSN_Host::ML_shift",
    "ML_BSSN_Host::ML_shiftrhs",
    "ML_BSSN_Host::ML_trace_curv",
    "ML_BSSN_Host::ML_trace_curvrhs"};
  AssertGroupStorage(cctkGH, "ML_BSSN_Host_Advect", 18, groups);
  
  EnsureStencilFits(cctkGH, "ML_BSSN_Host_Advect", 5, 5, 5);
  
  LoopOverInterior(cctkGH, ML_BSSN_Host_Advect_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_Host_Advect_Body");
  }
}

} // namespace ML_BSSN_Host
