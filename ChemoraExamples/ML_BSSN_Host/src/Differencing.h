#include <assert.h>
#include "vectors.h"

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth1(u) (kmul(p1o840dx,kmadd(ToReal(-672),KRANC_GFOFFSET3D(u,-1,0,0),kmadd(ToReal(672),KRANC_GFOFFSET3D(u,1,0,0),kmadd(ToReal(168),KRANC_GFOFFSET3D(u,-2,0,0),kmadd(ToReal(-168),KRANC_GFOFFSET3D(u,2,0,0),kmadd(ToReal(-32),KRANC_GFOFFSET3D(u,-3,0,0),kmadd(ToReal(32),KRANC_GFOFFSET3D(u,3,0,0),kmadd(ToReal(3),KRANC_GFOFFSET3D(u,-4,0,0),kmul(ToReal(-3),KRANC_GFOFFSET3D(u,4,0,0)))))))))))
#else
#  define PDstandardNth1(u) (PDstandardNth1_impl(u,p1o840dx,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o840dx,kmadd(ToReal(-672),KRANC_GFOFFSET3D(u,-1,0,0),kmadd(ToReal(672),KRANC_GFOFFSET3D(u,1,0,0),kmadd(ToReal(168),KRANC_GFOFFSET3D(u,-2,0,0),kmadd(ToReal(-168),KRANC_GFOFFSET3D(u,2,0,0),kmadd(ToReal(-32),KRANC_GFOFFSET3D(u,-3,0,0),kmadd(ToReal(32),KRANC_GFOFFSET3D(u,3,0,0),kmadd(ToReal(3),KRANC_GFOFFSET3D(u,-4,0,0),kmul(ToReal(-3),KRANC_GFOFFSET3D(u,4,0,0))))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth2(u) (kmul(p1o840dy,kmadd(ToReal(-672),KRANC_GFOFFSET3D(u,0,-1,0),kmadd(ToReal(672),KRANC_GFOFFSET3D(u,0,1,0),kmadd(ToReal(168),KRANC_GFOFFSET3D(u,0,-2,0),kmadd(ToReal(-168),KRANC_GFOFFSET3D(u,0,2,0),kmadd(ToReal(-32),KRANC_GFOFFSET3D(u,0,-3,0),kmadd(ToReal(32),KRANC_GFOFFSET3D(u,0,3,0),kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,-4,0),kmul(ToReal(-3),KRANC_GFOFFSET3D(u,0,4,0)))))))))))
#else
#  define PDstandardNth2(u) (PDstandardNth2_impl(u,p1o840dy,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o840dy,kmadd(ToReal(-672),KRANC_GFOFFSET3D(u,0,-1,0),kmadd(ToReal(672),KRANC_GFOFFSET3D(u,0,1,0),kmadd(ToReal(168),KRANC_GFOFFSET3D(u,0,-2,0),kmadd(ToReal(-168),KRANC_GFOFFSET3D(u,0,2,0),kmadd(ToReal(-32),KRANC_GFOFFSET3D(u,0,-3,0),kmadd(ToReal(32),KRANC_GFOFFSET3D(u,0,3,0),kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,-4,0),kmul(ToReal(-3),KRANC_GFOFFSET3D(u,0,4,0))))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth3(u) (kmul(p1o840dz,kmadd(ToReal(-672),KRANC_GFOFFSET3D(u,0,0,-1),kmadd(ToReal(672),KRANC_GFOFFSET3D(u,0,0,1),kmadd(ToReal(168),KRANC_GFOFFSET3D(u,0,0,-2),kmadd(ToReal(-168),KRANC_GFOFFSET3D(u,0,0,2),kmadd(ToReal(-32),KRANC_GFOFFSET3D(u,0,0,-3),kmadd(ToReal(32),KRANC_GFOFFSET3D(u,0,0,3),kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,0,-4),kmul(ToReal(-3),KRANC_GFOFFSET3D(u,0,0,4)))))))))))
#else
#  define PDstandardNth3(u) (PDstandardNth3_impl(u,p1o840dz,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardNth2_impl(u, p1o840dz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth11(u) (kmul(p1o5040dx2,kmadd(ToReal(-14350),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(8064),kadd(KRANC_GFOFFSET3D(u,-1,0,0),KRANC_GFOFFSET3D(u,1,0,0)),kmadd(ToReal(-1008),kadd(KRANC_GFOFFSET3D(u,-2,0,0),KRANC_GFOFFSET3D(u,2,0,0)),kmadd(ToReal(128),kadd(KRANC_GFOFFSET3D(u,-3,0,0),KRANC_GFOFFSET3D(u,3,0,0)),kmul(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,-4,0,0),KRANC_GFOFFSET3D(u,4,0,0)))))))))
#else
#  define PDstandardNth11(u) (PDstandardNth11_impl(u,p1o5040dx2,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth11_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o5040dx2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth11_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o5040dx2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o5040dx2,kmadd(ToReal(-14350),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(8064),kadd(KRANC_GFOFFSET3D(u,-1,0,0),KRANC_GFOFFSET3D(u,1,0,0)),kmadd(ToReal(-1008),kadd(KRANC_GFOFFSET3D(u,-2,0,0),KRANC_GFOFFSET3D(u,2,0,0)),kmadd(ToReal(128),kadd(KRANC_GFOFFSET3D(u,-3,0,0),KRANC_GFOFFSET3D(u,3,0,0)),kmul(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,-4,0,0),KRANC_GFOFFSET3D(u,4,0,0))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth22(u) (kmul(p1o5040dy2,kmadd(ToReal(-14350),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(8064),kadd(KRANC_GFOFFSET3D(u,0,-1,0),KRANC_GFOFFSET3D(u,0,1,0)),kmadd(ToReal(-1008),kadd(KRANC_GFOFFSET3D(u,0,-2,0),KRANC_GFOFFSET3D(u,0,2,0)),kmadd(ToReal(128),kadd(KRANC_GFOFFSET3D(u,0,-3,0),KRANC_GFOFFSET3D(u,0,3,0)),kmul(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,0,-4,0),KRANC_GFOFFSET3D(u,0,4,0)))))))))
#else
#  define PDstandardNth22(u) (PDstandardNth22_impl(u,p1o5040dy2,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth22_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o5040dy2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth22_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o5040dy2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o5040dy2,kmadd(ToReal(-14350),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(8064),kadd(KRANC_GFOFFSET3D(u,0,-1,0),KRANC_GFOFFSET3D(u,0,1,0)),kmadd(ToReal(-1008),kadd(KRANC_GFOFFSET3D(u,0,-2,0),KRANC_GFOFFSET3D(u,0,2,0)),kmadd(ToReal(128),kadd(KRANC_GFOFFSET3D(u,0,-3,0),KRANC_GFOFFSET3D(u,0,3,0)),kmul(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,0,-4,0),KRANC_GFOFFSET3D(u,0,4,0))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth33(u) (kmul(p1o5040dz2,kmadd(ToReal(-14350),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(8064),kadd(KRANC_GFOFFSET3D(u,0,0,-1),KRANC_GFOFFSET3D(u,0,0,1)),kmadd(ToReal(-1008),kadd(KRANC_GFOFFSET3D(u,0,0,-2),KRANC_GFOFFSET3D(u,0,0,2)),kmadd(ToReal(128),kadd(KRANC_GFOFFSET3D(u,0,0,-3),KRANC_GFOFFSET3D(u,0,0,3)),kmul(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,0,0,-4),KRANC_GFOFFSET3D(u,0,0,4)))))))))
#else
#  define PDstandardNth33(u) (PDstandardNth33_impl(u,p1o5040dz2,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth33_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o5040dz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth33_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o5040dz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardNth22_impl(u, p1o5040dz2, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth12(u) (kmul(p1o705600dxdy,kmadd(ToReal(-451584),kadd(KRANC_GFOFFSET3D(u,-1,1,0),KRANC_GFOFFSET3D(u,1,-1,0)),kmadd(ToReal(451584),kadd(KRANC_GFOFFSET3D(u,-1,-1,0),KRANC_GFOFFSET3D(u,1,1,0)),kmadd(ToReal(112896),kadd(KRANC_GFOFFSET3D(u,-1,2,0),kadd(KRANC_GFOFFSET3D(u,1,-2,0),kadd(KRANC_GFOFFSET3D(u,-2,1,0),KRANC_GFOFFSET3D(u,2,-1,0)))),kmadd(ToReal(-112896),kadd(KRANC_GFOFFSET3D(u,-1,-2,0),kadd(KRANC_GFOFFSET3D(u,1,2,0),kadd(KRANC_GFOFFSET3D(u,-2,-1,0),KRANC_GFOFFSET3D(u,2,1,0)))),kmadd(ToReal(-28224),kadd(KRANC_GFOFFSET3D(u,-2,2,0),KRANC_GFOFFSET3D(u,2,-2,0)),kmadd(ToReal(28224),kadd(KRANC_GFOFFSET3D(u,-2,-2,0),KRANC_GFOFFSET3D(u,2,2,0)),kmadd(ToReal(-21504),kadd(KRANC_GFOFFSET3D(u,-1,3,0),kadd(KRANC_GFOFFSET3D(u,1,-3,0),kadd(KRANC_GFOFFSET3D(u,-3,1,0),KRANC_GFOFFSET3D(u,3,-1,0)))),kmadd(ToReal(21504),kadd(KRANC_GFOFFSET3D(u,-1,-3,0),kadd(KRANC_GFOFFSET3D(u,1,3,0),kadd(KRANC_GFOFFSET3D(u,-3,-1,0),KRANC_GFOFFSET3D(u,3,1,0)))),kmadd(ToReal(5376),kadd(KRANC_GFOFFSET3D(u,-2,3,0),kadd(KRANC_GFOFFSET3D(u,2,-3,0),kadd(KRANC_GFOFFSET3D(u,-3,2,0),KRANC_GFOFFSET3D(u,3,-2,0)))),kmadd(ToReal(-5376),kadd(KRANC_GFOFFSET3D(u,-2,-3,0),kadd(KRANC_GFOFFSET3D(u,2,3,0),kadd(KRANC_GFOFFSET3D(u,-3,-2,0),KRANC_GFOFFSET3D(u,3,2,0)))),kmadd(ToReal(-1024),kadd(KRANC_GFOFFSET3D(u,-3,3,0),KRANC_GFOFFSET3D(u,3,-3,0)),kmadd(ToReal(1024),kadd(KRANC_GFOFFSET3D(u,-3,-3,0),KRANC_GFOFFSET3D(u,3,3,0)),kmadd(ToReal(2016),kadd(KRANC_GFOFFSET3D(u,-1,4,0),kadd(KRANC_GFOFFSET3D(u,1,-4,0),kadd(KRANC_GFOFFSET3D(u,-4,1,0),KRANC_GFOFFSET3D(u,4,-1,0)))),kmadd(ToReal(-2016),kadd(KRANC_GFOFFSET3D(u,-1,-4,0),kadd(KRANC_GFOFFSET3D(u,1,4,0),kadd(KRANC_GFOFFSET3D(u,-4,-1,0),KRANC_GFOFFSET3D(u,4,1,0)))),kmadd(ToReal(-504),kadd(KRANC_GFOFFSET3D(u,-2,4,0),kadd(KRANC_GFOFFSET3D(u,2,-4,0),kadd(KRANC_GFOFFSET3D(u,-4,2,0),KRANC_GFOFFSET3D(u,4,-2,0)))),kmadd(ToReal(504),kadd(KRANC_GFOFFSET3D(u,-2,-4,0),kadd(KRANC_GFOFFSET3D(u,2,4,0),kadd(KRANC_GFOFFSET3D(u,-4,-2,0),KRANC_GFOFFSET3D(u,4,2,0)))),kmadd(ToReal(96),kadd(KRANC_GFOFFSET3D(u,-3,4,0),kadd(KRANC_GFOFFSET3D(u,3,-4,0),kadd(KRANC_GFOFFSET3D(u,-4,3,0),KRANC_GFOFFSET3D(u,4,-3,0)))),kmadd(ToReal(-96),kadd(KRANC_GFOFFSET3D(u,-3,-4,0),kadd(KRANC_GFOFFSET3D(u,3,4,0),kadd(KRANC_GFOFFSET3D(u,-4,-3,0),KRANC_GFOFFSET3D(u,4,3,0)))),kmadd(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,-4,4,0),KRANC_GFOFFSET3D(u,4,-4,0)),kmul(ToReal(9),kadd(KRANC_GFOFFSET3D(u,-4,-4,0),KRANC_GFOFFSET3D(u,4,4,0))))))))))))))))))))))))
#else
#  define PDstandardNth12(u) (PDstandardNth12_impl(u,p1o705600dxdy,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth12_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth12_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o705600dxdy,kmadd(ToReal(-451584),kadd(KRANC_GFOFFSET3D(u,-1,1,0),KRANC_GFOFFSET3D(u,1,-1,0)),kmadd(ToReal(451584),kadd(KRANC_GFOFFSET3D(u,-1,-1,0),KRANC_GFOFFSET3D(u,1,1,0)),kmadd(ToReal(112896),kadd(KRANC_GFOFFSET3D(u,-1,2,0),kadd(KRANC_GFOFFSET3D(u,1,-2,0),kadd(KRANC_GFOFFSET3D(u,-2,1,0),KRANC_GFOFFSET3D(u,2,-1,0)))),kmadd(ToReal(-112896),kadd(KRANC_GFOFFSET3D(u,-1,-2,0),kadd(KRANC_GFOFFSET3D(u,1,2,0),kadd(KRANC_GFOFFSET3D(u,-2,-1,0),KRANC_GFOFFSET3D(u,2,1,0)))),kmadd(ToReal(-28224),kadd(KRANC_GFOFFSET3D(u,-2,2,0),KRANC_GFOFFSET3D(u,2,-2,0)),kmadd(ToReal(28224),kadd(KRANC_GFOFFSET3D(u,-2,-2,0),KRANC_GFOFFSET3D(u,2,2,0)),kmadd(ToReal(-21504),kadd(KRANC_GFOFFSET3D(u,-1,3,0),kadd(KRANC_GFOFFSET3D(u,1,-3,0),kadd(KRANC_GFOFFSET3D(u,-3,1,0),KRANC_GFOFFSET3D(u,3,-1,0)))),kmadd(ToReal(21504),kadd(KRANC_GFOFFSET3D(u,-1,-3,0),kadd(KRANC_GFOFFSET3D(u,1,3,0),kadd(KRANC_GFOFFSET3D(u,-3,-1,0),KRANC_GFOFFSET3D(u,3,1,0)))),kmadd(ToReal(5376),kadd(KRANC_GFOFFSET3D(u,-2,3,0),kadd(KRANC_GFOFFSET3D(u,2,-3,0),kadd(KRANC_GFOFFSET3D(u,-3,2,0),KRANC_GFOFFSET3D(u,3,-2,0)))),kmadd(ToReal(-5376),kadd(KRANC_GFOFFSET3D(u,-2,-3,0),kadd(KRANC_GFOFFSET3D(u,2,3,0),kadd(KRANC_GFOFFSET3D(u,-3,-2,0),KRANC_GFOFFSET3D(u,3,2,0)))),kmadd(ToReal(-1024),kadd(KRANC_GFOFFSET3D(u,-3,3,0),KRANC_GFOFFSET3D(u,3,-3,0)),kmadd(ToReal(1024),kadd(KRANC_GFOFFSET3D(u,-3,-3,0),KRANC_GFOFFSET3D(u,3,3,0)),kmadd(ToReal(2016),kadd(KRANC_GFOFFSET3D(u,-1,4,0),kadd(KRANC_GFOFFSET3D(u,1,-4,0),kadd(KRANC_GFOFFSET3D(u,-4,1,0),KRANC_GFOFFSET3D(u,4,-1,0)))),kmadd(ToReal(-2016),kadd(KRANC_GFOFFSET3D(u,-1,-4,0),kadd(KRANC_GFOFFSET3D(u,1,4,0),kadd(KRANC_GFOFFSET3D(u,-4,-1,0),KRANC_GFOFFSET3D(u,4,1,0)))),kmadd(ToReal(-504),kadd(KRANC_GFOFFSET3D(u,-2,4,0),kadd(KRANC_GFOFFSET3D(u,2,-4,0),kadd(KRANC_GFOFFSET3D(u,-4,2,0),KRANC_GFOFFSET3D(u,4,-2,0)))),kmadd(ToReal(504),kadd(KRANC_GFOFFSET3D(u,-2,-4,0),kadd(KRANC_GFOFFSET3D(u,2,4,0),kadd(KRANC_GFOFFSET3D(u,-4,-2,0),KRANC_GFOFFSET3D(u,4,2,0)))),kmadd(ToReal(96),kadd(KRANC_GFOFFSET3D(u,-3,4,0),kadd(KRANC_GFOFFSET3D(u,3,-4,0),kadd(KRANC_GFOFFSET3D(u,-4,3,0),KRANC_GFOFFSET3D(u,4,-3,0)))),kmadd(ToReal(-96),kadd(KRANC_GFOFFSET3D(u,-3,-4,0),kadd(KRANC_GFOFFSET3D(u,3,4,0),kadd(KRANC_GFOFFSET3D(u,-4,-3,0),KRANC_GFOFFSET3D(u,4,3,0)))),kmadd(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,-4,4,0),KRANC_GFOFFSET3D(u,4,-4,0)),kmul(ToReal(9),kadd(KRANC_GFOFFSET3D(u,-4,-4,0),KRANC_GFOFFSET3D(u,4,4,0)))))))))))))))))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth13(u) (kmul(p1o705600dxdz,kmadd(ToReal(-451584),kadd(KRANC_GFOFFSET3D(u,-1,0,1),KRANC_GFOFFSET3D(u,1,0,-1)),kmadd(ToReal(451584),kadd(KRANC_GFOFFSET3D(u,-1,0,-1),KRANC_GFOFFSET3D(u,1,0,1)),kmadd(ToReal(112896),kadd(KRANC_GFOFFSET3D(u,-1,0,2),kadd(KRANC_GFOFFSET3D(u,1,0,-2),kadd(KRANC_GFOFFSET3D(u,-2,0,1),KRANC_GFOFFSET3D(u,2,0,-1)))),kmadd(ToReal(-112896),kadd(KRANC_GFOFFSET3D(u,-1,0,-2),kadd(KRANC_GFOFFSET3D(u,1,0,2),kadd(KRANC_GFOFFSET3D(u,-2,0,-1),KRANC_GFOFFSET3D(u,2,0,1)))),kmadd(ToReal(-28224),kadd(KRANC_GFOFFSET3D(u,-2,0,2),KRANC_GFOFFSET3D(u,2,0,-2)),kmadd(ToReal(28224),kadd(KRANC_GFOFFSET3D(u,-2,0,-2),KRANC_GFOFFSET3D(u,2,0,2)),kmadd(ToReal(-21504),kadd(KRANC_GFOFFSET3D(u,-1,0,3),kadd(KRANC_GFOFFSET3D(u,1,0,-3),kadd(KRANC_GFOFFSET3D(u,-3,0,1),KRANC_GFOFFSET3D(u,3,0,-1)))),kmadd(ToReal(21504),kadd(KRANC_GFOFFSET3D(u,-1,0,-3),kadd(KRANC_GFOFFSET3D(u,1,0,3),kadd(KRANC_GFOFFSET3D(u,-3,0,-1),KRANC_GFOFFSET3D(u,3,0,1)))),kmadd(ToReal(5376),kadd(KRANC_GFOFFSET3D(u,-2,0,3),kadd(KRANC_GFOFFSET3D(u,2,0,-3),kadd(KRANC_GFOFFSET3D(u,-3,0,2),KRANC_GFOFFSET3D(u,3,0,-2)))),kmadd(ToReal(-5376),kadd(KRANC_GFOFFSET3D(u,-2,0,-3),kadd(KRANC_GFOFFSET3D(u,2,0,3),kadd(KRANC_GFOFFSET3D(u,-3,0,-2),KRANC_GFOFFSET3D(u,3,0,2)))),kmadd(ToReal(-1024),kadd(KRANC_GFOFFSET3D(u,-3,0,3),KRANC_GFOFFSET3D(u,3,0,-3)),kmadd(ToReal(1024),kadd(KRANC_GFOFFSET3D(u,-3,0,-3),KRANC_GFOFFSET3D(u,3,0,3)),kmadd(ToReal(2016),kadd(KRANC_GFOFFSET3D(u,-1,0,4),kadd(KRANC_GFOFFSET3D(u,1,0,-4),kadd(KRANC_GFOFFSET3D(u,-4,0,1),KRANC_GFOFFSET3D(u,4,0,-1)))),kmadd(ToReal(-2016),kadd(KRANC_GFOFFSET3D(u,-1,0,-4),kadd(KRANC_GFOFFSET3D(u,1,0,4),kadd(KRANC_GFOFFSET3D(u,-4,0,-1),KRANC_GFOFFSET3D(u,4,0,1)))),kmadd(ToReal(-504),kadd(KRANC_GFOFFSET3D(u,-2,0,4),kadd(KRANC_GFOFFSET3D(u,2,0,-4),kadd(KRANC_GFOFFSET3D(u,-4,0,2),KRANC_GFOFFSET3D(u,4,0,-2)))),kmadd(ToReal(504),kadd(KRANC_GFOFFSET3D(u,-2,0,-4),kadd(KRANC_GFOFFSET3D(u,2,0,4),kadd(KRANC_GFOFFSET3D(u,-4,0,-2),KRANC_GFOFFSET3D(u,4,0,2)))),kmadd(ToReal(96),kadd(KRANC_GFOFFSET3D(u,-3,0,4),kadd(KRANC_GFOFFSET3D(u,3,0,-4),kadd(KRANC_GFOFFSET3D(u,-4,0,3),KRANC_GFOFFSET3D(u,4,0,-3)))),kmadd(ToReal(-96),kadd(KRANC_GFOFFSET3D(u,-3,0,-4),kadd(KRANC_GFOFFSET3D(u,3,0,4),kadd(KRANC_GFOFFSET3D(u,-4,0,-3),KRANC_GFOFFSET3D(u,4,0,3)))),kmadd(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,-4,0,4),KRANC_GFOFFSET3D(u,4,0,-4)),kmul(ToReal(9),kadd(KRANC_GFOFFSET3D(u,-4,0,-4),KRANC_GFOFFSET3D(u,4,0,4))))))))))))))))))))))))
#else
#  define PDstandardNth13(u) (PDstandardNth13_impl(u,p1o705600dxdz,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth13_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth13_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardNth12_impl(u, p1o705600dxdz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth21(u) (kmul(p1o705600dxdy,kmadd(ToReal(-451584),kadd(KRANC_GFOFFSET3D(u,-1,1,0),KRANC_GFOFFSET3D(u,1,-1,0)),kmadd(ToReal(451584),kadd(KRANC_GFOFFSET3D(u,-1,-1,0),KRANC_GFOFFSET3D(u,1,1,0)),kmadd(ToReal(112896),kadd(KRANC_GFOFFSET3D(u,-1,2,0),kadd(KRANC_GFOFFSET3D(u,1,-2,0),kadd(KRANC_GFOFFSET3D(u,-2,1,0),KRANC_GFOFFSET3D(u,2,-1,0)))),kmadd(ToReal(-112896),kadd(KRANC_GFOFFSET3D(u,-1,-2,0),kadd(KRANC_GFOFFSET3D(u,1,2,0),kadd(KRANC_GFOFFSET3D(u,-2,-1,0),KRANC_GFOFFSET3D(u,2,1,0)))),kmadd(ToReal(-28224),kadd(KRANC_GFOFFSET3D(u,-2,2,0),KRANC_GFOFFSET3D(u,2,-2,0)),kmadd(ToReal(28224),kadd(KRANC_GFOFFSET3D(u,-2,-2,0),KRANC_GFOFFSET3D(u,2,2,0)),kmadd(ToReal(-21504),kadd(KRANC_GFOFFSET3D(u,-1,3,0),kadd(KRANC_GFOFFSET3D(u,1,-3,0),kadd(KRANC_GFOFFSET3D(u,-3,1,0),KRANC_GFOFFSET3D(u,3,-1,0)))),kmadd(ToReal(21504),kadd(KRANC_GFOFFSET3D(u,-1,-3,0),kadd(KRANC_GFOFFSET3D(u,1,3,0),kadd(KRANC_GFOFFSET3D(u,-3,-1,0),KRANC_GFOFFSET3D(u,3,1,0)))),kmadd(ToReal(5376),kadd(KRANC_GFOFFSET3D(u,-2,3,0),kadd(KRANC_GFOFFSET3D(u,2,-3,0),kadd(KRANC_GFOFFSET3D(u,-3,2,0),KRANC_GFOFFSET3D(u,3,-2,0)))),kmadd(ToReal(-5376),kadd(KRANC_GFOFFSET3D(u,-2,-3,0),kadd(KRANC_GFOFFSET3D(u,2,3,0),kadd(KRANC_GFOFFSET3D(u,-3,-2,0),KRANC_GFOFFSET3D(u,3,2,0)))),kmadd(ToReal(-1024),kadd(KRANC_GFOFFSET3D(u,-3,3,0),KRANC_GFOFFSET3D(u,3,-3,0)),kmadd(ToReal(1024),kadd(KRANC_GFOFFSET3D(u,-3,-3,0),KRANC_GFOFFSET3D(u,3,3,0)),kmadd(ToReal(2016),kadd(KRANC_GFOFFSET3D(u,-1,4,0),kadd(KRANC_GFOFFSET3D(u,1,-4,0),kadd(KRANC_GFOFFSET3D(u,-4,1,0),KRANC_GFOFFSET3D(u,4,-1,0)))),kmadd(ToReal(-2016),kadd(KRANC_GFOFFSET3D(u,-1,-4,0),kadd(KRANC_GFOFFSET3D(u,1,4,0),kadd(KRANC_GFOFFSET3D(u,-4,-1,0),KRANC_GFOFFSET3D(u,4,1,0)))),kmadd(ToReal(-504),kadd(KRANC_GFOFFSET3D(u,-2,4,0),kadd(KRANC_GFOFFSET3D(u,2,-4,0),kadd(KRANC_GFOFFSET3D(u,-4,2,0),KRANC_GFOFFSET3D(u,4,-2,0)))),kmadd(ToReal(504),kadd(KRANC_GFOFFSET3D(u,-2,-4,0),kadd(KRANC_GFOFFSET3D(u,2,4,0),kadd(KRANC_GFOFFSET3D(u,-4,-2,0),KRANC_GFOFFSET3D(u,4,2,0)))),kmadd(ToReal(96),kadd(KRANC_GFOFFSET3D(u,-3,4,0),kadd(KRANC_GFOFFSET3D(u,3,-4,0),kadd(KRANC_GFOFFSET3D(u,-4,3,0),KRANC_GFOFFSET3D(u,4,-3,0)))),kmadd(ToReal(-96),kadd(KRANC_GFOFFSET3D(u,-3,-4,0),kadd(KRANC_GFOFFSET3D(u,3,4,0),kadd(KRANC_GFOFFSET3D(u,-4,-3,0),KRANC_GFOFFSET3D(u,4,3,0)))),kmadd(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,-4,4,0),KRANC_GFOFFSET3D(u,4,-4,0)),kmul(ToReal(9),kadd(KRANC_GFOFFSET3D(u,-4,-4,0),KRANC_GFOFFSET3D(u,4,4,0))))))))))))))))))))))))
#else
#  define PDstandardNth21(u) (PDstandardNth21_impl(u,p1o705600dxdy,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth21_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth21_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardNth12_impl(u, p1o705600dxdy, cdj, cdk);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth23(u) (kmul(p1o705600dydz,kmadd(ToReal(-451584),kadd(KRANC_GFOFFSET3D(u,0,-1,1),KRANC_GFOFFSET3D(u,0,1,-1)),kmadd(ToReal(451584),kadd(KRANC_GFOFFSET3D(u,0,-1,-1),KRANC_GFOFFSET3D(u,0,1,1)),kmadd(ToReal(112896),kadd(KRANC_GFOFFSET3D(u,0,-1,2),kadd(KRANC_GFOFFSET3D(u,0,1,-2),kadd(KRANC_GFOFFSET3D(u,0,-2,1),KRANC_GFOFFSET3D(u,0,2,-1)))),kmadd(ToReal(-112896),kadd(KRANC_GFOFFSET3D(u,0,-1,-2),kadd(KRANC_GFOFFSET3D(u,0,1,2),kadd(KRANC_GFOFFSET3D(u,0,-2,-1),KRANC_GFOFFSET3D(u,0,2,1)))),kmadd(ToReal(-28224),kadd(KRANC_GFOFFSET3D(u,0,-2,2),KRANC_GFOFFSET3D(u,0,2,-2)),kmadd(ToReal(28224),kadd(KRANC_GFOFFSET3D(u,0,-2,-2),KRANC_GFOFFSET3D(u,0,2,2)),kmadd(ToReal(-21504),kadd(KRANC_GFOFFSET3D(u,0,-1,3),kadd(KRANC_GFOFFSET3D(u,0,1,-3),kadd(KRANC_GFOFFSET3D(u,0,-3,1),KRANC_GFOFFSET3D(u,0,3,-1)))),kmadd(ToReal(21504),kadd(KRANC_GFOFFSET3D(u,0,-1,-3),kadd(KRANC_GFOFFSET3D(u,0,1,3),kadd(KRANC_GFOFFSET3D(u,0,-3,-1),KRANC_GFOFFSET3D(u,0,3,1)))),kmadd(ToReal(5376),kadd(KRANC_GFOFFSET3D(u,0,-2,3),kadd(KRANC_GFOFFSET3D(u,0,2,-3),kadd(KRANC_GFOFFSET3D(u,0,-3,2),KRANC_GFOFFSET3D(u,0,3,-2)))),kmadd(ToReal(-5376),kadd(KRANC_GFOFFSET3D(u,0,-2,-3),kadd(KRANC_GFOFFSET3D(u,0,2,3),kadd(KRANC_GFOFFSET3D(u,0,-3,-2),KRANC_GFOFFSET3D(u,0,3,2)))),kmadd(ToReal(-1024),kadd(KRANC_GFOFFSET3D(u,0,-3,3),KRANC_GFOFFSET3D(u,0,3,-3)),kmadd(ToReal(1024),kadd(KRANC_GFOFFSET3D(u,0,-3,-3),KRANC_GFOFFSET3D(u,0,3,3)),kmadd(ToReal(2016),kadd(KRANC_GFOFFSET3D(u,0,-1,4),kadd(KRANC_GFOFFSET3D(u,0,1,-4),kadd(KRANC_GFOFFSET3D(u,0,-4,1),KRANC_GFOFFSET3D(u,0,4,-1)))),kmadd(ToReal(-2016),kadd(KRANC_GFOFFSET3D(u,0,-1,-4),kadd(KRANC_GFOFFSET3D(u,0,1,4),kadd(KRANC_GFOFFSET3D(u,0,-4,-1),KRANC_GFOFFSET3D(u,0,4,1)))),kmadd(ToReal(-504),kadd(KRANC_GFOFFSET3D(u,0,-2,4),kadd(KRANC_GFOFFSET3D(u,0,2,-4),kadd(KRANC_GFOFFSET3D(u,0,-4,2),KRANC_GFOFFSET3D(u,0,4,-2)))),kmadd(ToReal(504),kadd(KRANC_GFOFFSET3D(u,0,-2,-4),kadd(KRANC_GFOFFSET3D(u,0,2,4),kadd(KRANC_GFOFFSET3D(u,0,-4,-2),KRANC_GFOFFSET3D(u,0,4,2)))),kmadd(ToReal(96),kadd(KRANC_GFOFFSET3D(u,0,-3,4),kadd(KRANC_GFOFFSET3D(u,0,3,-4),kadd(KRANC_GFOFFSET3D(u,0,-4,3),KRANC_GFOFFSET3D(u,0,4,-3)))),kmadd(ToReal(-96),kadd(KRANC_GFOFFSET3D(u,0,-3,-4),kadd(KRANC_GFOFFSET3D(u,0,3,4),kadd(KRANC_GFOFFSET3D(u,0,-4,-3),KRANC_GFOFFSET3D(u,0,4,3)))),kmadd(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,0,-4,4),KRANC_GFOFFSET3D(u,0,4,-4)),kmul(ToReal(9),kadd(KRANC_GFOFFSET3D(u,0,-4,-4),KRANC_GFOFFSET3D(u,0,4,4))))))))))))))))))))))))
#else
#  define PDstandardNth23(u) (PDstandardNth23_impl(u,p1o705600dydz,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth23_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dydz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth23_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dydz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o705600dydz,kmadd(ToReal(-451584),kadd(KRANC_GFOFFSET3D(u,0,-1,1),KRANC_GFOFFSET3D(u,0,1,-1)),kmadd(ToReal(451584),kadd(KRANC_GFOFFSET3D(u,0,-1,-1),KRANC_GFOFFSET3D(u,0,1,1)),kmadd(ToReal(112896),kadd(KRANC_GFOFFSET3D(u,0,-1,2),kadd(KRANC_GFOFFSET3D(u,0,1,-2),kadd(KRANC_GFOFFSET3D(u,0,-2,1),KRANC_GFOFFSET3D(u,0,2,-1)))),kmadd(ToReal(-112896),kadd(KRANC_GFOFFSET3D(u,0,-1,-2),kadd(KRANC_GFOFFSET3D(u,0,1,2),kadd(KRANC_GFOFFSET3D(u,0,-2,-1),KRANC_GFOFFSET3D(u,0,2,1)))),kmadd(ToReal(-28224),kadd(KRANC_GFOFFSET3D(u,0,-2,2),KRANC_GFOFFSET3D(u,0,2,-2)),kmadd(ToReal(28224),kadd(KRANC_GFOFFSET3D(u,0,-2,-2),KRANC_GFOFFSET3D(u,0,2,2)),kmadd(ToReal(-21504),kadd(KRANC_GFOFFSET3D(u,0,-1,3),kadd(KRANC_GFOFFSET3D(u,0,1,-3),kadd(KRANC_GFOFFSET3D(u,0,-3,1),KRANC_GFOFFSET3D(u,0,3,-1)))),kmadd(ToReal(21504),kadd(KRANC_GFOFFSET3D(u,0,-1,-3),kadd(KRANC_GFOFFSET3D(u,0,1,3),kadd(KRANC_GFOFFSET3D(u,0,-3,-1),KRANC_GFOFFSET3D(u,0,3,1)))),kmadd(ToReal(5376),kadd(KRANC_GFOFFSET3D(u,0,-2,3),kadd(KRANC_GFOFFSET3D(u,0,2,-3),kadd(KRANC_GFOFFSET3D(u,0,-3,2),KRANC_GFOFFSET3D(u,0,3,-2)))),kmadd(ToReal(-5376),kadd(KRANC_GFOFFSET3D(u,0,-2,-3),kadd(KRANC_GFOFFSET3D(u,0,2,3),kadd(KRANC_GFOFFSET3D(u,0,-3,-2),KRANC_GFOFFSET3D(u,0,3,2)))),kmadd(ToReal(-1024),kadd(KRANC_GFOFFSET3D(u,0,-3,3),KRANC_GFOFFSET3D(u,0,3,-3)),kmadd(ToReal(1024),kadd(KRANC_GFOFFSET3D(u,0,-3,-3),KRANC_GFOFFSET3D(u,0,3,3)),kmadd(ToReal(2016),kadd(KRANC_GFOFFSET3D(u,0,-1,4),kadd(KRANC_GFOFFSET3D(u,0,1,-4),kadd(KRANC_GFOFFSET3D(u,0,-4,1),KRANC_GFOFFSET3D(u,0,4,-1)))),kmadd(ToReal(-2016),kadd(KRANC_GFOFFSET3D(u,0,-1,-4),kadd(KRANC_GFOFFSET3D(u,0,1,4),kadd(KRANC_GFOFFSET3D(u,0,-4,-1),KRANC_GFOFFSET3D(u,0,4,1)))),kmadd(ToReal(-504),kadd(KRANC_GFOFFSET3D(u,0,-2,4),kadd(KRANC_GFOFFSET3D(u,0,2,-4),kadd(KRANC_GFOFFSET3D(u,0,-4,2),KRANC_GFOFFSET3D(u,0,4,-2)))),kmadd(ToReal(504),kadd(KRANC_GFOFFSET3D(u,0,-2,-4),kadd(KRANC_GFOFFSET3D(u,0,2,4),kadd(KRANC_GFOFFSET3D(u,0,-4,-2),KRANC_GFOFFSET3D(u,0,4,2)))),kmadd(ToReal(96),kadd(KRANC_GFOFFSET3D(u,0,-3,4),kadd(KRANC_GFOFFSET3D(u,0,3,-4),kadd(KRANC_GFOFFSET3D(u,0,-4,3),KRANC_GFOFFSET3D(u,0,4,-3)))),kmadd(ToReal(-96),kadd(KRANC_GFOFFSET3D(u,0,-3,-4),kadd(KRANC_GFOFFSET3D(u,0,3,4),kadd(KRANC_GFOFFSET3D(u,0,-4,-3),KRANC_GFOFFSET3D(u,0,4,3)))),kmadd(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,0,-4,4),KRANC_GFOFFSET3D(u,0,4,-4)),kmul(ToReal(9),kadd(KRANC_GFOFFSET3D(u,0,-4,-4),KRANC_GFOFFSET3D(u,0,4,4)))))))))))))))))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth31(u) (kmul(p1o705600dxdz,kmadd(ToReal(-451584),kadd(KRANC_GFOFFSET3D(u,-1,0,1),KRANC_GFOFFSET3D(u,1,0,-1)),kmadd(ToReal(451584),kadd(KRANC_GFOFFSET3D(u,-1,0,-1),KRANC_GFOFFSET3D(u,1,0,1)),kmadd(ToReal(112896),kadd(KRANC_GFOFFSET3D(u,-1,0,2),kadd(KRANC_GFOFFSET3D(u,1,0,-2),kadd(KRANC_GFOFFSET3D(u,-2,0,1),KRANC_GFOFFSET3D(u,2,0,-1)))),kmadd(ToReal(-112896),kadd(KRANC_GFOFFSET3D(u,-1,0,-2),kadd(KRANC_GFOFFSET3D(u,1,0,2),kadd(KRANC_GFOFFSET3D(u,-2,0,-1),KRANC_GFOFFSET3D(u,2,0,1)))),kmadd(ToReal(-28224),kadd(KRANC_GFOFFSET3D(u,-2,0,2),KRANC_GFOFFSET3D(u,2,0,-2)),kmadd(ToReal(28224),kadd(KRANC_GFOFFSET3D(u,-2,0,-2),KRANC_GFOFFSET3D(u,2,0,2)),kmadd(ToReal(-21504),kadd(KRANC_GFOFFSET3D(u,-1,0,3),kadd(KRANC_GFOFFSET3D(u,1,0,-3),kadd(KRANC_GFOFFSET3D(u,-3,0,1),KRANC_GFOFFSET3D(u,3,0,-1)))),kmadd(ToReal(21504),kadd(KRANC_GFOFFSET3D(u,-1,0,-3),kadd(KRANC_GFOFFSET3D(u,1,0,3),kadd(KRANC_GFOFFSET3D(u,-3,0,-1),KRANC_GFOFFSET3D(u,3,0,1)))),kmadd(ToReal(5376),kadd(KRANC_GFOFFSET3D(u,-2,0,3),kadd(KRANC_GFOFFSET3D(u,2,0,-3),kadd(KRANC_GFOFFSET3D(u,-3,0,2),KRANC_GFOFFSET3D(u,3,0,-2)))),kmadd(ToReal(-5376),kadd(KRANC_GFOFFSET3D(u,-2,0,-3),kadd(KRANC_GFOFFSET3D(u,2,0,3),kadd(KRANC_GFOFFSET3D(u,-3,0,-2),KRANC_GFOFFSET3D(u,3,0,2)))),kmadd(ToReal(-1024),kadd(KRANC_GFOFFSET3D(u,-3,0,3),KRANC_GFOFFSET3D(u,3,0,-3)),kmadd(ToReal(1024),kadd(KRANC_GFOFFSET3D(u,-3,0,-3),KRANC_GFOFFSET3D(u,3,0,3)),kmadd(ToReal(2016),kadd(KRANC_GFOFFSET3D(u,-1,0,4),kadd(KRANC_GFOFFSET3D(u,1,0,-4),kadd(KRANC_GFOFFSET3D(u,-4,0,1),KRANC_GFOFFSET3D(u,4,0,-1)))),kmadd(ToReal(-2016),kadd(KRANC_GFOFFSET3D(u,-1,0,-4),kadd(KRANC_GFOFFSET3D(u,1,0,4),kadd(KRANC_GFOFFSET3D(u,-4,0,-1),KRANC_GFOFFSET3D(u,4,0,1)))),kmadd(ToReal(-504),kadd(KRANC_GFOFFSET3D(u,-2,0,4),kadd(KRANC_GFOFFSET3D(u,2,0,-4),kadd(KRANC_GFOFFSET3D(u,-4,0,2),KRANC_GFOFFSET3D(u,4,0,-2)))),kmadd(ToReal(504),kadd(KRANC_GFOFFSET3D(u,-2,0,-4),kadd(KRANC_GFOFFSET3D(u,2,0,4),kadd(KRANC_GFOFFSET3D(u,-4,0,-2),KRANC_GFOFFSET3D(u,4,0,2)))),kmadd(ToReal(96),kadd(KRANC_GFOFFSET3D(u,-3,0,4),kadd(KRANC_GFOFFSET3D(u,3,0,-4),kadd(KRANC_GFOFFSET3D(u,-4,0,3),KRANC_GFOFFSET3D(u,4,0,-3)))),kmadd(ToReal(-96),kadd(KRANC_GFOFFSET3D(u,-3,0,-4),kadd(KRANC_GFOFFSET3D(u,3,0,4),kadd(KRANC_GFOFFSET3D(u,-4,0,-3),KRANC_GFOFFSET3D(u,4,0,3)))),kmadd(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,-4,0,4),KRANC_GFOFFSET3D(u,4,0,-4)),kmul(ToReal(9),kadd(KRANC_GFOFFSET3D(u,-4,0,-4),KRANC_GFOFFSET3D(u,4,0,4))))))))))))))))))))))))
#else
#  define PDstandardNth31(u) (PDstandardNth31_impl(u,p1o705600dxdz,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth31_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth31_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardNth12_impl(u, p1o705600dxdz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardNth32(u) (kmul(p1o705600dydz,kmadd(ToReal(-451584),kadd(KRANC_GFOFFSET3D(u,0,-1,1),KRANC_GFOFFSET3D(u,0,1,-1)),kmadd(ToReal(451584),kadd(KRANC_GFOFFSET3D(u,0,-1,-1),KRANC_GFOFFSET3D(u,0,1,1)),kmadd(ToReal(112896),kadd(KRANC_GFOFFSET3D(u,0,-1,2),kadd(KRANC_GFOFFSET3D(u,0,1,-2),kadd(KRANC_GFOFFSET3D(u,0,-2,1),KRANC_GFOFFSET3D(u,0,2,-1)))),kmadd(ToReal(-112896),kadd(KRANC_GFOFFSET3D(u,0,-1,-2),kadd(KRANC_GFOFFSET3D(u,0,1,2),kadd(KRANC_GFOFFSET3D(u,0,-2,-1),KRANC_GFOFFSET3D(u,0,2,1)))),kmadd(ToReal(-28224),kadd(KRANC_GFOFFSET3D(u,0,-2,2),KRANC_GFOFFSET3D(u,0,2,-2)),kmadd(ToReal(28224),kadd(KRANC_GFOFFSET3D(u,0,-2,-2),KRANC_GFOFFSET3D(u,0,2,2)),kmadd(ToReal(-21504),kadd(KRANC_GFOFFSET3D(u,0,-1,3),kadd(KRANC_GFOFFSET3D(u,0,1,-3),kadd(KRANC_GFOFFSET3D(u,0,-3,1),KRANC_GFOFFSET3D(u,0,3,-1)))),kmadd(ToReal(21504),kadd(KRANC_GFOFFSET3D(u,0,-1,-3),kadd(KRANC_GFOFFSET3D(u,0,1,3),kadd(KRANC_GFOFFSET3D(u,0,-3,-1),KRANC_GFOFFSET3D(u,0,3,1)))),kmadd(ToReal(5376),kadd(KRANC_GFOFFSET3D(u,0,-2,3),kadd(KRANC_GFOFFSET3D(u,0,2,-3),kadd(KRANC_GFOFFSET3D(u,0,-3,2),KRANC_GFOFFSET3D(u,0,3,-2)))),kmadd(ToReal(-5376),kadd(KRANC_GFOFFSET3D(u,0,-2,-3),kadd(KRANC_GFOFFSET3D(u,0,2,3),kadd(KRANC_GFOFFSET3D(u,0,-3,-2),KRANC_GFOFFSET3D(u,0,3,2)))),kmadd(ToReal(-1024),kadd(KRANC_GFOFFSET3D(u,0,-3,3),KRANC_GFOFFSET3D(u,0,3,-3)),kmadd(ToReal(1024),kadd(KRANC_GFOFFSET3D(u,0,-3,-3),KRANC_GFOFFSET3D(u,0,3,3)),kmadd(ToReal(2016),kadd(KRANC_GFOFFSET3D(u,0,-1,4),kadd(KRANC_GFOFFSET3D(u,0,1,-4),kadd(KRANC_GFOFFSET3D(u,0,-4,1),KRANC_GFOFFSET3D(u,0,4,-1)))),kmadd(ToReal(-2016),kadd(KRANC_GFOFFSET3D(u,0,-1,-4),kadd(KRANC_GFOFFSET3D(u,0,1,4),kadd(KRANC_GFOFFSET3D(u,0,-4,-1),KRANC_GFOFFSET3D(u,0,4,1)))),kmadd(ToReal(-504),kadd(KRANC_GFOFFSET3D(u,0,-2,4),kadd(KRANC_GFOFFSET3D(u,0,2,-4),kadd(KRANC_GFOFFSET3D(u,0,-4,2),KRANC_GFOFFSET3D(u,0,4,-2)))),kmadd(ToReal(504),kadd(KRANC_GFOFFSET3D(u,0,-2,-4),kadd(KRANC_GFOFFSET3D(u,0,2,4),kadd(KRANC_GFOFFSET3D(u,0,-4,-2),KRANC_GFOFFSET3D(u,0,4,2)))),kmadd(ToReal(96),kadd(KRANC_GFOFFSET3D(u,0,-3,4),kadd(KRANC_GFOFFSET3D(u,0,3,-4),kadd(KRANC_GFOFFSET3D(u,0,-4,3),KRANC_GFOFFSET3D(u,0,4,-3)))),kmadd(ToReal(-96),kadd(KRANC_GFOFFSET3D(u,0,-3,-4),kadd(KRANC_GFOFFSET3D(u,0,3,4),kadd(KRANC_GFOFFSET3D(u,0,-4,-3),KRANC_GFOFFSET3D(u,0,4,3)))),kmadd(ToReal(-9),kadd(KRANC_GFOFFSET3D(u,0,-4,4),KRANC_GFOFFSET3D(u,0,4,-4)),kmul(ToReal(9),kadd(KRANC_GFOFFSET3D(u,0,-4,-4),KRANC_GFOFFSET3D(u,0,4,4))))))))))))))))))))))))
#else
#  define PDstandardNth32(u) (PDstandardNth32_impl(u,p1o705600dydz,cdj,cdk))
static CCTK_REAL_VEC PDstandardNth32_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dydz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandardNth32_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o705600dydz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardNth23_impl(u, p1o705600dydz, cdj, cdk);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandard2nd1(u) (kmul(p1o2dx,ksub(KRANC_GFOFFSET3D(u,1,0,0),KRANC_GFOFFSET3D(u,-1,0,0))))
#else
#  define PDstandard2nd1(u) (PDstandard2nd1_impl(u,p1o2dx,cdj,cdk))
static CCTK_REAL_VEC PDstandard2nd1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandard2nd1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o2dx,ksub(KRANC_GFOFFSET3D(u,1,0,0),KRANC_GFOFFSET3D(u,-1,0,0)));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandard2nd2(u) (kmul(p1o2dy,ksub(KRANC_GFOFFSET3D(u,0,1,0),KRANC_GFOFFSET3D(u,0,-1,0))))
#else
#  define PDstandard2nd2(u) (PDstandard2nd2_impl(u,p1o2dy,cdj,cdk))
static CCTK_REAL_VEC PDstandard2nd2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandard2nd2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o2dy,ksub(KRANC_GFOFFSET3D(u,0,1,0),KRANC_GFOFFSET3D(u,0,-1,0)));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandard2nd3(u) (kmul(p1o2dz,ksub(KRANC_GFOFFSET3D(u,0,0,1),KRANC_GFOFFSET3D(u,0,0,-1))))
#else
#  define PDstandard2nd3(u) (PDstandard2nd3_impl(u,p1o2dz,cdj,cdk))
static CCTK_REAL_VEC PDstandard2nd3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDstandard2nd3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandard2nd2_impl(u, p1o2dz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDdissipationNth1(u) (kmul(p1o1024dx,kmadd(ToReal(-252),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(210),kadd(KRANC_GFOFFSET3D(u,-1,0,0),KRANC_GFOFFSET3D(u,1,0,0)),kmadd(ToReal(-120),kadd(KRANC_GFOFFSET3D(u,-2,0,0),KRANC_GFOFFSET3D(u,2,0,0)),kmadd(ToReal(45),kadd(KRANC_GFOFFSET3D(u,-3,0,0),KRANC_GFOFFSET3D(u,3,0,0)),kmadd(ToReal(-10),kadd(KRANC_GFOFFSET3D(u,-4,0,0),KRANC_GFOFFSET3D(u,4,0,0)),kadd(KRANC_GFOFFSET3D(u,-5,0,0),KRANC_GFOFFSET3D(u,5,0,0)))))))))
#else
#  define PDdissipationNth1(u) (PDdissipationNth1_impl(u,p1o1024dx,cdj,cdk))
static CCTK_REAL_VEC PDdissipationNth1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1024dx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDdissipationNth1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1024dx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o1024dx,kmadd(ToReal(-252),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(210),kadd(KRANC_GFOFFSET3D(u,-1,0,0),KRANC_GFOFFSET3D(u,1,0,0)),kmadd(ToReal(-120),kadd(KRANC_GFOFFSET3D(u,-2,0,0),KRANC_GFOFFSET3D(u,2,0,0)),kmadd(ToReal(45),kadd(KRANC_GFOFFSET3D(u,-3,0,0),KRANC_GFOFFSET3D(u,3,0,0)),kmadd(ToReal(-10),kadd(KRANC_GFOFFSET3D(u,-4,0,0),KRANC_GFOFFSET3D(u,4,0,0)),kadd(KRANC_GFOFFSET3D(u,-5,0,0),KRANC_GFOFFSET3D(u,5,0,0))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDdissipationNth2(u) (kmul(p1o1024dy,kmadd(ToReal(-252),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(210),kadd(KRANC_GFOFFSET3D(u,0,-1,0),KRANC_GFOFFSET3D(u,0,1,0)),kmadd(ToReal(-120),kadd(KRANC_GFOFFSET3D(u,0,-2,0),KRANC_GFOFFSET3D(u,0,2,0)),kmadd(ToReal(45),kadd(KRANC_GFOFFSET3D(u,0,-3,0),KRANC_GFOFFSET3D(u,0,3,0)),kmadd(ToReal(-10),kadd(KRANC_GFOFFSET3D(u,0,-4,0),KRANC_GFOFFSET3D(u,0,4,0)),kadd(KRANC_GFOFFSET3D(u,0,-5,0),KRANC_GFOFFSET3D(u,0,5,0)))))))))
#else
#  define PDdissipationNth2(u) (PDdissipationNth2_impl(u,p1o1024dy,cdj,cdk))
static CCTK_REAL_VEC PDdissipationNth2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1024dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDdissipationNth2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1024dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o1024dy,kmadd(ToReal(-252),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(210),kadd(KRANC_GFOFFSET3D(u,0,-1,0),KRANC_GFOFFSET3D(u,0,1,0)),kmadd(ToReal(-120),kadd(KRANC_GFOFFSET3D(u,0,-2,0),KRANC_GFOFFSET3D(u,0,2,0)),kmadd(ToReal(45),kadd(KRANC_GFOFFSET3D(u,0,-3,0),KRANC_GFOFFSET3D(u,0,3,0)),kmadd(ToReal(-10),kadd(KRANC_GFOFFSET3D(u,0,-4,0),KRANC_GFOFFSET3D(u,0,4,0)),kadd(KRANC_GFOFFSET3D(u,0,-5,0),KRANC_GFOFFSET3D(u,0,5,0))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDdissipationNth3(u) (kmul(p1o1024dz,kmadd(ToReal(-252),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(210),kadd(KRANC_GFOFFSET3D(u,0,0,-1),KRANC_GFOFFSET3D(u,0,0,1)),kmadd(ToReal(-120),kadd(KRANC_GFOFFSET3D(u,0,0,-2),KRANC_GFOFFSET3D(u,0,0,2)),kmadd(ToReal(45),kadd(KRANC_GFOFFSET3D(u,0,0,-3),KRANC_GFOFFSET3D(u,0,0,3)),kmadd(ToReal(-10),kadd(KRANC_GFOFFSET3D(u,0,0,-4),KRANC_GFOFFSET3D(u,0,0,4)),kadd(KRANC_GFOFFSET3D(u,0,0,-5),KRANC_GFOFFSET3D(u,0,0,5)))))))))
#else
#  define PDdissipationNth3(u) (PDdissipationNth3_impl(u,p1o1024dz,cdj,cdk))
static CCTK_REAL_VEC PDdissipationNth3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1024dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDdissipationNth3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1024dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDdissipationNth2_impl(u, p1o1024dz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDupwindNth1(u) (kmul(kmadd(ToReal(-378),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-5),kmadd(ToReal(-210),KRANC_GFOFFSET3D(u,1,0,0),kmadd(ToReal(-12),KRANC_GFOFFSET3D(u,-2,0,0),kmadd(ToReal(84),kadd(KRANC_GFOFFSET3D(u,-1,0,0),KRANC_GFOFFSET3D(u,2,0,0)),kadd(KRANC_GFOFFSET3D(u,-3,0,0),kmadd(ToReal(-28),KRANC_GFOFFSET3D(u,3,0,0),kmul(KRANC_GFOFFSET3D(u,4,0,0),ToReal(6))))))),kmul(KRANC_GFOFFSET3D(u,5,0,0),ToReal(3)))),kmul(p1o840dx,dir1)))
#else
#  define PDupwindNth1(u) (PDupwindNth1_impl(u,p1o840dx,cdj,cdk))
static CCTK_REAL_VEC PDupwindNth1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDupwindNth1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{ assert(0); return ToReal(1e30); /* ERROR */ }
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDupwindNthAnti1(u) (kmul(p1o1680dx,kmadd(ToReal(-1470),KRANC_GFOFFSET3D(u,-1,0,0),kmadd(ToReal(1470),KRANC_GFOFFSET3D(u,1,0,0),kmadd(ToReal(480),KRANC_GFOFFSET3D(u,-2,0,0),kmadd(ToReal(-480),KRANC_GFOFFSET3D(u,2,0,0),kmadd(ToReal(-145),KRANC_GFOFFSET3D(u,-3,0,0),kmadd(ToReal(145),KRANC_GFOFFSET3D(u,3,0,0),kmadd(ToReal(30),KRANC_GFOFFSET3D(u,-4,0,0),kmadd(ToReal(-30),KRANC_GFOFFSET3D(u,4,0,0),kmadd(ToReal(-3),KRANC_GFOFFSET3D(u,-5,0,0),kmul(ToReal(3),KRANC_GFOFFSET3D(u,5,0,0)))))))))))))
#else
#  define PDupwindNthAnti1(u) (PDupwindNthAnti1_impl(u,p1o1680dx,cdj,cdk))
static CCTK_REAL_VEC PDupwindNthAnti1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1680dx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDupwindNthAnti1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1680dx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o1680dx,kmadd(ToReal(-1470),KRANC_GFOFFSET3D(u,-1,0,0),kmadd(ToReal(1470),KRANC_GFOFFSET3D(u,1,0,0),kmadd(ToReal(480),KRANC_GFOFFSET3D(u,-2,0,0),kmadd(ToReal(-480),KRANC_GFOFFSET3D(u,2,0,0),kmadd(ToReal(-145),KRANC_GFOFFSET3D(u,-3,0,0),kmadd(ToReal(145),KRANC_GFOFFSET3D(u,3,0,0),kmadd(ToReal(30),KRANC_GFOFFSET3D(u,-4,0,0),kmadd(ToReal(-30),KRANC_GFOFFSET3D(u,4,0,0),kmadd(ToReal(-3),KRANC_GFOFFSET3D(u,-5,0,0),kmul(ToReal(3),KRANC_GFOFFSET3D(u,5,0,0))))))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDupwindNthSymm1(u) (kmul(p1o560dx,kmadd(ToReal(-252),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(210),kadd(KRANC_GFOFFSET3D(u,-1,0,0),KRANC_GFOFFSET3D(u,1,0,0)),kmadd(ToReal(-120),kadd(KRANC_GFOFFSET3D(u,-2,0,0),KRANC_GFOFFSET3D(u,2,0,0)),kmadd(ToReal(45),kadd(KRANC_GFOFFSET3D(u,-3,0,0),KRANC_GFOFFSET3D(u,3,0,0)),kmadd(ToReal(-10),kadd(KRANC_GFOFFSET3D(u,-4,0,0),KRANC_GFOFFSET3D(u,4,0,0)),kadd(KRANC_GFOFFSET3D(u,-5,0,0),KRANC_GFOFFSET3D(u,5,0,0)))))))))
#else
#  define PDupwindNthSymm1(u) (PDupwindNthSymm1_impl(u,p1o560dx,cdj,cdk))
static CCTK_REAL_VEC PDupwindNthSymm1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o560dx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDupwindNthSymm1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o560dx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o560dx,kmadd(ToReal(-252),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(210),kadd(KRANC_GFOFFSET3D(u,-1,0,0),KRANC_GFOFFSET3D(u,1,0,0)),kmadd(ToReal(-120),kadd(KRANC_GFOFFSET3D(u,-2,0,0),KRANC_GFOFFSET3D(u,2,0,0)),kmadd(ToReal(45),kadd(KRANC_GFOFFSET3D(u,-3,0,0),KRANC_GFOFFSET3D(u,3,0,0)),kmadd(ToReal(-10),kadd(KRANC_GFOFFSET3D(u,-4,0,0),KRANC_GFOFFSET3D(u,4,0,0)),kadd(KRANC_GFOFFSET3D(u,-5,0,0),KRANC_GFOFFSET3D(u,5,0,0))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDonesided1(u) (kmul(kmul(p1odx,dir1),ksub(KRANC_GFOFFSET3D(u,1,0,0),KRANC_GFOFFSET3D(u,0,0,0))))
#else
#  define PDonesided1(u) (PDonesided1_impl(u,p1odx,cdj,cdk))
static CCTK_REAL_VEC PDonesided1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDonesided1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{ assert(0); return ToReal(1e30); /* ERROR */ }
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDonesidedPlus2nd1(u) (kmul(pm1o2dx,kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-4),KRANC_GFOFFSET3D(u,1,0,0),KRANC_GFOFFSET3D(u,2,0,0)))))
#else
#  define PDonesidedPlus2nd1(u) (PDonesidedPlus2nd1_impl(u,pm1o2dx,cdj,cdk))
static CCTK_REAL_VEC PDonesidedPlus2nd1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC pm1o2dx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDonesidedPlus2nd1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC pm1o2dx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(pm1o2dx,kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-4),KRANC_GFOFFSET3D(u,1,0,0),KRANC_GFOFFSET3D(u,2,0,0))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDonesidedMinus2nd1(u) (kmul(p1o2dx,kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-4),KRANC_GFOFFSET3D(u,-1,0,0),KRANC_GFOFFSET3D(u,-2,0,0)))))
#else
#  define PDonesidedMinus2nd1(u) (PDonesidedMinus2nd1_impl(u,p1o2dx,cdj,cdk))
static CCTK_REAL_VEC PDonesidedMinus2nd1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDonesidedMinus2nd1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o2dx,kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-4),KRANC_GFOFFSET3D(u,-1,0,0),KRANC_GFOFFSET3D(u,-2,0,0))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDupwindNth2(u) (kmul(kmadd(ToReal(-378),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-5),kmadd(ToReal(-210),KRANC_GFOFFSET3D(u,0,1,0),kmadd(ToReal(-12),KRANC_GFOFFSET3D(u,0,-2,0),kmadd(ToReal(84),kadd(KRANC_GFOFFSET3D(u,0,-1,0),KRANC_GFOFFSET3D(u,0,2,0)),kadd(KRANC_GFOFFSET3D(u,0,-3,0),kmadd(ToReal(-28),KRANC_GFOFFSET3D(u,0,3,0),kmul(KRANC_GFOFFSET3D(u,0,4,0),ToReal(6))))))),kmul(KRANC_GFOFFSET3D(u,0,5,0),ToReal(3)))),kmul(p1o840dy,dir2)))
#else
#  define PDupwindNth2(u) (PDupwindNth2_impl(u,p1o840dy,cdj,cdk))
static CCTK_REAL_VEC PDupwindNth2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDupwindNth2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{ assert(0); return ToReal(1e30); /* ERROR */ }
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDupwindNthAnti2(u) (kmul(p1o1680dy,kmadd(ToReal(-1470),KRANC_GFOFFSET3D(u,0,-1,0),kmadd(ToReal(1470),KRANC_GFOFFSET3D(u,0,1,0),kmadd(ToReal(480),KRANC_GFOFFSET3D(u,0,-2,0),kmadd(ToReal(-480),KRANC_GFOFFSET3D(u,0,2,0),kmadd(ToReal(-145),KRANC_GFOFFSET3D(u,0,-3,0),kmadd(ToReal(145),KRANC_GFOFFSET3D(u,0,3,0),kmadd(ToReal(30),KRANC_GFOFFSET3D(u,0,-4,0),kmadd(ToReal(-30),KRANC_GFOFFSET3D(u,0,4,0),kmadd(ToReal(-3),KRANC_GFOFFSET3D(u,0,-5,0),kmul(ToReal(3),KRANC_GFOFFSET3D(u,0,5,0)))))))))))))
#else
#  define PDupwindNthAnti2(u) (PDupwindNthAnti2_impl(u,p1o1680dy,cdj,cdk))
static CCTK_REAL_VEC PDupwindNthAnti2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1680dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDupwindNthAnti2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1680dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o1680dy,kmadd(ToReal(-1470),KRANC_GFOFFSET3D(u,0,-1,0),kmadd(ToReal(1470),KRANC_GFOFFSET3D(u,0,1,0),kmadd(ToReal(480),KRANC_GFOFFSET3D(u,0,-2,0),kmadd(ToReal(-480),KRANC_GFOFFSET3D(u,0,2,0),kmadd(ToReal(-145),KRANC_GFOFFSET3D(u,0,-3,0),kmadd(ToReal(145),KRANC_GFOFFSET3D(u,0,3,0),kmadd(ToReal(30),KRANC_GFOFFSET3D(u,0,-4,0),kmadd(ToReal(-30),KRANC_GFOFFSET3D(u,0,4,0),kmadd(ToReal(-3),KRANC_GFOFFSET3D(u,0,-5,0),kmul(ToReal(3),KRANC_GFOFFSET3D(u,0,5,0))))))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDupwindNthSymm2(u) (kmul(p1o560dy,kmadd(ToReal(-252),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(210),kadd(KRANC_GFOFFSET3D(u,0,-1,0),KRANC_GFOFFSET3D(u,0,1,0)),kmadd(ToReal(-120),kadd(KRANC_GFOFFSET3D(u,0,-2,0),KRANC_GFOFFSET3D(u,0,2,0)),kmadd(ToReal(45),kadd(KRANC_GFOFFSET3D(u,0,-3,0),KRANC_GFOFFSET3D(u,0,3,0)),kmadd(ToReal(-10),kadd(KRANC_GFOFFSET3D(u,0,-4,0),KRANC_GFOFFSET3D(u,0,4,0)),kadd(KRANC_GFOFFSET3D(u,0,-5,0),KRANC_GFOFFSET3D(u,0,5,0)))))))))
#else
#  define PDupwindNthSymm2(u) (PDupwindNthSymm2_impl(u,p1o560dy,cdj,cdk))
static CCTK_REAL_VEC PDupwindNthSymm2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o560dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDupwindNthSymm2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o560dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o560dy,kmadd(ToReal(-252),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(210),kadd(KRANC_GFOFFSET3D(u,0,-1,0),KRANC_GFOFFSET3D(u,0,1,0)),kmadd(ToReal(-120),kadd(KRANC_GFOFFSET3D(u,0,-2,0),KRANC_GFOFFSET3D(u,0,2,0)),kmadd(ToReal(45),kadd(KRANC_GFOFFSET3D(u,0,-3,0),KRANC_GFOFFSET3D(u,0,3,0)),kmadd(ToReal(-10),kadd(KRANC_GFOFFSET3D(u,0,-4,0),KRANC_GFOFFSET3D(u,0,4,0)),kadd(KRANC_GFOFFSET3D(u,0,-5,0),KRANC_GFOFFSET3D(u,0,5,0))))))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDonesided2(u) (kmul(kmul(p1ody,dir2),ksub(KRANC_GFOFFSET3D(u,0,1,0),KRANC_GFOFFSET3D(u,0,0,0))))
#else
#  define PDonesided2(u) (PDonesided2_impl(u,p1ody,cdj,cdk))
static CCTK_REAL_VEC PDonesided2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1ody, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDonesided2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1ody, const ptrdiff_t cdj, const ptrdiff_t cdk)
{ assert(0); return ToReal(1e30); /* ERROR */ }
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDonesidedPlus2nd2(u) (kmul(pm1o2dy,kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-4),KRANC_GFOFFSET3D(u,0,1,0),KRANC_GFOFFSET3D(u,0,2,0)))))
#else
#  define PDonesidedPlus2nd2(u) (PDonesidedPlus2nd2_impl(u,pm1o2dy,cdj,cdk))
static CCTK_REAL_VEC PDonesidedPlus2nd2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC pm1o2dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDonesidedPlus2nd2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC pm1o2dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(pm1o2dy,kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-4),KRANC_GFOFFSET3D(u,0,1,0),KRANC_GFOFFSET3D(u,0,2,0))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDonesidedMinus2nd2(u) (kmul(p1o2dy,kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-4),KRANC_GFOFFSET3D(u,0,-1,0),KRANC_GFOFFSET3D(u,0,-2,0)))))
#else
#  define PDonesidedMinus2nd2(u) (PDonesidedMinus2nd2_impl(u,p1o2dy,cdj,cdk))
static CCTK_REAL_VEC PDonesidedMinus2nd2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDonesidedMinus2nd2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1o2dy,kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-4),KRANC_GFOFFSET3D(u,0,-1,0),KRANC_GFOFFSET3D(u,0,-2,0))));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDupwindNth3(u) (kmul(kmadd(ToReal(-378),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-5),kmadd(ToReal(-210),KRANC_GFOFFSET3D(u,0,0,1),kmadd(ToReal(-12),KRANC_GFOFFSET3D(u,0,0,-2),kmadd(ToReal(84),kadd(KRANC_GFOFFSET3D(u,0,0,-1),KRANC_GFOFFSET3D(u,0,0,2)),kadd(KRANC_GFOFFSET3D(u,0,0,-3),kmadd(ToReal(-28),KRANC_GFOFFSET3D(u,0,0,3),kmul(KRANC_GFOFFSET3D(u,0,0,4),ToReal(6))))))),kmul(KRANC_GFOFFSET3D(u,0,0,5),ToReal(3)))),kmul(p1o840dz,dir3)))
#else
#  define PDupwindNth3(u) (PDupwindNth3_impl(u,p1o840dz,cdj,cdk))
static CCTK_REAL_VEC PDupwindNth3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDupwindNth3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o840dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{ assert(0); return ToReal(1e30); /* ERROR */ }
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDupwindNthAnti3(u) (kmul(p1o1680dz,kmadd(ToReal(-1470),KRANC_GFOFFSET3D(u,0,0,-1),kmadd(ToReal(1470),KRANC_GFOFFSET3D(u,0,0,1),kmadd(ToReal(480),KRANC_GFOFFSET3D(u,0,0,-2),kmadd(ToReal(-480),KRANC_GFOFFSET3D(u,0,0,2),kmadd(ToReal(-145),KRANC_GFOFFSET3D(u,0,0,-3),kmadd(ToReal(145),KRANC_GFOFFSET3D(u,0,0,3),kmadd(ToReal(30),KRANC_GFOFFSET3D(u,0,0,-4),kmadd(ToReal(-30),KRANC_GFOFFSET3D(u,0,0,4),kmadd(ToReal(-3),KRANC_GFOFFSET3D(u,0,0,-5),kmul(ToReal(3),KRANC_GFOFFSET3D(u,0,0,5)))))))))))))
#else
#  define PDupwindNthAnti3(u) (PDupwindNthAnti3_impl(u,p1o1680dz,cdj,cdk))
static CCTK_REAL_VEC PDupwindNthAnti3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1680dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDupwindNthAnti3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o1680dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDupwindNthAnti2_impl(u, p1o1680dz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDupwindNthSymm3(u) (kmul(p1o560dz,kmadd(ToReal(-252),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(210),kadd(KRANC_GFOFFSET3D(u,0,0,-1),KRANC_GFOFFSET3D(u,0,0,1)),kmadd(ToReal(-120),kadd(KRANC_GFOFFSET3D(u,0,0,-2),KRANC_GFOFFSET3D(u,0,0,2)),kmadd(ToReal(45),kadd(KRANC_GFOFFSET3D(u,0,0,-3),KRANC_GFOFFSET3D(u,0,0,3)),kmadd(ToReal(-10),kadd(KRANC_GFOFFSET3D(u,0,0,-4),KRANC_GFOFFSET3D(u,0,0,4)),kadd(KRANC_GFOFFSET3D(u,0,0,-5),KRANC_GFOFFSET3D(u,0,0,5)))))))))
#else
#  define PDupwindNthSymm3(u) (PDupwindNthSymm3_impl(u,p1o560dz,cdj,cdk))
static CCTK_REAL_VEC PDupwindNthSymm3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o560dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDupwindNthSymm3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o560dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDupwindNthSymm2_impl(u, p1o560dz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDonesided3(u) (kmul(kmul(p1odz,dir3),ksub(KRANC_GFOFFSET3D(u,0,0,1),KRANC_GFOFFSET3D(u,0,0,0))))
#else
#  define PDonesided3(u) (PDonesided3_impl(u,p1odz,cdj,cdk))
static CCTK_REAL_VEC PDonesided3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDonesided3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{ assert(0); return ToReal(1e30); /* ERROR */ }
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDonesidedPlus2nd3(u) (kmul(pm1o2dz,kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-4),KRANC_GFOFFSET3D(u,0,0,1),KRANC_GFOFFSET3D(u,0,0,2)))))
#else
#  define PDonesidedPlus2nd3(u) (PDonesidedPlus2nd3_impl(u,pm1o2dz,cdj,cdk))
static CCTK_REAL_VEC PDonesidedPlus2nd3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC pm1o2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDonesidedPlus2nd3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC pm1o2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDonesidedPlus2nd2_impl(u, pm1o2dz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDonesidedMinus2nd3(u) (kmul(p1o2dz,kmadd(ToReal(3),KRANC_GFOFFSET3D(u,0,0,0),kmadd(ToReal(-4),KRANC_GFOFFSET3D(u,0,0,-1),KRANC_GFOFFSET3D(u,0,0,-2)))))
#else
#  define PDonesidedMinus2nd3(u) (PDonesidedMinus2nd3_impl(u,p1o2dz,cdj,cdk))
static CCTK_REAL_VEC PDonesidedMinus2nd3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDonesidedMinus2nd3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1o2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDonesidedMinus2nd2_impl(u, p1o2dz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDPlus1(u) (kmul(p1odx,ksub(KRANC_GFOFFSET3D(u,1,0,0),KRANC_GFOFFSET3D(u,0,0,0))))
#else
#  define PDPlus1(u) (PDPlus1_impl(u,p1odx,cdj,cdk))
static CCTK_REAL_VEC PDPlus1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDPlus1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1odx,ksub(KRANC_GFOFFSET3D(u,1,0,0),KRANC_GFOFFSET3D(u,0,0,0)));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDPlus2(u) (kmul(p1ody,ksub(KRANC_GFOFFSET3D(u,0,1,0),KRANC_GFOFFSET3D(u,0,0,0))))
#else
#  define PDPlus2(u) (PDPlus2_impl(u,p1ody,cdj,cdk))
static CCTK_REAL_VEC PDPlus2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1ody, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDPlus2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1ody, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1ody,ksub(KRANC_GFOFFSET3D(u,0,1,0),KRANC_GFOFFSET3D(u,0,0,0)));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDPlus3(u) (kmul(p1odz,ksub(KRANC_GFOFFSET3D(u,0,0,1),KRANC_GFOFFSET3D(u,0,0,0))))
#else
#  define PDPlus3(u) (PDPlus3_impl(u,p1odz,cdj,cdk))
static CCTK_REAL_VEC PDPlus3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDPlus3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDPlus2_impl(u, p1odz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDMinus1(u) (kmul(p1odx,ksub(KRANC_GFOFFSET3D(u,0,0,0),KRANC_GFOFFSET3D(u,-1,0,0))))
#else
#  define PDMinus1(u) (PDMinus1_impl(u,p1odx,cdj,cdk))
static CCTK_REAL_VEC PDMinus1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDMinus1_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1odx,ksub(KRANC_GFOFFSET3D(u,0,0,0),KRANC_GFOFFSET3D(u,-1,0,0)));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDMinus2(u) (kmul(p1ody,ksub(KRANC_GFOFFSET3D(u,0,0,0),KRANC_GFOFFSET3D(u,0,-1,0))))
#else
#  define PDMinus2(u) (PDMinus2_impl(u,p1ody,cdj,cdk))
static CCTK_REAL_VEC PDMinus2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1ody, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDMinus2_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1ody, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return kmul(p1ody,ksub(KRANC_GFOFFSET3D(u,0,0,0),KRANC_GFOFFSET3D(u,0,-1,0)));
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDMinus3(u) (kmul(p1odz,ksub(KRANC_GFOFFSET3D(u,0,0,0),KRANC_GFOFFSET3D(u,0,0,-1))))
#else
#  define PDMinus3(u) (PDMinus3_impl(u,p1odz,cdj,cdk))
static CCTK_REAL_VEC PDMinus3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL_VEC PDMinus3_impl(const CCTK_REAL* restrict const u, const CCTK_REAL_VEC p1odz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDMinus2_impl(u, p1odz, cdk, cdj);
}
#endif

