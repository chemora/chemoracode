/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"
#include "loopcontrol.h"
#include "vectors.h"

namespace ML_BSSN_Host {

extern "C" void ML_BSSN_Host_RHS_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_Host_RHS_calc_every != ML_BSSN_Host_RHS_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_curvrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_curvrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_dtlapserhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_dtlapserhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_dtshiftrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_dtshiftrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_Gammarhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_Gammarhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_lapserhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_lapserhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_log_confacrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_log_confacrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_metricrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_metricrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_shiftrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_shiftrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_Host::ML_trace_curvrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_Host::ML_trace_curvrhs.");
  return;
}

static void ML_BSSN_Host_RHS_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL_VEC t CCTK_ATTRIBUTE_UNUSED = ToReal(cctk_time);
  const CCTK_REAL_VEC cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_ORIGIN_SPACE(0));
  const CCTK_REAL_VEC cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_ORIGIN_SPACE(1));
  const CCTK_REAL_VEC cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_ORIGIN_SPACE(2));
  const CCTK_REAL_VEC dt CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_DELTA_TIME);
  const CCTK_REAL_VEC dx CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_DELTA_SPACE(0));
  const CCTK_REAL_VEC dy CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_DELTA_SPACE(1));
  const CCTK_REAL_VEC dz CCTK_ATTRIBUTE_UNUSED = 
    ToReal(CCTK_DELTA_SPACE(2));
  const CCTK_REAL_VEC dxi CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dx);
  const CCTK_REAL_VEC dyi CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dy);
  const CCTK_REAL_VEC dzi CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dz);
  const CCTK_REAL_VEC khalf CCTK_ATTRIBUTE_UNUSED = ToReal(0.5);
  const CCTK_REAL_VEC kthird CCTK_ATTRIBUTE_UNUSED = 
    ToReal(0.333333333333333333333333333333);
  const CCTK_REAL_VEC ktwothird CCTK_ATTRIBUTE_UNUSED = 
    ToReal(0.666666666666666666666666666667);
  const CCTK_REAL_VEC kfourthird CCTK_ATTRIBUTE_UNUSED = 
    ToReal(1.33333333333333333333333333333);
  const CCTK_REAL_VEC hdxi CCTK_ATTRIBUTE_UNUSED = 
    kmul(dxi,ToReal(0.5));
  const CCTK_REAL_VEC hdyi CCTK_ATTRIBUTE_UNUSED = 
    kmul(dyi,ToReal(0.5));
  const CCTK_REAL_VEC hdzi CCTK_ATTRIBUTE_UNUSED = 
    kmul(dzi,ToReal(0.5));
  /* Initialize predefined quantities */
  const CCTK_REAL_VEC p1o1024dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0009765625),dx);
  const CCTK_REAL_VEC p1o1024dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0009765625),dy);
  const CCTK_REAL_VEC p1o1024dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0009765625),dz);
  const CCTK_REAL_VEC p1o1680dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000595238095238095238095238095238),dx);
  const CCTK_REAL_VEC p1o1680dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000595238095238095238095238095238),dy);
  const CCTK_REAL_VEC p1o1680dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000595238095238095238095238095238),dz);
  const CCTK_REAL_VEC p1o2dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.5),dx);
  const CCTK_REAL_VEC p1o2dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.5),dy);
  const CCTK_REAL_VEC p1o2dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.5),dz);
  const CCTK_REAL_VEC p1o5040dx2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000198412698412698412698412698413),kmul(dx,dx));
  const CCTK_REAL_VEC p1o5040dy2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000198412698412698412698412698413),kmul(dy,dy));
  const CCTK_REAL_VEC p1o5040dz2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000198412698412698412698412698413),kmul(dz,dz));
  const CCTK_REAL_VEC p1o560dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00178571428571428571428571428571),dx);
  const CCTK_REAL_VEC p1o560dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00178571428571428571428571428571),dy);
  const CCTK_REAL_VEC p1o560dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00178571428571428571428571428571),dz);
  const CCTK_REAL_VEC p1o705600dxdy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1.41723356009070294784580498866e-6),kmul(dx,dy));
  const CCTK_REAL_VEC p1o705600dxdz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1.41723356009070294784580498866e-6),kmul(dx,dz));
  const CCTK_REAL_VEC p1o705600dydz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1.41723356009070294784580498866e-6),kmul(dy,dz));
  const CCTK_REAL_VEC p1o840dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00119047619047619047619047619048),dx);
  const CCTK_REAL_VEC p1o840dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00119047619047619047619047619048),dy);
  const CCTK_REAL_VEC p1o840dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00119047619047619047619047619048),dz);
  const CCTK_REAL_VEC p1odx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dx);
  const CCTK_REAL_VEC p1ody CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dy);
  const CCTK_REAL_VEC p1odz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),dz);
  const CCTK_REAL_VEC pm1o2dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(-0.5),dx);
  const CCTK_REAL_VEC pm1o2dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(-0.5),dy);
  const CCTK_REAL_VEC pm1o2dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(-0.5),dz);
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3STR(ML_BSSN_Host_RHS,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2],
    vecimin,vecimax, CCTK_REAL_VEC_SIZE)
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL_VEC AL CCTK_ATTRIBUTE_UNUSED = vec_load(A[index]);
    CCTK_REAL_VEC alphaL CCTK_ATTRIBUTE_UNUSED = vec_load(alpha[index]);
    CCTK_REAL_VEC At11L CCTK_ATTRIBUTE_UNUSED = vec_load(At11[index]);
    CCTK_REAL_VEC At12L CCTK_ATTRIBUTE_UNUSED = vec_load(At12[index]);
    CCTK_REAL_VEC At13L CCTK_ATTRIBUTE_UNUSED = vec_load(At13[index]);
    CCTK_REAL_VEC At22L CCTK_ATTRIBUTE_UNUSED = vec_load(At22[index]);
    CCTK_REAL_VEC At23L CCTK_ATTRIBUTE_UNUSED = vec_load(At23[index]);
    CCTK_REAL_VEC At33L CCTK_ATTRIBUTE_UNUSED = vec_load(At33[index]);
    CCTK_REAL_VEC B1L CCTK_ATTRIBUTE_UNUSED = vec_load(B1[index]);
    CCTK_REAL_VEC B2L CCTK_ATTRIBUTE_UNUSED = vec_load(B2[index]);
    CCTK_REAL_VEC B3L CCTK_ATTRIBUTE_UNUSED = vec_load(B3[index]);
    CCTK_REAL_VEC beta1L CCTK_ATTRIBUTE_UNUSED = vec_load(beta1[index]);
    CCTK_REAL_VEC beta2L CCTK_ATTRIBUTE_UNUSED = vec_load(beta2[index]);
    CCTK_REAL_VEC beta3L CCTK_ATTRIBUTE_UNUSED = vec_load(beta3[index]);
    CCTK_REAL_VEC gt11L CCTK_ATTRIBUTE_UNUSED = vec_load(gt11[index]);
    CCTK_REAL_VEC gt12L CCTK_ATTRIBUTE_UNUSED = vec_load(gt12[index]);
    CCTK_REAL_VEC gt13L CCTK_ATTRIBUTE_UNUSED = vec_load(gt13[index]);
    CCTK_REAL_VEC gt22L CCTK_ATTRIBUTE_UNUSED = vec_load(gt22[index]);
    CCTK_REAL_VEC gt23L CCTK_ATTRIBUTE_UNUSED = vec_load(gt23[index]);
    CCTK_REAL_VEC gt33L CCTK_ATTRIBUTE_UNUSED = vec_load(gt33[index]);
    CCTK_REAL_VEC phiL CCTK_ATTRIBUTE_UNUSED = vec_load(phi[index]);
    CCTK_REAL_VEC trKL CCTK_ATTRIBUTE_UNUSED = vec_load(trK[index]);
    CCTK_REAL_VEC Xt1L CCTK_ATTRIBUTE_UNUSED = vec_load(Xt1[index]);
    CCTK_REAL_VEC Xt2L CCTK_ATTRIBUTE_UNUSED = vec_load(Xt2[index]);
    CCTK_REAL_VEC Xt3L CCTK_ATTRIBUTE_UNUSED = vec_load(Xt3[index]);
    
    /* Include user supplied include files */
    /* Precompute derivatives */
    const CCTK_REAL_VEC PDstandardNth1alpha CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&alpha[index]);
    const CCTK_REAL_VEC PDstandardNth2alpha CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&alpha[index]);
    const CCTK_REAL_VEC PDstandardNth3alpha CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&alpha[index]);
    const CCTK_REAL_VEC PDstandardNth11alpha CCTK_ATTRIBUTE_UNUSED = PDstandardNth11(&alpha[index]);
    const CCTK_REAL_VEC PDstandardNth22alpha CCTK_ATTRIBUTE_UNUSED = PDstandardNth22(&alpha[index]);
    const CCTK_REAL_VEC PDstandardNth33alpha CCTK_ATTRIBUTE_UNUSED = PDstandardNth33(&alpha[index]);
    const CCTK_REAL_VEC PDstandardNth12alpha CCTK_ATTRIBUTE_UNUSED = PDstandardNth12(&alpha[index]);
    const CCTK_REAL_VEC PDstandardNth13alpha CCTK_ATTRIBUTE_UNUSED = PDstandardNth13(&alpha[index]);
    const CCTK_REAL_VEC PDstandardNth23alpha CCTK_ATTRIBUTE_UNUSED = PDstandardNth23(&alpha[index]);
    const CCTK_REAL_VEC PDstandardNth1beta1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&beta1[index]);
    const CCTK_REAL_VEC PDstandardNth2beta1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&beta1[index]);
    const CCTK_REAL_VEC PDstandardNth3beta1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&beta1[index]);
    const CCTK_REAL_VEC PDstandardNth11beta1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth11(&beta1[index]);
    const CCTK_REAL_VEC PDstandardNth22beta1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth22(&beta1[index]);
    const CCTK_REAL_VEC PDstandardNth33beta1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth33(&beta1[index]);
    const CCTK_REAL_VEC PDstandardNth12beta1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth12(&beta1[index]);
    const CCTK_REAL_VEC PDstandardNth13beta1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth13(&beta1[index]);
    const CCTK_REAL_VEC PDstandardNth23beta1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth23(&beta1[index]);
    const CCTK_REAL_VEC PDstandardNth1beta2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&beta2[index]);
    const CCTK_REAL_VEC PDstandardNth2beta2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&beta2[index]);
    const CCTK_REAL_VEC PDstandardNth3beta2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&beta2[index]);
    const CCTK_REAL_VEC PDstandardNth11beta2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth11(&beta2[index]);
    const CCTK_REAL_VEC PDstandardNth22beta2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth22(&beta2[index]);
    const CCTK_REAL_VEC PDstandardNth33beta2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth33(&beta2[index]);
    const CCTK_REAL_VEC PDstandardNth12beta2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth12(&beta2[index]);
    const CCTK_REAL_VEC PDstandardNth13beta2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth13(&beta2[index]);
    const CCTK_REAL_VEC PDstandardNth23beta2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth23(&beta2[index]);
    const CCTK_REAL_VEC PDstandardNth1beta3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&beta3[index]);
    const CCTK_REAL_VEC PDstandardNth2beta3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&beta3[index]);
    const CCTK_REAL_VEC PDstandardNth3beta3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&beta3[index]);
    const CCTK_REAL_VEC PDstandardNth11beta3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth11(&beta3[index]);
    const CCTK_REAL_VEC PDstandardNth22beta3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth22(&beta3[index]);
    const CCTK_REAL_VEC PDstandardNth33beta3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth33(&beta3[index]);
    const CCTK_REAL_VEC PDstandardNth12beta3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth12(&beta3[index]);
    const CCTK_REAL_VEC PDstandardNth13beta3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth13(&beta3[index]);
    const CCTK_REAL_VEC PDstandardNth23beta3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth23(&beta3[index]);
    const CCTK_REAL_VEC PDstandardNth1gt11 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&gt11[index]);
    const CCTK_REAL_VEC PDstandardNth2gt11 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&gt11[index]);
    const CCTK_REAL_VEC PDstandardNth3gt11 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&gt11[index]);
    const CCTK_REAL_VEC PDstandardNth11gt11 CCTK_ATTRIBUTE_UNUSED = PDstandardNth11(&gt11[index]);
    const CCTK_REAL_VEC PDstandardNth22gt11 CCTK_ATTRIBUTE_UNUSED = PDstandardNth22(&gt11[index]);
    const CCTK_REAL_VEC PDstandardNth33gt11 CCTK_ATTRIBUTE_UNUSED = PDstandardNth33(&gt11[index]);
    const CCTK_REAL_VEC PDstandardNth12gt11 CCTK_ATTRIBUTE_UNUSED = PDstandardNth12(&gt11[index]);
    const CCTK_REAL_VEC PDstandardNth13gt11 CCTK_ATTRIBUTE_UNUSED = PDstandardNth13(&gt11[index]);
    const CCTK_REAL_VEC PDstandardNth23gt11 CCTK_ATTRIBUTE_UNUSED = PDstandardNth23(&gt11[index]);
    const CCTK_REAL_VEC PDstandardNth1gt12 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&gt12[index]);
    const CCTK_REAL_VEC PDstandardNth2gt12 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&gt12[index]);
    const CCTK_REAL_VEC PDstandardNth3gt12 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&gt12[index]);
    const CCTK_REAL_VEC PDstandardNth11gt12 CCTK_ATTRIBUTE_UNUSED = PDstandardNth11(&gt12[index]);
    const CCTK_REAL_VEC PDstandardNth22gt12 CCTK_ATTRIBUTE_UNUSED = PDstandardNth22(&gt12[index]);
    const CCTK_REAL_VEC PDstandardNth33gt12 CCTK_ATTRIBUTE_UNUSED = PDstandardNth33(&gt12[index]);
    const CCTK_REAL_VEC PDstandardNth12gt12 CCTK_ATTRIBUTE_UNUSED = PDstandardNth12(&gt12[index]);
    const CCTK_REAL_VEC PDstandardNth13gt12 CCTK_ATTRIBUTE_UNUSED = PDstandardNth13(&gt12[index]);
    const CCTK_REAL_VEC PDstandardNth23gt12 CCTK_ATTRIBUTE_UNUSED = PDstandardNth23(&gt12[index]);
    const CCTK_REAL_VEC PDstandardNth1gt13 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&gt13[index]);
    const CCTK_REAL_VEC PDstandardNth2gt13 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&gt13[index]);
    const CCTK_REAL_VEC PDstandardNth3gt13 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&gt13[index]);
    const CCTK_REAL_VEC PDstandardNth11gt13 CCTK_ATTRIBUTE_UNUSED = PDstandardNth11(&gt13[index]);
    const CCTK_REAL_VEC PDstandardNth22gt13 CCTK_ATTRIBUTE_UNUSED = PDstandardNth22(&gt13[index]);
    const CCTK_REAL_VEC PDstandardNth33gt13 CCTK_ATTRIBUTE_UNUSED = PDstandardNth33(&gt13[index]);
    const CCTK_REAL_VEC PDstandardNth12gt13 CCTK_ATTRIBUTE_UNUSED = PDstandardNth12(&gt13[index]);
    const CCTK_REAL_VEC PDstandardNth13gt13 CCTK_ATTRIBUTE_UNUSED = PDstandardNth13(&gt13[index]);
    const CCTK_REAL_VEC PDstandardNth23gt13 CCTK_ATTRIBUTE_UNUSED = PDstandardNth23(&gt13[index]);
    const CCTK_REAL_VEC PDstandardNth1gt22 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&gt22[index]);
    const CCTK_REAL_VEC PDstandardNth2gt22 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&gt22[index]);
    const CCTK_REAL_VEC PDstandardNth3gt22 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&gt22[index]);
    const CCTK_REAL_VEC PDstandardNth11gt22 CCTK_ATTRIBUTE_UNUSED = PDstandardNth11(&gt22[index]);
    const CCTK_REAL_VEC PDstandardNth22gt22 CCTK_ATTRIBUTE_UNUSED = PDstandardNth22(&gt22[index]);
    const CCTK_REAL_VEC PDstandardNth33gt22 CCTK_ATTRIBUTE_UNUSED = PDstandardNth33(&gt22[index]);
    const CCTK_REAL_VEC PDstandardNth12gt22 CCTK_ATTRIBUTE_UNUSED = PDstandardNth12(&gt22[index]);
    const CCTK_REAL_VEC PDstandardNth13gt22 CCTK_ATTRIBUTE_UNUSED = PDstandardNth13(&gt22[index]);
    const CCTK_REAL_VEC PDstandardNth23gt22 CCTK_ATTRIBUTE_UNUSED = PDstandardNth23(&gt22[index]);
    const CCTK_REAL_VEC PDstandardNth1gt23 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&gt23[index]);
    const CCTK_REAL_VEC PDstandardNth2gt23 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&gt23[index]);
    const CCTK_REAL_VEC PDstandardNth3gt23 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&gt23[index]);
    const CCTK_REAL_VEC PDstandardNth11gt23 CCTK_ATTRIBUTE_UNUSED = PDstandardNth11(&gt23[index]);
    const CCTK_REAL_VEC PDstandardNth22gt23 CCTK_ATTRIBUTE_UNUSED = PDstandardNth22(&gt23[index]);
    const CCTK_REAL_VEC PDstandardNth33gt23 CCTK_ATTRIBUTE_UNUSED = PDstandardNth33(&gt23[index]);
    const CCTK_REAL_VEC PDstandardNth12gt23 CCTK_ATTRIBUTE_UNUSED = PDstandardNth12(&gt23[index]);
    const CCTK_REAL_VEC PDstandardNth13gt23 CCTK_ATTRIBUTE_UNUSED = PDstandardNth13(&gt23[index]);
    const CCTK_REAL_VEC PDstandardNth23gt23 CCTK_ATTRIBUTE_UNUSED = PDstandardNth23(&gt23[index]);
    const CCTK_REAL_VEC PDstandardNth1gt33 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&gt33[index]);
    const CCTK_REAL_VEC PDstandardNth2gt33 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&gt33[index]);
    const CCTK_REAL_VEC PDstandardNth3gt33 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&gt33[index]);
    const CCTK_REAL_VEC PDstandardNth11gt33 CCTK_ATTRIBUTE_UNUSED = PDstandardNth11(&gt33[index]);
    const CCTK_REAL_VEC PDstandardNth22gt33 CCTK_ATTRIBUTE_UNUSED = PDstandardNth22(&gt33[index]);
    const CCTK_REAL_VEC PDstandardNth33gt33 CCTK_ATTRIBUTE_UNUSED = PDstandardNth33(&gt33[index]);
    const CCTK_REAL_VEC PDstandardNth12gt33 CCTK_ATTRIBUTE_UNUSED = PDstandardNth12(&gt33[index]);
    const CCTK_REAL_VEC PDstandardNth13gt33 CCTK_ATTRIBUTE_UNUSED = PDstandardNth13(&gt33[index]);
    const CCTK_REAL_VEC PDstandardNth23gt33 CCTK_ATTRIBUTE_UNUSED = PDstandardNth23(&gt33[index]);
    const CCTK_REAL_VEC PDstandardNth1phi CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&phi[index]);
    const CCTK_REAL_VEC PDstandardNth2phi CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&phi[index]);
    const CCTK_REAL_VEC PDstandardNth3phi CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&phi[index]);
    const CCTK_REAL_VEC PDstandardNth11phi CCTK_ATTRIBUTE_UNUSED = PDstandardNth11(&phi[index]);
    const CCTK_REAL_VEC PDstandardNth22phi CCTK_ATTRIBUTE_UNUSED = PDstandardNth22(&phi[index]);
    const CCTK_REAL_VEC PDstandardNth33phi CCTK_ATTRIBUTE_UNUSED = PDstandardNth33(&phi[index]);
    const CCTK_REAL_VEC PDstandardNth12phi CCTK_ATTRIBUTE_UNUSED = PDstandardNth12(&phi[index]);
    const CCTK_REAL_VEC PDstandardNth13phi CCTK_ATTRIBUTE_UNUSED = PDstandardNth13(&phi[index]);
    const CCTK_REAL_VEC PDstandardNth23phi CCTK_ATTRIBUTE_UNUSED = PDstandardNth23(&phi[index]);
    const CCTK_REAL_VEC PDstandardNth1trK CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&trK[index]);
    const CCTK_REAL_VEC PDstandardNth2trK CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&trK[index]);
    const CCTK_REAL_VEC PDstandardNth3trK CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&trK[index]);
    const CCTK_REAL_VEC PDstandardNth1Xt1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&Xt1[index]);
    const CCTK_REAL_VEC PDstandardNth2Xt1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&Xt1[index]);
    const CCTK_REAL_VEC PDstandardNth3Xt1 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&Xt1[index]);
    const CCTK_REAL_VEC PDstandardNth1Xt2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&Xt2[index]);
    const CCTK_REAL_VEC PDstandardNth2Xt2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&Xt2[index]);
    const CCTK_REAL_VEC PDstandardNth3Xt2 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&Xt2[index]);
    const CCTK_REAL_VEC PDstandardNth1Xt3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth1(&Xt3[index]);
    const CCTK_REAL_VEC PDstandardNth2Xt3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth2(&Xt3[index]);
    const CCTK_REAL_VEC PDstandardNth3Xt3 CCTK_ATTRIBUTE_UNUSED = PDstandardNth3(&Xt3[index]);
    /* Calculate temporaries and grid functions */
    ptrdiff_t dir1 CCTK_ATTRIBUTE_UNUSED = kisgn(beta1L);
    
    ptrdiff_t dir2 CCTK_ATTRIBUTE_UNUSED = kisgn(beta2L);
    
    ptrdiff_t dir3 CCTK_ATTRIBUTE_UNUSED = kisgn(beta3L);
    
    CCTK_REAL_VEC detgt CCTK_ATTRIBUTE_UNUSED = ToReal(1);
    
    CCTK_REAL_VEC gtu11 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(gt22L,gt33L,kmul(gt23L,gt23L)),detgt);
    
    CCTK_REAL_VEC gtu12 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(gt13L,gt23L,kmul(gt12L,gt33L)),detgt);
    
    CCTK_REAL_VEC gtu13 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(gt12L,gt23L,kmul(gt13L,gt22L)),detgt);
    
    CCTK_REAL_VEC gtu22 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(gt11L,gt33L,kmul(gt13L,gt13L)),detgt);
    
    CCTK_REAL_VEC gtu23 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(gt12L,gt13L,kmul(gt11L,gt23L)),detgt);
    
    CCTK_REAL_VEC gtu33 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(gt11L,gt22L,kmul(gt12L,gt12L)),detgt);
    
    CCTK_REAL_VEC Gtl111 CCTK_ATTRIBUTE_UNUSED = 
      kmul(PDstandardNth1gt11,ToReal(0.5));
    
    CCTK_REAL_VEC Gtl112 CCTK_ATTRIBUTE_UNUSED = 
      kmul(PDstandardNth2gt11,ToReal(0.5));
    
    CCTK_REAL_VEC Gtl113 CCTK_ATTRIBUTE_UNUSED = 
      kmul(PDstandardNth3gt11,ToReal(0.5));
    
    CCTK_REAL_VEC Gtl122 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(ToReal(-0.5),PDstandardNth1gt22,PDstandardNth2gt12);
    
    CCTK_REAL_VEC Gtl123 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ksub(kadd(PDstandardNth2gt13,PDstandardNth3gt12),PDstandardNth1gt23),ToReal(0.5));
    
    CCTK_REAL_VEC Gtl133 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(ToReal(-0.5),PDstandardNth1gt33,PDstandardNth3gt13);
    
    CCTK_REAL_VEC Gtl211 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(ToReal(-0.5),PDstandardNth2gt11,PDstandardNth1gt12);
    
    CCTK_REAL_VEC Gtl212 CCTK_ATTRIBUTE_UNUSED = 
      kmul(PDstandardNth1gt22,ToReal(0.5));
    
    CCTK_REAL_VEC Gtl213 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kadd(PDstandardNth1gt23,ksub(PDstandardNth3gt12,PDstandardNth2gt13)),ToReal(0.5));
    
    CCTK_REAL_VEC Gtl222 CCTK_ATTRIBUTE_UNUSED = 
      kmul(PDstandardNth2gt22,ToReal(0.5));
    
    CCTK_REAL_VEC Gtl223 CCTK_ATTRIBUTE_UNUSED = 
      kmul(PDstandardNth3gt22,ToReal(0.5));
    
    CCTK_REAL_VEC Gtl233 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(ToReal(-0.5),PDstandardNth2gt33,PDstandardNth3gt23);
    
    CCTK_REAL_VEC Gtl311 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(ToReal(-0.5),PDstandardNth3gt11,PDstandardNth1gt13);
    
    CCTK_REAL_VEC Gtl312 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kadd(PDstandardNth1gt23,ksub(PDstandardNth2gt13,PDstandardNth3gt12)),ToReal(0.5));
    
    CCTK_REAL_VEC Gtl313 CCTK_ATTRIBUTE_UNUSED = 
      kmul(PDstandardNth1gt33,ToReal(0.5));
    
    CCTK_REAL_VEC Gtl322 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(ToReal(-0.5),PDstandardNth3gt22,PDstandardNth2gt23);
    
    CCTK_REAL_VEC Gtl323 CCTK_ATTRIBUTE_UNUSED = 
      kmul(PDstandardNth2gt33,ToReal(0.5));
    
    CCTK_REAL_VEC Gtl333 CCTK_ATTRIBUTE_UNUSED = 
      kmul(PDstandardNth3gt33,ToReal(0.5));
    
    CCTK_REAL_VEC Gtlu111 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl111,gtu11,kmadd(Gtl112,gtu12,kmul(Gtl113,gtu13)));
    
    CCTK_REAL_VEC Gtlu112 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl111,gtu12,kmadd(Gtl112,gtu22,kmul(Gtl113,gtu23)));
    
    CCTK_REAL_VEC Gtlu113 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl111,gtu13,kmadd(Gtl112,gtu23,kmul(Gtl113,gtu33)));
    
    CCTK_REAL_VEC Gtlu121 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl112,gtu11,kmadd(Gtl122,gtu12,kmul(Gtl123,gtu13)));
    
    CCTK_REAL_VEC Gtlu122 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl112,gtu12,kmadd(Gtl122,gtu22,kmul(Gtl123,gtu23)));
    
    CCTK_REAL_VEC Gtlu123 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl112,gtu13,kmadd(Gtl122,gtu23,kmul(Gtl123,gtu33)));
    
    CCTK_REAL_VEC Gtlu131 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl113,gtu11,kmadd(Gtl123,gtu12,kmul(Gtl133,gtu13)));
    
    CCTK_REAL_VEC Gtlu132 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl113,gtu12,kmadd(Gtl123,gtu22,kmul(Gtl133,gtu23)));
    
    CCTK_REAL_VEC Gtlu133 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl113,gtu13,kmadd(Gtl123,gtu23,kmul(Gtl133,gtu33)));
    
    CCTK_REAL_VEC Gtlu211 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl211,gtu11,kmadd(Gtl212,gtu12,kmul(Gtl213,gtu13)));
    
    CCTK_REAL_VEC Gtlu212 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl211,gtu12,kmadd(Gtl212,gtu22,kmul(Gtl213,gtu23)));
    
    CCTK_REAL_VEC Gtlu213 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl211,gtu13,kmadd(Gtl212,gtu23,kmul(Gtl213,gtu33)));
    
    CCTK_REAL_VEC Gtlu221 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl212,gtu11,kmadd(Gtl222,gtu12,kmul(Gtl223,gtu13)));
    
    CCTK_REAL_VEC Gtlu222 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl212,gtu12,kmadd(Gtl222,gtu22,kmul(Gtl223,gtu23)));
    
    CCTK_REAL_VEC Gtlu223 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl212,gtu13,kmadd(Gtl222,gtu23,kmul(Gtl223,gtu33)));
    
    CCTK_REAL_VEC Gtlu231 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl213,gtu11,kmadd(Gtl223,gtu12,kmul(Gtl233,gtu13)));
    
    CCTK_REAL_VEC Gtlu232 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl213,gtu12,kmadd(Gtl223,gtu22,kmul(Gtl233,gtu23)));
    
    CCTK_REAL_VEC Gtlu233 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl213,gtu13,kmadd(Gtl223,gtu23,kmul(Gtl233,gtu33)));
    
    CCTK_REAL_VEC Gtlu311 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl311,gtu11,kmadd(Gtl312,gtu12,kmul(Gtl313,gtu13)));
    
    CCTK_REAL_VEC Gtlu312 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl311,gtu12,kmadd(Gtl312,gtu22,kmul(Gtl313,gtu23)));
    
    CCTK_REAL_VEC Gtlu313 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl311,gtu13,kmadd(Gtl312,gtu23,kmul(Gtl313,gtu33)));
    
    CCTK_REAL_VEC Gtlu321 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl312,gtu11,kmadd(Gtl322,gtu12,kmul(Gtl323,gtu13)));
    
    CCTK_REAL_VEC Gtlu322 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl312,gtu12,kmadd(Gtl322,gtu22,kmul(Gtl323,gtu23)));
    
    CCTK_REAL_VEC Gtlu323 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl312,gtu13,kmadd(Gtl322,gtu23,kmul(Gtl323,gtu33)));
    
    CCTK_REAL_VEC Gtlu331 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl313,gtu11,kmadd(Gtl323,gtu12,kmul(Gtl333,gtu13)));
    
    CCTK_REAL_VEC Gtlu332 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl313,gtu12,kmadd(Gtl323,gtu22,kmul(Gtl333,gtu23)));
    
    CCTK_REAL_VEC Gtlu333 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl313,gtu13,kmadd(Gtl323,gtu23,kmul(Gtl333,gtu33)));
    
    CCTK_REAL_VEC Gt111 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl111,gtu11,kmadd(Gtl211,gtu12,kmul(Gtl311,gtu13)));
    
    CCTK_REAL_VEC Gt211 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl111,gtu12,kmadd(Gtl211,gtu22,kmul(Gtl311,gtu23)));
    
    CCTK_REAL_VEC Gt311 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl111,gtu13,kmadd(Gtl211,gtu23,kmul(Gtl311,gtu33)));
    
    CCTK_REAL_VEC Gt112 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl112,gtu11,kmadd(Gtl212,gtu12,kmul(Gtl312,gtu13)));
    
    CCTK_REAL_VEC Gt212 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl112,gtu12,kmadd(Gtl212,gtu22,kmul(Gtl312,gtu23)));
    
    CCTK_REAL_VEC Gt312 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl112,gtu13,kmadd(Gtl212,gtu23,kmul(Gtl312,gtu33)));
    
    CCTK_REAL_VEC Gt113 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl113,gtu11,kmadd(Gtl213,gtu12,kmul(Gtl313,gtu13)));
    
    CCTK_REAL_VEC Gt213 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl113,gtu12,kmadd(Gtl213,gtu22,kmul(Gtl313,gtu23)));
    
    CCTK_REAL_VEC Gt313 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl113,gtu13,kmadd(Gtl213,gtu23,kmul(Gtl313,gtu33)));
    
    CCTK_REAL_VEC Gt122 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl122,gtu11,kmadd(Gtl222,gtu12,kmul(Gtl322,gtu13)));
    
    CCTK_REAL_VEC Gt222 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl122,gtu12,kmadd(Gtl222,gtu22,kmul(Gtl322,gtu23)));
    
    CCTK_REAL_VEC Gt322 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl122,gtu13,kmadd(Gtl222,gtu23,kmul(Gtl322,gtu33)));
    
    CCTK_REAL_VEC Gt123 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl123,gtu11,kmadd(Gtl223,gtu12,kmul(Gtl323,gtu13)));
    
    CCTK_REAL_VEC Gt223 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl123,gtu12,kmadd(Gtl223,gtu22,kmul(Gtl323,gtu23)));
    
    CCTK_REAL_VEC Gt323 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl123,gtu13,kmadd(Gtl223,gtu23,kmul(Gtl323,gtu33)));
    
    CCTK_REAL_VEC Gt133 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl133,gtu11,kmadd(Gtl233,gtu12,kmul(Gtl333,gtu13)));
    
    CCTK_REAL_VEC Gt233 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl133,gtu12,kmadd(Gtl233,gtu22,kmul(Gtl333,gtu23)));
    
    CCTK_REAL_VEC Gt333 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gtl133,gtu13,kmadd(Gtl233,gtu23,kmul(Gtl333,gtu33)));
    
    CCTK_REAL_VEC Xtn1 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gt111,gtu11,kmadd(Gt122,gtu22,kmadd(ToReal(2),kmadd(Gt112,gtu12,kmadd(Gt113,gtu13,kmul(Gt123,gtu23))),kmul(Gt133,gtu33))));
    
    CCTK_REAL_VEC Xtn2 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gt211,gtu11,kmadd(Gt222,gtu22,kmadd(ToReal(2),kmadd(Gt212,gtu12,kmadd(Gt213,gtu13,kmul(Gt223,gtu23))),kmul(Gt233,gtu33))));
    
    CCTK_REAL_VEC Xtn3 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gt311,gtu11,kmadd(Gt322,gtu22,kmadd(ToReal(2),kmadd(Gt312,gtu12,kmadd(Gt313,gtu13,kmul(Gt323,gtu23))),kmul(Gt333,gtu33))));
    
    CCTK_REAL_VEC Rt11 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(ToReal(3),kmadd(Gt111,Gtlu111,kmadd(Gt112,Gtlu112,kmul(Gt113,Gtlu113))),kmadd(ToReal(2),kmadd(Gt211,Gtlu121,kmadd(Gt212,Gtlu122,kmadd(Gt213,Gtlu123,kmadd(Gt311,Gtlu131,kmadd(Gt312,Gtlu132,kmul(Gt313,Gtlu133)))))),kmadd(Gt211,Gtlu211,kmadd(Gt212,Gtlu212,kmadd(Gt213,Gtlu213,kmadd(Gt311,Gtlu311,kmadd(Gt312,Gtlu312,kmadd(Gt313,Gtlu313,kmadd(gt11L,PDstandardNth1Xt1,kmadd(gt12L,PDstandardNth1Xt2,kmadd(gt13L,PDstandardNth1Xt3,kmadd(ToReal(0.5),knmsub(gtu11,PDstandardNth11gt11,kmadd(ToReal(-2),kmul(gtu12,PDstandardNth12gt11),kmadd(ToReal(-2),kmul(gtu13,PDstandardNth13gt11),knmsub(gtu22,PDstandardNth22gt11,kmsub(ToReal(-2),kmul(gtu23,PDstandardNth23gt11),kmul(gtu33,PDstandardNth33gt11)))))),kmadd(Gtl111,Xtn1,kmadd(Gtl112,Xtn2,kmul(Gtl113,Xtn3)))))))))))))));
    
    CCTK_REAL_VEC Rt12 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(ToReal(4),kmadd(Gt211,Gtlu221,kmadd(Gt212,Gtlu222,kmul(Gt213,Gtlu223))),kmadd(ToReal(2),kmadd(Gt112,Gtlu111,kmadd(Gt122,Gtlu112,kmadd(Gt123,Gtlu113,kmadd(Gt111,Gtlu121,kmadd(Gt212,Gtlu121,kmadd(Gt112,Gtlu122,kmadd(Gt222,Gtlu122,kmadd(Gt113,Gtlu123,kmadd(Gt223,Gtlu123,kmadd(Gt312,Gtlu131,kmadd(Gt322,Gtlu132,kmadd(Gt323,Gtlu133,kmadd(Gt111,Gtlu211,kmadd(Gt112,Gtlu212,kmadd(Gt113,Gtlu213,kmadd(Gt311,Gtlu231,kmadd(Gt312,Gtlu232,kmadd(Gt313,Gtlu233,kmadd(Gt311,Gtlu321,kmadd(Gt312,Gtlu322,kmul(Gt313,Gtlu323))))))))))))))))))))),knmsub(gtu11,PDstandardNth11gt12,kmadd(ToReal(-2),kmul(gtu12,PDstandardNth12gt12),kmadd(ToReal(-2),kmul(gtu13,PDstandardNth13gt12),kmadd(gt12L,PDstandardNth1Xt1,kmadd(gt22L,PDstandardNth1Xt2,kmadd(gt23L,PDstandardNth1Xt3,knmsub(gtu22,PDstandardNth22gt12,kmadd(ToReal(-2),kmul(gtu23,PDstandardNth23gt12),kmadd(gt11L,PDstandardNth2Xt1,kmadd(gt12L,PDstandardNth2Xt2,kmadd(gt13L,PDstandardNth2Xt3,knmsub(gtu33,PDstandardNth33gt12,kmadd(Gtl112,Xtn1,kmadd(Gtl211,Xtn1,kmadd(Gtl122,Xtn2,kmadd(Gtl212,Xtn2,kmadd(Gtl123,Xtn3,kmul(Gtl213,Xtn3)))))))))))))))))))),ToReal(0.5));
    
    CCTK_REAL_VEC Rt13 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(ToReal(2),kmadd(Gt113,Gtlu111,kmadd(Gt123,Gtlu112,kmadd(Gt133,Gtlu113,kmadd(Gt213,Gtlu121,kmadd(Gt223,Gtlu122,kmadd(Gt233,Gtlu123,kmadd(Gt111,Gtlu131,kmadd(Gt313,Gtlu131,kmadd(Gt112,Gtlu132,kmadd(Gt323,Gtlu132,kmadd(Gt113,Gtlu133,kmadd(Gt333,Gtlu133,kmadd(Gt211,Gtlu231,kmadd(Gt212,Gtlu232,kmadd(Gt213,Gtlu233,kmadd(Gt111,Gtlu311,kmadd(Gt112,Gtlu312,kmadd(Gt113,Gtlu313,kmadd(Gt211,Gtlu321,kmadd(Gt212,Gtlu322,kmul(Gt213,Gtlu323))))))))))))))))))))),kmadd(ToReal(4),kmadd(Gt311,Gtlu331,kmadd(Gt312,Gtlu332,kmul(Gt313,Gtlu333))),knmsub(gtu11,PDstandardNth11gt13,kmadd(ToReal(-2),kmul(gtu12,PDstandardNth12gt13),kmadd(ToReal(-2),kmul(gtu13,PDstandardNth13gt13),kmadd(gt13L,PDstandardNth1Xt1,kmadd(gt23L,PDstandardNth1Xt2,kmadd(gt33L,PDstandardNth1Xt3,knmsub(gtu22,PDstandardNth22gt13,kmadd(ToReal(-2),kmul(gtu23,PDstandardNth23gt13),knmsub(gtu33,PDstandardNth33gt13,kmadd(gt11L,PDstandardNth3Xt1,kmadd(gt12L,PDstandardNth3Xt2,kmadd(gt13L,PDstandardNth3Xt3,kmadd(Gtl113,Xtn1,kmadd(Gtl311,Xtn1,kmadd(Gtl123,Xtn2,kmadd(Gtl312,Xtn2,kmadd(Gtl133,Xtn3,kmul(Gtl313,Xtn3)))))))))))))))))))),ToReal(0.5));
    
    CCTK_REAL_VEC Rt22 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gt112,kmadd(ToReal(2),Gtlu211,Gtlu121),kmadd(Gt122,kmadd(ToReal(2),Gtlu212,Gtlu122),kmadd(Gt123,kmadd(ToReal(2),Gtlu213,Gtlu123),kmadd(ToReal(3),kmadd(Gt212,Gtlu221,kmadd(Gt222,Gtlu222,kmul(Gt223,Gtlu223))),kmadd(ToReal(2),kmadd(Gt312,Gtlu231,kmadd(Gt322,Gtlu232,kmul(Gt323,Gtlu233))),kmadd(Gt312,Gtlu321,kmadd(Gt322,Gtlu322,kmadd(Gt323,Gtlu323,kmadd(gt12L,PDstandardNth2Xt1,kmadd(gt22L,PDstandardNth2Xt2,kmadd(gt23L,PDstandardNth2Xt3,kmadd(ToReal(0.5),knmsub(gtu11,PDstandardNth11gt22,kmadd(ToReal(-2),kmul(gtu12,PDstandardNth12gt22),kmadd(ToReal(-2),kmul(gtu13,PDstandardNth13gt22),knmsub(gtu22,PDstandardNth22gt22,kmsub(ToReal(-2),kmul(gtu23,PDstandardNth23gt22),kmul(gtu33,PDstandardNth33gt22)))))),kmadd(Gtl212,Xtn1,kmadd(Gtl222,Xtn2,kmul(Gtl223,Xtn3)))))))))))))));
    
    CCTK_REAL_VEC Rt23 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(ToReal(2),kmadd(Gt112,Gtlu131,kmadd(Gt122,Gtlu132,kmadd(Gt123,Gtlu133,kmadd(Gt113,Gtlu211,kmadd(Gt123,Gtlu212,kmadd(Gt133,Gtlu213,kmadd(Gt213,Gtlu221,kmadd(Gt223,Gtlu222,kmadd(Gt233,Gtlu223,kmadd(Gt212,Gtlu231,kmadd(Gt313,Gtlu231,kmadd(Gt222,Gtlu232,kmadd(Gt323,Gtlu232,kmadd(Gt223,Gtlu233,kmadd(Gt333,Gtlu233,kmadd(Gt112,Gtlu311,kmadd(Gt122,Gtlu312,kmadd(Gt123,Gtlu313,kmadd(Gt212,Gtlu321,kmadd(Gt222,Gtlu322,kmul(Gt223,Gtlu323))))))))))))))))))))),kmadd(ToReal(4),kmadd(Gt312,Gtlu331,kmadd(Gt322,Gtlu332,kmul(Gt323,Gtlu333))),knmsub(gtu11,PDstandardNth11gt23,kmadd(ToReal(-2),kmul(gtu12,PDstandardNth12gt23),kmadd(ToReal(-2),kmul(gtu13,PDstandardNth13gt23),knmsub(gtu22,PDstandardNth22gt23,kmadd(ToReal(-2),kmul(gtu23,PDstandardNth23gt23),kmadd(gt13L,PDstandardNth2Xt1,kmadd(gt23L,PDstandardNth2Xt2,kmadd(gt33L,PDstandardNth2Xt3,knmsub(gtu33,PDstandardNth33gt23,kmadd(gt12L,PDstandardNth3Xt1,kmadd(gt22L,PDstandardNth3Xt2,kmadd(gt23L,PDstandardNth3Xt3,kmadd(Gtl213,Xtn1,kmadd(Gtl312,Xtn1,kmadd(Gtl223,Xtn2,kmadd(Gtl322,Xtn2,kmadd(Gtl233,Xtn3,kmul(Gtl323,Xtn3)))))))))))))))))))),ToReal(0.5));
    
    CCTK_REAL_VEC Rt33 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gt113,kmadd(ToReal(2),Gtlu311,Gtlu131),kmadd(Gt123,kmadd(ToReal(2),Gtlu312,Gtlu132),kmadd(Gt133,kmadd(ToReal(2),Gtlu313,Gtlu133),kmadd(Gt213,kmadd(ToReal(2),Gtlu321,Gtlu231),kmadd(Gt223,kmadd(ToReal(2),Gtlu322,Gtlu232),kmadd(Gt233,kmadd(ToReal(2),Gtlu323,Gtlu233),kmadd(ToReal(3),kmadd(Gt313,Gtlu331,kmadd(Gt323,Gtlu332,kmul(Gt333,Gtlu333))),kmadd(ToReal(0.5),knmsub(gtu11,PDstandardNth11gt33,kmadd(ToReal(-2),kmul(gtu12,PDstandardNth12gt33),kmadd(ToReal(-2),kmul(gtu13,PDstandardNth13gt33),knmsub(gtu22,PDstandardNth22gt33,kmsub(ToReal(-2),kmul(gtu23,PDstandardNth23gt33),kmul(gtu33,PDstandardNth33gt33)))))),kmadd(gt13L,PDstandardNth3Xt1,kmadd(gt23L,PDstandardNth3Xt2,kmadd(gt33L,PDstandardNth3Xt3,kmadd(Gtl313,Xtn1,kmadd(Gtl323,Xtn2,kmul(Gtl333,Xtn3))))))))))))));
    
    CCTK_REAL_VEC fac1 CCTK_ATTRIBUTE_UNUSED = 
      IfThen(conformalMethod,kdiv(ToReal(-0.5),phiL),ToReal(1));
    
    CCTK_REAL_VEC cdphi1 CCTK_ATTRIBUTE_UNUSED = 
      kmul(fac1,PDstandardNth1phi);
    
    CCTK_REAL_VEC cdphi2 CCTK_ATTRIBUTE_UNUSED = 
      kmul(fac1,PDstandardNth2phi);
    
    CCTK_REAL_VEC cdphi3 CCTK_ATTRIBUTE_UNUSED = 
      kmul(fac1,PDstandardNth3phi);
    
    CCTK_REAL_VEC fac2 CCTK_ATTRIBUTE_UNUSED = 
      IfThen(conformalMethod,kdiv(ToReal(0.5),kmul(phiL,phiL)),ToReal(0));
    
    CCTK_REAL_VEC cdphi211 CCTK_ATTRIBUTE_UNUSED = 
      kmsub(fac2,kmul(PDstandardNth1phi,PDstandardNth1phi),kmul(fac1,ksub(kmadd(Gt111,PDstandardNth1phi,kmadd(Gt211,PDstandardNth2phi,kmul(Gt311,PDstandardNth3phi))),PDstandardNth11phi)));
    
    CCTK_REAL_VEC cdphi212 CCTK_ATTRIBUTE_UNUSED = 
      kmsub(fac2,kmul(PDstandardNth1phi,PDstandardNth2phi),kmul(fac1,ksub(kmadd(Gt112,PDstandardNth1phi,kmadd(Gt212,PDstandardNth2phi,kmul(Gt312,PDstandardNth3phi))),PDstandardNth12phi)));
    
    CCTK_REAL_VEC cdphi213 CCTK_ATTRIBUTE_UNUSED = 
      kmsub(fac2,kmul(PDstandardNth1phi,PDstandardNth3phi),kmul(fac1,ksub(kmadd(Gt113,PDstandardNth1phi,kmadd(Gt213,PDstandardNth2phi,kmul(Gt313,PDstandardNth3phi))),PDstandardNth13phi)));
    
    CCTK_REAL_VEC cdphi222 CCTK_ATTRIBUTE_UNUSED = 
      kmsub(fac2,kmul(PDstandardNth2phi,PDstandardNth2phi),kmul(fac1,kmadd(Gt122,PDstandardNth1phi,ksub(kmadd(Gt222,PDstandardNth2phi,kmul(Gt322,PDstandardNth3phi)),PDstandardNth22phi))));
    
    CCTK_REAL_VEC cdphi223 CCTK_ATTRIBUTE_UNUSED = 
      kmsub(fac2,kmul(PDstandardNth2phi,PDstandardNth3phi),kmul(fac1,kmadd(Gt123,PDstandardNth1phi,ksub(kmadd(Gt223,PDstandardNth2phi,kmul(Gt323,PDstandardNth3phi)),PDstandardNth23phi))));
    
    CCTK_REAL_VEC cdphi233 CCTK_ATTRIBUTE_UNUSED = 
      kmsub(fac2,kmul(PDstandardNth3phi,PDstandardNth3phi),kmul(fac1,kmadd(Gt133,PDstandardNth1phi,kmadd(Gt233,PDstandardNth2phi,kmsub(Gt333,PDstandardNth3phi,PDstandardNth33phi)))));
    
    CCTK_REAL_VEC Rphi11 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kadd(cdphi211,kmadd(ToReal(2),kmul(kmadd(gt11L,gtu11,ToReal(-1)),kmul(cdphi1,cdphi1)),kmul(gt11L,kmadd(cdphi211,gtu11,kmadd(cdphi222,gtu22,kmadd(ToReal(4),kmadd(cdphi1,kmadd(cdphi2,gtu12,kmul(cdphi3,gtu13)),kmul(cdphi2,kmul(cdphi3,gtu23))),kmadd(cdphi233,gtu33,kmul(ToReal(2),kmadd(cdphi212,gtu12,kmadd(cdphi213,gtu13,kmadd(cdphi223,gtu23,kmadd(gtu22,kmul(cdphi2,cdphi2),kmul(gtu33,kmul(cdphi3,cdphi3)))))))))))))),ToReal(-2));
    
    CCTK_REAL_VEC Rphi12 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kadd(cdphi212,kmadd(cdphi1,kmadd(cdphi2,kmadd(ToReal(4),kmul(gt12L,gtu12),ToReal(-2)),kmul(ToReal(4),kmul(gt12L,kmul(cdphi3,gtu13)))),kmul(gt12L,kmadd(cdphi211,gtu11,kmadd(cdphi222,gtu22,kmadd(ToReal(4),kmul(cdphi2,kmul(cdphi3,gtu23)),kmadd(cdphi233,gtu33,kmul(ToReal(2),kmadd(cdphi212,gtu12,kmadd(cdphi213,gtu13,kmadd(cdphi223,gtu23,kmadd(gtu11,kmul(cdphi1,cdphi1),kmadd(gtu22,kmul(cdphi2,cdphi2),kmul(gtu33,kmul(cdphi3,cdphi3))))))))))))))),ToReal(-2));
    
    CCTK_REAL_VEC Rphi13 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kadd(cdphi213,kmadd(cdphi1,kmadd(ToReal(4),kmul(gt13L,kmul(cdphi2,gtu12)),kmul(cdphi3,kmadd(ToReal(4),kmul(gt13L,gtu13),ToReal(-2)))),kmul(gt13L,kmadd(cdphi211,gtu11,kmadd(cdphi222,gtu22,kmadd(ToReal(4),kmul(cdphi2,kmul(cdphi3,gtu23)),kmadd(cdphi233,gtu33,kmul(ToReal(2),kmadd(cdphi212,gtu12,kmadd(cdphi213,gtu13,kmadd(cdphi223,gtu23,kmadd(gtu11,kmul(cdphi1,cdphi1),kmadd(gtu22,kmul(cdphi2,cdphi2),kmul(gtu33,kmul(cdphi3,cdphi3))))))))))))))),ToReal(-2));
    
    CCTK_REAL_VEC Rphi22 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kadd(cdphi222,kmadd(ToReal(2),kmul(kmadd(gt22L,gtu22,ToReal(-1)),kmul(cdphi2,cdphi2)),kmul(gt22L,kmadd(cdphi222,gtu22,kmadd(ToReal(4),kmadd(cdphi1,kmul(cdphi3,gtu13),kmul(cdphi2,kmadd(cdphi1,gtu12,kmul(cdphi3,gtu23)))),kmadd(cdphi233,gtu33,kmadd(gtu11,kmadd(ToReal(2),kmul(cdphi1,cdphi1),cdphi211),kmul(ToReal(2),kmadd(cdphi212,gtu12,kmadd(cdphi213,gtu13,kmadd(cdphi223,gtu23,kmul(gtu33,kmul(cdphi3,cdphi3))))))))))))),ToReal(-2));
    
    CCTK_REAL_VEC Rphi23 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kadd(cdphi223,kmadd(cdphi2,kmadd(ToReal(4),kmul(gt23L,kmul(cdphi1,gtu12)),kmul(cdphi3,kmadd(ToReal(4),kmul(gt23L,gtu23),ToReal(-2)))),kmul(gt23L,kmadd(ToReal(4),kmul(cdphi1,kmul(cdphi3,gtu13)),kmadd(cdphi222,gtu22,kmadd(cdphi233,gtu33,kmadd(gtu11,kmadd(ToReal(2),kmul(cdphi1,cdphi1),cdphi211),kmul(ToReal(2),kmadd(cdphi212,gtu12,kmadd(cdphi213,gtu13,kmadd(cdphi223,gtu23,kmadd(gtu22,kmul(cdphi2,cdphi2),kmul(gtu33,kmul(cdphi3,cdphi3)))))))))))))),ToReal(-2));
    
    CCTK_REAL_VEC Rphi33 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kadd(cdphi233,kmadd(gt33L,kmadd(kmadd(ToReal(4),kmul(cdphi1,cdphi2),kmul(ToReal(2),cdphi212)),gtu12,kmadd(cdphi222,gtu22,kmadd(ToReal(4),kmul(cdphi3,kmadd(cdphi1,gtu13,kmul(cdphi2,gtu23))),kmadd(cdphi233,gtu33,kmadd(gtu11,kmadd(ToReal(2),kmul(cdphi1,cdphi1),cdphi211),kmul(ToReal(2),kmadd(cdphi213,gtu13,kmadd(cdphi223,gtu23,kmul(gtu22,kmul(cdphi2,cdphi2)))))))))),kmul(ToReal(2),kmul(kmadd(gt33L,gtu33,ToReal(-1)),kmul(cdphi3,cdphi3))))),ToReal(-2));
    
    CCTK_REAL_VEC Atm11 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At11L,gtu11,kmadd(At12L,gtu12,kmul(At13L,gtu13)));
    
    CCTK_REAL_VEC Atm21 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At11L,gtu12,kmadd(At12L,gtu22,kmul(At13L,gtu23)));
    
    CCTK_REAL_VEC Atm31 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At11L,gtu13,kmadd(At12L,gtu23,kmul(At13L,gtu33)));
    
    CCTK_REAL_VEC Atm12 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At12L,gtu11,kmadd(At22L,gtu12,kmul(At23L,gtu13)));
    
    CCTK_REAL_VEC Atm22 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At12L,gtu12,kmadd(At22L,gtu22,kmul(At23L,gtu23)));
    
    CCTK_REAL_VEC Atm32 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At12L,gtu13,kmadd(At22L,gtu23,kmul(At23L,gtu33)));
    
    CCTK_REAL_VEC Atm13 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At13L,gtu11,kmadd(At23L,gtu12,kmul(At33L,gtu13)));
    
    CCTK_REAL_VEC Atm23 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At13L,gtu12,kmadd(At23L,gtu22,kmul(At33L,gtu23)));
    
    CCTK_REAL_VEC Atm33 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At13L,gtu13,kmadd(At23L,gtu23,kmul(At33L,gtu33)));
    
    CCTK_REAL_VEC Atu11 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Atm11,gtu11,kmadd(Atm12,gtu12,kmul(Atm13,gtu13)));
    
    CCTK_REAL_VEC Atu12 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Atm11,gtu12,kmadd(Atm12,gtu22,kmul(Atm13,gtu23)));
    
    CCTK_REAL_VEC Atu13 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Atm11,gtu13,kmadd(Atm12,gtu23,kmul(Atm13,gtu33)));
    
    CCTK_REAL_VEC Atu22 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Atm21,gtu12,kmadd(Atm22,gtu22,kmul(Atm23,gtu23)));
    
    CCTK_REAL_VEC Atu23 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Atm21,gtu13,kmadd(Atm22,gtu23,kmul(Atm23,gtu33)));
    
    CCTK_REAL_VEC Atu33 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Atm31,gtu13,kmadd(Atm32,gtu23,kmul(Atm33,gtu33)));
    
    CCTK_REAL_VEC e4phi CCTK_ATTRIBUTE_UNUSED = 
      IfThen(conformalMethod,kdiv(ToReal(1),kmul(phiL,phiL)),kexp(kmul(phiL,ToReal(4))));
    
    CCTK_REAL_VEC em4phi CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),e4phi);
    
    CCTK_REAL_VEC g11 CCTK_ATTRIBUTE_UNUSED = kmul(gt11L,e4phi);
    
    CCTK_REAL_VEC g12 CCTK_ATTRIBUTE_UNUSED = kmul(gt12L,e4phi);
    
    CCTK_REAL_VEC g13 CCTK_ATTRIBUTE_UNUSED = kmul(gt13L,e4phi);
    
    CCTK_REAL_VEC g22 CCTK_ATTRIBUTE_UNUSED = kmul(gt22L,e4phi);
    
    CCTK_REAL_VEC g23 CCTK_ATTRIBUTE_UNUSED = kmul(gt23L,e4phi);
    
    CCTK_REAL_VEC g33 CCTK_ATTRIBUTE_UNUSED = kmul(gt33L,e4phi);
    
    CCTK_REAL_VEC gu11 CCTK_ATTRIBUTE_UNUSED = kmul(em4phi,gtu11);
    
    CCTK_REAL_VEC gu12 CCTK_ATTRIBUTE_UNUSED = kmul(em4phi,gtu12);
    
    CCTK_REAL_VEC gu13 CCTK_ATTRIBUTE_UNUSED = kmul(em4phi,gtu13);
    
    CCTK_REAL_VEC gu22 CCTK_ATTRIBUTE_UNUSED = kmul(em4phi,gtu22);
    
    CCTK_REAL_VEC gu23 CCTK_ATTRIBUTE_UNUSED = kmul(em4phi,gtu23);
    
    CCTK_REAL_VEC gu33 CCTK_ATTRIBUTE_UNUSED = kmul(em4phi,gtu33);
    
    CCTK_REAL_VEC R11 CCTK_ATTRIBUTE_UNUSED = kadd(Rphi11,Rt11);
    
    CCTK_REAL_VEC R12 CCTK_ATTRIBUTE_UNUSED = kadd(Rphi12,Rt12);
    
    CCTK_REAL_VEC R13 CCTK_ATTRIBUTE_UNUSED = kadd(Rphi13,Rt13);
    
    CCTK_REAL_VEC R22 CCTK_ATTRIBUTE_UNUSED = kadd(Rphi22,Rt22);
    
    CCTK_REAL_VEC R23 CCTK_ATTRIBUTE_UNUSED = kadd(Rphi23,Rt23);
    
    CCTK_REAL_VEC R33 CCTK_ATTRIBUTE_UNUSED = kadd(Rphi33,Rt33);
    
    CCTK_REAL_VEC phirhsL CCTK_ATTRIBUTE_UNUSED = 
      IfThen(conformalMethod,kmul(phiL,kmadd(ToReal(0.333333333333333333333333333333),kmul(alphaL,trKL),kmul(kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3)),ToReal(-0.333333333333333333333333333333)))),kmadd(ToReal(-0.166666666666666666666666666667),kmul(alphaL,trKL),kmul(kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3)),ToReal(0.166666666666666666666666666667))));
    
    CCTK_REAL_VEC gt11rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmadd(ToReal(-2),kmul(alphaL,At11L),kmadd(ToReal(2),kmadd(gt11L,PDstandardNth1beta1,kmadd(gt12L,PDstandardNth1beta2,kmul(gt13L,PDstandardNth1beta3))),kmul(kmul(gt11L,kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3))),ToReal(-0.666666666666666666666666666667))));
    
    CCTK_REAL_VEC gt12rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(ToReal(-6),kmul(alphaL,At12L),kmadd(ToReal(3),kmadd(gt22L,PDstandardNth1beta2,kmadd(gt23L,PDstandardNth1beta3,kmadd(gt11L,PDstandardNth2beta1,kmul(gt13L,PDstandardNth2beta3)))),kmul(gt12L,kadd(PDstandardNth1beta1,kmadd(ToReal(-2),PDstandardNth3beta3,PDstandardNth2beta2))))),ToReal(0.333333333333333333333333333333));
    
    CCTK_REAL_VEC gt13rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(ToReal(-6),kmul(alphaL,At13L),kmadd(ToReal(3),kmadd(gt23L,PDstandardNth1beta2,kmadd(gt33L,PDstandardNth1beta3,kmadd(gt11L,PDstandardNth3beta1,kmul(gt12L,PDstandardNth3beta2)))),kmul(gt13L,kadd(PDstandardNth1beta1,kmadd(ToReal(-2),PDstandardNth2beta2,PDstandardNth3beta3))))),ToReal(0.333333333333333333333333333333));
    
    CCTK_REAL_VEC gt22rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmadd(ToReal(-2),kmul(alphaL,At22L),kmadd(ToReal(2),kmadd(gt12L,PDstandardNth2beta1,kmadd(gt22L,PDstandardNth2beta2,kmul(gt23L,PDstandardNth2beta3))),kmul(kmul(gt22L,kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3))),ToReal(-0.666666666666666666666666666667))));
    
    CCTK_REAL_VEC gt23rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(ToReal(-6),kmul(alphaL,At23L),kmadd(ToReal(3),kmadd(gt13L,PDstandardNth2beta1,kmadd(gt33L,PDstandardNth2beta3,kmadd(gt12L,PDstandardNth3beta1,kmul(gt22L,PDstandardNth3beta2)))),kmul(gt23L,kmadd(ToReal(-2),PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3))))),ToReal(0.333333333333333333333333333333));
    
    CCTK_REAL_VEC gt33rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmadd(ToReal(-2),kmul(alphaL,At33L),kmadd(ToReal(-0.666666666666666666666666666667),kmul(gt33L,kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3))),kmul(kmadd(gt13L,PDstandardNth3beta1,kmadd(gt23L,PDstandardNth3beta2,kmul(gt33L,PDstandardNth3beta3))),ToReal(2))));
    
    CCTK_REAL_VEC dotXt1 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gtu11,PDstandardNth11beta1,kmadd(gtu22,PDstandardNth22beta1,kmadd(gtu33,PDstandardNth33beta1,kmadd(ToReal(0.333333333333333333333333333333),kmadd(gtu11,kadd(PDstandardNth11beta1,kadd(PDstandardNth12beta2,PDstandardNth13beta3)),kmadd(gtu12,kadd(PDstandardNth12beta1,kadd(PDstandardNth22beta2,PDstandardNth23beta3)),kmul(gtu13,kadd(PDstandardNth13beta1,kadd(PDstandardNth23beta2,PDstandardNth33beta3))))),kmadd(ToReal(-2),kmadd(Atu11,PDstandardNth1alpha,kmadd(Atu12,PDstandardNth2alpha,kmul(Atu13,PDstandardNth3alpha))),kmadd(ToReal(2),kmadd(gtu12,PDstandardNth12beta1,kmadd(gtu13,PDstandardNth13beta1,kmadd(gtu23,PDstandardNth23beta1,kmul(alphaL,kmadd(ToReal(6),kmadd(Atu11,cdphi1,kmadd(Atu12,cdphi2,kmul(Atu13,cdphi3))),kmadd(Atu11,Gt111,kmadd(ToReal(2),kmul(Atu12,Gt112),kmadd(ToReal(2),kmul(Atu13,Gt113),kmadd(Atu22,Gt122,kmadd(ToReal(2),kmul(Atu23,Gt123),kmadd(Atu33,Gt133,kmul(kmadd(gtu11,PDstandardNth1trK,kmadd(gtu12,PDstandardNth2trK,kmul(gtu13,PDstandardNth3trK))),ToReal(-0.666666666666666666666666666667))))))))))))),kmsub(kmsub(ToReal(0.666666666666666666666666666667),kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3)),PDstandardNth1beta1),Xtn1,kmadd(PDstandardNth3beta1,Xtn3,kmul(PDstandardNth2beta1,Xtn2)))))))));
    
    CCTK_REAL_VEC dotXt2 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gtu11,PDstandardNth11beta2,kmadd(gtu22,PDstandardNth22beta2,kmadd(gtu33,PDstandardNth33beta2,kmadd(ToReal(0.333333333333333333333333333333),kmadd(gtu12,kadd(PDstandardNth11beta1,kadd(PDstandardNth12beta2,PDstandardNth13beta3)),kmadd(gtu22,kadd(PDstandardNth12beta1,kadd(PDstandardNth22beta2,PDstandardNth23beta3)),kmul(gtu23,kadd(PDstandardNth13beta1,kadd(PDstandardNth23beta2,PDstandardNth33beta3))))),kmadd(ToReal(-2),kmadd(Atu12,PDstandardNth1alpha,kmadd(Atu22,PDstandardNth2alpha,kmul(Atu23,PDstandardNth3alpha))),kmadd(ToReal(2),kmadd(gtu12,PDstandardNth12beta2,kmadd(gtu13,PDstandardNth13beta2,kmadd(gtu23,PDstandardNth23beta2,kmul(alphaL,kmadd(ToReal(6),kmadd(Atu12,cdphi1,kmadd(Atu22,cdphi2,kmul(Atu23,cdphi3))),kmadd(Atu11,Gt211,kmadd(ToReal(2),kmul(Atu12,Gt212),kmadd(ToReal(2),kmul(Atu13,Gt213),kmadd(Atu22,Gt222,kmadd(ToReal(2),kmul(Atu23,Gt223),kmadd(Atu33,Gt233,kmul(kmadd(gtu12,PDstandardNth1trK,kmadd(gtu22,PDstandardNth2trK,kmul(gtu23,PDstandardNth3trK))),ToReal(-0.666666666666666666666666666667))))))))))))),knmsub(PDstandardNth1beta2,Xtn1,kmsub(kmsub(ToReal(0.666666666666666666666666666667),kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3)),PDstandardNth2beta2),Xtn2,kmul(PDstandardNth3beta2,Xtn3)))))))));
    
    CCTK_REAL_VEC dotXt3 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gtu11,PDstandardNth11beta3,kmadd(gtu22,PDstandardNth22beta3,kmadd(gtu33,PDstandardNth33beta3,kmadd(ToReal(0.333333333333333333333333333333),kmadd(gtu13,kadd(PDstandardNth11beta1,kadd(PDstandardNth12beta2,PDstandardNth13beta3)),kmadd(gtu23,kadd(PDstandardNth12beta1,kadd(PDstandardNth22beta2,PDstandardNth23beta3)),kmul(gtu33,kadd(PDstandardNth13beta1,kadd(PDstandardNth23beta2,PDstandardNth33beta3))))),kmadd(ToReal(-2),kmadd(Atu13,PDstandardNth1alpha,kmadd(Atu23,PDstandardNth2alpha,kmul(Atu33,PDstandardNth3alpha))),kmadd(ToReal(2),kmadd(gtu12,PDstandardNth12beta3,kmadd(gtu13,PDstandardNth13beta3,kmadd(gtu23,PDstandardNth23beta3,kmul(alphaL,kmadd(ToReal(6),kmadd(Atu13,cdphi1,kmadd(Atu23,cdphi2,kmul(Atu33,cdphi3))),kmadd(Atu11,Gt311,kmadd(ToReal(2),kmul(Atu12,Gt312),kmadd(ToReal(2),kmul(Atu13,Gt313),kmadd(Atu22,Gt322,kmadd(ToReal(2),kmul(Atu23,Gt323),kmadd(Atu33,Gt333,kmul(kmadd(gtu13,PDstandardNth1trK,kmadd(gtu23,PDstandardNth2trK,kmul(gtu33,PDstandardNth3trK))),ToReal(-0.666666666666666666666666666667))))))))))))),knmsub(PDstandardNth1beta3,Xtn1,kmsub(kmsub(ToReal(0.666666666666666666666666666667),kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3)),PDstandardNth3beta3),Xtn3,kmul(PDstandardNth2beta3,Xtn2)))))))));
    
    CCTK_REAL_VEC Xt1rhsL CCTK_ATTRIBUTE_UNUSED = dotXt1;
    
    CCTK_REAL_VEC Xt2rhsL CCTK_ATTRIBUTE_UNUSED = dotXt2;
    
    CCTK_REAL_VEC Xt3rhsL CCTK_ATTRIBUTE_UNUSED = dotXt3;
    
    CCTK_REAL_VEC dottrK CCTK_ATTRIBUTE_UNUSED = 
      kmsub(alphaL,kmadd(ToReal(2),kmadd(Atm12,Atm21,kmadd(Atm13,Atm31,kmul(Atm23,Atm32))),kmadd(ToReal(0.333333333333333333333333333333),kmul(trKL,trKL),kmadd(Atm11,Atm11,kmadd(Atm22,Atm22,kmul(Atm33,Atm33))))),kmul(em4phi,kmadd(gtu11,PDstandardNth11alpha,kmadd(gtu22,PDstandardNth22alpha,kmadd(gtu33,kmadd(ToReal(2),kmul(cdphi3,PDstandardNth3alpha),PDstandardNth33alpha),kmadd(ToReal(2),kmadd(gtu12,PDstandardNth12alpha,kmadd(gtu13,kmadd(cdphi1,PDstandardNth3alpha,PDstandardNth13alpha),kmul(gtu23,kmadd(cdphi2,PDstandardNth3alpha,PDstandardNth23alpha)))),kmadd(PDstandardNth1alpha,kmsub(ToReal(2),kmadd(cdphi1,gtu11,kmadd(cdphi2,gtu12,kmul(cdphi3,gtu13))),Xtn1),kmsub(PDstandardNth2alpha,kmsub(ToReal(2),kmadd(cdphi1,gtu12,kmadd(cdphi2,gtu22,kmul(cdphi3,gtu23))),Xtn2),kmul(PDstandardNth3alpha,Xtn3)))))))));
    
    CCTK_REAL_VEC trKrhsL CCTK_ATTRIBUTE_UNUSED = dottrK;
    
    CCTK_REAL_VEC Ats11 CCTK_ATTRIBUTE_UNUSED = 
      ksub(kmadd(kmadd(ToReal(4),cdphi1,Gt111),PDstandardNth1alpha,kmadd(Gt211,PDstandardNth2alpha,kmadd(Gt311,PDstandardNth3alpha,kmul(alphaL,R11)))),PDstandardNth11alpha);
    
    CCTK_REAL_VEC Ats12 CCTK_ATTRIBUTE_UNUSED = 
      ksub(kmadd(kmadd(ToReal(2),cdphi2,Gt112),PDstandardNth1alpha,kmadd(kmadd(ToReal(2),cdphi1,Gt212),PDstandardNth2alpha,kmadd(Gt312,PDstandardNth3alpha,kmul(alphaL,R12)))),PDstandardNth12alpha);
    
    CCTK_REAL_VEC Ats13 CCTK_ATTRIBUTE_UNUSED = 
      ksub(kmadd(kmadd(ToReal(2),cdphi3,Gt113),PDstandardNth1alpha,kmadd(Gt213,PDstandardNth2alpha,kmadd(kmadd(ToReal(2),cdphi1,Gt313),PDstandardNth3alpha,kmul(alphaL,R13)))),PDstandardNth13alpha);
    
    CCTK_REAL_VEC Ats22 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gt122,PDstandardNth1alpha,ksub(kmadd(kmadd(ToReal(4),cdphi2,Gt222),PDstandardNth2alpha,kmadd(Gt322,PDstandardNth3alpha,kmul(alphaL,R22))),PDstandardNth22alpha));
    
    CCTK_REAL_VEC Ats23 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gt123,PDstandardNth1alpha,ksub(kmadd(kmadd(ToReal(2),cdphi3,Gt223),PDstandardNth2alpha,kmadd(kmadd(ToReal(2),cdphi2,Gt323),PDstandardNth3alpha,kmul(alphaL,R23))),PDstandardNth23alpha));
    
    CCTK_REAL_VEC Ats33 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gt133,PDstandardNth1alpha,kmadd(Gt233,PDstandardNth2alpha,ksub(kmadd(kmadd(ToReal(4),cdphi3,Gt333),PDstandardNth3alpha,kmul(alphaL,R33)),PDstandardNth33alpha)));
    
    CCTK_REAL_VEC trAts CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Ats11,gu11,kmadd(Ats22,gu22,kmadd(ToReal(2),kmadd(Ats12,gu12,kmadd(Ats13,gu13,kmul(Ats23,gu23))),kmul(Ats33,gu33))));
    
    CCTK_REAL_VEC At11rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmadd(alphaL,kmadd(At11L,trKL,kmul(kmadd(At11L,Atm11,kmadd(At12L,Atm21,kmul(At13L,Atm31))),ToReal(-2))),kmadd(ToReal(2),kmadd(At11L,PDstandardNth1beta1,kmadd(At12L,PDstandardNth1beta2,kmul(At13L,PDstandardNth1beta3))),kmadd(ToReal(-0.666666666666666666666666666667),kmul(At11L,kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3))),kmul(em4phi,kmadd(ToReal(-0.333333333333333333333333333333),kmul(g11,trAts),Ats11)))));
    
    CCTK_REAL_VEC At12rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmadd(alphaL,kmadd(At12L,trKL,kmul(kmadd(At11L,Atm12,kmadd(At12L,Atm22,kmul(At13L,Atm32))),ToReal(-2))),kmadd(At22L,PDstandardNth1beta2,kmadd(At23L,PDstandardNth1beta3,kmadd(At11L,PDstandardNth2beta1,kmadd(At13L,PDstandardNth2beta3,kmadd(At12L,kadd(PDstandardNth1beta1,kmadd(ToReal(-0.666666666666666666666666666667),kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3)),PDstandardNth2beta2)),kmul(em4phi,kmadd(ToReal(-0.333333333333333333333333333333),kmul(g12,trAts),Ats12))))))));
    
    CCTK_REAL_VEC At13rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmadd(alphaL,kmadd(At13L,trKL,kmul(kmadd(At11L,Atm13,kmadd(At12L,Atm23,kmul(At13L,Atm33))),ToReal(-2))),kmadd(At23L,PDstandardNth1beta2,kmadd(At33L,PDstandardNth1beta3,kmadd(At11L,PDstandardNth3beta1,kmadd(At12L,PDstandardNth3beta2,kmadd(At13L,kadd(PDstandardNth1beta1,kmadd(ToReal(-0.666666666666666666666666666667),kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3)),PDstandardNth3beta3)),kmul(em4phi,kmadd(ToReal(-0.333333333333333333333333333333),kmul(g13,trAts),Ats13))))))));
    
    CCTK_REAL_VEC At22rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmadd(alphaL,kmadd(At22L,trKL,kmul(kmadd(At12L,Atm12,kmadd(At22L,Atm22,kmul(At23L,Atm32))),ToReal(-2))),kmadd(ToReal(2),kmadd(At12L,PDstandardNth2beta1,kmadd(At22L,PDstandardNth2beta2,kmul(At23L,PDstandardNth2beta3))),kmadd(ToReal(-0.666666666666666666666666666667),kmul(At22L,kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3))),kmul(em4phi,kmadd(ToReal(-0.333333333333333333333333333333),kmul(g22,trAts),Ats22)))));
    
    CCTK_REAL_VEC At23rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmadd(alphaL,kmadd(At23L,trKL,kmul(kmadd(At12L,Atm13,kmadd(At22L,Atm23,kmul(At23L,Atm33))),ToReal(-2))),kmadd(At13L,PDstandardNth2beta1,kmadd(At33L,PDstandardNth2beta3,kmadd(At12L,PDstandardNth3beta1,kmadd(At22L,PDstandardNth3beta2,kmadd(At23L,kadd(PDstandardNth2beta2,kmadd(ToReal(-0.666666666666666666666666666667),kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3)),PDstandardNth3beta3)),kmul(em4phi,kmadd(ToReal(-0.333333333333333333333333333333),kmul(g23,trAts),Ats23))))))));
    
    CCTK_REAL_VEC At33rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmadd(alphaL,kmadd(At33L,trKL,kmul(kmadd(At13L,Atm13,kmadd(At23L,Atm23,kmul(At33L,Atm33))),ToReal(-2))),kmadd(ToReal(-0.666666666666666666666666666667),kmul(At33L,kadd(PDstandardNth1beta1,kadd(PDstandardNth2beta2,PDstandardNth3beta3))),kmadd(ToReal(2),kmadd(At13L,PDstandardNth3beta1,kmadd(At23L,PDstandardNth3beta2,kmul(At33L,PDstandardNth3beta3))),kmul(em4phi,kmadd(ToReal(-0.333333333333333333333333333333),kmul(g33,trAts),Ats33)))));
    
    CCTK_REAL_VEC alpharhsL CCTK_ATTRIBUTE_UNUSED = 
      kneg(kmul(kmul(kmadd(ToReal(1 - 
      LapseACoeff),kmadd(ToReal(AlphaDriver),kadd(ToReal(-1),alphaL),trKL),kmul(ToReal(LapseACoeff),AL)),kpow(alphaL,harmonicN)),ToReal(harmonicF)));
    
    CCTK_REAL_VEC ArhsL CCTK_ATTRIBUTE_UNUSED = 
      kmul(knmsub(ToReal(AlphaDriver),AL,dottrK),ToReal(LapseACoeff));
    
    CCTK_REAL_VEC eta CCTK_ATTRIBUTE_UNUSED = ToReal(1);
    
    CCTK_REAL_VEC theta CCTK_ATTRIBUTE_UNUSED = ToReal(1);
    
    CCTK_REAL_VEC beta1rhsL CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC beta2rhsL CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC beta3rhsL CCTK_ATTRIBUTE_UNUSED;
    
    if (harmonicShift)
    {
      beta1rhsL = 
        kmul(kmul(alphaL,kmul(phiL,kmadd(alphaL,kmadd(gtu11,kmadd(ToReal(2),kmadd(phiL,kmul(gtu23,PDstandardNth2gt13),PDstandardNth1phi),kmul(phiL,kmadd(gtu12,PDstandardNth2gt11,kmul(gtu13,PDstandardNth3gt11)))),kmadd(phiL,kmadd(ToReal(2),kmadd(gtu13,kmul(gtu22,PDstandardNth2gt23),kmul(gtu11,kmul(gtu33,PDstandardNth3gt13))),kmul(gtu33,kmsub(gtu13,PDstandardNth3gt33,kmul(gtu12,PDstandardNth2gt33)))),kmul(ToReal(2),kmadd(gtu12,kmadd(phiL,kmul(gtu23,PDstandardNth3gt22),PDstandardNth2phi),kmul(gtu13,PDstandardNth3phi))))),kmul(phiL,kmadd(ToReal(-2),kmul(gtu13,PDstandardNth3alpha),kmadd(gtu11,kmadd(ToReal(-2),kmadd(alphaL,kmul(gtu23,PDstandardNth1gt23),PDstandardNth1alpha),kmul(alphaL,knmsub(gtu22,PDstandardNth1gt22,kmsub(ToReal(2),kmadd(gtu12,PDstandardNth1gt12,kmul(gtu23,PDstandardNth3gt12)),kmul(gtu33,PDstandardNth1gt33))))),kmadd(gtu12,kmadd(ToReal(-2),PDstandardNth2alpha,kmul(alphaL,kmadd(gtu22,PDstandardNth2gt22,kmul(ToReal(2),kmul(gtu33,PDstandardNth3gt23))))),kmul(alphaL,kmadd(gtu13,kmadd(ToReal(4),kmul(gtu12,PDstandardNth1gt23),kmsub(ToReal(2),kmul(gtu23,PDstandardNth2gt33),kmul(gtu22,PDstandardNth3gt22))),kmadd(PDstandardNth1gt11,kmul(gtu11,gtu11),kmul(ToReal(2),kmadd(gtu11,kmadd(gtu13,PDstandardNth1gt13,kmul(gtu22,PDstandardNth2gt12)),kmadd(PDstandardNth1gt22,kmul(gtu12,gtu12),kmul(PDstandardNth1gt33,kmul(gtu13,gtu13))))))))))))))),ToReal(0.5));
      
      beta2rhsL = 
        kmul(kmul(alphaL,kmul(phiL,kmadd(ToReal(2),kmul(alphaL,kmadd(gtu12,kmadd(phiL,kmadd(gtu22,PDstandardNth2gt12,kmul(gtu13,PDstandardNth3gt11)),PDstandardNth1phi),kmadd(gtu22,kmadd(phiL,kmul(gtu33,PDstandardNth3gt23),PDstandardNth2phi),kmadd(gtu23,PDstandardNth3phi,kmul(phiL,kmadd(gtu22,kmadd(gtu23,PDstandardNth2gt23,kmul(gtu13,PDstandardNth3gt12)),kmul(PDstandardNth2gt11,kmul(gtu12,gtu12)))))))),kmul(phiL,kmadd(gtu12,kmadd(ToReal(-2),PDstandardNth1alpha,kmul(alphaL,kmadd(gtu11,PDstandardNth1gt11,kmadd(gtu22,PDstandardNth1gt22,kmadd(ToReal(4),kmul(gtu23,PDstandardNth2gt13),kmul(gtu33,kmsub(ToReal(2),PDstandardNth3gt13,PDstandardNth1gt33))))))),kmadd(gtu22,kmadd(ToReal(-2),kmadd(alphaL,kmul(gtu13,PDstandardNth2gt13),PDstandardNth2alpha),kmsub(alphaL,kmul(gtu23,PDstandardNth3gt22),kmul(alphaL,kmadd(gtu33,PDstandardNth2gt33,kmul(gtu11,PDstandardNth2gt11))))),kmadd(gtu23,kmadd(ToReal(-2),PDstandardNth3alpha,kmul(alphaL,kmul(gtu33,PDstandardNth3gt33))),kmul(alphaL,knmsub(gtu11,kmul(gtu23,PDstandardNth3gt11),kmadd(PDstandardNth2gt22,kmul(gtu22,gtu22),kmul(ToReal(2),kmadd(gtu11,kmadd(gtu22,PDstandardNth1gt12,kmul(gtu23,PDstandardNth1gt13)),kmadd(gtu13,kmadd(gtu22,PDstandardNth1gt23,kmul(gtu23,PDstandardNth1gt33)),kmul(PDstandardNth2gt33,kmul(gtu23,gtu23))))))))))))))),ToReal(0.5));
      
      beta3rhsL = 
        kmul(kmul(alphaL,kmul(phiL,kmadd(ToReal(2),kmul(alphaL,kmadd(gtu13,kmadd(phiL,kmadd(gtu12,PDstandardNth2gt11,kmadd(gtu22,PDstandardNth2gt12,kmul(gtu33,PDstandardNth3gt13))),PDstandardNth1phi),kmadd(gtu23,kmadd(phiL,kmul(gtu33,PDstandardNth3gt23),PDstandardNth2phi),kmadd(gtu33,PDstandardNth3phi,kmul(phiL,kmadd(gtu22,kmul(gtu33,PDstandardNth2gt23),kmul(PDstandardNth3gt11,kmul(gtu13,gtu13)))))))),kmul(phiL,kmadd(gtu23,kmadd(ToReal(-2),PDstandardNth2alpha,kmul(alphaL,knmsub(gtu11,PDstandardNth2gt11,kmadd(gtu22,PDstandardNth2gt22,kmul(gtu33,PDstandardNth2gt33))))),kmadd(gtu13,kmadd(ToReal(-2),PDstandardNth1alpha,kmul(alphaL,kmadd(gtu11,PDstandardNth1gt11,knmsub(gtu22,PDstandardNth1gt22,kmadd(gtu33,PDstandardNth1gt33,kmul(ToReal(4),kmul(gtu23,PDstandardNth3gt12))))))),kmadd(gtu33,kmsub(ToReal(-2),kmadd(alphaL,kmul(gtu12,PDstandardNth3gt12),PDstandardNth3alpha),kmul(alphaL,kmadd(gtu22,PDstandardNth3gt22,kmul(gtu11,PDstandardNth3gt11)))),kmul(alphaL,kmadd(ToReal(2),kmadd(gtu11,kmadd(gtu23,PDstandardNth1gt12,kmul(gtu33,PDstandardNth1gt13)),kmadd(gtu12,kmadd(gtu23,PDstandardNth1gt22,kmul(gtu33,kadd(PDstandardNth1gt23,PDstandardNth2gt13))),kmul(PDstandardNth3gt22,kmul(gtu23,gtu23)))),kmul(PDstandardNth3gt33,kmul(gtu33,gtu33))))))))))),ToReal(0.5));
    }
    else
    {
      beta1rhsL = 
        kmul(kmul(theta,kadd(Xt1L,kmadd(ToReal(BetaDriver),kmul(ToReal(-1 + 
        ShiftBCoeff),kmul(beta1L,eta)),kmul(ToReal(ShiftBCoeff),ksub(B1L,Xt1L))))),ToReal(ShiftGammaCoeff));
      
      beta2rhsL = 
        kmul(kmul(theta,kadd(Xt2L,kmadd(ToReal(BetaDriver),kmul(ToReal(-1 + 
        ShiftBCoeff),kmul(beta2L,eta)),kmul(ToReal(ShiftBCoeff),ksub(B2L,Xt2L))))),ToReal(ShiftGammaCoeff));
      
      beta3rhsL = 
        kmul(kmul(theta,kadd(Xt3L,kmadd(ToReal(BetaDriver),kmul(ToReal(-1 + 
        ShiftBCoeff),kmul(beta3L,eta)),kmul(ToReal(ShiftBCoeff),ksub(B3L,Xt3L))))),ToReal(ShiftGammaCoeff));
    }
    
    CCTK_REAL_VEC B1rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmul(knmsub(ToReal(BetaDriver),kmul(B1L,eta),dotXt1),ToReal(ShiftBCoeff));
    
    CCTK_REAL_VEC B2rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmul(knmsub(ToReal(BetaDriver),kmul(B2L,eta),dotXt2),ToReal(ShiftBCoeff));
    
    CCTK_REAL_VEC B3rhsL CCTK_ATTRIBUTE_UNUSED = 
      kmul(knmsub(ToReal(BetaDriver),kmul(B3L,eta),dotXt3),ToReal(ShiftBCoeff));
    /* Copy local copies back to grid functions */
    vec_store_partial_prepare(i,vecimin,vecimax);
    vec_store_nta_partial(alpharhs[index],alpharhsL);
    vec_store_nta_partial(Arhs[index],ArhsL);
    vec_store_nta_partial(At11rhs[index],At11rhsL);
    vec_store_nta_partial(At12rhs[index],At12rhsL);
    vec_store_nta_partial(At13rhs[index],At13rhsL);
    vec_store_nta_partial(At22rhs[index],At22rhsL);
    vec_store_nta_partial(At23rhs[index],At23rhsL);
    vec_store_nta_partial(At33rhs[index],At33rhsL);
    vec_store_nta_partial(B1rhs[index],B1rhsL);
    vec_store_nta_partial(B2rhs[index],B2rhsL);
    vec_store_nta_partial(B3rhs[index],B3rhsL);
    vec_store_nta_partial(beta1rhs[index],beta1rhsL);
    vec_store_nta_partial(beta2rhs[index],beta2rhsL);
    vec_store_nta_partial(beta3rhs[index],beta3rhsL);
    vec_store_nta_partial(gt11rhs[index],gt11rhsL);
    vec_store_nta_partial(gt12rhs[index],gt12rhsL);
    vec_store_nta_partial(gt13rhs[index],gt13rhsL);
    vec_store_nta_partial(gt22rhs[index],gt22rhsL);
    vec_store_nta_partial(gt23rhs[index],gt23rhsL);
    vec_store_nta_partial(gt33rhs[index],gt33rhsL);
    vec_store_nta_partial(phirhs[index],phirhsL);
    vec_store_nta_partial(trKrhs[index],trKrhsL);
    vec_store_nta_partial(Xt1rhs[index],Xt1rhsL);
    vec_store_nta_partial(Xt2rhs[index],Xt2rhsL);
    vec_store_nta_partial(Xt3rhs[index],Xt3rhsL);
  }
  CCTK_ENDLOOP3STR(ML_BSSN_Host_RHS);
}
extern "C" void ML_BSSN_Host_RHS(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_Host_RHS_Body");
  }
  if (cctk_iteration % ML_BSSN_Host_RHS_calc_every != ML_BSSN_Host_RHS_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_BSSN_Host::ML_curv",
    "ML_BSSN_Host::ML_curvrhs",
    "ML_BSSN_Host::ML_dtlapse",
    "ML_BSSN_Host::ML_dtlapserhs",
    "ML_BSSN_Host::ML_dtshift",
    "ML_BSSN_Host::ML_dtshiftrhs",
    "ML_BSSN_Host::ML_Gamma",
    "ML_BSSN_Host::ML_Gammarhs",
    "ML_BSSN_Host::ML_lapse",
    "ML_BSSN_Host::ML_lapserhs",
    "ML_BSSN_Host::ML_log_confac",
    "ML_BSSN_Host::ML_log_confacrhs",
    "ML_BSSN_Host::ML_metric",
    "ML_BSSN_Host::ML_metricrhs",
    "ML_BSSN_Host::ML_shift",
    "ML_BSSN_Host::ML_shiftrhs",
    "ML_BSSN_Host::ML_trace_curv",
    "ML_BSSN_Host::ML_trace_curvrhs"};
  AssertGroupStorage(cctkGH, "ML_BSSN_Host_RHS", 18, groups);
  
  EnsureStencilFits(cctkGH, "ML_BSSN_Host_RHS", 4, 4, 4);
  
  LoopOverInterior(cctkGH, ML_BSSN_Host_RHS_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_Host_RHS_Body");
  }
}

} // namespace ML_BSSN_Host
