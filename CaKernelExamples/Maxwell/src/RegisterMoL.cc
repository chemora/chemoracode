/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

extern "C" void Maxwell_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr = 0;
  
  /* Register all the evolved grid functions with MoL */
  ierr += MoLRegisterEvolved(CCTK_VarIndex("Maxwell::B1"),  CCTK_VarIndex("Maxwell::B1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("Maxwell::B2"),  CCTK_VarIndex("Maxwell::B2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("Maxwell::B3"),  CCTK_VarIndex("Maxwell::B3rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("Maxwell::El1"),  CCTK_VarIndex("Maxwell::El1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("Maxwell::El2"),  CCTK_VarIndex("Maxwell::El2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("Maxwell::El3"),  CCTK_VarIndex("Maxwell::El3rhs"));
  
  /* Register all the evolved Array functions with MoL */
  return;
}
