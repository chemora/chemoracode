#undef KRANC_DIFF_FUNCTIONS
#define KRANC_C
#include "Differencing.h"
#include "GenericFD.h"

#undef KRANC_GFOFFSET3D
#define KRANC_GFOFFSET3D(u,i,j,k) I3D(u,i,j,k)


/* Define macros used in calculations */
#define INITVALUE (42)
#define QAD(x) (SQR(SQR(x)))
#define INV(x) ((1.0) / (x))
#define SQR(x) ((x) * (x))
#define CUB(x) ((x) * (x) * (x))

CAKERNEL_EM_evol_Begin
  
  /* Include user-supplied include files */
  
  /* Initialise finite differencing variables */
  CCTK_REAL const dx = params.cagh_dx;
  CCTK_REAL const dy = params.cagh_dy;
  CCTK_REAL const dz = params.cagh_dz;
  CCTK_REAL const dt = params.cagh_dt;
  CCTK_REAL const t = params.cagh_time;
  CCTK_REAL const dxi = INV(dx);
  CCTK_REAL const dyi = INV(dy);
  CCTK_REAL const dzi = INV(dz);
  CCTK_REAL const khalf = 0.5;
  CCTK_REAL const kthird = 1/3.0;
  CCTK_REAL const ktwothird = 2.0/3.0;
  CCTK_REAL const kfourthird = 4.0/3.0;
  CCTK_REAL const keightthird = 8.0/3.0;
  CCTK_REAL const hdxi = 0.5 * dxi;
  CCTK_REAL const hdyi = 0.5 * dyi;
  CCTK_REAL const hdzi = 0.5 * dzi;
  
  /* Initialize predefined quantities */
  CCTK_REAL const p1o2dx = 0.5*INV(dx);
  CCTK_REAL const p1o2dy = 0.5*INV(dy);
  CCTK_REAL const p1o2dz = 0.5*INV(dz);
  CCTK_REAL const p1o4dxdy = 0.25*INV(dx*dy);
  CCTK_REAL const p1o4dxdz = 0.25*INV(dx*dz);
  CCTK_REAL const p1o4dydz = 0.25*INV(dy*dz);
  CCTK_REAL const p1odx2 = INV(SQR(dx));
  CCTK_REAL const p1ody2 = INV(SQR(dy));
  CCTK_REAL const p1odz2 = INV(SQR(dz));
  
  /* Assign local copies of arrays functions */
  
  
  
  /* Calculate temporaries and arrays functions */
  
  /* Copy local copies back to grid functions */
  CAKERNEL_EM_evol_Computations_Begin
    
    /* Assign local copies of grid functions */
    
    CCTK_REAL B1L = I3D(B1,0,0,0);
    CCTK_REAL B2L = I3D(B2,0,0,0);
    CCTK_REAL B3L = I3D(B3,0,0,0);
    CCTK_REAL El1L = I3D(El1,0,0,0);
    CCTK_REAL El2L = I3D(El2,0,0,0);
    CCTK_REAL El3L = I3D(El3,0,0,0);
    
    
    /* Include user supplied include files */
    
    /* Precompute derivatives */
    CCTK_REAL const PDstandard2B1 = PDstandard2(B1);
    CCTK_REAL const PDstandard3B1 = PDstandard3(B1);
    CCTK_REAL const PDstandard1B2 = PDstandard1(B2);
    CCTK_REAL const PDstandard3B2 = PDstandard3(B2);
    CCTK_REAL const PDstandard1B3 = PDstandard1(B3);
    CCTK_REAL const PDstandard2B3 = PDstandard2(B3);
    CCTK_REAL const PDstandard2El1 = PDstandard2(El1);
    CCTK_REAL const PDstandard3El1 = PDstandard3(El1);
    CCTK_REAL const PDstandard1El2 = PDstandard1(El2);
    CCTK_REAL const PDstandard3El2 = PDstandard3(El2);
    CCTK_REAL const PDstandard1El3 = PDstandard1(El3);
    CCTK_REAL const PDstandard2El3 = PDstandard2(El3);
    
    /* Calculate temporaries and grid functions */
    CCTK_REAL El1rhsL = PDstandard2B3 - PDstandard3B2;
    
    CCTK_REAL El2rhsL = -PDstandard1B3 + PDstandard3B1;
    
    CCTK_REAL El3rhsL = PDstandard1B2 - PDstandard2B1;
    
    CCTK_REAL B1rhsL = -PDstandard2El3 + PDstandard3El2;
    
    CCTK_REAL B2rhsL = PDstandard1El3 - PDstandard3El1;
    
    CCTK_REAL B3rhsL = -PDstandard1El2 + PDstandard2El1;
    
    /* Copy local copies back to grid functions */
    I3D(B1rhs,0,0,0) = B1rhsL;
    I3D(B2rhs,0,0,0) = B2rhsL;
    I3D(B3rhs,0,0,0) = B3rhsL;
    I3D(El1rhs,0,0,0) = El1rhsL;
    I3D(El2rhs,0,0,0) = El2rhsL;
    I3D(El3rhs,0,0,0) = El3rhsL;
    
  CAKERNEL_EM_evol_Computations_End
  
CAKERNEL_EM_evol_End
