#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <algorithm>
#include <cassert>
#include <cfloat>
#include <climits>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include <sys/time.h>

#if defined(__CUDACC__)
#  include <cuda.h>
#  include <cuda_runtime.h>
#elif defined(_OPENMP)
#  include <omp.h>
#endif



using namespace std;



namespace WaveToyCUDA {



  //////////////////////////////////////////////////////////////////////////////
  
  // Generic definitions
  
#define restrict __restrict__
#define UNUSED __attribute__((__unused__))
  
  
  
  //////////////////////////////////////////////////////////////////////////////
  
  // Use CUDA if available; otherwise, provide trivial work-arounds
#if defined(__CUDACC__)
  
#  define debug_assert(x) ((x) ? assert_error = __LINE__ : 0)
#  define debug_printf(...)
  
#else

#  define __constant__ const
#  define __device__
#  define __host__
  
#  ifdef _OPENMP
#    define __syncthreads() do { _Pragma("omp barrier"); } while(0)
#  else
#    define __syncthreads() (0)
#  endif
  
  struct dim3 {
    int x, y, z;
    dim3(int x_ = 1, int y_ = 1, int z_ = 1)
      : x(x_), y(y_), z(z_)
    {
    }
  };
  
  enum cudaError_t { cudaSuccess, cudaErrorUnknown };
  
  char const* cudaGetErrorString (cudaError_t const err)
  {
    return err ? "success" : "unknown error";
  }
  
  cudaError_t cudaMalloc (void ** const p, size_t const s)
  {
    *p = malloc(s);
    return *p or s==0 ? cudaSuccess : cudaErrorUnknown;
  }
  
  cudaError_t cudaFree (void * const p)
  {
    free(p);
    return cudaSuccess;
  }
  
  enum cudaMemcpyKind { cudaMemcpyHostToDevice, cudaMemcpyDeviceToHost };
  cudaError_t cudaMemcpy (void *restrict const dst,
                          void const *restrict const src,
                          size_t const s,
                          cudaMemcpyKind const kind)
  {
    memcpy (dst, src, s);
    return cudaSuccess;
  }
#  define cudaMemcpyToSymbol(dst,...)                                   \
  cudaMemcpyToSymbol1 (const_cast<void*>((void*)&(dst)), __VA_ARGS__)
  cudaError_t
  cudaMemcpyToSymbol1 (void *restrict const dst,
                       void const *restrict const src,
                       size_t const s,
                       size_t const offset = 0,
                       cudaMemcpyKind const kind = cudaMemcpyHostToDevice)
  {
    memcpy ((char*)dst + offset, src, s);
    return cudaSuccess;
  }
  
  cudaError_t cudaDeviceSynchronize ()
  {
    return cudaSuccess;
  }
  
  cudaError_t cudaGetDeviceCount (int *restrict const cnt)
  {
    *cnt = 1;
    return cudaSuccess;
  }
  
  struct cudaDeviceProp {
    int maxThreadsPerBlock;
    int maxThreadsDim[3];
    int maxGridSize[3];
    size_t totalGlobalMem;
    size_t sharedMemPerBlock;
  };
  
  cudaError_t cudaGetDeviceProperties (cudaDeviceProp *restrict const prop,
                                       int const idx)
  {
    if (idx!=0) return cudaErrorUnknown;
#ifdef _OPENMP
    prop->maxThreadsPerBlock = omp_get_max_threads();
#else
    prop->maxThreadsPerBlock = 1; // no multithreading
#endif
    prop->maxThreadsDim[0] = prop->maxThreadsPerBlock; // why not
    prop->maxThreadsDim[1] = prop->maxThreadsPerBlock;
    prop->maxThreadsDim[2] = prop->maxThreadsPerBlock;
    prop->maxGridSize[0] = INT_MAX; // arbitrary
    prop->maxGridSize[1] = INT_MAX;
    prop->maxGridSize[2] = INT_MAX;
    prop->totalGlobalMem = sizeof(size_t)<8 ? ULONG_MAX : ULLONG_MAX; // large
    prop->sharedMemPerBlock = prop->totalGlobalMem; // why not
    return cudaSuccess;
  }

#  define debug_assert(x)   assert(x)
#  define debug_printf(...) /*printf(__VA_ARGS__)*/
  
#endif
  
  
  
  // Provide additional convenience CUDA functions
  
  __device__ int assert_error = 0;
  
#define CUDA_CHECK(func)                                                \
  do {                                                                  \
    assert_error = 0;                                                   \
    cudaError_t const err = (func);                                     \
    if (err != cudaSuccess) {                                           \
      char const *const msg = cudaGetErrorString (err);                 \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING, \
                  "CUDA error: %s", msg);                               \
    }                                                                   \
    if (assert_error) {                                                 \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING, \
                  "CUDA assert error in line %d", assert_error);        \
    }                                                                   \
  } while(0)
  
  ostream& operator<< (ostream& os, dim3 const& dim)
  {
    return os << "[" << dim.x << "," << dim.y << "," << dim.z << "]";
  }
  
  template<typename T>
  T * restrict cudaNew (size_t const n = 1)
  {
    void * p;
    CUDA_CHECK(cudaMalloc (&p, n*sizeof(T)));
    return static_cast<T*>(p);
  }
  
  template<typename T>
  void cudaDelete (T * restrict const p)
  {
    CUDA_CHECK(cudaFree(p));
  }
  
#if 0
  // Permute CUDA's grid dimensions.
  // CUDA offers grid with a maximum size [*,*,1], whereas we prefer
  // [1,*,*] because this should be more cache efficient. We apply
  // these functions whenever we interact with cuda, i.e. when
  // examining device properties, when calling device functions, and
  // when using gridIdx and gridDim.
  
  // NOTE: This is an invasive change, and permuting indices
  // explicitly everywhere seems fragile. We don't do this yet, until
  // we can think of a better way.
  __host__ __device__ dim3 appGridDim (dim3 const& cudaGridDim)
  {
    return dim3(cudaGridDim.z, cudaGridDim.x, cudaGridDim.y);
  }
  __host__ __device__ dim3 cudaGridDim (dim3 const& appGridDim)
  {
    return dim3(cudaGridDim.y, cudaGridDim.z, cudaGridDim.x);
  }
#endif
  
  
  
  // Provide wrappers for GPU programming
  
#ifdef __CUDACC__
  
#  define CCTK_CUDACALL __global__
#  define CCTK_CUDA_EXECUTE(func, gridDim, blockDim, sharedSize, args)  \
  do {                                                                  \
    func <<<gridDim, blockDim, sharedSize>>> args;                      \
    CUDA_CHECK(cudaDeviceSynchronize());                                \
  } while(0)
  
#  define CCTK_CUDA_DECLARE_SHARED(type,var) extern __shared__ type var[];
  
#else
  
#  define CCTK_CUDACALL
  
  // Define the global (but thread-local) state variables
#ifdef _OPENMP
  dim3 *restrict threadIdxArr = NULL;
  extern dim3 *restrict threadIdxPtr;
#  pragma omp threadprivate(threadIdxPtr)
  dim3 *restrict threadIdxPtr = NULL;
#  define threadIdx (*threadIdxPtr)
#else
  dim3 threadIdx;
#endif
  dim3 blockIdx;
  dim3 blockDim;
  dim3 gridDim;
  int const warpSize = 1;       // aka vector size
  
  void *restrict cctk_shared_ptr  = NULL;
  size_t         cctk_shared_size = 0;
  
#  ifdef _OPENMP
  
  // Execute the blocks sequentially and the threads in parallel
  // TODO: distinguish between "close" and "far-apart" threads, and
  // execute blocks on far-apart threads (e.g. hyperthreading vs.
  // different cores, or same socket vs. different socket)
#  define CCTK_CUDA_EXECUTE(func, gridDim_, blockDim_, sharedSize_, args) \
  do {                                                                  \
    assert (not cctk_shared_ptr);                                       \
    cctk_shared_size = (sharedSize_);                                   \
    cctk_shared_ptr = malloc(cctk_shared_size);                         \
    gridDim = (gridDim_);                                               \
    blockDim = (blockDim_);                                             \
    int const num_threads = blockDim.x * blockDim.y * blockDim.z;       \
    assert (not threadIdxArr);                                          \
    threadIdxArr = new dim3[num_threads];                               \
    omp_set_num_threads(num_threads);                                   \
    for (blockIdx.z = 0; blockIdx.z < gridDim.z; ++blockIdx.z) {        \
      for (blockIdx.y = 0; blockIdx.y < gridDim.y; ++blockIdx.y) {      \
        for (blockIdx.x = 0; blockIdx.x < gridDim.x; ++blockIdx.x) {    \
          _Pragma("omp parallel")                                       \
          {                                                             \
            int thread_num = omp_get_thread_num();                      \
            threadIdxPtr = &threadIdxArr[thread_num];                   \
            threadIdx.x = thread_num % blockDim.x; thread_num /= blockDim.x; \
            threadIdx.y = thread_num % blockDim.y; thread_num /= blockDim.y; \
            threadIdx.z = thread_num % blockDim.z; thread_num /= blockDim.z; \
            assert (thread_num==0);                                     \
            func args;                                                  \
          }                                                             \
        }                                                               \
      }                                                                 \
    }                                                                   \
    omp_set_num_threads(omp_get_max_threads());                         \
    delete[] threadIdxArr;                                              \
    threadIdxArr = NULL;                                                \
    cctk_shared_size = 0;                                               \
    free (cctk_shared_ptr);                                             \
    cctk_shared_ptr = NULL;                                             \
  } while(0)

#  else
  
  // Execute the blocks sequentially; there cannot be any threads
#  define CCTK_CUDA_EXECUTE(func, gridDim_, blockDim_, sharedSize_, args) \
  do {                                                                  \
    assert (not cctk_shared_ptr);                                       \
    cctk_shared_size = (sharedSize_);                                   \
    cctk_shared_ptr = malloc(cctk_shared_size);                         \
    gridDim = (gridDim_);                                               \
    blockDim = (blockDim_);                                             \
    int const num_threads = blockDim.x * blockDim.y * blockDim.z;       \
    assert (num_threads == 1);                                          \
    threadIdx = dim3(0,0,0);   /* there is only one thread */           \
    for (blockIdx.z = 0; blockIdx.z < gridDim.z; ++blockIdx.z) {        \
      for (blockIdx.y = 0; blockIdx.y < gridDim.y; ++blockIdx.y) {      \
        for (blockIdx.x = 0; blockIdx.x < gridDim.x; ++blockIdx.x) {    \
          func args;                                                    \
        }                                                               \
      }                                                                 \
    }                                                                   \
    cctk_shared_size = 0;                                               \
    free (cctk_shared_ptr);                                             \
    cctk_shared_ptr = NULL;                                             \
  } while(0)
  
#  endif
  
#  define CCTK_CUDA_DECLARE_SHARED(type,var)            \
  type *restrict const var = (type*)cctk_shared_ptr;
  
#endif
  
  
  
  //////////////////////////////////////////////////////////////////////////////
  
  // Generic helpers
  
  // Determine good memory access alignment (in bytes). This is the
  // alignment that the first thread in a warp should use.
  __host__ __device__ int getAlignment (int const s)
  {
    // This is true on current hardware
    switch (s) {
    case  4: return  64;
    case  8: return 128;
    case 16: return 256;
    default: return   1;        // this memory access will be slow
    }
  }
  
  // Integer division that rounds up
  __host__ __device__ int divup (int const a, int const b)
  {
    return (a+b-1)/b;
  }
  
  // Find the next higher power of 2; taken from
  // <http://en.wikipedia.org/wiki/Power_of_two>
  __host__ __device__ int nexthigher (int k)
  {
    --k;
    for (unsigned i=1; i<sizeof(int)*CHAR_BIT; i<<=1)
      k |= k >> i;
    return k+1;
  }  
  
  __host__ __device__ int nextlower (int const k)
  {
    debug_assert (k>0);
    return nexthigher(k-1) >> 1;
  }  
  
  // Align a to a multiple of b (downwards, i.e. decreasing a)
  __host__ __device__ int align (int const a, int const b)
  {
    debug_assert (b == nexthigher(b));
    return a & - b;
  }
  
  // Align a to a multiple of b upwards (i.e. increasing a)
  __host__ __device__ int alignup (int const a, int const b)
  {
    return align(a + b-1, b);
  }
  
  template<typename T>
  __host__ __device__
  T min3 (T const& x0, T const& x1, T const& x2)
  {
    return min(min(x0, x1), x2);
  }
  
  template<typename T>
  __host__ __device__
  T min4 (T const& x0, T const& x1, T const& x2, T const& x3)
  {
    return min(min(min(x0, x1), x2), x3);
  }
  
  // Get the current time
  double gettime ()
  {
    struct timeval tv;
    gettimeofday (&tv, NULL);
    return tv.tv_sec + 1.0e-6*tv.tv_usec;
  }
  
  
  
  
  //////////////////////////////////////////////////////////////////////////////
  
  // An abstraction for regions, slabs, and iterating over them
  
  class reg_t {
    static int const D = 3;
    int dim[D];
    int idx[D];
    bool invariant() const
    {
      for (int d=0; d<D; ++d) {
        if (dim[d]<0) return false;
        if (idx[d]<0 or idx[d]>=dim[d]) return false;
      }
      return true;
    }
    static dim3 vectToDim(int const vec[D])
    {
      return dim3(vec[0],vec[1],vec[2]);
    }
    static void setFromDim(int vec[D], dim3 const& dim)
    {
      vec[0]=dim.x;
      vec[1]=dim.y;
      vec[2]=dim.z;
    }
  public:
    int index() const { return idx[0]+dim[0]*(idx[1]+dim[1]*idx[2]); }
    reg_t ()
    {
      for (int d=0; d<3; ++d) {
        dim[d] = 0;
        idx[d] = 0;
      }
      assert (invariant());
    }
    reg_t (int const dim_[3])
    {
      for (int d=0; d<3; ++d) {
        dim[d] = dim_[d];
        idx[d] = 0;
      }
      assert (invariant());
    }
    reg_t (dim3 const& dim_)
    {
      setFromDim(dim, dim_);
      for (int d=0; d<3; ++d) {
        idx[d] = 0;
      }
      assert (invariant());
    }
    reg_t (int const dim_[3], int const idx_[3])
    {
      for (int d=0; d<3; ++d) {
        dim[d] = dim_[d];
        idx[d] = idx_[d];
      }
      assert (invariant());
    }
    reg_t (dim3 const& dim_, dim3 const idx_)
    {
      setFromDim(dim, dim_);
      setFromDim(idx, idx_);
      assert (invariant());
    }
    reg_t (reg_t const& outer, reg_t const& inner)
    {
      for (int d=0; d<3; ++d) {
        dim[d] = outer.dim[d]*inner.dim[d];
        idx[d] = outer.idx[d]*inner.dim[d] + inner.idx[d];
      }
      assert (invariant());
    }
  };
  
  struct slab_t {
    reg_t reg;
    reg_t act;
  };
  


  //////////////////////////////////////////////////////////////////////////////
  
  // Define an abstraction for iterating over 3D arrays. This uses
  // CUDA's threads, blocks, and grids, and introduces super-blocks
  // and super-grids for added flexibility. Super-block iterations
  // occur on the device, super-grid iterations occur on the host.
  
  // Unfortunately, CUDA uses a strange naming scheme, where the
  // number of threads is called blockDim, and the number of blocks is
  // called gridDim. This gives the term "block" an ambiguous meaning.
  // This is confusing in loops, where e.g. threadIdx loops up to
  // blockDim, blockidx loops up to gridDim etc., so that loop
  // counters and loop bounds have different names.
  
  cudaDeviceProp devProp;
  
  struct loop_t {
    // dim3 threadDim;     // [always 1]
    dim3 blockDim;         // [also provided by CUDA]
    dim3 superBlockDim;
    dim3 gridDim;
    dim3 superGridDim;
    // dim3 threadIdx;     // [provided by CUDA]
    // dim3 subBlockIdx;   // [iterated in the loop]
    // dim3 blockIdx;      // [provided by CUDA]
    dim3 subGridIdx;
    // dim3 gridIdx;       // [always 0]
    
    // Invariant:
    //    threadIdx   < blockDim
    //    subBlockIdx < superBlockDim
    //    blockIdx    < gridDim
    //    subGridIdx  < superGridDim
    
    __host__ __device__
    dim3 Idx (dim3 const& my_subGridIdx,
              dim3 const& my_blockIdx,
              dim3 const& my_subBlockIdx,
              dim3 const& my_threadIdx) const
    {
      // return (my_threadIdx + blockDim *
      //         (my_subBlockIdx + superBlockDim *
      //          (my_blockIdx + gridDim * my_subGridIdx)));
      return dim3 (my_threadIdx.x + blockDim.x *
                   (my_subBlockIdx.x + superBlockDim.x *
                    (my_blockIdx.x + gridDim.x * my_subGridIdx.x)),
                   my_threadIdx.y + blockDim.y *
                   (my_subBlockIdx.y + superBlockDim.y *
                    (my_blockIdx.y + gridDim.y * my_subGridIdx.y)),
                   my_threadIdx.z + blockDim.z *
                   (my_subBlockIdx.z + superBlockDim.z *
                    (my_blockIdx.z + gridDim.z * my_subGridIdx.z)));
    }
    
    loop_t (CCTK_ARGUMENTS)
    {
      DECLARE_CCTK_ARGUMENTS;
      DECLARE_CCTK_PARAMETERS;
      
      int devCnt;
      CUDA_CHECK (cudaGetDeviceCount (&devCnt));
      int const devIdx = 0;
      if (devIdx >= devCnt) {
        CCTK_WARN (CCTK_WARN_ABORT, "Not enough CUDA devices present");
      }
      CUDA_CHECK (cudaGetDeviceProperties (&devProp, devIdx));
      
      // Choose the block size
      int const bdx =
        min3(cctk_lsh[0], blockDim_x, devProp.maxThreadsDim[0]);
      int const bdy =
        min4(cctk_lsh[1], blockDim_y, devProp.maxThreadsDim[1],
            devProp.maxThreadsPerBlock/bdx);
      int const bdz =
        min4(cctk_lsh[2], blockDim_z, devProp.maxThreadsDim[2],
            devProp.maxThreadsPerBlock/(bdx*bdy));
      assert (bdx*bdy*bdz <= devProp.maxThreadsPerBlock);
      assert (bdx*bdy*bdz > 0);
      blockDim = dim3(bdx,bdy,bdz);
      
      // Choose the super-block size
      superBlockDim =
        dim3(min(divup(cctk_lsh[0], blockDim.x),
                 divup(superBlockDim_x, blockDim.x)),
             min(divup(cctk_lsh[1], blockDim.y),
                 divup(superBlockDim_y, blockDim.y)),
             min(divup(cctk_lsh[2], blockDim.z),
                 divup(superBlockDim_z, blockDim.z)));
      assert (superBlockDim.x * superBlockDim.y * superBlockDim.z > 0);
      
      // Choose the grid size
      int const gdx =
        min3(divup(cctk_lsh[0], blockDim.x * superBlockDim.x),
             gridDim_x, devProp.maxGridSize[0]);
      int const gdy =
        min3(divup(cctk_lsh[1], blockDim.y * superBlockDim.y),
             gridDim_y, devProp.maxGridSize[1]);
      int const gdz =
        min3(divup(cctk_lsh[2], blockDim.z * superBlockDim.z),
             gridDim_z, devProp.maxGridSize[2]);
      assert (gdx*gdy*gdz > 0);
      gridDim = dim3(gdx,gdy,gdz);
      
      // Calculate the super-grid size
      superGridDim =
        dim3(divup(cctk_lsh[0], blockDim.x * superBlockDim.x * gridDim.x),
             divup(cctk_lsh[1], blockDim.y * superBlockDim.y * gridDim.y),
             divup(cctk_lsh[2], blockDim.z * superBlockDim.z * gridDim.z));
      assert (superGridDim.x * superGridDim.y * superGridDim.z > 0);
      
      assert (blockDim.x * superBlockDim.x * gridDim.x * superGridDim.x >=
              cctk_lsh[0]);
      assert (blockDim.y * superBlockDim.y * gridDim.y * superGridDim.y >=
              cctk_lsh[1]);
      assert (blockDim.z * superBlockDim.z * gridDim.z * superGridDim.z >=
              cctk_lsh[2]);
    }
    
    friend ostream& operator<< (ostream& os, loop_t const& loop);
  };
  
  ostream& operator<< (ostream& os, loop_t const& loop)
  {
    dim3 const
      total (loop.blockDim.x * loop.superBlockDim.x *
             loop.gridDim.x * loop.superGridDim.x,
             loop.blockDim.y * loop.superBlockDim.y *
             loop.gridDim.y * loop.superGridDim.y,
             loop.blockDim.z * loop.superBlockDim.z *
             loop.gridDim.z * loop.superGridDim.z);
    return os << "loop_t{\n"
              << "   blockDim=" << loop.blockDim << ",\n"
              << "   superBlockDim=" << loop.superBlockDim << ",\n"
              << "   gridDim=" << loop.gridDim << ",\n"
              << "   superGridDim=" << loop.superGridDim << ",\n"
              << "   total=" << total << "\n"
              << "}";
  }
  
  // Iterator for calling device functions
#define CCTK_CUDA_ITERATE(func, loop, sharedSize, args)                 \
  do {                                                                  \
    for (loop.subGridIdx.z = 0;                                         \
         loop.subGridIdx.z < loop.superGridDim.z;                       \
         ++loop.subGridIdx.z)                                           \
    {                                                                   \
      for (loop.subGridIdx.y = 0;                                       \
           loop.subGridIdx.y < loop.superGridDim.y;                     \
           ++loop.subGridIdx.y)                                         \
      {                                                                 \
        for (loop.subGridIdx.x = 0;                                     \
             loop.subGridIdx.x < loop.superGridDim.x;                   \
             ++loop.subGridIdx.x)                                       \
        {                                                               \
          CCTK_CUDA_EXECUTE                                             \
            (func, loop.gridDim, loop.blockDim, sharedSize, args);      \
        }                                                               \
      }                                                                 \
    }                                                                   \
  } while (0)
  
  // Iterator for inside the device functions
  // TODO: move some of the index calculations to the host
  // TODO: make gridDim a template?
  // TODO: allow permuting of grid indices (via a template?)
  // TODO: distinguish between warps and vector-elements, and have an
  // earlier if statement for warps
#define DECLARE_CCTK_LOOP                                       \
  loop_t cctk_loop(CCTK_PASS_CTOC);                             \
                                                                \
  int const& istp = cctk_loop.blockDim.x;                       \
  int const& jstp = cctk_loop.blockDim.y;                       \
  int const& kstp = cctk_loop.blockDim.z;                       \
  int const& icnt = cctk_loop.superBlockDim.x;                  \
  int const& jcnt = cctk_loop.superBlockDim.y;                  \
  int const& kcnt = cctk_loop.superBlockDim.z;                  \
                                                                \
  int const shared_ni = istp * icnt + 2*cctk_nghostzones[0];    \
  int const shared_nj = jstp * jcnt + 2*cctk_nghostzones[1];    \
  int const shared_nk = kstp * kcnt + 2*cctk_nghostzones[2];    \
  int const shared_di = 1;                                      \
  int const shared_dj = shared_di * shared_ni;                  \
  int const shared_dk = shared_dj * shared_nj;                  \
  int const shared_np = shared_dk * shared_nk;

  // Iterate over a region
#define CCTK_CUDA_LOOP(i,j,k, ind, shared_ind)                          \
  int const imin = (0 + blockDim.x *                                    \
                    (0 + cctk_loop.superBlockDim.x *                    \
                     (blockIdx.x + gridDim.x *                          \
                      cctk_loop.subGridIdx.x)));                        \
  int const jmin = (0 + blockDim.y *                                    \
                    (0 + cctk_loop.superBlockDim.y *                    \
                     (blockIdx.y + gridDim.y *                          \
                      cctk_loop.subGridIdx.y)));                        \
  int const kmin = (0 + blockDim.z *                                    \
                    (0 + cctk_loop.superBlockDim.z *                    \
                     (blockIdx.z + gridDim.z *                          \
                      cctk_loop.subGridIdx.z)));                        \
  int const& istp = blockDim.x;                                         \
  int const& jstp = blockDim.y;                                         \
  int const& kstp = blockDim.z;                                         \
  int const& icnt = cctk_loop.superBlockDim.x;                          \
  int const& jcnt = cctk_loop.superBlockDim.y;                          \
  int const& kcnt = cctk_loop.superBlockDim.z;                          \
  int const& ioff = threadIdx.x;                                        \
  int const& joff = threadIdx.y;                                        \
  int const& koff = threadIdx.z;                                        \
  int const imax = imin + icnt * istp;                                  \
  int const jmax = jmin + jcnt * jstp;                                  \
  int const kmax = kmin + kcnt * kstp;                                  \
                                                                        \
  int const shared_imin = max(imin - nghostsi, 0);                      \
  int const shared_jmin = max(jmin - nghostsj, 0);                      \
  int const shared_kmin = max(kmin - nghostsk, 0);                      \
  int const shared_imax = min(imax + nghostsi, ni);                     \
  int const shared_jmax = min(jmax + nghostsj, nj);                     \
  int const shared_kmax = min(kmax + nghostsk, nk);                     \
                                                                        \
  int const shared_ni = shared_imax - shared_imin;                      \
  int const shared_nj = shared_jmax - shared_jmin;                      \
  int const shared_nk = shared_kmax - shared_kmin;                      \
  int const shared_di = 1;                                              \
  int const shared_dj = shared_di * shared_ni;                          \
  int const shared_dk = shared_dj * shared_nj;                          \
  int const shared_np = shared_dk * shared_nk;                          \
                                                                        \
  do {                                                                  \
    for (int sbk=0; sbk<kcnt; ++sbk) {                                  \
      int const k = kmin + kstp * sbk + koff;                           \
      for (int sbj=0; sbj<jcnt; ++sbj) {                                \
        int const j = jmin + jstp * sbj + joff;                         \
        for (int sbi=0; sbi<icnt; ++sbi) {                              \
          int const i = imin + istp * sbi + ioff;                       \
          if (i<ni and j<nj and k<nk) {                                 \
            dim3 const subBlockIdx UNUSED (sbi, sbj, sbk);              \
            int const ind = di*i + dj*j + dk*k;                         \
            int const shared_i = i - shared_imin;                       \
            int const shared_j = j - shared_jmin;                       \
            int const shared_k = k - shared_kmin;                       \
            debug_assert (shared_i >= 0 and shared_i < shared_ni);      \
            debug_assert (shared_j >= 0 and shared_j < shared_nj);      \
            debug_assert (shared_k >= 0 and shared_k < shared_nk);      \
            int const shared_ind =                                      \
              shared_di*shared_i + shared_dj*shared_j + shared_dk*shared_k; \
            debug_assert (shared_ind >= 0 and shared_ind < shared_np);  \
            {
#define CCTK_CUDA_ENDLOOP                                               \
            }                                                           \
          }                                                             \
        }                                                               \
      }                                                                 \
    }                                                                   \
  } while(0)
  
  // Iterate over a region with ghost zones
#define CCTK_CUDA_LOOP_GHOSTED(i,j,k, ind, shared_ind)                  \
  do {                                                                  \
    int const imin = (0 + blockDim.x *                                  \
                      (0 + cctk_loop.superBlockDim.x *                  \
                       (blockIdx.x + gridDim.x *                        \
                        cctk_loop.subGridIdx.x)));                      \
    int const jmin = (0 + blockDim.y *                                  \
                      (0 + cctk_loop.superBlockDim.y *                  \
                       (blockIdx.y + gridDim.y *                        \
                        cctk_loop.subGridIdx.y)));                      \
    int const kmin = (0 + blockDim.z *                                  \
                      (0 + cctk_loop.superBlockDim.z *                  \
                       (blockIdx.z + gridDim.z *                        \
                        cctk_loop.subGridIdx.z)));                      \
    int const& istp = blockDim.x;                                       \
    int const& jstp = blockDim.y;                                       \
    int const& kstp = blockDim.z;                                       \
    int const& icnt = cctk_loop.superBlockDim.x;                        \
    int const& jcnt = cctk_loop.superBlockDim.y;                        \
    int const& kcnt = cctk_loop.superBlockDim.z;                        \
    int const& ioff = threadIdx.x;                                      \
    int const& joff = threadIdx.y;                                      \
    int const& koff = threadIdx.z;                                      \
    int const imax = imin + icnt * istp;                                \
    int const jmax = jmin + jcnt * jstp;                                \
    int const kmax = kmin + kcnt * kstp;                                \
                                                                        \
    int const shared_imin = max(imin - nghostsi, 0);                    \
    int const shared_jmin = max(jmin - nghostsj, 0);                    \
    int const shared_kmin = max(kmin - nghostsk, 0);                    \
    int const shared_imax = min(imax + nghostsi, ni);                   \
    int const shared_jmax = min(jmax + nghostsj, nj);                   \
    int const shared_kmax = min(kmax + nghostsk, nk);                   \
    int const& shared_istp = istp;                                      \
    int const& shared_jstp = jstp;                                      \
    int const& shared_kstp = kstp;                                      \
    int const shared_icnt = divup(shared_imax - shared_imin, shared_istp); \
    int const shared_jcnt = divup(shared_jmax - shared_jmin, shared_jstp); \
    int const shared_kcnt = divup(shared_kmax - shared_kmin, shared_kstp); \
                                                                        \
    int const shared_ni = shared_imax - shared_imin;                    \
    int const shared_nj = shared_jmax - shared_jmin;                    \
    int const shared_nk = shared_kmax - shared_kmin;                    \
    int const shared_di = 1;                                            \
    int const shared_dj = shared_di * shared_ni;                        \
    int const shared_dk = shared_dj * shared_nj;                        \
    int const shared_np = shared_dk * shared_nk;                        \
                                                                        \
    debug_assert (shared_np );                                          \
                                                                        \
    for (int sbk=0; sbk<shared_kcnt; ++sbk) {                           \
      int const k = shared_kmin + shared_kstp * sbk + koff;             \
      for (int sbj=0; sbj<shared_jcnt; ++sbj) {                         \
        int const j = shared_jmin + shared_jstp * sbj + joff;           \
        for (int sbi=0; sbi<shared_icnt; ++sbi) {                       \
          int const i = shared_imin + shared_istp * sbi + ioff;         \
          if (i<shared_imax and j<shared_jmax and k<shared_kmax) {      \
            int const ind = di*i + dj*j + dk*k;                         \
            int const shared_i = i - shared_imin;                       \
            int const shared_j = j - shared_jmin;                       \
            int const shared_k = k - shared_kmin;                       \
            debug_assert (shared_i >= 0 and shared_i < shared_ni);      \
            debug_assert (shared_j >= 0 and shared_j < shared_nj);      \
            debug_assert (shared_k >= 0 and shared_k < shared_nk);      \
            int const shared_ind =                                      \
              shared_di*shared_i + shared_dj*shared_j + shared_dk*shared_k; \
            debug_assert (shared_ind >= 0 and shared_ind < shared_np);  \
            {
#define CCTK_CUDA_ENDLOOP_GHOSTED               \
            }                                   \
          }                                     \
        }                                       \
      }                                         \
    }                                           \
  } while(0)
  
  
  //////////////////////////////////////////////////////////////////////////////

  // Data structures for passing arguments and parameters to device
  // functions. These should in the end be auto-generated by the CST.
  
  struct driver_t {
    // Grid function pointers
    CCTK_REAL *restrict u      ;
    CCTK_REAL *restrict rho    ;
    CCTK_REAL *restrict u_p    ;
    CCTK_REAL *restrict rho_p  ;
    CCTK_REAL *restrict u_dot  ;
    CCTK_REAL *restrict rho_dot;
  };
  // TODO: Store this in a GH extension
  driver_t *restrict cctk_driver = NULL;
  
  
  
  struct args_t {
    // Copies of Cactus grid structure metadata
    int ni, nj, nk;
    int nghostsi, nghostsj, nghostsk;
    CCTK_REAL xmin, ymin, zmin;
    CCTK_REAL dx, dy, dz, dt;
    // Convenience values
    // Note: We don't store di since it is always one
    int dj, dk, np;
    CCTK_REAL idx2, idy2, idz2;
    CCTK_REAL dt2;
    
    // Grid function pointers
    CCTK_REAL *restrict u      ;
    CCTK_REAL *restrict rho    ;
    CCTK_REAL *restrict u_p    ;
    CCTK_REAL *restrict rho_p  ;
    CCTK_REAL *restrict u_dot  ;
    CCTK_REAL *restrict rho_dot;
    
    args_t ()
    {
    }
    
    args_t (CCTK_ARGUMENTS)
    {
      DECLARE_CCTK_ARGUMENTS;
      DECLARE_CCTK_PARAMETERS;
      
      ni       = cctk_lsh[0];
      nj       = cctk_lsh[1];
      nk       = cctk_lsh[2];
      nghostsi = cctk_nghostzones[0];
      nghostsj = cctk_nghostzones[1];
      nghostsk = cctk_nghostzones[2];
      xmin     = CCTK_ORIGIN_SPACE(0);
      ymin     = CCTK_ORIGIN_SPACE(1);
      zmin     = CCTK_ORIGIN_SPACE(2);
      dx       = CCTK_DELTA_SPACE(0);
      dy       = CCTK_DELTA_SPACE(1);
      dz       = CCTK_DELTA_SPACE(2);
      dt       = CCTK_DELTA_TIME;
      
      dj =    ni;
      dk = dj*nj;
      np = dk*nk;
      
      idx2 = (CCTK_REAL)1.0/pow(dx,2);
      idy2 = (CCTK_REAL)1.0/pow(dy,2);
      idz2 = (CCTK_REAL)1.0/pow(dz,2);
      dt2  = (CCTK_REAL)0.5*dt;
      
      // this->u       = u      ;
      // this->rho     = rho    ;
      // this->u_p     = u_p    ;
      // this->rho_p   = rho_p  ;
      // this->u_dot   = u_dot  ;
      // this->rho_dot = rho_dot;
      
      this->u       = cctk_driver->u      ;
      this->rho     = cctk_driver->rho    ;
      this->u_p     = cctk_driver->u_p    ;
      this->rho_p   = cctk_driver->rho_p  ;
      this->u_dot   = cctk_driver->u_dot  ;
      this->rho_dot = cctk_driver->rho_dot;
    }
  };
  
#define CCTK_CUDA_DECLARE_ARGUMENTS                             \
  int const  di       UNUSED = 1                 ;              \
  int const& dj       UNUSED = cctk_args.dj      ;              \
  int const& dk       UNUSED = cctk_args.dk      ;              \
  int const& ni       UNUSED = cctk_args.ni      ;              \
  int const& nj       UNUSED = cctk_args.nj      ;              \
  int const& nk       UNUSED = cctk_args.nk      ;              \
  int const& nghostsi UNUSED = cctk_args.nghostsi;              \
  int const& nghostsj UNUSED = cctk_args.nghostsj;              \
  int const& nghostsk UNUSED = cctk_args.nghostsk;              \
  CCTK_REAL const& xmin UNUSED = cctk_args.xmin;                \
  CCTK_REAL const& ymin UNUSED = cctk_args.ymin;                \
  CCTK_REAL const& zmin UNUSED = cctk_args.zmin;                \
  CCTK_REAL const& dx   UNUSED = cctk_args.dx  ;                \
  CCTK_REAL const& dy   UNUSED = cctk_args.dy  ;                \
  CCTK_REAL const& dz   UNUSED = cctk_args.dz  ;                \
  CCTK_REAL const& dt   UNUSED = cctk_args.dt  ;                \
  CCTK_REAL const& idx2 UNUSED = cctk_args.idx2;                \
  CCTK_REAL const& idy2 UNUSED = cctk_args.idy2;                \
  CCTK_REAL const& idz2 UNUSED = cctk_args.idz2;                \
  CCTK_REAL const& dt2  UNUSED = cctk_args.dt2 ;                \
  CCTK_REAL* restrict const u       UNUSED = cctk_args.u      ; \
  CCTK_REAL* restrict const rho     UNUSED = cctk_args.rho    ; \
  CCTK_REAL* restrict const u_p     UNUSED = cctk_args.u_p    ; \
  CCTK_REAL* restrict const rho_p   UNUSED = cctk_args.rho_p  ; \
  CCTK_REAL* restrict const u_dot   UNUSED = cctk_args.u_dot  ; \
  CCTK_REAL* restrict const rho_dot UNUSED = cctk_args.rho_dot;
  
  
  
  struct params_t {
    // Copies of Cactus parameters
    CCTK_REAL A, w;
    // Convenience values, determined by the parameters above
    CCTK_REAL iw2h;
    
    params_t ()
    {
    }
    
    params_t (CCTK_ARGUMENTS)
    {
      DECLARE_CCTK_ARGUMENTS;
      DECLARE_CCTK_PARAMETERS;
      
      this->A = A;
      this->w = w;
      
      iw2h = -(CCTK_REAL)0.5/pow(w,2);
    }
  };
  
#define CCTK_CUDA_DECLARE_PARAMETERS                    \
  CCTK_REAL const& A    UNUSED = cctk_params.A    ;     \
  CCTK_REAL const& w    UNUSED = cctk_params.w    ;     \
  CCTK_REAL const& iw2h UNUSED = cctk_params.iw2h ;
  
  
  
  //////////////////////////////////////////////////////////////////////////////
  
  // Scheduled functions and their device counterparts
  
  // Keep arguments write protected to improve performance
  __constant__ args_t cctk_args;
  
  // Keep parameters write protected to improve performance
  __constant__ params_t cctk_params;
  
  extern "C"
  void
  WaveToyCUDA_setup (CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    
    int const np = cctk_lsh[0]*cctk_lsh[1]*cctk_lsh[2];
    
    assert (not cctk_driver);
    cctk_driver = new driver_t;
    
    cctk_driver->u       = cudaNew<CCTK_REAL> (np);
    cctk_driver->rho     = cudaNew<CCTK_REAL> (np);
    cctk_driver->u_p     = cudaNew<CCTK_REAL> (np);
    cctk_driver->rho_p   = cudaNew<CCTK_REAL> (np);
    cctk_driver->u_dot   = cudaNew<CCTK_REAL> (np);
    cctk_driver->rho_dot = cudaNew<CCTK_REAL> (np);
    
    params_t const params(CCTK_PASS_CTOC);
    // override write protection
    CUDA_CHECK
      (cudaMemcpyToSymbol (cctk_params, &params, sizeof(params_t)));
    
    args_t const args(CCTK_PASS_CTOC);
    // override write protection
    CUDA_CHECK
       (cudaMemcpyToSymbol (cctk_args, &args, sizeof(args_t)));
    
    for (int k=0; k<args.nk; ++k) {
      for (int j=0; j<args.nj; ++j) {
        for (int i=0; i<args.ni; ++i) {
          int const ind = i + args.ni * (j + args.nj * k);
          CCTK_REAL const val = 1000.0 * (i + 1000.0 * (j + 1000.0 * k));
          u  [ind] = val + 1;
          rho[ind] = val + 2;
        }
      }
    }
    CUDA_CHECK
      (cudaMemcpy (cctk_driver->u  , u  , sizeof(CCTK_REAL) * args.np,
                   cudaMemcpyHostToDevice));
    CUDA_CHECK
      (cudaMemcpy (cctk_driver->rho, rho, sizeof(CCTK_REAL) * args.np,
                   cudaMemcpyHostToDevice));
    
    CCTK_INFO ("Loop characteristics:");
    dim3 const lsh (cctk_lsh[0], cctk_lsh[1], cctk_lsh[2]);
    cout << "Grid:\n"
         << "   lsh=" << lsh << "\n";
    loop_t const loop(CCTK_PASS_CTOC);
    cout << loop << "\n";
    cout << "Total global memory:     " << devProp.totalGlobalMem    << "\n"
         << "Shared memory per block: " << devProp.sharedMemPerBlock << "\n";
  }
  
  
  
  CCTK_CUDACALL
  void init (loop_t const cctk_loop)
  {
    CCTK_CUDA_DECLARE_ARGUMENTS;
    CCTK_CUDA_DECLARE_PARAMETERS;
    
    CCTK_CUDA_LOOP(i,j,k, ind, shared_ind) {
      CCTK_REAL const x = xmin+dx*i;
      CCTK_REAL const y = ymin+dy*j;
      CCTK_REAL const z = zmin+dz*k;
      CCTK_REAL const r2 = pow(x,2) + pow(y,2) + pow(z,2);
      u  [ind] = A*exp(iw2h*r2);
      rho[ind] = 0.0;
    } CCTK_CUDA_ENDLOOP;
  }
  
  extern "C"
  void
  WaveToyCUDA_init (CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    
    loop_t loop(CCTK_PASS_CTOC);
    CCTK_CUDA_ITERATE (init, loop, 0, (loop));
  }
  
  
  
  CCTK_CUDACALL
  void cycle (loop_t const cctk_loop)
  {
    CCTK_CUDA_DECLARE_ARGUMENTS;
    CCTK_CUDA_DECLARE_PARAMETERS;
    
    CCTK_CUDA_LOOP(i,j,k, ind, shared_ind) {
      u_p  [ind] = u  [ind];
      rho_p[ind] = rho[ind];
    } CCTK_CUDA_ENDLOOP;
  }
  
  extern "C"
  void
  WaveToyCUDA_cycle (CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    
    loop_t loop(CCTK_PASS_CTOC);
    CCTK_CUDA_ITERATE (cycle, loop, 0, (loop));
  }
  
  
  
  CCTK_CUDACALL
  void rhs (loop_t const cctk_loop, size_t const shared_size)
  {
    CCTK_CUDA_DECLARE_ARGUMENTS;
    CCTK_CUDA_DECLARE_PARAMETERS;
    
    // TODO: check size
    CCTK_CUDA_DECLARE_SHARED(CCTK_REAL, shared_u);
    
    // Copy data from global to shared (i.e. from slow to fast) memory 
    CCTK_CUDA_LOOP_GHOSTED(i,j,k, ind, shared_ind) {
      shared_u[shared_ind] = u[ind];
    } CCTK_CUDA_ENDLOOP_GHOSTED;
    __syncthreads();
    
    CCTK_CUDA_LOOP(i,j,k, ind, shared_ind) {
      if (i==0 or i==ni-1 or j==0 or j==nj-1 or k==0 or k==nk-1) {
        // Dirichlet boundary condition
        u_dot  [ind] = 0.0;
        rho_dot[ind] = 0.0;
      } else {
        // Wave equation
#if 0
        CCTK_REAL const ddxu = (u[ind-di]-2*u[ind]+u[ind+di])*idx2;
        CCTK_REAL const ddyu = (u[ind-dj]-2*u[ind]+u[ind+dj])*idy2;
        CCTK_REAL const ddzu = (u[ind-dk]-2*u[ind]+u[ind+dk])*idz2;
#else
        CCTK_REAL const ddxu = (shared_u[shared_ind-shared_di] - 2*shared_u[shared_ind] + shared_u[shared_ind+shared_di]) * idx2;
        CCTK_REAL const ddyu = (shared_u[shared_ind-shared_dj] - 2*shared_u[shared_ind] + shared_u[shared_ind+shared_dj]) * idy2;
        CCTK_REAL const ddzu = (shared_u[shared_ind-shared_dk] - 2*shared_u[shared_ind] + shared_u[shared_ind+shared_dk]) * idz2;
#endif
        u_dot  [ind] = rho[ind];
        rho_dot[ind] = ddxu + ddyu + ddzu;
      }
    } CCTK_CUDA_ENDLOOP;
  }
  
  extern "C"
  void
  WaveToyCUDA_rhs (CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    DECLARE_CCTK_LOOP;
    
    size_t const sharedSize = sizeof(CCTK_REAL) * shared_np;
    {
      static bool beenhere = false;
      if (not beenhere) {
        beenhere = true;
        cout << "RHS: Shared memory usage: " << sharedSize << "\n";
      }
    }
    assert (sharedSize <= devProp.sharedMemPerBlock);
    CCTK_CUDA_ITERATE (rhs, cctk_loop, sharedSize, (cctk_loop, sharedSize));
  }
  
  
  
  CCTK_CUDACALL
  void step1 (loop_t const cctk_loop)
  {
    CCTK_CUDA_DECLARE_ARGUMENTS;
    CCTK_CUDA_DECLARE_PARAMETERS;
    
    CCTK_CUDA_LOOP(i,j,k, ind, shared_ind) {
      u  [ind] = u_p  [ind] + dt2*u_dot  [ind];
      rho[ind] = rho_p[ind] + dt2*rho_dot[ind];
    } CCTK_CUDA_ENDLOOP;
  }
  
  extern "C"
  void
  WaveToyCUDA_step1 (CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    
    loop_t loop(CCTK_PASS_CTOC);
    CCTK_CUDA_ITERATE (step1, loop, 0, (loop));
  }
  
  
  
  CCTK_CUDACALL
  void step2 (loop_t const cctk_loop)
  {
    CCTK_CUDA_DECLARE_ARGUMENTS;
    CCTK_CUDA_DECLARE_PARAMETERS;
    
    CCTK_CUDA_LOOP(i,j,k, ind, shared_ind) {
      u  [ind] = u_p  [ind] + dt*u_dot  [ind];
      rho[ind] = rho_p[ind] + dt*rho_dot[ind];
    } CCTK_CUDA_ENDLOOP;
  }
  
  extern "C"
  void
  WaveToyCUDA_step2 (CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    
    loop_t loop(CCTK_PASS_CTOC);
    CCTK_CUDA_ITERATE (step2, loop, 0, (loop));
  }
  
  
  
  extern "C"
  void
  WaveToyCUDA_output (CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    
    args_t const host_args(CCTK_PASS_CTOC);
    
    CUDA_CHECK
      (cudaMemcpy (u  , cctk_driver->u  , sizeof(CCTK_REAL) * host_args.np,
                   cudaMemcpyDeviceToHost));
    CUDA_CHECK
      (cudaMemcpy (rho, cctk_driver->rho, sizeof(CCTK_REAL) * host_args.np,
                   cudaMemcpyDeviceToHost));
  }
  
  
  
  // A data structure for reductions
  
  struct norms_t {
    // State of the norm
    CCTK_REAL u1, u2, u4, ucnt, umin, umax;
    
    // Initialise the norm state
    __host__ __device__
    void init ()
    {
      u1   = 0.0;
      u2   = 0.0;
      u4   = 0.0;
      ucnt = 0.0;
      umin = +DBL_MAX;
      umax = -DBL_MAX;
    }
    // Add a new value to the norm state
    __host__ __device__
    void reduce (CCTK_REAL const& u)
    {
      u1 += fabs(u);
      u2 += pow(u,2);
      u4 += pow(u,4);
      ucnt += 1.0;
      umin = min(umin, u);
      umax = max(umax, u);
    }
    // Combine two norm states
    __host__ __device__
    void combine (norms_t const& n)
    {
      u1 += n.u1;
      u2 += n.u2;
      u4 += n.u4;
      ucnt += n.ucnt;
      umin = min(umin, n.umin);
      umax = max(umax, n.umax);
    }
    // Finalise the norm
    __host__ __device__
    void finish ()
    {
      u1 = u1/ucnt;
      u2 = sqrt(u2/ucnt);
      u4 = pow(u4/ucnt,(CCTK_REAL)0.25);
    }
  };
  
  CCTK_CUDACALL
  void analyse (loop_t const cctk_loop,
                norms_t *restrict const device_norms,
                int const device_norms_size)
  {
    CCTK_CUDA_DECLARE_ARGUMENTS;
    CCTK_CUDA_DECLARE_PARAMETERS;
    
    // Reduce over super-block
    norms_t thread_norm;
    thread_norm.init();
    CCTK_CUDA_LOOP(i,j,k, ind, shared_ind) {
      thread_norm.reduce(u[ind]);
    } CCTK_CUDA_ENDLOOP;
    
    // Reduce over block
    CCTK_CUDA_DECLARE_SHARED(norms_t, shared_norms);
    int const bd = blockDim.x * blockDim.y * blockDim.z;
    int const ti =
      threadIdx.x + blockDim.x * (threadIdx.y + blockDim.y * threadIdx.z);
    debug_assert (ti < bd);
    shared_norms[ti] = thread_norm;
    int nvals = bd;
    // round down to the next power of 2
    int next_nvals = nextlower(nvals);
    debug_assert (next_nvals>0 and 2*next_nvals<=nvals);
    while (nvals > 1) {
      if (nvals > warpSize) __syncthreads();
      if (ti < nvals - next_nvals) {
        shared_norms[ti].combine(shared_norms[next_nvals+ti]);
      }
      nvals = next_nvals;
      next_nvals >>= 1;
    }
    
    // Return one value per super-block, to be reduced on the host
    if (ti == 0) {
      dim3 const gsgDim (gridDim.x * cctk_loop.superGridDim.x,
                         gridDim.y * cctk_loop.superGridDim.y,
                         gridDim.z * cctk_loop.superGridDim.z);
      int const gsgd = gsgDim.x * gsgDim.y * gsgDim.z;
      debug_assert (device_norms_size == gsgd);
      dim3 const bsgIdx (blockIdx.x + gridDim.x * cctk_loop.subGridIdx.x,
                         blockIdx.y + gridDim.y * cctk_loop.subGridIdx.y,
                         blockIdx.z + gridDim.z * cctk_loop.subGridIdx.z);
      int const bsgi =
        bsgIdx.x + gsgDim.x * (bsgIdx.y + gsgDim.y * bsgIdx.z);
      debug_assert (bsgi < gsgd);
      device_norms[bsgi] = shared_norms[0];
    }
  }
  
  extern "C"
  void
  WaveToyCUDA_analyse (CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    
    loop_t loop(CCTK_PASS_CTOC);
    
    int const block_size =
      loop.blockDim.x * loop.blockDim.y * loop.blockDim.z;
    int const grid_size =
      loop.gridDim.x * loop.gridDim.y * loop.gridDim.z *
      loop.superGridDim.x * loop.superGridDim.y * loop.superGridDim.z;
    
    norms_t * restrict const device_norms = cudaNew<norms_t>(grid_size);
    
    CCTK_CUDA_ITERATE (analyse, loop, sizeof(norms_t)*block_size,
                       (loop, device_norms, grid_size));
    CUDA_CHECK(cudaDeviceSynchronize());
    
    norms_t * restrict const host_norms = new norms_t[grid_size];
    CUDA_CHECK
      (cudaMemcpy (host_norms, device_norms, sizeof(norms_t)*grid_size,
                   cudaMemcpyDeviceToHost));
    cudaDelete(device_norms);
    
    norms_t norms;
    norms.init();
    for (int n=0; n<grid_size; ++n) {
      norms.combine(host_norms[n]);
    }
    delete[] host_norms;
    norms.finish();
    
    *u1   = norms.u1  ;
    *u2   = norms.u2  ;
    *u4   = norms.u4  ;
    *ucnt = norms.ucnt;
    *umin = norms.umin;
    *umax = norms.umax;
  }
  
  
  
} // end namespace WaveToyCUDA
