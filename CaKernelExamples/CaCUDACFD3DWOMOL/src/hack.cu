
#include"cctk.h"
#include"cctk_Parameters.h"
#include"cctk_Arguments.h"
#include<assert.h>
#include<stdio.h>

int CaKernel_SwapVarsI(void *cctkGH_, int vi, int tl, int vi2, int tl2);

extern "C"
void CAKERNEL_Launch_Update_Velocity(CCTK_ARGUMENTS);

extern "C"
void CAKERNEL_Launch_Update_Pressure(CCTK_ARGUMENTS);

extern "C"
void CAKERNEL_Launch_Update_Boundaries(CCTK_ARGUMENTS);


void hack_up_vel(CCTK_ARGUMENTS){
  DECLARE_CCTK_ARGUMENTS;

  size_t datasize = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2] * sizeof(CCTK_REAL);
  int vi=-1, vi2=-1;
  
  CAKERNEL_Launch_Update_Velocity(CCTK_PASS_CTOC);

  if(*((int *)CCTK_VarDataPtr(cctkGH, 0, "ChemoraDevice::device_process"))){
//  printf("Performin update velocity\n")  ;
    assert((vi = CCTK_VarIndex("CaCUDACFD3DWOMOL::vx"))>=0);
    assert((vi2 = CCTK_VarIndex("CaCUDACFD3DWOMOL::vx_out"))>=0);
    CaKernel_SwapVarsI(cctkGH, vi, 0, vi2, 0);
 
    assert((vi = CCTK_VarIndex("CaCUDACFD3DWOMOL::vy"))>=0);
    assert((vi2 = CCTK_VarIndex("CaCUDACFD3DWOMOL::vy_out"))>=0);
    CaKernel_SwapVarsI(cctkGH, vi, 0, vi2, 0);
 
    assert((vi = CCTK_VarIndex("CaCUDACFD3DWOMOL::vz"))>=0);
    assert((vi2 = CCTK_VarIndex("CaCUDACFD3DWOMOL::vz_out"))>=0);
    CaKernel_SwapVarsI(cctkGH, vi, 0, vi2, 0);
  }else{
    memcpy(vx, vx_out, datasize);
    memcpy(vy, vy_out, datasize);
    memcpy(vz, vz_out, datasize);
//    memset(vx_out, 0, datasize);
//    memset(vy_out, 0, datasize);
//    memset(vz_out, 0, datasize);
  }
}

void hack_up_pres(CCTK_ARGUMENTS){
  DECLARE_CCTK_ARGUMENTS;

  size_t datasize = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2] * sizeof(CCTK_REAL);
  int vi=-1, vi2=-1;

  CAKERNEL_Launch_Update_Pressure(CCTK_PASS_CTOC);
  
//  printf("Performin update pressure\n")  ;
  if(*((int *)CCTK_VarDataPtr(cctkGH, 0, "ChemoraDevice::device_process"))){
    assert((vi = CCTK_VarIndex("CaCUDACFD3DWOMOL::vx"))>=0);
    assert((vi2 = CCTK_VarIndex("CaCUDACFD3DWOMOL::vx_out"))>=0);
    CaKernel_SwapVarsI(cctkGH, vi, 0, vi2, 0);
 
    assert((vi = CCTK_VarIndex("CaCUDACFD3DWOMOL::vy"))>=0);
    assert((vi2 = CCTK_VarIndex("CaCUDACFD3DWOMOL::vy_out"))>=0);
    CaKernel_SwapVarsI(cctkGH, vi, 0, vi2, 0);
 
    assert((vi = CCTK_VarIndex("CaCUDACFD3DWOMOL::vz"))>=0);
    assert((vi2 = CCTK_VarIndex("CaCUDACFD3DWOMOL::vz_out"))>=0);
    CaKernel_SwapVarsI(cctkGH, vi, 0, vi2, 0);
  }else{
    memcpy(vx, vx_out, datasize);
    memcpy(vy, vy_out, datasize);
    memcpy(vz, vz_out, datasize);
//    memset(vx_out, 0, datasize);
//    memset(vy_out, 0, datasize);
//    memset(vz_out, 0, datasize);
  }
}

void hack_up_bound(CCTK_ARGUMENTS){
  DECLARE_CCTK_ARGUMENTS;

  CAKERNEL_Launch_Update_Boundaries(CCTK_PASS_CTOC);

}
