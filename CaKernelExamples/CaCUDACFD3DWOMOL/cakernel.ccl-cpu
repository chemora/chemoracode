# CUDA Kernel definition for thorn CaCUDACFD3D

CCTK_CUDA_KERNEL Update_Velocity
   TYPE=cpu_openmp/3dblock STENCIL="1,1,1,1,1,1" TILE="66,16,16" SHARECODE=yes  {

  CCTK_CUDA_KERNEL_VARIABLE cached=yes intent=inout timelevels=1 {
    vx, vy, vz
  } "velocity u^i"

#These variables are actually "out". The reason why they are tagged so is that
#they are not used on CPU at all and we don't want to synchronize. They are 
#swapped with velocity for the purpose of these computations.
  CCTK_CUDA_KERNEL_VARIABLE cached=no intent=in timelevels=1 {
    vx_out, vy_out, vz_out
  } "temporary trick"

  CCTK_CUDA_KERNEL_VARIABLE cached=no intent=in timelevels=1 {
    p # in the interface.ccl
  } "pressure p"

  CCTK_CUDA_KERNEL_PARAMETER {
    density # in the param.ccl file
  } "density of the incompressible fluid"
}

CCTK_CUDA_KERNEL Update_Pressure
    TYPE=cpu_openmp/3dblock STENCIL="1,1,1,1,1,1" TILE="66,16,16" SHARECODE=yes  {

  CCTK_CUDA_KERNEL_VARIABLE cached=yes intent=in timelevels=1 { 
    vx, vy, vz 
  } "velocity u^i"

  CCTK_CUDA_KERNEL_VARIABLE cached=no intent=in timelevels=1 {
    vx_out, vy_out, vz_out
  } "temporary trick"

  CCTK_CUDA_KERNEL_VARIABLE cached=no intent=inout timelevels=1 { 
    p 
  } "pressure p"

  CCTK_CUDA_KERNEL_PARAMETER { 
    density 
  } "density of the incompressible fluid"
}

CCTK_CUDA_KERNEL Update_Boundaries
    TYPE=cpu_openmp/boundary_s
    TILE="66,16,16"
    SHARECODE=yes  
{
  CCTK_CUDA_KERNEL_VARIABLE cached=no intent=inout
  {
    vx, vy, vz
  } "velocity u^i"
}

