#include <stdlib.h>
#include <stdio.h>
#include <vector>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"


using namespace std;

struct CT_Main_Data
{
  vector<CCTK_REAL> a;
} ct_main_data;


CCTK_REAL scalar_vals[] = { 10.0, 200.0, 3000.0 };
const int sv_size = sizeof(scalar_vals)/sizeof(scalar_vals[0]);


extern "C" void
CT_Main_Data_Init(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_PARAMETERS;
  DECLARE_CCTK_ARGUMENTS;

  CCTK_VInfo(CCTK_THORNSTRING, "The CT main run is starting.\n");

  srand48(2735l);

  const size_t n_elts = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2];
  ct_main_data.a.resize(n_elts);

  *myscalar = -1000;

  for ( int k=0; k<cctk_lsh[2]; k++ )
    for ( int j=0; j<cctk_lsh[1]; j++ )
      for ( int i=0; i<cctk_lsh[0]; i++ )
        {
          const int idx = CCTK_GFINDEX3D(cctkGH,i,j,k);
          a[idx] = drand48();
          ct_main_data.a[idx] = a[idx];
        }
}


extern "C" void
CT_Main_PreEvolve(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_PARAMETERS;
  DECLARE_CCTK_ARGUMENTS;

  *myscalar = scalar_vals[cctk_iteration % sv_size];
  if ( false )
    printf("In iteration %d setting my scalar to %f\n",
           cctk_iteration,*myscalar);
}

extern "C" void
CT_Main_Terminate(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_PARAMETERS;
  DECLARE_CCTK_ARGUMENTS;

  CCTK_VInfo(CCTK_THORNSTRING, "The CT main run is starting.\n");

  const int nprocs = CCTK_nProcs(cctkGH);

  const int DIMSIZE = 6;
  CCTK_INT bndsize[DIMSIZE];
  CCTK_INT is_ghostbnd[DIMSIZE];
  CCTK_INT is_symbnd[DIMSIZE];
  CCTK_INT is_physbnd[DIMSIZE];

  const int rv = GetBoundarySizesAndTypes
    (cctkGH, DIMSIZE, bndsize, is_ghostbnd, is_symbnd, is_physbnd);
  assert( !rv );

  const int bnd_up_i = cctk_lsh[0] - bndsize[1];
  const int bnd_up_j = cctk_lsh[1] - bndsize[3];
  const int bnd_up_k = cctk_lsh[2] - bndsize[5];

  int err_count = 0;

  auto& sa = ct_main_data.a;  // Shadow a.

  for ( int k=0; k<cctk_lsh[2]; k++ )
    for ( int j=0; j<cctk_lsh[1]; j++ )
      for ( int i=0; i<cctk_lsh[0]; i++ )
        {
          const bool interior =
               i >= bndsize[0] && i < bnd_up_i
            && j >= bndsize[2] && j < bnd_up_j
            && k >= bndsize[4] && k < bnd_up_k;

          const bool phys_boundary =
               is_physbnd[0] && i <  bndsize[0]
            || is_physbnd[1] && i >= bnd_up_i
            || is_physbnd[2] && j <  bndsize[2]
            || is_physbnd[3] && j >= bnd_up_j
            || is_physbnd[4] && k <  bndsize[4]
            || is_physbnd[5] && k >= bnd_up_k;

          assert( nprocs || interior ^ phys_boundary );

          if ( !interior ) continue;

          const auto aoff = [&](int dx, int dy, int dz)
            { return sa[CCTK_GFINDEX3D(cctkGH,i+dx,j+dy,k+dz)]; };

          const int idx = CCTK_GFINDEX3D(cctkGH,i,j,k);
          const CCTK_REAL check =
            sa[idx] + ( aoff(0,1,0) <= aoff(0,-1,0) ? *myscalar : 0.0 ) +
            ( aoff(0,1,0) > aoff(0,-1,0) || ctMainConstThreshold >= 5 ?
              *myscalar
              + ( aoff(1,0,0) > aoff(-1,0,0) ? aoff(-1,0,0) : aoff(1,0,0) ) :
              ( aoff(0,0,1) < aoff(0,0,-1) ? aoff(0,-1,0) : aoff(0,1,0) ) );

          const CCTK_REAL err = fabs( check - b[idx] );
          if ( err < 1e-5 ) continue;
          err_count++;
        }

  CCTK_VInfo(CCTK_THORNSTRING,
             "Checked data, %d errors found.", err_count);


  CCTK_VInfo(CCTK_THORNSTRING, "The CT main run has completed.\n");
}
