Instructions for Very Unfinished Chemora Test Code
 (22 June 2018, 12:45:32 CDT)

Note:
 These instructions have not yet been tested.
 Don't attempt unless you understand Unix, Cactus, and Kranc.

Prerequisites
 - A working Cactus installation.
   Use .../chemoracode/ChemoraTest/ct.th to pull thorns.
 - A working Kranc installation, which itself requires Mathematica.

Generate a Thorn (or skip to Use Distro Thorn, below)
 
 - Check out a copy of the dmk-chemora branch of Kranc from
   https://github.com/ianhinder/Kranc.git

 - Apply the patch at etc/kranc.diff to Kranc.
   (The patch won't be needed after the scalar changes are tested.)
   These instructions assume that you know how to apply a patch.
   Normally that's too much to assume, but since these instructions are very
   temporary it makes no sense to thoroughly test them, meaning that users
   are likely to encounter problems which can be baffling to those with
   less experience.

 - Generate a thorn from the EDL script
   cd .../chemoracode/ChemoraTest
   kranc -c .../Cactus ct-edl.kranc

   Note: The patched kranc has a -c option for specifying the Cactus dir.


Use Distro Thorn (only if didn't finish Generate a Thorn, above)

 - Copy distro's version of thorn.
   cd .../chemoracode/ChemoraTest
   cp -R ct_edl_repo ct_edl


Create Configuration and Run Code

 - Create a configuration
   cd .../Cactus
   gmake myctconfiguration THORNLIST=.../chemoracode/ChemoraTest/ct.th

 - Build
   cd .../Cactus
   gmake myctconfiguration -j 40

 - Run
   ./exe/cactus_myctconfiguration .../chemoracode/ChemoraTest/ct.par


Settings to Vary

 As of this writing scalars work correctly in statically compiled
 CaKernel code, Kranc-generated CPU C code, and dynamically generated
 Chemora code.

 Changes to ct.par for:

 - Statically Compiled (CaKernel)
   CaCUDALib::dynamic_compilation = no
   CaKernel::use_kranc_c = no

 - Kranc CPU C Code
   CaCUDALib::dynamic_compilation = yes
   CaKernel::use_kranc_c = yes

 - Dynamically Compiled Chemora Code
   CaCUDALib::dynamic_compilation = yes
   CaKernel::use_kranc_c = no


Verify

 The CT_Main thorn prints out a completion message that includes a
 tally of errors.

INFO (CT_Main): Checked data, 0 errors found.
INFO (CT_Main): The CT main run has completed.
