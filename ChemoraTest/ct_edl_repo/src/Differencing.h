#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder21(u) ((-KRANC_GFOFFSET3D(u,-1,0,0) + KRANC_GFOFFSET3D(u,1,0,0))*p1o2dx)
#else
#  define PDstandardfdOrder21(u) (PDstandardfdOrder21_impl(u,p1o2dx,cdj,cdk))
static CCTK_REAL PDstandardfdOrder21_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder21_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-KRANC_GFOFFSET3D(u,-1,0,0) + KRANC_GFOFFSET3D(u,1,0,0))*p1o2dx;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder22(u) ((-KRANC_GFOFFSET3D(u,0,-1,0) + KRANC_GFOFFSET3D(u,0,1,0))*p1o2dy)
#else
#  define PDstandardfdOrder22(u) (PDstandardfdOrder22_impl(u,p1o2dy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder22_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder22_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-KRANC_GFOFFSET3D(u,0,-1,0) + KRANC_GFOFFSET3D(u,0,1,0))*p1o2dy;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder23(u) ((-KRANC_GFOFFSET3D(u,0,0,-1) + KRANC_GFOFFSET3D(u,0,0,1))*p1o2dz)
#else
#  define PDstandardfdOrder23(u) (PDstandardfdOrder23_impl(u,p1o2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder23_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder23_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder22_impl(u, p1o2dz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder41(u) ((-8*KRANC_GFOFFSET3D(u,-1,0,0) + 8*KRANC_GFOFFSET3D(u,1,0,0) + KRANC_GFOFFSET3D(u,-2,0,0) - KRANC_GFOFFSET3D(u,2,0,0))*p1o12dx)
#else
#  define PDstandardfdOrder41(u) (PDstandardfdOrder41_impl(u,p1o12dx,cdj,cdk))
static CCTK_REAL PDstandardfdOrder41_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o12dx, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder41_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o12dx, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-8*KRANC_GFOFFSET3D(u,-1,0,0) + 8*KRANC_GFOFFSET3D(u,1,0,0) + KRANC_GFOFFSET3D(u,-2,0,0) - KRANC_GFOFFSET3D(u,2,0,0))*p1o12dx;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder42(u) ((-8*KRANC_GFOFFSET3D(u,0,-1,0) + 8*KRANC_GFOFFSET3D(u,0,1,0) + KRANC_GFOFFSET3D(u,0,-2,0) - KRANC_GFOFFSET3D(u,0,2,0))*p1o12dy)
#else
#  define PDstandardfdOrder42(u) (PDstandardfdOrder42_impl(u,p1o12dy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder42_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o12dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder42_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o12dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-8*KRANC_GFOFFSET3D(u,0,-1,0) + 8*KRANC_GFOFFSET3D(u,0,1,0) + KRANC_GFOFFSET3D(u,0,-2,0) - KRANC_GFOFFSET3D(u,0,2,0))*p1o12dy;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder43(u) ((-8*KRANC_GFOFFSET3D(u,0,0,-1) + 8*KRANC_GFOFFSET3D(u,0,0,1) + KRANC_GFOFFSET3D(u,0,0,-2) - KRANC_GFOFFSET3D(u,0,0,2))*p1o12dz)
#else
#  define PDstandardfdOrder43(u) (PDstandardfdOrder43_impl(u,p1o12dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder43_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o12dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder43_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o12dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder42_impl(u, p1o12dz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder211(u) ((-2*KRANC_GFOFFSET3D(u,0,0,0) + KRANC_GFOFFSET3D(u,-1,0,0) + KRANC_GFOFFSET3D(u,1,0,0))*p1odx2)
#else
#  define PDstandardfdOrder211(u) (PDstandardfdOrder211_impl(u,p1odx2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder211_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1odx2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder211_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1odx2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-2*KRANC_GFOFFSET3D(u,0,0,0) + KRANC_GFOFFSET3D(u,-1,0,0) + KRANC_GFOFFSET3D(u,1,0,0))*p1odx2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder222(u) ((-2*KRANC_GFOFFSET3D(u,0,0,0) + KRANC_GFOFFSET3D(u,0,-1,0) + KRANC_GFOFFSET3D(u,0,1,0))*p1ody2)
#else
#  define PDstandardfdOrder222(u) (PDstandardfdOrder222_impl(u,p1ody2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder222_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1ody2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder222_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1ody2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-2*KRANC_GFOFFSET3D(u,0,0,0) + KRANC_GFOFFSET3D(u,0,-1,0) + KRANC_GFOFFSET3D(u,0,1,0))*p1ody2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder233(u) ((-2*KRANC_GFOFFSET3D(u,0,0,0) + KRANC_GFOFFSET3D(u,0,0,-1) + KRANC_GFOFFSET3D(u,0,0,1))*p1odz2)
#else
#  define PDstandardfdOrder233(u) (PDstandardfdOrder233_impl(u,p1odz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder233_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1odz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder233_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1odz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder222_impl(u, p1odz2, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder411(u) ((30*KRANC_GFOFFSET3D(u,0,0,0) - 16*(KRANC_GFOFFSET3D(u,-1,0,0) + KRANC_GFOFFSET3D(u,1,0,0)) + KRANC_GFOFFSET3D(u,-2,0,0) + KRANC_GFOFFSET3D(u,2,0,0))*pm1o12dx2)
#else
#  define PDstandardfdOrder411(u) (PDstandardfdOrder411_impl(u,pm1o12dx2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder411_impl(const CCTK_REAL* restrict const u, const CCTK_REAL pm1o12dx2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder411_impl(const CCTK_REAL* restrict const u, const CCTK_REAL pm1o12dx2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (30*KRANC_GFOFFSET3D(u,0,0,0) - 16*(KRANC_GFOFFSET3D(u,-1,0,0) + KRANC_GFOFFSET3D(u,1,0,0)) + KRANC_GFOFFSET3D(u,-2,0,0) + KRANC_GFOFFSET3D(u,2,0,0))*pm1o12dx2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder422(u) ((30*KRANC_GFOFFSET3D(u,0,0,0) - 16*(KRANC_GFOFFSET3D(u,0,-1,0) + KRANC_GFOFFSET3D(u,0,1,0)) + KRANC_GFOFFSET3D(u,0,-2,0) + KRANC_GFOFFSET3D(u,0,2,0))*pm1o12dy2)
#else
#  define PDstandardfdOrder422(u) (PDstandardfdOrder422_impl(u,pm1o12dy2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder422_impl(const CCTK_REAL* restrict const u, const CCTK_REAL pm1o12dy2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder422_impl(const CCTK_REAL* restrict const u, const CCTK_REAL pm1o12dy2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (30*KRANC_GFOFFSET3D(u,0,0,0) - 16*(KRANC_GFOFFSET3D(u,0,-1,0) + KRANC_GFOFFSET3D(u,0,1,0)) + KRANC_GFOFFSET3D(u,0,-2,0) + KRANC_GFOFFSET3D(u,0,2,0))*pm1o12dy2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder433(u) ((30*KRANC_GFOFFSET3D(u,0,0,0) - 16*(KRANC_GFOFFSET3D(u,0,0,-1) + KRANC_GFOFFSET3D(u,0,0,1)) + KRANC_GFOFFSET3D(u,0,0,-2) + KRANC_GFOFFSET3D(u,0,0,2))*pm1o12dz2)
#else
#  define PDstandardfdOrder433(u) (PDstandardfdOrder433_impl(u,pm1o12dz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder433_impl(const CCTK_REAL* restrict const u, const CCTK_REAL pm1o12dz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder433_impl(const CCTK_REAL* restrict const u, const CCTK_REAL pm1o12dz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder422_impl(u, pm1o12dz2, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder212(u) ((KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o4dxdy)
#else
#  define PDstandardfdOrder212(u) (PDstandardfdOrder212_impl(u,p1o4dxdy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder212_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder212_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o4dxdy;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder213(u) ((KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o4dxdz)
#else
#  define PDstandardfdOrder213(u) (PDstandardfdOrder213_impl(u,p1o4dxdz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder213_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder213_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder212_impl(u, p1o4dxdz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder221(u) ((KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o4dxdy)
#else
#  define PDstandardfdOrder221(u) (PDstandardfdOrder221_impl(u,p1o4dxdy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder221_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder221_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder212_impl(u, p1o4dxdy, cdj, cdk);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder223(u) ((KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o4dydz)
#else
#  define PDstandardfdOrder223(u) (PDstandardfdOrder223_impl(u,p1o4dydz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder223_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dydz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder223_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dydz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o4dydz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder231(u) ((KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o4dxdz)
#else
#  define PDstandardfdOrder231(u) (PDstandardfdOrder231_impl(u,p1o4dxdz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder231_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder231_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder212_impl(u, p1o4dxdz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder232(u) ((KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o4dydz)
#else
#  define PDstandardfdOrder232(u) (PDstandardfdOrder232_impl(u,p1o4dydz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder232_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dydz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder232_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dydz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder223_impl(u, p1o4dydz, cdj, cdk);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder412(u) ((-64*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 64*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0) + KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) + KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy)
#else
#  define PDstandardfdOrder412(u) (PDstandardfdOrder412_impl(u,p1o144dxdy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder412_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder412_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-64*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 64*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0) + KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) + KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder413(u) ((-64*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 64*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2) + KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) + KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz)
#else
#  define PDstandardfdOrder413(u) (PDstandardfdOrder413_impl(u,p1o144dxdz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder413_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder413_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder412_impl(u, p1o144dxdz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder421(u) ((-64*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 64*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0) + KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) + KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy)
#else
#  define PDstandardfdOrder421(u) (PDstandardfdOrder421_impl(u,p1o144dxdy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder421_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder421_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder412_impl(u, p1o144dxdy, cdj, cdk);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder423(u) ((-64*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 64*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,2) + KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) + KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz)
#else
#  define PDstandardfdOrder423(u) (PDstandardfdOrder423_impl(u,p1o144dydz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder423_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder423_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-64*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 64*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,2) + KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) + KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder431(u) ((-64*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 64*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2) + KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) + KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz)
#else
#  define PDstandardfdOrder431(u) (PDstandardfdOrder431_impl(u,p1o144dxdz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder431_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder431_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder412_impl(u, p1o144dxdz, cdk, cdj);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder432(u) ((-64*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 64*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,2) + KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) + KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz)
#else
#  define PDstandardfdOrder432(u) (PDstandardfdOrder432_impl(u,p1o144dydz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder432_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder432_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return PDstandardfdOrder423_impl(u, p1o144dydz, cdj, cdk);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2111(u) (0)
#else
#  define PDstandardfdOrder2111(u) (PDstandardfdOrder2111_impl(u,p1o1,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2111_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o1, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2111_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o1, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return 0;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2222(u) (0)
#else
#  define PDstandardfdOrder2222(u) (PDstandardfdOrder2222_impl(u,p1o1,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2222_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o1, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2222_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o1, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return 0;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2333(u) (0)
#else
#  define PDstandardfdOrder2333(u) (PDstandardfdOrder2333_impl(u,p1o1,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2333_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o1, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2333_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o1, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return 0;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4111(u) ((2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-2,0,0) + KRANC_GFOFFSET3D(u,2,0,0))*p1o2dx3)
#else
#  define PDstandardfdOrder4111(u) (PDstandardfdOrder4111_impl(u,p1o2dx3,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4111_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx3, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4111_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx3, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-2,0,0) + KRANC_GFOFFSET3D(u,2,0,0))*p1o2dx3;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4222(u) ((2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,0,-2,0) + KRANC_GFOFFSET3D(u,0,2,0))*p1o2dy3)
#else
#  define PDstandardfdOrder4222(u) (PDstandardfdOrder4222_impl(u,p1o2dy3,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4222_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dy3, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4222_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dy3, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,0,-2,0) + KRANC_GFOFFSET3D(u,0,2,0))*p1o2dy3;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4333(u) ((2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,0,0,-2) + KRANC_GFOFFSET3D(u,0,0,2))*p1o2dz3)
#else
#  define PDstandardfdOrder4333(u) (PDstandardfdOrder4333_impl(u,p1o2dz3,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4333_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dz3, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4333_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dz3, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,0,0,-2) + KRANC_GFOFFSET3D(u,0,0,2))*p1o2dz3;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2112(u) ((2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dx2dy)
#else
#  define PDstandardfdOrder2112(u) (PDstandardfdOrder2112_impl(u,p1o2dx2dy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2112_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2112_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dx2dy;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2113(u) ((2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dx2dz)
#else
#  define PDstandardfdOrder2113(u) (PDstandardfdOrder2113_impl(u,p1o2dx2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2113_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2113_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dx2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2221(u) ((2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dxdy2)
#else
#  define PDstandardfdOrder2221(u) (PDstandardfdOrder2221_impl(u,p1o2dxdy2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2221_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2221_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dxdy2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2223(u) ((2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dy2dz)
#else
#  define PDstandardfdOrder2223(u) (PDstandardfdOrder2223_impl(u,p1o2dy2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2223_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2223_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dy2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2331(u) ((2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dxdz2)
#else
#  define PDstandardfdOrder2331(u) (PDstandardfdOrder2331_impl(u,p1o2dxdz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2331_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2331_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dxdz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2332(u) ((2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dydz2)
#else
#  define PDstandardfdOrder2332(u) (PDstandardfdOrder2332_impl(u,p1o2dydz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2332_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2332_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dydz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4112(u) ((240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 128*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 16*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,-2,0)) - 16*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,2,0)) + 8*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) + KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dx2dy)
#else
#  define PDstandardfdOrder4112(u) (PDstandardfdOrder4112_impl(u,p1o144dx2dy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4112_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4112_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 128*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 16*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,-2,0)) - 16*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,2,0)) + 8*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) + KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dx2dy;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4113(u) ((240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 128*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,1)) + 16*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,-2)) - 16*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,2)) + 8*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) + KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dx2dz)
#else
#  define PDstandardfdOrder4113(u) (PDstandardfdOrder4113_impl(u,p1o144dx2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4113_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4113_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 128*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,1)) + 16*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,-2)) - 16*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,2)) + 8*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) + KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dx2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4221(u) ((240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0)) + 128*(KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,-1,2,0)) - 8*(KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,-2,1,0)) - 16*(KRANC_GFOFFSET3D(u,2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) + KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy2)
#else
#  define PDstandardfdOrder4221(u) (PDstandardfdOrder4221_impl(u,p1o144dxdy2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4221_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4221_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0)) + 128*(KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,-1,2,0)) - 8*(KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,-2,1,0)) - 16*(KRANC_GFOFFSET3D(u,2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) + KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4223(u) ((240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 128*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,1)) + 16*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,-2)) - 16*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,2)) + 8*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) + KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dy2dz)
#else
#  define PDstandardfdOrder4223(u) (PDstandardfdOrder4223_impl(u,p1o144dy2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4223_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4223_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 128*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,1)) + 16*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,-2)) - 16*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,2)) + 8*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) + KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dy2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4331(u) ((240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1)) + 128*(KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,-1,0,2)) - 8*(KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,-2,0,1)) - 16*(KRANC_GFOFFSET3D(u,2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) + KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz2)
#else
#  define PDstandardfdOrder4331(u) (PDstandardfdOrder4331_impl(u,p1o144dxdz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4331_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4331_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1)) + 128*(KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,-1,0,2)) - 8*(KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,-2,0,1)) - 16*(KRANC_GFOFFSET3D(u,2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) + KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4332(u) ((240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1)) + 128*(KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,-1,2)) - 8*(KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,1,2)) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) + 16*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,-2,1)) - 16*(KRANC_GFOFFSET3D(u,0,2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) + KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz2)
#else
#  define PDstandardfdOrder4332(u) (PDstandardfdOrder4332_impl(u,p1o144dydz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4332_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4332_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1)) + 128*(KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,-1,2)) - 8*(KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,1,2)) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) + 16*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,-2,1)) - 16*(KRANC_GFOFFSET3D(u,0,2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) + KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2122(u) ((2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dxdy2)
#else
#  define PDstandardfdOrder2122(u) (PDstandardfdOrder2122_impl(u,p1o2dxdy2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2122_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2122_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dxdy2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2133(u) ((2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dxdz2)
#else
#  define PDstandardfdOrder2133(u) (PDstandardfdOrder2133_impl(u,p1o2dxdz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2133_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2133_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dxdz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2211(u) ((2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dx2dy)
#else
#  define PDstandardfdOrder2211(u) (PDstandardfdOrder2211_impl(u,p1o2dx2dy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2211_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2211_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dx2dy;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2233(u) ((2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dydz2)
#else
#  define PDstandardfdOrder2233(u) (PDstandardfdOrder2233_impl(u,p1o2dydz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2233_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2233_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dydz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2311(u) ((2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dx2dz)
#else
#  define PDstandardfdOrder2311(u) (PDstandardfdOrder2311_impl(u,p1o2dx2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2311_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2311_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dx2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2322(u) ((2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dy2dz)
#else
#  define PDstandardfdOrder2322(u) (PDstandardfdOrder2322_impl(u,p1o2dy2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2322_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2322_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dy2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4122(u) ((240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0)) + 128*(KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,-1,2,0)) - 8*(KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,-2,1,0)) - 16*(KRANC_GFOFFSET3D(u,2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) + KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy2)
#else
#  define PDstandardfdOrder4122(u) (PDstandardfdOrder4122_impl(u,p1o144dxdy2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4122_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4122_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0)) + 128*(KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,-1,2,0)) - 8*(KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,-2,1,0)) - 16*(KRANC_GFOFFSET3D(u,2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) + KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4133(u) ((240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1)) + 128*(KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,-1,0,2)) - 8*(KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,-2,0,1)) - 16*(KRANC_GFOFFSET3D(u,2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) + KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz2)
#else
#  define PDstandardfdOrder4133(u) (PDstandardfdOrder4133_impl(u,p1o144dxdz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4133_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4133_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1)) + 128*(KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,-1,0,2)) - 8*(KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,-2,0,1)) - 16*(KRANC_GFOFFSET3D(u,2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) + KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4211(u) ((240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 128*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 16*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,-2,0)) - 16*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,2,0)) + 8*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) + KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dx2dy)
#else
#  define PDstandardfdOrder4211(u) (PDstandardfdOrder4211_impl(u,p1o144dx2dy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4211_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4211_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 128*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 16*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,-2,0)) - 16*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,2,0)) + 8*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) + KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dx2dy;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4233(u) ((240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1)) + 128*(KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,-1,2)) - 8*(KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,1,2)) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) + 16*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,-2,1)) - 16*(KRANC_GFOFFSET3D(u,0,2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) + KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz2)
#else
#  define PDstandardfdOrder4233(u) (PDstandardfdOrder4233_impl(u,p1o144dydz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4233_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4233_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1)) + 128*(KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,-1,2)) - 8*(KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,1,2)) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) + 16*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,-2,1)) - 16*(KRANC_GFOFFSET3D(u,0,2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) + KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4311(u) ((240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 128*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,1)) + 16*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,-2)) - 16*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,2)) + 8*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) + KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dx2dz)
#else
#  define PDstandardfdOrder4311(u) (PDstandardfdOrder4311_impl(u,p1o144dx2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4311_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4311_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 128*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,1)) + 16*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,-2)) - 16*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,2)) + 8*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) + KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dx2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4322(u) ((240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 128*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,1)) + 16*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,-2)) - 16*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,2)) + 8*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) + KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dy2dz)
#else
#  define PDstandardfdOrder4322(u) (PDstandardfdOrder4322_impl(u,p1o144dy2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4322_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4322_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 128*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,1)) + 16*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,-2)) - 16*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,2)) + 8*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) + KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dy2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2121(u) ((2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dx2dy)
#else
#  define PDstandardfdOrder2121(u) (PDstandardfdOrder2121_impl(u,p1o2dx2dy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2121_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2121_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dx2dy;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2131(u) ((2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dx2dz)
#else
#  define PDstandardfdOrder2131(u) (PDstandardfdOrder2131_impl(u,p1o2dx2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2131_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2131_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dx2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2212(u) ((2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dxdy2)
#else
#  define PDstandardfdOrder2212(u) (PDstandardfdOrder2212_impl(u,p1o2dxdy2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2212_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2212_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o2dxdy2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2232(u) ((2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dy2dz)
#else
#  define PDstandardfdOrder2232(u) (PDstandardfdOrder2232_impl(u,p1o2dy2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2232_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2232_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,0,-1) - 2*KRANC_GFOFFSET3D(u,0,0,1) - KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dy2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2313(u) ((2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dxdz2)
#else
#  define PDstandardfdOrder2313(u) (PDstandardfdOrder2313_impl(u,p1o2dxdz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2313_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2313_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,-1,0,0) - 2*KRANC_GFOFFSET3D(u,1,0,0) - KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o2dxdz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2323(u) ((2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dydz2)
#else
#  define PDstandardfdOrder2323(u) (PDstandardfdOrder2323_impl(u,p1o2dydz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2323_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2323_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o2dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (2*KRANC_GFOFFSET3D(u,0,-1,0) - 2*KRANC_GFOFFSET3D(u,0,1,0) - KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o2dydz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4121(u) ((240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 128*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 16*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,-2,0)) - 16*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,2,0)) + 8*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) + KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dx2dy)
#else
#  define PDstandardfdOrder4121(u) (PDstandardfdOrder4121_impl(u,p1o144dx2dy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4121_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4121_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 128*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 16*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,-2,0)) - 16*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,2,0)) + 8*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) + KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dx2dy;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4131(u) ((240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 128*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,1)) + 16*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,-2)) - 16*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,2)) + 8*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) + KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dx2dz)
#else
#  define PDstandardfdOrder4131(u) (PDstandardfdOrder4131_impl(u,p1o144dx2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4131_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4131_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dx2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 128*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,1)) + 16*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,-2)) - 16*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,2)) + 8*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) + KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dx2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4212(u) ((240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0)) + 128*(KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,-1,2,0)) - 8*(KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,-2,1,0)) - 16*(KRANC_GFOFFSET3D(u,2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) + KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy2)
#else
#  define PDstandardfdOrder4212(u) (PDstandardfdOrder4212_impl(u,p1o144dxdy2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4212_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4212_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,-1,1,0)) + 128*(KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,-1,2,0)) - 8*(KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,-2,1,0)) - 16*(KRANC_GFOFFSET3D(u,2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) - KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) + KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4232(u) ((240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 128*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,1)) + 16*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,-2)) - 16*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,2)) + 8*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) + KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dy2dz)
#else
#  define PDstandardfdOrder4232(u) (PDstandardfdOrder4232_impl(u,p1o144dy2dz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4232_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4232_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dy2dz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,0,-1) - 240*KRANC_GFOFFSET3D(u,0,0,1) - 30*KRANC_GFOFFSET3D(u,0,0,-2) + 30*KRANC_GFOFFSET3D(u,0,0,2) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 128*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,1)) + 16*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,-2)) - 16*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,2)) + 8*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) + KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dy2dz;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4313(u) ((240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1)) + 128*(KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,-1,0,2)) - 8*(KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,-2,0,1)) - 16*(KRANC_GFOFFSET3D(u,2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) + KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz2)
#else
#  define PDstandardfdOrder4313(u) (PDstandardfdOrder4313_impl(u,p1o144dxdz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4313_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4313_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,-1,0,0) - 240*KRANC_GFOFFSET3D(u,1,0,0) - 128*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,-1,0,1)) + 128*(KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,-1,0,2)) - 8*(KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2)) - 30*KRANC_GFOFFSET3D(u,-2,0,0) + 30*KRANC_GFOFFSET3D(u,2,0,0) + 16*(KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,-2,0,1)) - 16*(KRANC_GFOFFSET3D(u,2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) - KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) + KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4323(u) ((240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1)) + 128*(KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,-1,2)) - 8*(KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,1,2)) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) + 16*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,-2,1)) - 16*(KRANC_GFOFFSET3D(u,0,2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) + KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz2)
#else
#  define PDstandardfdOrder4323(u) (PDstandardfdOrder4323_impl(u,p1o144dydz2,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4323_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4323_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz2, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (240*KRANC_GFOFFSET3D(u,0,-1,0) - 240*KRANC_GFOFFSET3D(u,0,1,0) - 128*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,-1,1)) + 128*(KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,-1,2)) - 8*(KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,1,2)) - 30*KRANC_GFOFFSET3D(u,0,-2,0) + 30*KRANC_GFOFFSET3D(u,0,2,0) + 16*(KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,-2,1)) - 16*(KRANC_GFOFFSET3D(u,0,2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) - KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) + KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz2;
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2123(u) ((KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o4dxdy*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,3))
#else
#  define PDstandardfdOrder2123(u) (PDstandardfdOrder2123_impl(u,p1o4dxdy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2123_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2123_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o4dxdy*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,3);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2132(u) ((KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o4dxdz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,2))
#else
#  define PDstandardfdOrder2132(u) (PDstandardfdOrder2132_impl(u,p1o4dxdz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2132_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2132_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o4dxdz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,2);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2213(u) ((KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o4dxdy*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,3))
#else
#  define PDstandardfdOrder2213(u) (PDstandardfdOrder2213_impl(u,p1o4dxdy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2213_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2213_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (KRANC_GFOFFSET3D(u,-1,-1,0) - KRANC_GFOFFSET3D(u,-1,1,0) - KRANC_GFOFFSET3D(u,1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0))*p1o4dxdy*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,3);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2231(u) ((KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o4dydz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,1))
#else
#  define PDstandardfdOrder2231(u) (PDstandardfdOrder2231_impl(u,p1o4dydz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2231_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dydz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2231_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dydz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o4dydz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,1);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2312(u) ((KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o4dxdz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,2))
#else
#  define PDstandardfdOrder2312(u) (PDstandardfdOrder2312_impl(u,p1o4dxdz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2312_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2312_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (KRANC_GFOFFSET3D(u,-1,0,-1) - KRANC_GFOFFSET3D(u,-1,0,1) - KRANC_GFOFFSET3D(u,1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1))*p1o4dxdz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,2);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder2321(u) ((KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o4dydz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,1))
#else
#  define PDstandardfdOrder2321(u) (PDstandardfdOrder2321_impl(u,p1o4dydz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder2321_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dydz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder2321_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o4dydz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (KRANC_GFOFFSET3D(u,0,-1,-1) - KRANC_GFOFFSET3D(u,0,-1,1) - KRANC_GFOFFSET3D(u,0,1,-1) + KRANC_GFOFFSET3D(u,0,1,1))*p1o4dydz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,1,1);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4123(u) ((-64*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 64*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0) + KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) + KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,3))
#else
#  define PDstandardfdOrder4123(u) (PDstandardfdOrder4123_impl(u,p1o144dxdy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4123_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4123_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-64*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 64*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0) + KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) + KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,3);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4132(u) ((-64*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 64*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2) + KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) + KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,2))
#else
#  define PDstandardfdOrder4132(u) (PDstandardfdOrder4132_impl(u,p1o144dxdz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4132_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4132_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-64*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 64*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2) + KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) + KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,2);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4213(u) ((-64*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 64*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0) + KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) + KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,3))
#else
#  define PDstandardfdOrder4213(u) (PDstandardfdOrder4213_impl(u,p1o144dxdy,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4213_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4213_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdy, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-64*(KRANC_GFOFFSET3D(u,-1,1,0) + KRANC_GFOFFSET3D(u,1,-1,0)) + 64*(KRANC_GFOFFSET3D(u,-1,-1,0) + KRANC_GFOFFSET3D(u,1,1,0)) + 8*(KRANC_GFOFFSET3D(u,-1,2,0) + KRANC_GFOFFSET3D(u,1,-2,0) + KRANC_GFOFFSET3D(u,-2,1,0) + KRANC_GFOFFSET3D(u,2,-1,0)) - 8*(KRANC_GFOFFSET3D(u,-1,-2,0) + KRANC_GFOFFSET3D(u,1,2,0) + KRANC_GFOFFSET3D(u,-2,-1,0) + KRANC_GFOFFSET3D(u,2,1,0)) + KRANC_GFOFFSET3D(u,-2,-2,0) - KRANC_GFOFFSET3D(u,-2,2,0) - KRANC_GFOFFSET3D(u,2,-2,0) + KRANC_GFOFFSET3D(u,2,2,0))*p1o144dxdy*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,3);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4231(u) ((-64*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 64*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,2) + KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) + KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,1))
#else
#  define PDstandardfdOrder4231(u) (PDstandardfdOrder4231_impl(u,p1o144dydz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4231_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4231_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-64*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 64*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,2) + KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) + KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,1);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4312(u) ((-64*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 64*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2) + KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) + KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,2))
#else
#  define PDstandardfdOrder4312(u) (PDstandardfdOrder4312_impl(u,p1o144dxdz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4312_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4312_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dxdz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-64*(KRANC_GFOFFSET3D(u,-1,0,1) + KRANC_GFOFFSET3D(u,1,0,-1)) + 64*(KRANC_GFOFFSET3D(u,-1,0,-1) + KRANC_GFOFFSET3D(u,1,0,1)) + 8*(KRANC_GFOFFSET3D(u,-1,0,2) + KRANC_GFOFFSET3D(u,1,0,-2) + KRANC_GFOFFSET3D(u,-2,0,1) + KRANC_GFOFFSET3D(u,2,0,-1)) - 8*(KRANC_GFOFFSET3D(u,-1,0,-2) + KRANC_GFOFFSET3D(u,1,0,2) + KRANC_GFOFFSET3D(u,-2,0,-1) + KRANC_GFOFFSET3D(u,2,0,1)) + KRANC_GFOFFSET3D(u,-2,0,-2) - KRANC_GFOFFSET3D(u,-2,0,2) - KRANC_GFOFFSET3D(u,2,0,-2) + KRANC_GFOFFSET3D(u,2,0,2))*p1o144dxdz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,2);
}
#endif

#ifndef KRANC_DIFF_FUNCTIONS
#  define PDstandardfdOrder4321(u) ((-64*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 64*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,2) + KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) + KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,1))
#else
#  define PDstandardfdOrder4321(u) (PDstandardfdOrder4321_impl(u,p1o144dydz,cdj,cdk))
static CCTK_REAL PDstandardfdOrder4321_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz, const ptrdiff_t cdj, const ptrdiff_t cdk) CCTK_ATTRIBUTE_NOINLINE CCTK_ATTRIBUTE_UNUSED;
static CCTK_REAL PDstandardfdOrder4321_impl(const CCTK_REAL* restrict const u, const CCTK_REAL p1o144dydz, const ptrdiff_t cdj, const ptrdiff_t cdk)
{
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL);
  return (-64*(KRANC_GFOFFSET3D(u,0,-1,1) + KRANC_GFOFFSET3D(u,0,1,-1)) + 64*(KRANC_GFOFFSET3D(u,0,-1,-1) + KRANC_GFOFFSET3D(u,0,1,1)) + 8*(KRANC_GFOFFSET3D(u,0,-1,2) + KRANC_GFOFFSET3D(u,0,1,-2) + KRANC_GFOFFSET3D(u,0,-2,1) + KRANC_GFOFFSET3D(u,0,2,-1)) - 8*(KRANC_GFOFFSET3D(u,0,-1,-2) + KRANC_GFOFFSET3D(u,0,1,2) + KRANC_GFOFFSET3D(u,0,-2,-1) + KRANC_GFOFFSET3D(u,0,2,1)) + KRANC_GFOFFSET3D(u,0,-2,-2) - KRANC_GFOFFSET3D(u,0,-2,2) - KRANC_GFOFFSET3D(u,0,2,-2) + KRANC_GFOFFSET3D(u,0,2,2))*p1o144dydz*KrancTensor_Private_StandardCenteredDifferencingOperator(1,2,1);
}
#endif

