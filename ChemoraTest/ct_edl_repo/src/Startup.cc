/*  File produced by Kranc */

#include "cctk.h"

#include "Chemora.h"

extern "C" int ct_edl_Startup(void)
{
  const char* banner CCTK_ATTRIBUTE_UNUSED = "ct_edl";
  CCTK_RegisterBanner(banner);
  chemora_cg_thorn_startup();
  return 0;
}
