#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void CaCUDAToy_RegVar(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  /* Register all the evolved grid functions with CaCUDAMoL */
  MoLRegisterEvolved(CCTK_VarIndex("wavetoy::rho"),  CCTK_VarIndex("wavetoy::rhorhs"));
  MoLRegisterEvolved(CCTK_VarIndex("wavetoy::u"),  CCTK_VarIndex("wavetoy::urhs"));
  return;
}
