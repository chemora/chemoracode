#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

void CaCUDAToy_SelectBoundConds(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const char *bctype;

  bctype = NULL;
  if (CCTK_EQUALS(bound,"flat") || CCTK_EQUALS(bound,"static") ||
      CCTK_EQUALS(bound,"radiation") || CCTK_EQUALS(bound,"robin") ||
      CCTK_EQUALS(bound,"none"))
  {
    bctype = bound;
  }
  else if (CCTK_EQUALS(bound,"zero"))
  {
    bctype = "none";
  }

  /* Uses all default arguments, so invalid table handle -1 can be passed */
  if (bctype && Boundary_SelectVarForBC (cctkGH, CCTK_ALL_FACES, 1, -1,
                                         "wavetoy::u", bctype)
  && Boundary_SelectVarForBC (cctkGH, CCTK_ALL_FACES, 1, -1,
          "wavetoy::rho", bctype)< 0)
  {
    CCTK_WARN (0, "Error selecting boundary condition");
  }
}
