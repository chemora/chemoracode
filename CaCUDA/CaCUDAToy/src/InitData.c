#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#define SQR(val) ((val) * (val))

void CaCUDAToy_InitData( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i, j, k;
  int vindex;
  CCTK_REAL R, factor;

  for (k = 0; k < cctk_lsh[2]; k++)
  {
    for (j = 0; j < cctk_lsh[1]; j++)
    {
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        vindex = CCTK_GFINDEX3D(cctkGH, i, j, k);
        R = sqrt(
            x[vindex] * x[vindex] + y[vindex] * y[vindex]
                + z[vindex] * z[vindex]);
        u[vindex] = amplitude * exp(-SQR( (R - radius) / sigma ));

        factor = -2.0 * u[vindex] * (-radius + R) / (sigma * sigma * R);

        px[vindex] = factor * x[vindex];
        py[vindex] = factor * y[vindex];
        pz[vindex] = factor * z[vindex];
      }
    }
  }

}
