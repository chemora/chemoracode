#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "CaCUDA/CaCUDALib/src/CaCUDALib.h"


CCTK_CUDA_CALL CalcRHS (CCTK_CUDA_ARGUMENTS)
{

/* 2D tile in the shared memory */

  __shared__ CCTK_REAL smem2d[BLOCKDIMX + 2 * STENCILESIZE][BLOCKDIMY + 2 * STENCILESIZE];

  int ix = blockIdx.x * blockDim.x + threadIdx.x;
  int iy = blockIdx.y * blockDim.y + threadIdx.y;
  int idx_in = iy * cagh_ni + ix;              // index for reading input
  int idx_out = 0;                            // index for writing output
  int stride = cagh_ni * cagh_nj;               // distance between 2D slices (in elements)

  CCTK_REAL zp[STENCILESIZE];                     // variables for input “in front of” the current slice
  CCTK_REAL zm[STENCILESIZE];                     // variables for input “behind” the current slice

  CCTK_REAL current;                              // input value in the current slice
  int tx = threadIdx.x + STENCILESIZE;        // thread’s x-index into corresponding shared memory tile (adjusted for halos)
  int ty = threadIdx.y + STENCILESIZE;        // thread’s y-index into corresponding shared memory tile (adjusted for halos)

  /* short hands for finite difference */
  CCTK_REAL dx2i = 1.0/(cagh_dx * cagh_dx);
  CCTK_REAL dy2i = 1.0/(cagh_dy * cagh_dy);
  CCTK_REAL dz2i = 1.0/(cagh_dz * cagh_dz);

/* fill the front and behind data along Z-axis */

/*
  zm[2] = cagh_bufferIn[idx_in];
  idx_in += stride;

  zm[1] = cagh_bufferIn[idx_in];
  idx_in += stride;
*/

  zm[0] = cagh_bufferIn[idx_in];
  idx_in += stride;

  current = cagh_bufferIn[idx_in];
  idx_out = idx_in;
  idx_in += stride;

  zp[0] = cagh_bufferIn[idx_in];
  idx_in += stride;

/*
  zp[1] = cagh_bufferIn[idx_in];
  idx_in += stride;

  zp[2] = cagh_bufferIn[idx_in];
  idx_in += stride;

  zp[3] = cagh_bufferIn[idx_in];
  idx_in += stride;
*/

  for (int i = STENCILESIZE; i < cagh_nk - STENCILESIZE; i++)
  {
/* advance the slice (move the thread-front) */
/*
	      zm[3] = zm[2];
	      zm[2] = zm[1];
	      zm[1] = zm[0];
*/
	      zm[0] = current;
	      current = zp[0];
/*
	      zp[0] = zp[1];
	      zp[1] = zp[2];
	      zp[2] = zp[3];
*/
	      zp[0] = cagh_bufferIn[idx_in];

	      idx_in += stride;
	      idx_out += stride;
	      __syncthreads ();

	  /* update the data slice in smem2d */

	      if (threadIdx.y < STENCILESIZE)   // halo above/below
	      {
	        smem2d[threadIdx.y][tx] = cagh_bufferIn[idx_out - STENCILESIZE * cagh_ni];
	        smem2d[threadIdx.y + BLOCKDIMY + STENCILESIZE][tx] = cagh_bufferIn[idx_out + BLOCKDIMY * cagh_ni];
	      }
	      if (threadIdx.x < STENCILESIZE)   // halo left/right
	      {
	        smem2d[ty][threadIdx.x] = cagh_bufferIn[idx_out - STENCILESIZE];
	        smem2d[ty][threadIdx.x + BLOCKDIMX + STENCILESIZE] = cagh_bufferIn[idx_out + BLOCKDIMX];
	      }

/* update the slice in smem2d */

	      smem2d[ty][tx] = current;
	      __syncthreads ();

/* insert the numerical code here */

	      cagh_bufferOut[idx_out] = dx2i * (smem2d[tx + 1][ty    ] + smem2d[tx - 1][ty    ] - 2.0 * current)
		                          + dy2i * (smem2d[tx    ][ty + 1] + smem2d[tx    ][ty - 1] - 2.0 * current)
                                  + dz2i * (zp[0] + zm[0] - 2.0 * current);
  }
}

void  CaCUDAToy_CalcRHS(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  DECLARE_CCTK_CUDA_ARGUMENTS;

  int i,j,k;
  int istart, jstart, kstart, iend, jend, kend;
  istart = 1;
  jstart = 1;
  kstart = 1;

  iend = cctk_lsh[0] - 1;
  jend = cctk_lsh[1] - 1;
  kend = cctk_lsh[2] - 1;

  dim3              dimGrid;
  dim3              dimBlock;

  const size_t      volumeSize = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2];

  CUDA_SAFE_CALL(cudaMemcpy(d_bufferIn, CCTK_VarDataPtrI(cctkGH, 0, CCTK_VarIndex("wavetoy::u")), volumeSize * sizeof(CCTK_REAL), cudaMemcpyHostToDevice));

  dimBlock.x = BLOCKDIMX;
  dimBlock.y = BLOCKDIMY;
  dimBlock.z = BLOCKDIMZ;

  dimGrid.x  = (unsigned int)ceil((CCTK_REAL)cctk_lsh[0] / dimBlock.x);
  dimGrid.y  = (unsigned int)ceil((CCTK_REAL)cctk_lsh[1] / dimBlock.y);
  dimGrid.z  = 1;

  CCTK_CUDA_EXECUTE(CalcRHS,dimGrid,dimBlock,CCTK_CUDA_ARGUMENT_LIST);

/* calculate rhorhs on GPU*/

/* urhs will be set on CPU asynchronously */
  for (k = kstart; k < kend; k++)
    for (j = jstart; j < jend; j++)
      for (i = istart; i < iend; i++)
      {
          urhs[CCTK_GFINDEX3D(cctkGH,i,j,k)] = rho[CCTK_GFINDEX3D(cctkGH,i,j,k)];
    }

  CUDA_SAFE_CALL(cudaMemcpy(CCTK_VarDataPtrI(cctkGH, 0, CCTK_VarIndex("wavetoy::rhorhs")), d_bufferOut, volumeSize * sizeof(CCTK_REAL), cudaMemcpyDeviceToHost));

}
