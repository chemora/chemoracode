#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void  CaCUDAToy_CalcRHS(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int i,j,k;
  int vindex;
  int istart, jstart, kstart, iend, jend, kend;
  CCTK_REAL dx,dy,dz,dt,dx2,dy2,dz2,dt2,dx2i,dy2i,dz2i;
  /* Set up shorthands */
  /* Set up shorthands */
  dx = CCTK_DELTA_SPACE(0);
  dy = CCTK_DELTA_SPACE(1);
  dz = CCTK_DELTA_SPACE(2);
  dt = CCTK_DELTA_TIME;

  dx2 = dx*dx;
  dy2 = dy*dy;
  dz2 = dz*dz;
  dt2 = dt*dt;

  dx2i = 1.0/dx2;
  dy2i = 1.0/dy2;
  dz2i = 1.0/dz2;

  istart = 0;
  jstart = 0;
	kstart = 0;

	iend = cctk_lsh[0];
	jend = cctk_lsh[1];
	kend = cctk_lsh[2];

  for (k = kstart; k < kend; k++)
    for (j = jstart; j < jend; j++)
      for (i = istart; i < iend; i++)
      {
          vindex =  CCTK_GFINDEX3D(cctkGH,i,j,k);
          printf("u[%d]=%lf\n", vindex, u[vindex]);
/*
          rhorhs[vindex] =
           ( u[CCTK_GFINDEX3D(cctkGH,i+1,j  ,k  )]
           + u[CCTK_GFINDEX3D(cctkGH,i-1,j  ,k  )]
               - 2.0 * u[vindex])*dx2i
          +( u[CCTK_GFINDEX3D(cctkGH,i  ,j+1,k  )]
           + u[CCTK_GFINDEX3D(cctkGH,i  ,j-1,k  )]
               - 2.0 * u[vindex])*dy2i
          +( u[CCTK_GFINDEX3D(cctkGH,i  ,j  ,k+1)]
           + u[CCTK_GFINDEX3D(cctkGH,i  ,j,  k-1)]
               - 2.0 * u[vindex])*dz2i;
          urhs[vindex] = rho[vindex];
          */
    }

}
