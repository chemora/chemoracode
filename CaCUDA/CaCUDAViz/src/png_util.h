#ifndef _PNG_UTIL_H_
#define _PNG_UTIL_H_

#ifdef __cplusplus
extern "C"
{
#endif

  void *read_png (const char *, int *, int *, int *, int *);
  int write_png (const char *, int, int, int, int, const void *);

  void *read_raw (const char *, int *, int *, int *, int *);
  void write_raw (const char *, int, int, int, int, const void *);

  void *read_img (const char *, int *, int *, int *, int *);
  void write_img (const char *, int, int, int, int, const void *);

#ifdef __cplusplus
}                               /* extern "C" */
#endif

#endif                          /* _PNG_UTIL_H_ */
