 /*@@
    @file      Startup.c
    @date      18 April 2011
    @author    Thomas Radke, Jian Tao
    @desc
    Startup routines for CaCUDAViz.
    @enddesc
    @@ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cctk.h"
#include "cctk_IOMethods.h"
#include "cctk_Parameters.h"
#include "CaCUDAViz.h"

/********************************************************************
 ********************    External Routines   ************************
 ********************************************************************/
int CaCUDAViz_Startup (void);

/********************************************************************
 ********************    Internal Routines   ************************
 ********************************************************************/
static void *SetupGH (tFleshConfig * config, int conv_level, cGH * GH);

 /*@@
    @routine   CaCUDAViz_Startup
    @date      Thu 18 April 2011
    @author    Thomas Radke, Jian Tao
    @desc
    The startup registration routine for CaCUDAViz.
    @enddesc
    @calls     CCTK_RegisterGHExtension
    CCTK_RegisterGHExtensionSetupGH
    @@ */

int CaCUDAViz_Startup (void)
{
  int extension;

  /* register a new GH extension with a unique name */
  extension = CCTK_RegisterGHExtension ("CaCUDAViz");
  if (extension >= 0)
  {
    CCTK_RegisterGHExtensionSetupGH (extension, SetupGH);
  }
  else
  {
    CCTK_WARN (1, "Couldn't register GH extension 'CaCUDAViz'. "
               "This I/O method will not be registered");
  }
  return (0);
}

/********************************************************************
 ********************    Internal Routines   ************************
 ********************************************************************/
 /*@@
    @routine   SetupGH
    @date      Apr 18 2011
    @author    Thomas Radke, Jian Tao
    @desc
    Allocates and sets up the GH extension structure
    @enddesc
    @returntype void *
    @returndesc
    pointer to the allocated GH extension structure
    @endreturndesc
    @@ */

static void *SetupGH (tFleshConfig * config, int conv_level, cGH * GH)
{
  int i, numvars;
  CaCUDAVizGH *myGH;
  const char *my_out_dir;

  DECLARE_CCTK_PARAMETERS
    /* suppress compiler warnings about unused variables */
  (void) (config + 0);
  (void) (conv_level + 0);
  (void) (GH + 0);

  /* allocate the GH extension and its components */
  if ((myGH = (CaCUDAVizGH *) malloc (sizeof (CaCUDAVizGH))) == NULL)
  {
    CCTK_WARN (0, "Unable to allocate memory for GH");
  }

  /* register routines as a new I/O method "CaCUDAViz" */
  if ((i = CCTK_RegisterIOMethod ("CaCUDAViz")) >= 0)
  {
    CCTK_RegisterIOMethodOutputGH (i, CaCUDAViz_OutputGH);
    CCTK_RegisterIOMethodTimeToOutput (i, CaCUDAViz_TimeToOutput);
    CCTK_RegisterIOMethodTriggerOutput (i, CaCUDAViz_TriggerOutput);
    CCTK_RegisterIOMethodOutputVarAs (i, CaCUDAViz_OutputVarAs);
  }
  else
  {
    CCTK_WARN (0, "failed to register the IO method ");
  }

  /* announcing this new I/O method to the user */
  if (!CCTK_Equals (verbose, "none"))
  {
    CCTK_INFO ("I/O Method 'CaCUDAViz' registered");
    CCTK_INFO ("CaCUDAViz: Volume rendering of 3D grid functions/arrays "
               "on GPUs");
  }

  /* now allocate and initialize the GH extension structure's components */
  numvars = CCTK_NumVars ();
  myGH->out_every = (CCTK_INT *) malloc (numvars * sizeof (CCTK_INT));
  myGH->out_last = (int *) malloc (numvars * sizeof (int));

  for (i = 0; i < numvars; i++)
  {
    myGH->out_last[i] = -1;
  }

  myGH->out_vars = strdup ("");
  myGH->out_every_default = out_every - 1;

  /* get the name for CaCUDA's output directory */
  my_out_dir = *out_dir ? out_dir : io_out_dir;

  /* omit the directory if it's the current working dir */
  if (strcmp (my_out_dir, ".") == 0)
  {
    myGH->out_dir = strdup ("");
  }
  else
  {
    myGH->out_dir = (char *) malloc (strlen (my_out_dir) + 2);
    sprintf (myGH->out_dir, "%s/", my_out_dir);
  }

  /* create the output dir */
  i = IOUtil_CreateDirectory (GH, myGH->out_dir, 0, 0);
  if (i < 0)
  {
    CCTK_VWarn (1, __LINE__, __FILE__, CCTK_THORNSTRING,
                "Problem creating CaCUDAViz output directory '%s'",
                myGH->out_dir);
  }
  else if (i >= 0 && CCTK_Equals (verbose, "full"))
  {
    CCTK_VInfo (CCTK_THORNSTRING, "CaCUDAViz: Output to directory '%s'",
                myGH->out_dir);
  }


  myGH->stop_on_parse_errors = strict_io_parameter_check;
  CaCUDAViz_CheckSteerableParameters (GH, myGH);
  myGH->stop_on_parse_errors = 0;

  return (myGH);
}
