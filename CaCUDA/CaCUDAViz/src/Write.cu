 /*@@
    @header    Write.cu
    @date      Apr 18 2011
    @author    Jian Tao, Jinghua Ge, Thomas Radke
    @desc
    simple volume render code in CUDA.
    @enddesc
    @version
    @@ */

#if defined __CUDACC__
#include <cuda.h>
#include <cuda_runtime.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include "png_util.h"
#include "transform_matrix.h"
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"

#include "CactusBase/IOUtil/src/ioutil_AdvertisedFiles.h"
#include "CaCUDAViz.h"
#include "transform_matrix.h"
#include "volumeRender_kernel.h"

/* locally defined shortcuts */
typedef unsigned int uint;
typedef unsigned char uchar;
typedef float VolumeType;

#define iDivUp(a, b)   ((a + b - 1) / b)
#define x(a)   ((float)(a))

/********************************************************************
 ********************    Internal Routines   ************************
 ********************************************************************/
static void WriteData (const cGH * GH, int vindex, const char *alias);

/*@@
   @routine   CaCUDAViz_Write
   @date      Thu 18 April 2002
   @author    Jian Tao, Jinghua Ge
   @desc
              Volume rendering of a given variable
   @enddesc
   @var       GH
   @vdesc     pointer to CCTK GH
   @vtype     const cGH *
   @vio       in
   @endvar
   @var       vindex
   @vdesc     index of variable to output
   @vtype     int
   @vio       in
   @endvar
   @var       alias
   @vdesc     alias of variable
   @vtype     cont char *
   @vio       in
   @endvar
   @returntype int
   @returndesc
                0 for success, or<BR>
               -1 if variable has no storage assigned<BR>
   @endreturndesc
@@*/
extern "C" int CaCUDAViz_Write (const cGH * GH, int vindex, const char *alias)
{
  int myproc;

  char *fullname;
  DECLARE_CCTK_PARAMETERS;

  /* get the full variable name (used for error messages) */
  fullname = CCTK_FullName (vindex);

  /* check if variable has storage assigned */
  if (!CCTK_QueryGroupStorageI (GH, CCTK_GroupIndexFromVarI (vindex)))
  {
    CCTK_VWarn (2, __LINE__, __FILE__, CCTK_THORNSTRING,
                "No CaCUDAViz output for '%s' (no storage)", fullname);
    free (fullname);
    return (-1);
  }

  myproc = CCTK_MyProc (GH);

  /* and dump the data to file */
  if (myproc == 0)
  {
    WriteData (GH, vindex, alias);
  }
  return (0);
}


static void WriteData (const cGH * GH, int vindex, const char *alias)
{
  char *filename, *tmpfilename, *fullname;
  ioAdvertisedFileDesc advertised_file;
  float invViewMatrix[16];
  dim3 gridSize;
  // cudaExtent volumeSize = make_cudaExtent (32, 32, 32);
  float *floatgf;
  cudaExtent volumeSize;
  DECLARE_CCTK_PARAMETERS

  uint width, height;
  float trl[3], rot[3];
  float density, brightness, transferOffset, transferScale;

  dim3 blockSize (16, 16);

  const CaCUDAVizGH *myGH;
/*
 * this should be ok since we check image_width and image_height
 * to make sure that they are positive.
 */

  width  = image_width;
  height = image_height;

  trl[0] = x(translation_x);
  trl[1] = x(translation_y);
  trl[2] = x(translation_z);

  rot[0] = x(rotation_x);
  rot[1] = x(rotation_y);
  rot[2] = x(rotation_z);

  density = x(colormap_density);
  brightness = x(colormap_brightness);
  transferOffset = x(colormap_transferoffset);
  transferScale = x(colormap_transferscale);

  myGH = (const CaCUDAVizGH *) CCTK_GHExtension (GH, "CaCUDAViz");

//  used in OpenGL which could be used to project on a screen.
  //bool linearFiltering = true;
  //printf("WaveToyCUDA_analyse : colormap => black->red->yellow->green->cyan->blue->magenta->white\n");

//mv stands for model view matrix

  float mv[16] = {
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1
  };

//transform backwards to get the inverse of modleview matrix
//homogenous coordinate system
  translateHmg (trl, mv);
  rotateHmg (rot, mv);

//transpose form opengl definition to use in cuda implementation
  transposeHmg (mv, invViewMatrix);

/*
    float *p = invViewMatrix;
    p[0] = 1.; p[1] = 0.; p[2] = 0.; p[3] = -viewTranslation.x;
    p[4] = 0.; p[5] = 1.; p[6] = 0.; p[7] = -viewTranslation.y;
    p[8] = 0.; p[9] = 0.; p[10] = 1.; p[11] = -viewTranslation.z;
*/
  copyInvViewMatrix (invViewMatrix, sizeof (float4) * 3);

  const uint np = GH->cctk_lsh[0] * GH->cctk_lsh[1] * GH->cctk_lsh[2];
  volumeSize =
    make_cudaExtent (GH->cctk_lsh[0], GH->cctk_lsh[1], GH->cctk_lsh[2]);
  floatgf = 
    (float *) malloc ( np * sizeof(float));
  if(floatgf == NULL)
  {
    CCTK_WARN(0, "failed to allocate memory for floatgf !");
  }

  for (int i = 0; i <= np; i++)
  {
    floatgf[i] = x(*((CCTK_REAL *)CCTK_VarDataPtrI(GH, 0, vindex) + i));
  }
//using u, which is copied out from the cctk_driver->u in CactusCaCUDA,
//double GPU-CPU mem copy. Normal Cactus single copy from CPU to GPU. 
//also apply to rho

  /* data[var_num][TIMELEVEL][xyz] */
  /* use 0th timelevel
   * TODO: check if this is the previous timelevel or current.
   * */
  initCuda (floatgf, volumeSize);

  CCTK_VInfo (CCTK_THORNSTRING, "init Cuda 3D array (%d,%d,%d)",
              (int) volumeSize.width, (int) volumeSize.height,
              (int) volumeSize.depth);

  uint *output, *d_output;

  int pixelbuffersize = width * height * sizeof (uint);
  output = (uint *) malloc (pixelbuffersize);
  cudaMalloc ((void **) &d_output, pixelbuffersize);

  cudaMemset (d_output, 255, pixelbuffersize);

  // calculate grid size
  gridSize = dim3 (iDivUp (width, blockSize.x), iDivUp (height, blockSize.y));

  render_kernel (gridSize, blockSize,
                 d_output,
                 width, height,
                 density, brightness, transferOffset, transferScale);

  CCTK_INFO ("copy image buffer from GPU to CPU...");

  //copy the image data back
  cudaMemcpy (output, d_output, pixelbuffersize, cudaMemcpyDeviceToHost);

  CCTK_INFO ("free memory buffers");

  freeCudaBuffers ();

/* finished rendering, now we need to write to file */

  filename = (char *) malloc (strlen (myGH->out_dir) + strlen (alias) + 20);

  if (CCTK_Equals (mode, "remove"))
  {
    sprintf (filename, "%s%s.png", myGH->out_dir, alias);
    tmpfilename = (char *) malloc (strlen (filename) + 5);
    sprintf (tmpfilename, "%s.tmp", filename);
  }
  else
  {
    sprintf (filename, "%s%s.it_%d.png", myGH->out_dir, alias,
             GH->cctk_iteration);
    tmpfilename = NULL;
  }

  /* Write a PNG file to be advertised to a temporary file first
     and rename it later.
     This fixes a racing problem with HTTPD when running with pthreads support:
     a file could be downloaded (through the HTTPD thread) while is was still
     being written to (in the main simulation thread).
     Now the PNG is written to a temporary file first and then (atomically)
     renamed. */

  if (write_png (tmpfilename ? tmpfilename : filename, width, height, 4, 1, output) == 0)
  {
    /* in "remove" mode: rename and advertise the file for downloading */
    if (tmpfilename)
    {
      if (rename (tmpfilename, filename))
      {
        CCTK_VWarn (1, __LINE__, __FILE__, CCTK_THORNSTRING,
                    "Cannot rename temporary output file '%s' into '%s'",
                    tmpfilename, filename);
      }
      else
      {
        fullname = CCTK_FullName (vindex);
        advertised_file.slice = NULL;
        advertised_file.thorn = CCTK_THORNSTRING;
        advertised_file.varname = fullname;
        advertised_file.description = "Png of slices";
        advertised_file.mimetype = "image/png";

        IOUtil_AdvertiseFile (GH, filename, &advertised_file);

        free (fullname);
      }
    }
  }
  else
  {
    CCTK_VWarn (0, __LINE__, __FILE__, CCTK_THORNSTRING,
                "Cannot open CaCUDAViz %s output file '%s'",
                tmpfilename ? "temporary" : "",
                tmpfilename ? tmpfilename : filename);
  }

  free (output);
  free (filename);
  free (tmpfilename);
  free (floatgf);
  cudaFree (d_output);
}
