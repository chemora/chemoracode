#include <stdlib.h>
#include <stdio.h>
#include <math.h>


#include "transform_matrix.h"

#define dtor(a) (3.1416*a/180.0)


//-----------------------------------------------------------------------------
//4x4 matrix m = a * b 
//the matrix is saved column-wise as a 1D array
//-----------------------------------------------------------------------------
void multiply44 (float m[16], float a[16], float b[16])
{
  m[0] = b[0] * a[0] + b[1] * a[4] + b[2] * a[8] + b[3] * a[12];
  m[1] = b[0] * a[1] + b[1] * a[5] + b[2] * a[9] + b[3] * a[13];
  m[2] = b[0] * a[2] + b[1] * a[6] + b[2] * a[10] + b[3] * a[14];
  m[3] = b[0] * a[3] + b[1] * a[7] + b[2] * a[11] + b[3] * a[15];

  m[4] = b[4] * a[0] + b[5] * a[4] + b[6] * a[8] + b[7] * a[12];
  m[5] = b[4] * a[1] + b[5] * a[5] + b[6] * a[9] + b[7] * a[13];
  m[6] = b[4] * a[2] + b[5] * a[6] + b[6] * a[10] + b[7] * a[14];
  m[7] = b[4] * a[3] + b[5] * a[7] + b[6] * a[11] + b[7] * a[15];

  m[8] = b[8] * a[0] + b[9] * a[4] + b[10] * a[8] + b[11] * a[12];
  m[9] = b[8] * a[1] + b[9] * a[5] + b[10] * a[9] + b[11] * a[13];
  m[10] = b[8] * a[2] + b[9] * a[6] + b[10] * a[10] + b[11] * a[14];
  m[11] = b[8] * a[3] + b[9] * a[7] + b[10] * a[11] + b[11] * a[15];

  m[12] = b[12] * a[0] + b[13] * a[4] + b[14] * a[8] + b[15] * a[12];
  m[13] = b[12] * a[1] + b[13] * a[5] + b[14] * a[9] + b[15] * a[13];
  m[14] = b[12] * a[2] + b[13] * a[6] + b[14] * a[10] + b[15] * a[14];
  m[15] = b[12] * a[3] + b[13] * a[7] + b[14] * a[11] + b[15] * a[15];
}

//-----------------------------------------------------------------------------
//input : rotation angle[3] around x, y ,z
//output: rotation matrix m in homogeneous coord system
//-----------------------------------------------------------------------------

void rotateHmg (float angle[3], float m[16])
{
  float pm[16];
  memcpy (pm, m, 16 * sizeof (float));

  float rxc = cos (dtor (angle[0]));
  float rxs = sin (dtor (angle[0]));

  float ryc = cos (dtor (angle[1]));
  float rys = sin (dtor (angle[1]));

  float rzc = cos (dtor (angle[2]));
  float rzs = sin (dtor (angle[2]));

  float rotx[16] = { 1, 0, 0, 0,
    0, rxc, rxs, 0,
    0, -rxs, rxc, 0,
    0, 0, 0, 1
  };

  float roty[16] = { ryc, 0, -rys, 0,
    0, 1, 0, 0,
    rys, 0, ryc, 0,
    0, 0, 0, 1
  };

  float rotz[16] = { rzc, rzs, 0, 0,
    -rzs, rzc, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1
  };

  multiply44 (m, rotx, pm);
  multiply44 (pm, roty, m);
  multiply44 (m, rotz, pm);
}


//-----------------------------------------------------------------------------
//input : translate trl[3] in x, y , z direction
//output: homogeneous matrix m
//-----------------------------------------------------------------------------

void translateHmg (float trl[3], float m[16])
{
  m[12] += trl[0];
  m[13] += trl[1];
  m[14] += trl[2];
}


//-----------------------------------------------------------------------------
//input : Homogeneous matrix m
//output: transpose mt
//-----------------------------------------------------------------------------

void transposeHmg (float m[16], float mt[16])
{
  mt[0] = m[0];
  mt[4] = m[1];
  mt[8] = m[2];
  mt[12] = m[3];
  mt[1] = m[4];
  mt[5] = m[5];
  mt[9] = m[6];
  mt[13] = m[7];
  mt[2] = m[8];
  mt[6] = m[9];
  mt[10] = m[10];
  mt[14] = m[11];
  mt[3] = m[12];
  mt[7] = m[13];
  mt[11] = m[14];
  mt[15] = m[15];
}
