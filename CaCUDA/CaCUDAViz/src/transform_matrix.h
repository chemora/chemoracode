#ifndef _TRANSFORM_MATRIX_H_
#define _TRANSFORM_MATRIX_H_

#ifdef __cplusplus
extern "C"
{
#endif

  void multiply44 (float m[16], float a[16], float b[16]);
  void rotateHmg (float angle[3], float m[16]);
  void translateHmg (float trl[3], float m[16]);
  void transposeHmg (float m[16], float mt[16]);

#ifdef __cplusplus
}                               /* extern "C" */
#endif

#endif                          /* _TRANSFORM_MATRIX_H_ */
