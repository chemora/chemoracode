 /*@@
    @header    CaCUDAViz.h
    @date      Apr 18 2011
    @author    Jian Tao
    @desc
    Header file for CaCUDAViz
    @enddesc
    @version
    @@ */

#ifndef _CACUDAVIZ_H_
#define _CACUDAVIZ_H_

#ifdef __cplusplus
extern "C"
{
#endif

  typedef struct _CaCUDAVizGH_
  {
    /* default number of times to output */
    int out_every_default;

    /* number of times to output each individual variable */
    CCTK_INT *out_every;

    /* the last iteration output for each variable */
    int *out_last;

    /* current list of variables to output */
    char *out_vars;

    /* directories in which to output */
    char *out_dir;

    /* stop on I/O parameter parsing errors ? */
    int stop_on_parse_errors;

  } CaCUDAVizGH;

/* prototypes of functions to be registered for IO method */
  int CaCUDAViz_OutputGH (const cGH * GH);
  int CaCUDAViz_TimeToOutput (const cGH * GH, int);
  int CaCUDAViz_TriggerOutput (const cGH * GH, int);
  int CaCUDAViz_OutputVarAs (const cGH * GH, const char *var,
                             const char *alias);
/* other function prototypes for thorn-internal routines */
  int CaCUDAViz_Write (const cGH * GH, int vindex, const char *alias);
  void CaCUDAViz_CheckSteerableParameters (const cGH * GH,
                                           CaCUDAVizGH * myGH);

#ifdef __cplusplus
}                               // extern "C"
#endif

#endif                          /* _CACUDAVIZ_H_ */
