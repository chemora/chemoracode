

#ifndef __VOLUMERENDER_KERNEL_H_
#define __VOLUMERENDER_KERNEL_H_

extern "C" void setTextureFilterMode (bool bLinearFilter);
extern "C" void initCuda (void *h_volume, cudaExtent volumeSize);
extern "C" void freeCudaBuffers ();
extern "C" void render_kernel (dim3 gridSize, dim3 blockSize, uint * d_output,
                               uint imageW, uint imageH, float density,
                               float brightness, float transferOffset,
                               float transferScale);
extern "C" void copyInvViewMatrix (float *invViewMatrix, size_t sizeofMatrix);


#endif
