 /*@@
    @file      Output.c
    @date      Apr 18 2011
    @author    Thomas Radke, Jian Tao
    @desc
    Implementation of registered functions for IO method
    @enddesc
    @version
    @@ */

#include "cctk.h"
#include "cctk_Parameters.h"
#include "CaCUDAViz.h"

/*@@
   @routine    CaCUDAViz_OutputGH
   @date       Apr 18 2012
   @author     Jian Tao
   @desc
               Loops over all variables and outputs them if necessary
   @enddesc
   @returntype int
   @returndesc
               the number of variables which were output at this iteration
               (or 0 if it wasn't time to output yet)
   @endreturndesc
@@*/

static int CheckOutputVar (int vindex);
static void SetOutputFlag (int vindex, const char *optstring, void *arg);

int CaCUDAViz_OutputGH (const cGH * GH)
{
  int vindex, retval;
  const CaCUDAVizGH *myGH;


  retval = 0;
  myGH = CCTK_GHExtension (GH, "CaCUDAViz");

  /* loop over all variables */
  for (vindex = CCTK_NumVars () - 1; vindex >= 0; vindex--)
  {
    if (CaCUDAViz_TimeToOutput (GH, vindex) &&
        CaCUDAViz_Write (GH, vindex, CCTK_VarName (vindex)) == 0)
    {
      /* register variable as having output this iteration */
      myGH->out_last[vindex] = GH->cctk_iteration;
      retval++;
    }
  }

  return (retval);
}


/*@@
   @routine    CaCUDAViz_OutputVarAs
   @date       Thu 18 April 2002
   @author     Thomas Radke, Jian Tao
   @desc
               Unconditional output of a variable using the CaCUDAViz I/O method.
   @enddesc
   @calls      CaCUDAViz_Write

   @var        GH
   @vdesc      pointer to CCTK GH
   @vtype      const cGH *
   @vio        in
   @endvar
   @var        fullname
   @vdesc      complete name of variable to output
   @vtype      const char *
   @vio        in
   @endvar
   @var        alias
   @vdesc      alias name of variable to output
               (used to generate output filename)
   @vtype      const char *
   @vio        in
   @endvar

   @returntype int
   @returndesc
               return code of @seeroutine CaCUDAViz_Write, or<BR>
               -1 if variable cannot be output by CaCUDAViz
   @endreturndesc
@@*/
int CaCUDAViz_OutputVarAs (const cGH * GH, const char *fullname,
                           const char *alias)
{
  int vindex, retval;


  retval = -1;
  vindex = CCTK_VarIndex (fullname);

  if (CheckOutputVar (vindex) == 0)
  {
    retval = CaCUDAViz_Write (GH, vindex, alias);
  }

  return (retval);
}


 /*@@
    @routine    CaCUDAViz_TimeToOutput
    @date       Thu 18 April 2002
    @author     Thomas Radke
    @desc
    Decides if it is time to output a variable
    using the CaCUDAViz I/O method.
    @enddesc

    @var        GH
    @vdesc      pointer to CCTK GH
    @vtype      const cGH *
    @vio        in
    @endvar
    @var        vindex
    @vdesc      index of variable
    @vtype      int
    @vio        in
    @endvar

    @returntype int
    @returndesc
    1 if output should take place at this iteration, or<BR>
    0 if not
    @endreturndesc
    @@ */
int CaCUDAViz_TimeToOutput (const cGH * GH, int vindex)
{
  int result;
  char *fullname;
  CaCUDAVizGH *myGH;


  myGH = CCTK_GHExtension (GH, "CaCUDAViz");
  CaCUDAViz_CheckSteerableParameters (GH, myGH);

  /* check if this variable should be output */
  result = myGH->out_every[vindex] > 0 &&
    GH->cctk_iteration % myGH->out_every[vindex] == 0;
  if (result)
  {
    /* check if variable wasn't already output this iteration */
    if (myGH->out_last[vindex] == GH->cctk_iteration)
    {
      fullname = CCTK_FullName (vindex);
      CCTK_VWarn (5, __LINE__, __FILE__, CCTK_THORNSTRING,
                  "Already done CaCUDAViz output for '%s' in current "
                  "iteration (probably via triggers)", fullname);
      free (fullname);
      result = 0;
    }
  }

  return (result);
}


/*@@
   @routine    CaCUDAViz_TriggerOutput
   @date       Thu 18 April 2002
   @author     Thomas Radke
   @desc
               Triggers the output of a variable using the CaCUDAViz I/O method.
   @enddesc
   @calls      CaCUDAViz_Write

   @var        GH
   @vdesc      pointer to CCTK GH
   @vtype      const cGH *
   @vio        in
   @endvar
   @var        vindex
   @vdesc      index of variable to output
   @vtype      int
   @vio        in
   @endvar

   @returntype int
   @returndesc
               return code of @seeroutine CaCUDAViz_Write
   @endreturndesc
@@*/
int CaCUDAViz_TriggerOutput (const cGH * GH, int vindex)
{
  int retval;
  const CaCUDAVizGH *myGH;


  /* do the  output */
  retval = CaCUDAViz_Write (GH, vindex, CCTK_VarName (vindex));
  if (retval == 0)
  {
    /* register variable as having output this iteration */
    myGH = CCTK_GHExtension (GH, "CaCUDAViz");
    myGH->out_last[vindex] = GH->cctk_iteration;
  }

  return (retval);
}


/*@@
   @routine    CaCUDAViz_CheckSteerableParameters
   @date       Mon Oct 10 2000
   @author     Thomas Radke
   @desc
               Checks if CaCUDAViz steerable parameters were changed
               and does appropriate re-evaluation.
   @enddesc

   @calls      CCTK_TraverseString

   @var        myGH
   @vdesc      pointer to CaCUDAViz grid hierarchy
   @vtype      CaCUDAVizGH*
   @vio        inout
   @endvar
@@*/
void CaCUDAViz_CheckSteerableParameters (const cGH * GH, CaCUDAVizGH * myGH)
{
  int i, num_vars;
  char *fullname, *msg;
  DECLARE_CCTK_PARAMETERS
    /* how often to output */
    i = myGH->out_every_default;
  myGH->out_every_default = out_every >= 0 ? out_every : io_out_every;

  /* report if frequency changed */
  if (myGH->out_every_default != i && !CCTK_Equals (verbose, "none"))
  {
    if (myGH->out_every_default > 0)
    {
      CCTK_VInfo (CCTK_THORNSTRING, "Periodic CaCUDAViz output every %d "
                  "iterations", myGH->out_every_default);
    }
    else
    {
      CCTK_INFO ("Periodic CaCUDAViz output turned off");
    }
  }

  /* re-parse the 'CaCUDAViz::out_vars' parameter if it was changed */
  if (strcmp (out_vars, myGH->out_vars) || myGH->out_every_default != i)
  {
    num_vars = CCTK_NumVars ();

    memset (myGH->out_every, 0, num_vars * sizeof (CCTK_INT));

    if (CCTK_TraverseString (out_vars, SetOutputFlag, myGH,
                             CCTK_GROUP_OR_VAR) < 0)
    {
      CCTK_WARN (myGH->stop_on_parse_errors ? 0 : 1,
                 "error while parsing parameter 'CaCUDAViz::out_vars'");
    }

    if (myGH->out_every_default == i || !CCTK_Equals (verbose, "none"))
    {
      msg = NULL;
      for (i = 0; i < num_vars; i++)
      {
        if (myGH->out_every[i] > 0)
        {
          fullname = CCTK_FullName (i);
          if (!msg)
          {
            Util_asprintf (&msg,
                           "Periodic CaCUDAViz output requested for '%s'",
                           fullname);
          }
          else
          {
            char *tmp = msg;
            Util_asprintf (&msg, "%s, '%s'", msg, fullname);
            free (tmp);
          }
          free (fullname);
        }
      }
      if (msg)
      {
        CCTK_INFO (msg);
        free (msg);
      }
    }

    /* save the last setting of 'CaCUDAViz::out_vars' parameter */
    free (myGH->out_vars);
    myGH->out_vars = strdup (out_vars);
  }
}


/********************************************************************
 ********************    Internal Routines   ************************
 ********************************************************************/
/* check if this variable can be output (static conditions)
 *   - it is a grid function or array
 *   - it is not of any of the CCTK_COMPLEX data types
 *   - it has 3 dimensions
 * */

static int CheckOutputVar (int vindex)
{
  int groupindex;
  cGroup groupinfo;
  char *fullname;
  const char *errormsg;


  /* get the variable group information */
  groupindex = CCTK_GroupIndexFromVarI (vindex);
  CCTK_GroupData (groupindex, &groupinfo);

  errormsg = NULL;
  if (groupinfo.dim != 3)
  {
    errormsg = "grid function/array dimension is other than 3";
  }
  else if (groupinfo.grouptype != CCTK_GF
           && groupinfo.grouptype != CCTK_ARRAY)
  {
    errormsg = "not a grid function or array";
  }
  else
    if (!strncmp (CCTK_VarTypeName (groupinfo.vartype), "CCTK_COMPLEX", 12))
  {
    errormsg = "CCTK_COMPLEX variables cannot be dealt with";
  }

  if (errormsg)
  {
    fullname = CCTK_FullName (vindex);
    CCTK_VWarn (1, __LINE__, __FILE__, CCTK_THORNSTRING,
                "No CaCUDAViz output for '%s': %s", fullname, errormsg);
    free (fullname);
  }

  return (errormsg != NULL);
}


/* callback for CCTK_TraverseString() to set the output flag
   for the given variable */

static void SetOutputFlag (int vindex, const char *optstring, void *arg)
{
  const CaCUDAVizGH *myGH = (const CaCUDAVizGH *) arg;


  if (CheckOutputVar (vindex) == 0)
  {
    myGH->out_every[vindex] = myGH->out_every_default;

    if (optstring)
    {
      IOUtil_ParseOutputFrequency (CCTK_THORNSTRING, "CaCUDAViz::out_vars",
                                   myGH->stop_on_parse_errors,
                                   vindex, optstring,
                                   &myGH->out_every[vindex], NULL);
    }
  }
}
