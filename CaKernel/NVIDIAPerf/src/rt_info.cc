 /*@@
   @file      rt_info.cc
   @date      Sun Jan 15 14:13:23 2012
   @author    David Koppelman
   @desc
   Run-time collection of GPU resource usage and performance data.
   @enddesc
 @@*/

#include <stdio.h>
#include <stdarg.h>
#include <math.h>
#include <string>
#include <map>
#include <list>
#include <vector>
#include <set>
#include <algorithm>
#include <string.h>
#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include "NVIDIAPerf.h"

#ifndef HAVE_CUPTI

//
// Stub routines when RT info cannot be collected.
//
CCTK_INT NVIDIAPerf_atend(){};
CCTK_INT NVIDIAPerf_pre_init()
{ CCTK_WARN(CCTK_WARN_ALERT,
            "Cannot provide run-time GPU data because CUPTI not found.");}
void NVIDIAPerf_init(CCTK_ARGUMENTS) {
  CCTK_WARN(CCTK_WARN_ALERT,
            "Cannot provide run-time GPU data because CUPTI not found."); }

int NPerf_min_launches_get() { return 0; }
CCTK_INT rt_info_event_tracing_get() { return false; }
CCTK_INT rt_info_event_tracing_set(CCTK_INT setting) { return false; }
CCTK_INT rt_info_event_tracing_off() { return false; }

#else

#include <cuda.h>
#include <cupti.h>
#include "drvapi_error_string.h"

#ifdef HAVE_BFD
#include <bfd.h>
static char* demangle(const char *m)
{
  char* const d = bfd_demangle(NULL,m,0);
  return d ? d : strdup(m);
}
#else
static char* demangle(const char *m) { return strdup(m); }
#endif

using namespace std;

#define ASSERTS( expr )                                                       \
{ if ( !(expr) )                                                              \
    CCTK_VWarn                                                                \
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,                  \
       "Assertion failure."); }

#define CHECK_CUPTI_ERROR(err) \
        REPORT_CUPTI_ERROR(err,CCTK_WARN_COMPLAIN)

#define REPORT_CUPTI_ERROR(err,level)                                         \
  if ( err != CUPTI_SUCCESS )                                                 \
    {                                                                         \
      const char *errstr;                                                     \
      cuptiGetResultString(err, &errstr);                                     \
      CCTK_VWarn(level, __LINE__,__FILE__, CCTK_THORNSTRING,                  \
                 "Error %s for CUPTI API call.\n", errstr);                   \
    }

// Wrapper for CUPTI API calls.
//
#define CU( rval )                                                            \
  { const CUptiResult err = rval;                                             \
    REPORT_CUPTI_ERROR(err,CCTK_WARN_ABORT); }

// Error-check Wrapper for CUDA driver API calls.
//
#define CE(rv)                                                                \
 {                                                                            \
   const CUresult result = rv;                                                \
   if ( result != CUDA_SUCCESS )                                              \
     CCTK_VWarn(CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,         \
                "Error %d for CUDA Driver API call: %s\n",                    \
                result, getCudaDrvErrorString(result));                       \
 }

#if CUPTI_API_VERSION  >= 4
typedef CUpti_ActivityKernel2 Version_ActivityKernel;
#define version_cache_executed cacheConfig.config.executed
#else
typedef CUpti_ActivityKernel Version_ActivityKernel;
#define version_cache_executed cacheConfigExecuted
#endif

struct Device_Capabilities {
  int fu_sp;
  int fu_dp;
  int fu_int_al;  // Integer 32-bit add, logical.
  int fu_int_sc;  // Integer 32-bit shift.
  int fu_int_m;   // Integer 32-bit multiply, mad, sum of absolute difference.
  int fu_conv;    // Type conversions.
  int fu_special; // FP 32-bit reciprocal, etc.
  int fu_ls;      // Load Store
};

enum CC_Enum {
  CC_1x, CC_20, CC_21, CC_30, CC_35,
  CC_Unknown,
  CC_ENUM_SIZE
 };


class RT_Info;

static void dump_all(CUcontext context, uint32_t streamId, void *udata);
static void
rt_info_handle_kernel(RT_Info *rt_info, Version_ActivityKernel *record);
static void rt_info_handle(RT_Info *rt_info, CUpti_Activity *record);

class RTI_Kernel_Info;
class Event_Group;

typedef list<RTI_Kernel_Info*> RTI_List;
typedef vector<Event_Group*> EG_List;
const CUpti_EventGroup event_group_null = NULL;
const CUpti_EventID event_id_null = -1;
const CUdevice device_null = -1;

struct EG_Cache_Data
{
  EG_Cache_Data(RT_Info *rti, Event_Group *eg, RTI_Kernel_Info *ki)
    :rti(rti),eg(eg),ki(ki),have_data(false),miss_ratio(0),
     miss_count_per_thread(0){};
  EG_Cache_Data
  (RT_Info *rti, RTI_Kernel_Info *ki, double miss_count_per_thread,
   bool data_okay);
  EG_Cache_Data(RT_Info *rti, RTI_Kernel_Info *ki)
    :rti(rti),eg(NULL),ki(ki),have_data(false){}
  EG_Cache_Data():rti(NULL),eg(NULL),ki(NULL),have_data(false){}
  void summary_line_print(const char *label);
  RT_Info *rti;
  Event_Group *eg;
  RTI_Kernel_Info *ki;
  bool have_data;
  double mem_insn_per_thread;
  uint64_t hit_count_raw, miss_count_raw;
  double miss_bytes_per_load;
  double assumed_load_bw_GBps;
  double miss_ratio;
  double miss_count_per_thread;
};

struct Event_Info {
  Event_Info()
  {
    pseudo = chosen = okay = inited = false;
  }
  string name;
  bool inited;
  bool okay;
  bool pseudo;      // Data from a source other than a CUpti event.
  bool chosen;
  int idx;
  CUpti_EventID id;
  CUpti_EventGroup group;
};

typedef map<string,Event_Info> EI_Hash; // Event info.
typedef map<string,bool> EA_Hash; // Event attribute (static properties).


class NPerf_Metric_Value {
public:
  NPerf_Metric_Value():status(NPerf_Status_Status_Unset),d(0){};
  NPerf_Metric_Value(NPerf_Status s):status(s),d(0){};
  NPerf_Status status;
  CUpti_MetricValueKind kind;
  CUpti_MetricValue metric_value;
  double d;
  int warps_per_block;
  int blocks, block_size;
  double value_per_warp_get()
  { return status == NPerf_Status_OK
      ? d / ( warps_per_block * blocks ) : 0; }
  double value_per_thread_get()
  { return status == NPerf_Status_OK
      ? d / ( block_size * blocks ) : 0; }
};

template<typename T> T
cupti_metric_get_attr(CUpti_MetricID metric_id, CUpti_MetricAttribute attr)
{
  T val;
  size_t val_sz = sizeof(T);
  CU( cuptiMetricGetAttribute( metric_id, attr, &val_sz, &val) );
  assert( val_sz == sizeof(T) ); // Won't catch most mistakes!
  return val;
}

#define DEFINE_METRIC_GET_ATTR(T,abbrev)                                      \
T cupti_metric_get_attr_##abbrev                                              \
(CUpti_MetricID metric_id, CUpti_MetricAttribute attr)                        \
{ return cupti_metric_get_attr<T>(metric_id,attr); }

DEFINE_METRIC_GET_ATTR(int64_t,i);
DEFINE_METRIC_GET_ATTR(double,d);
DEFINE_METRIC_GET_ATTR(CUpti_MetricValueKind,k);

class Met_Event_Info {
public:
  Met_Event_Info():value(0){};
  uint64_t value;
};

class Metric_Info {
public:
  string name;
  CUpti_MetricID met_id;
  CUpti_MetricValueKind kind;
};


class Metrics {
public:
  Metrics():eg_sets(NULL),eg_curr(NULL){};
  ~Metrics();
  map<string,Metric_Info> metric_info;
  CUpti_EventGroupSets *eg_sets;
  CUpti_EventGroupSet *eg_curr;
};

class Metrics_Data {
public:
  Metrics_Data(){ reset(); };
  void reset()
  {
    set_next = 0;
    set_rounds = 0;
    num_launches = 0;
    elapsed_time_ns = 0;
    values_rounds = 0;
    event_info.clear();
    event_info_live.clear();
  };
  int set_next;
  int set_rounds;
  int num_launches;
  int64_t elapsed_time_ns; // Total over all launches.

  // Each event group collected exactly set_rounds times.
  map<CUpti_EventID,Met_Event_Info> event_info;
  // set_next sets collected set_rounds+1 times, the others set_rounds times.
  map<CUpti_EventID,Met_Event_Info> event_info_live;

  // Linearized events and values. Populated on demand.
  int values_rounds; // Value of set_rounds the last time values updated.
  vector<CUpti_EventID> event_ids;
  vector<uint64_t> values;
};

//
// Per-Kernel Information
//
class RTI_Kernel_Info
{
public:
  RTI_Kernel_Info()
  {
    rti = NULL;
    func_name = NULL;
    inited = false;
    have_cupti_kernel_data = false;
    func_name_demangled = NULL;
    call_count = 0;
    elapsed_time_ns = 0;
    call_count_lite = 0;
    elapsed_time_lite_ns = 0;
    event_vals = NULL;
    api_config_inited = false;
    api_config_change_warning_issued = false;
    api_block.x = 0;
    eg_next_idx = 0;
    util_dp_pct = util_sp_pct = 0;
    ipt_other_fp = 0;
    display = false;
    et_print_mult_from_ns = 1e-6;
    et_print_mult_txt = "ms";
    missed_val_cnt = 0;
    notice_missing_missed_val_cnt_start = -1;
  }
  ~RTI_Kernel_Info()
  {
    if ( func_name_demangled ) free( func_name_demangled );
  }

  RT_Info *rti;
  const char *func_name;
  bool inited;
  bool have_cupti_kernel_data;
  char *func_name_demangled;
  Version_ActivityKernel cupti_kernel;

  bool display;  // Print information about this kernel.

  // Data collected using API hooks.
  bool api_config_inited, api_config_change_warning_issued;
  dim3 api_grid, api_block;

  // Data collected using Activity API.
  int call_count; // Number of kernel launches.
  int64_t elapsed_time_ns;
  int call_count_lite;          // Data collected when no events traced.
  int64_t elapsed_time_lite_ns;

  CUfunction cu_function;
  int eg_next_idx;              // Index of next event group to sample.

  // Data collected using event API.
  uint64_t *event_vals;
  
  // Event not collected but value needed by this kernel. There is
  // also a RT_Info version of this variable, common to all kernels.
  map<string,bool> val_missed;
  // Event not collected but value used, resulting in incorrect output.
  map<string,bool> val_missed_noticed;
  int missed_val_cnt;
  
private:
  int notice_missing_missed_val_cnt_start;
public:
  void eg_val_missed(const char *event_name);
  void notice_missing_start()
  { notice_missing_missed_val_cnt_start = missed_val_cnt; }
  bool notice_missing_check_okay()
  { 
    const bool rv = notice_missing_missed_val_cnt_start == missed_val_cnt;
    notice_missing_missed_val_cnt_start = -1;
    return rv;
  }
  bool notice_missing_check_okay_start()
  { 
    const bool rv = notice_missing_check_okay();
    notice_missing_start();
    return rv;
  }

  void data_reset(RT_Info *rti);

  // Members initialized at end of run when preparing report, applies
  // to any launch of the kernel.
  //
  EG_Cache_Data ci_global_load;
  EG_Cache_Data ci_global_ro;   // Cached RO cache accesses.
  EG_Cache_Data ci_global_nc;   // Non-cached accesses to texture cache.
  int api_thread_count;         // Threads per launch.
  int api_block_size;           // Threads per block.
  int api_block_count;
  int api_warps_per_block;      // Rounded up.
  int api_blocks_per_mp;    // Rounded up, to reflect sampled mp.
  int warps_per_sm_per_launch;
  int active_blocks_per_mp;
  int active_warps_per_mp;
  double api_blocks_per_mp_avg;    // Average.
  double insn_per_thread;  // Set based on utilization group data.
  double et_print_mult_from_ns;
  const char *et_print_mult_txt;

  // Members used to accumulate data from multiple groups.
  //
  double tot_miss_per_thread, tot_assumed_load_bw_GBps;
  double tot_mem_insn_per_thread;

  double util_dp_pct, util_sp_pct;
  double ipt_other_fp;

  Metrics_Data md;
};

typedef map<string,RTI_Kernel_Info*> RTI_Hash;
typedef map<CUfunction,RTI_Kernel_Info> CUF_Hash;

//
// Per Process Information
//
class RT_Info {
public:
  RT_Info(){
    cupti_inited = false;
    stop_tracing = false;
    event_tracing_user_on = true;
    context = NULL;
    dev = device_null;
    call_trace_count = 0;
    call_trace_end = 0;
    call_trace_pause_level = 0;
    rti_event_count = 0;
    eg_enabled_count = 0;
    eg_empty = NULL;
    eg_insn_utilization = eg_cache = event_group_active = NULL;
    eg_insn_utilization_2 = eg_insn_issue = eg_insn_types = NULL;
    eg_cache_u = eg_cache_local = eg_cache_shared = NULL;
    eg_cache_l1_gst = eg_cache_l1_lst = NULL;
    eg_cache_l2 = NULL;
    eg_cache_l21 = NULL;
    fake_fh_next = NULL;
    event_callbacks_inited = false;
    event_tracing_active = false;
    assume_dp = sizeof(CCTK_REAL) == 8;
    assumed_load_size = sizeof(CCTK_REAL);
    event_attr_inited = false;
    device_data_collected = false;
    kernel_last = NULL;
    metrics_inited = false;
  };
  bool cupti_inited;      // True if CUPTI successfully initialized.
  bool event_callbacks_inited;
  bool stop_tracing;      // True if tracing was started unnecessarily.
  bool display_early;  // Display some data as collected (before end)
  int call_trace_start, call_trace_count, call_trace_end;
  int call_trace_pause_level;
  CUpti_SubscriberHandle subscriber;
  CUfunction fake_fh_next;
  map<string,CUfunction> fake_fh_map;

  // Assume that FP arithmetic instructions are double precision
  // and that global loads and stores are for DP values.
  bool assume_dp;
  int assumed_load_size;  // Assumed size of load and store.

  // Used to turn tracing on and off during a run.
  bool event_tracing_user_on;
  bool tracing_get() { return event_tracing_user_on; }
  bool tracing_set(bool setting){
    const bool rv = event_tracing_user_on;
    event_tracing_user_on = setting;
    return rv;
  }
  bool tracing_on(){ return tracing_set(true); }
  bool tracing_off(){ return tracing_set(false); }

  // Per-kernel information indexed by function handle, CUfunction.
  CUF_Hash kernel_info_fh;
  RTI_Hash kernel_info_name;
  map<uint32_t,CUfunction> corr_to_h;

  // The most recent kernel for which event has been collected.
  RTI_Kernel_Info *kernel_last;

  CUdevice dev;                 // Device on which info being collected.
  CUcontext context;            // Make sure that only one context used.
  set<uint32_t> active_streams; // Streams with record queues.

  // GPU Characteristics
  bool device_data_collected;
  int cc_major, cc_minor;
  CC_Enum cc_idx;
  CUdevprop devprop;
  int numMultiprocessors;
  int max_threads_per_mp;
  static const int gpu_name_size = 80;
  char gpu_name[gpu_name_size+1];
  int wp_lg;
  int wp_sz;
  int wp_mask;

  void device_data_collect()
  {
    if ( device_data_collected ) return;
    if ( dev == device_null ) return;
    device_data_collected = true;
    CE( cuDeviceGetProperties(&devprop,dev) );
    CE( cuDeviceComputeCapability(&cc_major, &cc_minor, dev) );
    CE( cuDeviceGetName(gpu_name,gpu_name_size,dev) );

    wp_lg = 5; // Checked by the assertion below.
    wp_sz = devprop.SIMDWidth;
    wp_mask = wp_sz - 1;
    ASSERTS( 1 << wp_lg == wp_sz );

    cc_idx =
      cc_major == 1 ? CC_1x :
      cc_major == 2 ?
      ( cc_minor == 0 ? CC_20 : 
        cc_minor == 1 ? CC_21 : CC_Unknown ) :
      cc_major == 3 ?    
      ( cc_minor == 0 ? CC_30 : 
        cc_minor == 5 ? CC_35 : CC_Unknown ) :
      CC_Unknown;

    CE( cuDeviceGetAttribute
        (&numMultiprocessors, CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT,
         dev) );

    CE( cuDeviceGetAttribute
        (&max_threads_per_mp, 
         CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR,
         dev) );
  }

  bool event_attr_unit_thread(const char *event_name);
private:
  bool event_attr_inited;
  // True if event unit is per thread; false or not present, per warp.
  EA_Hash event_attr_attr;
public:

  int rti_event_count;
  int eg_enabled_count;
  EG_List eg_enabled;
  map<string,Event_Group*> event_to_group;
  Event_Group *eg_empty; // Used for collecting timing data.
  Event_Group *event_group_active;
  Event_Group *eg_insn_utilization;
  Event_Group *eg_insn_utilization_2, *eg_insn_issue;
  Event_Group *eg_insn_types;
  Event_Group *eg_cache, *eg_cache_u, *eg_cache_local, *eg_cache_shared;
  Event_Group *eg_cache_l2, *eg_cache_l21;
  Event_Group *eg_cache_l1_gst;  // Level 1 Global Stores
  Event_Group *eg_cache_l1_lst;  // Level 1 Local Stores
  bool event_tracing_active; // If true event monitoring on. Detects nesting.
  uint64_t event_tracing_start_timestamp; // In ns

  // Event not collected but value needed. Common to all kernels.
  map<string,bool> val_missed;

  Event_Group* eg_find(RTI_Kernel_Info *ki, const char *event_name);
  uint64_t eg_find_val(RTI_Kernel_Info *ki, const char *event_name);
  double last_kernel_find_val_per_block(const char *event_name)
    { return eg_find_val_per_block(kernel_last,event_name); }
  uint64_t eg_find_val_per_block(RTI_Kernel_Info *ki, const char *event_name);
  double eg_find_val_per_thread(RTI_Kernel_Info *ki, const char *event_name);

  // Data computed at end of run, declared here for use by other
  // reporting routines.
  //
  double device_memory_bandwidth_GBps;

  void context_set(CUcontext ctx)
  {
    if ( context ) return;
    ASSERTS( !context );
    context = ctx;
  }

  void event_callbacks_init();

  void active_stream_insert(CUcontext ctx, uint32_t sid)
  {
    ASSERTS( ctx );
    if ( context == NULL ) context = ctx;
    // Code not written for multiple contexts.
    ASSERTS( ctx == context );
    active_streams.insert(sid);
  }
  void active_stream_erase(CUcontext ctx, uint32_t sid)
  {
    ASSERTS( ctx );
    ASSERTS( context );
    active_streams.erase(sid);
  }

  void on_api_enter(CUpti_CallbackData *cbdata);
  void on_api_exit(CUpti_CallbackData *cbdata);

  void data_reset()
  {
    CUF_Hash& kafh = kernel_info_fh;
    for ( CUF_Hash::iterator it = kafh.begin(); it != kafh.end(); it++ )
      {
        RTI_Kernel_Info* const rti = &it->second;
        rti->data_reset(this);
      }
  }

  CUfunction fake_fh_get(string name)
  {
    const unsigned int before = fake_fh_map.size();
    CUfunction& fh = fake_fh_map[name];
    if ( before != fake_fh_map.size() )
      {
        fake_fh_next = CUfunction((char*)(fake_fh_next) + 16 );
        fh = fake_fh_next;
      }
    return fh;
  }

  void queues_dump_all()
  {
    dump_all(NULL, 0, this);
    for ( set<uint32_t>::iterator it = active_streams.begin();
          it != active_streams.end(); it++ )
      dump_all(context, *it, this);
  }

  CCTK_INT atend();
  EG_Cache_Data atend_cache_report
  (Event_Group *eg, RTI_Kernel_Info *ki,
   const char *group_name,
   const char *evt_hit, const char *evt_miss,
   double mem_insn_per_thread = 0, bool data_okay = true);
  void print_ki_heading
  (Event_Group *eg, RTI_Kernel_Info *ki, const char *group_name);

private:
  Metrics metrics;
  void metrics_init();
  bool metrics_inited;
public:
  bool metric_add(string metric_name);
  NPerf_Metric_Value metric_value_get(string kernel_name, string metric_name);
  NPerf_Status metric_data_status(string kernel_name);
};

class Event_Group {
public:
  Event_Group(RT_Info *rtip):rti(rtip)
  {
    inited = false; okay = false;
    event_names = "";
    event_group_problem_post_init_count = 0;
  }
  RT_Info* const rti;
  bool inited;
  bool okay;

  // Number of times event group could not be enabled.
  int event_group_problem_post_init_count;
  CUpti_EventGroup cupti_event_group;
  EI_Hash info;
  uint64_t zero;
  string event_names; // For debugging.

  void init()
  {
    CUptiResult err =
      cuptiEventGroupCreate(rti->context, &cupti_event_group, 0);
    inited = true;
    if ( err != CUPTI_SUCCESS )
      {
        REPORT_CUPTI_ERROR(err,CCTK_WARN_ALERT);
        okay = false;
        return;
      }

    okay = true; // Can still be reset.
    add_pseudo("call_count");
    add_pseudo("elapsed_time_ns");
  }
private:
  Event_Info* add_info(const char *event_name)
  {
    Event_Info* const ei = &info[event_name];
    ASSERTS( !ei->inited );  // Check for double-additions.
    const int idx = rti->rti_event_count++;
    ei->idx = idx;
    ei->inited = true;
    ei->name = event_name;
    ei->group = cupti_event_group;
    event_names += event_name;
    event_names += " ";
    rti->event_to_group[event_name] = this;
    return ei;
  }
public:
  bool add_pseudo(const char *event_name)
  {
    Event_Info* const ei = add_info(event_name);
    ei->id = 0;
    ei->pseudo = true;
    ei->chosen = true;
    ei->okay = true;
    return true;
  }
  bool addv()
  {
    init();
    add_finished();
    return true;
  }
  bool addv(const char *first_event, ...)
  {
    va_list ap;
    add(first_event);
    va_start(ap,first_event);
    while ( const char *event = va_arg(ap, const char*) ) add(event);
    va_end(ap);
    add_finished();
    return true;
  }
  bool add(const char *event_name)
  {
    if ( !inited ) init();
    //  if ( !okay ) return false;
    CUpti_EventID event_id;
    CUptiResult err = cuptiEventGetIdFromName(rti->dev, event_name, &event_id);
    if ( err != CUPTI_SUCCESS )
      {
        CCTK_VWarn
        (CCTK_WARN_ALERT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "CUPTI (NVIDIA GPU) event %s unrecognized\n", event_name);
        okay = false;
        return false;
      }

    Event_Info* const ei = add_info(event_name);
    ei->id = event_id;
    ei->chosen = true;

    CUptiResult err_a = cuptiEventGroupAddEvent(cupti_event_group,event_id);

    if ( err_a != CUPTI_SUCCESS )
      {
        const char *errstr;
        cuptiGetResultString(err_a, &errstr);
        CCTK_VWarn
        (CCTK_WARN_ALERT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "CUPTI (NVIDIA GPU) event %s problem: %s\n",
         event_name, errstr);
        okay = false;
        return false;
      }

    ei->okay = true;
    return true;
  }
  void add_finished()
  {
    if ( !okay ) return;
    rti->eg_enabled.push_back(this);
    rti->eg_enabled_count++;
  }

  bool have_data(RTI_Kernel_Info *ki, const char* name = NULL)
  { 
    return 
      okay && val(ki,"call_count") > 0 && ( !name || have_val(ki,name) );
  }

  bool have_val(RTI_Kernel_Info *ki, const char* name)
  {
    EI_Hash::iterator ei = info.find(name);
    return ei != info.end();
  }

  uint64_t& val(RTI_Kernel_Info *ki, ... )
  {
    va_list ap;
    va_start(ap,ki);
    Event_Info *evt_info = NULL;
    while ( const char* const evt_name_i = va_arg(ap, const char*) )
      {
        EI_Hash::iterator ei = info.find(evt_name_i);
        if ( ei == info.end() ) continue;
        evt_info = &(*ei).second;
        break;
      }
    va_end(ap);
    if ( evt_info ) return val(ki,evt_info);
    ASSERTS( false );
    return zero;
  }
  uint64_t& val(RTI_Kernel_Info *ki, const char* name)
  {
    return val(ki,&info[name]);
  }
  uint64_t& val(RTI_Kernel_Info *ki, Event_Info *ei)
  {
    ASSERTS( ei->inited );
    if ( !ei->okay || !ki->event_vals )
      {
        zero = 0;
        return zero;
      }
    return ki->event_vals[ei->idx];
  }

  uint64_t get_warps_launched(RTI_Kernel_Info *ki)
  {
    if ( have_val(ki,"warps_launched") ) return val(ki,"warps_launched");
    const uint64_t call_count = val(ki,"call_count");
    return ki->warps_per_sm_per_launch * call_count;
  }
  uint64_t get_threads_launched(RTI_Kernel_Info *ki)
  {
    if ( have_val(ki,"threads_launched") ) return val(ki,"threads_launched");
    const uint64_t warps_launched = get_warps_launched(ki);
    const int blocks_launched = warps_launched / ki->api_warps_per_block;
    return blocks_launched * ki->api_block_size;
  }
};

Event_Group*
RT_Info::eg_find(RTI_Kernel_Info *ki, const char *event_name)
{
  map<string,Event_Group*>::iterator it = event_to_group.find(event_name);
  if ( it == event_to_group.end() ) return NULL;
  if ( it->second->val(ki,"call_count") == 0 ) return NULL;
  return it->second;
}

uint64_t
RT_Info::eg_find_val(RTI_Kernel_Info *ki, const char *event_name)
{
  Event_Group* const eg = eg_find(ki,event_name);
  ASSERTS( eg && eg->have_val(ki,event_name) );
  if ( !eg || !eg->have_val(ki,event_name) ) return -1;
  const uint64_t call_count = eg->val(ki,"call_count");
  return eg->val(ki,event_name) / call_count;
}

uint64_t
RT_Info::eg_find_val_per_block(RTI_Kernel_Info *ki, const char *event_name)
{
  Event_Group* const eg = eg_find(ki,event_name);
  if ( !eg ) { ki->eg_val_missed(event_name);  return 0; }
  const uint64_t call_count = eg->val(ki,"call_count");
  return eg->val(ki,event_name) / ( call_count * ki->api_blocks_per_mp );
}

double
RT_Info::eg_find_val_per_thread(RTI_Kernel_Info *ki, const char *event_name)
{
  Event_Group* const eg = eg_find(ki,event_name);
  if ( !eg ) { ki->eg_val_missed(event_name);  return 0; }
  const double event_val = eg->val(ki,event_name);
  const bool event_unit_thread = event_attr_unit_thread(event_name);
  const double unit_val =
    event_unit_thread
    ? eg->get_threads_launched(ki)
    : eg->get_warps_launched(ki);
  return event_val/max(1.0,unit_val);
}

void
RTI_Kernel_Info::eg_val_missed(const char *event_name)
{
  if ( notice_missing_missed_val_cnt_start < 0 )
    val_missed_noticed[event_name] = true;
  if ( !val_missed[event_name] )
    {
      val_missed[event_name] = true;
      if ( rti && !rti->val_missed[event_name] )
        {
          printf("****  Missing event: %s\n",event_name);
          rti->val_missed[event_name] = true;
      }
    }
  missed_val_cnt++;
}

void
RTI_Kernel_Info::data_reset(RT_Info *rti)
{
  if ( !api_config_inited ) return;
  api_config_inited = false;
  call_count = 0;
  elapsed_time_ns = 0;
  call_count_lite = 0;
  elapsed_time_lite_ns = 0;
  eg_next_idx = 0;
  for ( int i=0; i<rti->rti_event_count; i++ ) event_vals[i] = 0;
  md.reset();
}


// Kludge, until I figure out persistent storage.
RT_Info *rt_info = NULL;


//
// Code below based on NVIDIA CUPTI sample prog  activity.cpp
// The code is not based on any understanding of the relationship
// between buffer size and number of buffers on tracing overhead.
//
#define BUF_SIZE (32 * 1024)
#define ALIGN_SIZE (8)
#define ALIGN_BUFFER(buffer, align)                                            \
  (((uintptr_t) (buffer) & ((align)-1)) ? ((buffer) + (align) - ((uintptr_t) (buffer) & ((align)-1))) : (buffer))

#if CUPTI_API_VERSION  < 4

/**
 * Allocate a new BUF_SIZE buffer and add it to the queue specified by
 * 'context' and 'streamId'.
 */
static void
queueNewBuffers(CUcontext context, uint32_t streamId)
{
  const size_t size = BUF_SIZE;
  for ( int i=0; i<20; i++ )
    {
      uint8_t *buffer = (uint8_t *) malloc(size+ALIGN_SIZE);
      CU(cuptiActivityEnqueueBuffer
         (context, streamId, ALIGN_BUFFER(buffer, ALIGN_SIZE), size));
    }
}

/**
 * Dump the contents of the top buffer in the queue specified by
 * 'context' and 'streamId', and return the top buffer. If the queue
 * is empty return NULL.
 */
static uint8_t *
dump(CUcontext context, uint32_t streamId, void *udata)
{
  RT_Info* const rti = (RT_Info*) udata;
  uint8_t *buffer = NULL;
  size_t validBufferSizeBytes;
  CUptiResult status = cuptiActivityDequeueBuffer
    (context, streamId, &buffer, &validBufferSizeBytes);
  if ( status == CUPTI_ERROR_QUEUE_EMPTY ) return NULL;
  CU(status);

  CUpti_Activity *record = NULL;
  do {
    const CUptiResult status =
      cuptiActivityGetNextRecord(buffer, validBufferSizeBytes, &record);
    if ( status == CUPTI_ERROR_MAX_LIMIT_REACHED ) break;
    CU(status);
    if ( context == rti->context )
      rt_info_handle(rti, record);
  } while (1);

  // report any records dropped from the queue
  size_t dropped;
  CU(cuptiActivityGetNumDroppedRecords(context, streamId, &dropped));
  if ( dropped )
    CCTK_VWarn
      (CCTK_WARN_COMPLAIN, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Dropped %u activity records\n", (unsigned int)dropped);

  return buffer;
}

static void
dump_all(CUcontext context, uint32_t streamId, void *udata)
{
  while ( uint8_t* const buffer = dump(context, streamId, udata) ) free(buffer);
}

/**
 * If the top buffer in the queue specified by 'context' and
 * 'streamId' is full, then dump its contents and return the
 * buffer. If the top buffer is not full, return NULL.
 */
static uint8_t *
dumpIfFull(CUcontext context, uint32_t streamId, void *udata)
{
  size_t validBufferSizeBytes;
  const CUptiResult status =
    cuptiActivityQueryBuffer(context, streamId, &validBufferSizeBytes);
  if ( status == CUPTI_ERROR_MAX_LIMIT_REACHED ) {
    return dump(context, streamId, udata);
  } else if ((status != CUPTI_SUCCESS) && (status != CUPTI_ERROR_QUEUE_EMPTY)) {
    CU(status);
  }
  return NULL;
}

static void
handleSync
(CUpti_CallbackId cbid, const CUpti_SynchronizeData *syncData, void *udata)
{
  // check the top buffer of the global queue and dequeue if full. If
  // we dump a buffer add it back to the queue
  if ( uint8_t* const buffer = dumpIfFull(NULL, 0, udata) )
    CU(cuptiActivityEnqueueBuffer(NULL, 0, buffer, BUF_SIZE));

  // dump context buffer on context sync
  if (cbid == CUPTI_CBID_SYNCHRONIZE_CONTEXT_SYNCHRONIZED) {
    if ( uint8_t* const buffer = dumpIfFull(syncData->context, 0, udata) )
      CU(cuptiActivityEnqueueBuffer(syncData->context, 0, buffer, BUF_SIZE));
  }
  // dump stream buffer on stream sync
  else if (cbid == CUPTI_CBID_SYNCHRONIZE_STREAM_SYNCHRONIZED) {
    uint32_t streamId;
    CU(cuptiGetStreamId(syncData->context, syncData->stream, &streamId));
    if ( uint8_t* const buffer = dumpIfFull(syncData->context,streamId,udata) )
      CU(cuptiActivityEnqueueBuffer
         (syncData->context, streamId, buffer, BUF_SIZE));
  }
}

static void
handleResource
(CUpti_CallbackId cbid, const CUpti_ResourceData *resourceData, void *udata)
{
  uint32_t streamId;
  RT_Info* const rt_info = (RT_Info*) udata;

  switch ( cbid ) {
  case CUPTI_CBID_RESOURCE_CONTEXT_CREATED:
    // enqueue buffers on a context's queue when the context is created
    rt_info->context_set(resourceData->context);
    queueNewBuffers(resourceData->context, 0);
    break;

  case CUPTI_CBID_RESOURCE_CONTEXT_DESTROY_STARTING:
    // dump all buffers on a context destroy
    dump_all(resourceData->context,0,udata);
    break;

  case CUPTI_CBID_RESOURCE_STREAM_CREATED:
    // enqueue buffers on a stream's queue when a non-default stream is created
    CU(cuptiGetStreamId
       (resourceData->context, resourceData->resourceHandle.stream, &streamId));
    rt_info->active_stream_insert(resourceData->context,streamId);
    queueNewBuffers(resourceData->context, streamId);
    break;

  case CUPTI_CBID_RESOURCE_STREAM_DESTROY_STARTING:
    // dump all buffers on a stream destroy
    CU(cuptiGetStreamId
       (resourceData->context, resourceData->resourceHandle.stream, &streamId));
    dump_all(resourceData->context, streamId, udata);
    rt_info->active_stream_erase(resourceData->context,streamId);
    break;

  default:
    break;
  }
}

#else

void
cb_buffer_requested(uint8_t **buffer, size_t *size, size_t *maxNumRecords)
{
  uint8_t *bfr = (uint8_t *) malloc(BUF_SIZE + ALIGN_SIZE);
  if ( !bfr )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "NVIDIAPerf: Out of memory.\n");

  *size = BUF_SIZE;
  *buffer = ALIGN_BUFFER(bfr, ALIGN_SIZE);
  *maxNumRecords = 0;
}

void
cb_buffer_completed
(CUcontext ctx, uint32_t streamId, 
 uint8_t *buffer, size_t size, size_t validSize)
{
  if (validSize > 0) 
    {
      do {
        CUpti_Activity *record = NULL;
        CUptiResult status = 
          cuptiActivityGetNextRecord(buffer, validSize, &record);
        if ( status == CUPTI_SUCCESS ) { rt_info_handle(rt_info, record); }
        else if (status == CUPTI_ERROR_MAX_LIMIT_REACHED) { break; }
        else { CU(status); }
      } while (1);

      // report any records dropped from the queue
      size_t dropped;
      CU(cuptiActivityGetNumDroppedRecords(ctx, streamId, &dropped));
      if ( dropped )
        CCTK_VWarn
          (CCTK_WARN_COMPLAIN, __LINE__,__FILE__, CCTK_THORNSTRING,
           "Dropped %u activity records\n", (unsigned int)dropped);
    }

  free(buffer);
}

// Unused for CUPT API >= 4.
static void
dump_all(CUcontext context, uint32_t streamId, void *udata){}

#endif


#define DECL_PARAMS(t,v) t *v = (t*) cbd->functionParams;

static void CUPTIAPI
traceCallback(void *userdata, CUpti_CallbackDomain domain,
              CUpti_CallbackId cbid, const void *cbdata)
{
  RT_Info* const rt_info = (RT_Info*) userdata;

#if CUPTI_API_VERSION >= 4
  if ( domain == CUPTI_CB_DOMAIN_RESOURCE )
    {
      CUpti_ResourceData* const resourceData = (CUpti_ResourceData*) cbdata;
      if ( cbid == CUPTI_CBID_RESOURCE_CONTEXT_CREATED )
        rt_info->context_set(resourceData->context);
    }
#else
  if (domain == CUPTI_CB_DOMAIN_RESOURCE) {
    handleResource(cbid, (CUpti_ResourceData *)cbdata, userdata);
  } else if (domain == CUPTI_CB_DOMAIN_SYNCHRONIZE) {
    handleSync(cbid, (CUpti_SynchronizeData *)cbdata, userdata);
  } 
#endif
  else if ( domain == CUPTI_CB_DOMAIN_DRIVER_API
              || domain == CUPTI_CB_DOMAIN_RUNTIME_API )
    {
      CUpti_CallbackData* const cbd = (CUpti_CallbackData*) cbdata;

      if ( cbd->callbackSite == CUPTI_API_EXIT )
        {
          if ( cbid == CUPTI_DRIVER_TRACE_CBID_cuModuleGetFunction )
            {
              DECL_PARAMS(cuModuleGetFunction_params,params);
              RTI_Kernel_Info& ki = rt_info->kernel_info_fh[*params->hfunc];
              ki.rti = rt_info;
              ki.func_name = strdup(params->name);
              ki.func_name_demangled = demangle(ki.func_name);
              ki.cu_function = *params->hfunc;
              rt_info->kernel_info_name[string(ki.func_name_demangled)] = &ki;
            }
        }

      const bool __attribute__ ((unused)) launch_runtime =
        ( domain == CUPTI_CB_DOMAIN_RUNTIME_API &&
          cbid == CUPTI_RUNTIME_TRACE_CBID_cudaLaunch_v3020 );

      const bool launch_driver =
        ( domain == CUPTI_CB_DOMAIN_DRIVER_API &&
          cbid == CUPTI_DRIVER_TRACE_CBID_cuLaunchKernel );

      if ( launch_driver )
        {
          if ( cbd->callbackSite == CUPTI_API_ENTER )
            rt_info->on_api_enter(cbd);
          else if ( cbd->callbackSite == CUPTI_API_EXIT )
            rt_info->on_api_exit(cbd);
        }

      if ( rt_info->call_trace_count >= rt_info->call_trace_end ) return;

      // Maybe one day provide configuration option to activate this.
      if ( false && cbd->callbackSite == CUPTI_API_EXIT )
        {
          printf("Exit from %s, cbid %d, serial %d, rv %d\n",
                 cbd->functionName, cbid, rt_info->call_trace_count,
                 *(CUresult*)cbd->functionReturnValue);
        }

      if ( rt_info->call_trace_pause_level ) return;
      if ( rt_info->call_trace_count++ < rt_info->call_trace_start )
        return;
#define DP(tp) tp* const p = (tp*) cbd->functionParams
      switch ( cbid ) {
      case CUPTI_DRIVER_TRACE_CBID_cuMemcpyHtoD_v2:
        {
          DP(cuMemcpyHtoD_v2_params);
          printf("Memcopy %#llx[%lu]\n",p->dstDevice,p->ByteCount);
        }
        break;
      case CUPTI_DRIVER_TRACE_CBID_cuMemcpy3D_v2:
        {
          CUDA_MEMCPY3D* const pcopy = *(CUDA_MEMCPY3D**)cbd->functionParams;
          const char* const mt[] = {"???","Hst","Dev","Ary","Uni"};
          printf("Memcpy 3D %s -> %s %p "
                 "(%zd,%zd,%zd)->(%zd,%zd,%zd) %zdx%zdx%zd\n",
                 mt[pcopy->srcMemoryType],
                 mt[pcopy->dstMemoryType],
                 pcopy->dstMemoryType == 1 ? (void*)pcopy->dstHost :
                 pcopy->dstMemoryType == 2 ? (void*)pcopy->dstDevice : NULL,
                 pcopy->srcXInBytes, pcopy->srcY, pcopy->srcZ,
                 pcopy->dstXInBytes, pcopy->dstY, pcopy->dstZ,
                 pcopy->WidthInBytes, pcopy->Height, pcopy->Depth
                 );
          break;
        }

      }
#undef DP
      printf("Call of %s, cbid %d, serial %d\n",
             cbd->functionName, cbid, rt_info->call_trace_count);
    }
}

extern "C" CCTK_INT
rt_info_trace_for(int num)
{
  if ( !rt_info ) return 0;
  rt_info->call_trace_start = rt_info->call_trace_count;
  rt_info->call_trace_end = rt_info->call_trace_start + num;
  return 1;
}

extern "C" CCTK_INT
rt_info_trace_pause()
{
  if ( !rt_info ) return 0;
  return rt_info->call_trace_pause_level++;
}
extern "C" CCTK_INT
rt_info_trace_resume()
{
  if ( !rt_info ) return 0;
  return --rt_info->call_trace_pause_level == 0;
}

static void
rt_info_handle(RT_Info *rt_info, CUpti_Activity *record)
{
  if ( rt_info->stop_tracing ) return;

  switch ( record->kind ) {
  case CUPTI_ACTIVITY_KIND_KERNEL:
    rt_info_handle_kernel(rt_info,(Version_ActivityKernel*)record);
    break;
  default:
    break;
  }
}

 /*@@
   @routine    rt_info_handle_kernel
   @date       Tue Jan 17 15:05:54 2012
   @author     David Koppelman
   @desc
   Record information about a kernel launch, and possibly print info.
   @enddesc
   @calls
   @calledby
   @history

   @endhistory

@@*/
static void
rt_info_handle_kernel(RT_Info *rt_info, Version_ActivityKernel *kernel)
{
  string func_name(kernel->name);
  const uint32_t corr_id = kernel->correlationId;
  map<uint32_t,CUfunction>::iterator hi = rt_info->corr_to_h.find(corr_id);
  CUfunction fh = hi != rt_info->corr_to_h.end() ? hi->second :
    rt_info->fake_fh_get(kernel->name);

  RTI_Kernel_Info& ki = rt_info->kernel_info_fh[fh];
  const bool first_time = !ki.have_cupti_kernel_data;

  if ( !rt_info->eg_enabled_count )
    {
      ki.elapsed_time_ns += kernel->end - kernel->start;
      ki.call_count++;
    }

  if ( !first_time ) return;

  // Initialize kernel information.
  //
  ki.inited = true;
  if ( !ki.func_name )
    {
      ki.rti = rt_info;
      ki.func_name = func_name.c_str();
      ki.func_name_demangled = demangle(ki.func_name);
    }

  // To Do: See if launch configuration varies for a kernel.
  ki.cupti_kernel = *kernel;
  ki.have_cupti_kernel_data = true;

  // Note: Execution seemed to deadlock calling cudaFuncGetAttributes here.

  // Return if user can wait until end of run.
  if ( !rt_info->display_early ) return;

  CCTK_VInfo
    (CCTK_THORNSTRING,
     " %s\n   %6u/%6u sta/dyn shared, %6u loc, %d regs\n",
     ki.func_name_demangled,
     kernel->staticSharedMemory,
     kernel->dynamicSharedMemory,
     kernel->localMemoryPerThread,
     kernel->registersPerThread);
}

void
RT_Info::on_api_enter(CUpti_CallbackData *cbdata)
{
  context = cbdata->context;

  if ( dev == device_null ) CE( cuCtxGetDevice(&dev) );
  event_callbacks_init();
  metrics_init();
  device_data_collect();
  if ( !event_tracing_user_on ) return;
  if ( !eg_enabled_count ) return;

  cuLaunchKernel_params* const params =
    (cuLaunchKernel_params*)cbdata->functionParams;
  CUfunction hfunc = params->f;
  RTI_Kernel_Info& ki = kernel_info_fh[hfunc];
 
  //  This is why kernel event tracing should not be used on production runs.
  CE( cuCtxSynchronize() );
  ASSERTS( !event_tracing_active );

  event_tracing_active = true;

  CU( cuptiSetEventCollectionMode
      (context, CUPTI_EVENT_COLLECTION_MODE_KERNEL) );

  const bool rt_info_metrics =
    metrics.eg_sets && ki.eg_next_idx == eg_enabled_count;

  if ( !metrics.eg_sets && ki.eg_next_idx == eg_enabled_count )
    ki.eg_next_idx = 0;

  if ( rt_info_metrics )
    {
      event_group_active = NULL;
      Metrics_Data &md = ki.md;
      assert( !metrics.eg_curr );
      CUpti_EventGroupSet* const egs =
        metrics.eg_curr = &metrics.eg_sets->sets[md.set_next];

      for ( int i=0; i<egs->numEventGroups; i++ )
        {
          int all = 1;
          CU( cuptiEventGroupSetAttribute
              ( egs->eventGroups[i],
                CUPTI_EVENT_GROUP_ATTR_PROFILE_ALL_DOMAIN_INSTANCES,
                sizeof(all), &all));
          CU( cuptiEventGroupEnable(egs->eventGroups[i]) );
        }
    }
  else
    {
      const int this_eg_idx = ki.eg_next_idx++;
      event_group_active = eg_enabled[this_eg_idx];

      ASSERTS( event_group_active->okay );
      const CUptiResult res_group_enable =
        event_group_active == eg_empty ? CUPTI_SUCCESS :
        cuptiEventGroupEnable( event_group_active->cupti_event_group );
#if CUDA_VERSION >= 5050
      if ( res_group_enable == CUPTI_ERROR_HARDWARE_BUSY )
        {
          if ( !event_group_active->event_group_problem_post_init_count )
            printf("Hardware busy, eg idx %d: %s.\n",
                   this_eg_idx, event_group_active->event_names.c_str());
          event_group_active->event_group_problem_post_init_count++;
          event_group_active = NULL;
        }
      else
#endif
        CU( res_group_enable );
    }

  CU( cuptiDeviceGetTimestamp(context, &event_tracing_start_timestamp) );
}

void
RT_Info::on_api_exit(CUpti_CallbackData *cbdata)
{
  cuLaunchKernel_params* const params =
    (cuLaunchKernel_params*)cbdata->functionParams;
  CUfunction hfunc = params->f;
  const uint32_t correlation_id = cbdata->correlationId;
  const CUcontext context = cbdata->context;
  corr_to_h[correlation_id] = hfunc;

  if ( !event_tracing_user_on ) return;
  if ( !eg_enabled_count ) return;

  CE( cuCtxSynchronize() ); ///  SHOULD THIS BE AVOIDED??
  ASSERTS( event_tracing_active );
  event_tracing_active = false;
  uint64_t event_tracing_stop_timestamp;
  CU( cuptiDeviceGetTimestamp(context, &event_tracing_stop_timestamp) );
  const int64_t elapsed =
    event_tracing_stop_timestamp - event_tracing_start_timestamp;

  RTI_Kernel_Info& ki = rt_info->kernel_info_fh[hfunc];

  if ( !ki.event_vals )
    ki.event_vals =
        (uint64_t*) calloc(rti_event_count,sizeof(ki.event_vals[0]));

  ki.elapsed_time_ns += elapsed;
  ki.call_count++;

  if ( !metrics.eg_curr && !event_group_active ) return;

  if ( event_group_active == eg_empty )
    {
      ki.elapsed_time_lite_ns += elapsed;
      ki.call_count_lite++;
    }

  kernel_last = &ki;

  dim3 grid = dim3(params->gridDimX, params->gridDimY, params->gridDimZ);
  dim3 block = dim3(params->blockDimX, params->blockDimY, params->blockDimZ);
  if ( ki.api_config_inited )
    {
      if ( !ki.api_config_change_warning_issued &&
           ( ki.api_grid.x != grid.x || ki.api_grid.y != grid.y
             || ki.api_grid.z != grid.z
             || ki.api_block.x != block.x || ki.api_block.y != block.y
             || ki.api_block.z != block.z ) )
        {
          ki.api_config_change_warning_issued = true;
          CCTK_VWarn
            (CCTK_WARN_COMPLAIN, __LINE__,__FILE__, CCTK_THORNSTRING,
             "Kernel at %p launched with multiple configurations\n",
             hfunc);
        }
    }
  else
    {
      ki.api_config_inited = true;
      ki.api_grid = grid;  ki.api_block = block;
      ki.api_block_count = ki.api_grid.x * ki.api_grid.y * ki.api_grid.z;
      ki.api_block_size = ki.api_block.x * ki.api_block.y * ki.api_block.z;
      ki.api_thread_count = ki.api_block_count * ki.api_block_size;
      ki.api_warps_per_block = ( ( ki.api_block_size + wp_mask ) >> wp_lg );
      ki.api_blocks_per_mp_avg = 
        double(ki.api_block_count) / numMultiprocessors;
      ki.api_blocks_per_mp = 0.999 + ki.api_blocks_per_mp_avg;
      // This value may be overwritten by event data showing a
      // measured number of warps per sm.
      ki.warps_per_sm_per_launch =
        ki.api_warps_per_block * ki.api_blocks_per_mp;
    }

  if ( metrics.eg_curr )
    {
      Metrics_Data &md = ki.md;
      CUpti_EventGroupSet* const egs = metrics.eg_curr;
      metrics.eg_curr = NULL;

      for ( int i=0; i<egs->numEventGroups; i++ )
        {
          CUpti_EventGroup eg = egs->eventGroups[i];
#define SZ(v) size_t v##_sz=sizeof(v);
          auto ga = [&](CUpti_EventGroupAttribute attr)
          {
            int val;
            size_t val_sz = sizeof(val);
            CU( cuptiEventGroupGetAttribute ( eg, attr, &val_sz, &val ) );
            assert( val_sz == sizeof(val) );
            return val;
          };

          const int group_domain = ga(CUPTI_EVENT_GROUP_ATTR_EVENT_DOMAIN_ID);
          int num_total_instances; SZ(num_total_instances);
          CU( cuptiDeviceGetEventDomainAttribute
              ( dev, group_domain,
                CUPTI_EVENT_DOMAIN_ATTR_TOTAL_INSTANCE_COUNT,
                &num_total_instances_sz, &num_total_instances ) );
          const int num_instances = ga(CUPTI_EVENT_GROUP_ATTR_INSTANCE_COUNT);
          const int num_events = ga(CUPTI_EVENT_GROUP_ATTR_NUM_EVENTS);
          CUpti_EventID event_ids[num_events];
          SZ(event_ids);
          CU( cuptiEventGroupGetAttribute
              ( eg,
                CUPTI_EVENT_GROUP_ATTR_EVENTS,
                &event_ids_sz, &event_ids[0] ) );
          for ( int j=0; j<num_events; j++ )
            {
              uint64_t vals[num_instances];
              SZ(vals);
              CU( cuptiEventGroupReadEvent
                  ( eg, CUPTI_EVENT_READ_FLAG_NONE,
                    event_ids[j], &vals_sz, &vals[0] ) );
              uint64_t val_sum = 0;
              for ( auto v : vals ) val_sum += v;
              const uint64_t val_norm =
                val_sum * num_total_instances / num_instances;
              md.event_info_live[event_ids[j]].value = val_norm;
            }
#undef SZ
          CU( cuptiEventGroupDisable( eg ) );
        }

      if ( ++md.set_next == metrics.eg_sets->numSets )
        {
          for ( auto elt : md.event_info_live )
            md.event_info[elt.first].value += elt.second.value;
          md.set_next = 0;
          md.set_rounds++;
          ki.eg_next_idx = 0;
        }

      md.num_launches++;
      md.elapsed_time_ns += elapsed;

      return;
    }

  event_group_active->val(&ki,"elapsed_time_ns") += elapsed;
  event_group_active->val(&ki,"call_count")++;

  for ( EI_Hash::iterator ie = event_group_active->info.begin();
        ie != event_group_active->info.end(); ie++ )
    {
      Event_Info* const ei = &ie->second;
      if ( !ei->okay || ei->pseudo ) continue;

      uint64_t e_data;
      size_t e_size = sizeof(e_data);

      CU( cuptiEventGroupReadEvent
          ( ei->group, CUPTI_EVENT_READ_FLAG_NONE, ei->id, &e_size, &e_data ) );
      ki.event_vals[ei->idx] += e_data;
    }

  CU( cuptiEventGroupDisable( event_group_active->cupti_event_group ) );
  event_group_active = NULL;
}


 /*@@
   @routine    NVIDIAPerf_pre_init
   @date       Tue Jan 17 15:23:50 2012
   @author     David Koppelman
   @desc
   Initialize CUPTI API, used by GPU run-time info collection.
   @enddesc
   @calls
   @calledby
   @history

   @endhistory

@@*/
CCTK_INT
NVIDIAPerf_pre_init()
{
  DECLARE_CCTK_PARAMETERS;

  if ( !rt_info_on ) return 0;  // Cactus parameter.

  // Ideally this routine would be called before any other NVIDIAPerf
  // API call, but there's no way to assure that in Cactus yet.
  // So, return if this routine already called.
  if ( rt_info ) return 0;

  CCTK_VInfo
    (CCTK_THORNSTRING,
     "Initializing. This needs to run before any other GPU code.\n");

  // The code below must execute as early as possible, including
  // before any--ANY--CUDA API routines are called.
  //
  // Note: Installing the callback later works for CUDA but not for
  // OpenCL.

  CU(cuptiActivityEnable(CUPTI_ACTIVITY_KIND_DEVICE));
  CU(cuptiActivityEnable(CUPTI_ACTIVITY_KIND_KERNEL));

#if CUPTI_API_VERSION >= 4
  CU(cuptiActivityRegisterCallbacks(cb_buffer_requested,cb_buffer_completed));
#else
  queueNewBuffers(NULL,0);
#endif

  // Note: It is possible that tracing is not wanted for this
  // processor, however since we don't know the processor number yet
  // we start tracing just in case we do need it.
  //
  rt_info = new RT_Info;
  rt_info->call_trace_start = call_trace_start;
  rt_info->call_trace_end = call_trace_start + call_trace_amount;

  CUptiResult cuptierr = cuptiSubscribe
    (&rt_info->subscriber,(CUpti_CallbackFunc)traceCallback, (void*)rt_info);

  if ( cuptierr != CUPTI_SUCCESS )
    {
      CHECK_CUPTI_ERROR(cuptierr);
      return 0;
    }

  CU(cuptiEnableDomain(1, rt_info->subscriber, CUPTI_CB_DOMAIN_DRIVER_API));
  CU(cuptiEnableDomain(1, rt_info->subscriber, CUPTI_CB_DOMAIN_RUNTIME_API));
  CU(cuptiEnableDomain(1, rt_info->subscriber, CUPTI_CB_DOMAIN_RESOURCE));
  CU(cuptiEnableDomain(1, rt_info->subscriber, CUPTI_CB_DOMAIN_SYNCHRONIZE));

  rt_info->cupti_inited = true;
  rt_info->display_early = rt_info_early;

  return 0;
}

void
RT_Info::metrics_init()
{
  if ( metrics_inited ) return;
  metric_add("eligible_warps_per_cycle");
  metric_add("gld_efficiency");
  metric_add("gld_throughput");
#if 0
  metric_add("shared_efficiency");
  metric_add("shared_replay_overhead");
  metric_add("sm_efficiency");
  metric_add("inst_executed");
  metric_add("inst_issued");
  metric_add("flop_count_dp");
  metric_add("ipc");
  metric_add("issued_ipc");
  metric_add("issue_slots");
  metric_add("issue_slot_utilization");
  metric_add("l2_l1_read_hit_rate");
  metric_add("l2_l1_read_throughput");
  metric_add("l2_read_throughput");
  metric_add("l2_utilization");
  metric_add("nc_cache_global_hit_rate");
  metric_add("sysmem_utilization");
  metric_add("dram_read_throughput");
  metric_add("dram_write_throughput");
  metric_add("dram_utilization");
#endif
  metrics_inited = true;

  vector<CUpti_MetricID> metric_ids;
  for ( auto &mi: metrics.metric_info ) metric_ids.push_back(mi.second.met_id);

  if ( metrics.eg_sets )
    CU( cuptiEventGroupSetsDestroy( metrics.eg_sets ) );

  CU( cuptiMetricCreateEventGroupSets
      ( context, sizeof(metric_ids[0])*metric_ids.size(), metric_ids.data(),
        &metrics.eg_sets) );
}

bool
RT_Info::metric_add(string name)
{
  if ( !rt_info ) return false;
  CUpti_MetricID met_id;
  CUptiResult rv = cuptiMetricGetIdFromName(dev,name.c_str(),&met_id);
  if ( rv == CUPTI_ERROR_INVALID_METRIC_NAME ) return false;
  CU( rv );
  Metric_Info &mi = metrics.metric_info[name];
  mi.name = name;
  mi.met_id = met_id;
  mi.kind = cupti_metric_get_attr_k(mi.met_id,CUPTI_METRIC_ATTR_VALUE_KIND);

  metrics_inited = false;
  return true;
}

bool
NPerf_metric_collect(const char *name)
{ return rt_info->metric_add(name); }

NPerf_Status
RT_Info::metric_data_status(string kernel_name)
{
  if ( !rt_info ) return NPerf_Status_Off;
  RTI_Kernel_Info* const ki = kernel_info_name[kernel_name];
  if ( !ki ) return NPerf_Status_Kernel_Not_Found;
  if ( ki->md.set_rounds ) return NPerf_Status_OK;
  return NPerf_Status_Insufficient_Launches;
}

NPerf_Status
NPerf_metric_data_status(const char* kernel_name)
{ return rt_info->metric_data_status(kernel_name); }


NPerf_Metric_Value
RT_Info::metric_value_get(string kernel_name, string metric_name)
{
  if ( !rt_info ) return NPerf_Metric_Value(NPerf_Status_Off);

  RTI_Kernel_Info* const ki = kernel_info_name[kernel_name];
  if ( !ki ) return NPerf_Metric_Value(NPerf_Status_Kernel_Not_Found);
  auto mii = metrics.metric_info.find(metric_name);
  if ( mii == metrics.metric_info.end() )
    return NPerf_Metric_Value(NPerf_Status_Metric_Not_Found);
  Metric_Info& mi = metrics.metric_info[metric_name];
  Metrics_Data& md = ki->md;

  if ( md.set_rounds == 0 )
    return NPerf_Metric_Value(NPerf_Status_Insufficient_Launches);
  if ( md.values_rounds != md.set_rounds )
    {
      md.event_ids.clear();
      md.values.clear();
      for ( auto elt : md.event_info )
        {
          md.event_ids.push_back(elt.first);
          md.values.push_back(elt.second.value/md.set_rounds);
        }
      md.values_rounds = md.set_rounds;
    }
  const int num_events = md.event_ids.size();

  NPerf_Metric_Value npv(NPerf_Status_OK);
  npv.kind = mi.kind;
  npv.blocks = ki->api_block_count;
  npv.block_size = ki->api_block_size;
  npv.warps_per_block = ki->api_warps_per_block;
  assert( npv.blocks );
  assert( npv.warps_per_block );

  const int64_t et_ns =
    ki->elapsed_time_lite_ns / max(1,ki->call_count_lite);

  CU( cuptiMetricGetValue
      (dev, mi.met_id,
       sizeof(md.event_ids[0])*num_events, md.event_ids.data(),
       sizeof(md.values[0])*num_events, md.values.data(),
       et_ns, &npv.metric_value) );

  switch ( mi.kind ) {
  case CUPTI_METRIC_VALUE_KIND_DOUBLE:
  case CUPTI_METRIC_VALUE_KIND_PERCENT:            // 0.0 - 100.0
    npv.d = npv.metric_value.metricValueDouble;            break;
  case CUPTI_METRIC_VALUE_KIND_UINT64:
    npv.d = npv.metric_value.metricValueUint64;            break;
  case CUPTI_METRIC_VALUE_KIND_INT64:
  case CUPTI_METRIC_VALUE_KIND_THROUGHPUT:        // Bytes per second
    npv.d = npv.metric_value.metricValueInt64;             break;
  case CUPTI_METRIC_VALUE_KIND_UTILIZATION_LEVEL: // 0-10
    npv.d = npv.metric_value.metricValueUtilizationLevel;  break;
  default: assert(false);
  };

  return npv;
}

double
NPerf_metric_value_get
(const char* kernel_name, const char *metric_name)
{
  return rt_info->metric_value_get(kernel_name,metric_name).d;
}

double
NPerf_metric_value_per_warp_get
(const char* kernel_name, const char *metric_name)
{
  return
    rt_info->metric_value_get(kernel_name,metric_name).value_per_warp_get();
}

double
NPerf_metric_value_per_thread_get
(const char* kernel_name, const char *metric_name)
{
  return
    rt_info->metric_value_get(kernel_name,metric_name).value_per_thread_get();
}

Metrics::~Metrics()
{
  if ( !eg_sets ) return;
  CU( cuptiEventGroupSetsDestroy( eg_sets ) );
  eg_sets = NULL;
}


void
RT_Info::event_callbacks_init()
{
  if ( event_callbacks_inited ) return;
  event_callbacks_inited = true;

  DECLARE_CCTK_PARAMETERS;

  int cc_major, cc_minor;
  CE( cuDeviceComputeCapability(&cc_major, &cc_minor, dev) );

  eg_empty = new Event_Group(this);
  eg_empty->addv();

  eg_insn_utilization = new Event_Group(this);
  eg_insn_types = new Event_Group(this);
  eg_insn_utilization_2 = new Event_Group(this);
  eg_insn_issue = new Event_Group(this);
  eg_cache_u = new Event_Group(this);
  eg_cache = new Event_Group(this);
  eg_cache_l1_gst = new Event_Group(this);
  eg_cache_l1_lst = new Event_Group(this);
  eg_cache_local = new Event_Group(this);
  eg_cache_shared = new Event_Group(this);
  eg_cache_l2 = new Event_Group(this);
  eg_cache_l21 = new Event_Group(this);

  // Common

  eg_insn_utilization_2->addv
    ("active_warps",
     "warps_launched",
     "active_cycles",
     NULL);

  // Note: CC 2.0 Tesla 2050 13 June 2012, 17:11:45 CDT
  //   sm_cta_launched seems to return a number 3x too high.
  eg_cache->addv
    ("sm_cta_launched", "l1_global_load_hit", "l1_global_load_miss", NULL);

  if ( show_cache_data )
    {
      eg_cache_l1_lst->addv
        ("sm_cta_launched",
         "l1_local_store_hit", "l1_local_store_miss", NULL);

      eg_cache_local->addv
        ("sm_cta_launched", "l1_local_load_hit", "l1_local_load_miss", NULL);
    }

  switch ( cc_major ) {

  case 2:
    if ( show_execution_utilization )
      {
        (new Event_Group(this))->addv
          ("warps_launched",
           "gld_request", "gst_request",
           "local_load", "local_store",
           NULL);

        (new Event_Group(this))->addv
          ("warps_launched", "shared_load", "shared_store", NULL);

        eg_insn_types->addv
          ("warps_launched",
           "branch", "divergent_branch",
           NULL);

        if ( cc_minor == 1 )
          eg_insn_issue->addv
            ("inst_executed",
             "inst_issued1_0", "inst_issued2_0",
             "inst_issued1_1", "inst_issued2_1",
             "warps_launched",
             NULL);
        else
          eg_insn_issue->addv
            ("inst_executed",
             "inst_issued",
             "warps_launched",
             NULL);

        break;
      }

    if ( show_cache_data )
      {
        (new Event_Group(this))->addv
          ( "sm_cta_launched",
            "l1_shared_bank_conflict",  // Not Kepler
            NULL );
      }

    break;

  case 3:

    if ( show_group_2 )
      {
        (new Event_Group(this))->addv
          ("inst_executed",
           "thread_inst_executed",
           "not_predicated_off_thread_inst_executed",
           "warps_launched",
           NULL);

        // Texture / RO Cache Stuff


        (new Event_Group(this))->addv
          ("rocache_subp0_gld_warp_count_64b",
           "rocache_subp2_gld_warp_count_64b",
           NULL);

        (new Event_Group(this))->addv
          (
           "rocache_subp0_gld_warp_count_32b",
           "rocache_subp2_gld_warp_count_32b",
           NULL);

        (new Event_Group(this))->addv
          ("tex2_cache_sector_misses", NULL );
        (new Event_Group(this))->addv
          ("tex3_cache_sector_misses", NULL );
        (new Event_Group(this))->addv
          ("tex0_cache_sector_queries", NULL );
        (new Event_Group(this))->addv
          ("tex1_cache_sector_queries", NULL );
        (new Event_Group(this))->addv
          ("tex2_cache_sector_queries", NULL );
        (new Event_Group(this))->addv
          ("tex3_cache_sector_queries", NULL );


        // L2 Cache Stuff
#if 0
        // Not yet finished.
        eg_cache_l2->addv
          ("l2_subp0_read_sector_misses", "l2_subp0_read_hit_sectors",
           //  "l2_subp0_read_sector_queries",
           NULL);
        eg_cache_l21->addv
          ("l2_subp1_read_sector_misses", "l2_subp1_read_hit_sectors",
           //  "l2_subp0_read_sector_queries",
           NULL);
#endif
      }

    if ( show_execution_utilization )
      {
        // For Issue Success
        (new Event_Group(this))->addv
          ("inst_executed",
           "warps_launched",
           "inst_issued1",
           "inst_issued2",
           NULL);

        // Memory Divergence Replays
        eg_cache_shared->addv
          ( "sm_cta_launched",
            "shared_load_replay",
            "shared_store_replay",
            NULL );
        (new Event_Group(this))->addv
          ( "sm_cta_launched",
            "global_ld_mem_divergence_replays",
            "global_st_mem_divergence_replays",
            NULL );
        }

    if ( show_cache_data )
      {
        (new Event_Group(this))->addv
          ("warps_launched", "gld_request", "gst_request",
           NULL);
        (new Event_Group(this))->addv
          ("warps_launched", 
           "inst_executed",
           NULL);

        (new Event_Group(this))->addv
          ("warps_launched", "local_load", "local_store", NULL);

        (new Event_Group(this))->addv
          ("warps_launched", "shared_load", "shared_store", NULL);

        (new Event_Group(this))->addv
          ("rocache_gld_inst_32bit",
           "gld_inst_32bit",NULL);

        (new Event_Group(this))->addv
          ("rocache_gld_inst_64bit",
           "gld_inst_64bit",NULL);

        (new Event_Group(this))->addv
          ("rocache_gld_inst_128bit",
           "gld_inst_128bit",NULL);

        (new Event_Group(this))->addv
          ("rocache_subp3_gld_warp_count_32b",
           "rocache_subp1_gld_warp_count_32b",
           NULL);
        (new Event_Group(this))->addv
          ("rocache_subp3_gld_warp_count_64b",
           "rocache_subp1_gld_warp_count_64b",
           NULL);
        (new Event_Group(this))->addv
          ("rocache_subp3_gld_warp_count_128b",
           "rocache_subp1_gld_warp_count_128b",
           NULL);

        (new Event_Group(this))->addv
          ("tex0_cache_sector_misses", NULL );
        (new Event_Group(this))->addv
          ("tex1_cache_sector_misses", NULL );

      }

    break;

  default:
    // Error message needed here.
    break;
  }

}



 /*@@
   @routine    NVIDIAPerf_init
   @date       Sun Jan 15 14:22:36 2012
   @author     David Koppelman
   @desc
   Finish initialization of RT information collection.
   @enddesc
   @calls
   @calledby
   @history

   @endhistory

@@*/
void
NVIDIAPerf_init(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if ( !rt_info_on ) return;  // Cactus parameter.
  if ( !rt_info ) return;

  const int myproc = CCTK_MyProc(cctkGH);

  // Return if tracing really is needed.
  //
  if ( myproc == 0 ) return;

  // Here we turn off tracing, which had to be started before arguments
  // were available.

  rt_info->stop_tracing = true;
  rt_info->queues_dump_all();
  CU(cuptiEnableDomain(0, rt_info->subscriber, CUPTI_CB_DOMAIN_DRIVER_API));
  CU(cuptiEnableDomain(0, rt_info->subscriber, CUPTI_CB_DOMAIN_RUNTIME_API));
  CU(cuptiEnableDomain(0, rt_info->subscriber, CUPTI_CB_DOMAIN_RESOURCE));
  CU(cuptiEnableDomain(0, rt_info->subscriber, CUPTI_CB_DOMAIN_SYNCHRONIZE));
  CU(cuptiUnsubscribe(rt_info->subscriber));
}

int
NPerf_min_launches_get()
{
  if ( !rt_info ) return 0;
  return rt_info->eg_enabled_count;
}


CCTK_INT
rt_info_event_tracing_get()
{
  DECLARE_CCTK_PARAMETERS;
  if ( !rt_info_on ) return false;
  if ( !rt_info ) NVIDIAPerf_pre_init();
  return rt_info->tracing_get();
}

CCTK_INT
rt_info_event_tracing_set(CCTK_INT setting)
{
  DECLARE_CCTK_PARAMETERS;
  if ( !rt_info_on ) return false;
  if ( !rt_info ) NVIDIAPerf_pre_init();
  return rt_info->tracing_set(setting);
}

CCTK_INT
rt_info_event_tracing_off()
{
  DECLARE_CCTK_PARAMETERS;
  if ( !rt_info_on ) return false;
  if ( !rt_info ) NVIDIAPerf_pre_init();
  return rt_info->tracing_off();
}

void
NPerf_data_reset()
{
  DECLARE_CCTK_PARAMETERS;
  if ( !rt_info_on ) return;
  if ( !rt_info ) return;
  rt_info->data_reset();
}

double
NPerf_get_val_per_block(const char *name)
{
  DECLARE_CCTK_PARAMETERS;
  if ( !rt_info_on ) return -1;
  if ( !rt_info ) return -2;
  if ( !rt_info->kernel_last ) return -3;
  return rt_info->last_kernel_find_val_per_block(name);
}



bool
RT_Info::event_attr_unit_thread(const char *event_name)
{
  if ( !event_attr_inited )
    {
      event_attr_inited = true;

      // True if event is per thread, false if per warp.
      event_attr_attr["rocache_gld_inst_8bit"] = true;
      event_attr_attr["rocache_gld_inst_16bit"] = true;
      event_attr_attr["rocache_gld_inst_32bit"] = true;
      event_attr_attr["rocache_gld_inst_64bit"] = true;
      event_attr_attr["rocache_gld_inst_128bit"] = true;
      event_attr_attr["gld_inst_8bit"] = true;
      event_attr_attr["gld_inst_16bit"] = true;
      event_attr_attr["gld_inst_32bit"] = true;
      event_attr_attr["gld_inst_64bit"] = true;
      event_attr_attr["gld_inst_128bit"] = true;

      event_attr_attr["tex0_cache_sector_misses"] = true;
      event_attr_attr["tex1_cache_sector_misses"] = true;
      event_attr_attr["tex2_cache_sector_misses"] = true;
      event_attr_attr["tex3_cache_sector_misses"] = true;
    }
  EA_Hash::iterator ei = event_attr_attr.find(event_name);
  return ei == event_attr_attr.end() ? false : ei->second;
}


// For sort of kernels.
//
static bool comp_rti(RTI_Kernel_Info *a, RTI_Kernel_Info *b)
{
  return strcmp(a->func_name_demangled,b->func_name_demangled) < 0;
}
static bool comp_time(RTI_Kernel_Info *a, RTI_Kernel_Info *b)
{
  return a->elapsed_time_ns > b->elapsed_time_ns;
}

 /*@@
   @routine    NVIDIAPerf_atend
   @date       Sun Jan 15 14:31:15 2012
   @author     David Koppelman
   @desc
   Print run-time collected GPU resource and performance data.
   @enddesc
   @calls
   @calledby
   @history

   @endhistory

@@*/
CCTK_INT
NVIDIAPerf_atend()
{
  if ( !rt_info ) return 0;
  return rt_info->atend();
}

void
RT_Info::print_ki_heading
(Event_Group *eg, RTI_Kernel_Info *ki, const char *group_name)
{
  if ( !ki->display ) return;
  const int evt_call_count = eg->val(ki,"call_count");
  const bool have_warp_count = eg->have_val(ki,"warps_launched");
  const int warps_launched =
    have_warp_count ? int(eg->val(ki,"sm_cta_launched", "warps_launched")) : 0;
  printf(" Data From %s Group\n",group_name);
  printf("   Launches %4d,   Warps Sampled ", evt_call_count);
  if ( have_warp_count )
    printf("%5d (%5.1f / Launch)",
           warps_launched, double(warps_launched) / evt_call_count);
  else
    printf("NA");
  printf(",  %7.1f %s / Launch\n",
         eg->val(ki,"elapsed_time_ns") * ki->et_print_mult_from_ns
         / evt_call_count,
         ki->et_print_mult_txt);
}

EG_Cache_Data::EG_Cache_Data
(RT_Info *rti, RTI_Kernel_Info *ki, double miss_count_per_threadp,
 bool data_okay):
  rti(rti),eg(NULL),ki(ki),have_data(data_okay)
{
  miss_count_per_thread = mem_insn_per_thread = miss_count_per_threadp;
  miss_ratio = 1;
  miss_count_raw = miss_count_per_threadp;
  hit_count_raw = 0;

  const double elapsed_time_per_launch_ns =
    double(ki->elapsed_time_ns) / ki->call_count;

  assumed_load_bw_GBps =
    mem_insn_per_thread * ki->api_thread_count * rti->assumed_load_size
    / elapsed_time_per_launch_ns;
}

EG_Cache_Data
RT_Info::atend_cache_report
(Event_Group *eg, RTI_Kernel_Info *ki,
 const char *group_name,
 const char *evt_hit, const char *evt_miss,
 double mem_insn_per_thread, bool data_okay)
{
  EG_Cache_Data cd(this,eg,ki);
  if ( !data_okay || !eg->have_data(ki) ) return cd;
  cd.have_data = true;
  cd.mem_insn_per_thread = mem_insn_per_thread;

  if ( !evt_hit )
    print_ki_heading(eg,ki,group_name);

  const int evt_call_count = eg->val(ki,"call_count");
  const uint64_t warps_launched = eg->val(ki,"sm_cta_launched");
  const uint64_t load_hit =
    evt_hit ? eg->val(ki,evt_hit) : 0;
  const uint64_t load_miss = eg->val(ki,evt_miss);
  cd.miss_count_raw = load_miss;
  cd.hit_count_raw = load_hit;
  const bool assume_uncached = load_miss + load_hit == 0;

  if ( !evt_hit )
    {
      if ( ki->display )
        printf("   %s %lu\n", group_name, load_miss);
      cd.miss_ratio = 1;
      return cd;
    }
  
  const double insn_per_thread = ki->insn_per_thread;
  const int warps_per_sm_per_run =
    ki->warps_per_sm_per_launch * evt_call_count;

  // The following works for a Tesla 2050, CC 2.0.
  //
  cd.miss_bytes_per_load =
    assume_uncached
    ? assumed_load_size
    : load_miss * 2.0 / ( max(1.0, mem_insn_per_thread*warps_per_sm_per_run ) );

  const double miss_ratio =
    cd.miss_ratio = cd.miss_bytes_per_load / assumed_load_size;

  cd.miss_count_per_thread = mem_insn_per_thread * miss_ratio;

  const char *event_abbrev = evt_hit ? "Miss" : "Event";
  if ( false && !mem_insn_per_thread )
    printf("   %s / Thd  %.1f,  %s / kI %.2f\n",
           event_abbrev,
           cd.miss_count_per_thread,
           event_abbrev,
           1000.0 * load_miss / ( insn_per_thread * warps_launched * 32 ));

  const double elapsed_time_per_launch_ns =
    double(eg->val(ki,"elapsed_time_ns")) / evt_call_count;

  cd.assumed_load_bw_GBps =
    mem_insn_per_thread * miss_ratio * ki->api_thread_count * assumed_load_size
    / elapsed_time_per_launch_ns;

  return cd;
}

void
EG_Cache_Data::summary_line_print(const char *label)
{
  if ( !label )
    {
      ki->tot_miss_per_thread = 0;
      ki->tot_mem_insn_per_thread = 0;
      ki->tot_assumed_load_bw_GBps = 0;
      if ( !ki->display ) return;
      //   Miss / Thd   Miss / kI   Ratio    Data / GB/s  BW Util   (H,M)
      printf
        (" L1 Cache Data (from multiple groups) -------------------------------------\n");
      printf("   %-5s %10s  %10s  %7s  %8s  %8s  (Hit,Miss)\n",
             "",
             "Miss / Thd",
             "Miss / kI",
             "M Ratio",
             "GB / s",
             "BW Util");
      return;
    }

  if ( string(label) == "Total" )
    {
      if ( !ki->display ) return;
      printf("   %-5s %10.1f  %10.3f   %-6s   %7.3f  %7.3f%%\n",
             label,
             ki->tot_miss_per_thread,
             1000.0 * ki->tot_miss_per_thread / ki->insn_per_thread,
             "",
             ki->tot_assumed_load_bw_GBps,
             100.0 * ki->tot_assumed_load_bw_GBps
             / rti->device_memory_bandwidth_GBps
             );
      return;
    }

  if ( eg && !eg->have_data(ki) ) return;
  if ( !have_data ) return;

  ki->tot_miss_per_thread += miss_count_per_thread;
  ki->tot_mem_insn_per_thread += mem_insn_per_thread;
  ki->tot_assumed_load_bw_GBps += assumed_load_bw_GBps;

  if ( !ki->display ) return;

  printf("   %-5s %10.1f  %10.3f   %6.4f   %7.3f  %7.3f%%  (%.0f,%.0f)\n",
         label,
         miss_count_per_thread,
         1000.0 * miss_count_per_thread / ki->insn_per_thread,
         miss_ratio,
         assumed_load_bw_GBps,
         100.0 * assumed_load_bw_GBps / rti->device_memory_bandwidth_GBps,
         double(hit_count_raw), double(miss_count_raw)
         );
}

CCTK_INT
RT_Info::atend()
{
  DECLARE_CCTK_PARAMETERS;

  if ( !rt_info->cupti_inited ) return 0;
  if ( rt_info->stop_tracing ) return 0;

  // Suppress the possible early display of information still sitting
  // in the queues. (That information will be displayed in this routine.)
  //
  rt_info->display_early = false;

  if ( dev == device_null ) CE( cuCtxGetDevice(&dev) );

  // Note: Should probably call these on all devices that were used.
  //
  CE( cuCtxSynchronize() );
  cudaDeviceReset();

  device_data_collect();

  CU(cuptiEnableDomain(0, subscriber, CUPTI_CB_DOMAIN_DRIVER_API));
  CU(cuptiEnableDomain(0, subscriber, CUPTI_CB_DOMAIN_RUNTIME_API));
  CU(cuptiEnableDomain(0, subscriber, CUPTI_CB_DOMAIN_RESOURCE));
  CU(cuptiEnableDomain(0, subscriber, CUPTI_CB_DOMAIN_SYNCHRONIZE));
  CU(cuptiUnsubscribe(subscriber));

  // If necessary, lookup device information.
  //

  int dev_global_memory_bus_width_bits = 0;
  int dev_memory_clock_rate_khz = 0;
  CE( cuDeviceGetAttribute
      ( &dev_global_memory_bus_width_bits,
        CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH, dev) );
  CE( cuDeviceGetAttribute
      ( &dev_memory_clock_rate_khz,
        CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE, dev) );
  uint64_t attr_global_memory_bandwidth_kBps;
  size_t valSize = sizeof(attr_global_memory_bandwidth_kBps);
  CU( cuptiDeviceGetAttribute
      (dev, CUPTI_DEVICE_ATTR_GLOBAL_MEMORY_BANDWIDTH,
       &valSize, &attr_global_memory_bandwidth_kBps) );
  device_memory_bandwidth_GBps =
    attr_global_memory_bandwidth_kBps * 1e-6;

  int dev_l2_cache_size_bytes;
  CE( cuDeviceGetAttribute
      ( &dev_l2_cache_size_bytes, CU_DEVICE_ATTRIBUTE_L2_CACHE_SIZE, dev) );

  Device_Capabilities device_capabilities[CC_ENUM_SIZE];
  // Source: CUDA C Programming Guide Version 4.1 Table 5-1
  device_capabilities[CC_1x].fu_sp = 8;
  device_capabilities[CC_1x].fu_dp = 1;
  device_capabilities[CC_1x].fu_int_al = 8;
  device_capabilities[CC_1x].fu_int_sc = 8;
  device_capabilities[CC_1x].fu_int_m = 1;  // Multiple insn.
  device_capabilities[CC_1x].fu_conv = 2;
  device_capabilities[CC_1x].fu_ls = 8;
  device_capabilities[CC_1x].fu_special = 2;
  device_capabilities[CC_20].fu_sp = 32;
  device_capabilities[CC_20].fu_dp = 16;
  device_capabilities[CC_20].fu_int_al = 32;
  device_capabilities[CC_20].fu_int_sc = 16;
  device_capabilities[CC_20].fu_int_m = 16;
  device_capabilities[CC_20].fu_conv = 16;
  device_capabilities[CC_20].fu_ls = 16;
  device_capabilities[CC_20].fu_special = 4;
  device_capabilities[CC_21].fu_sp = 48;
  device_capabilities[CC_21].fu_dp = 4;
  device_capabilities[CC_21].fu_int_al = 48;
  device_capabilities[CC_21].fu_int_sc = 16;
  device_capabilities[CC_21].fu_int_m = 16;
  device_capabilities[CC_21].fu_conv = 16;
  device_capabilities[CC_21].fu_ls = 16;
  device_capabilities[CC_21].fu_special = 8;

  device_capabilities[CC_30].fu_sp = 192;
  device_capabilities[CC_30].fu_dp = 8;
  device_capabilities[CC_30].fu_int_al = 160;
  device_capabilities[CC_30].fu_int_sc = 32;
  device_capabilities[CC_30].fu_int_m = 32;
  device_capabilities[CC_30].fu_conv = 128;
  device_capabilities[CC_30].fu_ls = 64; // GUESS!
  device_capabilities[CC_30].fu_special = 32;

  device_capabilities[CC_35].fu_sp = 192;
  device_capabilities[CC_35].fu_dp = 64;
  device_capabilities[CC_35].fu_int_al = 160;
  device_capabilities[CC_35].fu_int_sc = 64;
  device_capabilities[CC_35].fu_int_m = 32;
  device_capabilities[CC_35].fu_conv = 128;
  device_capabilities[CC_35].fu_ls = 64; // GUESS!
  device_capabilities[CC_35].fu_special = 32;

  Device_Capabilities dev_cap = device_capabilities[cc_idx];

  const double clock_freq_hz = devprop.clockRate * 1000.0;
  const double clock_period_ns = 1e9 / clock_freq_hz;
  const int cores_per_mp = dev_cap.fu_sp;

  const double insn_p_ns_p_mp =
    clock_freq_hz * cores_per_mp * 1e-9;
  const double insn_p_ns = insn_p_ns_p_mp * numMultiprocessors;
  const double warps_p_ns_p_mp = insn_p_ns_p_mp / wp_sz;
  const double warps_p_ns = warps_p_ns_p_mp * numMultiprocessors;
  const double dev_flop_p_ns = 
    ( assume_dp ? dev_cap.fu_dp : dev_cap.fu_sp )
    * clock_freq_hz * numMultiprocessors * 1e-9;

  const int maxWarpsPerMultiprocessor = max_threads_per_mp >> wp_lg;


  // Retrieve remaining CUPTI records.
  //
  rt_info->queues_dump_all();

  // Sort kernel information by kernel name.
  //
  CUF_Hash& kafh = rt_info->kernel_info_fh;
  RTI_List sorted_time;
  int64_t total_time_ns = 0;
  for ( CUF_Hash::iterator it = kafh.begin(); it != kafh.end(); it++ )
    {
      RTI_Kernel_Info* const rti = &it->second;
      if ( !rti->call_count ) continue;
      total_time_ns += rti->elapsed_time_ns;
      sorted_time.push_back(&it->second);
    }
  sorted_time.sort( comp_time );
  const int64_t elapsed_time_threshold =
    total_time_ns * kernels_display_time_threshold;

  RTI_List sorted;
  int64_t total_time_show_ns = 0;
  int display_count = 0;
  for ( RTI_List::iterator it = sorted_time.begin();
        it != sorted_time.end(); it++ )
    {
      RTI_Kernel_Info* const rti = *it;
      const bool display =
        display_count < kernels_display_soft_limit
        || rti->elapsed_time_ns >= elapsed_time_threshold;
      rti->display = display;
      sorted.push_back(rti);
      if ( display )
        {
          display_count++;
          total_time_show_ns += rti->elapsed_time_ns;
        }
    }
  sorted.sort( comp_rti );

  const char* const div = "========================================";
  printf("%s%s\n",div,div);
  printf("Run-Time GPU Resource and Performance Data (%s)\n\n",
         CCTK_THORNSTRING);

  printf("GPU Dev %d  %s,  CC %d.%01d,  CUDA Cores / MP %d,  "
         "Freq %.3f GHz,  %d MP\n",
         dev, gpu_name,
         cc_major, cc_minor, cores_per_mp,
         clock_freq_hz * 1e-9, numMultiprocessors);
  printf("Execution rate %.1f insn/ns  %.1f warps/ns (using all MPs)\n",
         insn_p_ns, warps_p_ns);
  printf("Memory Bandwidth %.3f GB/s,  L2 Cache %d kiB.\n",
         device_memory_bandwidth_GBps,
         dev_l2_cache_size_bytes >> 10);
  uint32_t cupti_version;  CU( cuptiGetVersion(&cupti_version) );
  printf("CUPTI API build %d, run %d.\n",
         CUPTI_API_VERSION, cupti_version);

  printf("Number of launches needed: %zd events, %d metric.\n",
         eg_enabled.size(), metrics.eg_sets->numSets );

#define printf if ( ki->display ) printf

  // Print info on each kernel.
  //
  for ( RTI_List::iterator it = sorted.begin(); it != sorted.end(); it++ )
    {
      RTI_Kernel_Info* const ki = *it;
      Version_ActivityKernel& k = ki->cupti_kernel;

      printf("\nFor Kernel \"%s\"  (addr %p)\n",
             ki->func_name_demangled,
             ki->cu_function);

      if ( ki->have_cupti_kernel_data )
        {
          const int block_count = k.gridX * k.gridY * k.gridZ;
          const int block_size = k.blockX * k.blockY * k.blockZ;
          const int warps_per_block = ( ( block_size + wp_mask ) >> wp_lg );
          const double blocks_per_mp = ki->api_blocks_per_mp_avg;
          const double mp_occ = 100.0 * min(1.0,blocks_per_mp);
          
          const int cache_size_small_kiB = 16;
          const int cache_size_equal_kiB = 32;
          const int cache_size_large_kiB = 48;

          const int cache_size_kiB =
            k.version_cache_executed == CU_FUNC_CACHE_PREFER_SHARED
            ? cache_size_small_kiB :
            k.version_cache_executed == CU_FUNC_CACHE_PREFER_L1
            ? cache_size_large_kiB :
            k.version_cache_executed == CU_FUNC_CACHE_PREFER_EQUAL
            ? cache_size_equal_kiB : -1;
          const int dev_shared_size = ( 64 - cache_size_kiB ) << 10;

          printf(" Data From Kernel Activity API\n");

          printf("   Launches %6d\n"
                 "   Block size [%3d, %3d, %2d] = %5d threads = %2d warps.\n"
                 "   Grid size  [%3d, %3d, %2d] = %5d blocks,  %.1f per MP.\n",
                 ki->call_count,
                 k.blockX, k.blockY, k.blockZ, block_size,
                 block_size + 31 >> wp_lg,
                 k.gridX, k.gridY, k.gridZ, block_count, blocks_per_mp
                 );

          printf
            ("   Shared mem (stat + dyn) %6u + %6u B,  registers %d\n",
             k.staticSharedMemory, k.dynamicSharedMemory, k.registersPerThread);

          const int ker_shared_size =
            k.staticSharedMemory + k.dynamicSharedMemory;

          const int limit_blocks_by_warps =
            maxWarpsPerMultiprocessor / warps_per_block;
          const int limit_blocks_by_shared_mem =
            ker_shared_size == 0 ? limit_blocks_by_warps :
            dev_shared_size / ker_shared_size;
          const int limit_blocks_by_registers =
            devprop.regsPerBlock / k.registersPerThread / block_size;
          const int active_blocks_per_mp = ki->active_blocks_per_mp =
            min(min(limit_blocks_by_warps,limit_blocks_by_shared_mem),
                min(max(1,int(blocks_per_mp)),limit_blocks_by_registers));

          const int active_warps_per_mp = ki->active_warps_per_mp = 
            active_blocks_per_mp * warps_per_block;
          double warp_occ_pct =
            100 * double(active_warps_per_mp) / maxWarpsPerMultiprocessor;

          const int limit_warps_by_config =
            warps_per_block * ( max( 1, block_count / numMultiprocessors ) );
          const int limit_warps_by_regs =
            warps_per_block * limit_blocks_by_registers;
          const int limit_warps_by_shared_mem =
            ker_shared_size == 0 ? maxWarpsPerMultiprocessor :
            warps_per_block * limit_blocks_by_shared_mem;

          printf
            ("   L1 Cache Size %d kiB,  %.1f B / Thd.\n",
             cache_size_kiB,
             cache_size_kiB * 1024.0 / ( active_warps_per_mp * 32.0 ) );

          printf
            ("   Multiprocessor occupancy: %.1f%%  %s, "
             "based on %d blocks and %d MP.\n",
             mp_occ, mp_occ >= 99.9 ? "good" : "bad",
             block_count, numMultiprocessors);

          const char* const limiter_text =
            active_warps_per_mp >= maxWarpsPerMultiprocessor ? "hardware" :
            active_warps_per_mp == limit_warps_by_config ? "launch size" :
            active_warps_per_mp == limit_warps_by_regs ? "registers" :
            active_warps_per_mp == limit_warps_by_shared_mem ? "shared memory" :
            "???";

          printf
            ("   Warp Occupancy: %.1f%%  %s, based on %d active warps, "
             "limited by %s.\n",
             warp_occ_pct, warp_occ_pct > 99.9 ? "good" : "bad",
             active_warps_per_mp, limiter_text);

#define GOOD(m)                                                 \
          (m) >= maxWarpsPerMultiprocessor ? "good" :           \
            (m) > active_warps_per_mp ? "bad" : "limiter"

          printf
            ("   Number of active warps is minimum of the following limits:\n");
          printf
            ("     HW %d,  launch size %d (%s),  regs %d (%s), "
             "shared mem: %d (%s).\n",
             maxWarpsPerMultiprocessor,
             limit_warps_by_config, GOOD(limit_warps_by_config),
             limit_warps_by_regs, GOOD(limit_warps_by_regs),
             limit_warps_by_shared_mem, GOOD(limit_warps_by_shared_mem)
             );

        }

      const int api_block_size = ki->api_block_size;
      const int api_block_count = ki->api_block_count;
      const double api_blocks_per_mp = ki->api_blocks_per_mp_avg;

      if ( ki->api_block.x )
        {
          const char* const mult_abbr[] = { "ns", "µs", "ms", "s" };
          const int mult_limit = sizeof(mult_abbr)/sizeof(mult_abbr[0]);
          const int dig_grps =
            min( mult_limit,
                 max(0, int(log10(ki->elapsed_time_ns))/3-1));
          ki->et_print_mult_txt = mult_abbr[dig_grps];
          ki->et_print_mult_from_ns = pow(10,-dig_grps*3);
          
          printf(" Data From API Hooks\n");
          printf("   Launches %5d,                   "
                 "Total Execution Time %8.1f %2s\n",
                 ki->call_count,
                 ki->elapsed_time_ns * ki->et_print_mult_from_ns,
                 ki->et_print_mult_txt);
          printf("   Block size [%3d, %3d, %2d] = %5d threads = %2d warps.\n"
                 "   Grid size  [%3d, %3d, %2d] = %5d blocks,  %.1f per MP.\n",
                 ki->api_block.x, ki->api_block.y, ki->api_block.z,
                 api_block_size, api_block_size + 31 >> wp_lg,
                 ki->api_grid.x, ki->api_grid.y, ki->api_grid.z,
                 api_block_count, api_blocks_per_mp);
        }

      //
      // Compute Block / MP Average
      //
      {
        double obs_wp_per_sm_sum = 0;
        int obs_wp_per_sm_count = 0;
        for ( int i=0; i<eg_enabled_count; i++ )
          {
            Event_Group* const eg = eg_enabled[i];
            if ( ! eg->have_val(ki,"warps_launched") ) continue;
            const uint64_t evt_warps_launched = eg->val(ki,"warps_launched");
            const int evt_call_count = eg->val(ki,"call_count");
            if ( !evt_call_count ) continue;
            const double e_blk_mp = evt_warps_launched / evt_call_count;
            obs_wp_per_sm_sum += e_blk_mp;
            obs_wp_per_sm_count++;
          }
        ki->warps_per_sm_per_launch =
          obs_wp_per_sm_sum / max(1,obs_wp_per_sm_count);
      }

      //
      // Event Data
      //
      if ( Event_Group* const eg = eg_find(ki,"divergent_branch") )
        {
          const uint64_t evt_branch = eg->val(ki,"branch");
          const uint64_t evt_divergent_branch = eg->val(ki,"divergent_branch");

          printf(" Branch %lu  Diverg %lu\n", evt_branch, evt_divergent_branch);
        }

      if ( Event_Group* const eg =
           eg_find(ki,"not_predicated_off_thread_inst_executed") )
        {
          const uint64_t evt_warps_launched =
            eg->val(ki,"warps_launched");
          const uint64_t evt_inst_executed =
            eg->val(ki, "inst_executed");
          const uint64_t evt_thread_inst_executed =
            eg->val(ki, "thread_inst_executed");
          const uint64_t evt_live =
            eg->val(ki, "not_predicated_off_thread_inst_executed");
          printf(" WL %lu  Exec %lu  TExec %lu  Live %lu  Rat: %.3f\n",
                 evt_warps_launched,
                 evt_inst_executed,
                 evt_thread_inst_executed,
                 evt_live,
                 evt_thread_inst_executed / double(evt_inst_executed)
                 );
          const double insn_exec_ref_inv = 
            1.0 / ( 128 * double(evt_inst_executed) );
          printf(" WL  Eff %.4f  Eff %.4f\n",
                 evt_thread_inst_executed * insn_exec_ref_inv,
                 evt_live * insn_exec_ref_inv );

        }

      ki->notice_missing_start();
      const double lld_per_thread = eg_find_val_per_thread(ki,"local_load");
      const double lst_per_thread = eg_find_val_per_thread(ki,"local_store");
      const bool l_per_thread_okay = ki->notice_missing_check_okay_start();
      const double sld_per_thread = eg_find_val_per_thread(ki,"shared_load");
      const double sst_per_thread = eg_find_val_per_thread(ki,"shared_store");
      const bool s_per_thread_okay = ki->notice_missing_check_okay_start();
      const double gld_per_thread = eg_find_val_per_thread(ki,"gld_request");
      const double gst_per_thread = eg_find_val_per_thread(ki,"gst_request");
      const bool grw_per_thread_okay = ki->notice_missing_check_okay_start();

      const double grosp_per_thread_32b =
        cc_idx < CC_30 ? 0 :
        2 * eg_find_val_per_thread(ki,"rocache_subp1_gld_warp_count_32b")
          + eg_find_val_per_thread(ki,"rocache_subp3_gld_warp_count_32b");
      const double grosp_per_thread_64b =
        cc_idx < CC_30 ? 0 :
        2 * eg_find_val_per_thread(ki,"rocache_subp1_gld_warp_count_64b")
          + eg_find_val_per_thread(ki,"rocache_subp3_gld_warp_count_64b");
      const double grosp_per_thread_128b =
        cc_idx < CC_30 ? 0 :
        2 * eg_find_val_per_thread(ki,"rocache_subp1_gld_warp_count_128b")
          + eg_find_val_per_thread(ki,"rocache_subp3_gld_warp_count_128b");

      const double grosp_per_thread =
        grosp_per_thread_32b + grosp_per_thread_64b + grosp_per_thread_128b;

      const bool gnc_per_thread_okay = ki->notice_missing_check_okay_start();

      const double groi_per_thread =
        cc_idx < CC_30 ? 0 :
        eg_find_val_per_thread(ki,"rocache_gld_inst_32bit")
        + eg_find_val_per_thread(ki,"rocache_gld_inst_64bit")
        + eg_find_val_per_thread(ki,"rocache_gld_inst_128bit");

      const bool gro_per_thread_okay = ki->notice_missing_check_okay_start();

      const double gro_per_thread = groi_per_thread;
      const double gnc_per_thread =
                 max(0.0, grosp_per_thread - groi_per_thread);

      printf("gld pt: %.1f %.1f %.1f \n",
             eg_find_val_per_thread(ki,"gld_inst_32bit"),
             eg_find_val_per_thread(ki,"gld_inst_64bit"),
             eg_find_val_per_thread(ki,"gld_inst_128bit"));

      if ( Event_Group* const eg = eg_find(ki,"tex0_cache_sector_queries") )
        {
          ki->notice_missing_start();
          const uint64_t tc_m =
            eg_find_val(ki,"tex0_cache_sector_misses");
          const double ro_gld_32b_pt =
            eg_find_val_per_thread(ki,"rocache_subp3_gld_warp_count_32b");
          // Requests via "slice 3".
          const double ro_gld_64b_pt =
            eg_find_val_per_thread(ki,"rocache_subp3_gld_warp_count_64b");
          const double ro_gld_128b_pt =
            eg_find_val_per_thread(ki,"rocache_subp3_gld_warp_count_128b");
          const double ro_gld_pt =
            ro_gld_32b_pt + ro_gld_64b_pt + ro_gld_128b_pt;
          const double miss_bytes_pt =
            eg_find_val_per_thread(ki,"tex0_cache_sector_misses") * 128;
          const double ro_gld_bytes_pt =
            4 * ro_gld_32b_pt + 8 * ro_gld_64b_pt + 16 * ro_gld_128b_pt;

          printf("RO GLD PT 32b  %.1f %.1f %.1f %.1f\n",
            eg_find_val_per_thread(ki,"rocache_subp0_gld_warp_count_32b"),
            eg_find_val_per_thread(ki,"rocache_subp1_gld_warp_count_32b"),
            eg_find_val_per_thread(ki,"rocache_subp2_gld_warp_count_32b"),
            eg_find_val_per_thread(ki,"rocache_subp3_gld_warp_count_32b"));
          printf("RO GLD PB 32b  %lu %lu %lu %lu\n",
            eg_find_val_per_block(ki,"rocache_subp0_gld_warp_count_32b"),
            eg_find_val_per_block(ki,"rocache_subp1_gld_warp_count_32b"),
            eg_find_val_per_block(ki,"rocache_subp2_gld_warp_count_32b"),
            eg_find_val_per_block(ki,"rocache_subp3_gld_warp_count_32b"));

          printf("RO GLD PB 64b %lu  %lu  %lu  %lu \n",
                 eg_find_val_per_block(ki,"rocache_subp0_gld_warp_count_64b"),
                 eg_find_val_per_block(ki,"rocache_subp1_gld_warp_count_64b"),
                 eg_find_val_per_block(ki,"rocache_subp2_gld_warp_count_64b"),
                 eg_find_val_per_block(ki,"rocache_subp3_gld_warp_count_64b")
                 );

          printf("TEX sect misses PB %lu %lu %lu %lu  "
                 "Queries PB %lu %lu %lu %lu\n",
                 eg_find_val_per_block(ki,"tex0_cache_sector_misses"),
                 eg_find_val_per_block(ki,"tex1_cache_sector_misses"),
                 eg_find_val_per_block(ki,"tex2_cache_sector_misses"),
                 eg_find_val_per_block(ki,"tex3_cache_sector_misses"),
                 eg_find_val_per_block(ki,"tex0_cache_sector_queries"),
                 eg_find_val_per_block(ki,"tex1_cache_sector_queries"),
                 eg_find_val_per_block(ki,"tex2_cache_sector_queries"),
                 eg_find_val_per_block(ki,"tex3_cache_sector_queries")
                 );

          printf("TEX pt sect misses %.1f %.1f %.1f %.1f  "
                 "Queries %.1f %.1f %.1f %.1f\n",
                 eg_find_val_per_thread(ki,"tex0_cache_sector_misses"),
                 eg_find_val_per_thread(ki,"tex1_cache_sector_misses"),
                 eg_find_val_per_thread(ki,"tex2_cache_sector_misses"),
                 eg_find_val_per_thread(ki,"tex3_cache_sector_misses"),
                 eg_find_val_per_thread(ki,"tex0_cache_sector_queries"),
                 eg_find_val_per_thread(ki,"tex1_cache_sector_queries"),
                 eg_find_val_per_thread(ki,"tex2_cache_sector_queries"),
                 eg_find_val_per_thread(ki,"tex3_cache_sector_queries")
                 );

          if ( ki->notice_missing_check_okay() )
            printf
              ("TEX %lu  %lu  RO Acc: %.1f (%.1f/%.1f/%.1f)  MR %.4f  %.2f\n",
               tc_m,
               eg_find_val(ki,"tex1_cache_sector_misses"),
               ro_gld_pt,
               ro_gld_32b_pt,ro_gld_64b_pt,ro_gld_128b_pt,
               miss_bytes_pt / max(1.0,ro_gld_bytes_pt),
               miss_bytes_pt
               );
        }

      if ( Event_Group* const eg = eg_find(ki,"inst_executed") )
        {
          const uint64_t evt_warps = eg->val(ki,"inst_executed");
          const uint64_t evt_warps_launched = eg->val(ki,"warps_launched");
          const uint64_t evt_elapsed_time_ns = eg->val(ki,"elapsed_time_ns");

          const double insn_per_thread = ki->insn_per_thread =
            double(evt_warps) / evt_warps_launched;
          // ipt: Instructions per thread.
          const double ipt_mem =
            gld_per_thread + gst_per_thread + gro_per_thread
            + sld_per_thread + sst_per_thread
            + lld_per_thread + lst_per_thread;

          print_ki_heading(eg,ki,"Execution Utilization");

          printf
            ("   Insn / Thd         %10.1f,   Mem Insn /Thd        %10.1f\n",
             insn_per_thread, ipt_mem);
          if ( grw_per_thread_okay )
            printf
              ("   Global Load / Thd  %10.1f,   Global Store / Thd   %10.1f\n",
               gld_per_thread, gst_per_thread);
          if ( gro_per_thread_okay )
            printf
              ("   Global RO   / Thd  %10.1f\n",
               gro_per_thread);
          if ( gnc_per_thread_okay )
            printf
              ("   Global NC   / Thd  %10.1f\n",
               gnc_per_thread);
          if ( s_per_thread_okay )
            printf
              ("   Shared Load / Thd  %10.1f,   Shared Store / Thd   %10.1f\n",
               sld_per_thread, sst_per_thread);
          if ( l_per_thread_okay )
            printf
              ("   Local Load / Thd   %10.1f,   Local Store / Thd    %10.1f\n",
               lld_per_thread, lst_per_thread);

          // Assume that address arithmetic not needed for local access.
#if 0
          const double ipt_mem_gs =
            gld_per_thread + gst_per_thread + gro_per_thread
            + sld_per_thread + sst_per_thread;
#endif
          const double ipt_other =
            insn_per_thread - ipt_mem;

#if 1
          const char* const mix_model = "Both sources from mem.";
          const double ipt_other_fp = ki->ipt_other_fp =
            ( gld_per_thread + sld_per_thread ) / 2;
          const double ipt_other_int = ipt_other - ipt_other_fp;
#else
          // Shelved Code: Estimate int insn count based on need for
          // address arithmetic.
          const char* const mix_model = "Int to load ratio.";
          const double model_assume_int_per_gload = 2;
          const double ipt_other_int_old =
            min( ipt_mem_gs * model_assume_int_per_gload
                 + ( max( ipt_other - ipt_mem_gs * model_assume_int_per_gload,
                          0.0 )
                     * model_assume_int_percent_alu ),
                 ipt_other );
          const double ipt_other_int = ipt_other * model_assume_int_percent_alu;
          const double ipt_other_fp =
            ki->ipt_other_fp = ipt_other - ipt_other_int;
#endif

          const double effective_int_fu =
            ( dev_cap.fu_int_al + dev_cap.fu_int_sc ) / 2.0;
          const double issue_cycles_part_non_fp =
            ipt_other_int / effective_int_fu;
          //  + ipt_mem / dev_cap.fu_ls;
          // Issue Cycles Single-Precision Per Thread
          const double issue_cycles_sp_pt =
            issue_cycles_part_non_fp
            + ipt_other_fp / dev_cap.fu_sp;
          const double issue_cycles_dp_pt =
            issue_cycles_part_non_fp
            + ipt_other_fp / dev_cap.fu_dp;

          const double threads_per_cycle =
            wp_sz * evt_warps_launched * clock_period_ns
            / evt_elapsed_time_ns;
          const double util_sp_pct = ki->util_sp_pct =
            100 * issue_cycles_sp_pt * threads_per_cycle;
          const double util_dp_pct = ki->util_dp_pct =
            100 * issue_cycles_dp_pt * threads_per_cycle;
          const bool use_dp = assume_dp;

          if ( gro_per_thread_okay && grw_per_thread_okay
               && l_per_thread_okay && s_per_thread_okay )
            {
              printf
                ("   Execution Utilization %7.1f%% %s  (%7.1f%% %s)\n",
                 use_dp ? util_dp_pct : util_sp_pct,
                 use_dp ? "double" : "single",
                 !use_dp ? util_dp_pct : util_sp_pct,
                 !use_dp ? "double" : "single");
              printf
                ("   -- Assuming: int %.1f%% / %.0f FU,  FP %.1f%% / %d FU,  "
                 "mem %.1f%% / %d FU.\n"
                 "   -- Model Name: \"%s\"\n",
                 100 * ipt_other_int / insn_per_thread,
                 effective_int_fu,
                 100 * ipt_other_fp / insn_per_thread,
                 use_dp ? dev_cap.fu_dp : dev_cap.fu_sp,
                 100 * ipt_mem / insn_per_thread,
                 dev_cap.fu_ls,mix_model);
            }
        }

      if ( ki->md.set_rounds > 0 )
        {
          Metrics_Data& md = ki->md;
          const double et_ns =
            ki->elapsed_time_lite_ns / max(1,ki->call_count_lite);
          const double cycles = 1e-9 * et_ns * clock_freq_hz;
          printf
            ("\nMetrics stuff collected over %d rounds. Each launch "
             "%.0f cyc or %.6f ms\n",
             md.set_rounds, cycles, et_ns * 1e-6);

          for ( auto &elt: metrics.metric_info )
            {
              Metric_Info& mi = elt.second;

              //  CUPTI_METRIC_ATTR_NAME
              //  CUPTI_METRIC_ATTR_SHORT_DESCRIPTION
              //  CUPTI_METRIC_ATTR_LONG_DESCRIPTION

              //  * The metric value is a 64-bit double.
              // CUPTI_METRIC_VALUE_KIND_DOUBLE            = 0,
              //  * The metric value is a 64-bit unsigned integer.
              // CUPTI_METRIC_VALUE_KIND_UINT64            = 1,
              //  * The metric value is a percentage represented by a 64-bit
              //  * double. For example, 57.5% is represented by the value 57.5.
              // CUPTI_METRIC_VALUE_KIND_PERCENT           = 2,
              //  * The metric value is a throughput represented by a 64-bit
              // CUPTI_METRIC_VALUE_KIND_THROUGHPUT        = 3,
              //  * The metric value is a 64-bit signed integer.
              // CUPTI_METRIC_VALUE_KIND_INT64             = 4,
              //  * The metric value is a utilization level, as represented by
              //  * CUpti_MetricValueUtilizationLevel.
              // CUPTI_METRIC_VALUE_KIND_UTILIZATION_LEVEL = 5

              NPerf_Metric_Value npv = metric_value_get(ki->func_name,mi.name);
              printf("%-25s  ",mi.name.c_str());

              switch ( npv.kind ) {
              case CUPTI_METRIC_VALUE_KIND_DOUBLE:
                printf("%12.8f\n",npv.metric_value.metricValueDouble);
                break;
              case CUPTI_METRIC_VALUE_KIND_UINT64:
                printf("%12lu or %7.1f per wp or %7.1f per thd\n",
                       npv.metric_value.metricValueUint64,
                       npv.value_per_warp_get(),
                       npv.value_per_thread_get() );
                break;
              case CUPTI_METRIC_VALUE_KIND_INT64:
                printf("%ld\n",npv.metric_value.metricValueInt64);
                break;
              case CUPTI_METRIC_VALUE_KIND_THROUGHPUT:
                printf
                  ("%10.6f GB/s\n", npv.metric_value.metricValueInt64 * 1e-9);
                break;
              case CUPTI_METRIC_VALUE_KIND_PERCENT:
                printf("%10.6f%%\n",npv.metric_value.metricValuePercent);
                break;
              case CUPTI_METRIC_VALUE_KIND_UTILIZATION_LEVEL:
                printf
                  ("%3d (0-10)\n",npv.metric_value.metricValueUtilizationLevel);
                break;
              default:
                assert(false);
              }
            }
        }


      if ( Event_Group*  const eg = eg_find(ki,"inst_issued1_0") )
        { // CC 2.1
          const uint64_t evt_inst_executed =
            eg->val(ki,"inst_executed");
          const double evt_inst_issued1_0 =
            eg->val(ki,"inst_issued1_0");
          const double evt_inst_issued2_0 =
            eg->val(ki,"inst_issued2_0");
          const double evt_inst_issued1_1 =
            eg->val(ki,"inst_issued1_1");
          const double evt_inst_issued2_1 =
            eg->val(ki,"inst_issued2_1");
          const double inst_issued =
            evt_inst_issued1_0 + 2 * evt_inst_issued2_0
            + evt_inst_issued1_1 + 2 * evt_inst_issued2_1;

          const uint64_t evt_warps_launched =
            eg->val(ki,"warps_launched");
          print_ki_heading(eg,ki,"Instruction Re-Issue");
          printf("   Issue Success %.4f  Replay per warp %.1f\n"
                 "   (ex,is, i10,i20, i11,i21) "
                 "(%.0f,%.0f, %.0f,%.0f, %.0f,%.0f)\n",
                 double(evt_inst_executed) / max(1.0,inst_issued),
                 ( inst_issued - evt_inst_executed ) / evt_warps_launched,
                 double(evt_inst_executed), inst_issued,
                 evt_inst_issued1_0, evt_inst_issued2_0,
                 evt_inst_issued1_1, evt_inst_issued2_1);
        }

      if ( Event_Group*  const eg = eg_find(ki,"inst_issued") )
        {
          const uint64_t evt_inst_executed =
            eg->val(ki,"inst_executed");
          const uint64_t evt_inst_issued =
            eg->val(ki,"inst_issued");
          const uint64_t evt_warps_launched =
            eg->val(ki,"warps_launched");
          print_ki_heading(eg,ki,"Instruction Re-Issue");
          printf("   Issue Success %.4f  Replay per warp %.1f\n"
                 "   (exec,issue) (%.0f,%.0f)\n",
                 double(evt_inst_executed)/ max(uint64_t(1),evt_inst_issued),
                 double( evt_inst_issued - evt_inst_executed )
                 / evt_warps_launched,
                 double(evt_inst_executed), double(evt_inst_issued)
                 );
        }

      if ( Event_Group*  const eg = eg_find(ki,"inst_issued1") )
        { // CC 3.5
          const uint64_t evt_inst_executed =
            eg->val(ki,"inst_executed");
          const uint64_t evt_inst_issued1 =
            eg->val(ki,"inst_issued1");
          const uint64_t evt_inst_issued2 =
            eg->val(ki,"inst_issued2");
          const int64_t evt_inst_issued =
            evt_inst_issued1 + 2 * evt_inst_issued2;
          const uint64_t evt_warps_launched =
            eg->val(ki,"warps_launched");
          print_ki_heading(eg,ki,"Instruction Re-Issue");
          printf("   Exec %lu  I1 %lu  I2 %lu\n",
                 evt_inst_executed,
                 evt_inst_issued1,
                 evt_inst_issued2);
          printf("   Issue Success %.4f  or  %.4f Replay per warp %.1f\n"
                 "   (exec,issue) (%.0f,%.0f)\n",
                 double(evt_inst_executed)/ max(int64_t(1),evt_inst_issued),
                 double(evt_inst_executed)
                 / max(uint64_t(1),evt_inst_issued1+evt_inst_issued2),
                 double( evt_inst_issued - evt_inst_executed )
                 / evt_warps_launched,
                 double(evt_inst_executed), double(evt_inst_issued)
                 );
        }

      if ( eg_insn_utilization_2->have_data(ki) )
        {
          print_ki_heading(eg_insn_utilization_2,ki,
                           "Execution Utilization Two");

          const double evt_elapsed_time_ns =
            eg_insn_utilization_2->val(ki,"elapsed_time_ns");
          const double elapsed_time_cyc =
            1e-9 * evt_elapsed_time_ns * clock_freq_hz;
          const uint64_t evt_active_cycles =
            eg_insn_utilization_2->val(ki,"active_cycles");
          const uint64_t evt_active_warps =
            eg_insn_utilization_2->val(ki,"active_warps");

          printf("   Average Occupancy  %.1f warps,  Active Cycles  %.1f%%\n",
                 double(evt_active_warps) / evt_active_cycles,
                 100.0 * evt_active_cycles / elapsed_time_cyc);
        }

      if ( Event_Group* const eg = eg_find(ki,"shared_load_replay") )
        {
          const double evt_shared_load_replay =
            eg_find_val_per_block(ki,"shared_load_replay");
          const double evt_shared_store_replay =
            eg_find_val_per_block(ki,"shared_store_replay");
          printf(" Memory Diverg Replay per Bl: (sld,sst,gld,gst) "
                 "%.0f  %.0f  %lu %lu\n",
                 evt_shared_load_replay, evt_shared_store_replay,
                 eg_find_val_per_block(ki,"global_ld_mem_divergence_replays"),
                 eg_find_val_per_block(ki,"global_st_mem_divergence_replays")
                 );
        }

      if ( show_cache_data )
        {
          ki->ci_global_load =
            atend_cache_report
            (eg_cache,ki,"Global L1 Load",
             "l1_global_load_hit",
             "l1_global_load_miss",gld_per_thread,grw_per_thread_okay);

          EG_Cache_Data
            ci_global_store(this,ki,gst_per_thread,grw_per_thread_okay);

          EG_Cache_Data cgr(this,ki);
          ki->ci_global_ro = cgr;
          EG_Cache_Data cnc(this,ki);
          ki->ci_global_nc = cnc;

          if ( cc_idx == CC_35 )
            {
              // CC 3.5 - RO and NC Cache Miss Ratio
              ki->notice_missing_start();
              ki->ci_global_ro.mem_insn_per_thread = gro_per_thread;
              ki->ci_global_ro.hit_count_raw =
                ki->ci_global_ro.miss_count_raw = 0;

              ki->ci_global_nc.mem_insn_per_thread = gnc_per_thread;
              ki->ci_global_nc.hit_count_raw =
                ki->ci_global_nc.miss_count_raw = 0;

              // Cacheable Texture Cache Accesses
              const double gro_bytes_pt =
                4 * eg_find_val_per_thread(ki,"rocache_gld_inst_32bit")
                + 8 * eg_find_val_per_thread(ki,"rocache_gld_inst_64bit")
                + 16 * eg_find_val_per_thread(ki,"rocache_gld_inst_128bit");

              // Total Texture Cache Accesses
              const double tex_bytes_pt = 4 * grosp_per_thread_32b
                + 8 * grosp_per_thread_64b + 16 * grosp_per_thread_128b;

              const double gnc_bytes_pt = max(0.0,tex_bytes_pt-gro_bytes_pt);

              // Could add tex2 and tex 3, if so adjust tex_c_bytes_pt.
              ki->notice_missing_start();
              const uint64_t tex_c_m =
                eg_find_val_per_block(ki,"tex0_cache_sector_misses") +
                eg_find_val_per_block(ki,"tex1_cache_sector_misses");

              const bool miss_data_okay = ki->notice_missing_check_okay();

              const int warp_count = ki->api_warps_per_block;
              const int tex_c_sampled_warps =
                ( warp_count >> 1 ) + ( ( warp_count & 0x3 ) == 0x3 );

              const double tex_c_bytes_pt =
                32.0 * tex_c_m * warp_count
                / max( 1, tex_c_sampled_warps * api_block_size );

              // Texture Cache, Noncached Access
              ki->ci_global_nc.miss_ratio = 
                gro_per_thread ? 1.0 : tex_c_bytes_pt / max(1.0,tex_bytes_pt);

              // Texture Cache, Cacheable Access
              ki->ci_global_ro.miss_ratio =
                gro_per_thread
                ? ( tex_c_bytes_pt - gnc_bytes_pt ) / max(1.0,gro_bytes_pt)
                : 0;

              ki->ci_global_ro.miss_count_per_thread =
                ki->ci_global_ro.miss_ratio * gro_per_thread;

              ki->ci_global_nc.miss_count_per_thread =
                ki->ci_global_nc.miss_ratio * gnc_per_thread;

              const double ro_miss_bytes_pt =
                ki->ci_global_ro.miss_ratio * gro_bytes_pt;

              ki->ci_global_ro.miss_bytes_per_load =
                ro_miss_bytes_pt / max(1.0,gro_per_thread);

              const double nc_miss_bytes_pt =
                ki->ci_global_nc.miss_ratio * gnc_bytes_pt;

              ki->ci_global_nc.miss_bytes_per_load =
                nc_miss_bytes_pt / max(1.0,gnc_per_thread);

              const double elapsed_time_per_launch_ns =
                double(ki->elapsed_time_ns) / ki->call_count;
              ki->ci_global_ro.assumed_load_bw_GBps =
                ro_miss_bytes_pt * ki->api_thread_count
                / elapsed_time_per_launch_ns;

              ki->ci_global_nc.assumed_load_bw_GBps =
                nc_miss_bytes_pt * ki->api_thread_count
                / elapsed_time_per_launch_ns;

              ki->ci_global_ro.have_data = miss_data_okay;
              ki->ci_global_nc.have_data = miss_data_okay;
            }

          EG_Cache_Data ci_local_load =
            atend_cache_report
            (eg_cache_local,ki,"Local L1 Load",
             "l1_local_load_hit",
             "l1_local_load_miss",lld_per_thread,l_per_thread_okay);

          EG_Cache_Data ci_local_store =
            atend_cache_report
            (eg_cache_l1_lst,ki,"Local L1 Store",
             "l1_local_store_hit",
             "l1_local_store_miss",lst_per_thread,l_per_thread_okay);

          // CC 2.x
          if ( Event_Group* const eg = eg_find(ki,"l1_shared_bank_conflict") )
            atend_cache_report
              (eg,ki,"Shared Bank Conflict",
               NULL,
               "l1_shared_bank_conflict",sld_per_thread,s_per_thread_okay);

          ki->ci_global_load.summary_line_print(NULL); // Print headings.
          ki->ci_global_load.summary_line_print("GLd");
          ki->ci_global_ro.summary_line_print("GRO");
          ki->ci_global_nc.summary_line_print("GNC");
          ci_global_store.summary_line_print("GSt");
          ci_local_load.summary_line_print("LLd");
          ci_local_store.summary_line_print("LSt");
          ci_local_store.summary_line_print("Total");

          const double l2_hit_count_per_thread =
            ci_local_load.miss_count_per_thread +
            ci_local_store.miss_count_per_thread;

          const double l2_miss_count_per_thread =
            ki->ci_global_load.miss_count_per_thread + gst_per_thread;

          // Performance Model
          if ( true )
            {
              const double umm =
                double(ki->api_thread_count>>5) /
                ( numMultiprocessors * ki->active_warps_per_mp );
              const double time_per_thread_ns =
                double(ki->elapsed_time_ns) / ki->call_count;
              const double thread_etime_ms =
                1e-6 * time_per_thread_ns / umm;

              const double thread_etime_cyc =
                1e-3 * thread_etime_ms * clock_freq_hz;

              const double even_sched_interval =
                ki->active_warps_per_mp * 32.0 / cores_per_mp;

              // Average initiation interval based on instruction dependencies.
              // If each instruction depended on its immediate predecessor
              // value would be 24 for an NVIDIA G80 to 2012 devices.
              //
              const double avg_sched_interval = 12;

              const double nomem_sched_interval =
                max(even_sched_interval,avg_sched_interval);

              //  const double miss_latency_cyc = 500;  // Works on laptop.
              const double l2_hit_latency_cyc = 550;  // Tesla C2050.
              const double l2_miss_latency_cyc = 850;  // Tesla C2050.

              const double hideable_latency =
                ki->insn_per_thread
                * max(0.0, even_sched_interval - avg_sched_interval);

              const double mem_latency =
                l2_hit_count_per_thread * l2_hit_latency_cyc
                + l2_miss_count_per_thread * l2_miss_latency_cyc;

              const double modeled_exec_time =
                ki->insn_per_thread * avg_sched_interval
                + max(hideable_latency,mem_latency);

              const double lm =
                ( thread_etime_cyc - ki->insn_per_thread * avg_sched_interval )
                / max(1.0,l2_miss_count_per_thread+l2_hit_count_per_thread);

              printf(
                     " Data From Performance Model ----------------------------------------------\n");

              printf
                ("   Execution Time / Thd %9.3f kcyc,  Slack %.1f kcyc,  "
                 "Mem Lat  %.0f kcyc.\n",
                 1e-3 * thread_etime_cyc, 1e-3 * hideable_latency,
                 1e-3 * mem_latency);

              printf
                ("   Modeled ET / Thd     %9.3f kcyc,  "
                 "Finagled Mem Lat %.0f cyc.\n",
                 1e-3 * modeled_exec_time,
                 hideable_latency >= mem_latency ? 0 : lm );

              const double  no_mem_modeled_et =
                ki->insn_per_thread * nomem_sched_interval;

              printf
                ("   Mod. ET w/o Mem Lat  %9.3f kcyc  %.1f%%.\n",
                 1e-3 * no_mem_modeled_et,
                 100.0 * no_mem_modeled_et / thread_etime_cyc);
              printf
                ("   -- Assuming: L2 Hit %.0f cyc, Mem Hit %.0f cyc, "
                 "Sched II %.f cyc\n",
                 l2_hit_latency_cyc,
                 l2_miss_latency_cyc,
                 avg_sched_interval);
            }

        }

      if ( eg_cache_l2->have_data(ki) )
        {
          // EXPERIMENTAL
          printf("  Launches %ld,  L2 (M,H) (%lu,%lu)\n",
                 eg_cache_l2->val(ki,"call_count"),
                 eg_cache_l2->val(ki,"l2_subp0_read_sector_misses"),
                 eg_cache_l2->val(ki,"l2_subp0_read_hit_sectors"));
        }
      if ( eg_cache_l21->have_data(ki) )
        {
          // EXPERIMENTAL
          printf("  Launches %ld,  L2 (M,H) (%lu,%lu)\n",
                 eg_cache_l21->val(ki,"call_count"),
                 eg_cache_l21->val(ki,"l2_subp1_read_sector_misses"),
                 eg_cache_l21->val(ki,"l2_subp1_read_hit_sectors"));
        }

      if ( ki->val_missed_noticed.size() )
        {
          printf("\nWarning: Some items above incorrectly shown as zero.\n"
                 "         These items are based on the following CUPTI\n"
                 "         events:\n");
          for ( map<string,bool>::iterator i = ki->val_missed_noticed.begin();
                i != ki->val_missed_noticed.end(); i++ )
            printf("%s ", i->first.c_str());
          printf("\n");
        }


      if ( ki->val_missed.size() )
        {
          printf
            ("\nNote: Some performance data not collected possibly because there were\n"
             "      not enough kernel launches, %d are needed. The data would\n"
             "      be computed with the following CUPTI events:\n",
             eg_enabled_count);
          for ( map<string,bool>::iterator i = ki->val_missed.begin();
                i != ki->val_missed.end(); i++ )
            printf("%s ", i->first.c_str());
          printf("\n");
        }
    }
#undef printf




  printf
    ("\nInformation not displayed for %zd kernels comprising %.6f of "
     "sampled time.\n"
     "   See parameters NVIDIAPerf::kernels_display_soft_limit and\n"
     "   kernels_display_time_threshold.\n",
     sorted_time.size() - display_count,
     1.0 - double(total_time_show_ns)/max(int64_t(1),total_time_ns));

  printf("\nRun-Time GPU Data Notes:\n");
  printf
    (" \"HW\" indicates the maximum number of warps per MP that the device\n"
     "      supports.\n");
  printf
    (" The number of threads is based on the first launch of each kernel.\n");
  printf
    (" Global Load and Store refer to instructions whose space was not\n"
     "   determined at compile time. Usually these are global addresses\n"
     "   but they can be local or shared.\n");
  printf
    (" Warp occupancy reference: Chapter 4 (Execution Configuration Optimizations),\n"
     "   NVIDIA, \"CUDA C Best Practices Guide,\", May 2011.\n");
  printf
    (" The \"GB / s  BW Util\" columns are based on the assumption that all\n"
     "   L1 cache misses also miss the L2 cache and that each access is\n"
     "   to %d bytes of data.\n"
     " The \"(Hit,Miss)\" column is for sampled multiprocessors and launches,\n"
     "   other data in the L1 Cache Data table is appropriately scaled.\n"
     "   The number of misses for GSt in this column is scaled differently\n"
     "   than for the other rows: it is the number of global stores per\n"
     "   thread. (All global stores write through the L1 cache and so are\n"
     "   effectively a miss).\n",
     assumed_load_size);

  const bool have_ro = cc_idx == CC_35;
  const char * const summary_col_head = have_ro ? "RO G LD" : "L1 G LD";

  printf("\nKernel Summary Data and Totals\n");
  double total_et_ns = 0;
  double total_flop = 0;
  double total_data = 0;
  printf("%8s  %3s  %-7s  %-7s  %-7s  %-7s  %s\n",
         "Time", "Wps", summary_col_head, "FP",
         "Exec", "Mem BW",
         "Kernel");
  printf("%8s  %3s  %-7s  %-7s  %-7s  %-7s  %s\n",
         "/ ms", "/MP", "M Ratio", "Util",
         "Util", "Util",
         "Name");
  printf("%8s  %3s  %-7s  %-7s  %-7s  %-7s  %s\n",
         "--------", "---",
         "-------", "-------", "-------", "-------",
         "------");

  string annoying_prefix("CAKERNEL_DEVICE_");

  for ( RTI_List::iterator it = sorted.begin(); it != sorted.end(); it++ )
    {
      RTI_Kernel_Info* const ki = *it;

      string func_name(ki->func_name_demangled);
      if ( func_name.find(annoying_prefix) == 0 )
        func_name = func_name.substr(annoying_prefix.length());

      const double util = assume_dp ? ki->util_dp_pct : ki->util_sp_pct;
      const int threads_per_run = ki->api_thread_count * ki->call_count;
      const double flop_per_run =
        ki->ipt_other_fp * threads_per_run;
      const double flop_util =
        flop_per_run / ( ki->elapsed_time_ns * dev_flop_p_ns );
      total_flop += flop_per_run;
      total_et_ns += ki->elapsed_time_ns;
      const double data_per_run =
        ki->tot_miss_per_thread * assumed_load_size * threads_per_run;
      total_data += data_per_run;
      printf("%8.1f  %3d   %6.4f  %6.1f%%  %6.1f%%  %6.1f%%  %s\n",
             ki->elapsed_time_ns * 1e-6,
             ki->active_warps_per_mp,
             have_ro ? ki->ci_global_ro.miss_ratio
             : ki->ci_global_load.miss_ratio,
             100 * flop_util,
             util,
             100.0 * ki->tot_assumed_load_bw_GBps
             / device_memory_bandwidth_GBps,
             func_name.c_str());
    }

  printf
    ("Total ET  %.2f ms\n", total_et_ns * 1e-6);
  printf
    ("Total FP    %.0f ops.   Zero-Overhead %.1f GFLOP/s  (%.1f%% of potential)\n",
     total_flop,
     total_flop / total_et_ns,
     100.0 * total_flop / ( total_et_ns * dev_flop_p_ns ));
  printf
    ("Total Data  %.0f bytes.  Zero-Overhead %.1f GB/s  (%.1f%% of potential)\n",
     total_data,
     total_data / total_et_ns,
     100.0 * total_data / ( total_et_ns * device_memory_bandwidth_GBps ));
    
  printf
    ("Notes: \"Zero-Overhead\" indicates rate if there were no time\n"
     "       between kernel launches.\n"
     "       FP Ops count based on an assumed mix of %.1f%% FP insn among non-\n"
     "       memory instructions. (The number of mem insn is measured.)\n"
     "       The FP op count used for device potential and performance\n"
     "       treat MADD instructions as single FP operations.\n",
     100.0 - 100 * model_assume_int_percent_alu
     );

  printf("%s%s\n",div,div);
  return 0;
}

#endif
