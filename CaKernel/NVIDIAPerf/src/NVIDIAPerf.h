 /*@@  -*- c++ -*-
   @file      NVIDIAPerf.h
   @date      Fri Jun  1 17:49:16 2012
   @author    David Koppelman
   @desc 
   Declaration of NVIDIAPerf functions callable from other thorns.
   @enddesc 
 @@*/

#ifndef NVIDIAPERF_H
#define NVIDIAPERF_H

typedef enum
  { NPerf_Status_OK,
    NPerf_Status_Status_Unset,
    NPerf_Status_Off,
    NPerf_Status_Kernel_Not_Found,
    NPerf_Status_Metric_Not_Found,
    NPerf_Status_Insufficient_Launches,
    NPerf_Status_ENUME_SIZE }
  NPerf_Status;

int NPerf_min_launches_get(); 
void NPerf_data_reset();
double NPerf_get_val_per_block(const char *name);

bool NPerf_metric_collect(const char *name);
NPerf_Status NPerf_metric_data_status(const char* kernel_name);
double NPerf_metric_value_get
(const char* kernel_name, const char *metric_name);
double NPerf_metric_value_per_warp_get
(const char* kernel_name, const char *metric_name);
double NPerf_metric_value_per_thread_get
(const char* kernel_name, const char *metric_name);

#endif
