#!/bin/bash

################################################################################
# Prepare
################################################################################

# Set up shell
if [ "$(echo ${VERBOSE} | tr '[:upper:]' '[:lower:]')" = 'yes' ]; then
    set -x                      # Output commands
fi
set -e                          # Abort on errors

SRCDIR=$(dirname $0)

################################################################################
# Build or use existing
################################################################################

# Use cases:
#
# 1. End-user, who just wants to use the code.  Without setting any
#    options, the pre-built JAR file inside the Piraha thorn will be
#    used.  A different installation of Piraha may be used by setting
#    PIRAHA_JAR to point to the built JAR file outside of this thorn.
#
# 2. Developer, who wants Piraha to be built as part of the Cactus
#    build process.  Set PIRAHA_JAR = BUILD (note that this is not the
#    default; the default is "").  Piraha will be built from the dist
#    directory into the config directory and the resulting JAR file
#    will be used.  If there is no Java compiler, this is a fatal
#    error, and the user must unset PIRAHA_JAR.

if ! java -version >/dev/null 2>/dev/null ; then 
    echo 'BEGIN ERROR'
    echo 'You will need a Java runtime environment to use the Piraha library'
    echo 'END ERROR'
    exit 1
fi

if [ "z${PIRAHA_JAR}" = "zBUILD" ]; then
    if ! javac -version >/dev/null 2>/dev/null ; then
        echo "BEGIN ERROR"
        echo "PIRAHA_JAR has been set to BUILD, but Java compiler 'javac' cannot be found.  Leave PIRAHA_JAR unset in order to use the pre-built Piraha library, or set it to the location of an already-built Piraha JAR file."
        echo "END ERROR"
        exit 1
    fi
fi

THORN=Piraha
NAME=piraha
SRCDIR=$(dirname $0)
DIST_JAR=${SRCDIR}/dist/${NAME}.jar

echo 'BEGIN MESSAGE'
if [ "z${PIRAHA_JAR}" = z ]
then
  PIRAHA_JAR=NO_BUILD
fi

if [ "z${PIRAHA_JAR}" = zBUILD ]
then
  if [ -x edu ]
  then
    true
  else
    # Unpack source
    jar xf ${PACKAGE_DIR}/CaKernel/Piraha/src/piraha.zip
  fi  
  echo ${DIST_JAR} : edu/lsu/cct/piraha/*.java edu/lsu/cct/piraha/examples/*.java > ${NAME}.mk
  echo -e "\techo Building ${NAME}.jar" >> ${NAME}.mk
  echo -e "\tmkdir -p ../dist" >> ${NAME}.mk
  echo -e "\tjavac -Xlint:unchecked -d ../dist edu/lsu/cct/${NAME}/*.java edu/lsu/cct/piraha/examples/*.java -cp ${PIRAHA_JAR}:." >> ${NAME}.mk
  echo -e "\t(cd ../dist; jar cf ${DIST_JAR} edu)" >> ${NAME}.mk
  echo ${MAKE} -f ${NAME}.mk
  ${MAKE} -f ${NAME}.mk
fi
echo "END MESSAGE"

echo "BEGIN MAKE_DEFINITION"
echo "HAVE_PIRAHA = 1"
echo "PIRAHA_JAR = ${DIST_JAR}"
echo "END MAKE_DEFINITION"
