#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <carpet.hh>

#include <stdio.h>
#include <string.h>
#include <string>
#include <sys/file.h>
#include <sys/types.h>
#include <unistd.h>

#include <cassert>
#include <vector>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "util.hh"
#include "device.hh"

using namespace std;



pui_t *pui;

static bool chemora_device_SetDevice_called = false;
static int chemora_device_index;
static int chemora_device_lock_fd;

void ChemoraDevice_SetDevice(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_PARAMETERS;
  DECLARE_CCTK_ARGUMENTS;

  if ( use_kranc_c )
    {
      *device_process = false;
      *host_process = true;
      chemora_device_SetDevice_called = true;
      chemora_device_index = -1;
      return;
    }

  // My MPI process
  const int myproc = CCTK_MyProc(cctkGH);

  // Number of devices on this host
  int ndev;
  cudaError_t err = cudaGetDeviceCount(&ndev);
  if(err != cudaSuccess) CCTK_WARN(CCTK_WARN_ALERT, cudaGetErrorString(err));
  if (ndev == 0) {
    CCTK_WARN(CCTK_WARN_ALERT,
              "There are no GPUs available. Make sure that you activate the CUDA thorn in your parameter file!");
  }

  string cc_req(gpu_cuda_cc_requirement);
  const bool cc_requirement = cc_req != "none" && cc_req != "";
  assert( !cc_requirement || cc_req[0] == '=' && cc_req[2] == '.' );
  const int cc_major_req = cc_requirement ? cc_req[1] - '0' : 0;
  const int cc_minor_req = cc_requirement ? cc_req[3] - '0' : 0;

  // Order devices so that devices connected to a display are last.
  vector<int> devices;
  for ( int time_limit = 0; time_limit < 2; time_limit++ )
    for ( int dev = 0;  dev < ndev;  dev++ )
      {
        struct cudaDeviceProp cp;
        CUDA_SAFE_CALL( cudaGetDeviceProperties(&cp,dev) );

        if ( cp.kernelExecTimeoutEnabled != time_limit ) continue;

        if ( cc_requirement &&
             ( cp.major != cc_major_req || cp.minor != cc_minor_req ) )
          {
            CCTK_VInfo
              (CCTK_THORNSTRING,
               "Skipping GPU %d, of CC, %d.%d, require one of %d.%d.",
               dev, cp.major, cp.minor, cc_major_req, cc_minor_req );
            continue;
          }

        devices.push_back(dev);
      }

  if ( ( ndev || cc_requirement ) && devices.empty() )
    CCTK_VWarn(CCTK_WARN_ABORT,
               __LINE__, __FILE__, CCTK_THORNSTRING,
              "No suitable GPUs found. Consider relaxing setting of parameter "
              "ChemoraDevice::gpu_cuda_cc_requirement, set to %s",
               gpu_cuda_cc_requirement);

  int const myhost = CCTK_MyHost(cctkGH);
  int const mynprocs = CCTK_nProcsOnHost(cctkGH, myhost);

  if ( mynprocs == 1 )
    {
      // Assume any number of independent Chemora jobs on this node.
      // Use lock files to reserve GPUs.

      string lock_path_prefix = "/tmp/chemora-gpu-lock-";

      chemora_device_SetDevice_called = true;
      chemora_device_index = -1; // Modified below.

      for ( int tries=0; tries<20; tries++ )
        {
          if ( tries )
            {
              const int sleep_delay_s = 1;
              CCTK_VInfo
                (CCTK_THORNSTRING,
                 "Could not find an available GPU, waiting %d s and retrying.",
                 sleep_delay_s);
              sleep(sleep_delay_s);
            }

          for ( int& dev_idx: devices )
            {
              string lock_path = lock_path_prefix + to_string(dev_idx);
              const int lock_fd =
                open(lock_path.c_str(), O_CREAT | O_WRONLY,
                     S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH );
              if ( lock_fd < 0 )
                CCTK_VWarn
                  (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,
                   "Could not open GPU lock file %s: %s\n",
                   lock_path.c_str(), strerror(errno) );
              const int flock_rv = flock( lock_fd, LOCK_EX | LOCK_NB );
              const int flock_errno = errno;
              if ( !flock_rv )
                {
                  string entry = "Locked by " + to_string(getpid()) + "\n";
                  write(lock_fd,entry.data(),entry.size());
                  CCTK_VInfo
                    (CCTK_THORNSTRING,
                     "Locking CUDA GPU device %d for our use after %d tries.",
                     dev_idx, tries+1);
                  chemora_device_lock_fd = lock_fd;
                  chemora_device_index = dev_idx;
                  break;
                }
              close(lock_fd);
              if ( flock_errno == EWOULDBLOCK )
                {
                  CCTK_VInfo
                    (CCTK_THORNSTRING,
                     "CUDA GPU device %d locked by another process.",
                     dev_idx);
                  continue;
                }
              CCTK_VWarn
                (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,
                 "Could not lock GPU lock file %s: %s",
                 lock_path.c_str(), strerror(flock_errno) );
            }
          if ( chemora_device_index != -1 ) break;
        }

      *device_process = chemora_device_index != -1;
      *host_process   = not *device_process;

      return;
    }

  // Assume that all Chemora processes on this node were spawned as part
  // of a single MPI run.

  // List of MPI processes running on this host
  vector<int> myprocs(mynprocs);
  CCTK_ProcsOnHost(cctkGH, myhost, &myprocs[0], mynprocs);
  int myprocid = -1;
  for (int i=0; i<mynprocs; ++i) {
    if (myprocs[i] == myproc) myprocid = i;
  }
  assert(myprocid != -1);
  
  // pui always has a size of mynprocs
  MALLOC_SAFE_CALL(pui = (pui_t*) malloc(mynprocs * sizeof(pui_t)));
  
  CCTK_VInfo(CCTK_THORNSTRING,
             "This host has %d device(s) and %d MPI process(es)",
             ndev, mynprocs);
  
  // Assign GPUs and cores to the MPI processes. First assign one GPU
  // and one core to each MPI process, then divide the remaining cores
  // among the remaining MPI processes.
  
  // Don't know how to handle this
//  assert(not allow_multiple_process_on_gpu);
  
  CCTK_VInfo(CCTK_THORNSTRING,
             "Process/device mapping for this node:");
  for (int i=0; i<mynprocs; i++) {
    // Set up pui structure
    pui[i].host = myhost;
    pui[i].nprocs = mynprocs;
    pui[i].dev = -1;
    pui[i].proc = -1;
    
    if (myproc == myprocs[i]) {
      pui[i].proc = myproc;
    }
    
    // Does this MPI process use a GPU?
    bool const isdev = not allow_no_process_on_gpu and 
      (i<ndev or allow_multiple_process_on_gpu)
      and !(disable_gpu_on_first_process_on_node && i == 0);

    if (isdev) {
      pui[i].dev = devices[i % ndev];
      CUDA_SAFE_CALL(cudaGetDeviceProperties(&(pui[i].devprop), pui[i].dev));
    }
    CCTK_VInfo(CCTK_THORNSTRING,
               "nHost ID: %d\tProcess index on node: %d\tDevice ID: %d",
               myhost, i, pui[i].dev);
  }
  
  // Testing Device_GetDevice
  int const devid = Device_GetDevice(cctkGH);
  if (devid != -1) {
    CCTK_VInfo(CCTK_THORNSTRING,
               "Host ID: %d\tProcess ID: %d\tDevice ID: %d",
               myhost, myproc, devid);
  } else {
    CCTK_VInfo(CCTK_THORNSTRING,
               "Host ID: %d\tProcess ID: %d\tNo device, using CPU cores",
               myhost, myproc);
  }
  CCTK_VInfo(CCTK_THORNSTRING,
             "This is process #%d on this host", myprocid);
  *device_process = devid != -1;
  *host_process   = not *device_process;
  
  if (set_cpu_affinity) {
#ifdef HAVE_SCHED_GETAFFINITY
    // Set CPU affinities
    // TODO: check this
    int const num_cores_on_host = mynprocs * dist::num_threads();
    int num_available_cores = num_cores_on_host;
    if (verbose_core_assignments) {
      CCTK_VInfo(CCTK_THORNSTRING,
                 "   There are %d cores available", num_available_cores);
    }
    vector<bool> available_cores(num_cores_on_host, true);
    vector<vector<bool> > cores_for_process(mynprocs);
    // First assign cores to GPUs
    for (int i=0; i<mynprocs; i++) {
      int const dev = pui[i].dev;
      if (dev != -1) {
        // Running on a GPU -- need a nearby core
        assert(dev>=0 and dev<16);
        int core = gpu_core[dev];
        if (core == -1) {
          // Find a free core
          for (int c=0; c<num_cores_on_host; ++c) {
            if (available_cores[c]) {
              core = c;
              break;
            }
          }
        }
        assert(core != -1);
        if (verbose_core_assignments) {
          CCTK_VInfo(CCTK_THORNSTRING,
                     "   Process %d [GPU]: starting with core %d",
                     myproc, core);
        }
        cores_for_process.at(i).resize(num_cores_on_host, false);
        for (int c=core; c<core+gpu_num_cores[dev]; ++c) {
          if (c>=num_cores_on_host) break;
          if (verbose_core_assignments) {
            CCTK_VInfo(CCTK_THORNSTRING,
                       "   Process %d [GPU]: using core %d",
                       myproc, c);
          }
          cores_for_process.at(i).at(c) = true;
          if (not oversubscribe_gpu_cores) {
            available_cores.at(c) = false;
            --num_available_cores;
          }
        }
      }
    }
    if (verbose_core_assignments) {
      CCTK_VInfo(CCTK_THORNSTRING,
                 "   There are %d cores left", num_available_cores);
    }
    // Then assign the left-over cores to CPUs
    int const num_cpu_procs = mynprocs - ndev >= 0 ? mynprocs - ndev : 0;
    assert(num_available_cores >= num_cpu_procs);
    int const num_cores_per_proc =
      num_cpu_procs == 0 ? 0 : num_cpu_procs / num_available_cores;
    for (int i=0; i<mynprocs; i++) {
      int const dev = pui[i].dev;
      if (dev == -1) {
        // Running on a CPU -- need some cores
        cores_for_process.at(i).resize(num_cores_on_host, false);
        int num_cores = 0;
        int proc_numa_node = -1;
        // Find free cores
        for (int c=0; c<num_cores_on_host; ++c) {
          if (available_cores.at(c)) {
            int const numa_node = c / cores_in_numa_node;
            if (proc_numa_node == -1) {
              proc_numa_node = numa_node;
              if (verbose_core_assignments) {
                CCTK_VInfo(CCTK_THORNSTRING,
                           "   Process %d [CPU]: using NUMA node %d",
                           myproc, numa_node);
              }
            }
            // Only choose this core if it is on the same NUMA node
            if (numa_node == proc_numa_node) {
              if (verbose_core_assignments) {
                CCTK_VInfo(CCTK_THORNSTRING,
                           "   Process %d [CPU]: using core %d",
                           myproc, c);
              }
              cores_for_process.at(i).at(c) = true;
              available_cores.at(c) = false;
              --num_available_cores;
              ++num_cores;
              if (num_cores == num_cores_per_proc) break;
            }
          }
        }
        // Ensure this process has cores
        assert(num_cores>0);
      }
    }
    // Set CPU affinity
    {
      // Count available cores
      int num_cores = 0;
      cout << "Process " << myproc << ": running on cores";
      for (int c=0; c<num_cores_on_host; ++c) {
        if (cores_for_process.at(myprocid).at(c)) {
          cout << " " << c;
          ++num_cores;
        }
      }
      cout << " [" << num_cores << " cores ]\n";
      // Set number of OpenMP threads
#ifdef _OPENMP
      omp_set_num_threads(num_cores);
#endif
      // Assign cores to threads
#pragma omp parallel
      {
        cpu_set_t cpumask;
        CPU_ZERO(&cpumask);
        for (int c=0; c<num_cores_on_host; ++c) {
          if (cores_for_process.at(myprocid).at(c)) {
            CPU_SET(c, &cpumask);
          }
        }
        int const ierr = sched_setaffinity(0, sizeof(cpumask), &cpumask);
        assert(not ierr);
      }
    }
#else
    CCTK_WARN(CCTK_WARN_ALERT,
              "Setting the CPU affinity is not supported on this system");
#endif
  }
}



// @return  -1     is not device
// something else: device id
int ChemoraDevice_GetDevice(const CCTK_POINTER_TO_CONST _cctkGH)
{
  DECLARE_CCTK_PARAMETERS;

  const cGH *  cctkGH = (cGH *) _cctkGH;
  const int myhost = CCTK_MyHost(cctkGH);
  const int myproc = CCTK_MyProc(cctkGH);
  const int mynprocs = CCTK_nProcsOnHost(cctkGH, myhost);

  if ( use_kranc_c ) return -1;

  if ( mynprocs == 1 )
    return chemora_device_SetDevice_called ? chemora_device_index : -1;

  extern pui_t *pui;

  for (int i = 0; i < mynprocs; i++)
  {
    if ((myproc == pui[i].proc) && (pui[i].dev != -1))
      return pui[i].dev;
  }
  return -1;
}


extern "C" int
ChemoraDevice_ReleaseDevice()
{
  if ( !chemora_device_SetDevice_called || chemora_device_index == -1 )
    return 1;
  CCTK_VInfo
    (CCTK_THORNSTRING,
     "Releasing lock on CUDA GPU device %d, which was for our use.\n",
     chemora_device_index);
  string fin_msg = "Process " + to_string(getpid()) + " releasing lock.\n";
  write(chemora_device_lock_fd,fin_msg.data(),fin_msg.size());
  flock(chemora_device_lock_fd, LOCK_UN | LOCK_NB);
  close(chemora_device_lock_fd);
  return 1;
}
