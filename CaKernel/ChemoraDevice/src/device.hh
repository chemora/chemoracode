#pragma once

#include <cctk.h>

//#if HAVE_CUDA == 1
#include <cuda.h>
#include <cuda_runtime.h>
//#endif

extern "C"
{
  /* processing unit information */
  typedef struct _pui_t
  {
      /* my unique host id */
      CCTK_INT host;
      /* my dev id */
      CCTK_INT dev;
      /* my mpi rank or process id */
      CCTK_INT proc;
      /* number of processes on my host */
      CCTK_INT nprocs;
      /* my dev property */
      struct cudaDeviceProp devprop;
  } pui_t;

  int ChemoraDevice_WhichDevice(const CCTK_POINTER_TO_CONST cctkGH);
  /* array of processing units */
  extern pui_t *pui;
}
