#pragma once

#include "cctk.h"
#include <stdio.h>
#include <stdlib.h>

#ifdef CCTK_MPI
#include "mpi.h"
#endif

//#if HAVE_CUDA == 1
#include <cuda.h>
#include <cuda_runtime.h>

extern "C"
{

#define CUDA_SAFE_CALL( call )                                             \
  {                                                                        \
    cudaError Err = call;                                                  \
    if(Err != cudaSuccess)                                                 \
    {                                                                      \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,   \
            "CUDA error: %s", cudaGetErrorString( Err));                   \
      exit(EXIT_FAILURE);                                                  \
    }                                                                      \
  }

#define CUDA_CHECK_LAST_CALL(msg) __cutilCheckMsg(msg, __FILE__, __LINE__)

  inline void __cutilCheckMsg(const char *errorMessage, const char *file,
      const int line)
  {
    cudaError_t err = cudaGetLastError();
    if (cudaSuccess != err)
    {
      fprintf(stderr, "%s(%i) : cutilCheckMsg() CUTIL CUDA error : %s : %s.\n",
          file, line, errorMessage, cudaGetErrorString(err));
      exit(-1);
    }
    err = cudaDeviceSynchronize();
    if (cudaSuccess != err)
    {
      fprintf(stderr,
          "%s(%i) : cutilCheckMsg cudaDeviceSynchronize error: %s : %s.\n", file,
          line, errorMessage, cudaGetErrorString(err));
      exit(-1);
    }
  }
}
//#endif  /* HAVE_CUDA == 1 */

/* Malloc Util */

#define MALLOC_SAFE_CALL( call )                                           \
  {                                                                        \
    void *Mrr = call;                                                      \
    if(Mrr == NULL)                                                        \
    {                                                                      \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,   \
            "Malloc error: %s", "failed to allocate memory !");            \
      exit(-1);                                                            \
    }                                                                      \
  }

/* MPI Util */

#ifdef CCTK_MPI

#define MPI_SAFE_CALL( call )                                              \
  {                                                                        \
    int err = call;                                                        \
    if (err != MPI_SUCCESS)                                                \
    {                                                                      \
      char mpi_error_string[MPI_MAX_ERROR_STRING+1];                       \
      int resultlen;                                                       \
      MPI_Error_string (err, mpi_error_string, &resultlen);                \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,   \
            "MPI error: %s", mpi_error_string);                            \
      exit(-1);                                                            \
     }                                                                     \
   }
#endif
