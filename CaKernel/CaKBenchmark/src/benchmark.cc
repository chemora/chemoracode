 /*@@
   @file      benchmark.cc
   @date      Sun Jan 12 17:50:00 2014
   @author    David M. Koppelman
   @desc 

   Run a simple 7-point stencil code, and perform a correctness test.

   The correctness test ensures that grid access functions, such
   as I3D(a1,1,0,-1), return the correct element. If not, the program
   will end with a fatal error.
      
   @enddesc 
 @@*/

#include <stdio.h>
#include <assert.h>

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include "benchmark.h"

static void cakb_verify(int invocation_position, CCTK_ARGUMENTS);

extern "C" int
CaKB_Startup()
{
  DECLARE_CCTK_PARAMETERS;
  CCTK_VInfo(CCTK_THORNSTRING,"CaKBenchmark Starting\n");
  return 0;
}

extern "C" void
CaKB_Init_Data(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const int j_shift = IDX_FIELD_WIDTH;
  const int k_shift = j_shift << 1;
  const CCTK_REAL frac_shift = CCTK_REAL(1) / ( 1 << ( j_shift * 3 ) );

  for ( int k=0; k<cctk_lsh[2]; k++ )
    for ( int j=0; j<cctk_lsh[1]; j++ )
      for ( int i=0; i<cctk_lsh[0]; i++ )
        {
          const int idx = CCTK_GFINDEX3D(cctkGH,i,j,k);
          const int encode = ( k << k_shift ) + ( j << j_shift ) + i;
          a0[idx] = encode * frac_shift;
        }

  *my_scalar = 0.5;
}

extern "C" void
CaKB_Verify_1(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  // First call of verify in a time step.
  cakb_verify(1,CCTK_PASS_CTOC);
}

extern "C" void
CaKB_Verify_2(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  // Second call of verify in a time step.
  cakb_verify(2,CCTK_PASS_CTOC);
}

static void
cakb_verify(int invocation_position, CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const int nprocs = CCTK_nProcs(cctkGH);
  const int myproc = CCTK_MyProc(cctkGH);

  const int DIMSIZE = 6;
  CCTK_INT bndsize[DIMSIZE];
  CCTK_INT is_ghostbnd[DIMSIZE];
  CCTK_INT is_symbnd[DIMSIZE];
  CCTK_INT is_physbnd[DIMSIZE];

  const int rv = GetBoundarySizesAndTypes
    (cctkGH, DIMSIZE, bndsize, is_ghostbnd, is_symbnd, is_physbnd);
  assert( !rv );

  const int bnd_up_i = cctk_lsh[0] - bndsize[1];
  const int bnd_up_j = cctk_lsh[1] - bndsize[3];
  const int bnd_up_k = cctk_lsh[2] - bndsize[5];

  const int j_shift = IDX_FIELD_WIDTH;
  const int k_shift = j_shift << 1;
  const CCTK_REAL frac_shift = ( 1 << ( j_shift * 3 ) );
  const CCTK_REAL err_tolerance = 
    ( sizeof(CCTK_REAL) == 8 ? 1 : 10000 ) / frac_shift;
  const int iter_expect =
    ( cctk_iteration + invocation_position - 1 ) * a_real_parameter;

  int num_correct = 0;
  int num_checked = 0;

  if ( sizeof(CCTK_REAL) < 8 && iter_expect > 1 ) return;
  int err_iter_cnt = 0;
  int err_pos_cnt = 0;
  int err_res_cnt = 0;

  for ( int k=0; k<cctk_lsh[2]; k++ )
    for ( int j=0; j<cctk_lsh[1]; j++ )
      for ( int i=0; i<cctk_lsh[0]; i++ )
        {
          const bool interior =
               i >= bndsize[0] && i < bnd_up_i
            && j >= bndsize[2] && j < bnd_up_j
            && k >= bndsize[4] && k < bnd_up_k;
            
          const bool phys_boundary =
               is_physbnd[0] && i <  bndsize[0]
            || is_physbnd[1] && i >= bnd_up_i
            || is_physbnd[2] && j <  bndsize[2]
            || is_physbnd[3] && j >= bnd_up_j
            || is_physbnd[4] && k <  bndsize[4]
            || is_physbnd[5] && k >= bnd_up_k;

          assert( nprocs || interior ^ phys_boundary );

          if ( !interior && !phys_boundary ) continue;

          const int idx = CCTK_GFINDEX3D(cctkGH,i,j,k);
          const int encode =
            ( k << k_shift ) + ( j << j_shift ) + i;
          const double val_rd = a0[idx];
          const double val_wr = a1[idx];
          const double result = b[idx];

#define I3D(v,di,dj,dk) v[CCTK_GFINDEX3D(cctkGH,i+di,j+dj,k+dk)]
          const double result_check = phys_boundary ? 1 : val_rd + *my_scalar
            + CCTK_REAL(.1) * I3D(a0,-1,0,0) + CCTK_REAL(.2) * I3D(a0,1,0,0)
            + CCTK_REAL(.3) * I3D(a0,0,-1,0) + CCTK_REAL(.4) * I3D(a0,0,1,0)
            + CCTK_REAL(.5) * I3D(a0,0,0,-1) + CCTK_REAL(.6) * I3D(a0,0,0,1);
#undef I3D

          const double result_err = fabs(result_check-result);
          if ( result_err > err_tolerance )
            {
              err_res_cnt++;
              if ( err_res_cnt < 5 )
                CCTK_VWarn
                  (CCTK_WARN_ALERT, __LINE__,__FILE__, CCTK_THORNSTRING,
                   "Wrong value at %d,%d,%d. "
                   " %.9f != %.9f (correct)\n",
                   i,j,k,result,result_check);
            }

          double val_int = 0;
          const int en_val = modf(val_wr,&val_int) * frac_shift;
          const int iter_found = val_int;
          const bool correct_pos_marker = en_val == encode;

          if ( iter_expect != iter_found )
            {
              err_iter_cnt++;
              if ( err_iter_cnt < 5 )
                CCTK_VWarn
                  (CCTK_WARN_ALERT, __LINE__,__FILE__, CCTK_THORNSTRING,
                   "Wrong count at %d,%d,%d. "
                   "Count: %d (found) != %d (correct) (%d checked)\n",
                   i,j,k,iter_found,iter_expect,num_checked);
            }

          if ( sizeof(CCTK_REAL) == 8 && !correct_pos_marker )
            {
              err_pos_cnt++;
              if ( err_pos_cnt < 10 )
                {
                  const int ei = en_val & ( ( 1 << j_shift ) - 1 );
                  const int ej =
                    ( en_val >> j_shift ) & ( ( 1 << j_shift ) - 1 );
                  const int ek = en_val >> k_shift;
                  CCTK_VWarn
                    (CCTK_WARN_ALERT, __LINE__,__FILE__, CCTK_THORNSTRING,
                     "Wrong location at %d,%d,%d -- found %d,%d,%d.  "
                     "Count: %d (found) != %d (correct) (%d checked)\n",
                     i,j,k,ei,ej,ek,iter_found,iter_expect,num_checked);
                }
            }

          num_checked++;
        }
  if ( !( err_iter_cnt == 0 && err_pos_cnt == 0 && err_res_cnt == 0 ) )
    CCTK_VWarn                                                                
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Proc %d Iter %d, # wrong count, %d (%.1f%%); "
       "# wrong loc %d (%.1f%%); # wrong val %d (%.1f%%)\n",
       myproc,
       iter_expect, err_iter_cnt,
       100.0 * err_iter_cnt/double(num_checked),
       err_pos_cnt,
       100.0 * err_pos_cnt/double(num_checked),
       err_res_cnt,
       100.0 * err_res_cnt/double(num_checked));

  *my_scalar += 0.5;

}

extern "C" void
CaKB_Finish(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_VInfo(CCTK_THORNSTRING,"CaKBenchmark Finishing\n");
}
