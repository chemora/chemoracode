#! /bin/bash

################################################################################
# Prepare
################################################################################

# Set up shell
if [ "$(echo ${VERBOSE} | tr '[:upper:]' '[:lower:]')" = 'yes' ]; then
    set -x                      # Output commands
fi
set -e                          # Abort on errors

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
${SCRIPT_DIR}/config-openacc


################################################################################
# CUDA (Compute Unified Device Architecture)
################################################################################

if [ -z "${CUDA_DIR}" ]; then
    echo "BEGIN MESSAGE"
    echo "CUDA selected, but CUDA_DIR not set. Checking some places..."
    echo "END MESSAGE"
    
    if which nvcc >/dev/null 2>/dev/null; then
        cucc_path=$(dirname `which nvcc`)/..
    fi
    FILES="bin/nvcc"
    DIRS="$cucc_path /usr/local/cuda /opt/local/cuda /usr/cuda /opt/cuda"
    for file in $FILES; do
        for dir in $DIRS; do
            if test -r "$dir/$file"; then
                CUDA_DIR="$dir"
                break
            fi
        done
    done
    
    if [ -z "$CUDA_DIR" ]; then
        echo "BEGIN MESSAGE"
        echo "CUDA not found"
        echo "END MESSAGE"
    else
        echo "BEGIN MESSAGE"
        echo "Found CUDA in ${CUDA_DIR}"
        echo "END MESSAGE"
    fi
fi



# Set options

cuda_lib_dirs=""
for dir in /lib64 ${CUDA_DIR}/lib64 ${CUDA_DIR}/lib; do
    if [ -d $dir ]; then
        cuda_lib_dirs="$cuda_lib_dirs $dir $dir/stubs"
    fi
done

: ${CUDA_INC_DIRS="${CUDA_DIR}/include"}
: ${CUDA_LIB_DIRS="$cuda_lib_dirs"}
: ${CUDA_LIBS="cudart cuda"}

echo "BEGIN MAKE_DEFINITION"
echo "HAVE_CUDA     = 1"
echo "CUDA_DIR      = ${CUDA_DIR}"
echo "CUDA_INC_DIRS = ${CUDA_INC_DIRS}"
echo "CUDA_LIB_DIRS = ${CUDA_LIB_DIRS}"
echo "CUDA_LIBS     = ${CUDA_LIBS}"
echo "END MAKE_DEFINITION"

echo 'INCLUDE_DIRECTORY $(CUDA_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(CUDA_LIB_DIRS)'
echo 'LIBRARY           $(CUDA_LIBS)'

################################################################################
# CUCC (CUDA compiler)
################################################################################

if [ -x $CUDA_DIR/bin/nvcc ]; then
    : ${CUCC:="$CUDA_DIR/bin/nvcc"}
fi

if which $CUCC >/dev/null 2>/dev/null; then
    cucc_path=`which $CUCC 2>/dev/null`
fi

if test -z $cucc_path || test ! -x $cucc_path ; then
    echo "BEGIN ERROR"
    echo "Could not find nvcc (CUDA compiler driver) with $CUCC"
    echo "END ERROR"
    exit 1
fi

if $cucc_path --version | grep NVIDIA > /dev/null; then
 :
else
    echo "BEGIN ERROR"
    echo "Path $cucc_path not responding like nvcc (CUDA compiler driver)."
    echo "END ERROR"
    exit 1
fi

: ${CUCC_EXTRA_FLAGS=""}
: ${CUCC_EXTRA_DEBUG_FLAGS=""}
: ${CUCC_EXTRA_OPTIMISE_FLAGS=""}

# Can we detect these automatically?
: ${CUDA_ARCH="sm_35"}
: ${CUDA_BITS="64"}

# Check that we are not using double precision with a compute
# capability which does not support it.  nvcc reports a warning and
# generates useless code if we allow this to happen.

if [ -z "$REAL_PRECISION" ]; then
    REAL_PRECISION=8      # This is the default chosen in configure.in
fi

for arch in sm_10 sm_11 sm_12 compute_10 compute_11 compute_12; do
    if [ $CUDA_ARCH = $arch ]; then
        if [ $REAL_PRECISION -gt 4 ]; then
            echo "BEGIN ERROR"
            echo "CUDA_ARCH is set to $CUDA_ARCH which only supports REAL_PRECISION <= 4, but REAL_PRECISION is set to $REAL_PRECISION"
            echo "END ERROR"
            exit 1
        fi
    fi
done

if [ ! -z "$CUCC" ]; then
    echo "BEGIN MAKE_DEFINITION"
    echo "CUCC                = ${CUCC}"
    echo "CUCCFLAGS           = -m ${CUDA_BITS} -arch ${CUDA_ARCH} -Xcompiler -march=native -g  -Xcompiler -ansi ${CUCC_EXTRA_FLAGS}"
    echo "CUCC_DEBUG_FLAGS    = ${CUCC_EXTRA_DEBUG_FLAGS}"
    echo "CUCC_OPTIMISE_FLAGS = -O3 ${CUCC_EXTRA_OPTIMISE_FLAGS}"
    echo "CUCC_WARN_FLAGS     = -Xcompiler -Wall ${CUCC_EXTRA_WARN_FLAGS}"
    echo "END MAKE_DEFINITION"
fi

################################################################################
# CUPTI (CUDA Performance Tool Interface)
################################################################################

if [ ! -z "$CUPTI_DIR" -a ! -r "$CUPTI_DIR" ]; then
    echo "BEGIN ERROR"
    echo "CUPTI_DIR set to $CUPTI_DIR but this directory is at least unreadable"
    echo "END ERROR"
    exit 1
fi

for dir in $CUPTI_DIR $CUDA_DIR/extras/CUPTI $CUDA_DIR; do
    if [ -f "$dir/include/cupti.h" ]; then
        : ${CUPTI_INC_DIRS="${dir}/include"}
        : ${CUPTI_LIB_DIRS="${dir}/lib64 ${dir}/lib"}
        break
    fi
done

if [ ! -z "$CUPTI_INC_DIRS" -a ! -z "$CUPTI_LIB_DIRS" ]; then
    HAVE_CUPTI=1
else
    HAVE_CUPTI=0
fi    

: ${CUPTI_LIBS="cupti"}

################################################################################
# BFD  GNU Binary Tools API (Used for demangling)
################################################################################

# Don't try too hard to find bfd.h. If it's missing then kernel names will
# be mangled (combined with characters indicating argument and return types).
#
if [ -r "/usr/include/bfd.h" ]; then
echo "BEGIN MAKE_DEFINITION"
echo CXXFLAGS += -DHAVE_BFD
echo "END MAKE_DEFINITION"
echo LIBRARY -lbfd
fi

################################################################################
# BZIP2
################################################################################

if [ -r "/usr/include/bzlib.h" ]; then
echo "BEGIN MAKE_DEFINITION"
echo CXXFLAGS += -DHAVE_BZLIB
echo "END MAKE_DEFINITION"
echo LIBRARY -lbz2
fi

# Set options

echo "BEGIN MAKE_DEFINITION"
echo "HAVE_CUPTI     = $HAVE_CUPTI"
if [ $HAVE_CUPTI -eq 1 ]; then
    echo "CUPTI_INC_DIRS = ${CUPTI_INC_DIRS}"
    echo "CUPTI_LIB_DIRS = ${CUPTI_LIB_DIRS}"
    echo "CUPTI_LIBS     = ${CUPTI_LIBS}"
    echo "CXXFLAGS       += -DHAVE_CUPTI"
fi
echo "END MAKE_DEFINITION"

echo 'INCLUDE_DIRECTORY $(CUPTI_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(CUPTI_LIB_DIRS)'
echo 'LIBRARY           $(CUPTI_LIBS)'
