 /*@@  -*- c++ -*-
   @file      chemora_cg_kranc_startup_h
   @date
   @author    David Koppelman
   @desc
   Header material needed by Kranc-inserted code in Startup.cc, used
   to provide intermediate format computation kernel code to the Chemora
   code generation system.
   @enddesc
 @@*/

#ifndef chemora_cg_kranc_startup_h
#define chemora_cg_kranc_startup_h
#include <cctk_Parameters.h>
#include "chemora_cg.h"

#define CHEMORA_CG_KRANC_INIT_THORN_START(thorn_name)                         \
 DECLARE_CCTK_PARAMETERS;                                                     \
 Chemora_CG* const chemora = Chemora_CG_get();                                \
 Chemora_CG_Thorn* const ct = chemora->thorn_new(thorn_name);                 \
 Chemora_CG_Calc *c = NULL;                                                   \
 if ( use_kranc_c ) return;

#define CHEMORA_CG_KRANC_INIT_THORN_KERNEL(kname) \
 c = ct->calc_new(kname);

#define set_parameter c->set_parameter

#define assign_scalar c->assign_scalar
#define assign_from_expr c->assign_from_expr
#define assign_from_func c->assign_from_func
#define assign_from_ifelse c->assign_from_ifelse
#define gf_store c->gf_store
#define assign_gf_axes c->assign_gf_axes
#define assign_from_gf_load c->assign_from_gf_load
#define assign_from_offset_load c->assign_from_offset_load
#define assign_from_gf_load_switch c->assign_from_gf_load_switch

#define CHEMORA_CG_KRANC_INIT_THORN_END

#endif
