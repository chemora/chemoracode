 /*@@
   @file      CaCUDALib_driver_support.cc
   @date      $Date:$
   @author    David Koppelman
   @desc
   Support routines for using the CUDA driver interface, including
   support for dynamic compilation.
   @enddesc
   @version $Revision:$
   @id $Id:$
 @@*/

#include <cctk.h>
#include <cctk_Parameters.h>
#include <cctk_Arguments.h>
#include <iostream>
#include <fstream>
#include <assert.h>
#ifdef HAVE_BZLIB
#include <bzlib.h>
#endif
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>

#include <list>
#include <algorithm>
#include <malloc.h>

#include "pstring.h"
#include "putil.h"
#include "CaCUDALib_driver_support.h"
#include "microbench.cuh"
#include <time.h>
#include <dlfcn.h>
#ifdef HAVE_OPENACC
#include <openacc.h>
#endif

CaCUDALib_CUDA_Driver_Manager *cacudalib_cuda_driver_manager = NULL;

static void* (*prev_malloc_hook)(const size_t amt, const void *caller);
static void* (*prev_memalign_hook)
  (const size_t alignment, const size_t amt, const void *caller);

static void*
malloc_addr_check(void *base, size_t amt)
{
  static int found_count = 0;
  size_t rvi = size_t(base);
  size_t target = 0x7fffdeebb010;
  if ( target >= rvi && target < rvi + amt )
    found_count++;
  return base;
}

static void *my_malloc_hook(const size_t amt, const void *caller)
{
  return malloc_addr_check( prev_malloc_hook(amt,caller), amt );
}

static void *my_memalign_hook
(const size_t alignment, const size_t amt, const void *caller)
{
  return malloc_addr_check( prev_memalign_hook(alignment,amt,caller), amt );
}


CaCUDALib_CUDA_Driver_Manager::CaCUDALib_CUDA_Driver_Manager()
{
  init_called = false;
  mi = NULL;
  trace_gf_inited = false;
  trace_gf_gf_buffer = NULL;
  devidx = -1;
  cu_context = NULL;
  compile_cucc_target = NULL;
  compile_cucc_options = "";
  param_gf_offset_tolerant = false;
  param_gf_offset_var_min = 1000000;
  param_l1_restrict_gf_min = 50;
  param_warps_max = 48;
  param_warps_l1_limited_max = 8;
  compile_nvcc_path = NULL;
  nan_scan_buffer = NULL;
  nan_scan_buffer_size = 0;
  bzero(device_capabilities,sizeof(device_capabilities));

  const bool hook_malloc = false;
#undef MALLOC_HOOK_USE

  if ( hook_malloc )
    {
#ifndef MALLOC_HOOK_USE
      CCTK_VWarn
        (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "Did you set hook_malloc to true? Then please compile with\n"
         "MALLOC_HOOK_USE defined in file CaCUDALib_driver_support.cc.");
      prev_malloc_hook = my_malloc_hook;     // Just to avoid warnings.
      prev_memalign_hook = my_memalign_hook; // Just to avoid warnings.
#else
      /// WARNING: realloc not yet hooked.
      prev_malloc_hook = __malloc_hook;
      __malloc_hook = my_malloc_hook;

      prev_memalign_hook = __memalign_hook;
      __memalign_hook = my_memalign_hook;
#endif
    }
}

CaCUDALib_CUDA_Driver_Manager::~CaCUDALib_CUDA_Driver_Manager()
{
  if ( gpu_info.device_data_collected )
    assert( !memcmp(&gpu_info,&gpu_info_cpy,sizeof(gpu_info)) );
  if ( trace_gf_gf_buffer ) delete [] trace_gf_gf_buffer;
}

void
CaCUDALib_CUDA_Driver_Manager::msg_info(const char *fmt, ...) 
{
  DECLARE_CCTK_PARAMETERS;
  if ( myproc != 0 ) return;
  pString msg;
  va_list ap;
  va_start(ap,fmt);
  msg.vsprintf(fmt,ap);
  va_end(ap);
  CCTK_INFO(msg.s);
}

void
CaCUDALib_CUDA_Driver_Manager::msg_tune(const char *fmt, ...) 
{
  DECLARE_CCTK_PARAMETERS;
  if ( myproc != 0 || !dynamic_compile_show_tuning_decisions ) return;
  pString msg;
  va_list ap;
  va_start(ap,fmt);
  msg.vsprintf(fmt,ap);
  va_end(ap);
  CCTK_INFO(msg.s);
}

extern "C"
void CaCUDALib_DM_InitDev(CCTK_ARGUMENTS)
{
  CaCUDALib_InitDevice(CCTK_PASS_CTOC);
}

void
CaCUDALib_CUDA_Driver_Manager::init_device(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (init_called)
    return;

  init_called = true;
  myproc = CCTK_MyProc(cctkGH);
  param_gf_offset_var_min = dynamic_compile_gf_offset_min_var;
  param_gf_offset_tolerant = I_dont_mind_wrong_answers;
  param_l1_restrict_gf_min = dynamic_compile_l1_restrict_gf_min;
  param_warps_max = dynamic_compile_warps_max;
  param_warps_l1_limited_max = dynamic_compile_warps_l1_limited_max;
  allow_recompile_anytime = dynamic_compile_recompile == string("on");
  allow_recompile_init =
    allow_recompile_anytime
    || dynamic_compile_recompile == string("initialization");

  tile_size_overrides_parse(dynamic_compile_tile_size_overrides);

  // Accelerator will choose the GPU for us, but will not select or
  // initialize it.
  devidx = Device_GetDevice(cctkGH);

  if ( devidx == - 1 )
    {
      CCTK_VWarn
        (CCTK_WARN_ALERT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "Attempt to initialize dynamic compilation without a\n"
         "GPU assigned.");
      return;
    }

  CE( cuInit(0) );
  CE( cuDeviceGet(&cu_device, devidx) );

#ifdef HAVE_OPENACC
  // Beware: acc_init will fail if it calls CUDA stub routines. See Ticket 244.
  acc_init(acc_device_nvidia);
  acc_set_device_num(devidx,acc_device_nvidia);
  CE( cuCtxGetCurrent(&cu_context) );
#else
  CE( cuCtxCreate(&cu_context, 0, cu_device) );
#endif

  assert( cu_context );
  CUdevice dev_now;
  CE( cuCtxGetDevice(&dev_now) );
  assert( cu_device == dev_now );


  gpu_info_init(devidx);
  gpu_info_set_with_microbenchmark(devidx);
  gpu_info_freeze();

  int cc_major, cc_minor;
  CE( cuDeviceGetAttribute
      (&cc_major, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, cu_device) );
  CE( cuDeviceGetAttribute
      (&cc_minor, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR, cu_device) );

  const char* paths[] = 
    { dynamic_compile_nvcc_path, STR(DC_CUCC),
      "nvcc", "/usr/local/cuda/bin/nvcc" };
  const int path_count = sizeof(paths)/sizeof(paths[0]);

  compile_nvcc_path = NULL;

  for ( int i=0; i<path_count; i++ )
    {
      const char* const p = paths[i];
      if ( !p || !strlen(p) ) continue;
      pStringF cmd("%s --version",p);
      const int rv = system(cmd.s);
      if ( rv == -1 || WEXITSTATUS(rv) != 0 ) continue;
      compile_nvcc_path = p;
      break;
    }

  if ( !compile_nvcc_path )
    {
      pString nvcc_msg;
      for ( int i=0; i<path_count; i++ ) nvcc_msg.sprintf("%s:",paths[i]);
      CCTK_VWarn
        (CCTK_WARN_ALERT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "Not performing dynamic compilation because could not find nvcc "
         "(CUDA compiler), falling back on statically compiled kernels.\n Search path for nvcc: %s\n"
         "Try setting parameter dynamic_compile_nvcc_path.\n",
         nvcc_msg.s);
    }

  if ( compile_nvcc_path )
    {
      const char *l = NULL;
      for ( const char *p = compile_nvcc_path; *p; p++ ) if ( *p == '/' ) l = p;
      if ( l )
        compile_nvdisasm_path.assign(compile_nvcc_path,l-compile_nvcc_path+1);
      else
        compile_nvdisasm_path = "";
      compile_nvdisasm_path += "nvdisasm";
    }

  pStringF cucc_target_default
    ("--gpu-architecture=sm_%d%d", cc_major, cc_minor);

  if ( dynamic_compile_cucc_target
       && strlen(dynamic_compile_cucc_target) )
    compile_cucc_target = dynamic_compile_cucc_target;
  else
    compile_cucc_target = cucc_target_default.dup();

  if ( dynamic_compile_cucc_options
       && strlen(dynamic_compile_cucc_options) )
    compile_cucc_options = dynamic_compile_cucc_options;
 
  CCTK_VInfo
    (CCTK_THORNSTRING,
     "Device %d successfully initialized", devidx);

  report_total_et = 0;

  // Only used by statically compiled code.
  CE( cuEventCreate( &sc_launch_start_ev, CU_EVENT_BLOCKING_SYNC) );
  CE( cuEventCreate( &sc_launch_end_ev, CU_EVENT_BLOCKING_SYNC) );
}

void
CaCUDALib_CUDA_Driver_Manager::gpu_info_init(int dev)
{
  CaCUDALib_GPU_Info& gi = gpu_info;

  gi.device_data_collected = true;

  CE( cuDriverGetVersion(&gi.cuda_driver_version) );

  CE( cuDeviceGetName(gi.gpu_name,gi.gpu_name_size,dev) );
  CE( cuDeviceGetAttribute
      (&gi.cc_major, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, dev) );
  CE( cuDeviceGetAttribute
      (&gi.cc_minor, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR, dev) );

  gi.cc_idx =
    gi.cc_major == 1 ? CC_1x :
    gi.cc_major == 2 ?
    ( gi.cc_minor == 0 ? CC_20 : 
      gi.cc_minor == 1 ? CC_21 : CC_Unknown ) :
    gi.cc_major == 3 ?    
    ( gi.cc_minor == 0 ? CC_30 : 
      gi.cc_minor == 5 ? CC_35 : CC_Unknown ) :
    gi.cc_major == 5 ?
    ( gi.cc_minor == 0 ? CC_50 :
      gi.cc_minor == 2 ? CC_52 :
      gi.cc_minor == 3 ? CC_53 : CC_Unknown ) :
    gi.cc_major == 6 ?
    ( gi.cc_minor == 0 ? CC_60 :
      gi.cc_minor == 1 ? CC_61 :
      gi.cc_minor == 2 ? CC_62 : CC_Unknown ) :
    CC_Unknown;

  gi.is_geforce = strncmp("GeForce",gi.gpu_name,7) == 0;

  CE( cuDeviceGetAttribute
      (&gi.num_multiprocessors, CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT,
       dev) );

  CE( cuDeviceGetAttribute
      ( &gi.max_registers_per_bl,
        CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK, dev) );

  CE( cuDeviceGetAttribute
      ( &gi.max_registers_per_mp,
        CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_MULTIPROCESSOR, dev) );

  gi.max_registers_per_th = gi.cc_idx <= CC_30 ? 63 : 255;

  CE( cuDeviceGetAttribute
      (&gi.max_threads_per_mp, 
       CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR, dev) );

  CE( cuDeviceGetAttribute
      (&gi.max_threads_per_bl, 
       CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK, dev) ); 

  gi.max_blocks_per_mp =
    gi.cc_major == 2 ? 8 :
    gi.cc_major == 3 ? 16 :
    gi.cc_major == 5 ? 32 :
    gi.cc_major == 6 ? 32 : 0;

  CE( cuDeviceGetAttribute
      ( &gi.constant_sz_bytes,
        CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY, dev ) );

  CE( cuDeviceGetAttribute
      ( &gi.shared_sz_per_bl_bytes, 
        CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK, dev ) );

  CE( cuDeviceGetAttribute
      ( &gi.shared_sz_per_mp_bytes, 
        CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_MULTIPROCESSOR, dev ) );

  CE( cuDeviceGetAttribute
      ( &gi.l2_cache_sz_bytes, CU_DEVICE_ATTRIBUTE_L2_CACHE_SIZE, dev) );

  // Note: Size of read-only cache for CC 3.5 and 3.7 is an upper bound.
  gi.roc_sz_bytes =
    gi.cc_idx == CC_35 ? 3 << 14 :
    gi.cc_idx >= CC_50 ? 3 << 14 : 0;

  // Based on experiments performed on Kepler GPUs.
  //
  gi.insn_cache_sz_bytes = 0x8000;

  gi.insn_size_bytes = 8;
  gi.code_size_per_insn_bytes = 9;

  gi.sass_ldst_offset_bits = gi.cc_idx < CC_50 ? 32 : 24;
  gi.sass_ldst_offset_min = -( int64_t(1) << ( gi.sass_ldst_offset_bits - 1 ) );
  gi.sass_ldst_offset_max = -gi.sass_ldst_offset_min-1;

  CE( cuDeviceTotalMem( &gi.global_mem_sz_bytes, dev) );

  int clock_rate_khz;
  CE( cuDeviceGetAttribute
      ( &clock_rate_khz,
        CU_DEVICE_ATTRIBUTE_CLOCK_RATE, dev) );
  gi.clock_freq_hz = 1000.0 * clock_rate_khz;

  int memory_clock_rate_khz;
  CE( cuDeviceGetAttribute
      ( &memory_clock_rate_khz,
        CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE, dev) );
  gi.mem_clock_freq_hz = 1000.0 * memory_clock_rate_khz;

  CE( cuDeviceGetAttribute
      ( &gi.global_mem_bus_width_bits,
        CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH, dev) );

  gi.chip_bw_Bps =
    2.0 * gi.mem_clock_freq_hz * ( gi.global_mem_bus_width_bits >> 3 );

  // Source: CUDA C Programming Guide Version 4.1 Table 5-1
  device_capabilities[CC_1x].fu_sp = 8;
  device_capabilities[CC_1x].fu_dp = 1;
  device_capabilities[CC_1x].fu_int_al = 8;
  device_capabilities[CC_1x].fu_int_sc = 8;
  device_capabilities[CC_1x].fu_int_m = 1;  // Multiple insn.
  device_capabilities[CC_1x].fu_conv = 2;
  device_capabilities[CC_1x].fu_ls = 8;
  device_capabilities[CC_1x].fu_special = 2;
  device_capabilities[CC_20].fu_sp = 32;
  device_capabilities[CC_20].fu_dp = 16;
  device_capabilities[CC_20].fu_int_al = 32;
  device_capabilities[CC_20].fu_int_sc = 16;
  device_capabilities[CC_20].fu_int_m = 16;
  device_capabilities[CC_20].fu_conv = 16;
  device_capabilities[CC_20].fu_ls = 16;
  device_capabilities[CC_20].fu_special = 4;
  device_capabilities[CC_21].fu_sp = 48;
  device_capabilities[CC_21].fu_dp = 4;
  device_capabilities[CC_21].fu_int_al = 48;
  device_capabilities[CC_21].fu_int_sc = 16;
  device_capabilities[CC_21].fu_int_m = 16;
  device_capabilities[CC_21].fu_conv = 16;
  device_capabilities[CC_21].fu_ls = 16;
  device_capabilities[CC_21].fu_special = 8;

  device_capabilities[CC_30].fu_sp = 192;
  device_capabilities[CC_30].fu_dp = 8;
  device_capabilities[CC_30].fu_int_al = 160;
  device_capabilities[CC_30].fu_int_sc = 32;
  device_capabilities[CC_30].fu_int_m = 32;
  device_capabilities[CC_30].fu_conv = 128;
  device_capabilities[CC_30].fu_ls = 64;
  device_capabilities[CC_30].fu_special = 32;

  device_capabilities[CC_35].fu_sp = 192;
  device_capabilities[CC_35].fu_dp = 64;      // Lower for GeForce GPUs.
  device_capabilities[CC_35].fu_int_al = 160;
  device_capabilities[CC_35].fu_int_sc = 64;
  device_capabilities[CC_35].fu_int_m = 32;
  device_capabilities[CC_35].fu_conv = 128;
  device_capabilities[CC_35].fu_ls = 64;
  device_capabilities[CC_35].fu_special = 32;

  device_capabilities[CC_50].fu_sp = 128;
  device_capabilities[CC_50].fu_dp = 4;  // Four!
  device_capabilities[CC_50].fu_int_al = 128;
  device_capabilities[CC_50].fu_int_sc = 64;
  device_capabilities[CC_50].fu_int_m = 1;   // Multiple instructions.
  device_capabilities[CC_50].fu_conv = 32;
  device_capabilities[CC_50].fu_ls = 64;
  device_capabilities[CC_50].fu_special = 32;

  device_capabilities[CC_52] = device_capabilities[CC_50];

  // Note: CC_53 has 16-bit FP instructions, older CC lack these.
  device_capabilities[CC_53] = device_capabilities[CC_50];

  device_capabilities[CC_60].fu_sp = 64;
  device_capabilities[CC_60].fu_dp = 32;
  device_capabilities[CC_60].fu_int_al = 64;
  device_capabilities[CC_60].fu_int_sc = 32;
  device_capabilities[CC_60].fu_int_m = 1;   // Multiple instructions.
  device_capabilities[CC_60].fu_conv = 16;
  device_capabilities[CC_60].fu_ls = 64;
  device_capabilities[CC_60].fu_special = 16;

  device_capabilities[CC_61].fu_sp = 128;
  device_capabilities[CC_61].fu_dp = 4;   // Four!
  device_capabilities[CC_61].fu_int_al = 128;
  device_capabilities[CC_61].fu_int_sc = 64;
  device_capabilities[CC_61].fu_int_m = 1;   // Multiple instructions.
  device_capabilities[CC_61].fu_conv = 32;
  device_capabilities[CC_61].fu_ls = 64;
  device_capabilities[CC_61].fu_special = 32;


  gi.dev_cap = device_capabilities[gi.cc_idx];

  // Multiple of threads for which instruction issue is fully utilized.
  gi.thd_granularity =
    gi.cc_major == 1 ? 32 :
    gi.cc_major == 2 ? 64 :
    gi.cc_major == 3 ? 128 : 128;

  const int sp_per_mp = gi.dev_cap.fu_sp;
  const int dp_per_mp = gi.dev_cap.fu_dp;

  const bool report = true;
  if ( !report ) return;

  pString rpt;
  rpt += "** CaCUDALib GPU Info Report **\n";

  rpt.sprintf("CUDA DRIVER VERSION %d\n",gi.cuda_driver_version);

  rpt.sprintf
    ("GPU %d: %s @ %.2f GHz WITH %zd MiB GLOBAL MEM\n",
     dev, gi.gpu_name, gi.clock_freq_hz * 1e-9,
     gi.global_mem_sz_bytes >> 20);

  rpt.sprintf
    ("GPU %d: L2: %d kiB   MEM<->L2: %.1f GB/s\n",
     dev,
     gi.l2_cache_sz_bytes >> 10,
     gi.chip_bw_Bps * 1e-9);
  
  rpt.sprintf
    ("GPU %d: CC: %d.%d  MP: %2d  SP/MP: %3d  DP/MP: %2d  TH/BL: %4d\n",
     dev, gi.cc_major, gi.cc_minor,
     gi.num_multiprocessors,
     sp_per_mp, dp_per_mp,
     gi.max_threads_per_bl);

  rpt.sprintf
    ("GPU %d: SHARED: %5d B/BL  %5d B/MP   CONST: %5d B  # REGS: %5d\n",
     dev,
     gi.shared_sz_per_bl_bytes, gi.shared_sz_per_mp_bytes,
     gi.constant_sz_bytes, gi.max_registers_per_bl);

  msg_info("%s",rpt.s);

}

void
CaCUDALib_CUDA_Driver_Manager::microbench_range_clamp
(const char *param_name, MB_Value *v)
{
  if ( v->val < v->min || v->val > v->max )
    {
      CCTK_VWarn
        (CCTK_WARN_ALERT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "Micro-benchmark result %s=%f out of range [%f,%f], "
         "using fallback value %f\n",
         param_name, v->val, v->min, v->max, v->fbk);
      v->val = v->fbk;
    }
}

void
CaCUDALib_CUDA_Driver_Manager::gpu_info_set_with_microbenchmark(int dev)
{
    const bool event_tracing_prev = NPerf_event_tracing_off();
    NPerf_call_tracing_pause();

    CaCUDALib_GPU_Info& gi = gpu_info;
    const int fu_sp = gi.dev_cap.fu_sp;
    const int fu_dp = gi.dev_cap.fu_dp;
    const int fu_int = gi.dev_cap.fu_int_al;
    assert( fu_sp > 0 ); // Make sure gpu_info_init ran.

    const double time_start = time_wall_fp();
    // measure L2 cache and global memory latency
    gi.l2_cache_lat_cyc.val = mb_get_l2cache_lat(&gi);
    gi.gl_mem_lat_cyc.val = mb_get_gmem_lat(&gi);

    const int sm_num_strides =
      sizeof(gi.sh_mem_lat_cyc) / sizeof(gi.sh_mem_lat_cyc[0]);
    const int bar_num_sizes =
      sizeof(gi.sh_mem_sync_cyc) / sizeof(gi.sh_mem_sync_cyc[0]);

    // measure shared memory latency with different bank conflict counts
    for ( int i=0; i<sm_num_strides; i++ )
      gi.sh_mem_lat_cyc[i].val = mb_get_smem_lat(&gi, CCTK_REAL(1<<i));

    // measure shared memory synchronization overhead with different
    // warps/block
    for ( int i=0; i<bar_num_sizes; i++ )
      gi.sh_mem_sync_cyc[i].val = mb_get_smem_sync(&gi, 1<<i);

    // measure floating point operation latency and throughput
    gi.sp_fp_op_lat_cyc[FP_MADD].val = mb_get_sp_madd_lat(&gi);
    gi.dp_fp_op_lat_cyc[FP_MADD].val = mb_get_dp_madd_lat(&gi);
    gi.sp_fp_thpt_op_p_cyc[FP_MADD].val = mb_get_sp_madd_thpt(&gi);
    gi.dp_fp_thpt_op_p_cyc[FP_MADD].val = mb_get_dp_madd_thpt(&gi);
    gi.sp_fp_op_lat_cyc[FP_RECIP].val = mb_get_sp_recip_lat(&gi);
    gi.dp_fp_op_lat_cyc[FP_RECIP].val = mb_get_dp_recip_lat(&gi);
    gi.sp_fp_thpt_op_p_cyc[FP_RECIP].val = mb_get_sp_recip_thpt(&gi);
    gi.dp_fp_thpt_op_p_cyc[FP_RECIP].val = mb_get_dp_recip_thpt(&gi);
    gi.sp_fp_op_lat_cyc[FP_DIV].val = mb_get_sp_div_lat(&gi);
    gi.dp_fp_op_lat_cyc[FP_DIV].val = mb_get_dp_div_lat(&gi);
    gi.sp_fp_thpt_op_p_cyc[FP_DIV].val = mb_get_sp_div_thpt(&gi);
    gi.dp_fp_thpt_op_p_cyc[FP_DIV].val = mb_get_dp_div_thpt(&gi);
    gi.sp_fp_op_lat_cyc[FP_POW].val = mb_get_sp_pow_lat(&gi);
    gi.dp_fp_op_lat_cyc[FP_POW].val = mb_get_dp_pow_lat(&gi);
    gi.sp_fp_thpt_op_p_cyc[FP_POW].val = mb_get_sp_pow_thpt(&gi);
    gi.dp_fp_thpt_op_p_cyc[FP_POW].val = mb_get_dp_pow_thpt(&gi);
    gi.sp_fp_op_lat_cyc[FP_SIN].val = mb_get_sp_sin_lat(&gi);
    gi.dp_fp_op_lat_cyc[FP_SIN].val = mb_get_dp_sin_lat(&gi);
    gi.sp_fp_thpt_op_p_cyc[FP_SIN].val = mb_get_sp_sin_thpt(&gi);
    gi.dp_fp_thpt_op_p_cyc[FP_SIN].val = mb_get_dp_sin_thpt(&gi);
    gi.sp_fp_op_lat_cyc[FP_SQRT].val = mb_get_sp_sqrt_lat(&gi);
    gi.dp_fp_op_lat_cyc[FP_SQRT].val = mb_get_dp_sqrt_lat(&gi);
    gi.sp_fp_thpt_op_p_cyc[FP_SQRT].val = mb_get_sp_sqrt_thpt(&gi);
    gi.dp_fp_thpt_op_p_cyc[FP_SQRT].val = mb_get_dp_sqrt_thpt(&gi);
    gi.sp_fp_op_lat_cyc[FP_SQRT_RECIP].val = mb_get_sp_sqrt_recip_lat(&gi);
    gi.dp_fp_op_lat_cyc[FP_SQRT_RECIP].val = mb_get_dp_sqrt_recip_lat(&gi);
    gi.sp_fp_thpt_op_p_cyc[FP_SQRT_RECIP].val = mb_get_sp_sqrt_recip_thpt(&gi);
    gi.dp_fp_thpt_op_p_cyc[FP_SQRT_RECIP].val = mb_get_dp_sqrt_recip_thpt(&gi);
    const double elapsed_time_s = time_wall_fp() - time_start;

    // set reasonable ranges and fallback values for micro-benchmark results
    // fallback values are based on NVIDIA K20xm
    const int ref_cc = CC_35;
    const int ref_fu_sp = max(1,device_capabilities[ref_cc].fu_sp);
    const int ref_fu_dp = max(1,device_capabilities[ref_cc].fu_dp);
    const int ref_fu_int = max(1,device_capabilities[ref_cc].fu_int_al);

    // Compute a "mix" scale factor between the reference and current
    // device based on an assumed 50/50 mix of FP and integer
    // instructions. This will be used to scale fallback throughput
    // values of those PTX instructions which are likely implemented
    // using library functions.

    const double ref_fu_sp_mix = 2.0/(1.0/ref_fu_sp + 1.0/ref_fu_int);
    const double ref_fu_dp_mix = 2.0/(1.0/ref_fu_dp + 1.0/ref_fu_int);

    const double fu_sp_mix = 2.0/(1.0/fu_sp + 1.0/fu_int);
    const double fu_dp_mix = 2.0/(1.0/fu_dp + 1.0/fu_int);

    const double scale_thpt_sp_mix = fu_sp_mix / ref_fu_sp_mix;
    const double scale_thpt_dp_mix = fu_dp_mix / ref_fu_dp_mix;

    gi.l2_cache_lat_cyc.init(100, 400, 193);
    gi.gl_mem_lat_cyc.init(200, 800, 306);

    for ( int i=0; i<sm_num_strides; i++ )
      gi.sh_mem_lat_cyc[i].init
        (20, 64 << i,  32 + ( i ? 32 << i - 1 : 0 ) );

    for ( int i=0; i<bar_num_sizes; i++ )
      if ( gi.cc_idx < CC_50 )
        gi.sh_mem_sync_cyc[i].init(100 + i * 15, 400 + i * 70, 273 + i * 35.4);
    else
        gi.sh_mem_sync_cyc[i].init(20 + i * 5, 100 + i * 20, 40 + i * 5);

    gi.sp_fp_op_lat_cyc[FP_MADD].init(5, 40, 12);
    gi.dp_fp_op_lat_cyc[FP_MADD].init(5, 80, 12);
    gi.sp_fp_thpt_op_p_cyc[FP_MADD].init(0.5*fu_sp, 1.5*fu_sp, fu_sp);
    gi.dp_fp_thpt_op_p_cyc[FP_MADD].init(0.5*fu_dp, 1.5*fu_dp, fu_dp);;

    gi.sp_fp_op_lat_cyc[FP_RECIP].init(50, 800, 210.3);
    gi.dp_fp_op_lat_cyc[FP_RECIP].init(50, 800, 278.4);

#define SCALE_SP_DP(insn, ref_sp, ref_dp)                                     \
    gi.sp_fp_thpt_op_p_cyc[insn]                                              \
      .init_scaled(scale_thpt_sp_mix, 0.1, 4.0, ref_sp);                      \
    gi.dp_fp_thpt_op_p_cyc[insn]                                              \
      .init_scaled(scale_thpt_dp_mix, 0.1, 4.0, ref_dp);

    SCALE_SP_DP(FP_RECIP, 5.2, 3.5);

    gi.sp_fp_op_lat_cyc[FP_DIV].init(50, 1600, 209.9);
    gi.dp_fp_op_lat_cyc[FP_DIV].init(50, 1600, 333.4);

    SCALE_SP_DP(FP_DIV, 5.2, 2.4);

    gi.sp_fp_op_lat_cyc[FP_POW].init(50, 3200, 688.7);
    gi.dp_fp_op_lat_cyc[FP_POW].init(50, 3200, 1409.0);

    SCALE_SP_DP(FP_POW, 1.3, 0.5);

    gi.sp_fp_op_lat_cyc[FP_SIN].init(50, 1600, 206.3);
    gi.dp_fp_op_lat_cyc[FP_SIN].init(50, 1600, 272.4);

    SCALE_SP_DP(FP_SIN, 5.6, 2.2);

    gi.sp_fp_op_lat_cyc[FP_SQRT].init(50, 1600, 210.3);
    gi.dp_fp_op_lat_cyc[FP_SQRT].init(50, 1600, 369.9);

    SCALE_SP_DP(FP_SQRT, 5.4, 2.7);

    gi.sp_fp_op_lat_cyc[FP_SQRT_RECIP].init(50, 1600, 418.4);
    gi.dp_fp_op_lat_cyc[FP_SQRT_RECIP].init(50, 1600, 590.5);

    SCALE_SP_DP(FP_SQRT_RECIP, 2.8, 1.5);

    // make sure micro-benchmarks would return reasonable results
    // issue a warning and use fallback values when measured results out of
    // range
    for ( int i=0; i<sm_num_strides; i++ )
      microbench_range_clamp
        ( pstringf("sh_mem_lat_cyc (Shared memory latency, stride %d)", 1<<i),
          &(gi.sh_mem_lat_cyc[i]) );

    for ( int i=0; i<bar_num_sizes; i++ )
      microbench_range_clamp
        ( pstringf
          ("sh_mem_sync_cyc (Shared memory sync. overhead, warps/block=%d)",
           1<<i),
          &(gi.sh_mem_sync_cyc[i]) );

    for ( int i = 0; i < FP_TYPE_ENUM_SIZE; i++ )
      {
        const string fp_op = mb_fp_type[i];
        string fp_check = fp_op + " single-precision operation latency(cyc)";
        microbench_range_clamp(fp_check.c_str(), &(gi.sp_fp_op_lat_cyc[i]));
        fp_check = fp_op + " double-precision operation latency(cyc)";
        microbench_range_clamp(fp_check.c_str(), &(gi.dp_fp_op_lat_cyc[i]));
        fp_check = fp_op + " single-precision throughput(op/cyc)";
        microbench_range_clamp(fp_check.c_str(), &(gi.sp_fp_thpt_op_p_cyc[i]));
        fp_check = fp_op + " double-precision throughput(op/cyc)";
        microbench_range_clamp(fp_check.c_str(), &(gi.dp_fp_thpt_op_p_cyc[i]));
      }

    pString rpt;
    rpt += "** CaCUDALib GPU micro-benchmark Report **\n";

    rpt.sprintf("%-17s  %10s  [ %10s ]   %12s  [ %12s ]\n",
                "Operation", "Latency", "Latency",
                "Throughput", "Throughput");
    rpt.sprintf("%-17s  %10s  [ %10s ]   %12s  [ %12s ]\n",
                "", "Measured", "Fallback", "Measured", "Fallback");

    auto prow = [&](const char *name, MB_Value &mbv)
      { rpt.sprintf
        ("%-17s  %6.1f cyc  [ %6.1f cyc ]\n", name, mbv.val, mbv.fbk); };
    auto prowt = [&](const char *name, MB_Value &mbl, MB_Value &mbt)
      { rpt.sprintf
        ("%-17s  %6.1f cyc  [ %6.1f cyc ]   %5.1f op/cyc  [ %5.1f op/cyc ]\n",
         name, mbl.val, mbl.fbk, mbt.val, mbt.fbk); };

    prow("L2 Cache Lat", gi.l2_cache_lat_cyc);
    prow("Global Memory Lat", gi.gl_mem_lat_cyc);

    for ( int i=0; i<sm_num_strides; i++ )
      prow( pstringf("SM Lat Str %d",1<<i), gi.sh_mem_lat_cyc[i] );

    for ( int i=0; i<bar_num_sizes; i++ )
      prow( pstringf("Sync Size %d",1<<i), gi.sh_mem_sync_cyc[i] );

    for ( int i = 0; i < FP_TYPE_ENUM_SIZE; i++ )
      prowt( pstringf("%s %s", "DP", mb_fp_type[i]),
             gi.dp_fp_op_lat_cyc[i], gi.dp_fp_thpt_op_p_cyc[i] );

    for ( int i = 0; i < FP_TYPE_ENUM_SIZE; i++ )
      prowt( pstringf("%s %s", "SP", mb_fp_type[i]),
             gi.sp_fp_op_lat_cyc[i], gi.sp_fp_thpt_op_p_cyc[i] );

    rpt.sprintf("GPU microbenchmark elapsed time %.3f s.\n", elapsed_time_s);

    msg_info("%s",rpt.s);

    rpt = "";

    NPerf_call_tracing_resume();
    NPerf_event_tracing_set(event_tracing_prev);
}

void
CaCUDALib_CUDA_Driver_Manager::gpu_info_freeze()
{
  gpu_info_static = gpu_info;
  gpu_info_cpy = gpu_info;
  const bool opt_mb_val_lax = true;

  auto fb = [&](MB_Value& mbv)
    {
      assert( mbv.max >= mbv.min );
      assert( opt_mb_val_lax || mbv.val >= mbv.min && mbv.val <= mbv.max );
      assert( mbv.fbk >= mbv.min && mbv.fbk <= mbv.max );
      mbv.val = mbv.fbk;
    };
#define FB(a) for ( MB_Value& mbv : gpu_info_static.a ) fb(mbv);
  fb(gpu_info_static.l2_cache_lat_cyc);
  fb(gpu_info_static.gl_mem_lat_cyc);
  FB(sh_mem_lat_cyc);
  FB(sh_mem_sync_cyc);
  FB(sp_fp_op_lat_cyc); FB(dp_fp_op_lat_cyc);
  FB(sp_fp_thpt_op_p_cyc); FB(dp_fp_thpt_op_p_cyc);
#undef FB

}

extern "C" CCTK_INT
nan_scan
(CCTK_POINTER cctkGH, CCTK_INT gsize_points, void *d_addr)
{
  CaCUDALib_CUDA_Driver_Manager* const dm =
    CaCUDALib_CUDA_driver_manager_get();
  assert( dm );
  dm->nan_scan((cGH*)cctkGH, gsize_points, (CUdeviceptr)d_addr);
  return 0;
}

void
CaCUDALib_CUDA_Driver_Manager::nan_scan_dtoh
(int gsize_bytes, int copy_size_bytes, CUdeviceptr d_addr)
{
  CUdeviceptr pbase;
  size_t psize;
  NPerf_call_tracing_pause();
  CE( cuMemGetAddressRange(&pbase, &psize, d_addr) );
  assert( psize >= gsize_bytes );
  assert( size_t(pbase) + psize >= size_t(d_addr) + gsize_bytes );
  if ( copy_size_bytes > nan_scan_buffer_size )
    {
      nan_scan_buffer_size = copy_size_bytes;
      nan_scan_buffer =
        (CCTK_REAL*) realloc( nan_scan_buffer, nan_scan_buffer_size );
    }
  CE( cuMemcpyDtoH( nan_scan_buffer, d_addr, copy_size_bytes ) );
  NPerf_call_tracing_resume();
}

void
CaCUDALib_CUDA_Driver_Manager::nan_scan
(CCTK_ARGUMENTS, int gsize_points, CUdeviceptr d_addr)
{
  const int gsize_bytes = gsize_points * sizeof(CCTK_REAL);
  nan_scan_dtoh(gsize_bytes,gsize_bytes,d_addr);

  for ( int j=0; j<gsize_points; j++ )
    assert( isfinite(nan_scan_buffer[j]) );
}

void
CaCUDALib_CUDA_Driver_Manager::trace_gf_init(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if ( trace_gf_inited ) return;

  trace_gf_inited = true;
  trace_gf_done = false;

  if ( myproc != 0 )
    {
      opt_trace_gf = 0;
      return;
    }

  const char *ptr = trace_gf;
  while ( *ptr == ' ' ) ptr++;
  switch ( *ptr ) {
  case 0: case 'n': opt_trace_gf = 0;  return;
  case 'r': case 'w': opt_trace_gf = *ptr; break;
  default:
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Unrecognized option for parameter CaKernel::trace_gf.\n"
       "Should start with 'n', 'r', or 'w'.\n");
  }

  string userid = getenv("USER");
  pSplit pieces(out_dir,'/');
  string out_dir_name = pieces.pop();

  trace_gf_path =
    "/tmp/chemora-" + userid + "-" + out_dir_name + "-gf-trace.dat";

  const int cagh_ni = cctk_lsh[0];
  const int cagh_nj = cctk_lsh[1];
  const int cagh_nk = cctk_lsh[2];
  opt_trace_gf_lim_nk = min(5,cagh_nk);

  opt_trace_gf_sample_size_max_bytes =
    cagh_ni * cagh_nj * opt_trace_gf_lim_nk * sizeof(CCTK_REAL);

  if ( opt_trace_gf == 'w' )
    {
      trace_gf_fd = creat(trace_gf_path.c_str(),00644);
      DM_Trace_Trace_Header& h = trace_gf_trace_header;
      h.ni = cagh_ni; h.nj = cagh_nj; h.nk = cagh_nk;
      h.elt_size_bytes = sizeof(CCTK_REAL);
      const int gf_size_bytes =
        cagh_ni * cagh_nj * cagh_nk * h.elt_size_bytes;
      h.sample_size_bytes =
        min( gf_size_bytes, opt_trace_gf_sample_size_max_bytes );
      pWrite(trace_gf_fd,h);
      trace_gf_eof_fo = off_t(1) << 30;
    }

  if ( opt_trace_gf == 'r' )
    {
      trace_gf_fd = open(trace_gf_path.c_str(),0);
      if ( trace_gf_fd < 0 )
        CCTK_VWarn(CCTK_WARN_ABORT,__LINE__,__FILE__, CCTK_THORNSTRING,
                   "Could not find trace file at %s.\n",
                   trace_gf_path.c_str());
      assert( trace_gf_fd > 0 );
      trace_gf_eof_fo = lseek(trace_gf_fd, 0, SEEK_END);
      lseek(trace_gf_fd, 0, SEEK_SET);
      DM_Trace_Trace_Header& h = trace_gf_trace_header;
      const ssize_t amt = pRead(trace_gf_fd, h);
      assert( amt == sizeof(h) );
      assert( h.ni == cagh_ni );
      assert( h.nj == cagh_nj );
      assert( h.nk == cagh_nk );
      assert( h.elt_size_bytes == sizeof(CCTK_REAL) );
      trace_gf_sample_size_elts = h.sample_size_bytes / h.elt_size_bytes;
      trace_gf_gf_buffer = new CCTK_REAL[trace_gf_sample_size_elts];
    }
}


void
CaCUDALib_CUDA_Driver_Manager::nan_scan
(CCTK_ARGUMENTS, CaKernel_Kernel_Config *kc,
 void **karg_addrs, char dir_to_examine)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const bool compile_only =
      *(int *)CCTK_VarDataPtr(cctkGH, 0, "CaKernel::compile_only");

  if ( compile_only ) return;

  const bool do_nan_scan = false;
  trace_gf_init(CCTK_PASS_CTOC);

  if ( opt_trace_gf && trace_gf_done ) return;

  if ( !opt_trace_gf && !do_nan_scan ) return;

  const int ni = cctk_lsh[0];
  const int nj = cctk_lsh[1];
  const int nk = cctk_lsh[2];
  const int gsize_points = ni * nj * nk;
  const int num_gf_args = kc->var_tmpl_info_size;

  CUdeviceptr **karg_cuaddrs = (CUdeviceptr**) karg_addrs;

  DM_Trace_Trace_Header& h = trace_gf_trace_header;

  const int gsize_bytes = gsize_points * sizeof(CCTK_REAL);
  const int copy_size_bytes =
    max( do_nan_scan ? gsize_bytes : 0,
         opt_trace_gf ? h.sample_size_bytes : 0 );

  const size_t msg_gf_bad_points_max = 5;
  const double mismatch_tolerance_abs = 1e-20;
  const double mismatch_tolerance_rel = sizeof(CCTK_REAL) == 8 ? 1e-8 : 1e-5;
  pString trace_msg;
  size_t n_bad_points = 0;
  int n_bad_gf = 0;
  int n_gf_absent = 0;
  pString bad_gf;

  off_t data_end_fo = 0;

  vector<int> gf_sorted;
  for ( int i=0; i<num_gf_args; i++ )
    {
      const char dir =
        string("in") == kc->var_tmpl_info[i].intent ? 'i' :
        string("inout") == kc->var_tmpl_info[i].intent ? 'b' :
        string("out") == kc->var_tmpl_info[i].intent ? 'o' : 'x';
      assert( dir != 'x' );
      if ( dir_to_examine == 'i' && dir == 'o' ) continue;
      if ( dir_to_examine == 'o' && dir == 'i' ) continue;
      gf_sorted.push_back(i);
    }

  sort( gf_sorted.begin(), gf_sorted.end(), [&](int a, int b)
        { return strcmp(kc->var_tmpl_info[a].name,
                        kc->var_tmpl_info[b].name) <= 0; } );

  const bool gout = dir_to_examine == 'o';
  const int ib = gout ? kc->bwid_n[0] : 0, ie = gout ? kc->i_stop[0] : ni;
  const int jb = gout ? kc->bwid_n[1] : 0, je = gout ? kc->i_stop[1] : nj;
  const int kb = gout ? kc->bwid_n[2] : 0;
  const int ke = min( opt_trace_gf_lim_nk, gout ? kc->i_stop[2] : nk );

  map<string,off_t> trace_name_to_fo;
  const int name_len_lim = sizeof(DM_Trace_Sample_Header::gf_name) - 1;

  DM_Trace_Bunch_Header bh;
  if ( opt_trace_gf == 'w' )
    {
      bh.num_gf = gf_sorted.size();
      bh.direction = dir_to_examine;
      pWrite(trace_gf_fd,bh);
      for ( int i: gf_sorted )
        {
          DM_Trace_Sample_Header sh;
          sh.gf_index = kc->var_tmpl_info[i].vari;
          strncpy(sh.gf_name,kc->var_tmpl_info[i].name,name_len_lim);
          sh.gf_name[name_len_lim] = 0;
          pWrite(trace_gf_fd,sh);
        }
    }
  else if ( opt_trace_gf == 'r' )
    {
      pRead(trace_gf_fd,bh);
      off_t data_start = lseek(trace_gf_fd,0,SEEK_CUR)
        + bh.num_gf * sizeof(DM_Trace_Sample_Header);
      data_end_fo = data_start + bh.num_gf * h.sample_size_bytes;
      for ( int i=0; i<bh.num_gf; i++ )
        {
          DM_Trace_Sample_Header sh;
          pRead(trace_gf_fd,sh);
          string name_str = sh.gf_name;
          trace_name_to_fo[name_str] = data_start + i * h.sample_size_bytes;
        }
    }

  for ( int i: gf_sorted )
    {
      CUdeviceptr d_addr = *karg_cuaddrs[i];
      nan_scan_dtoh(gsize_bytes,copy_size_bytes,d_addr);

      if ( opt_trace_gf == 'w' )
        {
          write(trace_gf_fd,nan_scan_buffer,h.sample_size_bytes);
        }
      else if ( opt_trace_gf == 'r' )
        {
          string name_str = kc->var_tmpl_info[i].name;
          if ( name_str.size() > name_len_lim )
            name_str.resize(name_len_lim);
          if ( const off_t fo = trace_name_to_fo[name_str] )
            {
              lseek(trace_gf_fd,fo,SEEK_SET);
              read(trace_gf_fd,trace_gf_gf_buffer,h.sample_size_bytes);
              size_t n_gf_bad_points = 0;
              pString gf_msg;
              for ( int ck = kb;  ck < ke;  ck++ )
                for ( int cj = jb;  cj < je;  cj++ )
                  for ( int ci = ib;  ci < ie;  ci++ )
                    {
                      const int j = ci + cj * ni + ck * ni * nj;
                      if ( nan_scan_buffer[j] == trace_gf_gf_buffer[j] )
                        continue;
                      double delta =
                        fabs( nan_scan_buffer[j] - trace_gf_gf_buffer[j] );
                      if ( delta <= mismatch_tolerance_abs ) continue;
                      double larger =
                        max(fabs(nan_scan_buffer[j]),
                            fabs(trace_gf_gf_buffer[j]));
                      double frac_diff = delta / larger;
                      if ( frac_diff <= mismatch_tolerance_rel ) continue;
                      n_gf_bad_points++;
                      if ( n_gf_bad_points > msg_gf_bad_points_max ) continue;
                      gf_msg.sprintf
                        (" Mismatch at %3d, %3d, %3d   "
                         "%.10e != %.10e (trace)\n",
                         ci, cj, ck, nan_scan_buffer[j], trace_gf_gf_buffer[j]);
                    }
              n_bad_points += n_gf_bad_points;
              if ( n_gf_bad_points )
                {
                  trace_msg.sprintf
                    ("Found %6zd mismatches in %s, for example:\n",
                     n_gf_bad_points, kc->var_tmpl_info[i].name);
                  trace_msg += gf_msg;
                  n_bad_gf++;
                  bad_gf.sprintf
                    ("%s%s", n_bad_gf > 1 ? ", " : "",
                     kc->var_tmpl_info[i].name);
                }
            }
          else
            {
              n_gf_absent++;
            }
        }

      if ( ! do_nan_scan ) continue;
      for ( int j=0; j<gsize_points; j++ )
        if ( !isfinite(nan_scan_buffer[j]) )
          CCTK_VWarn
            (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
             "Non-finite item in gf %s[%d] (vari %d).\n",
             kc->var_tmpl_info[i].name, j, kc->var_tmpl_info[i].vari);
    }

  if ( data_end_fo )
    lseek(trace_gf_fd, data_end_fo, SEEK_SET);

  if ( n_bad_points )
    {
      pString full_msg;
      full_msg += "** GF Trace Mismatches **\n";
      full_msg.sprintf
        ("In %s::%s at iteration %d.\n",
         kc->thorn_name,kc->kernel_name, cctk_iteration);
      full_msg.sprintf
        ("Number of GF: in sim %zd, in trace %d, sim/trace %d.\n",
         gf_sorted.size(), bh.num_gf, n_gf_absent);
      full_msg.sprintf
        ("Found a total of %zd mismatches in %d %s GF%s:\n",
         n_bad_points, n_bad_gf, dir_to_examine == 'i' ? "input" : "output",
         n_bad_points != 0 ? "'s" : "");
      full_msg += "  " + bad_gf + "\n";
      full_msg += "For your debugging convenience:\n";
      full_msg.sprintf
        ("  Grid (ni,nj,nk) %d, %d, %d;  Traced (ni,nj,lk) %d, %d, %d\n",
         ni, nj, nk, ni, nj, opt_trace_gf_lim_nk );
      full_msg.sprintf
        ("  Actv (ai,aj,ak) %d, %d, %d;  %d+%d+%d, %d+%d+%d, %d+%d+%d\n",
         kc->i_length[0], kc->i_length[1], kc->i_length[2],
         kc->bwid_n[0], kc->i_length[0], kc->bwid_p[0],
         kc->bwid_n[1], kc->i_length[1], kc->bwid_p[1],
         kc->bwid_n[2], kc->i_length[2], kc->bwid_p[2]);
      full_msg.sprintf
        ("  ni*nj = %d;  ni*nj*lk = %d;  ai*aj = %d;  ai*aj*alk = %d\n",
         ni * nj, ni * nj * opt_trace_gf_lim_nk,
         kc->i_length[0] * kc->i_length[1],
         kc->i_length[0] * kc->i_length[1] * ( ke - kb ));
      full_msg += trace_msg;
      full_msg.sprintf
        ("Mismatch if abs(a-b)>%g and abs(a-b)/max(abs(a),abs(b))>%g\n",
         mismatch_tolerance_abs, mismatch_tolerance_rel);
      CCTK_VWarn
        (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "%s", full_msg.s);
    }

  if ( opt_trace_gf && lseek(trace_gf_fd,0,SEEK_CUR) >= trace_gf_eof_fo )
    {
      CCTK_VInfo
        (CCTK_THORNSTRING,
         "%s at iteration %d.\n",
         opt_trace_gf == 'r'
         ? "Exhausted trace data" : "Reached trace file size limit",
         cctk_iteration);
      trace_gf_done = true;
    }

}

bool
CaCUDALib_CUDA_Driver_Manager::module_get
(CCTK_ARGUMENTS, CaKernel_Kernel_Config *kc,
 const string src_buildtime_path_p, DM_Compile_Type compile_type)
{
  DECLARE_CCTK_ARGUMENTS;
  // Return true if module needs to be initialized.
  ASSERTS( compile_type == DM_Dynamic );

  if ( devidx == -1 )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Internal Error: Dynamic GPU compilation attempted without a\n"
       "GPU assigned.  init_called: %d",
       init_called);

  Chemora_CG_Calc* const c =
    Chemora_CG_get_calc_try(kc->thorn_name,kc->kernel_name);

  string mod_key = c
    ? c->module_key_make(CCTK_PASS_CTOC,kc)
    : cakernel_module_key_make(CCTK_PASS_CTOC,kc);

  mi = &modules[mod_key];
  if ( mi->inited ) return false;
  mi->inited = true;
  mi->dm = this;
  mi->module_name = kc->kernel_name;
  mi->module_key = mod_key;
  mi->src_buildtime_path = src_buildtime_path_p;
  mi->lc.l1_local_only = false;
  return true;
}

void
CaCUDALib_CUDA_Driver_Manager::module_src_set
(int src_uncomp_chars, int src_comp_chars, char *src_words)
{
#ifdef HAVE_BZLIB
  if ( mi->src_buildtime_path == src_unpacked_buildtime_path )
    {
      mi->src_unpacked_path = src_unpacked_path;
      mi->unpacked_out_files_base = unpacked_out_files_base;
      return;
    }

  // char* const userid = cuserid(NULL);
  char* const userid = getenv("USER");
  pStringF cakernel_dir("/tmp/cactus-cakernel-%s",userid);
  int ierr = mkdir(cakernel_dir.s,0700);
  assert( !ierr || errno == EEXIST );
  string dir_i = cakernel_dir.s;
  const string targ_configs = "/configs/";
  const size_t conf_part_start = mi->src_buildtime_path.find(targ_configs);
  for ( size_t idx = conf_part_start + targ_configs.length() - 1;;)
    {
      const size_t idx_next =
        mi->src_buildtime_path.find_first_of("/",idx+1);
      if ( idx_next >= string::npos ) break;
      string part = mi->src_buildtime_path.substr(idx,idx_next-idx);
      idx = idx_next;
      if ( part == "/build" ) continue;
      if ( part == "/bindings" ) continue;
      if ( part == "/include" ) continue;
      if ( part == "/AUX" ) continue;
      dir_i += part;
      mkdir(dir_i.c_str(),0700);
    }

  pStringF file_path_no_ext("%s/%s",dir_i.c_str(),mi->module_name.c_str());
  pStringF curr_unpacked_src_path("%s-%03d-src.cup",file_path_no_ext.s,myproc);

  char* const out_buffer = (char*) malloc(src_uncomp_chars);
  const int data_size_char = src_comp_chars;
  unsigned int uncomp_size_arg = src_uncomp_chars;

  const int rv =
    BZ2_bzBuffToBuffDecompress
    (out_buffer,&uncomp_size_arg,src_words,data_size_char,0,0);
  assert( rv == BZ_OK );
  const int fd = open(curr_unpacked_src_path.s,
                      O_WRONLY | O_CREAT | O_TRUNC, 0600);
  if ( fd == -1 )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Could not open %s for output because %s\n",
       curr_unpacked_src_path.s, strerror(errno));
    
  write(fd, out_buffer, src_uncomp_chars);
  close(fd);
  free(out_buffer);

  src_unpacked_buildtime_path = mi->src_buildtime_path;
  src_unpacked_path = curr_unpacked_src_path.s;
  unpacked_out_files_base = file_path_no_ext.s;
  mi->src_unpacked_path = src_unpacked_path;
  mi->unpacked_out_files_base = unpacked_out_files_base;
#endif
}

void
CaCUDALib_CUDA_Driver_Manager::module_set_l1_local_only(bool local_only)
{
  // Deprecated.
  mi->lc.l1_local_only = local_only;
}

void
CaCUDALib_CUDA_Driver_Manager::module_vals_set_or_reset
(const string target, const string replacement, bool reset)
{
  ASSERTS( mi->inited );
  ASSERTS( reset == ( mi->lookup(target) != "" ) );

  // Don't write a zero-length string because that is used to indicate
  // a missing replacement target.
  mi->replacements[target] = replacement == "" ? " " : replacement;
}

void
CaCUDALib_CUDA_Driver_Manager::module_load()
{
  Chemora_CG_Calc* const c =
    Chemora_CG_get_calc_try(mi->kc.thorn_name,mi->kc.kernel_name);
  if ( c && c->code_target_acc_get() )
    module_load_acc(c); else module_load_cuda(c);
}

void
CaCUDALib_CUDA_Driver_Manager::module_load_cuda(Chemora_CG_Calc *c)
{
  DECLARE_CCTK_PARAMETERS;

  if ( c ) chemora_dyn_build(); else cakernel_dyn_build();

  // Compile edited preprocessed file.
  //
  // Note: -Xcudafe -w suppresses warnings, which are emitted undeservedly.
  // L1 cache local only  -Xptxas -dlcm=cg 

  pString other_flags("");

  if ( mi->lc.l1_local_only )
    other_flags += "-Xptxas -dlcm=cg ";

  string sass_out_file_path = mi->dynamic_out_files_base + ".sass";
  string cubin_file_path = mi->dynamic_out_files_base + ".cubin";
  
  pStringF cmd
    ("%s --cubin %s %s %s -Xcudafe -w '%s' -o '%s' ",
     compile_nvcc_path,
     other_flags.s,
     compile_cucc_target, compile_cucc_options, 
     mi->dynamic_src_file_path.c_str(), cubin_file_path.c_str());

  // Generate assembly code (for performance tuning).
  //
  pStringF cmd_asm
    ("echo '# %s ' > '%s'; %s --print-code '%s' >> '%s' &",
     cmd.s, sass_out_file_path.c_str(), compile_nvdisasm_path.c_str(), 
     cubin_file_path.c_str(), sass_out_file_path.c_str());
  mi->cmd_generate_asm = cmd_asm.s;

  const bool opt_use_nvcc = compile_cucc_options && compile_cucc_options[0];

  const char* const calc_name = c->calc_name.c_str();

  if ( opt_use_nvcc )
    {
      const int rv = system(cmd.s);
      if ( rv == -1 || WEXITSTATUS(rv) != 0 )
        CCTK_VWarn
          (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
           "Problem compiling %s using command \"%s\".\n",
           calc_name, cmd.s);
      else
        CCTK_VInfo
          (CCTK_THORNSTRING,
           "Compiling kernel %s using nvcc command: \"%s\".\n",
           calc_name,  cmd.s);
      // Pass the compiled code to the CUDA driver.
      //
      CE( cuModuleLoad(&mi->cu_module,cubin_file_path.c_str()) );
      mi->module_loaded = true;
      return;
    }

  const size_t cjl_ilog_buffer_size_bytes = 1 << 16;
  const size_t cjl_elog_buffer_size_bytes = 1 << 16;
  vector<char> cjl_ilog_buffer(cjl_ilog_buffer_size_bytes);
  vector<char> cjl_elog_buffer(cjl_elog_buffer_size_bytes);
  cjl_ilog_buffer[0] = 0;
  cjl_elog_buffer[0] = 0;
  pVector<CUjit_option> cjl_options;
  pVector<void*> cjl_ovals;
  auto add_opt = [&] ( CUjit_option opt, size_t val )
    { cjl_options += opt; cjl_ovals += (void*)val; };
  add_opt(CU_JIT_TARGET, gpu_info.cc_major * 10 + gpu_info.cc_minor );
  add_opt(CU_JIT_LOG_VERBOSE, 0 );
  add_opt(CU_JIT_INFO_LOG_BUFFER, ptrdiff_t(cjl_ilog_buffer.data()));
  add_opt(CU_JIT_INFO_LOG_BUFFER_SIZE_BYTES, cjl_ilog_buffer_size_bytes );
  add_opt(CU_JIT_ERROR_LOG_BUFFER, ptrdiff_t(cjl_elog_buffer.data()));
  add_opt(CU_JIT_ERROR_LOG_BUFFER_SIZE_BYTES, cjl_elog_buffer_size_bytes );

  CCTK_VInfo
    (CCTK_THORNSTRING, "Compiling kernel %s using driver JIT API.\n",
     calc_name);

  CUlinkState cj_state;
  CE( cuLinkCreate
      ( cjl_options.size(), cjl_options.data(), cjl_ovals.data(), &cj_state) );

  pVector<CUjit_option> cja_options;
  pVector<ptrdiff_t> cja_ovals;

  const CUresult linka_rv = cuLinkAddFile
    ( cj_state, CU_JIT_INPUT_PTX, mi->dynamic_src_file_path.c_str(),
      cja_options.size(), cja_options.data(), (void**) cja_ovals.data() );

  size_t cubin_size;
  void *cubin_out;
  const CUresult linkc_rv = cuLinkComplete(cj_state, &cubin_out, &cubin_size );

  if ( cjl_ilog_buffer[0] )
    CCTK_VInfo
      (CCTK_THORNSTRING, "PTX compiler info log:\n%s",cjl_ilog_buffer.data());
  if ( cjl_elog_buffer[0] )
    CCTK_VInfo
      (CCTK_THORNSTRING, "PTX compiler error log:\n%s",cjl_elog_buffer.data());

  if ( linka_rv != CUDA_SUCCESS || linkc_rv != CUDA_SUCCESS )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Problem compiling using CUDA RTC library. See log output above.\n");

  if ( dynamic_compile_generate_asm )
    {
      const int cb_fd =
        open(cubin_file_path.c_str(), O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR );
      assert( cb_fd >= 0 );
      const ssize_t amt = write(cb_fd, cubin_out, cubin_size);
      assert( amt == cubin_size );
      close(cb_fd);
    }

  // Pass the compiled code to the CUDA driver.
  //
  CE( cuModuleLoadData(&mi->cu_module,cubin_out) );
  mi->module_loaded = true;

  CE( cuLinkDestroy(cj_state) );
}

void
CaCUDALib_CUDA_Driver_Manager::module_load_acc(Chemora_CG_Calc *c)
{
  DECLARE_CCTK_PARAMETERS;

  assert( mi && !mi->dll_handle );

#ifndef HAVE_OPENACC
  assert( false );
#endif

  chemora_dyn_build();

  string dir = mi->dynamic_out_files_base;
  dir.resize(dir.rfind("/"));

  string&& file_base_cs = mi->dynamic_out_files_base + "-acc";
  const char* const file_base = file_base_cs.c_str();
  pStringF src_file_path("%s.cc", file_base);
  pStringF obj_file_path("%s.o", file_base);
  pStringF so_file_path("%s.so", file_base);
  pStringF sass_file_path("%s.sass", file_base);

  const char* const acc_cuda_target =
    gpu_info.cuda_driver_version >= 9000 ? ",cuda9.0" : "";

  pString compile_flags;
  string opt_flags_opt(chemora_acc_pgi_flags_opt);
  compile_flags += opt_flags_opt.size() ? opt_flags_opt : " -O4";

  compile_flags += " -acc -std=c++11";
  compile_flags += " -fPIC";
  compile_flags.sprintf
    (" -ta=tesla:cc%d%d,nordc,keep%s",
     gpu_info.cc_major, gpu_info.cc_minor, acc_cuda_target );

#ifdef PGI_ROOT
  string pgi_root = STR(PGI_ROOT);
#else
  string pgi_root = "/usr/bin";
#endif

  string pgi_compiler_path = pgi_root + "/bin/pgc++";

  pString cmd_compile =
    pgi_compiler_path
    + " " + compile_flags
    + " -c " + src_file_path
    + " -o " + obj_file_path;

  pStringF cmd_pipe("cd %s && %s",dir.c_str(),cmd_compile.s);

  const int rvc = system(cmd_pipe.s);
  if ( rvc == -1 || WEXITSTATUS(rvc) != 0 )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Problem compiling using command %s.\n",
       cmd_pipe.s);

  pStringF cmd_link("ld -shared -o %s %s", so_file_path.s, obj_file_path.s );

  const int rvl = system(cmd_link.s);
  if ( rvl == -1 || WEXITSTATUS(rvl) != 0 )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Problem linking using command %s.\n",
       cmd_link.s);

  mi->dll_handle = dlopen(so_file_path,RTLD_LAZY);
  if ( !mi->dll_handle )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Problem loading shared library for %s: %s\n",
       file_base, dlerror());

  // Generate assembly code (for performance tuning).
  //
  pStringF cmd_asm
    ("echo '# %s ' > '%s'; %s --print-code %s.*.bin >> '%s' &",
     cmd_compile.s, sass_file_path.s, compile_nvdisasm_path.c_str(),
     file_base, sass_file_path.s);
  mi->cmd_generate_asm = cmd_asm.s;

  mi->module_loaded = true;
}

void
CaCUDALib_CUDA_Driver_Manager::cakernel_dyn_build()
{
  DECLARE_CCTK_PARAMETERS;

  ASSERTS( mi->inited );
  const bool use_embedded = mi->src_unpacked_path.length();
  string src_path =
    use_embedded ? mi->src_unpacked_path : mi->src_buildtime_path + ".cup";
  pStringF out_files_base
    ("%s-%03d",
     use_embedded
     ? mi->unpacked_out_files_base.c_str() : mi->src_buildtime_path.c_str(),
     myproc);
  mi->dynamic_out_files_base = out_files_base.s;
  pString cup_out_file_path = out_files_base + ".cup";
  mi->dynamic_src_file_path = cup_out_file_path.s;

  ifstream raw_source(src_path.c_str());
  if ( !raw_source.good() )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Could not open %s for input.\n", src_path.c_str());

  FILE* const processed_source = fopen(cup_out_file_path,"w");
  if ( !processed_source )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Could not open %s for output.\n", cup_out_file_path.s);

  int target_found_count = 0;
  string start_marker = "QQQ_start_marker_";
  string end_marker = "QQQ_end_marker";

  // Read (preprocessed) source code for kernel, substitute actual
  // variables for certain variables, write result to new file.
  //
  string line = "";
  while ( !raw_source.eof() )
    {
      if ( line.length() == 0 )
        getline( raw_source, line );

      // Look for substitution marker on this line.
      //
      const size_t m_start_pos = line.find(start_marker);
      if ( m_start_pos == string::npos )
        {
          // If not found write the line unmodified.
          fprintf(processed_source,"%s\n", line.c_str());
          line = "";
          continue;
        }

      // Extract the key for this substitution from the source
      // file and use it to look up the replacment text.
      //
      const size_t key_start_pos = m_start_pos + start_marker.length();
      const size_t first_semi = line.find_first_of(";");
      const size_t key_end_pos = line.find_first_of(" =",key_start_pos);
      string key = line.substr(key_start_pos,key_end_pos-key_start_pos);
      string repl = mi->lookup(key);
      if ( repl == "" )
        CCTK_VWarn(CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
                   "Internal Error: Could not find replacement target %s\n"
                   "in file %s (generated from CaKernel Code).\n",
                   key.c_str(), src_path.c_str());
      const size_t m_end_pos_raw = line.find(end_marker,key_end_pos);

      // If true, replace the next line with the replacment text; if
      // false, replace the text between the start and end marker
      // (which must be on this line) with the replacement text.
      //
      const bool replace_next_line =
        first_semi > key_start_pos && m_end_pos_raw == string::npos;

      if ( target_found_count == 0 )
        {
          // A quick and dirty way of providing type names.
          fprintf(processed_source,"typedef %s CCTK_REAL;\n",
                  sizeof(CCTK_REAL) == 4 ? "float" : "double");
          fprintf(processed_source,"%s\n","typedef int CCTK_INT;");
        }
      target_found_count++;
      if ( replace_next_line )
        {
          fprintf(processed_source,"%s\n",line.c_str());
          getline( raw_source, line );
          fprintf(processed_source,"%s\n", repl.c_str());
          line = "";
        }
      else
        {
          const size_t pos_end = line.find_first_of(";",m_end_pos_raw) + 1;
          const size_t start_marker_end =
            line.find_first_of(";",key_start_pos) + 1;
          fprintf(processed_source,"%s\n",
                  line.substr(0,start_marker_end).c_str());
          fprintf(processed_source,"%s\n", repl.c_str());
          line = line.substr(pos_end);
        }
    }

  raw_source.close();
  fclose(processed_source);

  ASSERTS( target_found_count >= 1 );
}

void
CaCUDALib_CUDA_Driver_Manager::chemora_dyn_build()
{
  DECLARE_CCTK_PARAMETERS;

  ASSERTS( mi->inited );

  Chemora_CG_Calc* const c = 
    Chemora_CG_get_calc_try(mi->kc.thorn_name,mi->kc.kernel_name);

  const bool verbose = false;
  if ( verbose ) printf("Chemora building %s\n",mi->kc.kernel_name);

  mi->dynamic_out_files_base = c->generate_code();
  string kn = mi->kc.kernel_name;
  const bool ptx = true;

  mi->dynamic_src_file_path =
    mi->dynamic_out_files_base + ( ptx ? ".ptx" : ".cu" );
}

void
CaCUDALib_CUDA_Driver_Manager::module_generate_assembler()
{
  DECLARE_CCTK_PARAMETERS;

  if ( !dynamic_compile_generate_asm ) return;
  if ( mi->cmd_generate_asm == "" ) return;
  system(mi->cmd_generate_asm.c_str());
  mi->cmd_generate_asm = "";
}

void
CaCUDALib_CUDA_Driver_Manager::module_unload()
{
  if ( ! mi->module_loaded ) return;
  CE( cuModuleUnload( mi->cu_module ) );
  mi->module_loaded = false;
  assert( mi->function_info.size() == 0 );
  for ( map<string,Function_Info>::iterator fiter = mi->functions.begin();
        fiter != mi->functions.end(); fiter++ )
    fiter->second.inited = false;
}


Module_Info::Module_Info()
{
  inited = false;
  module_loaded = false;
  dll_handle = NULL;
  dm = NULL;
  src_unpacked_path = "";
  config_decide_iteration = 0;
  num_launches_notrace = num_launches = 0;
  CE( cuEventCreate( &launch_start_ev, CU_EVENT_BLOCKING_SYNC ) );
}

Function_Info::~Function_Info()
{
  if ( launch_end_ev )
    {
      // Don't check for an error since if CUDA has shut
      // down error messages are not reliable.
      cuEventDestroy( launch_end_ev );
    }
}

CaCUDALib_GPU_Info&
Module_Info::gpu_info_get()
{
  return *dm->gpu_info_get();
}

CaCUDALib_GPU_Info&
Module_Info::gpu_info_static_get()
{
  return *dm->gpu_info_static_get();
}

void
Module_Info::function_info_insert(Function_Info *fi)
{
  function_info += fi;
  fi->mi = this;
}

void
Module_Info::function_info_reset()
{
  function_info.clear();
}

Function_Info*
CaCUDALib_CUDA_Driver_Manager::function_info_get(const string func_name)
{
  Function_Info* const fi = &mi->functions[func_name];

  if ( !fi->mi ) fi->mi = mi; 

  return fi;
}

CUfunction
CaCUDALib_CUDA_Driver_Manager::function_load(const string func_name)
{
  if ( !mi->module_loaded ) module_load();
  Function_Info* const fi = function_info_get(func_name);

  if ( !fi->inited )
    {
      fi->inited = true;
      CE( cuModuleGetFunction
          (&fi->cu_function, mi->cu_module, func_name.c_str() ) );
    }

  return fi->cu_function_get();
}

void
CaCUDALib_CUDA_Driver_Manager::var_to_device
(const char *var_name, void *var_host_addr, size_t var_host_size)
{
  ASSERTS( mi );
  if ( !mi->module_loaded ) module_load();
  CUdeviceptr var_dev_addr;
  size_t var_dev_size;
  CE( cuModuleGetGlobal
      (&var_dev_addr, &var_dev_size, mi->cu_module, var_name) );

  // If already on device return.
  if ( symbols[var_dev_addr] != "" ) return;

  symbols[var_dev_addr] = mi->module_name;

  ASSERTS( var_dev_size == var_host_size );
  CE( cuMemcpyHtoD( var_dev_addr, var_host_addr, var_dev_size ) );
}

void
CaCUDALib_CUDA_Driver_Manager::launch(void *args[],CCTK_ARGUMENTS)
{
  mi->launch(args,CCTK_PASS_CTOC);
}

static bool comp_name(Module_Info *a, Module_Info *b)
{
  return a->module_name <= b->module_name;
}

typedef list<Module_Info*> Mod_List;

int CaCUDALib_Report()
{
  CaCUDALib_CUDA_Driver_Manager* const dm = CaCUDALib_CUDA_driver_manager_get();
  if ( dm ) dm->report_print();
  return 0;
}

void
CaCUDALib_CUDA_Driver_Manager::report_print()
{
  DECLARE_CCTK_PARAMETERS;

  if ( opt_trace_gf == 'w' )
    {
      msg_info("Wrote trace file to %s",trace_gf_path.c_str());
    }

  if ( myproc != 0 ) return;
  if ( !compile_nvcc_path ) return;

  Chemora_CG* const cg = Chemora_CG_get();
  if ( cg ) cg->report_print();

  Mod_List mod_sorted;
  for ( Mod_Map::iterator it = modules.begin(); it != modules.end(); it++ )
    mod_sorted.push_back(&it->second);
  mod_sorted.sort( comp_name );

  pString msg;

  msg.sprintf("Dynamic Compilation Data\n");

  int num_cak_units = 0;

  for ( auto mii: mod_sorted )
    {
      Chemora_CG_Calc* const c =
        Chemora_CG_get_calc_try(mii->kc.thorn_name,mii->kc.kernel_name);
      if ( c ) continue;

      num_cak_units++;

      CaCUDA_Kernel_Launch_Parameters& klp = mii->klp;
      CaKernel_Kernel_Config& kc = mii->kc;
      CaKernel_Launch_Config& lc = mii->lc;
      pString stntxt;
#define ABBR(sn,sp) stntxt.sprintf( sn == sp ? "%d" : "%d/%d", sn, sp);
      ABBR(kc.sten_xn,kc.sten_xp); stntxt +=" x ";
      ABBR(kc.sten_yn,kc.sten_yp); stntxt +=" x ";
      ABBR(kc.sten_zn,kc.sten_zp);
#undef ABBR
      msg.sprintf
        (" %3d x%2d x%2d (/%2d/%2d) %s %s  %s\n",
         klp.tile_x, klp.tile_y, klp.tile_z, klp.tile_yy, klp.tile_zz,
         stntxt.s,
         lc.rationale.c_str(), mii->module_name.c_str());
    }
  if ( num_cak_units ) msg_info(msg.s);

  if ( use_kranc_c )
    {
      assert( report_total_et == 0 );
      assert( cg->first_thorn_init_time_s != 0 );
      report_total_et = 1e3 * ( time_wall_fp() - cg->first_thorn_init_time_s );
    }

  pStringF tot("Total measured execution time: %.3f ms\n", report_total_et);
  msg_info(tot.s);
}

void
CaCUDALib_driver_support_set_lc(CCTK_ARGUMENTS, CaKernel_Kernel_Config &kc)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const int DIMSIZE = 6;
  CCTK_INT bndsize[DIMSIZE];
  CCTK_INT is_ghostbnd[DIMSIZE];
  CCTK_INT is_symbnd[DIMSIZE];
  CCTK_INT is_physbnd[DIMSIZE];

  map<string,bool> wb;

  // Example of how to watch boundaries.
  //  wb["DEVICE_ML_BSSN_RHS_Dalpha_2_etc"] = true;
  //  wb["DEVICE_ML_BSSN_RHS_Dalpha_3_etc"] = true;


  const int rv = GetBoundarySizesAndTypes
    (cctkGH, DIMSIZE, bndsize, is_ghostbnd, is_symbnd, is_physbnd);
  assert( !rv );

  const bool show_b = wb[kc.kernel_name];
  if ( show_b )
    printf("Boundaries for %s with lsh %d/%d/%d\n  ",
           kc.kernel_name, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2]);

  for ( int d=0; d<3; d++ )
    {
      const int f = 2 * d; // f is for face.
      const bool want_before = is_physbnd[f] && kc.want_physbnd[f];
      const bool want_after = is_physbnd[f+1] && kc.want_physbnd[f+1];
      kc.bwid_n[d] = want_before ? 0 : bndsize[f];
      kc.bwid_p[d] = want_after ? 0 : bndsize[f+1];

      kc.i_stop[d] = cctk_lsh[d] - kc.bwid_p[d];
      kc.i_length[d] = kc.i_stop[d] - kc.bwid_n[d];
      if ( show_b )
        printf
          ("%d, %c%c%c%c %d-%3d (%3d); ",
           d, is_ghostbnd[f] ? 'g' : '-', is_symbnd[f] ? 's' : '-',
           kc.want_physbnd[f] ? 'w' : '-', is_physbnd[f] ? 'p' : '-',
           kc.bwid_n[d],kc.i_stop[d], kc.i_length[d]);
    }

  if ( show_b ) printf("\n");

  pString sten_err_msg;

#define FCHECK(sten,face,face_name)                                           \
  if ( kc.sten > kc.face )                                                    \
    sten_err_msg.sprintf                                                      \
      ("  Stencil %s of width %d extends beyond boundary of width %d.\n",     \
       face_name, kc.sten, kc.face );

  FCHECK(sten_xn,bwid_n[0],"lower x");
  FCHECK(sten_xp,bwid_p[0],"upper x");
  FCHECK(sten_yn,bwid_n[1],"lower y");
  FCHECK(sten_yp,bwid_p[1],"upper y");
  FCHECK(sten_zn,bwid_n[2],"lower z");
  FCHECK(sten_zp,bwid_p[2],"upper z");

# undef FCHECK

  if ( sten_err_msg > 0 )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Problem with stencil or physical boundary zone for kernel %s\n%s",
       kc.kernel_name, sten_err_msg.s);
}
