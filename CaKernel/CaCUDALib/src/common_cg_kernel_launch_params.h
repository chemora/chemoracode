#ifndef common_cg_kernel_launch_params_h
#define common_cg_kernel_launch_params_h

struct Stncl {
  char xn, xp, yn, yp, zn, zp;
  int x() { return xn + xp; }
  int y() { return yn + yp; }
  int z() { return zn + zp; }
};

inline void stncl_reset(Stncl &s)
{ s.xn = s.xp = s.yn = s.yp = s.zn = s.zp = 0; }

struct Tile {
  int x, y, z, yy, zz;
};


// Parameters visible to kernel.  Constants for dynamically compiled code.
//
// Instances are initialized in host code by the routines performing
// the kernel launch for static code, and launch.cc for dynamically
// compiled code. Each kernel declares this in its main routine as
// param and its value is either copied from a global version or from
// compile-time constants (prepared by the dynamic compilation code).
//
struct CaCUDA_Kernel_Launch_Parameters
{
  CCTK_INT cagh_iteration;
  CCTK_INT cagh_ni;
  CCTK_INT cagh_nj;
  CCTK_INT cagh_nk;
  CCTK_INT cagh_blocky;
  CCTK_REAL cagh_dx;
  CCTK_REAL cagh_dy;
  CCTK_REAL cagh_dz;
  CCTK_REAL cagh_dt;
  CCTK_REAL cagh_xmin;
  CCTK_REAL cagh_ymin;
  CCTK_REAL cagh_zmin;
  CCTK_REAL cagh_time;
  CCTK_INT cagh_boundsPhys;

  // Width of boundaries based on iteration preferences for this
  // kernel. If a face has a physical boundary and the kernel
  // requested iteration into that boundary, then the respective width
  // will be zero.
  //
  int bwid_xn, bwid_xp, bwid_yn, bwid_yp, bwid_zn, bwid_zp;

  // Lengths based on the local grid size and the boundary widths.
  // above.
  //
  int i_length_x, i_length_y, i_length_z;

  CCTK_INT tile_x, tile_y, tile_z;
  CCTK_INT tile_yy, tile_zz;  // Number of tile iterations.
  CCTK_INT block_size;
  CCTK_INT gf_use_offsets;
  CCTK_INT force_one_block_per_mp;
  int shared_gf_target; // Number of gf to make shared when auto selected.
};

#endif
