 /*@@
   @file      microbench.cu
   @date
   @author    Yue Hu
   @desc
   GPU micro-benchmarks to explore unreleased performance critical parameters:
   including: L2 cache, global memory, shared memory, floating point compute
   units.
   @enddesc
 @@*/

#include "microbench.cuh"
#define GPU_TIMER_INIT                        \
           float et;                          \
           float cycle;                       \
           cudaEvent_t t_start;               \
           cudaEvent_t t_end;                 \
           CR( cudaEventCreate(&t_start) );   \
           CR( cudaEventCreate(&t_end) );
#define GPU_TIMER_BEGIN                       \
           CR( cudaEventRecord(t_start,0) );
#define GPU_TIMER_END                         \
           CR( cudaEventRecord(t_end,0) );    \
           CR( cudaEventSynchronize(t_end) ); \
           CR( cudaEventElapsedTime(&et, t_start, t_end) ); \
           cycle = (*gi).clock_freq_hz * et * 1e-3;
#define N_RUNS 3

// 1. L2 cache and global memory
template<int N>
__global__ void
gmem_lat_kernel(const u_int64_t * __restrict__ in, u_int64_t * __restrict__ out)
{
  u_int64_t *p = (u_int64_t *) in[0];
#pragma unroll 512
  for( int i = 0; i < N; i++ ) p = (u_int64_t *) *p;
  out[0] = (u_int64_t) p;
}

double
mb_access_gmem_lat(CaCUDALib_GPU_Info* const gi, int mem_sz, int stride = 256)
{
  const int N = 1 << 15;
  const int grid = 1, block = 1;
  u_int64_t *in = (u_int64_t *) malloc(mem_sz);
  u_int64_t *out = (u_int64_t *) malloc(sizeof(u_int64_t));
  u_int64_t *d_in, *d_out;
  CR( cudaMalloc(&d_in, mem_sz) );
  CR( cudaMalloc(&d_out, sizeof(u_int64_t)) );

  // initialize array with address pointers for dependent memory access
  int i;
  for( i = 0; i < mem_sz - stride; i += stride )
    in[i / sizeof(u_int64_t)] = (u_int64_t)d_in + i + stride;
  in[i / sizeof(u_int64_t)] = (u_int64_t)d_in;

  GPU_TIMER_INIT;
  double cycle_min = 1e30;
  cudaMemcpy(d_in, in, mem_sz, cudaMemcpyHostToDevice);
  gmem_lat_kernel<N><<<grid,block>>>(d_in, d_out); // avoid icache miss
  for ( int i = 0; i < N_RUNS; i++ )
    {
      GPU_TIMER_BEGIN;
      gmem_lat_kernel<N><<<grid,block>>>(d_in, d_out);
      GPU_TIMER_END; // measured time in cycle is stored in variable "cycle"
      cycle_min = cycle < cycle_min ? cycle : cycle_min;
    }
  CR( cudaMemcpy(out, d_out, sizeof(u_int64_t), cudaMemcpyDeviceToHost) );
  free(in); free(out); CR( cudaFree(d_in) ); CR( cudaFree(d_out) );
  return cycle_min / N;
}

double
mb_get_l2cache_lat(CaCUDALib_GPU_Info* const gi)
{
  return mb_access_gmem_lat(gi, 0.5 * (*gi).l2_cache_sz_bytes);
}

double
mb_get_gmem_lat(CaCUDALib_GPU_Info* const gi)
{
  return mb_access_gmem_lat(gi, 16 * (*gi).l2_cache_sz_bytes);
}


// 2. Shared memory
template <int N, typename T>
__global__ void
smem_lat_kernel(u_int64_t * __restrict__ out, int stride)
{
  const int asize = ( 3 << 14 ) / sizeof(T);
  __shared__ T smem[asize];   // allocate 48KB shared memory
  union { T* p; size_t i ;} pun; pun.p = smem;
  T sami = pun.i;
  const int tid = threadIdx.x, nthd = blockDim.x;
  const int i_stride = nthd * stride;
  const int i_end = asize - 2 * i_stride;
  int i;
  // initialize array with address pointers for dependent memory access
  for ( i = 0; i < i_end; i += i_stride )
    smem[i + tid * stride] = sami + ( i + tid * stride + i_stride ) * sizeof(T);
  smem[i + tid * stride] = sami + ( tid * stride ) * sizeof(T);
  __syncthreads();

  T *j = (T *)smem[tid * stride];
#pragma unroll 32
  for(int i = 0; i < N; i++) j = (T *) *j;
  out[tid] = (u_int64_t) j;
}

template <typename T>
double
mb_get_smem_lat_sp_or_dp(CaCUDALib_GPU_Info* const gi, int stride)
{
  const int N = 1 << 16;
  const int grid = 1, block = 32;
  u_int64_t *out = (u_int64_t *) malloc(block * sizeof(u_int64_t));
  u_int64_t *d_out;
  CR( cudaMalloc(&d_out, block * sizeof(u_int64_t)));

  GPU_TIMER_INIT;
  double cycle_min = 1e30;
  // first call to avoid icache miss
  smem_lat_kernel<N,T><<<grid,block>>>(d_out, stride);
  for ( int i = 0; i < N_RUNS; i++ )
    {
      GPU_TIMER_BEGIN;
      smem_lat_kernel<N,T><<<grid,block>>>(d_out, stride);
      GPU_TIMER_END;
      cycle_min = cycle < cycle_min ? cycle : cycle_min;
    }
  CR( cudaMemcpy(out, d_out, block * sizeof(u_int64_t), cudaMemcpyDeviceToHost)
      );
  free(out); CR( cudaFree(d_out) );
  return cycle_min / N;
}

double
mb_get_smem_lat(CaCUDALib_GPU_Info* const gi, double stride)
{ return mb_get_smem_lat_sp_or_dp<u_int64_t>(gi,stride); }
double
mb_get_smem_lat(CaCUDALib_GPU_Info* const gi, float stride)
{ return mb_get_smem_lat_sp_or_dp<u_int32_t>(gi,stride); }

template <int N, bool sync>
__global__ void
smem_sync_kernel
(const u_int64_t * __restrict__ in, u_int64_t * __restrict__ out)
{
  __shared__ u_int64_t smem[6144];   // allocate 48KB shared memory
  const int tid = threadIdx.x, nthd = blockDim.x;
  u_int64_t res = 0;
#pragma unroll 32
  for ( int i = 0; i < N; i++ )
    {
      int s_addr = tid;
      int g_addr = i * nthd + s_addr;
      smem[s_addr] = in[g_addr];
      if (sync) __syncthreads();
      res |= smem[tid];
      out[nthd + tid] = res;
    }
}

double
mb_get_smem_sync(CaCUDALib_GPU_Info* const gi, int n_warp)
{
  const int N = 1 << 12;
  const int grid = 1, block = n_warp << 5;
  const int sz_in = ( N + 1 ) * block * sizeof(u_int64_t);
  const int sz_out = 2 * block * sizeof(u_int64_t);
  u_int64_t *out = (u_int64_t *) malloc (sz_out);
  u_int64_t *d_in, *d_out;
  CR( cudaMalloc(&d_in, sz_in) );
  CR( cudaMalloc(&d_out, sz_out) );

  GPU_TIMER_INIT;
  double time_wt, time_wo;
  smem_sync_kernel<N,1><<<grid,block>>>(d_in, d_out);  // avoid icache miss
  GPU_TIMER_BEGIN;
  for ( int i = 0; i < N_RUNS; i++ )
    smem_sync_kernel<N,1><<<grid,block>>>(d_in, d_out);
  GPU_TIMER_END;
  CR( cudaMemcpy(out, d_out, sz_out, cudaMemcpyDeviceToHost) );
  time_wt = cycle;

  smem_sync_kernel<N,0><<<grid,block>>>(d_in, d_out);
  GPU_TIMER_BEGIN;
  for ( int i = 0; i < N_RUNS; i++ )
    smem_sync_kernel<N,0><<<grid,block>>>(d_in, d_out);
  GPU_TIMER_END;
  CR( cudaMemcpy(out, d_out, sz_out, cudaMemcpyDeviceToHost) );
  time_wo = cycle;

  free(out); CR( cudaFree(d_in) ); CR( cudaFree(d_out) );
  return (time_wt - time_wo) / N / N_RUNS;
}


// 3. Floating point compute units
template <typename T, int N, FP_Type FP_TYPE>
__global__ void
fp_lat_kernel(const T * __restrict__ in, T * __restrict__ out)
{
  const int tid = threadIdx.x;
  T a, b, res;
  switch ( FP_TYPE )
    {
      case FP_MADD: a = 0.000001; b = 1.000001; res = 0.000001; break;
      case FP_RECIP:
      case FP_POW:  a = 1.000001; res = 0.999999; break;
      case FP_DIV:  a = 1.000001; res = 1e6; break;
      case FP_SIN:  res = 0.999999; break;
      case FP_SQRT:
      case FP_SQRT_RECIP: res = 3.1415926e20; break;
      default: assert(0);
    }

  switch ( FP_TYPE )
    {
      case FP_MADD:
#pragma unroll 256
        for ( int i = 0; i < N ; i++ ) res = res * b + a;
        break;
      case FP_RECIP:
        for ( int i = 0; i < N ; i++ ) res = 1 / res;
        break;
      case FP_DIV:
        for ( int i = 0; i < N ; i++ ) res = res / a;
        break;
      case FP_POW:
        for ( int i = 0; i < N ; i++ ) res = pow(res, a);
        break;
      case FP_SIN:
        for ( int i = 0; i < N / 2; i++ )
          {
            // sin and cos have same lat and thpt. use both to avoid res = 0
            res = sin(res);
            res = cos(res);
          }
        break;
      case FP_SQRT:
        for ( int i = 0; i < N ; i++ ) res = sqrt(res);
        break;
      case FP_SQRT_RECIP:
        for ( int i = 0; i < N ; i++ ) res = T(1.0) / sqrt(res);
        break;
      default: assert(0);
    }
  out[tid] = res;
}

template <typename T, FP_Type FP_TYPE>
double
mb_get_fp_lat(CaCUDALib_GPU_Info* const gi)
{
  const int N =
    FP_TYPE == FP_MADD ? 1 << 19 :
    FP_TYPE == FP_POW ? 1 << 14 : 1 << 15;
  const int grid = 1, block = 1;
  const int mem_sz = block * sizeof(T);
  T *in = (T *) malloc(mem_sz);
  T *out = (T *) malloc(mem_sz);
  T *d_in, *d_out;
  CR( cudaMalloc(&d_in, mem_sz) );
  CR( cudaMalloc(&d_out, mem_sz) );
  CR( cudaMemcpy(d_in, in, mem_sz, cudaMemcpyHostToDevice) );

  GPU_TIMER_INIT;
  double cycle_min = 1e30;
  fp_lat_kernel<T,N,FP_TYPE><<<grid,block>>>(d_in, d_out); // avoid icache miss
  for ( int i = 0; i < N_RUNS; i++ )
    {
      GPU_TIMER_BEGIN;
      fp_lat_kernel<T,N,FP_TYPE><<<grid,block>>>(d_in, d_out);
      GPU_TIMER_END; // measured time in cycle is stored in variable "cycle"
      cycle_min = cycle < cycle_min ? cycle : cycle_min;
    }
  CR( cudaMemcpy(out, d_out, mem_sz, cudaMemcpyDeviceToHost) );
  free(in); free(out); CR( cudaFree(d_in) ); CR( cudaFree(d_out) );
  return cycle_min / N;
}

template <typename T, int N, FP_Type FP_TYPE>
__global__ void
fp_thpt_kernel(const T * __restrict__ in, T * __restrict__ out)
{
  const int tid = threadIdx.x;
  if ( FP_TYPE == FP_MADD )
    {
      const T a = in[tid], b = out[tid], c = a + b, d = a - b;
      T res0 = 0, res1 = 0, res2 = 0, res3 = 0;
      T res4 = 0, res5 = 0, res6 = 0, res7 = 0;
      T res8 = 0, res9 = 0, res10 = 0, res11 = 0;
      T res12 = 0, res13 = 0, res14 = 0, res15 = 0;
      const int N_ = N >> 4;

      #pragma unroll 64
      for ( int i = 0; i < N_; i++ )
        {
          res0 = res0 * a + a;
          res1 = res1 * a + b;
          res2 = res2 * a + c;
          res3 = res3 * a + d;
          res4 = res4 * b + a;
          res5 = res5 * b + b;
          res6 = res6 * b + c;
          res7 = res7 * b + d;
          res8 = res8 * c + a;
          res9 = res9 * c + b;
          res10 = res10 * c + c;
          res11 = res11 * c + d;
          res12 = res12 * d + a;
          res13 = res13 * d + b;
          res14 = res14 * d + c;
          res15 = res15 * d + d;
        }
      res0 += res1; res2 += res3; res4 += res5; res6 += res7;
      res8 += res9; res10 += res11; res12 += res13; res14 += res15;
      res0 += res2; res4 += res6; res8 += res10; res12 += res14;
      res0 += res4; res8 += res12;
      out[tid] = res0 + res8;
    }
  else
    {
      T res0, res1, res2, res3, a;
      switch ( FP_TYPE )
        {
          case FP_RECIP:
          case FP_POW:
            a = 1.000001;
            res0 = res1 = res2 = res3 = 0.999999;
            break;
          case FP_DIV:
            a = 1.000001;
            res0 = res1 = res2 = res3 = 1e6;
            break;
          case FP_SIN:
            a = 1.000001;
            res0 = res1 = res2 = res3 = 0.999999;
            break;
          case FP_SQRT:
          case FP_SQRT_RECIP:
            res0 = res1 = res2 = res3 = 3.1415926e20;
            break;
          default: assert(0);
        }

      switch ( FP_TYPE )
        {
          case FP_RECIP:
            for ( int i = 0; i < N >> 2; i++ )
              {
                res0 = 1 / res0; res1 = 1 / res1;
                res2 = 1 / res2; res3 = 1 / res3;
              }
            break;
          case FP_DIV:
            for ( int i = 0; i < N >> 2; i++ )
              {
                res0 = res0 / a; res1 = res1 / a;
                res2 = res2 / a; res3 = res3 / a;
              }
            break;
          case FP_POW:
            for ( int i = 0; i < N >> 2; i++ )
              {
                res0 = pow(res0, a); res1 = pow(res1, a);
                res2 = pow(res2, a); res3 = pow(res3, a);
              }
            break;
          case FP_SIN:
            for ( int i = 0; i < N >> 3; i++ )
              {
                res0 = sin(res0); res1 = sin(res1);
                res2 = sin(res2); res3 = sin(res3);
                res0 = cos(res0); res1 = cos(res1);
                res2 = cos(res2); res3 = cos(res3);
              }
            break;
          case FP_SQRT:
            for ( int i = 0; i < N >> 2; i++ )
              {
                res0 = sqrt(res0); res1 = sqrt(res1);
                res2 = sqrt(res2); res3 = sqrt(res3);
              }
            break;
          case FP_SQRT_RECIP:
            for ( int i = 0; i < N >> 2; i++ )
              {
                res0 = T(1.0) / sqrt(res0); res1 = T(1.0) / sqrt(res1);
                res2 = T(1.0) / sqrt(res2); res3 = T(1.0) / sqrt(res3);
              }
            break;
          default: assert(0);
        }
      res0 += res1; res2 += res3;
      out[tid] = res0 + res2;
    }
}


template <typename T, FP_Type FP_TYPE>
double
mb_get_fp_thpt(CaCUDALib_GPU_Info* const gi)
{
  const int N =
    FP_TYPE == FP_MADD ? 1 << 19 :
    FP_TYPE == FP_POW ? 1 << 12 : 1 << 14;
  const int grid = 1, block = 1024;
  const int mem_sz = block * sizeof(T);
  T *in = (T *) malloc(mem_sz);
  T *out = (T *) malloc(mem_sz);
  T *d_in, *d_out;
  CR( cudaMalloc(&d_in, mem_sz) );
  CR( cudaMalloc(&d_out, mem_sz) );
  CR( cudaMemcpy(d_in, in, mem_sz, cudaMemcpyHostToDevice) );

  GPU_TIMER_INIT;
  double cycle_min = 1e30;
  fp_thpt_kernel<T,N,FP_TYPE><<<grid,block>>>(d_in, d_out); // avoid icache miss
  for ( int i = 0; i < N_RUNS; i++ )
    {
      GPU_TIMER_BEGIN;
      fp_thpt_kernel<T,N,FP_TYPE><<<grid,block>>>(d_in, d_out);
      GPU_TIMER_END; // measured time in cycle is stored in variable "cycle"
      cycle_min = cycle < cycle_min ? cycle : cycle_min;
    }
  CR( cudaMemcpy(out, d_out, mem_sz, cudaMemcpyDeviceToHost) );
  free(in); free(out); CR( cudaFree(d_in) ); CR( cudaFree(d_out) );
  return ( N + 0.0 ) * block / cycle_min;
}

// define functions below since all template related functions have to be in
// the same file
double mb_get_sp_madd_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<float, FP_MADD>(gi);
}
double mb_get_dp_madd_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<double, FP_MADD>(gi);
}
double mb_get_sp_madd_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<float, FP_MADD>(gi);
}
double mb_get_dp_madd_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<double, FP_MADD>(gi);
}

double mb_get_sp_recip_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<float, FP_RECIP>(gi);
}
double mb_get_dp_recip_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<double, FP_RECIP>(gi);
}
double mb_get_sp_recip_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<float, FP_RECIP>(gi);
}
double mb_get_dp_recip_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<double, FP_RECIP>(gi);
}

double mb_get_sp_div_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<float, FP_DIV>(gi);
}
double mb_get_dp_div_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<double, FP_DIV>(gi);
}
double mb_get_sp_div_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<float, FP_DIV>(gi);
}
double mb_get_dp_div_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<double, FP_DIV>(gi);
}

double mb_get_sp_pow_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<float, FP_POW>(gi);
}
double mb_get_dp_pow_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<double, FP_POW>(gi);
}
double mb_get_sp_pow_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<float, FP_POW>(gi);
}
double mb_get_dp_pow_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<double, FP_POW>(gi);
}

double mb_get_sp_sin_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<float, FP_SIN>(gi);
}
double mb_get_dp_sin_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<double, FP_SIN>(gi);
}
double mb_get_sp_sin_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<float, FP_SIN>(gi);
}
double mb_get_dp_sin_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<double, FP_SIN>(gi);
}

double mb_get_sp_sqrt_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<float, FP_SQRT>(gi);
}
double mb_get_dp_sqrt_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<double, FP_SQRT>(gi);
}
double mb_get_sp_sqrt_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<float, FP_SQRT>(gi);
}
double mb_get_dp_sqrt_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<double, FP_SQRT>(gi);
}

double mb_get_sp_sqrt_recip_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<float, FP_SQRT_RECIP>(gi);
}
double mb_get_dp_sqrt_recip_lat(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_lat<double, FP_SQRT_RECIP>(gi);
}
double mb_get_sp_sqrt_recip_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<float, FP_SQRT_RECIP>(gi);
}
double mb_get_dp_sqrt_recip_thpt(CaCUDALib_GPU_Info* const gi) {
  return mb_get_fp_thpt<double, FP_SQRT_RECIP>(gi);
}
