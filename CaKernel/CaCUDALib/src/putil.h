 /*@@  -*- c++ -*-
   @file      putil.h
   @date      Wed Jun  6 15:49:25 2012
   @author    David Koppelman
   @desc 
              Misc utilities.
   @enddesc 
 @@*/

#ifndef PUTIL_H
#define PUTIL_H
#include <string>
#include <vector>

using namespace std;

class pSplit {
public:
  pSplit(const char *str, char sep, const char *empty_val_p = NULL):
    empty_val(empty_val_p)
  {
    const char *start = str;
    if ( start )
      while ( *start )
        {
          const char *ptr = start;
          while ( *ptr && *ptr != sep ) ptr++;
          if ( start != ptr )
            {
              queue.push_back(string(start,ptr-start));
            }
          if ( !*ptr ) break;
          start = ++ptr;
        }
    next_elt = 0;
    elts = size();
  }

  operator bool () const { return next_elt < elts; }
  operator const char* ()
  {
    return next_elt == elts ? empty_val : queue[next_elt++].c_str();
  }

  string pop() { return next_elt == elts ? string(empty_val) : queue[--elts]; }

  int size() const { return queue.size(); }

private:
  const char *empty_val;
  int elts, next_elt;
  vector<string> queue;
};

#endif
