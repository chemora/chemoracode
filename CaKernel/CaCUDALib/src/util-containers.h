// -*- c++ -*-

#ifndef UTIL_CONTAINERS_H
#define UTIL_CONTAINERS_H

#include <vector>
#include <deque>
#include <map>

#if __cplusplus >= 201103L
#define CHEMORA_EXPLICIT explicit
#else
#define CHEMORA_EXPLICIT
#endif

using namespace std;

template <typename T>
class pVectorI : public vector<T*> {
public:
  pVectorI():vector<T*>(){}
  pVectorI(T* nd):vector<T*>(){ vector<T*>::push_back(nd); }
  void operator = (T* nd) { vector<T*>::clear(); vector<T*>::push_back(nd); }
  void operator += (T* nd) { vector<T*>::push_back(nd); }
  void operator += (pVectorI<T> v) {
    vector<T*>::insert(vector<T*>::end(),v.begin(),v.end());
  }
  pVectorI<T> operator + (pVectorI<T> v) {
    pVectorI<T> r = *this; r += v;
    return r;
    }
  operator size_t () { return vector<T*>::size(); }
  CHEMORA_EXPLICIT operator bool () { return !vector<T*>::empty(); }
  T* pop()
  {
    if ( vector<T*>::empty() ) return NULL;
    T* const rv = vector<T*>::back();
    vector<T*>::pop_back();
    return rv;
  }
  bool member(T* nd)
  {
    for ( int i=0; i<vector<T*>::size(); i++ ) 
      if ( vector<T*>::at(i) == nd ) return true;
    return false;
  }
};

template <typename T>
class pDequeI : public deque<T*> {
public:
  pDequeI():deque<T*>(){}
  pDequeI(T* nd):deque<T*>(){ deque<T*>::push_back(nd); }
  void operator = (T* nd) { deque<T*>::clear(); deque<T*>::push_back(nd); }
  void operator += (T* nd) { deque<T*>::push_back(nd); }
  void operator += (pDequeI<T> v) {
    deque<T*>::insert(deque<T*>::end(),v.begin(),v.end());
    }
  void operator += (vector<T*> v) {
    deque<T*>::insert(deque<T*>::end(),v.begin(),v.end());
    }
  pDequeI<T> operator + (pDequeI<T> v) {
    pDequeI<T> r = *this; r += v;
    return r;
    }
  operator size_t () { return deque<T*>::size(); }
  CHEMORA_EXPLICIT operator bool () { return !deque<T*>::empty(); }
  T* pop()
  {
    if ( deque<T*>::empty() ) return NULL;
    T* const rv = deque<T*>::back();
    deque<T*>::pop_back();
    return rv;
  }
  T* popf()
  {
    if ( deque<T*>::empty() ) return NULL;
    T* const rv = deque<T*>::front();
    deque<T*>::pop_front();
    return rv;
  }
  T* peek()
  {
    return deque<T*>::empty() ? NULL : deque<T*>::back();
  }
  bool member(T* nd)
  {
    for ( int i=0; i<deque<T*>::size(); i++ )
      if ( deque<T*>::at(i) == nd ) return true;
    return false;
  }
};


template <typename T>
class pVector : public vector<T> {
public:
  pVector():vector<T>(){};
  pVector(size_t sz):vector<T>(sz){};
  void operator += (const T& nd) { vector<T>::push_back(nd); }
  void operator += (vector<T> v)
    {
      vector<T>::insert(vector<T>::end(),v.begin(),v.end());
    }
  pVector<T> operator + (pVector<T> v) {
    pVector<T> r = *this; r += v;
    return r;
  }
  bool pop(T& val)
  {
    if ( vector<T>::empty() ) return false;
    val = vector<T>::back();
    vector<T>::pop_back();
    return true;
  }
  T pop()
  {
    if ( vector<T>::empty() ) return T(0);
    T val = vector<T>::back();
    vector<T>::pop_back();
    return val;
  }
};

template <typename T>
class pVectorI_Iter {
public:
  // Make sure this is some kind of error.
  pVectorI_Iter(const pVectorI_Iter<T>& iter);

  pVectorI_Iter(vector<T*>& nodesp):nodes(nodesp),size(nodes.size()),idx(-1){}

  // Explicit keywords prevents invoking this member in integer expressions.
  CHEMORA_EXPLICIT operator bool () {
    const bool rv = ++idx < size;
    if ( !rv ) assert( size <= nodes.size() );
    c = rv ? nodes[idx] : NULL;
    return rv;
  }
  operator int () { return idx; }
  operator T* ()  { return c; }
  T* curr() { return c; }
  T* operator -> () { return c; }
private:
  vector<T*>& nodes;
  T *c;
  const int size;
  int idx;
};

template <typename T>
class pVector_Iter {
public:
  // Make sure this is some kind of error.
  pVector_Iter(const pVector_Iter<T>& iter);

  pVector_Iter(vector<T>& nodesp):
    nodes(nodesp),size(nodes.size()),in_range(false),idx(-1){}
  CHEMORA_EXPLICIT operator bool () { return in_range = ++idx < size; }
  T& curr() { return in_range ? nodes[idx] : z; }
private:
  vector<T>& nodes;
  T z;
  const int size;
  bool in_range;
  int idx;
};

template <typename T>
class pMap_Str_Obj_Iter {
  typedef map<string,T> pMap_SOI_Elt;
public:
  pMap_Str_Obj_Iter(pMap_SOI_Elt& gfm):gfmi(gfm.begin()),gfmend(gfm.end()){}
  CHEMORA_EXPLICIT operator bool ()
  {
    if ( gfmi == gfmend ) return false;
    current = &gfmi->second;
    gfmi++;
    return true;
  }
  operator T* () { return current; }
  T* operator -> () { return current; }
private:
  typename pMap_SOI_Elt::iterator gfmi, gfmend;
  T *current;
};

template <typename K, typename T>
class pMMapI : public multimap<K,T*> {
public:
  typedef multimap<K,T*> MM;
  typedef typename MM::iterator It;
  T* lookup(K keyp)
  {
    It iter = find(keyp);
    return iter == MM::end() ? NULL : iter->second;
  }
  It at(int idx)
  {
    // Intended for debugging.
    It it = MM::begin();
    for ( int i=0; i<idx; i++ ) it++;
    return it;
  };
  K key(int idx) { return at(idx)->first; }
  K key() {
    typename MM::iterator iter = --MM::end();
    return iter->first;
  }
  T* val(int idx) { return at(idx)->second; }
  T* peek() { return peek_or_pop(false); }
  T* shift()
  {
    if ( MM::empty() ) return NULL;
    typename MM::iterator iter = MM::begin();
    T* const rv = iter->second;
    MM::erase(iter);
    return rv;
  }
  T* pop() { return peek_or_pop(true); }
  void insert(K keyp, T *nd) {
    MM::insert( std::pair<K,T*>(keyp,nd) );
  }
  operator int () { return MM::size(); }
  operator vector<T*> ()
  {
    vector<T*> rv;
    for ( It it = MM::begin(); it != MM::end(); ++it ) rv.push_back(it->second);
    return rv;
  }
  operator pVectorI<T> ()
  {
    pVectorI<T> rv;
    for ( It it = MM::begin(); it != MM::end(); ++it ) rv.push_back(it->second);
    return rv;
  }

private:
  T* peek_or_pop(bool do_pop)
  {
    if ( MM::empty() ) return NULL;
    typename MM::iterator iter = --MM::end();
    T* const rv = iter->second;
    if ( do_pop ) MM::erase(iter);
    return rv;
  }
};

template <typename K, typename T>
class pMMapI_Iter {
public:
  pMMapI_Iter(pMMapI<K,T>& imp) :im(imp), size(imp.size()), it(imp.begin()){}

  CHEMORA_EXPLICIT operator bool () {
    if ( it == im.end() ) { assert( size <= im.size() );  return false; }
    c = it++;
    return true;
  }
  operator T* ()  { return c->second; }
  operator int () { return pos(); }
  T* curr() { return c->second; }
  T* operator -> () { return c->second; }
  K key() { return c->first; }
  int pos() { return std::distance(im.begin(),c); }

private:
  pMMapI<K,T>& im;
  const int size;
  typename pMMapI<K,T>::iterator it, c;
};

#endif
