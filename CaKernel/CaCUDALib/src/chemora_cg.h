 /*@@  -*- c++ -*-
   @file      chemora_cg.h
   @date
   @author    David Koppelman
   @desc
   CUDA code generation based on EDL- or Kranc- generated intermediate
   format computation kernels.
   @enddesc
 @@*/

#ifndef chemora_cg_h
#define chemora_cg_h

#include <cuda.h>
#include <map>
#include <vector>
#include <deque>
#include <string>
#include "util-containers.h"

#include "launch.h"
#include "common_cg_kernel_launch_params.h"

enum CStncl_Axes_Enum { SX_x = 0, SX_y, SX_z, SX_pad, SX_SIZE };

typedef int8_t Offset; // MODIFY UNION IN Offsets BEFORE CHANGING SIZE.

struct Offsets {
  Offsets():packed(0){};
  Offsets(Offset offsets[SX_SIZE])
  { for ( int ax=0; ax<SX_SIZE; ax++ ) a[ax]=offsets[ax]; };
  Offsets(Offset di, Offset dj, Offset dk):i(di),j(dj),k(dk),pad(0){};
  union {
    u_int32_t packed;
    // Hmm, do I need to worry about byte ordering?
    Offset a[SX_SIZE];
    struct { Offset i, j, k, pad; };
  };
  Offset& operator [] (int idx) { return a[idx]; };
  bool operator == (Offsets o) const { return packed == o.packed; };
};

struct CStncl {
  CStncl(CStncl stncl, Offsets ref)
    {
      for ( int ax=0; ax<SX_SIZE; ax++ )
        {
          omin[ax] = stncl.omin[ax] - ref[ax];
          omax[ax] = stncl.omax[ax] - ref[ax];
        }
    }
  CStncl() { reset(); }
  static const Offset o_limit_min = -100;
  static const Offset o_limit_max = 100;
  union {
    Offset omin[SX_SIZE];
    struct { Offset imin, jmin, kmin, pmin; };
  };
  union {
    Offset omax[SX_SIZE];
    struct { Offset imax, jmax, kmax, pmax; };
  };
  void include(Offsets offset)
    {
      for ( int ax=0; ax<SX_SIZE; ax++ )
        {
          if ( offset[ax] < omin[ax] ) omin[ax] = offset[ax];
          if ( offset[ax] > omax[ax] ) omax[ax] = offset[ax];
        }
    }
  void include(CStncl stncl)
    {
      include(stncl.omin);
      include(stncl.omax);
    }
  void unset_to_zero()
    {
      for ( int ax=0; ax<SX_SIZE; ax++ )
        {
          if ( omin[ax] == o_limit_max ) omin[ax] = 0;
          if ( omax[ax] == o_limit_min ) omax[ax] = 0;
        }
    }
  void reset()
    {
      for ( int ax=0; ax<SX_SIZE; ax++ )
        {
          omin[ax] = o_limit_max;
          omax[ax] = o_limit_min;
        }
    }
  int halo_width(int ax) { return omax[ax]-omin[ax]; }
  int x() { return imax-imin; }
  int y() { return jmax-jmin; }
  int z() { return kmax-kmin; }
};


enum Array_Axes_Enum
  { AX_0 = 0, AX_x = 1,  AX_y = 2, AX_xy = 3,
    AX_z = 4, AX_xz = 5, AX_yz = 6, AX_xyz = 7, AX_SIZE = 8 };
typedef u_int8_t Array_Axes;

enum Node_Operation
{ 
  NO_Unset,
  NO_Literal, NO_Identity,
  NO_Switch, NO_If,
  // Result of operations below must be a Boolean.
  NO_BOOLEAN_START,
  NO_And, NO_Or, NO_Not, // Boolean sources.
  NO_Less, NO_LessEqual,
  NO_Equal, NO_NotEqual,
  NO_GreaterEqual,
  NO_Greater,
  NO_BOOLEAN_END,
  // Result of operations above must be a Boolean.
  NO_CopySign,
  NO_Abs, NO_Max, NO_Min, NO_Negate,
  NO_Sum, NO_Product,
  NO_Reciprocal, NO_Divide, NO_SquareRoot,
  NO_Power, NO_Sin, NO_Cos,
  NO_Function,
  NO_GFO_Load, NO_GF_Store,
  NO_Opaque,
  NO_SIZE
};

const char* const node_operation_str[] =
{
  "Unset",
  "Literal", "Identity",
  "Switch", "If",
  // Result of operations below must be a Boolean.
  "BOOLEAN_START",
  "And", "Or", "Not",
  "Less", "LessEqual",
  "Equal", "NotEqual",
  "GreaterEqual",
  "Greater",
  "BOOLEAN_END",
  // Result of operations above must be a Boolean.
  "CopySign",
  "Abs", "Max", "Min", "Negate",
  "Sum", "Product",
  "Reciprocal", "Divide", "SquareRoot",
  "Power", "Sin", "Cos",
  "Function",
  "GFO_Load", "GF_Store",
  "Opaque",
  "SIZE"
};

class Static_Op_Info {
public:
  Static_Op_Info():no(NO_Unset),lt_set(false),is_commutative(false){};
  string kranc_name;
  Node_Operation no;
  string ptx_mnemonic;
  string c_op_or_func; // Operator or function name.
  bool is_c_operator;
  bool lt_set;
  bool is_commutative;
  double latency;     // Cycles
  double throughput;  // Operations per cycle per multiprocessor.
};


class Module_Info;
class Function_Info;
struct Move_Proposal;
struct Grid_Function;
struct Buffer_Group;
struct Kernel_User_Setting;
class Chemora_CG_Calc;
class Chemora_CG_Module_Data;
class ET_Components;
class pTable;

using namespace std;

typedef u_int32_t A_Vector_Type; // Must be unsigned.
typedef u_int64_t R_Vector_Type; // Must be unsigned.

class KNOIter {
public:
  KNOIter(A_Vector_Type avp):av(avp){}
  operator bool ();
  operator int () { return curr; }
  operator long int () { return curr; }
private:
  A_Vector_Type av;
  int curr;
};

class RVIter {
public:
  RVIter(R_Vector_Type avp):av(avp){}
  operator bool ();
  operator int () { return curr; }
  operator long int () { return curr; }
private:
  R_Vector_Type av;
  int curr;
};

enum NType
{
  NT_Unknown,
  NT_DO_Info,
  NT_Special,
  NT_Parameter,
  NT_Constant_Expr,  // A lhs symbol is assigned with the value.
  NT_Literal,        // Just the value, nothing to assign.

  NT_GFO_Load,    // Grid function load, maybe non-zero offset.
  NT_Expression,
  NT_Switch,

  NT_If,
  NT_GF_Store,
  NT_SIZE
};

enum PTX_Reg_Type
{
  PT_b32, PT_b64,
  PT_s16, PT_s32, PT_s64,
  PT_f32, PT_f64,
  PT_pred,
  PT_SIZE
};

inline bool rt_is_fp(PTX_Reg_Type rt) { return rt == PT_f32 || rt == PT_f64; }

const char* const
ptx_rr_str[]  =   { "b",   "bd",  "rh",   "r",   "rd",  "f",   "fd",  "p" };
const char* const
ptx_rr_type[] =   { "b32", "b64", "s16",  "s32", "s64", "f32", "f64", "pred" };
const int
ptx_rr_bytes[] =  {  4,     8,     2,      4,     8,     4,     8,     1 };
const char* const
ptx_rr_ctype[] =  { "u_int32_t", "u_int64_t", "int16_t", "int32_t", "int64_t",
                    "float", "double", "bool" };

struct PTX_RR
{
  PTX_RR(){reset();}
  PTX_RR(bool literal_val);
  void reset();
  PTX_Reg_Type rt;
  PTX_RR *orig;
  int global_num; // Unique across types.
  int num;        // Unique within a type.
  void maybe_unused_set() { use(); }
  int use() { return orig ? orig->uses++ : 0; }
  int uses;  // Make sure that just_once called <= 1 time.
  bool single_assign;
  bool ctc;
  bool ik_load;
  bool ctc_in_vali() const { return rt < PT_f32 || rt > PT_f64; }
  union { double ctc_val; int64_t ctc_vali; };
  string txt;
  const char* str() { return txt.c_str(); }
  operator bool () const { return num >= 0; }
  PTX_RR& just_once() { assert( !use() ); return *this; }
};

typedef pVectorI<PTX_RR> PTX_RRs;

struct PTX_RR_Addr_Base;

struct PTX_RR_Base_Extension {
  size_t addr;
  PTX_RR reg;   // Value in reg is addr + ptxr_thd_offset.
  int code_path;
  bool stale;
};

struct PTX_RR_Addr_Base {
  PTX_RR ptxr_thd_offset;
  int offset_scale_factor;
  Array_Axes_Enum axes;
  pVector<PTX_RR_Base_Extension> base_extensions;
};

struct PTX_Reg_Pool_Item {
  int grno; // Global register number.
  int seq_num; // Must be > 0.
  int hopovers_remaining;
};


class Chemora_CG;
class Chemora_CG_Thorn;
class Chemora_CG_Calc;
class Chemora_CG_Calc_Mapping;
struct Node;

class PTX_Reg_Pool {
public:
  void reset(int pool_idxp, int max_livep, Chemora_CG_Calc_Mapping *m);
  void tree_check(PTX_Reg_Pool *rp_curr);
  bool check(const PTX_RR_Base_Extension& be) const;
  bool check(const PTX_RR& r) const;
  int check_seq(int global_reg_num) const; // Return 0 if not found.
  void use(const PTX_RR& r);
  const PTX_RR_Base_Extension use(const PTX_RR_Base_Extension& be);
  void cull();
  void rnew(const PTX_RR_Base_Extension& be);
  void rnew(const PTX_RR& r);
  void rinsert(int grno);
  int occ() const { return seq_to_grno.size(); }
  int n_reg_defs;
  int pool_idx;

private:
  Chemora_CG_Calc_Mapping *m;
  void seq_to_grno_rebuild();
  void inherit(const PTX_Reg_Pool& rp);
  int max_live;
  int seq_num_next;
  map <int,int> seq_to_grno; // Sequence Number -> Global Register Number
  map <int,PTX_Reg_Pool_Item> rp_items; // Indexed by reg.global_num.
  map<PTX_Reg_Pool*,int> children;
};

typedef bool PTX_Insn_Emit;
static const PTX_Insn_Emit EMIT = true;

class PTX_Insn_Text : public string {
public:
  PTX_Insn_Text():string(){};
  PTX_Insn_Text(string s):string(s){};
  PTX_Insn_Text(const char* s):string(s){};
};

class PTX_Insn_Tail_Comment : public string {
public:
  PTX_Insn_Tail_Comment():string(){};
  PTX_Insn_Tail_Comment(string s):string(s){};
  PTX_Insn_Tail_Comment(const char* s):string(s){};
};

class PTX_Insn_Addr_Offs {
public:
  PTX_Insn_Addr_Offs(PTX_RR r, int64_t o):base_reg(r),offset(o){};
  PTX_RR base_reg;
  int64_t offset;
};

#define TCOMMENT(comment) PTX_Insn_Tail_Comment(comment)

class PTX_Insn
{
  public:
  PTX_Insn(Chemora_CG_Calc_Mapping *mp);
  Chemora_CG_Calc_Mapping *m;
  PTX_Insn& predicate(PTX_RR reg, bool negate);
  PTX_Insn& npredicate(PTX_RR reg);
  PTX_Insn& operator << (Node *);
  PTX_Insn& operator << (PTX_RR reg);
  PTX_Insn& operator << (double val);
  PTX_Insn& operator << (void *val);
  PTX_Insn& operator << (int val);
  PTX_Insn& operator << (int64_t val);
  PTX_Insn& operator << (size_t val);
  PTX_Insn& operator << (const char* opaque); // Treated as an operand.
  PTX_Insn& operator << (const PTX_Insn_Addr_Offs ao);
  PTX_Insn& operator << (const string& opaque); // Treated as an operand.
  PTX_Insn& operator << (PTX_Insn_Text text); // Text inserted, no other ch.
  PTX_Insn& operator << (PTX_Insn_Tail_Comment tail_comment);
  PTX_Insn& operator << (PTX_Insn_Emit dummy);
  operator PTX_RR();
  void emit();

  void operand_insert_start();

  pVector<PTX_RR> srcs;
  PTX_RR dest;
  // AOTW src_types is only used for operands with a non-fixed type, such
  // as the operand type in add. It is incorrect in many cases.
  PTX_Reg_Type src_types[4];  // Used for formatting immediates.

  int text_unindent_amt; // Used for predication indicators, directives, etc.
  int text_indent_adj;   // For ptx, set to a tab char.
  string text_inter_op; // Such as operators.
  string text_close;    // Such as closing parenthesis on call.
  string text;
  string tail_comment;
  int operands;
  int bidx; // Index into ptx_body_items vector.
  bool comma;
  bool emitted;
  bool predicate_present;
  // If true, instruction should not execute due to a literal predicate.
  bool predicated_off;
};


struct Pattern {
  Pattern(){};
  u_int8_t pattern, pattern_true;
  Offsets ref;
  Grid_Function *gf;
  CStncl stncl, stncl_true;
  u_int8_t n_shape[8];
};

inline int
pattern_pos_compute(Offsets offsets)
{
  int offset_axes = 0;
  for ( int a=0; a<3; a++ ) if ( offsets[a] ) offset_axes += 1 << a;
  return offset_axes;
}

inline u_int8_t
pattern_compute(Offsets offsets)
{
  return 1 << pattern_pos_compute(offsets);
}

inline int
pattern_pos_compute(Offsets offsets, Offsets ref_offsets)
{
  Offsets oo;  // Offset offsets.
  for ( int a=0; a<3; a++ ) oo[a] = offsets[a] - ref_offsets[a];
  return pattern_pos_compute(oo);
}

inline u_int8_t
pattern_compute(Offsets offsets, Offsets ref_offsets)
{
  return 1 << pattern_pos_compute(offsets,ref_offsets);
}

enum Pattern_Type
  { PT_No_Buffering, PT_Block_Y, PT_Block_Z,
    PT_Plus_Y, PT_Plus_Z, PT_Pin_Y, PT_Pin_Z };

inline bool pattern_type_is(u_int8_t pattern, Pattern_Type pcheck)
{
#define IA(m) ( 1 << (m) )
  const bool y_solo = pattern & IA(2);
  const bool y_comp = pattern & ( IA(3) | IA(6) | IA(7) );
  const bool z_solo = pattern & IA(4);
  const bool z_comp = pattern & ( IA(5) | IA(6) | IA(7) );
#undef IA
  if ( pcheck == PT_Plus_Y ) return y_solo && !y_comp;
  if ( pcheck == PT_Plus_Z ) return z_solo && !z_comp;
  if ( pcheck == PT_Pin_Y ) return y_solo && !( pattern & ~0x5 );
  if ( pcheck == PT_Pin_Z ) return z_solo && !( pattern & ~0x11 );
  assert( false );
  return false;
}


typedef pVectorI<Node> Nodes;
typedef pDequeI<Node> Nodesd;

enum Node_List_Occupancy {
  NLO_red_check = 1,
  NLO_ik_check = 2,
  NLO_red_change = 4
};

typedef map<string, Node*> Node_Map_Plain;
typedef map<int, Node*> Node_IMap_Plain;

class Node_IMap : public Node_IMap_Plain {
public:
  Node* lookup(int key)
  {
    Node_IMap_Plain::iterator iter = find(key);
    return iter == end() ? NULL : iter->second;
  }
  Node* peek() { return peek_or_pop(false); }
  Node* pop() { return peek_or_pop(true); }
private:
  Node* peek_or_pop(bool do_pop)
  {
    if ( empty() ) return NULL;
    Node_IMap_Plain::iterator iter = --end();
    Node* const rv = iter->second;
    if ( do_pop ) erase(iter);
    return rv;
  }
};


typedef Node_IMap::iterator NIMap_Iter;

typedef pMMapI<int,Node> Node_IMMap;
typedef pMMapI_Iter<int,Node> NIMIter;

typedef pMMapI<u_int64_t,Node> Node_U64MMap;
typedef pMMapI_Iter<u_int64_t,Node> NU64MIter;

class Node_Map : public Node_Map_Plain {
public:
  Node* lookup(string name)
  {
    Node_Map_Plain::iterator iter = find(name);
    return iter == end() ? NULL : iter->second;
  }
};

extern int prop_serial;

enum Node_New_Action {
  NN_New,        // Don't expect to find another node with same name.
  NN_Redefine,   // Not the first assignment to var with this name.
};

struct Node {
  Node(){ marker = 1; marker_mp = 1; };
  string lhs_name;
  // Disambiguated name. For when var assigned more than once.  NType ntype;
  string lhs_name_dis;
  NType ntype;
  Node_Operation no;
  string rhs_c_code;
  string c_code;
  string producer_c_code, consumer_c_code;
  string lhs_type;
  string func_name;
  int idx;                      // Sequential numbering of nodes.
  // Index of original node. Value is the same as idx except for
  // those nodes created in emit_code_kernel.
  int oidx;
  int mem_accesses;             // Number of loads or stores.
  u_int8_t offset_axes;         // Bit vector of offset axes.
  u_int8_t pattern;             // Bit vector of offset combinations.
  Offsets offset;
  int di() { return offset.i; }
  int di(Offsets ref) { return offset.i - ref[SX_x]; }
  int dj() { return offset.j; }
  int dj(Offsets ref) { return offset.j - ref[SX_y]; }
  int dk() { return offset.k; }
  int dk(Offsets ref) { return offset.k - ref[SX_z]; }
  int de(int axis) { return axis ? dk() : dj(); }
  int df(int axis) { return axis ? dj() : dk(); }

  // Based on emitted code, so zero if sources compile-time constants.
  double latency;    // Cycles.

  // Reciprocal of throughput in ops / cyc for *each* insn emitted.
  // That is, total issue usage is ops * issue_usage.
  double issue_usage;
  int ops;           // Number of emitted instructions.

  Nodes sources;
  Node *predecessor, *successor;
  int name_disambig;  // 0, first definition of lhs_name; 1, 2nd def, ...
  union { Node *switch_var, *leader; };
  Node_IMap switch_src;
  Nodes dests;
  Grid_Function *gf;

  bool visited;
  bool loop_body;

  // If true, definitely yes but value many not be currently available.
  // If false, then may or may not be a compile time constant.
  bool compile_time_constant;
  bool ctc_val_set;  // If true, is a compile time constant and val avail.
  bool identity; // dest = source[identity_src];
  bool cse_remapped;   // Not in name_to_node because of cse.
  bool opt_assoc_combined;
  bool opt_replaced;   // Not in name_to_node because of some other opt.
  char identity_src;
  CCTK_REAL ctc_val, ctc_val2;

  bool reducible;

  //
  /// Values below are valid only for a particular mapping.
  //
  char implied_assignment_kno;  // Valid only within emit_code_kernel.
  char root_kno;  // Kernel at which reduction nodes computes final answer.

  //
  /// Values used during constructions, code-generation, etc.
  //
  static const int KNO_OVER = 1 << 30;


  bool emittable; // Not a ctc, used in current kernel.

  int marker;
  int marker_mp;
  bool marker_check(int m) { return m == marker; }
  bool marker_check_mp(int m) { return m == marker_mp; }
  bool marker_set(int m)
    {
      if ( m == marker ) return true;
      marker = m;
      return false;
    }
  bool marker_set_mp(int m)  // Used for move proposal calculations.
    {
      if ( m == marker_mp ) return true;
      marker_mp = m;
      ik_mp_cv_version = -2;
      remove_vector = 0;
      dests_here_kno = KNO_OVER;
      dests_here = -1;
      examined_from = false;
      examined_to = 0;
      occ = 0;
      dce_kno = 255;
      delta_ik = 0;
      serial = prop_serial++;
      return false;
    }
  int serial;
  int dests_here;
  A_Vector_Type remove_vector;
  A_Vector_Type red_fp_update; // Only used for consistency checks.
  int red_fp_update_marker_mp;
  char occ; // Vector indicating occupancy in various lists.
  int sources_unscheduled;
  int dests_unemitted;
  char examined_to;
  bool examined_from;
  char delta_ik;
  char red_src_pos; // Valid only for dest node being emitted.
  unsigned char dce_kno;
  int dests_here_kno;

  union { int ik_mp_cv_version; int t_sched; };
  union { int ik_cv_version; int ss_root_idx; };
  A_Vector_Type ik_consumers_vector;
  A_Vector_Type ik_mp_consumers_vector;

  int ss_dests_unemitted;
  int ss_dest_pos_max;
  int ss_num_live_regs;
  R_Vector_Type sched_set_vector;

  PTX_RR ptxr;
  void *ik_device_addr;
  bool emitted;

  bool is_sink() { return ntype == NT_GF_Store; }

  // Constant regardless of If or Switch outcomes.
  bool is_strong_compile_time_constant() {
    return ntype == NT_Parameter
      || ntype == NT_Constant_Expr || no == NO_Literal; }

  bool assigns_lhs()
    {
      switch ( ntype ) {
      case NT_Parameter: case NT_Constant_Expr: case NT_Expression:
      case NT_GFO_Load:
      case NT_Switch: case NT_If:
        return true;
      case NT_DO_Info: case NT_Special: case NT_GF_Store:
      case NT_Literal:
        return false;
      default:
        assert( false );
        return false;
      }
    }
  bool result_is_boolean()
  { return no > NO_BOOLEAN_START && no < NO_BOOLEAN_END; }
};

class ENodes : public Nodes {
public:
  ENodes(Node_List_Occupancy nlop):Nodes(),nlo(nlop){}
  ENodes(int list_id):Nodes(),nlo(Node_List_Occupancy(1<<list_id))
  {
    assert( list_id < sizeof(Node().occ) * 8 );
  }
  ~ENodes() { clear(); }
  void reset() { vector<Node*>::clear(); } // Don't update occupancy status.
  void clear() { while ( pop() ); } // Update occupancy status.
  void operator = (Node* nd) { clear(); operator += (nd); }
  void operator += (Node* nd)
  {
    if ( member(nd) ) return;
    nd->occ |= nlo;
    Nodes::operator += (nd);
  }
  void operator += (Nodes nds)
  {
    const int nsz = nds;
    for ( int i=0; i<nsz; i++ ) operator += (nds[i]);
  }
  Node* pop()
  {
    Node* const nd = Nodes::pop();
    if ( !nd ) return NULL;
    nd->occ &= ~nlo;
    return nd;
  }
  bool member(Node *nd) { return nd->occ & nlo; }
private:
  const Node_List_Occupancy nlo;
};



enum Interkernel_Source_Type
  {
    IK_none = 0,   // No interkernel transfer.
    IK_simple,     // Value is result of producer node.
    IK_leader,     // Value is reduction based on sources at source kernel.
    IK_follower    // No value, ik transfer done by IK_leader.
 };

struct Mapping_Node {

  Mapping_Node();
  
  // Earliest assigned kernel, or num_kernels if ik never suitable for node.
  int serial;  // Set to mapping_serial in Move_Proposal.
  int ik_kno;
  Interkernel_Source_Type ik_needed;
  bool ik_st_emitted;
  char ik_ldr_pos;       // Set on children. (Sources to reduction op.)
  char ik_grp_sz;        // Set on children. (Sources to reduction op.)
  char red_kno;
  char red_root_srcs; // Number of operation sources on parent.
  R_Vector_Type reduction_coverage;

  bool ik_active()
  { return ik_needed == IK_simple || ik_needed == IK_leader; }

};

struct Grid_Function
{
  Grid_Function(const char* namep):name(namep),device_addr(0){}
  const string name;
  Array_Axes axes;
  Nodes nodes;
  char force;     // If 'g', force global; if 'r', force read-only.
  bool is_shared;
  bool is_in;
  bool is_out;
  void* device_addr;
  bool constant_space;
  int cs_local_size_elts;

  // Values below kernel-specific.
  ptrdiff_t offset_byte;
  int shared_var_idx;
  int local_var_idx;
  Buffer_Group *bg;
  Pattern pattern_info;
  int bg_sh_var_idx;  // Starts at zero in each buffer group.
};

typedef map <string,Grid_Function*> Grid_Function_Map_Plain;
class Grid_Function_Map : public Grid_Function_Map_Plain {
public:
  Grid_Function* lookup(string name)
  {
    Grid_Function_Map_Plain::iterator iter = find(name);
    return iter == end() ? NULL : iter->second;
  }
};


typedef pVectorI<Grid_Function> Grid_Functions;
typedef pVectorI_Iter<Grid_Function> GFVIter;

class GFIter {
public:
  GFIter(Grid_Function_Map& gfm):i(0),gfmi(gfm.begin()),gfmend(gfm.end()){}
  operator bool ()
  {
    if ( gfmi == gfmend ) return false;
    current = gfmi->second;
    gfmi++; i++;
    return true;
  }
  operator int () { return i; }
  operator Grid_Function* () { return current; }
  Grid_Function* operator -> () { return current; }
private:
  int i;
  Grid_Function_Map::iterator gfmi, gfmend;
  Grid_Function *current;
};

typedef u_int64_t Stncl_Packed;
const unsigned int stencil_radius_bits = 4;
const unsigned int stencil_pattern_bits = 8;
const unsigned int spacked_special_bits = 4;

enum Buffer_Style
  { BS_Unset, BS_Not_Cached, BS_Constant, BS_Read_Only_Cache, BS_Pin, BS_Plus,
    BS_Block, BS_ENUM_SIZE };
const char* const buffer_style_str[] =
  { "Unset", "Global", "Constant", "RO Cache", "Pin", "Plus", "Block" };
const char* const buffer_style_abbr[] =
  { "?", "G", "K", "R", "!", "+", "B" };

enum Buffer_Style_Interior
  { BSI_None,  // Shared memory not used.
    BSI_Block, // Use whole-block load routines (handles interior and halo).
    BSI_Self,  // Each thread loads its own value, irreg and GPO used for halo.
    BSI_Irreg, // Interior loaded by irregular code, maybe GPO for f-sten.
    BSI_GPO,   // Use GP-offset loads for interior. Not a good option, ATM.
    BSI_T0,    // Interior loaded by T0 code which ATM I'm too tired to expl.
    BSI_ENUM_SIZE };

const char* const buffer_style_interior_str[] =
 { "None", "Block", "Self", "Irreg", "GPO", "T0" };

struct Buffer_Group {
  Buffer_Group() { inited = false; }
  ~Buffer_Group() { inited = false; }
  bool inited;
  int idx;
  Grid_Functions gfs;
  pVector<Pattern> gf_patterns;
  Grid_Functions gfs_b;

  CStncl stncl;
  u_int8_t pattern;
  Array_Axes axes;
  bool no_reuse;
  // Members above reflect shape of offsets.

  char force;

  // Members below reflect buffering style actually chosen, not necessarily
  // the best possible.
  Buffer_Style style_try[2];
  Buffer_Style style;
  Buffer_Style_Interior interior;

  int8_t stncl_fmin, stncl_fmax, stncl_emin, stncl_emax, stncl_f, stncl_e;
  int x_length, e_length, f_length;

  int n_lds, n_py, n_pz, n_p0;

  int shared_vars;
  int shared_elts;
  int shared_array_base_byte;

  int sc_y_stride, sc_z_stride, sc_e_stride, sc_f_stride;
  int sc_var_stride_bytes;
  int sc_extra_x_tile;

  string halo_pos_array_name;
  int rounds_block, rows_per_block;
  int halo_rounds;
  int halo_pos_array_size;

  PTX_RR ptxr_sh_base_common;
  vector<PTX_RR> ptxr_halo_sh_addr;
  vector<PTX_RR> ptxr_halo_gl_base;
  PTX_RR ptxr_use_last_hp;

  // Block code.

  int sc_fetch_rounds;
  int sc_fetch_whole_rounds;

  PTX_RR ptxr_fetch_participant;

  PTX_RR ptxr_sc_sc_thd_base;
  PTX_RR_Addr_Base *addr_bo;
  PTX_RR ptxr_sc_extra_x_thd;
  PTX_RR ptxr_sc_fetch_participant_frac_round;
};

typedef map<Stncl_Packed,Buffer_Group> Buffer_Group_Map;
typedef pVectorI<Buffer_Group> Buffer_Groups;
typedef pVectorI_Iter<Buffer_Group> BGIter;

typedef pVectorI_Iter<Node> NIter;
  

struct Kernel_Arg_Array {
  Kernel_Arg_Array(string n, void* da):name(n),device_addr(da){};
  string name;
  void *device_addr;
};

typedef pVectorI<Kernel_Arg_Array> Kernel_Arg_Arrays;
typedef pVectorI_Iter<Kernel_Arg_Array> KAAIter;

struct ET_Components {
  ET_Components():mapping_serial(-3){}
  ET_Components(const ET_Components& etc);
  ET_Components& operator= (ET_Components etc);
  Chemora_CG_Calc_Mapping* m;
  void init(Chemora_CG_Calc_Mapping* m);
  void stencil_reset();
  double et_compute();

  Nodes ik_lds_pre, ik_lds_add, ik_lds_sub;
  Nodes ik_sts_pre, ik_sts_add, ik_sts_sub;
  int mapping_serial;
  bool modified;

  // A buffer group has a set of grid functions and describes a common
  // buffering strategy for those grid functions.
  Buffer_Group_Map buffer_group_map;
  Buffer_Groups buffer_groups;
  // Style to use for loads that are not part of a buffer group.
  Buffer_Style groupless_style;

  // All members starting at et will be zeroed on init.
  double et;  // Estimated time per grid point.
  double amt_elts;
  double amt_insn;
  int latency;
  double chained_lds;
  int regs_per_thd_guess;  // Estimate of number of registers used.
  int regs_per_thd;        // Number of regs assigned to thread.
  int over_regs;

  // Execution time in cycles per grid point per mp.
  // That is, execution time in cycles would be:
  //   ts_insn * ( num_grid_points / num_multiprocessors ).
  double ts_insn, ts_data, ts_latency;
  double ts_insn_ut, ts_latency_ut;
  double ts_insn_lim_fp, ts_data_lim;
  double ts_f_penalty;
  double ts_load_latency; // For comparison, only affected by thds/mp.

  int sh_ld_pt;
  double gl_ld_pt;
  int ro_ld_pt;         // ROC load instructions.
  double ro_fills_pt;   // Estimated number of fills.
  double ro_unique_pt;  // Unique elements per thread. Ideally = ro_fills_pt.
  double buf_ls_pt;

  // Utilization based on thread scheduling granularity, computation
  // grid, and tile shape.
  double utilization_slots;
  // Utilization based on tile shape and computation grid.
  double utilization_thds;
  // Utilization based on number of blocks and number of CUDA multiprocessors.
  double load_balance;
  int blocks_per_mp, f_halo;

  // Per grid point.
  int ik_ld, ik_st;
  int ops;
  int simple_ld;
  int simple_st;
  int stencil_ld;  // Number of nodes performing stencil operations.
  int unique_stencil_ld;
  int gls_insn;    // Global load and store instructions.
  int code_nonloop_insn, code_iter_insn, code_buff_insn;

  // Covers all non-memory instructions in the dataflow graph.
  // Does not include iteration instructions, nor instructions for
  // filling shared memory.
  double issue_usage;

  CStncl stncl;  // Stencil bounding box.
  Tile tile;
  int num_blocks;
  bool iter_y;
  u_int8_t pattern; // Bit vector of offset combinations.
  char style_label;
  int plus_style, pin_style, block_style;

  bool bl_per_mp_hint_used;

  bool need_corners_get(Buffer_Group *bg)
    {
      enum Pattern_Axes
        { P1 = 0, Px = 1, Py = 2, Pz = 4, Pxy = 3, Pxz = 5, Pyz = 6, Pxyz = 7 };
      const int need_corners_y = ( 1 << Pxz ) | ( 1 << Pxyz );
      const int need_corners_z = ( 1 << Pxy ) | ( 1 << Pxyz );
      return bg->pattern & ( iter_y ? need_corners_y : need_corners_z );
    }

};

class Chemora_CG_Kernel_Info {
public:
  Chemora_CG_Kernel_Info(){ fi = NULL; };
  ~Chemora_CG_Kernel_Info();
  string function_name;
  Function_Info *fi;
  bool should_run;
  ET_Components etc;
  Nodes nodes;
  int rr_base_extra;
  int live_reg_max;
  int ptx_nlive_32;
};

class Node_Backup {
public:
  Node_Backup(Chemora_CG_Calc_Mapping *cf);
  ~Node_Backup();

  Chemora_CG_Calc_Mapping* const m;
  Chemora_CG_Calc* const c;
  Nodes nodes;
  Node_Map name_to_node;
};

enum Work_Round_Type
  { WR_Irregular_Offsets, // Base point loaded from array.
    WR_GP_Offsets,        // Base point is this thread's GP.
    WR_T0_Offsets,        // Base point is 0 plus this thds pos in ..
    WR_ENUM_SIZE };
const char* const wrt_str[] = { "Ir", "GP", "T0" };

class WG_Round {
public:
  WG_Round(Grid_Function *gfp,int fp, int p_nump)
    :gf(gfp),wr_type(WR_GP_Offsets),ptxr_pred(NULL),f(fp),p_num(p_nump){}
  WG_Round(int gl_o, int sm_o, int rnd)
    :gf(NULL),wr_type(WR_Irregular_Offsets),ptxr_pred(NULL),
     f(rnd),gl_offset_sc(gl_o),sm_offset_sc(sm_o){}
  Grid_Function *gf;
  Work_Round_Type wr_type;
  int base_reg_idx;
  PTX_RR *ptxr_pred;
  int tid_start, tid_stop;
  int f; // Value of f for row to be buffered.
  int p_num;    // Number of piece within row.
  int reg_idx;  // Number of local registers used in each thread by h2 code.
  int gl_offset_sc;
  int sm_offset_sc;
};

class Work_Group {
public:
  Work_Group():gf_n(WR_ENUM_SIZE){};
  void operator += (WG_Round wgr)
  { wg_rounds += wgr; gf_n[wgr.wr_type][wgr.gf]++; }
  pVector<WG_Round> wg_rounds;
  vector< map<Grid_Function*,int> > gf_n;
  int idx;
  int tid_gstart; // Start of work group.
  int tid_gnext;  // First thread in next wg, or an invalid thread number.
};

class Work_Group_GP_Set {
public:
  Work_Group_GP_Set(){ wg_critical = NULL; };
  pVectorI<Work_Group> work_groups;
  Work_Group *wg_critical;
  int tid_start;
  int tid_stop;
};

struct Chemora_Halo2_Data {

  int num_irreg_sizes;
  int irreg_grp_num_thds;
  int irreg_part_thd_max;
  int num_reg_sets;
  int gl_base_t0_idx;
  int x_width_max;
  bool have_t0_rounds;
  int tree_max_cond_per_leaf;

  pVectorI<Work_Group> work_groups;

  string shared_offsets_array_name;
  string global_offsets_array_name;

  PTX_RR ptxr_halo2_participation;
  pVector<PTX_RR> ptxr_tree_nodes;
  pVector<PTX_RR> ptxr_participation;

  pVector<PTX_RR> ptxr_h2;
  pVector<PTX_RR> ptxr_halo2_sh_base;
  pVector<PTX_RR_Addr_Base*> addr_bo_halo2;
};

struct Chemora_Emit_Kernel_Data {

  // Common
  //
  int kno;
  ET_Components *etc;
  int cagh_ni;
  int block_size;
  int tile_e;
  int tile_f;
  int gl_z_stride;
  int gl_e_stride;
  int gl_f_stride;
  int iter_offs;
  int axes_len_global[AX_SIZE];

  int assumed_elt_size;
  PTX_Reg_Type pt_real;
  void *addr_base;
  int local_elts_max;

  pVector<PTX_RR_Addr_Base*> addr_bo_all;

  int tt;

  PTX_RR ptxr_tid;
  PTX_RR ptxr_gi_blk, ptxr_gj_blk, ptxr_gk_blk;
  PTX_RR ptxr_li, ptxr_lj, ptxr_lk, ptxr_li_sc;
  PTX_RR ptxr_elt_blk_idx;
  PTX_RR *ptxr_lo, *ptxr_l;
  PTX_RR ptxr_shared_buffer_addr;

  int code_num_pred_regs;

  int code_path_nnodes;
  int code_path_h2_nnodes;
  int code_path_1d;  // Code path diverged from main (1) but not in h2.
  int code_path;     //  If 1, all threads converged. Never 0.
  vector< vector< deque<PTX_RR*> > > code_path_regs_shareable;
  int regs_shareable_code_path_last;
  map<size_t,PTX_RR> gf_addr_to_ptxr;

  vector<PTX_Reg_Pool> code_path_reg_pool;

  Chemora_Halo2_Data halo2_data;

};

class Chemora_CG_Calc_Mapping {
public:

  Chemora_CG_Calc_Mapping()
    :c(NULL),num_kernels(0),report_started(false),report_cg2_ptr(NULL){ };
  ~Chemora_CG_Calc_Mapping();
 
  void init(Chemora_CG_Calc *cp, int num_kernels, string nickname);

  // Used for quick and dirty tuning and debug. 
  // See CaCUDALib_CUDA_Driver_Manager::msg_tune and msg_info.
  void printf(const char *fmt, ...) __attribute__ ((format(printf,2,3)));

  Static_Op_Info* soi_get(Node *nd);

  void ki_free_contents();
  Mapping_Node* mapping_node_get(Node *nd);
  Mapping_Node* mapping_node_get(Move_Proposal *mp, Node *nd);

  void assign_to(Node *node, int kno);
  void unassign_from(Nodes& nodes, int kno);
  bool is_assigned_to(Node *node, int kno);
  bool is_assigned_to(Move_Proposal *mp, Node *node, int kno);
  int num_dests_at(Node *node, int kno);
  bool has_dests_at(Node *node, int kno);
  bool has_dests_at(Move_Proposal *mp, Node *node, int kno);
  A_Vector_Type ik_consumers_vector_get(Node *node, A_Vector_Type mask = 0);
  A_Vector_Type ik_consumers_vector_get(Move_Proposal *mp, Node *node);
  A_Vector_Type nd_consumers_vector_get(Node *node);
  A_Vector_Type nd_consumers_vector_get(Move_Proposal *mp, Node *node);
  A_Vector_Type assignment_vector_get(Node *node) const 
  { return assignment_vector[node->idx]; }
  A_Vector_Type assignment_vector_get(Move_Proposal *mp, Node *node);

  Interkernel_Source_Type ik_needed_update(Node *nd);
  Interkernel_Source_Type ik_needed_update(Move_Proposal *mp, Node *nd);
  int ik_kno_update(Node *nd);
  int ik_kno_update(Move_Proposal *mp, Node *nd);
  Interkernel_Source_Type ik_update(Node *nd);
  Interkernel_Source_Type ik_update(Move_Proposal *mp, Node *nd);

  string ik_make(Node *node);
  string make_ik_name(Node *node);
  void random_split(int num_kernels);
  int split(int num_kernels);
  void fixup();
  void reduction_check(Move_Proposal *mp, Node *nd);
  void reduction_update(Move_Proposal *mp, Node *nd);
  void reduction_update(Node *nd);
  void reduction_update();
  void mp_add_plus(Move_Proposal *mp, Node *nd);
  void mp_add_plus_strict(Move_Proposal *mp, Node *nd);

  double move_propose(Move_Proposal *mp);
  void move_accept(Move_Proposal *mp);
  void klp_update(int kno);

  bool verify(Move_Proposal *mp = NULL, bool ignore_cost = false);
  string emit_code();
  string emit_code_clike();
  void emit_code_kernel(int kno);
  void emit_code_clike_kernel(int kno);
  void halo2_setup_and_emit_arrays();
  void halo2_emit_pre_loop_code();
  void sched_pos_max_update_or_verify(char option, Nodes& cnodes);
  void sched_pos_live_update_or_verify
  (char option, Nodes& rnodes, int& max_regs, double& avg_regs);
  void sched_pos_live_verify(Nodes& rnodes);
  Nodes sched_set_examine(Nodes sched_set_roots);

  Pattern pattern_find(const Nodes& nds);
  void et_gf_update(Move_Proposal *mp, ET_Components *etc, int kno);
  void et_gf_update(ET_Components *etc, int kno);
  void et_tile_update(ET_Components *etc, int kno);
  double compute_cost(Move_Proposal *mp);

  void quick_copy(Chemora_CG_Calc_Mapping *m);
  void regenerate(Chemora_CG_Calc_Mapping *m);

private:
  void buffer_style_table_key_make();
  string table_style_key;
public:
  void report_print();
  void report_compile_print();

  friend class ET_Components;
  friend class PTX_Insn;
  friend class PTX_Reg_Pool;

private:
  Chemora_Emit_Kernel_Data ek;

  FILE *emit_ptx_fh;
  bool emit_clike; // If true, emit C-like code, otherwise PTX.
  int ptx_line_label_cnt;
  int ptx_indent;
  pVector<int> ptx_indent_stack;
  void ptx_indent_push(int amt);
  void ptx_indent_push_abs(int amt);
  void ptx_indent_pop();
  void ptx_include(const char* path);
  void ptx_line(const char* line);
  void ptx_line(string line) { ptx_line(line.c_str()); }
  void ptx_linef(const char* fmt, ...) __attribute__ ((format(printf,2,3)));
  void ptx_line0f(const char* fmt, ...) __attribute__ ((format(printf,2,3)));
  void ptx_lines(const char* first_line, ...);
  void ptx_comment_line(const char* line);
  void ptx_body_start();
  void ptx_body_end();

  pair<int,int> code_path_sib_range_get(int code_path);
  int code_path_pool_idx_get(int code_path);
  PTX_Reg_Pool& code_path_pool_get(int code_path);

  FILE *ptx_lib_cu_fh;
  string ptx_lib_path_base;
  string ptx_lib_cu_path;
  int ptx_lib_nfuncs;

  void ptx_lib_start(string dir);
  void ptx_lib_entry_insert(const char *func, int argc, const char *dtype);
  void ptx_lib_emit();

  bool ptx_in_body;
  int ptx_emit_insns_pending;
  pVector<string> ptx_body_items;
  pVector<PTX_Insn> ptx_insns;

  PTX_RR ptx_make_reg(PTX_Reg_Type rt);
  PTX_Insn ptx_make(const char* mnemonic);
  PTX_Insn ptx_makef(const char* mnemonic, ...)
    __attribute__ ((format(printf,2,3)));
  PTX_Insn ptx_make(PTX_Reg_Type rt, const string mnemonic);
  PTX_Insn ptx_make
  (PTX_Reg_Type rt_dest, PTX_Reg_Type rt_src, const char* mnemonic);
  PTX_Insn ptx_make(PTX_RR& dest, const string mnemonic);
  PTX_Insn insn_cli_op_make
  (PTX_Reg_Type rt_dst, PTX_Reg_Type rt_src, const string operator_text);
  PTX_Insn insn_cli_op_make(PTX_Reg_Type rt, const string operator_text);
  PTX_Insn ptx_make(PTX_RR& dest, Static_Op_Info *soi);
  PTX_Insn ptx_make_insn_new(PTX_RR& dest);
  void ptx_make_1(PTX_Insn& insn, PTX_Reg_Type rt, const string mnemonic);
  void insn_ptx_make_1(PTX_Insn& insn,PTX_Reg_Type rt,const string mnemonic);
  string insn_cli_lhs_make(PTX_Insn& insn);
  void insn_cli_make_1(PTX_Insn& insn, PTX_Reg_Type rt, const string mnemonic);
  PTX_Insn ptx_make_lhs(PTX_Reg_Type rt, const string sep = string(""));
  PTX_Insn ptx_make_opaque_eq_reg(const string& lhs);
  PTX_Insn ptx_make_no_dest(PTX_Reg_Type rt, const string mnemonic);

  int ptx_make_gp_offset_global(int axes, int di, int dj, int dk);
  PTX_RR ptx_make_gfo_roc_load // Read-only cache.
  (PTX_Reg_Type pt, PTX_RR ptxr_addr_base, ptrdiff_t addr_offset,
   int axes, int di, int dj, int dk);
  PTX_RR ptx_make_gfo_roc_or_gl_load
  (PTX_Reg_Type pt, Grid_Function *gf,
   PTX_RR_Addr_Base *addr_bo, ptrdiff_t addr_offset,
   int di, int dj, int dk, char choice);
  PTX_RR ptx_make_gfo_load_mnemonic
  (PTX_Reg_Type pt, Grid_Function *gf,
   PTX_RR_Addr_Base *addr_bo, ptrdiff_t addr_offset,
    int di, int dj, int dk, const string mnemonic);

  PTX_RR_Addr_Base* ptx_addr_base_new
  (PTX_RR& ptxr_thd_offset, int elt_size_bytes = 0,
   Array_Axes_Enum axes = AX_xyz);

  PTX_RR_Base_Extension ptx_make_addr_base
  (PTX_RR_Addr_Base *base_bo, size_t addr, int acc_offset);
  PTX_Insn_Addr_Offs ptx_make_addr(WG_Round& wgr, int64_t iter_offs_sc);
  PTX_Insn_Addr_Offs ptx_make_addr(PTX_RR base_reg, int64_t offset = 0);
  PTX_Insn_Addr_Offs ptx_make_addr
  (PTX_RR_Addr_Base *base_bo, size_t addr, int offset = 0);
  PTX_Insn_Addr_Offs ptx_make_addr
  (PTX_RR_Addr_Base *base_bo, void* addr, int offset)
    { return ptx_make_addr(base_bo,size_t(addr),offset); }

  void ptx_cf_shared_block_setup(Buffer_Group *bg);
  void ptx_cf_shared_block_load_loop
  (Buffer_Group *bg, bool pre_loop, int xfer);
  void ptx_cf_shared_block_load_pre_loop(Buffer_Group *bg);
  void ptx_cf_cak_fetch_to_cache_round
  (Buffer_Group *bg, Grid_Function *gf,
   int e_s, int round, int e, int transfer);

  pVectorI<PTX_RR> ptx_all_regs;
  int ptx_rr_next[PT_SIZE];
  map<string,bool> ptx_reg_declared;

public:
  Chemora_CG_Calc *c;
  Chemora_CG_Thorn *ct;
  Chemora_CG *chemora;
  // Something briefly identifying the mapping strategy. Used to
  // construct a file name, so no more than two or three letters please.
  string mapping_nickname;
  int config_version; // Value of c->config_version when mapping created.
  Chemora_CG_Module_Data *modd;
  // Serial number for most recent mapping, or call to compute_cost.
  int mapping_serial;
  int accept_mp_serial;  // Serial number of most recent accepted move.
  int num_kernels;
  bool report_started;  // Used to determine when it's okay to free stuff.
  vector <Chemora_CG_Kernel_Info> kernel_info;
  vector <A_Vector_Type> assignment_vector;
  vector <A_Vector_Type> assignment_vector_prev;
  vector <Mapping_Node> mapping_node;

  // Make pTable pointer because pTable uses c++11 and this file, chemora_cg.h
  // is included in files which are compiled with older versions of C.
  pTable* report_cg2_ptr;

  double t_scaled;
  double t_scaled_approx;
  int t_scaled_num_approx;
};

class Chemora_CG_Module_Data {
public:
  Chemora_CG_Module_Data(){ mi = NULL;  m = NULL; }
  ~Chemora_CG_Module_Data() { if ( m ) delete m; }
  Chemora_CG_Calc_Mapping *m;
  Module_Info *mi;
  Kernel_Arg_Arrays kernel_arg_arrays;
  int gf_size_bytes;
};

typedef pVectorI<Chemora_CG_Module_Data> Chemora_CG_Module_Data_Vector;
typedef pVectorI_Iter<Chemora_CG_Module_Data> MODDIter;

struct CaCUDALib_GPU_Info;

class Chemora_CG_Calc {
public:

  Chemora_CG_Calc();
  void init
  (Chemora_CG_Thorn *ct, const char* calc_name,
   Chemora_CG_Calc *diff_operations_calc);

  Chemora_CG_Thorn* thorn_get() const { return ct; }

  string module_key_make(CCTK_ARGUMENTS, CaKernel_Kernel_Config *kc);
  void module_info_set(Module_Info *mip);

  bool code_target_acc_get();

  void at_launch_prepare(const char* name);

  Static_Op_Info* soi_get(Node *nd);

  Node* node_new(NType ntype, const char* name_fmt, ...)
    __attribute__ ((format(printf,3,4)));
  Node* node_new(const char* name, NType ntype, Node_New_Action nna = NN_New);
  Node* node_lookup(const char* name);
  Node* node_lookup(string name) { return node_lookup(name.c_str()); }
  Node* node_literal_lookup(double val);
  Node* node_literal_init(Node *nd, double val);
  void node_remap_to(Node *from, Node *to);
  void node_src_add(Node *nd, Node *src);
  void node_srcs_add(Node *nd, Nodes& srcs);
  string node_cse_key_make(Node *nd);
  void node_perf_update(Node *nd, ET_Components *etc = NULL);

  void nodes_at_generic_processing_done();
  void nodes_at_specialization_start();

  void node_srcs_clear(Node *nd);
  // It's okay if replacement intersects existing sources.
  void node_srcs_replace(Node *nd, Nodes replacement);
  void node_srcs_of_dests_replace(Node *nd_old, Node *nd_new);
  void node_to_identity_and_remove(Node *nd, Node *id_src);
  void node_to_identity_and_remove(Node *nd, int var_src);
  void node_remove(Node *nd);
  void nodes_propagate_ctcs(Nodes& nodes);
  void optimize_factor_ac_plus_ax();
  void optimize_fp_selects();
private:
  int nds_ident_from_op;
  int nds_zero_from_mult;
  int nds_neg_from_mult;
  int nds_red, nds_red_srcs;

  // Counts below computed at end of finalize routine.
  int fp_ops;    // Number of multiplies and adds.
  int fp_other;  // Number of other "computational" FP instructions.
  int fp_not;    // Number of other instructions.

public:

  void set_real_type(float dummy){ real_type = "float"; };
  void set_real_type(double dummy){ real_type = "double"; };

  void set_parameter(const char* name, const char* btype);

  void set_launch_params_c_src(string launch_params_c_src)
    { kernel_launch_parameters_c_src = launch_params_c_src; }

  void launch_gf_arg_set(const char* name, void *device_addr);
  void launch_scalar_arg_set(const char* name, void *device_addr);

  Node* assign_scalar(const char* name);

  void assign_from_constant_expr
  (const char* name, const char* rhs_c_code);

  Node* assign_from_literal(const char* name, double val);
  Node* assign_from_literal(const char* name, int val);

  void assign_gf_axes(const char* name_raw, int axes);

  Node* assign_from_gf_load
  (const char* lhs, const char* gf, int time_level = 0);
  Node* assign_from_offset_load
  (const char* lhs, const char* gf,
   const char* dx, const char* dy, const char* dz);
  Node* assign_from_offset_load
  (const char* lhs, const char* gf, int time_level,
   const char* dx, const char* dy, const char* dz);

  Node* assign_from_ik_load(const char* lhs);

  void assign_from_expr
  (const char* lhs, const char* fp_ops, const char* rhs_c_code, ...);
  void assign_from_func
  (const char* lhs, const char* func_name, const char* rhs_c_code, ...);
  void assign_from_gf_load_switch
  (const char* lhs, const char* gf_name, const char* switch_name, ...);
  void assign_from_ifelse
  (const char* lhs, const char* cond_name,
   const char* name_if, const char* name_else);

  void gf_store(const char* gf_name, const char* src_name);

  void gf_use(const char* gf_name, Node *node);

  void value_set(const char* param_name, int value);
  void value_set(const char* param_name, double value);
  void value_set_done() {}

  void gf_update();

  void node_c_code_make(Node *nd, const char *comment = NULL);

  bool need_params() { return true; }

  void visited_recompute();
  void dests_recompute();

  void set_kernel_launch_parameters(CaCUDA_Kernel_Launch_Parameters *klp);
  void calc_finalize();
  void reducible_set();

  void nd_print_sinks(Node *nd_start);

  string generate_code();

  void launch_verify_addr_args(void **args);
  Chemora_CG_Module_Data* module_data_get(Module_Info *mi);
  Chemora_CG_Module_Data* module_data_get(int idx);

  // Used for quick and dirty tuning and debug. 
  // See CaCUDALib_CUDA_Driver_Manager::msg_tune and msg_info.
  void printf(const char *fmt, ...) __attribute__ ((format(printf,2,3)));

  void report_print();
  void report_compile_print();

private:

  CaCUDALib_GPU_Info *gpu_info;
  Chemora_CG *chemora;
  Chemora_CG_Thorn *ct;
  Chemora_CG_Calc *diff_operations_calc;
public:
  string calc_name;
private:
  // The number of times that finalize was called. Normally, this
  // would be the number of different configurations that kernels
  // were generated for.
  int config_version;

  string kernel_function_name; // Name used in CUDA code.

  // For use in constructing the names of temporary files.
  std::string temporary_files_dir_get();
  std::string temporary_file_path_prefix_get();

  bool opt_code_target_set;
  bool opt_code_target_acc;

  int opt_rand_seed;
  int rand_seed;
  bool opt_metrics_extra;

  int opt_num_kernels;

  // Verify that each buffered value was loaded correctly.
  bool opt_verify_buffering;

  // 's', fixed stores;  'n', fixed number of kernels; '0', no constraint.
  char opt_move_constraint;
  char opt_buf_single_use, opt_buf_low_dim;
  char opt_buf_sten_x, opt_buf_sten_y, opt_buf_sten_z;
  u_int8_t opt_force_pattern;
  char opt_force_pattern_to;  // Storage to force assignment to.
  bool opt_force_one_block_per_mp;
  bool opt_reduce_emit_dataflow;
  int opt_halo2;
  int opt_icache_size;
  int icache_bytes;

  bool opt_array_bases_share, opt_4Gx_okay, opt_addr_literals;
  bool opt_addr_regs_opt;
  int opt_addr_regs_max;

  // Assumed number of times each instruction executes. Ideal is 1,
  // higher than 1 means that an instruction is replayed, for example,
  // due to a shared memory bank conflict.
  double opt_issue_per_exec;
  bool opt_sched_latency;
  int opt_small_block;
  bool opt_cse;
  int opt_opt_nodes_max;
  double opt_opt_props_per_node_per_cycle;
  int opt_opt_num_cycles;
  map<string,char> opt_assign_force_map;
  int opt_block_occ_no_hint_threshold;
  Kernel_User_Setting *kus;

  Nodes nodes;
  Nodes nodes_backup; // WARNING: pointers point to nodes in nodes.
  // Constructor sets marker to 1.  Should always be overwritten by 2.
  static const int marker_init_val = 2;  // Assigned by node_new.
  static const int marker_never = 3;     // Should never be assigned.
  int next_marker;
  int mp_version_next;
  Node_Map name_to_node, name_to_node_backup;
  Nodes do_nodes; // Differencing op nodes which are in diff_operations_calc.
  Node_Map name_to_do;
  Node_Map gfosig_to_node;
  Kernel_Arg_Arrays kernel_arg_arrays;
  Grid_Function_Map grid_functions;
  map<string,int> gf_axes;
  map<size_t,Grid_Function*> addr_to_gf;
  bool need_kernel_launch_parameters;
  int var_interm_next; // Number for generation of intermediate variables.

  friend class Move_Proposal;
  friend class Chemora_CG_Calc_Mapping;
  friend class ET_Components;
  friend class Node_Backup;
  friend class PTX_Reg_Pool;
  Chemora_CG_Module_Data *modd; // Current module.
  map<void*,Chemora_CG_Module_Data> module_data;
  Chemora_CG_Module_Data_Vector modd_vector;

  const char* real_type;

  Node* node_loop_start;

  // Stencil bounding box.
  CStncl stncl;

  double opt_wall_time_s;
  int opt_proposals;
  int opt_accepts;

  Module_Info *mi;
public:
  CaCUDA_Kernel_Launch_Parameters kernel_launch_parameters;
  string kernel_launch_parameters_c_src;

};

typedef map<string,Chemora_CG_Calc> Chemora_CG_Calcs;
typedef pMap_Str_Obj_Iter<Chemora_CG_Calc> CIter;

struct Move_Proposal {
  Move_Proposal();
  ~Move_Proposal();
  Chemora_CG_Calc_Mapping *m;
  void init(Chemora_CG_Calc_Mapping *m);
  int serial;
  int marker_mp;
  int from, to;
  bool immobile_stores;
  bool gang_move;
  bool copy_from;
  int k_remove_size;
  Node *candidate;
  Nodes *k_remove;
  Nodes nds_affected;
  int version; // Changes whenever nodes reassigned due to DCE, etc.
private:
  bool k_remove_insert_maybe_maybe(Node *nd, int kno, bool maybe);
public:
  bool k_remove_insert_maybe(Node *nd, int kno);
  void k_remove_insert_strict(Node *nd, int kno);
  bool k_remove_insert(Node *nd, int kno);
  Nodes to_add;
  Nodes ik_add;
  Nodes ik_add_maybe;
  Nodes ik_remove;
  Nodes ik_update; // ik_kno changes.
  ENodes red_change;  // Reduction nodes that may have changed.
  bool feasible;

  int mapping_node_size;
  Mapping_Node *mapping_node;
  ET_Components *etc;
  double et_total;
  double score;
};


class Chemora_CG_Thorn {
public:
  friend class Chemora_CG_Calc;

  void init(Chemora_CG *chemora, const char* thorn_name);

  const char* name_get() const { return thorn_name.c_str(); }

  Chemora_CG_Calc* calc_new(const char* calc_name);
  Chemora_CG_Calc* calc_get(const char* calc_name);     // Error if not found.
  Chemora_CG_Calc* calc_get_try(const char* calc_name); // Null if not found.
  void report_print();

  Chemora_CG_Calc diff_operations_calc;

  // Addresses of storage allocated on device (and not yet freed).
  pVector<CUdeviceptr> gpu_alloc_addrs;

  string code_compile_report_str;

private:
  Chemora_CG *chemora;
  Module_Info *mi;
  string thorn_name;
  Chemora_CG_Calcs chemora_calcs;
};


typedef map<string,Static_Op_Info*> Static_Op_Info_Map;

typedef map<string,Chemora_CG_Thorn> Chemora_CG_Thorns;
typedef pMap_Str_Obj_Iter<Chemora_CG_Thorn> ThIter;

class Chemora_CG {
public:
  Chemora_CG();
  Chemora_CG_Thorn* thorn_new(const char* thorn_name);
  Chemora_CG_Thorn* thorn_get(const char* thorn_name);
  Chemora_CG_Thorn* thorn_get_try(const char* thorn_name);

  void static_op_info_op_init();  // Operation names, etc.
  void static_op_info_lt_init(CaCUDALib_GPU_Info *gi);  // Measured performance.

  Static_Op_Info* static_op_info_new
  (const char *kranc_name, Node_Operation no,
   string ptx_mnemonic, string c_op = "");
  Static_Op_Info* static_op_info_new
  (Node_Operation no, string ptx_mnemonic, string c_op = "");
  void static_op_info_c_op_set(Static_Op_Info *soi, string c_op);
  void static_op_info_alias_set(Node_Operation no, const char *kranc_name);

  void static_op_info_lt_set
  (Node_Operation no, double latency, double throughput);

  Static_Op_Info* static_op_info_get(Node_Operation no);
  Static_Op_Info* static_op_info_lookup(const char *kranc_name);

  void report_print();

private:
  friend class Chemora_CG_Thorn;
  friend class Chemora_CG_Calc;
  friend class Chemora_CG_Calc_Mapping;
  friend class Chemora_CG_Module;
  friend class CaCUDALib_CUDA_Driver_Manager;

  Chemora_CG_Thorns chemora_thorns;

  // Mnemonics for instructions for the appropriate PTX version.
  //
  string ptx_mnem_ld_global;
  string ptx_mnem_ld_roc;
  string ptx_mnem_ld_generic;
  string ptx_mnem_st_global;
  string ptx_mnem_st_generic;

  // Instructions to use in specific situations.
  //
  string ptx_mnem_ld_single;
  string ptx_mnem_ld_offset;
  string ptx_mnem_ld_sm_populate;
  string ptx_mnem_ld_ik;
  string ptx_mnem_st_ik;

  bool static_op_info_lt_inited;  // Latency and timing.
  Static_Op_Info static_op_info[NO_SIZE];
  Static_Op_Info_Map static_op_info_map;

  bool rand_seed_set;
  bool rand_seed_printed;
  int rand_seed;

  bool at_cuda_ctx_available_ran;
  void at_cuda_ctx_available(bool opt_metrics_extra);

  int gpu_sm_amt;
  double gpu_comp_comm_ratio;

  double first_thorn_init_time_s;

};


extern Chemora_CG *chemora_global;

inline Chemora_CG* Chemora_CG_get() { return chemora_global; }

inline Chemora_CG_Calc* Chemora_CG_get_calc_try
(const char* thorn_name, const char* calc_name)
{
  Chemora_CG* const chemora = Chemora_CG_get();
  Chemora_CG_Thorn* const ct = chemora->thorn_get_try(thorn_name);
  if ( !ct ) return NULL;
  Chemora_CG_Calc* const c = ct->calc_get_try(calc_name);
  return c;
}

#define CHEMORA_AT_LAUNCH(thorn_name,kernel_name,kernel_function_name)        \
  Chemora_CG* const chemora = Chemora_CG_get();                               \
  Chemora_CG_Thorn* const ct = chemora->thorn_get_try(thorn_name);            \
  Chemora_CG_Calc* const c = ct ? ct->calc_get_try(kernel_name) : NULL;       \
  if ( c ) c->at_launch_prepare(kernel_function_name);

#define CHEMORA_SHOULD_INITIALIZE c && c->need_params()

#define CHEMORA_AT_LAUNCH_PARAM(p) c->value_set(#p,p);
#define CHEMORA_AT_LAUNCH_GF_ARG(n,da) c->launch_gf_arg_set(n,da);
#define CHEMORA_AT_LAUNCH_ARG(n,da) c->launch_scalar_arg_set(n,da);

#define CHEMORA_AT_LAUNCH_INIT_END c->value_set_done();

#endif
