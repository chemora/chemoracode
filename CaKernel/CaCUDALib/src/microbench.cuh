 /*@@  -*- c++ -*-
   @file      microbench.cu
   @date
   @author    Yue Hu
   @desc
   GPU micro-benchmarks to explore unreleased performance critical parameters:
   including: L2 cache, global memory, shared memory, floating point compute
   units.
   @enddesc
 @@*/

#ifndef MICROBENCH_CUH
#define MICROBENCH_CUH
#include "CaCUDALib_driver_support.h"

double mb_get_l2cache_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_gmem_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_smem_lat(CaCUDALib_GPU_Info* const gi, double stride);
double mb_get_smem_lat(CaCUDALib_GPU_Info* const gi, float stride);
double mb_get_smem_sync(CaCUDALib_GPU_Info* const gi, int n_warp);
double mb_get_sp_madd_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_madd_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_madd_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_madd_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_recip_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_recip_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_recip_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_recip_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_div_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_div_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_div_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_div_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_pow_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_pow_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_pow_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_pow_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_sin_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_sin_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_sin_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_sin_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_sqrt_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_sqrt_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_sqrt_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_sqrt_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_sqrt_recip_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_sqrt_recip_lat(CaCUDALib_GPU_Info* const gi);
double mb_get_sp_sqrt_recip_thpt(CaCUDALib_GPU_Info* const gi);
double mb_get_dp_sqrt_recip_thpt(CaCUDALib_GPU_Info* const gi);
#endif
