 /*@@
   @file      launch.cc
   @date      Wed Jun  6 13:29:04 2012
   @author    David Koppelman
   @desc 
              Configure (set tile size, etc) and launch
              a dynamically compiled kernel.
   @enddesc 
 @@*/


#include <string>
#include <iostream>
#include <sstream>

#include <dlfcn.h>
#include <assert.h>

#include <cctk.h>
#include <cctk_Parameters.h>
#include <cctk_Arguments.h>
#include <cctk_Schedule.h>

#include "chemora_cg.h"
#include "launch.h"
#include "CaCUDALib_driver_support.h"
#include "putil.h"
#include "pstring.h"

inline int iDivUp(int a, int b){ return (a + b - 1) / b; }

using namespace std;

string
CaCUDALib_CUDA_Driver_Manager::cakernel_module_key_make
(CCTK_ARGUMENTS, CaKernel_Kernel_Config *kc)
{
  /// WARNING: CaKernel will not work with multiple configurations.
  return string(kc->kernel_name);
}

void
CaCUDALib_CUDA_Driver_Manager::tile_size_overrides_parse
(const char *tile_size_overrides_param)
{
  pSplit overrides(tile_size_overrides_param,';');
  while ( const char *override = overrides )
    {
      pSplit parts(override,':');
      Kernel_User_Setting kus;
      kus.name_match_pattern = string(parts);
      pSplit shape_components(parts,',',"1");
      kus.tile_x = atoi(shape_components);
      kus.tile_y = atoi(shape_components);
      kus.tile_z = atoi(shape_components);
      kus.tile_xx = atoi(shape_components);
      kus.tile_yy = atoi(shape_components);
      kus.tile_zz = atoi(shape_components);

      if ( ( kus.tile_xx > 1 ) + ( kus.tile_yy > 1 ) + ( kus.tile_zz > 1 ) > 1 )
        CCTK_VWarn
          (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
           "Illegal tile size %s. Can iterate along only one iteration. "
           "(For example, \"%s:%d,%d,%d, 4,1,1\" would be okay.)",
           override, kus.name_match_pattern.c_str(),
           kus.tile_x, kus.tile_y, kus.tile_z);

      kernel_user_settings.push_back(kus);
    }
}

bool
CaCUDALib_CUDA_Driver_Manager::kernel_user_setting_match
(string pattern, string kernel_name)
{
  return pattern == "." || kernel_name.find(pattern) != string::npos;
}

Kernel_User_Setting*
CaCUDALib_CUDA_Driver_Manager::kernel_user_setting_lookup
(string kernel_name)
{
  for ( uint i=0; i<kernel_user_settings.size(); i++ )
    {
      Kernel_User_Setting& kus = kernel_user_settings[i];
      if ( kernel_user_setting_match(kus.name_match_pattern,kernel_name) )
           return &kus;
    }
  return NULL;
}

void
Module_Info::kernel_launch_params_init
(CaKernel_Kernel_Config *kernel_config, CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  kc = *kernel_config;

  klp.bwid_xn = kc.bwid_n[0];
  klp.bwid_xp = kc.bwid_p[0];
  klp.bwid_yn = kc.bwid_n[1];
  klp.bwid_yp = kc.bwid_p[1];
  klp.bwid_zn = kc.bwid_n[2];
  klp.bwid_zp = kc.bwid_p[2];
  klp.i_length_x = kc.i_length[0];
  klp.i_length_y = kc.i_length[1];
  klp.i_length_z = kc.i_length[2];

  gf_in_no_stencil = 0;
  gf_in_stencil = 0;

  kc.gf_var_size = kc.var_tmpl_info_size;
  kc.gf_var_analysis_data = new CAK_Var_Analysis_Data[kc.gf_var_size];

  // Prepare a map from grid function (var) name to
  // source-extracted stencil information.
  //
  std::map<string,CAK_Var_Src_Info*> var_to_src_var_info;
  for ( int i=0; i<kc.gf_stencil_src_info_size; i++ )
    {
      CAK_Var_Src_Info* const vi = &kc.var_src_info[i];
      var_to_src_var_info[string(vi->name)] = vi;
    }

  // Message for any stencils that extend outside of grid.
  //
  pString sten_err_msg;
  int* const sten_f = &kc.sten_xn;

  int out_count = 0;
  for ( int i=0; i<kc.gf_var_size; i++ )
    {
      CAK_Var_Tmpl_Info* const vi = &kc.var_tmpl_info[i];
      CAK_Var_Analysis_Data* const vad = &kc.gf_var_analysis_data[i];

      // Source-extracted stencil information.
      CAK_Var_Src_Info* const vsi = var_to_src_var_info[string(vi->name)];
      const bool vsi_okay = vsi && vsi->stencil_data_okay;

      vad->name = vi->name;
      vad->intent = vi->intent;
      vad->c_datatype = vi->c_datatype;
      vad->info_tmpl = vi;
      vad->info_src = vsi_okay ? vsi : NULL;

      int* const vi_sten = vsi_okay ? vsi->stencil : vi->stencil;

      pString var_sten_err_msg;
      const char* dim_str = "xyz";

      // Determine whether each maximum offset extends beyond kernel's
      // respective boundary.
      for ( int d=0; d<3; d++ )
        {
          const int fl = 2 * d;
          const int fu = fl + 1;
          if ( vi_sten[fl] > kc.bwid_n[d] )
            var_sten_err_msg.sprintf
              ("  Stencil %s %c of width %d extends beyond boundary "
               "of width %d.\n",
               "lower", dim_str[d],
               vi_sten[fl], kc.bwid_n[d] );
          if ( vi_sten[fu] > kc.bwid_p[d] )
            var_sten_err_msg.sprintf
              ("  Stencil %s %c of width %d extends beyond boundary "
               "of width %d.\n",
               "upper", dim_str[d],
               vi_sten[fu], kc.bwid_p[d] );
        }

      for ( int f=0; f<6; f++ )
        if ( sten_f[f] < vi_sten[f] )
          var_sten_err_msg.sprintf
            ("  Stencil %s %c of width %d extends beyond kernel-scope width %d "
             "specified in cakernel.ccl\n",
             f & 1 ? "upper" : "lower", dim_str[f/2],
             vi_sten[f], sten_f[f]);

      if ( var_sten_err_msg > 0 )
        sten_err_msg.sprintf("For variable %s ...\n%s",
                             vi->name, var_sten_err_msg.s);

      // Determine whether non-shared storage can be used along
      // y and z axes.
      //
      const int pattern_vector = vsi_okay ? vsi->pattern_vector : 0xff;
      bool plus_y_possible = true, plus_z_possible = true;
      for ( int p=0; p<8; p++ )
        if ( pattern_vector & ( 1 << p ) )
          {
            if ( p & 0x2 && ( p & 0x2 ) != p ) plus_y_possible = false;
            if ( p & 0x4 && ( p & 0x4 ) != p ) plus_z_possible = false;
          }
      vad->plus_y = plus_y_possible;
      vad->plus_z = plus_z_possible;

      int sum = 0;
      for ( int j=0; j<6; j++ ) sum += vi_sten[j];
      vad->stencil_access = sum > 0;
      vad->shared_candidate = 
        string(vi->intent) == "in" && vad->stencil_access;

      if ( string(vi->intent) == "out" ) { out_count++;  continue;  }

      if ( sum ) gf_in_stencil++; else gf_in_no_stencil++;
    }

  if ( sten_err_msg > 0 )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Problem with stencil or physical boundary zone for kernel %s\n%s",
       kc.kernel_name, sten_err_msg.s);

  kc.gf_out_count = out_count;

  klp.cagh_ni = cctk_lsh[0];
  klp.cagh_nj = cctk_lsh[1];
  klp.cagh_nk = cctk_lsh[2];
  klp.cagh_dx = CCTK_DELTA_SPACE(0);
  klp.cagh_dy = CCTK_DELTA_SPACE(1);
  klp.cagh_dz = CCTK_DELTA_SPACE(2);
  klp.cagh_dt = CCTK_DELTA_TIME;
  klp.cagh_xmin = CCTK_ORIGIN_SPACE(0);
  klp.cagh_ymin = CCTK_ORIGIN_SPACE(1);
  klp.cagh_zmin = CCTK_ORIGIN_SPACE(2);


  klp.tile_x = -1;

  dm->module_vals_set
    (STR(REPL_KEY_DYNAMIC_COMPILE),REPL_VALUE_DYNAMIC_COMPILE);
}

static int
wl_encode(int warp_limit, bool prefer_factors)
{ return ( warp_limit << 1 ) + prefer_factors; }

void
Module_Info::launch_config_decide()
{
  DECLARE_CCTK_PARAMETERS;

  const bool debug = false;
  Function_Info* const fi = dm->function_info_get(kc.kernel_function_name);
  const bool restrict_l1 = kc.gf_count >= dm->param_l1_restrict_gf_min;
  const int block_warp_limit_by_params =
    restrict_l1 ? dm->param_warps_l1_limited_max : dm->param_warps_max;

  double score_best = 1e10;
  int warp_limit_best = 0;
  bool prefer_factors_best = false;
  int warp_limit_try = block_warp_limit_by_params;
  bool prefer_factors_try = false;
  bool wl_tried[66];
  bzero(wl_tried,sizeof(wl_tried));

  pString shared_opt(dynamic_compile_shared_assignment);
  const bool shared_auto = shared_opt == "auto";
  const bool shared_static = shared_opt == "static";
  const int shared_var_user_limit = 
    shared_auto || shared_static ? -1 : atoi(dynamic_compile_shared_assignment);

  Kernel_User_Setting* const kus =
    dm->kernel_user_setting_lookup(kc.kernel_function_name);

  for ( config_decide_iteration = 1; config_decide_iteration < 50;
        config_decide_iteration++ )
    {
      bool& tried = wl_tried[wl_encode(warp_limit_try,prefer_factors_try)];
      const bool re_try = tried;
      tried = true;

      launch_config_decide_1(warp_limit_try,prefer_factors_try,kus);

      dm->module_vals_set_or_reset
        ( STR(REPL_KEY_UNROLL_MAIN), 
          lc.unroll_main ?
          "const bool cak__main_unroll = true;\n#pragma unroll" :
          "const bool cak__main_unroll = false;",
          config_decide_iteration > 1);

      // Prepare declarations of shared storage (possibly NULL) for
      // grid functions.
      //
      pString decl;
      int declared_shared_available =
        shared_auto ? klp.shared_gf_target :
        shared_static ? kc.gf_var_size : 
        min(shared_var_user_limit, klp.shared_gf_target);
      
      for ( int i=0; i<kc.gf_var_size ; i++ )
        {
          CAK_Var_Analysis_Data* const vi = &kc.gf_var_analysis_data[i];
          const char* const name = vi->name;
          const char* const var_type = vi->c_datatype;

          if ( shared_static && vi->info_tmpl->cached
               || ( !shared_static
                    && vi->shared_candidate
                    && declared_shared_available > 0 ) )
            {
              declared_shared_available--;
              decl += "cak__.sc_used = true;\n";
              decl.sprintf("const bool %s__is_shared = true;\n", name);
              decl.sprintf
                ("__attribute__((shared)) __attribute__((unused)) %s "
                 "%s__sh[cak__shared_cache_size];\n",
                 var_type, name);
              decl.sprintf
                ("%s* __attribute__((unused)) %s__s_me =\n"
                 "   &%s__sh[     %d + li\n"
                 "            + ( %d + lj ) * cak__sc_y_stride\n"
                 "            + ( %d + lk ) * cak__sc_z_stride ];\n",
                 var_type, name, name,
                 kc.sten_xn, kc.sten_yn, kc.sten_zn );
            } 
          else
            {
              decl.sprintf("const bool %s__is_shared = false;\n", name);
              decl.sprintf("%s* __attribute__((unused)) %s__sh = __null;\n",
                           var_type, name);
              decl.sprintf("%s* __attribute__((unused)) %s__s_me = __null;\n",
                           var_type, name);
            }
        }

      dm->module_vals_set_or_reset
        ( STR(REPL_KEY_SHARED_DECL), 
          decl.s, config_decide_iteration > 1);

      kernel_launch_params_set_or_reset(config_decide_iteration > 1);
      dm->module_unload();
      dm->function_load(kc.kernel_function_name);

      if ( kus ) break;

      if ( re_try ) break;

      const int kernel_max_threads = fi->max_threads_get();
      const int kernel_max_warps = kernel_max_threads >> 5;
      const bool will_run = kernel_max_threads >= klp.block_size;

      const int kernel_max_warps_clamped =
        min(kernel_max_warps, block_warp_limit_by_params);

      if ( will_run )
        {
          if ( score_best > lc.score )
            {
              warp_limit_best = warp_limit_try;
              prefer_factors_best = prefer_factors_try;
              score_best = lc.score;
              if ( debug )
                dm->msg_tune("-- Found a new best at bs %d try %d/%d (mw %d) "
                       "sc %.3f\n",
                       klp.block_size, warp_limit_try, prefer_factors_try,
                       kernel_max_warps, lc.score);
            }
          if ( wl_tried[wl_encode(kernel_max_warps_clamped,false)] )
            {
              if ( debug )
                dm->msg_tune("-- Finishing try %d  best %d/%d  sc %.3f\n",
                       warp_limit_try, warp_limit_best, prefer_factors_best,
                       score_best);

              if ( warp_limit_try == warp_limit_best
                   && prefer_factors_try == prefer_factors_best )
                break;
              warp_limit_try = warp_limit_best;
              prefer_factors_try = prefer_factors_best;
            }
          else
            {
              warp_limit_try = kernel_max_warps_clamped;
              prefer_factors_try = false;
            }
        }
      else
        {
          if ( debug )
            dm->msg_tune("Won't run %d\n",prefer_factors_try);
          if ( false && prefer_factors_try == false )
            {
              prefer_factors_try = true;
            }
          else
            for ( int w = kernel_max_warps; w > 2; w-- )
              if ( ! wl_tried[wl_encode(w,false)] )
                {
                  warp_limit_try = w;
                  prefer_factors_try = false;
                  break;
                }
        }
    }

  dm->module_generate_assembler();
  const int max_threads = fi->max_threads_get();

  if ( max_threads < klp.block_size )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Warp limit parameter value, %d, too large for kernel %s.\n"
       "Either set CaCUDALib::dynamic_compile_warps_max to a smaller\n"
       "value (16 is safe, 24 might work) or set\n"
       "CaCUDALib::dynamic_compile_recompile = on\n"
       "and make dynamic_compile_warps_max as large as 48.\n",
       dm->param_warps_max, kc.kernel_name);
}

void
Module_Info::launch_config_decide_1
(int warp_limit, bool prefer_factors, Kernel_User_Setting *kus)
{
  DECLARE_CCTK_PARAMETERS;

  const int wp_lg = 5;
  const int wp_sz = 1 << wp_lg;
  const int wp_mask = wp_sz - 1;

  const int x_length = kc.i_length[0];
  const int y_length = kc.i_length[1];
  const int z_length = kc.i_length[2];

  const int stncl_xn = kc.sten_xn;
  const int stncl_xp = kc.sten_xp;
  const int stncl_yn = kc.sten_yn;
  const int stncl_yp = kc.sten_yp;
  const int stncl_zn = kc.sten_zn;
  const int stncl_zp = kc.sten_zp;

  const int stncl_x = stncl_xn + stncl_xp;
  const int stncl_y = stncl_yn + stncl_yp;
  const int stncl_z = stncl_zn + stncl_zp;

  const int elt_size_assumed = sizeof(CCTK_REAL);

  const bool user_tile = kus;

  const bool restrict_l1 = kc.gf_count >= dm->param_l1_restrict_gf_min;
  const int block_warp_limit_w_shared = warp_limit;

  int tile_x = 0;
  int tile_y = 0;   // Number of threads in y direction.
  int tile_z = 0;
  int tile_yy = 0;  // Number of iterations in y direction.
  int tile_zz = 0;


  const int block_thread_limit_wo_shared = warp_limit << 5;
  const int block_thread_limit_w_shared = block_warp_limit_w_shared << 5;

  const int grid_points_count = x_length * y_length * z_length;
  const int line_lg = 7;  // CC 2.0 and 2.1 GPUs.
  const int real_per_line_lg = line_lg - ( elt_size_assumed == 4 ? 2 : 3 );
  const int real_per_line = 1 << real_per_line_lg;
  const int x_min_line = real_per_line - stncl_x;
  const double real_per_line_inv = 1.0 / real_per_line;
  const int line_real_mask = real_per_line - 1;
  const int cache_size_lines = 3 << 14 - line_lg;
  const int gf_sten_count = gf_in_stencil;
  const int gf_core_count =
    gf_in_stencil + gf_in_no_stencil ? gf_in_no_stencil : 1;

  pString rationale;

  const int n_min = 1;  // For tuning.
  if ( false && !user_tile && kc.shared_var_count )
    {
      // WARNING: This code is bitrotted. For the moment execute the
      // non-shared variable code, but note that it can choose a tile
      // size that is not executable due to shared memory usage.
      // Shared memory functionality will be fixed soon by Yue Hu.

      const int shared_memory_goal = 3 << 14;
      const int stncl_n[] = {stncl_xn,stncl_yn};
      const int stncl_p[] = {stncl_xp,stncl_yp};
      const int cctk_lsh[] = {klp.cagh_ni,klp.cagh_nj};
      int thd_cnt_max = 0;
      const int dir = stncl_xn + stncl_xp >= stncl_yn + stncl_yp ? 0 : 1;
      const int stncl_rn = stncl_n[dir];
      const int stncl_rp = stncl_p[dir];
      const int stncl_qn = stncl_n[1-dir];
      const int stncl_qp = stncl_p[1-dir];
      const int r_length = cctk_lsh[dir] - stncl_rn - stncl_rp;
      const int q_length = cctk_lsh[1-dir] - stncl_qn - stncl_qp;
      const int r_min_line =  dir ? 1 : x_min_line;
      const int q_min_line = !dir ? 1 : x_min_line;
      const int n_start = iDivUp ( r_length, block_thread_limit_w_shared );
      rationale = "shared";

      for ( int n = max(n_min,n_start); ; n++ )
        {
          const int tile_r =
            dir ? r_length - n + 1 : iDivUp( r_length, n );
          if ( tile_r < r_min_line && tile_x ) break;
          const int tile_q_max_sm =
            shared_memory_goal /
            ( kc.shared_var_count * sizeof(CCTK_REAL)
              * ( tile_r + stncl_rn + stncl_rp )
              * ( 1 + stncl_zn + stncl_zp ) )
            - stncl_qn - stncl_qp;
          if ( !tile_q_max_sm ) continue;
          const int q_max_occ = block_thread_limit_w_shared / tile_r;

          int tile_q1 = min(tile_q_max_sm, q_max_occ);
          int tile_q, tile_zi;

          if ( true || !stncl_z || tile_q1 < 4 )
            {
              tile_zi = 1;
              tile_q1 = min( tile_q1, q_length );
              const int m = iDivUp( q_length, tile_q1 );
              tile_q = dir ? q_length / m : tile_q1;
            }
          else
            {
              tile_q = ceilf( sqrtf( tile_q1 ) );
              tile_zi = tile_q1 / tile_q;
            }

          if ( tile_q < q_min_line && tile_x ) continue;
          if ( tile_q > tile_r && tile_x ) break;
          const int thd_cnt = tile_q * tile_r * tile_zi;
          if ( thd_cnt > thd_cnt_max )
            {
              thd_cnt_max = thd_cnt;
              tile_x = dir ? tile_q : tile_r;
              tile_y = dir ? tile_r : tile_q;
              tile_z = tile_zi;
            }
          if ( tile_r == 1 ) break;
        }
    }
  else
    {
      const int n_start = iDivUp( x_length, block_thread_limit_wo_shared );
      const int tile_z_limit = 800;
      const double mp_occ_util_factor = 4;

      int tile_candidate_count = 0;

      double score_max = 1e10;

      string annoying_prefix("CAKERNEL_DEVICE_");
      string func_name = kc.kernel_function_name;
      if ( func_name.find(annoying_prefix) == 0 )
        func_name = func_name.substr(annoying_prefix.length());

      dm->msg_tune("Targeting %d/%d warps trying configs for %s\n",
                   warp_limit, prefer_factors, func_name.c_str());

      int z_factors[z_length];
      int zf_idx = 0;
      int f_skips = 0;
      for ( int i=1; i<z_length; i++ )
        {
          if ( f_skips < 2 && z_length % i ) { f_skips++; continue; }
          f_skips = 0;
          z_factors[++zf_idx] = i;
        }
      z_factors[++zf_idx] = 100000;
      zf_idx = 0;
      for ( int i=1; i<y_length; i++ )
        {
          if ( f_skips < 2 && y_length % i ) { f_skips++; continue; }
          f_skips = 0;
        }

      for ( int n = max(n_min,n_start); ; n++ )
        {
          const int tile_xi =
            user_tile ? min(x_length,kus->tile_x) : iDivUp( x_length, n );
          if ( !user_tile && tile_xi <= x_min_line && tile_x ) break;
          const int tile_yz =
            user_tile ? kus->tile_y * kus->tile_z
            : block_thread_limit_wo_shared / tile_xi;
          if ( tile_yz == 0 ) continue;

          for ( int zidx = 1; zidx <= tile_yz; zidx++ )
            {
              const int tile_zi =
                user_tile ? min(z_length,kus->tile_z) :
                prefer_factors ? z_factors[zidx] : zidx;
              if ( !user_tile && tile_zi > tile_z_limit
                   || tile_zi > tile_yz ) break;

              const int tile_yi_limit = tile_yz / tile_zi;

              for ( int yidx = 1; yidx <= tile_yi_limit; yidx++ )
                {
                  const int tile_yi =
                    user_tile ? min(y_length,kus->tile_y) : yidx;

                  pString rat_occs;
                  int case_block_cnt_max = 0;

                  const int dir =
                    user_tile ? ( kus->tile_yy >= kus->tile_zz ? 1 : 2 ) :
                    tile_zi > tile_yi ? 1 : 2;

                  const int tile_yyi =
                    user_tile ? kus->tile_yy : dir == 1 ? 4 : 1;
                  const int tile_zzi =
                    user_tile ? kus->tile_zz : dir == 2 ? 4 : 1;

                  tile_candidate_count++;

                  const int tile_xp = x_length % tile_xi;
                  const int x_blocks = x_length / tile_xi; // Complete blocks.
                  const bool x_partial = tile_xp;
                  const int tile_yyy = tile_yi * tile_yyi;
                  const int y_blocks = y_length / tile_yyy;
                  const int tile_yyyp = y_length % tile_yyy;
                  const int tile_yyp = tile_yyyp / tile_yi; // Full iterations.
                  const int tile_yp = tile_yyyp % tile_yi;
                  const bool y_partial = tile_yyyp;

                  const int tile_zzz = tile_zi * tile_zzi;
                  const int z_blocks = z_length / tile_zzz;
                  const int tile_zzzp = z_length % tile_zzz;
                  const int tile_zzp = tile_zzzp / tile_zi; // Full iterations.
                  const int tile_zp = tile_zzzp % tile_zi;
                  const bool z_partial = tile_zzzp;

                  // If true, block (tile) iterates in y direction.
                  const bool iter_dir_y = tile_yyi > 1 || tile_zzi == 1;
                  // If true, block (tile) iterates in z direction.
                  const bool iter_dir_z = tile_zzi > 1;

                  double misses_tot = 0;
                  double misses_per_grid_point_tot = 0;
                  double misses_per_grid_point_upto_warp_tot = 0;
                  double score_total = 0;
                  int case_block_total = 0;
                  int grid_points_total = 0;

                  // Iterate over Block Completeness Cases
                  //
                  // case_vec = 0 : 
                  //    No partial execution. That is, block handles
                  //    tile_xxx * tile_yyy * tile_zzz elements.
                  // case_vec = 1 :
                  //    Partial execution in x direction, but
                  //    no partial execution in y and z directions.
                  //    Block handles tile tile_xp * tile_yyy * tile_zzz
                  //    elements.
                  // Et cetera.
                  //
                  for ( int case_vec = 0; case_vec < 8; case_vec++ )
                    {
                      const bool case_xp = case_vec & 1;  // Partial in x dir.
                      const bool case_yp = case_vec & 2;
                      const bool case_zp = case_vec & 4;
                      const int case_block_cnt =
                        ( case_xp ? int(x_partial) : x_blocks ) *
                        ( case_yp ? int(y_partial) : y_blocks ) *
                        ( case_zp ? int(z_partial) : z_blocks );

                      // Check whether this particular case is unnecessary
                      // because the number of elements in the case
                      // direction is a multiple of the tile size in
                      // that direction.  For example, if case_yp is
                      // true that means for this case we consider a partial
                      // block in the y direction. But if tile_yyy is
                      // a multiple y_length there won't be any partial
                      // blocks in the y direction.
                      if ( !case_block_cnt ) continue;

                      // Note: "c" in suffixes means "case".

                      // Number of iterations for this case.
                      //
                      const int tile_yyc = case_yp ? tile_yyp : tile_yyi;
                      const int tile_zzc = case_zp ? tile_zzp : tile_zzi;

                      // Number of iterations in which all threads
                      // along the direction of iteration are used.
                      //
                      const int tile_iterc = iter_dir_y ? tile_yyc : tile_zzc;

                      // Number of elements in x direction for this case.
                      //
                      const int tile_xc = case_xp ? tile_xp : tile_xi;

                      const int tile_yc_iter =
                        case_yp && !iter_dir_y ? tile_yp : tile_yi;
                      const int tile_zc_iter =
                        case_zp && !iter_dir_z ? tile_zp : tile_zi;

                      const int tile_yc_epi =
                        case_yp ? tile_yp : iter_dir_y ? 0 : tile_yi;
                      const int tile_zc_epi =
                        case_zp ? tile_zp : iter_dir_z ? 0 : tile_zi;

                      assert( tile_yc_epi != 0 || tile_iterc );
                      assert( tile_zc_epi != 0 || tile_iterc );

                      // True if tile length is partial along the
                      // direction of iteration (requiring an epilogue
                      // iteration).
                      //
                      const bool need_epi =
                        iter_dir_y && case_yp && tile_yc_epi
                        || iter_dir_z && case_zp && tile_zc_epi;

                      // Number of iterations including the epilogue
                      // iteration (if needed). In the epilogue
                      // iteration at least one but not all threads
                      // along the direction of iteration are active.
                      //
                      const int tile_iterc_up =
                        need_epi ? tile_iterc + 1 : tile_iterc;

                      const int x_skipped_thds = tile_xi - tile_xc;
                      const int tile_xc_skipped_upto_warps =
                        x_skipped_thds & ~wp_mask;
                      const int tile_xc_upto_warp =
                        tile_xi - ( tile_xc_skipped_upto_warps ?
                                    tile_xc_skipped_upto_warps - wp_sz
                                    + ( x_skipped_thds & wp_mask ) + 1 : 0 );

                      // Average number of active threads.
                      //
                      const int thd_cnt_avg =
                        tile_xc_upto_warp *
                        ( tile_iterc * tile_yc_iter * tile_zc_iter
                          + tile_yc_epi * tile_zc_epi ) / ( tile_iterc + 1 );
                    
                      // Average number of active warps.
                      //
                      const int warp_cnt = thd_cnt_avg + wp_mask >> wp_lg;

                      // Crude estimate of utilization based only on
                      // the number of active warps.
                      //
                      const double tile_thread_limit_util =
                        1.0 / ( 1.0 + mp_occ_util_factor / warp_cnt );
                      const double thread_limit_util = tile_thread_limit_util;

                      // Adjustment to number of cache lines.

                      const int x_core_mod = tile_xi & line_real_mask;

                      // Number of extra lines needed in x direction.
                      //
                      // Lazy because I'm not accounting for tile_xi
                      // sharing a factor with real_per_line (other than
                      // being an exact multiple).
                      const double x_core_lazy_extra_frac =
                        x_core_mod == 0 ? ( stncl_xn ? 1.0 : 0.0 ) :
                        tile_xi >= x_length >> 1 ? 0.0 :
                        x_core_mod * real_per_line_inv;
                      const double x_sten_lazy_extra_frac =
                        x_core_mod == 0 ? 0.0 :
                        tile_xi >= x_length >> 1 ? 0.0 :
                        ( x_core_mod + stncl_x ) * real_per_line_inv;

                      const double tile_x_len_lines =
                        ( tile_xc + real_per_line - 1 >> real_per_line_lg )
                        + x_core_lazy_extra_frac;

                      const double tile_x_sten_len_lines =
                        ( tile_xc + stncl_x + real_per_line - 1
                          >> real_per_line_lg ) + x_sten_lazy_extra_frac;

                      // Size of core accounting for complete iterations.
                      //
                      const double tile_core_size_lines_iter =
                        tile_x_len_lines * tile_yc_iter * tile_zc_iter;

                      // Size of core contributed by any partial iteration.
                      //
                      const double tile_core_size_lines_epi =
                        tile_x_len_lines * tile_yc_epi * tile_zc_epi;

                      // Size contributed by stencil in x direction, during
                      // complete iterations.
                      //
                      const double tile_x_sten_size_lines_iter =
                        ( tile_x_sten_len_lines - tile_x_len_lines )
                        * tile_yc_iter * tile_zc_iter;

                      // Size contributed by stencil in x direction, by
                      // any needed epilogue iterations.
                      //
                      const double tile_x_sten_size_lines_epi =
                        ( tile_x_sten_len_lines - tile_x_len_lines )
                        * tile_yc_epi * tile_zc_epi;

                      // Size contributed by stencil in y and z directions,
                      // for complete (_iter) and epilogue (_epi) iterations.
                      //
                      const double tile_y_sten_size_lines_iter =
                        tile_x_len_lines * stncl_y * tile_zc_iter;
                      const double tile_z_sten_size_lines_iter =
                        tile_x_len_lines * tile_yc_iter * stncl_z;
                      const double tile_y_sten_size_lines_epi =
                        tile_x_len_lines * stncl_y * tile_zc_epi;
                      const double tile_z_sten_size_lines_epi =
                        tile_x_len_lines * tile_yc_epi * stncl_z;

                      // Total size considering complete iterations.
                      //
                      const double tile_size_lines_iter =
                        tile_core_size_lines_iter
                        + tile_x_sten_size_lines_iter
                        + tile_y_sten_size_lines_iter
                        + tile_z_sten_size_lines_iter;

                      // Total size considering epilog iteration.
                      //
                      const double tile_size_lines_epi =
                        tile_core_size_lines_epi
                        + tile_x_sten_size_lines_epi
                        + tile_y_sten_size_lines_epi
                        + tile_z_sten_size_lines_epi;

                      // Length in y and z directions for a single
                      // iteration, for both a complete iteration and
                      // an epilogue iteration.
                      //
                      const int tile_yi_len_w_sten_iter =
                        tile_yc_iter + stncl_y;
                      const int tile_zi_len_w_sten_iter =
                        tile_zc_iter + stncl_z;
                      const int tile_yi_len_w_sten_epi = tile_yc_epi + stncl_y;
                      const int tile_zi_len_w_sten_epi = tile_zc_epi + stncl_z;

                      // Size in lines for a complete and an epilogue
                      // iteration considering only the stencil in the
                      // x, y, and z directions.
                      //
                      const double tile_size_xdir_iter =
                        tile_x_sten_len_lines * tile_yc_iter * tile_zc_iter;
                      const double tile_size_ydir_iter =
                        tile_x_len_lines * tile_yi_len_w_sten_iter
                        * tile_zc_iter;
                      const double tile_size_zdir_iter =
                        tile_x_len_lines * tile_yc_iter
                        * tile_zi_len_w_sten_iter;
                      const double tile_size_xdir_epi =
                        tile_x_sten_len_lines * tile_yc_epi * tile_zc_epi;
                      const double tile_size_ydir_epi =
                        tile_x_len_lines * tile_yi_len_w_sten_epi * tile_zc_epi;
                      const double tile_size_zdir_epi =
                        tile_x_len_lines * tile_yc_epi * tile_zi_len_w_sten_epi;

                      // Tile size to use when computing cache
                      // occupancy. If there is at least one full
                      // iteration, use the complete iteration size,
                      // otherwise use the epilogue size.
                      //
                      const double tile_size_lines_ref =
                        tile_iterc ? tile_size_lines_iter : tile_size_lines_epi;
                      const double tile_size_xdir_ref =
                        tile_iterc ? tile_size_xdir_iter : tile_size_xdir_epi;
                      const double tile_size_ydir_ref =
                        tile_iterc ? tile_size_ydir_iter : tile_size_ydir_epi;
                      const double tile_size_zdir_ref =
                        tile_iterc ? tile_size_zdir_iter : tile_size_zdir_epi;

                      // Fraction of cache occupied by access to all
                      // stencil grid functions for one complete
                      // iteration.
                      //
                      const double sten_iteration_occ =
                        cache_size_lines
                        / double ( gf_sten_count * tile_size_lines_ref );

                      // Fraction of cache occupied by access to one
                      // stencil grid function for one complete
                      // iteration.
                      //
                      const double sten_gf_occ =
                        cache_size_lines / double(tile_size_lines_ref);

                      // Fraction of cache used by access to one grid
                      // function with offsets only in x, y, or z
                      // directions (the x, y, or z stencils).
                      //
                      const double dim_x_occ =
                        cache_size_lines / double(tile_size_xdir_ref);
                      const double dim_y_occ =
                        cache_size_lines / double(tile_size_ydir_ref);
                      const double dim_z_occ =
                        cache_size_lines / double(tile_size_zdir_ref);
                      const double dim_min_occ =
                        min(dim_x_occ,min(dim_y_occ,dim_z_occ));

                      // Number of cold misses for grid-point
                      // (zero-offset a.k.a. non-stencil) accesses.
                      //
                      const int misses_core =
                        gf_core_count *
                        ( tile_iterc * tile_core_size_lines_iter
                          + tile_core_size_lines_epi );

                      // Number of misses assuming each y- and z-axis
                      // offset access is a miss, and assuming that
                      // x-axis accesses only suffer cold misses.
                      //
                      const int misses_worst_case =
                        gf_sten_count *
                        ( max(0, tile_iterc - 1 ) *
                          ( tile_size_xdir_iter
                            + stncl_y * tile_size_ydir_iter
                            + stncl_z * tile_size_zdir_iter )
                          + ( tile_size_xdir_epi
                              + stncl_y * tile_size_ydir_epi
                              + stncl_z * tile_size_zdir_epi ) );

                      // Number of misses assuming that in a sequence
                      // such as
                      //
                      //  GF(0,1,0) + GF(0,2,0) + GF(0,3,0)
                      //  + GF(1,0,0) + GF(2,0,0) + GF(3,0,0)
                      //
                      // There will be two misses, one for each dimension,
                      // where GF(x,y,z) is access to a grid function (variable)
                      // at offset x,y,z.
                      //
                      const int misses_dim_fits =
                        gf_sten_count *
                        ( tile_iterc *
                          ( tile_size_xdir_iter
                            + tile_size_ydir_iter + tile_size_zdir_iter )
                          + ( tile_size_xdir_epi
                              + tile_size_ydir_epi + tile_size_zdir_epi ) );

                      const int misses_gf_fits =
                        gf_sten_count *
                        ( tile_iterc * tile_size_lines_iter
                          + tile_size_lines_epi );

                      assert( dir != 0 );
                      const int tile_t_iter =
                        dir == 0 ? 0 :
                        dir == 1 ? tile_yc_iter : tile_zc_iter;
                      const int tile_rs =
                        dir == 0
                        ? tile_yi_len_w_sten_iter * tile_zi_len_w_sten_iter :
                        dir == 1
                        ? tile_x_sten_len_lines * tile_zi_len_w_sten_epi
                        : tile_x_sten_len_lines * tile_yi_len_w_sten_epi;

                      // Number of Cache Lines Needed (misses) for Best Case
                      //
                      // The first iteration brings tile_size_lines
                      // lines. On subsequent iterations only the new
                      // lines in the direction of iteration are
                      // needed.
                      //
                      const int misses_iter_fits =
                        gf_sten_count
                        * ( ( tile_iterc
                              ? tile_size_lines_iter : tile_size_lines_epi )
                            + ( max(0, tile_iterc_up - 1 )
                                * tile_t_iter * tile_rs ) );

                      const double misses_sten =
                        sten_iteration_occ >= 1.0
                        ? misses_iter_fits : 
                        sten_gf_occ >= 1.0
                        ? ( sten_iteration_occ * misses_iter_fits
                            + ( 1 - sten_iteration_occ ) * misses_gf_fits ) : 
                        dim_min_occ >= 1.0
                        ? ( sten_gf_occ * misses_gf_fits
                            + ( 1 - sten_gf_occ ) * misses_dim_fits ) :
                        dim_min_occ * misses_dim_fits +
                        ( 1 - dim_min_occ ) * misses_worst_case;

                      const double misses_case = misses_sten + misses_core;

                      const int grid_points_case =
                        tile_xc * tile_yc_iter * tile_zc_iter * tile_iterc +
                        tile_xc * tile_yc_epi * tile_zc_epi;

                      // The number of grid points accessed rounding up
                      // to a multiple of the warp size.
                      //
                      const int grid_points_upto_warp_case =
                        tile_xc_upto_warp *
                        ( tile_yc_iter * tile_zc_iter * tile_iterc
                          + tile_yc_epi * tile_zc_epi );

                      const double misses_per_grid_point_case =
                        real_per_line * misses_case / grid_points_case;

                      const double misses_per_grid_point_upto_warp_case =
                        real_per_line * misses_case
                        / grid_points_upto_warp_case;

                      assert( misses_per_grid_point_case >= 0.9999 );

                      const double score_case =
                        misses_per_grid_point_case / thread_limit_util;

                      misses_tot += misses_case * case_block_cnt;
                      misses_per_grid_point_tot +=
                        misses_per_grid_point_case * case_block_cnt;
                      misses_per_grid_point_upto_warp_tot +=
                        misses_per_grid_point_upto_warp_case * case_block_cnt;
                      score_total += score_case * case_block_cnt;
                      case_block_total += case_block_cnt;
                      grid_points_total += grid_points_case * case_block_cnt;
                      assert( grid_points_total <= grid_points_count );

                      if ( dm->myproc != 0 ) continue;
                      if ( case_block_cnt > case_block_cnt_max )
                        {
                          case_block_cnt_max = case_block_cnt;
                          pStringF
                            rat("occ %.2f/%.1f/%.1f C%d",
                                sten_iteration_occ, sten_gf_occ, dim_min_occ,
                                case_vec);
                          rat_occs.assign_take(rat.remove());
                        }

                      const bool show_cases = false;
                      if ( show_cases )
                        dm->msg_tune
                          ("Cons %2d x %2d x %2d dir %d "
                           "occ %.1f/%.1f/%.1f "
                           "m/g %.2f %.2f sc %.3f  %.3f  C%d * %d\n",
                           tile_xc, case_yp ? tile_yp : tile_yi,
                           case_zp ? tile_zp : tile_zi, dir,
                           sten_iteration_occ, sten_gf_occ, dim_min_occ,
                           misses_per_grid_point_case,
                           misses_per_grid_point_upto_warp_case,
                           score_case,
                           thread_limit_util,
                           case_vec, case_block_cnt
                           );
                    }

                  const double misses_per_grid_point =
                    misses_per_grid_point_tot / case_block_total;
                  const double misses_per_grid_point_upto_warp =
                    misses_per_grid_point_upto_warp_tot / case_block_total;
                  const double score = score_total / case_block_total;
                  assert( grid_points_total == grid_points_count );

                  if ( !user_tile && score >= score_max ) 
                    continue;
                  score_max = score;
                  lc.score = score;
                  tile_x = tile_xi;
                  tile_y = tile_yi;
                  tile_z = tile_zi;
                  tile_yy = tile_yyi;
                  tile_zz = tile_zzi;
#if 1
                  pStringF rat("%s m/g %.2f %.2f %.4f sc %.3f",
                               rat_occs.s, misses_per_grid_point,
                               misses_per_grid_point_upto_warp,
                               misses_per_grid_point_upto_warp / 7,
                               score);
                  rationale.assign_take(rat.remove());
#endif
                  
                  if ( 0 )
                    dm->msg_tune("Considering %2dx%2dx%2d %2d/%2d %s "
                           "m/g %.3f sc %.3f  %.3f\n",
                           tile_x, tile_y, tile_z,
                           tile_yy, tile_zz,
                           rat_occs.s,
                           misses_per_grid_point, score,
                           0.0);
                  dm->msg_tune("  \"%2d,%2d,%2d,  %2d,%2d\", # %s "
                           "m/g %.3f sc %.3f \n",
                           tile_x, tile_y, tile_z,
                           tile_yy, tile_zz,
                           rat_occs.s,
                           misses_per_grid_point, score);

                  if ( user_tile ) break;
                }
              if ( user_tile ) break;
            }
          if ( user_tile ) break;
        }

      dm->msg_tune("Considered %d tile candidates.\n",tile_candidate_count);
    }


  if ( !tile_x )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Could not find a suitable tile size for kernel %s, "
       "possibly due to shared memory.\n"
       "The kernel needs shared memory for %d grid functions.\n",
       kc.kernel_name, kc.shared_var_count);

  const int tile_yyy = tile_y * tile_yy;
  const int tile_zzz = tile_z * tile_zz;

  //
  // Prepare grid hierarchy values.
  //

  klp.tile_x = tile_x;
  klp.tile_y = tile_y;
  klp.tile_z = tile_z;
  klp.tile_yy = tile_yy;
  klp.tile_zz = tile_zz;
  klp.cagh_blocky =  iDivUp(y_length, tile_yyy);
  klp.block_size = tile_x * tile_y * tile_z;
  klp.gf_use_offsets = kc.gf_use_offsets;

  const int shared_elt_per_gf = 
    (tile_x + stncl_x) * (tile_y + stncl_y) * (tile_z + stncl_z);
  const int shared_size = dm->gpu_info.shared_sz_per_bl_bytes;
  const int max_shared_gf = 
    shared_size / ( shared_elt_per_gf * elt_size_assumed );

  klp.shared_gf_target = max_shared_gf;

  lc.l1_local_only = restrict_l1;
  lc.grid_x = iDivUp( x_length, tile_x );
  lc.grid_y = iDivUp( z_length, tile_zzz ) * klp.cagh_blocky;
  lc.unroll_main = !restrict_l1 && 
    ( stncl_y && tile_yy > 1 || stncl_z && tile_zz > 1 );
  lc.rationale = rationale;

  if ( dm->myproc == 0 && dynamic_compile_show_tuning_decisions )
    {
      pString round_txt;
      if ( user_tile )
        round_txt.sprintf
          ("user tile matching %s", kus->name_match_pattern.c_str());
      else
        round_txt.sprintf("Round %d",config_decide_iteration);

      CCTK_VInfo
        (CCTK_THORNSTRING,
         "** Kernel %s Dynamic Tuning Decisions (%s) **",
         kc.kernel_name, round_txt.s);
      CCTK_VInfo
        (CCTK_THORNSTRING,
         "Tile Size: %d x %d x %d. (/%d/%d)  "
         "Stencil Size:  %d/%d x %d/%d x %d/%d  GF (%d,%d,%d)  SH (%d)",
         tile_x, tile_y, tile_z,
         tile_yy, tile_zz,
         stncl_xn, stncl_xp, stncl_yn, stncl_yp, stncl_zn, stncl_zp,
         gf_in_stencil, gf_in_no_stencil, kc.gf_out_count,
         klp.shared_gf_target);

      CCTK_VInfo
        (CCTK_THORNSTRING,
         "Restrict L1 is %s because\n"
         "     GF count, %d, %s l1_restrict_gf_min, %d.\n"
         "     Since it is %s L1 cache can be used by %s accesses.",
         restrict_l1 ? "on" : "off",
         kc.gf_count,
         restrict_l1 ? ">=" : "<",
         dm->param_l1_restrict_gf_min,
         restrict_l1 ? "on" : "off",
         restrict_l1 ? "local but not global" : "local and global");
      if ( !user_tile )
        CCTK_VInfo
          (CCTK_THORNSTRING,
           "Active warps forced to <= %d because %s.",
           kc.shared_var_count ? tile_y :
           restrict_l1 ? dm->param_warps_l1_limited_max :
           dm->param_warps_max,
           kc.shared_var_count ? "of shared memory use" :
           restrict_l1 ? "restrict L1 is on and so limit is set to parameter ..warps_l1_limited_max" :
           "restrict L1 is off and so limit is set to parameter  ..warps_max");
    }
}

void
Module_Info::kernel_launch_params_set_or_reset(bool reset)
{
  ostringstream repl;
  const char* const real_type = sizeof(CCTK_REAL) == 8 ? "double" : "float";
#define MI(m) Mg("int",m)
#define MF(m) Mg(real_type,m)
#define Mg(t,m) \
  repl << "const " << t << " params_" << #m << " = " << klp.m << ";\n";
  MI(cagh_ni); MI(cagh_nj); MI(cagh_nk);
  MI(cagh_blocky);
  MF(cagh_dx); MF(cagh_dy); MF(cagh_dz); MF(cagh_dt);
  MF(cagh_xmin); MF(cagh_ymin); MF(cagh_zmin);
  MI(bwid_xn); MI(bwid_xp); MI(bwid_yn); MI(bwid_yp); MI(bwid_zn); MI(bwid_zp);
  MI(i_length_x); MI(i_length_y); MI(i_length_z);
  MI(cagh_boundsPhys);
  MI(tile_x); MI(tile_y); MI(tile_z);
  MI(tile_yy); MI(tile_zz);
  MI(block_size);
  MI(gf_use_offsets);
#undef MI
#undef MF
#undef Mg

  // Specify values for grid hierarchy variables.
  dm->module_vals_set_or_reset(STR(REPL_KEY_GRID_HIERARCHY),repl.str(),reset);
}

void
Module_Info::launch(void *args[], CCTK_ARGUMENTS)
{
  Chemora_CG_Calc* const c = 
    Chemora_CG_get_calc_try(kc.thorn_name,kc.kernel_name);

  if ( c ) chemora_launch(args,CCTK_PASS_CTOC);
  else     cakernel_launch(args,CCTK_PASS_CTOC);
}

void
Module_Info::cakernel_launch(void *args[], CCTK_ARGUMENTS)
{
  const bool compile_only =
    *(int *)CCTK_VarDataPtr(cctkGH, 0, "CaKernel::compile_only");

  Function_Info* const fi = dm->function_info_get(kc.kernel_function_name);

  if ( klp.tile_x == -1 )
    {
      assert( !fi->inited );
      fi->total_time_all_ms = 0;
      fi->total_time_notrace_ms = 0;
      CE( cuEventCreate( &fi->launch_end_ev, CU_EVENT_BLOCKING_SYNC) );
      launch_config_decide();
    }

  const int sm_usage = fi->static_shared_memory_get();

  if ( compile_only ) return;

  const int cache_small_size = 1 << 14;
  const int force_sm_amount_one_block =
    sm_usage <= cache_small_size ? cache_small_size : 3 * cache_small_size;
  const int force_sm_amount =
    kc.shared_var_count ? 0 : force_sm_amount_one_block - sm_usage;

  CE( cuCtxSetCacheConfig( CU_FUNC_CACHE_PREFER_L1 ) );
  CE( cuCtxSetSharedMemConfig
      ( sizeof(CCTK_REAL) == 8
        ? CU_SHARED_MEM_CONFIG_EIGHT_BYTE_BANK_SIZE
        : CU_SHARED_MEM_CONFIG_FOUR_BYTE_BANK_SIZE ) );

  CUstream stream = NULL;

  CE( cuEventRecord( launch_start_ev, stream ) );

  CE( cuLaunchKernel
      (fi->cu_function_get(), lc.grid_x, lc.grid_y, 1,
       klp.block_size, 1, 1,
       force_sm_amount, // Dynamic shared memory
       NULL, // Stream
       args, //  kernel_args,
       NULL) );

  CE( cuEventRecord( fi->launch_end_ev, stream ) );
  CE( cuEventSynchronize(fi->launch_end_ev) );
  float et_ms = -1;
  CE( cuEventElapsedTime( &et_ms, launch_start_ev, fi->launch_end_ev ) );
  assert( et_ms >= 0 );
  dm->report_total_et += et_ms;

  CE( cuCtxSetCacheConfig( CU_FUNC_CACHE_PREFER_SHARED ) );
}

void
Module_Info::chemora_launch(void *args[], CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  Chemora_CG_Calc* const c = 
    Chemora_CG_get_calc_try(kc.thorn_name,kc.kernel_name);

  const bool debug = false;

  const bool compile_only =
    *(int *)CCTK_VarDataPtr(cctkGH, 0, "CaKernel::compile_only");

  const bool target_cuda = !c->code_target_acc_get();

  c->module_info_set(this);

  if ( !module_loaded )
    {
      c->set_kernel_launch_parameters(&klp);
      c->calc_finalize();

      dm->module_load();
      dm->module_generate_assembler();

      for ( FIter fi(function_info); fi; )
        {
          fi->inited = true;
          fi->total_time_all_ms = 0;
          fi->total_time_notrace_ms = 0;
          CE( cuEventCreate( &fi->launch_end_ev, CU_EVENT_BLOCKING_SYNC) );
          if ( target_cuda )
            {
              CE( cuModuleGetFunction
                  ( &fi->cu_function, cu_module, fi->function_name.c_str() ) );
              const int max_threads = fi->max_threads_get();
              const bool problem = fi->klp.block_size > max_threads;
              assert( debug || !problem );
            }
          else
            {
              // OpenACC
              fi->c_function =
                (Func_Calc) dlsym(dll_handle,fi->function_name.c_str());
              if ( !fi->c_function )
                CCTK_VWarn
                  (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
                   "Could not find symbol %s in dynamically "
                   "linked code from %s ...\n... %s\n",
                   fi->function_name.c_str(), dynamic_out_files_base.c_str(),
                   dlerror());
            }
          fi->copied = true;
        }
      c->report_compile_print();
    }

  //
  // Prepare grid hierarchy values.
  //

  if ( compile_only ) return;

  CE( cuCtxSetCacheConfig( CU_FUNC_CACHE_PREFER_SHARED ) );

  CE( cuCtxSetSharedMemConfig
      ( sizeof(CCTK_REAL) == 8
        ? CU_SHARED_MEM_CONFIG_EIGHT_BYTE_BANK_SIZE
        : CU_SHARED_MEM_CONFIG_FOUR_BYTE_BANK_SIZE ) );

  const int gf_size_elts = klp.cagh_ni * klp.cagh_nj * klp.cagh_nk;
  const int gf_size_bytes = gf_size_elts * sizeof( CCTK_REAL );

  Chemora_CG_Thorn* const ct = c->thorn_get();

  Chemora_CG_Module_Data* const modd = c->module_data_get(this);
  assert( modd->mi == this );
  assert( modd->gf_size_bytes <= gf_size_bytes );

  CUcontext cu_context;
  CUdevice cu_device;
  CE( cuCtxGetCurrent( &cu_context ) );
  CE( cuCtxGetDevice( &cu_device ) );

  assert( dm->cu_context == cu_context );
  assert( dm->cu_device == cu_device );

  const int event_tracing_prev = NPerf_event_tracing_get();
  const bool no_tr = event_tracing_prev && bool(random() & 1);
  if ( no_tr ) NPerf_event_tracing_off();
  const bool event_tracing_off = !NPerf_event_tracing_get();

  const bool debug_ik = false;
  if ( debug_ik && target_cuda )
    {
      pVector<CUdeviceptr> a = ct->gpu_alloc_addrs;
      for ( CUdeviceptr da; a.pop(da); )
        {
          CUdeviceptr pbase;
          size_t psize;
          CE( cuMemGetAddressRange(&pbase, &psize, da) );
          CE( cuMemsetD8( da, 0x40, psize ) );
        }
    }

  CUstream stream = NULL;

  CE( cuEventRecord( launch_start_ev, stream ) );
  if ( event_tracing_off ) num_launches_notrace++;
  num_launches++;
  CUevent last_event = launch_start_ev;

  c->launch_verify_addr_args(args);

  if ( target_cuda )
  for ( auto& pair : constant_gfs )
    {
      auto& cgf = pair.second;
      if ( cgf.constant && cgf.valid ) continue;
      CUdeviceptr gf_addr;
      size_t gf_sz;
      CE( cuModuleGetGlobal
          ( &gf_addr, &gf_sz,cu_module, cgf.ptx_symbol.c_str() ) );
      CE( cuMemcpyDtoD( gf_addr, CUdeviceptr(cgf.device_addr), gf_sz ) );
      cgf.valid = true;
    }

  for ( FIter fi(function_info); fi; )
    {
      if ( target_cuda )
        {
          int sh_sz = fi->static_shared_memory_get();
          int dsize = fi->klp.force_one_block_per_mp ? (3<<14)-sh_sz : 0;

          CE( cuLaunchKernel
              (fi->cu_function_get(), fi->lc.grid_x, fi->lc.grid_y, 1,
               fi->klp.block_size, 1, 1,
               dsize, // Dynamic shared memory
               stream, // Stream
               args, //  kernel_args,
               NULL) );
        }
      else
        {
          // Open ACC
          fi->c_function(args);
        }

      CUcontext cu_context_after;
      CE( cuCtxGetCurrent( &cu_context_after ) );
      assert( dm->cu_context == cu_context_after );

      CE( cuEventRecord( fi->launch_end_ev, stream ) );
      last_event = fi->launch_end_ev;
    }

  CE( cuEventSynchronize(last_event) );

  last_event = launch_start_ev;

  for ( FIter fi(function_info); fi; )
    {
      float et_ms = -1;
      CE( cuEventElapsedTime( &et_ms, last_event, fi->launch_end_ev ) );
      assert( et_ms >= 0 );
      fi->total_time_all_ms += et_ms;
      if ( event_tracing_off ) fi->total_time_notrace_ms += et_ms;
      last_event = fi->launch_end_ev;
    }

  CE( cuCtxSetCacheConfig( CU_FUNC_CACHE_PREFER_SHARED ) );
  NPerf_event_tracing_set(event_tracing_prev);
}
