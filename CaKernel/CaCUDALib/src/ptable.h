// -*- c++ -*-
#ifndef PTABLE_H
#define PTABLE_H

#include "pstring.h"

class pTable {
public:

  pTable():num_lines(0),line(""),body(""){};

  int num_lines;
  pString line, body;
  enum Justification { pT_Unset, pT_Left, pT_Center, pT_Right };

  struct Col_Info {
    Col_Info(int s,int w,Justification j, const char *sp,const char *h)
      :start(s),width(w),just(j),sep(sp),header(h){};
    const int start;
    const int width;
    const Justification just;
    const string sep;
    const string header;
  };
  vector<Col_Info> col_info;

  void row_start()
  {
    row_end();
    num_lines++;
  }
  void row_end()
  {
    if ( !num_lines ) return;
    if ( !line.len() ) return;
    if ( !body.len() )
      {
        vector<string> hls = { string("") };
        for ( auto& ci: col_info )
          {
            if ( hls.back().length() > 0 ) hls.emplace_back("");
            int pos_start =
              ( ci.just == pT_Left ? 0 :
                ci.just == pT_Center ? ( ci.width - ci.header.length() ) >> 1 :
                ci.width - ci.header.length() )
              + ci.start - ci.sep.length();
            int pos_search = max(0,pos_start);
            auto hli = find_if
              ( hls.begin(), hls.end(),
                [&](const string s){ return s.length() <= pos_search; } );
            string& hl = *hli;
            int pad = pos_search - hl.length() + ci.sep.length();
            hl += string(pad,' ') + ci.header;
          }
        if ( hls.back().length() == 0 ) hls.pop_back();
        for ( auto& hl: hls ) body = hl + "\n" + body;
        string ul("");
        for ( auto& ci: col_info )
          {
            int pad = ci.start - ul.length();
            ul += string(pad,' ');
            ul += string(ci.width,'-');
          }
        body += ul + "\n";
      }
    body += line + "\n";
    line = "";
  }
  template <typename T>
  void entry
  (const char* h, const char* fmt, T val, Justification jp = pT_Right)
  {
    pStringF row(fmt,val);
    const char* sep = line.len() ? " " : "";
    line += sep;
    const int pre_len = line.len();
    line += row;
    if ( num_lines != 1 ) return;
    Justification j1 =
      h[0] == '<' ? pT_Left :
      h[0] == '|' ? pT_Center :
      h[0] == '>' ? pT_Right : pT_Unset;
    const bool jset = j1 != pT_Unset;
    Justification j = jset ? j1 : jp;
    const char* const h1 = jset ? h + 1 : h;
    col_info.emplace_back(pre_len,row.len(),j,sep,h1);
  }
  void entry(const char* h, const char val)
  {
    pStringF fmt("%%%zdc",strlen(h));
    entry(h,fmt.s,val,pT_Left);
  }
  void entry(const char* h, const pString& val)
  {
    pStringF fmt("%%%zds",strlen(h));
    entry(h,fmt.s,val.s,pT_Left);
  }

  const char* body_get()
    {
      row_end();
      return body.s;
    }

};

#endif
