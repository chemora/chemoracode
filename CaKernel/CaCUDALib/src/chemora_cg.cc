 /*@@
   @file      chemora_cg.cc
   @date
   @author    David Koppelman
   @desc
   CUDA code generation based on EDL- or Kranc- generated intermediate
   format computation kernels.
   @enddesc
 @@*/

#include <stdio.h>
#include <stdarg.h>
#include <map>
#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <limits.h>
#include <errno.h>

#include <cctk.h>
#include <cctk_Parameters.h>
#include <cuda.h>
#include <time.h>

#include <nvidiaperf.h>

#include "chemora_cg.h"

#include "pstring.h"
#include "ptable.h"
#include "putil.h"

#include "CaCUDALib_driver_support.h"

size_t chemora_cg_overrun_amt = 0x100000;

#define BZERO(v) bzero(v,sizeof(v))

int prop_serial = 0;
typedef enum { ET_False, ET_Examined, ET_Added } Examined_To;

const int MAX_KERNELS = 1000000;

static int ctz(u_int32_t n) __attribute__((unused));
static int ctz(u_int64_t n) __attribute__((unused));
static int pop(u_int32_t n) __attribute__((unused));
static int pop(u_int64_t n) __attribute__((unused));
static int ctz(u_int32_t n) { return __builtin_ctz(n); }
static int ctz(u_int64_t n) { return __builtin_ctzl(n); }
static int pop(u_int32_t n) { return __builtin_popcount(n); }
static int pop(u_int64_t n) { return __builtin_popcountl(n); }
static int iDivUp(int a, int b){ return (a + b - 1) / b; }

// Find Last 1. Find minimum number of bits needed.
static int fl1(u_int32_t n) __attribute__((unused));
static int fl1(u_int32_t n) {return 8 * sizeof(n) - __builtin_clz(n);}
static int fl1(int32_t n) __attribute__((unused));
static int fl1(int32_t n) {return fl1(u_int32_t(n));}
static int fl1(u_int64_t n) __attribute__((unused));
static int fl1(u_int64_t n) {return 8 * sizeof(n) - __builtin_clzl(n);}

const double ZLO = -1e-6;

#define max(a,b) ((a)>=(b)?a:b)
template<typename T> const char* plural(T n) { return n == 1 ? "" : "s"; }

#define divup(a,b) ( ( (a) + (b) - 1 ) / (b) )
#define rndup(a,b) ( (b) * divup(a,b) )

template<typename T1, typename T2> bool
set_min(T1& lhs, T2 rhs)
{ if ( rhs < lhs ) { lhs = rhs; return true; } return false; }
template<typename T1, typename T2> bool
set_max(T1& lhs, T2 rhs)
{ if ( rhs > lhs ) { lhs = rhs; return true; } return false; }

A_Vector_Type AV(int kno) { return A_Vector_Type(1) << kno; }
R_Vector_Type RV(int src_pos) { return R_Vector_Type(1) << src_pos; }

#define STR_EXPAND(t) #t
#define STR(t) STR_EXPAND(t)

KNOIter::operator bool ()
{
  if ( !av ) return false;
  curr = ctz(av);
  av = av ^ AV(curr);
  return true;
}

RVIter::operator bool ()
{
  if ( !av ) return false;
  curr = ctz(av);
  av = av ^ RV(curr);
  return true;
}


 /// Hardcoded Machine Specifications -- For Development Only
const int Op_Cost_Trig = 10;
const double comp_to_comm = 22;
const double comm_to_comp = 1.0 / comp_to_comm;

static const int warp_lg = 5;
static const int warp_sz = 1 << warp_lg;

using namespace std;

bool
is_member(Nodes& nodes, NIter& ni)
{
  return nodes.member(ni);
}

bool
is_member(Nodes& nodes, Node *nd)
{
  return nodes.member(nd);
}

bool
is_member(Nodes& nodes, int idx)
{
  for ( NIter ni(nodes); ni; ) if ( ni->idx == idx ) return true;
  return false;
}

bool
nodes_compare(Nodes& na, Nodes& nb, bool silent_if_equal)
{
  map<int,Node*> na_map, nb_map;
  for ( NIter nd(na); nd; ) na_map[nd->idx] = nd;
  Nodes only_a, only_b, both;
  for ( NIter nd(nb); nd; ) nb_map[nd->idx] = nd;

  for ( map<int,Node*>::iterator it = na_map.begin();
        it != na_map.end(); ++ it )
    if ( nb_map.find(it->first) == nb_map.end() )
      only_a += it->second; else both += it->second;

  for ( map<int,Node*>::iterator it = nb_map.begin();
        it != nb_map.end(); ++ it )
    if ( na_map.find(it->first) == na_map.end() )
      only_b += it->second;

  const bool equal = !only_a && !only_b;

  if ( silent_if_equal && equal ) return true;

  printf(" Only a (%3zd): ",only_a.size());
  const int plimit = 10;
  size_t n = 0;
  for ( NIter nd(only_a); nd; )
    if ( n++ == plimit ) break; else printf(" %d",nd->idx);
  if ( n == only_a.size() ) printf("\n"); else printf(" ...\n");
  n = 0;
  printf(" Both   (%3zd): ",both.size());
  for ( NIter nd(both); nd; )
    if ( n++ == plimit ) break; else printf(" %d",nd->idx);
  if ( n == both.size() ) printf("\n"); else printf(" ...\n");
  n = 0;
  printf(" Only b (%3zd): ",only_b.size());
  for ( NIter nd(only_b); nd; )
    if ( n++ == plimit ) break; else printf(" %d",nd->idx);
  if ( n == only_b.size() ) printf("\n"); else printf(" ...\n");

  return equal;
}
bool nodes_compare(Nodes& na, Nodes& nb)
{ return nodes_compare(na,nb,false); }


Chemora_CG_Calc::Chemora_CG_Calc()
{
  mi = NULL;
  kus = NULL;
  gpu_info = NULL;
  config_version = 0;

  need_kernel_launch_parameters = true;

  set_real_type(CCTK_REAL(1.0));

  node_loop_start = node_new("* loop start", NT_Special);

  var_interm_next = 0;

  opt_wall_time_s = 0;
  opt_proposals = 0;
  opt_accepts = 0;

  opt_code_target_set = false;

}


 /// Delta Time (cagh_delta_time, dt) Hack
//
//   If module_key_includes_delta_time is false then Chemora will
//   assume that dt is not referenced in Kranc-generated code. If this
//   assumption is wrong execution will fail with a fatal error. If
//   the variable is true Chemora will assume all modules reference
//   dt, which will force the generation of a module for each value of
//   dt encountered, which can slow down run time.  This hack will
//   be replaced when code using dt is encountered.
const bool module_key_includes_delta_time = false;

string
Chemora_CG_Calc::module_key_make(CCTK_ARGUMENTS, CaKernel_Kernel_Config *kc)
{
  DECLARE_CCTK_ARGUMENTS;
  pStringF mod_key
    ("%s-%x,%x,%x-%a,%a,%a-%a,%a,%a",
     kc->kernel_name,
     cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
     double(float(CCTK_DELTA_SPACE(0))),
     double(float(CCTK_DELTA_SPACE(1))),
     double(float(CCTK_DELTA_SPACE(2))),
     double(float(CCTK_ORIGIN_SPACE(0))),
     double(float(CCTK_ORIGIN_SPACE(1))),
     double(float(CCTK_ORIGIN_SPACE(2))));
  if ( module_key_includes_delta_time )
    mod_key.sprintf("-%a",CCTK_DELTA_TIME);
  return mod_key.ss();
}

void
Chemora_CG_Calc::module_info_set(Module_Info *mip)
{
  mi = mip;
  if ( !ct->mi ) ct->mi = mi;
  modd = module_data_get(mi);
  if ( !modd->mi )
    {
      modd_vector += modd;
      modd->mi = mi;
    }
  modd->kernel_arg_arrays = kernel_arg_arrays;
}

Chemora_CG_Module_Data*
Chemora_CG_Calc::module_data_get(int idx)
{
  return idx >= modd_vector.size() ? NULL : modd_vector[idx];
}

Chemora_CG_Module_Data*
Chemora_CG_Calc::module_data_get(Module_Info *mip)
{
  return &module_data[mip];
}

struct Opt_Info {
  union {
    bool *bptr; char *cptr; void *ptr; int *iptr; map<string,char> *Sptr;
    double *dptr;
  };
  char type;
  string def;
  char hash_val;  // Value to assign for 'S', string hash type.
  const char *valset;  // Or docstring.
};

string
Chemora_CG_Calc::temporary_files_dir_get()
{
  // Construct dir for dynamically generated files.

  char* const userid = getenv("USER");
  string dir = string("/tmp/chemora-") + userid + "-" + ct->name_get();
  int ierr = mkdir(dir.c_str(),0700);
  assert( !ierr || errno == EEXIST );
  return dir;
}
string
Chemora_CG_Calc::temporary_file_path_prefix_get()
{
  // Construct path for dynamically generated files.

  string dir = temporary_files_dir_get();
  pStringF path_base
    ("%s/d%d-%s", dir.c_str(), mi->dm->devidx_get(), calc_name.c_str());
  return path_base.ss();
}

void
Chemora_CG_Calc::init
(Chemora_CG_Thorn *ctp, const char* calc_namep, Chemora_CG_Calc *diff_ops)
{
  DECLARE_CCTK_PARAMETERS;

  ct = ctp;
  chemora = ct->chemora;
  calc_name = calc_namep;
  diff_operations_calc = diff_ops;
  next_marker = marker_never + 1;
  mp_version_next = marker_never + 1;

  pString shared_opt(dynamic_compile_shared_assignment);
  const bool shared_auto = shared_opt == "auto";
  const int shared_var_user_limit = 
    shared_auto ? -1 : atoi(dynamic_compile_shared_assignment);

  if ( !shared_auto && dynamic_compile_experimental_options[0] )
    {
      CCTK_VWarn
        (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "Parameter file error: parameter dynamic_compile_shared_assignment\n"
         "must be set to \"auto\" if parameter dynamic_compile_experimental_options\n"
         "is set. Either set dynamic_compile_shared_assignment to \"auto\"\n"
         "or remove dynamic_compile_experimental_options.");
    }

  opt_force_one_block_per_mp = false;
  opt_reduce_emit_dataflow = true;
  opt_cse = true;
  opt_sched_latency = true;
  opt_move_constraint = 'a';  // Automatic.
  const int max_kernels = 8 * sizeof( A_Vector_Type ) - 1;

  if ( shared_var_user_limit == 0 )
    {
      opt_buf_single_use = 'g';
      opt_buf_low_dim = 'g';
      opt_buf_sten_x = 'g';
      opt_buf_sten_y = 'g';
      opt_buf_sten_z = 'g';
      return;
    }

  pString attr_list;
  map<string,Opt_Info> opt_to_info;
  bool err_found = false;

#define DEF_OPT_COMMON(name,t,d,vs)                                           \
  {                                                                           \
    Opt_Info& oi = opt_to_info[string(#name)];                                \
    attr_list.sprintf                                                         \
      ("\n  %s (default value, \"%s\"; valid values, \"%s\")", #name, d, vs); \
    oi.ptr = &opt_##name;                                                     \
    oi.type = t;                                                              \
    oi.def = d;                                                               \
    oi.valset = vs;                                                           \
  }

#define DEF_OPT_COMMONA(name,t,d,vs) \
  { opt_##name = d; DEF_OPT_COMMON(name,t,#d,vs); }

#define DEF_OPT_STR_HASH(var,opt_name,hval,valid_val_msg)                     \
  {                                                                           \
    Opt_Info& oi = opt_to_info[opt_name];                                     \
    attr_list.sprintf                                                         \
      ("\n  %s (valid values, \"%s\")", opt_name, valid_val_msg);             \
    oi.ptr = &var;                                                            \
    oi.type = 'S';                                                            \
    oi.hash_val = hval;                                                       \
  }

#define DEF_OPT_IRREGULAR(opt_name,d,valid_val_msg)                           \
  {                                                                           \
    Opt_Info& oi = opt_to_info[opt_name];                                     \
    attr_list.sprintf                                                         \
      ("\n  %s (valid values, \"%s\")", opt_name, valid_val_msg);             \
    oi.def = d;                                                               \
    oi.ptr = NULL;                                                            \
    oi.type = 'I';                                                            \
  }


#define DEF_OPT_BOOL(name,def) DEF_OPT_COMMON(name,'b',def,valset_bool);
#define DEF_OPT_BOOL_INIT(name,def) DEF_OPT_COMMONA(name,'b',def,valset_bool);
#define DEF_OPT_CHAR(name,def,vs) DEF_OPT_COMMON(name,'c',def,vs);
#define DEF_OPT_STR(name,def,vs) DEF_OPT_COMMON(name,'S',def,vs);
#define DEF_OPT_INT(name,def,vs) DEF_OPT_COMMONA(name,'i',def,vs);
#define DEF_OPT_DOUBLE(name,def,doc) DEF_OPT_COMMONA(name,'d',def,doc);

  const char* const valset_bool = "01";
  const char* const valset_opt_buf_all = "agsr";

  DEF_OPT_BOOL_INIT(array_bases_share,1);
  DEF_OPT_INT
    (addr_regs_max,8,
     "Maximum number of registers used for certain memory addresses.");
  DEF_OPT_BOOL_INIT(4Gx_okay,1);
  DEF_OPT_BOOL_INIT(addr_regs_opt,1); // Use optimal (Belady) replacement.
  DEF_OPT_BOOL_INIT(addr_literals,0);

  DEF_OPT_BOOL_INIT(code_target_acc,0);
  opt_code_target_set = true;

  opt_metrics_extra = false;
  DEF_OPT_BOOL(metrics_extra,"0");

  DEF_OPT_BOOL_INIT(verify_buffering,0);

  DEF_OPT_INT(halo2,1,"Shared memory code variations:\n"
              " 0: Older method: Each thread handles all buffering.\n"
              " >0: Halo 2: Buffering assigned to different warps.\n"
              " 1: Protrusion flattening (should be best).\n"
              " 2: Max flattening.\n"
              " 3: No flattening.");

  DEF_OPT_IRREGULAR
  ("force_by_pattern", "0 to g",
    "\"PATTERN to STYPE\", PATTERN is a hex value, STYPE is r or g.");
  opt_force_pattern = 0;
  opt_force_pattern_to = 0;

  DEF_OPT_INT(rand_seed,0,"0, set using time; N, use N as seed.");

  DEF_OPT_DOUBLE
    (issue_per_exec,2,
     "Performance model parameter: ratio of issued to executed instructions.");

  pStringF max_k_msg
    ("Range -%d to +%d.  Positive, max kernels optimized; "
     "negative, distribute stores and don't optimize.",
     max_kernels, max_kernels);

  DEF_OPT_CHAR(move_constraint,"0","0asn");
  DEF_OPT_INT(num_kernels,30,max_k_msg.s);
  DEF_OPT_INT(small_block,10,"0, maximize block size; 10 use small blocks.");

  DEF_OPT_INT
    (opt_nodes_max,500,
     "Kernel mapping stochastic optimization parameter to specify the "
     "maximum number of nodes to assume when computing the number of move "
     "proposals to perform. Does not affect which nodes can be chosen.");
  DEF_OPT_DOUBLE
    (opt_props_per_node_per_cycle,1.0,
     "Kernel mapping stochastic optimization parameter to specify the "
     "value of f in the formula for the number of move proposals "
     "per cycle: P = N * f, where N is the number of nodes.  Value"
     "can be floating point.");
  DEF_OPT_INT
    (opt_num_cycles,2,
     "Kernel mapping stochastic optimization parameter to specify the "
     "number of annealing cycles.");

  DEF_OPT_BOOL(force_one_block_per_mp,"0");
  DEF_OPT_BOOL(reduce_emit_dataflow,"1");
  DEF_OPT_BOOL(cse,"1");
  DEF_OPT_BOOL(sched_latency,"1");
  DEF_OPT_CHAR(buf_single_use,"g","agr");
  opt_buf_single_use = 'g';
  DEF_OPT_CHAR(buf_low_dim,"g","agr0");
  opt_buf_low_dim = 'r';
  DEF_OPT_CHAR(buf_sten_x,"r",valset_opt_buf_all);
  opt_buf_sten_x = 'r';
  DEF_OPT_CHAR(buf_sten_y,"s",valset_opt_buf_all);
  opt_buf_sten_y = 'a';
  DEF_OPT_CHAR(buf_sten_z,"s",valset_opt_buf_all);
  opt_buf_sten_z = 'a';
  DEF_OPT_STR_HASH(opt_assign_force_map,"assign_force_g",'g',"Grid func name");
  DEF_OPT_STR_HASH(opt_assign_force_map,"assign_force_r",'r',"Grid func name");

  DEF_OPT_INT
    (block_occ_no_hint_threshold, 1,
     "Provide the minimum block-per-MP hint (minnctapersm) to the PTX compiler "
     "if the estimated number of blocks per MP is <= to this parameter and "
     "if the estimated number of blocks per MP is the same as the number "
     "of blocks per MP determined only by shared memory use.");

  DEF_OPT_INT
    (icache_size, 1,
     "If 0, ignore instruction cache when choosing unroll degree;\n"
     "if 1, use default icache size;\n"
     "otherwise, use parameter value as icache size.");

# undef DEF_OPT_BOOL
# undef DEF_OPT_CHAR
# undef DEF_OPT_COMMON

  pSplit calcs(dynamic_compile_experimental_options,';');
  pString err_msg("Parameter dynamic_compile_experimental_options error: ");

  while ( const char* const calc_raw = calcs )
    {
      pStringTrim calc(calc_raw);
      if ( !calc.len() ) continue;
      pSplit cpattern_opts(calc,':');
      const int num = cpattern_opts.size();
      if ( num > 2 )
        CCTK_VWarn
          (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
           "Parameter dynamic_compile_experimental_options error: "
           "The following component should have zero or one colons: \"%s\"",
           calc.s);
      const char* const cpattern = num == 2 ? pstringtrim(cpattern_opts) : ".";
      if ( strchr(cpattern,',') )
        CCTK_VWarn
          (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
           "Parameter dynamic_compile_experimental_options error: "
           "Commas found in pattern.  Perhaps the last comma should be a "
           "semicolon. Pattern is:\n \"%s\"",
           cpattern);

      const bool match =
        CaCUDALib_CUDA_Driver_Manager::kernel_user_setting_match
        (cpattern,calc_name);
      const char* const opts = cpattern_opts;
      pSplit opts_split(opts,',');
      while ( const char* const opt = opts_split )
        {
          pSplit attr_val(opt,'=',"=");
          pStringTrim attr(attr_val);
          string attrs = attr.ss();
          const bool val_present = attr_val.size() == 2;
          Opt_Info* const oi = &opt_to_info[attrs];

          if ( !oi->ptr )
            {
              err_msg.sprintf("Unrecognized attribute \"%s\".\n", attr.s);
              err_found = true;
              continue;
            }

          string val = val_present ? string(pstringtrim(attr_val)) : oi->def;

          if ( attrs == "force_by_pattern" )
            {
              const int tend = val.find("to");
              int perr = 0;
              if ( tend == string::npos )
                CCTK_VWarn
                  (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
                   "Parameter dynamic_compile_experimental_options error: "
                   "attribute force_by_pattern must contain \"to\", for example\n,"
                   "\"0xe3 to g\"");
              string pattern_str = val.substr(0,tend);
              const int force_pattern = strtol( val.c_str(), NULL, 16 );
              if ( force_pattern < 0 || force_pattern > 255 ) perr++;
              pStringTrim to( val.substr(tend+2).c_str() );
              if ( to != "g" && to != "r" ) perr++;

              if ( perr )
                {
                  CCTK_VWarn
                    (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
                     "Parameter dynamic_compile_experimental_options error: "
                     "attribute force_by_pattern value invalid, example value\n,"
                     "\"0xe3 to g\"");
                  continue;
                }
              opt_force_pattern = force_pattern;
              opt_force_pattern_to = to.s[0];
              continue;
            }

          const int inlen =
            oi->ptr && oi->valset ? strspn(val.c_str(),oi->valset) : -1;
          if ( ( oi->type == 'b' || oi->type == 'c' )
               && ( oi->ptr == NULL || inlen != 1 || val.size() != 1 ) )
            {
              err_msg.sprintf("Unrecognized attribute \"%s\".\n", attr.s);
              err_found = true;
            }

          if ( !match || err_found ) continue;
          switch ( oi->type ) {
          case 'b': oi->bptr[0] = val == "1"; break;
          case 'c': oi->cptr[0] = val == "a" ? oi->def[0] : val[0]; break;
          case 'i': oi->iptr[0] = atoi(val.c_str()); break;
          case 'd': oi->dptr[0] = atof(val.c_str()); break;
          case 'S':
            {
              map <string,char>& shash = *oi->Sptr;
              pSplit vpatterns(val.c_str(),'.');
              while ( const char* const pat = vpatterns )
                shash[pat] = oi->hash_val;
            }
            break;
          default: assert(false);
          }
        }
    }

  if ( opt_num_kernels <= 0 )
    {
      if ( opt_move_constraint != 'a' )
        CCTK_VWarn
          (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,
           "Experimental option opt_num_kernels must be positive if "
           "experimental option opt_move_constraint is set. Change "
           "the value of opt_num_kernels to a positive number and set "
           "opt_move_constraint to 's' for fixed and deterministic store "
           "placement or to 'n' for a fixed number of kernels.\n"
           "Thank you, your patience is appreciated.\n");
      opt_move_constraint = 's';
      opt_num_kernels = -opt_num_kernels;
    }
  if ( opt_move_constraint == 'a' ) opt_move_constraint = '0';

  if ( opt_num_kernels < 1 || opt_num_kernels > max_kernels )
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Experimental option max_kernels=%d is out of range.\n%s",
       opt_num_kernels, max_k_msg.s);

  if ( chemora->rand_seed_set )
    {
      rand_seed = chemora->rand_seed;
    }
  else
    {
      rand_seed = opt_rand_seed ?: time(NULL);
      chemora->rand_seed = rand_seed;
      chemora->rand_seed_set = true;
    }

  icache_bytes = -1;

  if ( err_found )
    {
      err_msg.sprintf("  Valid attributes are %s.",attr_list.s);
      CCTK_VWarn
        (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "%s", err_msg.s);
    }

#ifndef HAVE_OPENACC
  if ( opt_code_target_acc )
      CCTK_VWarn
        (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "Code target set to OpenACC, but OpenACC compiler not found.\n"
         "(In parameter file set code_target_acc=0 within dynamic_compile_experimental_options.)");
#endif
}

bool
Chemora_CG_Calc::code_target_acc_get()
{
  assert( opt_code_target_set );
  return opt_code_target_acc;
}

void
Chemora_CG_Calc::printf(const char *fmt,...)
{
  DECLARE_CCTK_PARAMETERS;
  if ( mi->dm->myproc_get() != 0
       || !dynamic_compile_show_tuning_decisions ) return;

  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt,ap);
  va_end(ap);
}

void
Chemora_CG_Calc_Mapping::printf(const char *fmt,...)
{
  DECLARE_CCTK_PARAMETERS;
  if ( c->mi->dm->myproc_get() != 0
       || !dynamic_compile_show_tuning_decisions ) return;

  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt,ap);
  va_end(ap);
}

void
Chemora_CG_Calc::at_launch_prepare(const char* name)
{
  kernel_function_name = name;
  kernel_arg_arrays.clear();
}

Node_Backup::Node_Backup(Chemora_CG_Calc_Mapping *mf):m(mf),c(mf->c)
{
  for ( NIter nd(c->nodes); nd; ) nodes += new Node(*nd);
  name_to_node = c->name_to_node;
}

Node_Backup::~Node_Backup()
{
  const int size = nodes.size();
  for ( int i=0; i<size; i++ ) *c->nodes[i] = *nodes[i];
  while ( c->nodes.size() > size ) delete c->nodes.pop();
  c->name_to_node = name_to_node;
  m->mapping_node.resize(nodes.size());
  m->assignment_vector.resize(nodes.size());
}

Node*
Chemora_CG_Calc::node_new(NType ntype, const char *name_fmt, ...)
{
  va_list ap;
  pString name;
  va_start(ap,name_fmt);
  name.vsprintf(name_fmt,ap);
  va_end(ap);
  return node_new(name.s,ntype);
}

Node*
Chemora_CG_Calc::node_new(const char* name, NType ntype, Node_New_Action nna)
{
  Node* const incumbent = name_to_node.lookup(name);

  if ( incumbent && nna == NN_New )
    {
      if ( ntype != NT_Unknown )
        {
          assert( incumbent->ntype == NT_Unknown );
          incumbent->ntype = ntype;
        }
      return incumbent;
    }

  Node* const eq = new Node;

  if ( incumbent )
    switch ( nna ) {
    case NN_Redefine: incumbent->successor = eq; break;
    default: assert( false );
    }

  eq->marker = marker_init_val;
  eq->marker_mp = marker_init_val;
  eq->red_fp_update_marker_mp = marker_init_val;
  eq->predecessor = nna == NN_Redefine ? incumbent : NULL;
  eq->successor = NULL;
  eq->name_disambig = incumbent ? incumbent->name_disambig + 1 : 0;
  eq->lhs_name_dis = pstringf("%s__%d",name,eq->name_disambig);
  eq->lhs_name = name;
  eq->ntype = ntype;
  eq->no = NO_Unset;
  eq->visited = true;
  eq->loop_body = false;
  eq->compile_time_constant = false;
  eq->ctc_val_set = false;
  eq->identity = false;
  eq->cse_remapped = false;
  eq->opt_assoc_combined = false;
  eq->opt_replaced = false;
  eq->issue_usage = -1;
  eq->ops = 0;
  eq->mem_accesses = 0;
  eq->oidx = eq->idx = nodes.size();
  eq->gf = NULL;
  eq->ik_mp_cv_version = marker_init_val;
  eq->ik_cv_version = -2;
  eq->occ = 0;
  nodes += eq;
  name_to_node[name] = eq;
  return eq;
}

void
Chemora_CG_Calc::nodes_at_generic_processing_done()
{
  for ( NIter nd(nodes); nd; ) nodes_backup += new Node(*nd);
  name_to_node_backup = name_to_node;
}

void
Chemora_CG_Calc::nodes_at_specialization_start()
{
  const int size = nodes_backup.size();
  for ( int i=0; i<size; i++ ) *nodes[i] = *nodes_backup[i];
  while ( nodes.size() > size ) delete nodes.pop();
  name_to_node = name_to_node_backup;
}

Node*
Chemora_CG_Calc::node_literal_lookup(double val)
{
  pStringF key1("=%g",val);
  Node* const incumbent1 = name_to_node.lookup(key1.s);
  if ( incumbent1 ) return incumbent1;
  pStringF key2("=%.20g",val);
  Node* const nd = node_new(key2.s,NT_Unknown);
  if ( nd->ntype != NT_Unknown ) return nd;
  return node_literal_init(nd,val);
}

Node*
Chemora_CG_Calc::node_literal_init(Node *nd, double val)
{
  if ( nd->ntype != NT_Unknown ) return nd;
  nd->ntype = NT_Literal;
  nd->no = NO_Literal;
  nd->compile_time_constant = true;
  nd->ctc_val = val;
  nd->ctc_val_set = true;
  return nd;
}

Node*
Chemora_CG_Calc::node_lookup(const char* name)
{
  Node* const nd = node_new(name,NT_Unknown);
  if ( nd->ntype != NT_Unknown || name[0] != '=' ) return nd;
  return node_literal_init(nd,atof(&name[1]));
}

void
Chemora_CG_Calc::node_remap_to(Node *nd_from, Node *nd_to)
{
  Node* const incumbent = name_to_node.lookup(nd_to->lhs_name);
  assert( incumbent == nd_to );
  nd_from->cse_remapped = true;
  name_to_node[nd_from->lhs_name] = nd_to;
}

void
Chemora_CG_Calc::node_src_add(Node *nd, Node *src)
{
  nd->sources += src;
  src->dests += nd;
}

void
Chemora_CG_Calc::node_srcs_add(Node *nd, Nodes& srcs)
{
  for ( NIter src(srcs); src; ) node_src_add(nd,src);
}

void
Chemora_CG_Calc::node_srcs_clear(Node *nd)
{
  for ( NIter src(nd->sources); src; )
    {
      Nodes new_dsts;
      int found = 0;
      for ( NIter dst(src->dests); dst; )
        if ( dst != nd || found++ ) new_dsts += dst;
      assert( found >= 1 );
      src->dests = new_dsts;
    }
  nd->sources.clear();
}

void
Chemora_CG_Calc::node_srcs_replace(Node *nd, Nodes replacement)
{
  node_srcs_clear(nd);
  node_srcs_add(nd,replacement);
}

void
Chemora_CG_Calc::node_srcs_of_dests_replace(Node *nd_old, Node *nd_new)
{
  map <int,int> seen;
  for ( NIter dst(nd_old->dests); dst; )
    {
      if ( seen[dst->idx]++ ) continue;
      Nodes new_srcs;
      int found = 0;
      for ( NIter src(dst->sources); src; )
        if ( src == nd_old ){ found++;  if ( nd_new ) new_srcs += nd_new; }
        else                { new_srcs += src; }
      assert( found >= 1 );
      dst->sources = new_srcs;
    }

  if ( !nd_new ) return;
  nd_new->dests += nd_old->dests;
}

void
Chemora_CG_Calc::node_to_identity_and_remove(Node *nd, Node *id_src)
{
  nd->no = NO_Identity;
  nd->identity = true;
  nd->identity_src = -1;
  nd->opt_replaced = true;
  nd->visited = false;
  node_srcs_of_dests_replace(nd,id_src);
  node_srcs_clear(nd);
}

void
Chemora_CG_Calc::node_to_identity_and_remove(Node *nd, int var_src)
{ node_to_identity_and_remove(nd,nd->sources[var_src]); }

void
Chemora_CG_Calc::node_remove(Node *nd)
{
  nd->opt_replaced = true;
  nd->visited = false;
  node_srcs_clear(nd);
}

void
Chemora_CG_Calc::nodes_propagate_ctcs(Nodes& ctc_initial)
{
  const int ctc_marker = next_marker++;
  Nodes ctc = ctc_initial;
  int new_ctcs = 0;
  for ( NIter nd(ctc); nd; ) nd->dests_here = new_ctcs;

  while ( Node* const nd = ctc.pop() )
    {
      assert( nd->ctc_val_set );
      // Grrr.
      nd->compile_time_constant = true;
      Nodes dests = nd->dests;  // Make a copy because nd->dests will change.
      for ( NIter dst(dests); dst; )
        {
          if ( dst->ctc_val_set ) continue;
          if ( !dst->marker_set(ctc_marker) )
            {
              dst->sources_unscheduled = 0;
              for ( NIter src(dst->sources); src; )
                if ( !src->ctc_val_set ) dst->sources_unscheduled++;
              dst->dests_here = new_ctcs;
            }
          else if ( nd->dests_here > dst->dests_here )
            dst->sources_unscheduled--;

          assert( dst->sources_unscheduled >= 0 );
          auto sval = [&](int idx) { return dst->sources[idx]->ctc_val; };
          auto sctc = [&](int idx) { return dst->sources[idx]->ctc_val_set; };

          Nodes live_srcs, ctc_srcs;
          for ( auto src: dst->sources )
            if ( src->ctc_val_set ) ctc_srcs += src; else live_srcs += src;

          //
          // Check for cases such as (1?x:y), 1 && x,  x*0, x*-1, etc.
          //

          // Number of non-constant operands can be >= 1.
          //
          switch ( dst->no ) {
          case NO_If:
            if ( sctc(0) )
              {
                const bool val = sval(0);
                node_to_identity_and_remove(dst,val?1:2);
                nds_ident_from_op++;
              }
            continue;
          case NO_And:
            {
              bool val = true;
              assert( dst->sources == 2 ); // Remove when > 2-input ANDs avail.
              for ( auto src: ctc_srcs ) val &= bool(src->ctc_val);
              if ( val )
                {
                  if ( dst->sources_unscheduled > 1 ) continue;
                  assert( live_srcs == 1 );
                  node_to_identity_and_remove(dst,live_srcs[0]);
                  nds_ident_from_op++;
                }
              else
                {
                  dst->ntype = NT_Constant_Expr;
                  node_srcs_clear(dst);
                  dst->ctc_val = 0;
                  dst->ctc_val_set = true;
                  dst->dests_here = ++new_ctcs;
                  ctc += dst;
                }
            }
            continue;

          case NO_Or:
            {
              bool val = false;
              for ( auto src: ctc_srcs ) val |= bool(src->ctc_val);
              if ( val )
                {
                  dst->ntype = NT_Constant_Expr;
                  node_srcs_clear(dst);
                  dst->ctc_val = 1;
                  dst->ctc_val_set = true;
                  dst->dests_here = ++new_ctcs;
                  ctc += dst;
                }
              else
                {
                  assert( live_srcs == 1 ); // Thanks for the testcase.
                  node_to_identity_and_remove(dst,live_srcs[0]);
                  nds_ident_from_op++;
                }
            }
            continue;

          default: break;
          }

          if ( dst->sources_unscheduled == 1 )
            {
              int var_src = -1;
              for ( NIter src(dst->sources); src; )
                if ( !src->ctc_val_set )
                  { assert( var_src == -1 );  var_src = src; }
              if ( var_src == -1 ) continue;
              Node* const live_src = dst->sources[var_src];

              switch ( dst->no ) {
              case NO_Sum:
                {
                  double val = 0;
                  for ( NIter src(dst->sources); src; )
                    if ( src->ctc_val_set ) val += src->ctc_val;
                  if ( val != 0 ) continue;
                  node_to_identity_and_remove(dst,var_src);
                  nds_ident_from_op++;
                }
                continue;
              case NO_Product:
                {
                  double val = 1;
                  for ( NIter src(dst->sources); src; )
                    if ( src->ctc_val_set ) val *= src->ctc_val;
                  if ( val == 1 )
                    {
                      node_to_identity_and_remove(dst,var_src);
                      nds_ident_from_op++;
                    }
                  else if ( val == 0 )
                    {
                      dst->ntype = NT_Constant_Expr;
                      node_srcs_clear(dst);
                      dst->ctc_val = 0;
                      dst->ctc_val_set = true;
                      dst->dests_here = ++new_ctcs;
                      ctc += dst;
                      nds_zero_from_mult++;
                    }
                  else if ( val == -1 )
                    {
                      dst->no = NO_Negate;
                      node_srcs_clear(dst);
                      node_src_add(dst,live_src);
                      nds_neg_from_mult++;
                    }
                }
                continue;
              case NO_Power:
                if ( var_src != 0 ) continue;
                {
                  const double e = dst->sources[1]->ctc_val;
                  if ( e == 1 ) {
                    node_to_identity_and_remove(dst,var_src);
                    nds_ident_from_op++;
                    continue;
                  }
                  if ( e == -1 ) {
                    dst->no = NO_Reciprocal;
                    node_srcs_clear(dst);
                    node_src_add(dst,live_src);
                    continue;
                  }
                  if ( e >= 2 && e <= 3 ) {
                    dst->no = NO_Product;
                    node_srcs_clear(dst);
                    for ( int i=0; i<e; i++ ) node_src_add(dst,live_src);
                    continue;
                  }

                  if ( e == 4 ) {
                    pStringF r_name("chemora__pow_%d",var_interm_next++);
                    Node* const prod = node_new(r_name.s,NT_Expression);
                    prod->no = NO_Product;
                    node_srcs_clear(dst);
                    node_src_add(prod,live_src);
                    node_src_add(prod,live_src);
                    node_c_code_make(prod,"Power 4");
                    dst->no = NO_Product;
                    node_src_add(dst,prod);
                    node_src_add(dst,prod);
                    continue;
                  }

                  if ( e == -2 ) {
                    pStringF r_name("chemora__pow_%d",var_interm_next++);
                    Node* const prod = node_new(r_name.s,NT_Expression);
                    prod->no = NO_Product;
                    dst->no = NO_Reciprocal;
                    node_srcs_clear(dst);
                    node_src_add(prod,live_src);
                    node_src_add(prod,live_src);
                    node_src_add(dst,prod);
                    node_c_code_make(prod,"Power -2");
                    continue;
                  }
                  if ( e == 0.5 ) {
                    dst->no = NO_SquareRoot;
                    node_srcs_clear(dst);
                    node_src_add(dst,live_src);
                    continue;
                  }
                }
                continue;
              case NO_Divide:
                assert( false ); // Write me!
                continue;

              default: continue;
              }
            }
          if ( dst->sources_unscheduled ) continue;
          for ( NIter src(dst->sources); src; ) assert( src->ctc_val_set );

          CCTK_REAL val = 0;
          switch ( dst->no ) {
          case NO_Not:
            val = !bool( sval(0) );
            break;
          case NO_Sum:
            val = 0;
            for ( NIter src(dst->sources); src; ) val += src->ctc_val;
            break;
          case NO_Abs:
          case NO_Negate:
          case NO_Min:
            assert( false ); // Write us.
            break;
          case NO_Max:
            val = dst->sources[0]->ctc_val;
            for ( NIter src(dst->sources); src; )
              if ( src->ctc_val > val ) val = src->ctc_val;
            assert( false ); // Need to verify.
            break;
          case NO_LessEqual: val = sval(0) <= sval(1); break;
          case NO_Less: val = sval(0) < sval(1); break;
          case NO_Greater: val = sval(0) > sval(1); break;
          case NO_GreaterEqual: val = sval(0) >= sval(1); break;
          case NO_Product:
            val = 1;
            for ( NIter src(dst->sources); src; ) val *= src->ctc_val;
            break;
          case NO_Divide:
            assert( dst->sources == 2 );
            val = dst->sources[0]->ctc_val / dst->sources[1]->ctc_val;
            break;
          case NO_Power:
            assert( dst->sources.size() == 2 );
            val = pow( dst->sources[0]->ctc_val, dst->sources[1]->ctc_val );
            break;
          case NO_Identity:
            assert( false );
            break;
          default:
            continue;
          }
          assert( !dst->ctc_val_set );
          assert( dst->ntype == NT_Expression
                  || dst->ntype == NT_Constant_Expr );
          dst->ntype = NT_Constant_Expr;
          dst->ctc_val = val;
          dst->ctc_val_set = true;
          dst->dests_here = ++new_ctcs;
          ctc += dst;
        }
    }
}

void
Chemora_CG_Calc::optimize_factor_ac_plus_ax()
{
  // Factor (ac+ax) -> a(c+x) where c is a constant.
  // (Mathematica likes (ac+ax) even when c = 1.)
  //
  for ( NIter nd(nodes); nd; )
    {
      if ( !nd->visited ) continue;
      if ( nd->no != NO_Sum ) continue;
      if ( nd->sources != 2 ) continue;
      Nodes nds_trash(nd);
      Nodes a_cand;
      bool is_neg[2] = {false,false};
      Nodes non_ctcs[2];
      int ctcs[2] = {0,0};
      double c_cand[2] = {0,0};
      const int marker = next_marker++;
      for ( NIter src(nd->sources); src; )
        {
          const int iter = src;
          const bool neg = src->no == NO_Negate;
          is_neg[iter] = neg;
          if ( neg ) nds_trash += src;
          Node* const s2 = src->no == NO_Negate ? src->sources[0] : src;
          Nodes nds = s2->no == NO_Product ? s2->sources : Nodes(s2);
          if ( s2->no == NO_Product ) nds_trash += s2;
          for ( NIter ssrci(nds); ssrci; )
            {
              if ( ssrci->no == NO_Negate ) is_neg[iter] = !is_neg[iter];
              Node* const ssrc =
                ssrci->no == NO_Negate ? ssrci->sources[0] : ssrci;
              if ( ssrc->ctc_val_set )
                { ctcs[iter]++;  c_cand[iter] = ssrc->ctc_val; }
              else
                { non_ctcs[iter] += ssrc; }
              if ( iter == 0 )
                { if ( !ssrc->ctc_val_set ) ssrc->marker_set(marker); }
              else
                { if ( ssrc->marker_check(marker) ) a_cand += ssrc; }
            }
        }
      if ( a_cand != 1 ) continue;
      if ( non_ctcs[0] == 2 && non_ctcs[1] == 2 ) continue;
      if ( non_ctcs[0] < 2 && non_ctcs[1] < 2 ) continue;
      Node* const nd_a = a_cand[0];
      const int x_side = non_ctcs[0] == 1 ? 1 : 0;
      const int c_side = 1 - x_side;
      Node* const nd_x =
        non_ctcs[x_side][0] == nd_a ? non_ctcs[x_side][1] : non_ctcs[x_side][0];
      assert( ctcs[x_side] == 0 );
      const bool val_c_neg = is_neg[c_side] ^ is_neg[x_side];
      const double val_c_1 = ctcs[c_side] ? c_cand[c_side] : 1;
      const double val_c = val_c_neg ? -val_c_1 : val_c_1;
      pStringF mul_name("chemora__factor_%d",var_interm_next++);
      Node* const prod = node_new(mul_name.s,NT_Expression);
      prod->no = NO_Product;
      node_srcs_of_dests_replace(nd,prod);
      pStringF sum_name("chemora__factor_%d",var_interm_next++);
      Node* const sum = node_new(sum_name.s,NT_Expression);
      sum->no = NO_Sum;
      Node* const nd_c = node_literal_lookup(val_c);
      node_src_add(sum,nd_c);
      node_src_add(sum,nd_x);
      node_src_add(prod,nd_a);
      node_src_add(prod,sum);
      Node* const nd_m1 = node_literal_lookup(-1);
      if ( is_neg[x_side] ) node_src_add(prod,nd_m1);
      node_c_code_make(prod,"Factor (ac+ax)");
      node_c_code_make(sum,"Factor (ac+ax)");
      for ( NIter nt(nds_trash); nt; ) node_remove(nt);
    }
}

void
Chemora_CG_Calc::optimize_fp_selects()
{
  // Replace subtree rooted at copysign with a set predicate and select.
  //
#if 0
  // For example, effectively convert:
  double s = 0.5 + copysign( 0.5, val );
  double y = factor1 * s + factor2 * ( 1 - s );
  // into
  double y = val >= 0 ? factor1 : factor2;
#endif

  Nodes ctc_fps;
  for ( NIter nd(nodes); nd; )
    {
      if ( !nd->visited || nd->no != NO_CopySign ) continue;
      Node* const targ = nd->sources[0];
      if ( !targ->ctc_val_set ) continue;
      assert( !nd->ctc_val_set );
      const int marker = next_marker++;
      const int marker_mul = next_marker++;
      nd->marker_set(marker);
      nd->ctc_val = fabs(targ->ctc_val);
      nd->ctc_val2 = -nd->ctc_val;
      Nodes stack = nd->dests;
      Nodes mul_nodes, interior;
      bool wont_work = false;
      while ( Node* const cur = stack.pop() )
        {
          // Assume no reconvergence before select.
          if ( ! cur->visited )
            printf("Problem with %d no %d (nd %d)\n",cur->idx,cur->no,nd->idx);
          assert( cur->visited );
          Nodes found;
          int outside_nodes = 0;
          double val = 0;
          bool sel_found = false;
          switch ( cur->no ) {
          case NO_Sum:
            val = 0;
            for ( NIter src(cur->sources); src; )
              if ( src->marker_check(marker) ) found += src;
              else if ( src->ctc_val_set ) val += src->ctc_val;
              else outside_nodes++;
            cur->ctc_val = cur->ctc_val2 = val;
            for ( NIter fnd(found); fnd; )
              {
                cur->ctc_val += fnd->ctc_val;
                cur->ctc_val2 += fnd->ctc_val2;
              }
            if ( outside_nodes ) wont_work = true;
            break;
          case NO_Product:
            val = 1;
            for ( NIter src(cur->sources); src; )
              if ( src->marker_check(marker) ) found += src;
              else if ( src->ctc_val_set ) val *= src->ctc_val;
              else outside_nodes++;
            cur->ctc_val = cur->ctc_val2 = val;
            for ( NIter fnd(found); fnd; )
              {
                cur->ctc_val *= fnd->ctc_val;
                cur->ctc_val2 *= fnd->ctc_val2;
              }
            if ( found == 1 )
              {
                Node* const fnd = found[0];
                if ( fnd->ctc_val == 0 || fnd->ctc_val2 == 0 )
                  {
                    assert( fnd->ctc_val != 0 || fnd->ctc_val2 != 0 );
                    assert( cur->ctc_val == 0 || cur->ctc_val2 == 0 );
                    assert( !cur->marker_check(marker) );
                    assert( !cur->marker_check(marker_mul) );
                    cur->marker_set(marker_mul);
                    sel_found = true;
                    mul_nodes += cur;
                  }
              }
            if ( outside_nodes && !sel_found || found > 1 ) wont_work = true;
            break;
          default:
            wont_work = true;
            break;
          }
          if ( sel_found ) continue;
          if ( wont_work ) break;
          interior += cur;
          assert( found );
          cur->marker_set(marker);
          stack += cur->dests;
        }
      if ( wont_work ) continue;
      if ( !mul_nodes ) continue;

      pStringF setp_name("chemora__copysign_repl_%d",var_interm_next++);
      Node* const setp = node_new(setp_name,NT_Expression);
      Node* const nzero = node_literal_lookup(0);
      setp->no = NO_GreaterEqual;
      node_src_add(setp,nd->sources[1]);
      node_src_add(setp,nzero);
      node_c_code_make(setp, "Copysign optimization.");
      node_remove(nd);

      const int marker_add = next_marker++;

      int add_cnt = 0;
      const A_Vector_Type vec_not_combinable = 0;
      map<A_Vector_Type,Nodes> tvec_to_node;
      for ( NIter mul(mul_nodes); mul; )
        {
          mul->ik_mp_consumers_vector = 0;
          mul->dests_here = -1; // >0 idx of other multiply.
          mul->occ = 0; // 1 created If node.
          for ( NIter dst(mul->dests); dst; )
            {
              assert( !dst->marker_check(marker) );
              assert( !dst->marker_check(marker_mul) );
              if ( dst->no != NO_Sum )
                { mul->ik_mp_consumers_vector = vec_not_combinable;  break; }
              if ( !dst->marker_set(marker_add) )
                dst->ik_consumers_vector = A_Vector_Type(1) << add_cnt++;
              mul->ik_mp_consumers_vector |= dst->ik_consumers_vector;
            }
          if ( mul->ik_mp_consumers_vector != vec_not_combinable )
            tvec_to_node[mul->ik_mp_consumers_vector] += mul;
        }

      for ( map<A_Vector_Type,Nodes>::iterator tv = tvec_to_node.begin();
            tv != tvec_to_node.end(); ++tv )
        {
          Nodes& nds = tv->second;
          Nodes nd_t, nd_f;
          for ( NIter mul(nds); mul; )
            if ( mul->ctc_val2 == 0 ) nd_t += mul; else nd_f += mul;

          if ( nd_t != 1 || nd_f != 1 )
            {
              // Yes, we went to all that trouble sorting the multiplies
              // into true and false bins when in fact the only case
              // we handle is exactly one of each. A future optimization.
              continue;
            }

          nd_t[0]->dests_here = nd_f[0]->idx;
          nd_f[0]->dests_here = nd_t[0]->idx;
        }

      for ( NIter mul(mul_nodes); mul; )
        {
          Nodes srcs_keep;
          for ( NIter src(mul->sources); src; )
            if ( !src->marker_check(marker) && !src->ctc_val_set )
              srcs_keep += src;
          assert ( srcs_keep > 0 && srcs_keep.size() < mul->sources );
          const double scale = mul->ctc_val + mul->ctc_val2;
          node_srcs_replace(mul,srcs_keep);
          mul->occ = 1;
          assert( scale != 0 );
          if ( scale != 1 )
            {
              Node* const sc_nd = node_literal_lookup(scale);
              ctc_fps += sc_nd;
              node_src_add(mul,sc_nd);
            }
          Node* const other_mul =
            mul->dests_here > 0 ? nodes[mul->dests_here] : NULL;
          assert( !other_mul ||
                  other_mul->ik_mp_consumers_vector
                  && other_mul->ik_mp_consumers_vector
                  == mul->ik_mp_consumers_vector );
          assert( !other_mul ||
                  bool(mul->ctc_val) == bool(other_mul->ctc_val2) );

          if ( other_mul && !other_mul->occ )
            {
              node_srcs_of_dests_replace(mul,NULL);
              mul->dests.clear();
              continue;
            }

          pStringF mul_name("%s.%d",setp_name.s,int(mul));
          Node* const sel = node_new(mul_name,NT_If);
          sel->no = NO_If;
          Node* const oth = other_mul ? other_mul : nzero;

          node_srcs_of_dests_replace(mul,sel);
          mul->dests.clear();
          node_src_add(sel,setp);
          node_src_add(sel, mul->ctc_val == 0 ? oth : mul );
          node_src_add(sel, mul->ctc_val == 0 ? mul : oth );
          node_c_code_make(sel, "Copysign optimization.");
        }
      for ( NIter inside(interior); inside; ) node_remove(inside);
    }

  nodes_propagate_ctcs(ctc_fps);
}


void
Chemora_CG_Calc::set_parameter(const char* param_name, const char* ptype)
{
  Node* const nd = node_new(param_name,NT_Parameter);
  nd->compile_time_constant = true;
  nd->lhs_type = ptype;
  nd->c_code = pstringf("%s = param.%s;", param_name, param_name);

  int cctk_ptype;
  const void* const val_ptr =
    CCTK_ParameterGet( param_name, ct->thorn_name.c_str(), &cctk_ptype);
  if ( !val_ptr ) return;
  double val;
  switch ( cctk_ptype ) {
  case PARAMETER_INT: val = *(const int*) val_ptr; break;
  case PARAMETER_REAL: val = *(const CCTK_REAL*) val_ptr; break;
  case PARAMETER_BOOLEAN: val = *(const bool*) val_ptr; break;
  default: assert( false );
  }

  nd->ctc_val = val;
  nd->ctc_val_set = true;
}

Node*
Chemora_CG_Calc::assign_from_literal
(const char* lhs_name, double val)
{
  Node* const nd = node_new(lhs_name,NT_Constant_Expr);
  nd->compile_time_constant = true;
  nd->lhs_type = "CCTK_REAL";
  nd->rhs_c_code = pstringf("%.20e",val);
  nd->c_code = pstringf("%s = %s;", lhs_name, nd->rhs_c_code.c_str());
  nd->ctc_val = val;
  nd->ctc_val_set = true;
  return nd;
}

Node*
Chemora_CG_Calc::assign_from_literal
(const char* lhs_name, int val)
{
  Node* const nd = node_new(lhs_name,NT_Constant_Expr);
  nd->compile_time_constant = true;
  nd->lhs_type = "int";
  nd->rhs_c_code = pstringf("%d",val);
  nd->c_code = pstringf("%s = %s;", lhs_name, nd->rhs_c_code.c_str());
  nd->ctc_val = val;
  nd->ctc_val_set = true;
  return nd;
}

void
Chemora_CG_Calc::assign_from_constant_expr
(const char* lhs_name, const char* c_code)
{
  Node* const nd = node_new(lhs_name,NT_Constant_Expr);
  nd->compile_time_constant = true;
  nd->lhs_type = "CCTK_REAL";
  nd->rhs_c_code = c_code;
  nd->c_code = pstringf("%s = %s;", lhs_name, c_code);
}

int
parse_ops(const char* ops_str)
{
  string ops_cpstr = ops_str;
  if ( ops_cpstr == "Literal" ) return 0;
  if ( ops_cpstr == "Identity" ) return 0;
  pSplit pieces(ops_str,';');
  int ops = 0;
  while ( const char* piece = pieces )
    {
      pSplit key_val(piece,':');
      const char* key = key_val;
      string k = key;
      const char* val_str = key_val;
      const int val = atoi(val_str);
      assert( val > 0 );
      if ( k == "Plus" || k == "Power" || k == "Times" )
        ops += val;
      else if ( k == "Trig" )
        ops += val * Op_Cost_Trig;
      else
        {
          assert( false );
        }
    }
  return ops;
}

Node*
Chemora_CG_Calc::assign_from_gf_load
(const char* lhs, const char* gf_name, int time_level)
{
  return assign_from_offset_load(lhs, gf_name, time_level, "0", "0", "0");
}

Node*
Chemora_CG_Calc::assign_from_offset_load
(const char* lhs, const char* gf_name,
 const char* dxs, const char* dys, const char* dzs)
{
  return assign_from_offset_load(lhs, gf_name, 0, dxs, dys, dzs);
}

Node*
Chemora_CG_Calc::assign_from_offset_load
(const char* lhs, const char* gf_name_raw, int time_level,
 const char* dxs, const char* dys, const char* dzs)
{
  const char* const gf_name =
    time_level == 0 ? gf_name_raw :
    time_level == 1 ? pstringf("%sOpast",gf_name_raw) :
    NULL;
  assert( gf_name );

  Node* const nd = node_new(lhs,NT_GFO_Load,NN_Redefine);
  nd->lhs_type = "CCTK_REAL";
  nd->rhs_c_code = pstringf("I3D(%s,%s,%s,%s)", gf_name, dxs, dys, dzs);
  nd->no = NO_GFO_Load;
  nd->c_code = pstringf("%s = %s;", lhs, nd->rhs_c_code.c_str());
  nd->sources += node_loop_start;
# define S(axis,o) \
  const Offset d##axis = atoi(d##axis##s);
  S(x,dx); S(y,dy); S(z,dz);
# undef S
  nd->offset = Offsets(dx, dy, dz);
  nd->offset_axes = ( dx ? AX_x : 0 ) + ( dy ? AX_y : 0 ) + ( dz ? AX_z : 0 );
  nd->pattern = pattern_compute(nd->offset);
  nd->mem_accesses = 1;
  nd->ops = 1;
  gf_use(gf_name,nd);

  pStringF key("%x#%s#%d#%d#%d#", nd->no, gf_name, dx, dy, dz);

  if ( Node* const incumb = gfosig_to_node.lookup(key.s) )
       node_remap_to(nd,incumb);
  else gfosig_to_node[key.s] = nd;

  return nd;
}

Node*
Chemora_CG_Calc::assign_scalar(const char* name)
{
  Node* const nd = node_new(name,NT_GFO_Load,NN_New);
  nd->lhs_type = "CCTK_REAL";
  nd->rhs_c_code = name;
  nd->no = NO_GFO_Load;
  nd->c_code = pstringf("extern %s %s;", nd->lhs_type.c_str(), name);
  nd->sources += node_loop_start; // Not really, but might help with reg pres.
  nd->offset = Offsets(0,0,0);
  nd->offset_axes = 0;
  nd->pattern = pattern_compute(nd->offset);
  nd->mem_accesses = 1;
  nd->ops = 1;
  assign_gf_axes(name,AX_0);
  gf_use(name,nd);

  return nd;
}

void
Chemora_CG_Calc::assign_from_expr
(const char* lhs, const char* ops_str, const char* rhs_c_code, ... )
{
  va_list ap;
  Nodes srcs;

  va_start(ap, rhs_c_code);
  int non_constant_sources = 0;
  while ( const char* const src_name = va_arg(ap,const char*) )
    {
      Node* const src = node_lookup(src_name);
      srcs += src;
      if ( !src->is_strong_compile_time_constant() ) non_constant_sources++;
    }
  va_end(ap);

  Node* const eq = node_new(lhs,NT_Unknown,NN_Redefine);
  assert( eq->ntype == NT_Unknown );

  eq->sources = srcs;

  eq->lhs_type = "CCTK_REAL";
  eq->rhs_c_code = rhs_c_code;
  eq->c_code = pstringf("%s = %s;", lhs, rhs_c_code);

  eq->ops = non_constant_sources ? parse_ops(ops_str) : 0;

  if ( ops_str && ops_str[0] )
    {
      string ops_cstr(ops_str);
      const bool has_minus = ops_cstr.find_first_of('-') != string::npos;
      eq->no =
        ops_cstr == "Literal" ? NO_Literal :
        ops_cstr == "Identity" ? NO_Identity :
        has_minus || srcs.size() != 2 ? NO_Opaque :
        ops_cstr == "Power:1;" ? NO_Power :
        ops_cstr == "Times:1;" ? NO_Product :
        ops_cstr == "Plus:1;" ? NO_Sum : NO_Opaque;
    }
  else
    {
      eq->no = NO_Opaque;
    }

  if ( eq->no == NO_Opaque && eq->sources.size() == 1 )
    {
      eq->no = non_constant_sources ? NO_Identity : NO_Literal;
    }

  assert( eq->no != NO_Opaque );
  assert( eq->no != NO_Identity || eq->sources.size() == 1 );

  if ( eq->no == NO_Identity )
    {
      eq->identity = true;
      eq->identity_src = 0;
    }

  if ( eq->no == NO_Literal )
    {
      eq->ctc_val = eq->sources[0]->ctc_val;
      eq->ctc_val_set = true;
    }

  eq->ntype = non_constant_sources ? NT_Expression : NT_Constant_Expr;
  eq->compile_time_constant = !non_constant_sources;
}

void
Chemora_CG_Calc::assign_from_func
(const char* lhs, const char* func_name, const char* rhs_c_code, ... )
{
  va_list ap;
  Nodes srcs;

  va_start(ap, rhs_c_code);
  int non_constant_sources = 0;
  while ( const char* const src_name = va_arg(ap,const char*) )
    {
      Node* const src = node_lookup(src_name);
      srcs += src;
      if ( !src->is_strong_compile_time_constant() ) non_constant_sources++;
    }
  va_end(ap);

  Node* const eq = node_new(lhs,NT_Unknown,NN_Redefine);
  assert( eq->ntype == NT_Unknown );

  eq->sources = srcs;

  eq->lhs_type = "CCTK_REAL";
  eq->rhs_c_code = rhs_c_code;
  eq->c_code = pstringf("%s = %s;", lhs, rhs_c_code);

  eq->func_name = func_name;
  Static_Op_Info* const soi = chemora->static_op_info_lookup(func_name);
  eq->no = soi ? soi->no : NO_Function;

  eq->ntype = non_constant_sources ? NT_Expression : NT_Constant_Expr;
  eq->compile_time_constant = !non_constant_sources;
}

void
Chemora_CG_Calc::assign_from_gf_load_switch
(const char* lhs, const char* gf_name, const char* switch_var, ...)
{
  va_list ap;
  if ( string(switch_var) == "False" )
    {
      va_start(ap,switch_var);
      const int val = va_arg(ap,int);
      const char* const diff_operator = va_arg(ap,const char*);
      assert( val == 0 );
      va_end(ap);
      string lhs_s = lhs;
      assert( lhs_s == diff_operator );
      return;
    }

  Node* const eq = node_new(lhs,NT_Switch);
  Node* const svar = node_lookup(switch_var);
  eq->sources += svar;
  svar->dests += eq;
  eq->switch_var = svar;
  eq->ops = 2;
  eq->lhs_type = "CCTK_REAL";
  pStringF c_code("%s = ",lhs);
  va_start(ap,switch_var);
  while ( true )
    {
      const int val = va_arg(ap,int);
      const char* const diff_operator = va_arg(ap,const char*);
      if ( !diff_operator ) break;
      assert( val >= 0 );
      Node* const src = node_lookup(diff_operator);
      assert( src->ntype != NT_Unknown );
      eq->switch_src[val] = src;
      eq->sources += src;
      c_code.sprintf(" %s == %d ? %s :", switch_var, val, diff_operator);
    }
  va_end(ap);
  c_code += "0;";
  eq->c_code = c_code;
}

void
Chemora_CG_Calc::assign_from_ifelse
(const char* lhs, const char* cond_name,
 const char* if_name, const char* else_name)
{
  Node* const nd = node_new(lhs,NT_If,NN_Redefine);
  nd->no = NO_If;
  Node* const cond_var = node_lookup(cond_name);
  cond_var->dests += nd;
  nd->switch_var = cond_var;
  Node* const isrc = node_lookup(if_name);
  Node* const esrc = node_lookup(else_name);
  nd->sources += cond_var;
  nd->sources += isrc;
  nd->sources += esrc;
  nd->ops = 2;
  nd->switch_src[0] = esrc;
  nd->switch_src[1] = isrc;
  nd->lhs_type = "CCTK_REAL";
  nd->c_code =
    string(lhs) + " = " + string(cond_name)
    + " ? " + isrc->lhs_name + " : " + esrc->lhs_name + ";";
}


void
Chemora_CG_Calc::gf_store
(const char* gf_name, const char* src_name)
{
  Node* const nd = node_new(gf_name,NT_GF_Store);
  Node* const src = node_lookup(src_name);
  nd->rhs_c_code = src_name;
  nd->no = NO_GF_Store;
  nd->c_code = pstringf("I3D_global(%s,0,0,0) = %s;", gf_name, src_name);
  // Store value must be the first source.
  nd->sources += src;
  nd->sources += node_loop_start;
  nd->offset.packed = 0;
  nd->mem_accesses = 1;
  nd->pattern = 1;
  nd->ops = 1;
  gf_use(gf_name,nd);
}

void
Chemora_CG_Calc::assign_gf_axes(const char* name_raw, int axes)
{
  assert( !gf_axes[name_raw] );
  gf_axes[name_raw] = axes;
}

void
Chemora_CG_Calc::gf_use(const char* name_raw, Node *node)
{
  const bool pattern_forced =
    opt_force_pattern_to && node->pattern & opt_force_pattern;
  const char name_forced = opt_assign_force_map[name_raw];
  string name_s = name_raw;  if ( pattern_forced ) name_s += "!_fg";
  const char* const name = name_s.c_str();
  if ( !grid_functions[name] )
    {
      const int axes =
        opt_buf_low_dim == '0' ? AX_xyz :
        gf_axes.count(name_raw) ? gf_axes[name_raw] : AX_xyz;

      Grid_Function* const gf = new Grid_Function(name_raw);
      grid_functions[name] = gf;
      gf->axes = axes;
      gf->force = name_forced ? name_forced :
        pattern_forced ? opt_force_pattern_to : 0;
    }

  Grid_Function* const gf = grid_functions[name];
  gf->nodes += node;
  node->gf = gf;
}

void
Chemora_CG_Calc::gf_update()
{
  for ( GFIter gf(grid_functions); gf; )
    {
      bool used_in = false;
      bool used_out = false;
      for ( NIter nd(gf->nodes); nd; )
        if ( nd->visited )
          switch ( nd->ntype ) {
          case NT_GFO_Load:
            used_in = true;
            break;
          case NT_GF_Store:
            used_out = true;
            break;
          default:
            assert( false );
          }
      gf->is_shared = false;
      gf->is_in = used_in;
      gf->is_out = used_out;
    }
}

void
Chemora_CG_Calc::launch_gf_arg_set(const char* name, void *device_addr)
{
  kernel_arg_arrays += new Kernel_Arg_Array(name,device_addr);
  Grid_Function* const gf = grid_functions.lookup(name);
  if ( !gf ) return;
  gf->device_addr = device_addr;
  addr_to_gf[size_t(device_addr)] = gf;
}

void
Chemora_CG_Calc::launch_scalar_arg_set(const char* name, void *device_addr)
{
  kernel_arg_arrays += new Kernel_Arg_Array(name,device_addr);
  Grid_Function* const gf = grid_functions.lookup(name);
  if ( !gf ) return;
  assert( gf->axes == AX_0 );
  gf->device_addr = device_addr;
  addr_to_gf[size_t(device_addr)] = gf;
}

void
Chemora_CG_Calc::launch_verify_addr_args(void **args)
{
  void ***argptr = (void***)args;

  modd = module_data_get(mi);
  assert( modd->mi );

  for ( KAAIter ka(modd->kernel_arg_arrays); ka; )
    {
      Grid_Function* const gf = grid_functions.lookup(ka->name.c_str());
      void* const d_addr_arg = **argptr++;
      if ( !gf ) continue;
      assert( d_addr_arg == ka->device_addr );
      assert( d_addr_arg == gf->device_addr );
    }
}


void
Chemora_CG_Calc::value_set(const char* name, int value)
{
  Node* const par = node_lookup(name);
  assert( par->ntype == NT_Parameter );
  int conds = 0;
  par->c_code = pstringf("%s = %d;", par->lhs_name.c_str(), value);
  par->ctc_val = value;
  par->ctc_val_set = true;
  for ( NIter dest(par->dests); dest; )
    if ( dest->switch_var == par )
      {
        dest->sources.erase(dest->sources.begin()+1,dest->sources.end());
        dest->ops = 0;
        NIMap_Iter ssi = dest->switch_src.find(value);
        assert( ssi != dest->switch_src.end() );
        dest->identity = true;
        dest->identity_src = dest->sources.size();
        dest->sources += ssi->second;
        conds++;
        dest->rhs_c_code = ssi->second->lhs_name;
        dest->c_code = pstringf
          ("%s = %s;", dest->lhs_name.c_str(), dest->rhs_c_code.c_str());
        }
}

void
Chemora_CG_Calc::value_set(const char* name, double value)
{
  Node* const par = node_lookup(name);
  assert( par->ntype == NT_Parameter );
  par->c_code = pstringf("%s = %.20g;", par->lhs_name.c_str(), value);
  par->ctc_val = value;
  par->ctc_val_set = true;
}

void
Chemora_CG_Calc::node_c_code_make(Node *nd, const char *comment)
{
  Static_Op_Info* const soi = chemora->static_op_info_get(nd->no);
  string c_code = nd->lhs_name + " = ";
  const char* const op =
    nd->no == NO_Abs ? NULL :
    nd->no == NO_Negate ? NULL :
    nd->no == NO_Sum ? "+" :
    nd->no == NO_Max ? NULL :
    nd->no == NO_Min ? NULL :
    nd->no == NO_Identity ? "" :
    nd->no == NO_Product ? "*" : NULL;
  assert( nd->no != NO_Identity || nd->sources.size() == 1 );
  if ( !op ) c_code += soi->kranc_name + "(";
  string sep = op ? op : ",";
  for ( NIter src(nd->sources); src; )
    {
      if ( src > 0 ) c_code += string(" ") + sep + " ";
      c_code += src->lhs_name;
    }
  if ( !op ) c_code += ")";
  c_code += ";";
  if ( comment ) c_code += string(" // ") + comment;
  nd->c_code = c_code;
}

void
Chemora_CG_Calc::visited_recompute()
{
  Nodes nstack;
  for ( NIter nd(nodes); nd; )
    {
      nd->visited = false;
      if ( nd->is_sink() ) nstack += nd;
    }

  while ( Node* const nd = nstack.pop() )
    {
      if ( nd->visited ) continue;
      nd->visited = true;
      for ( NIter src(nd->sources); src; )
        {
          assert( !src->cse_remapped && !src->opt_replaced );
          if ( !src->visited ) nstack += src;
        }
    }
}

void
Chemora_CG_Calc::dests_recompute()
{
  // Initialize destination pointers.
  //
  for ( NIter nd(nodes); nd; ) nd->dests.clear();
  for ( NIter nd(nodes); nd; )
    if ( nd->visited )
      for ( NIter src(nd->sources); src; ) src->dests += nd;
}

string
Chemora_CG_Calc::node_cse_key_make(Node *nd)
{
  Static_Op_Info* const soi = soi_get(nd);
  Nodes& srcs = nd->sources;
  Node_IMap ssrcs;
  const int nsrcs = soi->is_commutative ? srcs.size() : 0;
  for ( NIter src(srcs); src; ) ssrcs[ src->idx * nsrcs + src ] = src;
  pStringF key("%x#",nd->no);
  for ( NIMap_Iter it = ssrcs.begin(); it != ssrcs.end(); ++it )
    key.sprintf("%x#",it->second->idx);
  return key.ss();
}

void
Chemora_CG_Calc::node_perf_update(Node *nd, ET_Components *etc)
{
  CaCUDALib_GPU_Info& gi = *gpu_info;

  // If true, instruction will be the multiply part of a multiply/add.
  //
  const bool fusee =
    nd->no == NO_Product && nd->dests == 1 && nd->dests[0]->no == NO_Sum;

  // Modified below.
  nd->latency = 0; nd->issue_usage = 0; nd->ops = 0;

  if ( nd->ctc_val_set || !nd->visited ) return;

  switch ( nd->ntype ) {
  case NT_GFO_Load:
    {
      nd->ops = 1;
      if ( !etc ) return;

      Buffer_Group* const bg = nd->gf->bg;
      assert( bg );

      const int axis = 1 - etc->iter_y;
      const bool needs_shared = nd->di() || nd->df(axis);

      char acc_method = 0;

      switch ( bg->style ) {
      case BS_Not_Cached: acc_method = 'g'; break;
      case BS_Read_Only_Cache: acc_method = gi.roc_sz_bytes ? 'r' : 'g'; break;
      case BS_Pin: acc_method = 'b'; break;
      case BS_Block: acc_method = 's'; break;
      case BS_Plus: acc_method = needs_shared ? 's' : 'b'; break;
      case BS_Constant: acc_method = 'k'; break;
      default: assert( false );
      }

      switch ( acc_method ) {
      case 'g':  // Direct global access.
        nd->latency = gi.gl_mem_lat_cyc.val;
        nd->issue_usage = 1.0 / gi.dev_cap.fu_ls;
        break;
      case 's':  // Shared Memory
        nd->latency = gi.sh_mem_lat_cyc[1].val;
        nd->issue_usage = 1.0 / gi.dev_cap.fu_ls;
        break;
      case 'r':  // Read-only cache.
        // Use SP MADD as closest type to instructions used for
        // address calculation.
        nd->latency =
          2 * gi.sp_fp_op_lat_cyc[FP_MADD].val
          + 0.5 * gi.l2_cache_lat_cyc.val; // Update when ROC avail.
        nd->issue_usage =
          1.0 / gi.dev_cap.fu_ls
          + 2.0 / gi.sp_fp_thpt_op_p_cyc[FP_MADD].val;
        break;
      case 'k':  // Constant address space.  Assume a hit.
        nd->latency = gi.sh_mem_lat_cyc[0].val;
        // Assuming no conflicts.  Still, not sure about throughput.
        nd->issue_usage = 1.0 / gi.sp_fp_thpt_op_p_cyc[FP_MADD].val;
        break;
      case 'b':  // Bypass through a register. (Already loaded, val in reg.)
        break;
      default: assert( false );
      }
    }
    return;

  case NT_GF_Store:
    nd->latency = gi.sp_fp_op_lat_cyc[FP_MADD].val;
    nd->issue_usage = 1.0 / gi.dev_cap.fu_ls;
    nd->ops = 1;
    return;

  case NT_Special:
    nd->latency = 0; nd->issue_usage = 0; nd->ops = 1;
    return;
  case NT_Expression:
    break;
  case NT_If:
    break;
  default:
    assert( false );
  }

  Static_Op_Info* const soi = soi_get(nd);
  assert( soi->lt_set );

  if ( soi->throughput == 0 ) return;

  nd->latency = soi->latency;
  nd->issue_usage = 1.0 / soi->throughput;
  nd->ops = ( nd->no == NO_Sum || nd->no == NO_Product ) ?
    int(nd->sources) - 1 - nd->loop_body - fusee : 1;
  assert( nd->ops >= 0 );
}

void
Chemora_CG_Calc::calc_finalize()
{
  CaCUDALib_GPU_Info& gi =
    opt_rand_seed ? mi->gpu_info_static_get() : mi->gpu_info_get();
  gpu_info = &gi;

  const int assumed_elt_size = sizeof(CCTK_REAL);

  assert( !need_kernel_launch_parameters );

  chemora->static_op_info_lt_init(gpu_info);
  chemora->at_cuda_ctx_available(opt_metrics_extra);

  config_version++;

  /// SHOULD CHECK FOR CHANGES IN PARAMETERS.

  if ( !nodes_backup )
    {
      icache_bytes =
        opt_icache_size == 0 ? 0x1000000 :
        opt_icache_size == 1 ? gi.insn_cache_sz_bytes : opt_icache_size;

      assign_from_literal("E",2.7182818284590452354);
      assign_from_literal("Pi",M_PI);
      assign_from_literal("khalf", 0.5);
      assign_from_literal("kthird", 1/3.0);
      assign_from_literal("ktwothird", 2.0/3.0);
      assign_from_literal("kfourthird", 4.0/3.0);
      assign_from_literal("keightthird", 8.0/3.0);
      assign_from_literal("kminusone", -1);

      for ( map<string,char>::iterator it = opt_assign_force_map.begin();
            it != opt_assign_force_map.end(); ++it )
        {
          if ( it->second == 0 ) continue;
          if ( grid_functions[it->first] ) continue;
          CCTK_VWarn
            (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
"Parameter file error: dynamic_compile_experimental_options attribute\n"
"  assign_force_%c contained a variable, \"%s\", which did not match\n"
"  any grid function.\n",
             it->second, it->first.c_str());
        }

      nodes_at_generic_processing_done();

      if ( !chemora->rand_seed_printed )
        {
          chemora->rand_seed_printed = true;
          mi->dm->msg_info
            ("Random seed set to %d, %s.",
             rand_seed, opt_rand_seed ? "by user option" : "using time");
        }
    }

  nodes_at_specialization_start();

  CaCUDA_Kernel_Launch_Parameters& klp = kernel_launch_parameters;

  assign_from_literal("dx", klp.cagh_dx);
  assign_from_literal("dy", klp.cagh_dy);
  assign_from_literal("dz", klp.cagh_dz);
  Node* const nd_delta_time = assign_from_literal("dt", klp.cagh_dt);

  assign_from_expr("dxi", "Power:1;", "1.0/dx", "dx", "kminusone", NULL);
  assign_from_expr("dyi", "Power:1;", "1.0/dy", "dy", "kminusone", NULL);
  assign_from_expr("dzi", "Power:1;", "1.0/dz", "dz", "kminusone", NULL);

  assign_from_expr("hdxi", "Times:1;", "0.5 * dxi", "khalf", "dxi", NULL);
  assign_from_expr("hdyi", "Times:1;", "0.5 * dyi", "khalf", "dyi", NULL);
  assign_from_expr("hdzi", "Times:1;", "0.5 * dzi", "khalf", "dzi", NULL);

  //
  /// Miscellaneous Sanity Checks
  //
  for ( NIter nd(nodes); nd; )
    assert( nd->ntype != NT_Unknown );


  //
  /// Update value-dependent nodes.
  //

  gf_update();


  ///
  /// Update liveness. (visited)
  //

  visited_recompute();  dests_recompute();

  // Elide identities.
  for ( NIter nd(nodes); nd; )
    if ( nd->visited && nd->identity )
      node_to_identity_and_remove(nd,nd->identity_src);

  ///
  /// Propagate compile-time constants.
  //

  nds_ident_from_op = 0;
  nds_zero_from_mult = 0;
  nds_neg_from_mult = 0;

  {
    // Find initial set of nodes with compile-time constants.
    Nodes ctc;
    for ( NIter nd(nodes); nd; ) if ( nd->ctc_val_set ) ctc += nd;
    nodes_propagate_ctcs(ctc);
  }

  int nds_comb = 0;
  int nds_distr = 0;
  const int opt_iter_limit = 100;

  for ( int opt_iter = 0; opt_iter < opt_iter_limit; opt_iter++ )
    {
      int num_changes = 0;

      assert( opt_iter < opt_iter_limit - 1 );

      Nodes ctc;

      if ( opt_iter == 0 )
        optimize_factor_ac_plus_ax();

      // Combine op(a,op(b,c)) into op(a,b,c) for op in {+,*}.
      //
      for ( NIter nd(nodes); nd; )
        {
          if ( !nd->visited ) continue;
          if ( nd->ctc_val_set ) continue;
          const Node_Operation no = nd->no;
          if ( no != NO_Sum && no != NO_Product ) continue;

          Nodes updated_srcs;
          Nodes stack = nd.curr();
          Nodes removed_nodes;
          int combined_nodes = 0;
          while ( Node* const cur = stack.pop() )
            for ( NIter src(cur->sources); src; )
              if ( src->no == no && src->dests.size() == 1
                   && !src->ctc_val_set )
                {
                  combined_nodes++;
                  stack += src;
                  if ( src->opt_assoc_combined ) continue;
                  src->opt_assoc_combined = true;
                  removed_nodes += src;
                }
              else
                {
                  updated_srcs += src;
                }

          if ( combined_nodes == 0 ) continue;

          nds_comb += combined_nodes;
          num_changes++;

          for ( NIter nrm(removed_nodes); nrm; )
            {
              nrm->visited = false;
              node_srcs_clear(nrm);
            }

          node_srcs_clear(nd);
          double val = no == NO_Sum ? 0 : 1;

          for ( NIter src(updated_srcs); src; )
            if ( src->ctc_val_set )
              {
                if ( no == NO_Sum ) val += src->ctc_val;
                else val *= src->ctc_val;
              }
            else
              {
                node_src_add(nd,src);
              }
          if ( no == NO_Sum && val != 0 || no == NO_Product && val != 1 )
            {
              Node* const nd_val = node_literal_lookup(val);
              ctc += nd_val;
              node_src_add(nd,nd_val);
            }

          node_c_code_make(nd,"Combined.");

          if ( nd->sources == 1 )
            node_to_identity_and_remove(nd,0);
        }

      nodes_propagate_ctcs(ctc);
      ctc.clear();

      optimize_fp_selects();

      // Distribute: x(a+b) ->  xa + xb
      //
      for ( NIter nd(nodes); nd; )
        {
          if ( !nd->visited || nd->no != NO_Product ) continue;
          if ( nd->ctc_val_set ) continue;
          if ( nd->sources.size() != 2 ) continue;
          Node *sum = NULL;
          Nodes other;
          for ( NIter src(nd->sources); src; )
            if ( !sum && src->no == NO_Sum && src->dests.size() == 1 )
              sum = src;
            else
              other += src;
          if ( !sum ) continue;
          assert( other.size() == 1 );
          num_changes++;
          nds_distr++;
          Node* const nd_oth = other[0];
          Node* const n_sum =
            node_new(NT_Expression, "chemora__distr_%d", var_interm_next++);
          n_sum->no = NO_Sum;
          if ( nd_oth->ctc_val_set ) ctc += nd_oth;
          for ( NIter src(sum->sources); src; )
            {
              pStringF lhs_name("chemora__interm_%d",var_interm_next++);
              Node* const n_prod = node_new(lhs_name.s,NT_Expression);
              n_prod->no = NO_Product;
              node_src_add(n_sum,n_prod);
              node_src_add(n_prod,nd_oth);
              node_src_add(n_prod,src);
              if ( src->ctc_val_set ) ctc += src;
              node_c_code_make(n_prod,"Distr prod");
            }
          node_c_code_make(n_sum,"Distr sum");
          node_srcs_of_dests_replace(nd,n_sum);
          node_srcs_clear(nd);
          node_srcs_clear(sum);
          sum->opt_replaced = true; sum->visited = false;
          nd->opt_replaced = true; nd->visited = false;
        }

      nodes_propagate_ctcs(ctc);

      // Double inverses.
      //
      for ( NIter nd(nodes); nd; )
        {
          if ( !nd->visited ) continue;
          if ( nd->no != NO_Negate && nd->no != NO_Reciprocal ) continue;
          Node* const src = nd->sources[0];
          if ( src->no != nd->no ) continue;
          num_changes++;
          Node* const ssrc = src->sources[0];
          node_srcs_of_dests_replace(nd,ssrc);
          node_remove(nd);
          if ( !src->dests ) node_remove(src);
        }

      if ( num_changes == 0 ) break;
    }

  int nds_cfactor = 0;

  // Factor ca + da -> (c+d) a, when c and d are constants.
  //
  for ( NIter nd(nodes); nd; )
    {
      if ( !nd->visited ) continue;
      if ( nd->no != NO_Sum ) continue;
      const int marker = next_marker++;
      Nodes to_remove;
      for ( NIter src(nd->sources); src; )
        {
          if ( src->no != NO_Product ) continue;
          if ( src->sources != 2 ) continue;
          if ( src->dests != 1 ) continue;
          const int val_idx =
            src->sources[0]->ctc_val_set ? 0 :
            src->sources[1]->ctc_val_set ? 1 : -1;
          if ( val_idx == -1 ) continue;
          Node* const cnode = src->sources[val_idx];
          Node* const vnode = src->sources[1-val_idx];
          if ( ! vnode->marker_set(marker) )
            {
              vnode->dests_here = src->idx;
              vnode->examined_to = val_idx;
              continue;
            }
          Node* const fsrc = nodes[vnode->dests_here];
          assert( fsrc->dests[0] == nd );
          to_remove += src;
          const double old_val = fsrc->sources[vnode->examined_to]->ctc_val;
          const double new_val = old_val + cnode->ctc_val;
          node_srcs_clear(fsrc);
          node_src_add(fsrc,vnode);
          node_src_add(fsrc,node_literal_lookup(new_val));
          nds_cfactor++;
        }

      for ( NIter rm(to_remove); rm; )
        {
          node_srcs_clear(rm);
          node_srcs_of_dests_replace(rm,NULL);
          rm->visited = false;
        }
    }

  // CSE
  //
  int nds_cse_remapped_other = 0;
  Node_Map cse_sig_to_node;
  if ( opt_cse )
    for ( NIter nd(nodes); nd; )
      {
        if ( !nd->visited ) continue;
        if ( nd->ntype != NT_Expression ) continue;
        string key = node_cse_key_make(nd);
        if ( Node* const first = cse_sig_to_node.lookup(key) )
          {
            node_srcs_of_dests_replace(nd,first);
            node_remove(nd);
            nds_cse_remapped_other++;
          }
        else cse_sig_to_node[key] = nd;
      }

  // Factor a + a -> 2 a
  //
  for ( NIter nd(nodes); nd; )
    {
      if ( !nd->visited ) continue;
      if ( nd->no != NO_Sum ) continue;
      const int marker = next_marker++;
      Nodes uniq_srcs;
      for ( NIter src(nd->sources); src; )
        {
          if ( ! src->marker_set(marker) )
            {
              uniq_srcs += src;
              src->dests_here = 0;
            }
          src->dests_here++;
        }
      Nodes new_srcs;
      for ( NIter src(uniq_srcs); src; )
        {
          if ( src->dests_here == 1 )
            {
              new_srcs += src;
              continue;
            }
          pStringF lhs_name("chemora__interm_%d",var_interm_next++);
          Node* const n_prod = node_new(lhs_name.s,NT_Expression);
          n_prod->no = NO_Product;
          node_src_add(n_prod,src);
          node_src_add(n_prod,node_literal_lookup(src->dests_here));
          node_c_code_make(n_prod,"CFactor");
          new_srcs += n_prod;
          nds_cfactor++;
        }

      node_srcs_replace(nd,new_srcs);
    }


  // Put node_loop_start in the source list of every node in the
  // loop body.
  //
  const int marker = next_marker++;
  Nodes nstack;
  for ( NIter nd(node_loop_start->dests); nd; )
    {
      if ( nd->marker_set(marker) ) continue;
      assert( nd->visited );
      nstack += nd;
    }
  while ( Node* const nd = nstack.pop() )
    {
      nstack += nd->dests;
      nd->loop_body = true;
      if ( nd->marker_set(marker) ) continue;
      node_src_add(nd,node_loop_start);
    }

  visited_recompute();

  reducible_set();

  // Verify sources and dests.
  //
  for ( NIter nd(nodes); nd; )
    if ( nd->visited )
      {
        for ( NIter src(nd->sources); src; )
          {
            assert( src->visited );
            assert( is_member(src->dests,nd) );
          }
        for ( NIter dst(nd->dests); dst; )
          {
            assert( is_member(dst->sources,nd) );
          }
      }

  for ( NIter nd(nodes); nd; ) node_perf_update(nd);

  fp_ops = 0;
  fp_other = 0;
  fp_not = 0;

  for ( NIter nd(nodes); nd; )
    if ( nd->visited && !nd->ctc_val_set )
      {
        switch ( nd->no ){
        case NO_Product: case NO_Sum: fp_ops += nd->sources.size() - 2; break;
        case NO_Reciprocal: case NO_Divide: case NO_SquareRoot:
        case NO_Power: case NO_Sin: case NO_Cos: fp_other++; break;
        case NO_Negate: break;
        default: fp_not++; break;
        }
      }

  /// Mark GFs To Place in Constant Space
  //
  const int cs_size_bytes = gi.constant_sz_bytes;
  int cs_remaining_bytes = cs_size_bytes;
  for ( GFIter gf(grid_functions); gf; )
    {
      gf->constant_space = false;  // Changed below.
      if ( gf->axes & AX_x ) continue;
      int n_lds = 0;
      for ( auto& nd : gf->nodes ) if ( nd->visited ) n_lds++;
      if ( !n_lds ) continue;
      const int size_elts =
        gf->axes == AX_0 ? 1 :
        gf->axes == AX_y ? klp.cagh_nj :
        gf->axes == AX_z ? klp.cagh_nk :
        gf->axes == AX_yz ? klp.cagh_nj * klp.cagh_nk : 0;
      assert( size_elts );
      const int size_bytes = size_elts * assumed_elt_size;
      gf->cs_local_size_elts = size_elts;
      const bool enough_space = size_bytes <= cs_remaining_bytes;
      assert( gf->axes != AX_0 || enough_space ); // AX_0 not yet implemented.
      if ( !enough_space ) continue;
      cs_remaining_bytes -= size_bytes;
      gf->constant_space = true;
    }


  /// Compute Stencil Bounding Box
  //

  stncl.reset();
  for ( auto nd: do_nodes ) stncl.include(nd->offset);

  assert( module_key_includes_delta_time || !nd_delta_time->visited );

  int nds_visited = 0;
  int nds_ctc = 0;
  int nds_identity = 0;
  int nds_emitable = 0;
  int nds_cse_remapped_ld = 0;

  for ( NIter nd(nodes); nd; )
    {
      if ( nd->visited && !nd->ctc_val_set && !nd->identity )
        nds_emitable++;
      if ( nd->visited ) nds_visited++;
      if ( nd->ctc_val_set ) nds_ctc++;
      if ( nd->identity ) nds_identity++;
      if ( nd->cse_remapped ) nds_cse_remapped_ld++;
    }

  printf("\nCalculation %s finalized.\n", calc_name.c_str());
  printf(" Total %zd, (vis,ctc,ident, eable) (%d,%d,%d, %d)\n",
         nodes.size(), nds_visited, nds_ctc, nds_identity, nds_emitable);
  printf(" Optimizations: (ident_op, zero_mult, neg_mult) (%d,%d,%d)\n",
         nds_ident_from_op, nds_zero_from_mult, nds_neg_from_mult);
  printf("              : (comb,distr,cfact)  (%d,%d,%d)\n",
         nds_comb, nds_distr, nds_cfactor);
  printf("              : (red,srcs)  (%d,%.1f)  CSE (ld,other) (%d,%d)\n",
         nds_red, double(nds_red_srcs)/max(1,nds_red),
         nds_cse_remapped_ld,nds_cse_remapped_other);

}

void
Chemora_CG_Calc::set_kernel_launch_parameters
(CaCUDA_Kernel_Launch_Parameters *klp)
{
  kernel_launch_parameters = *klp;
  need_kernel_launch_parameters = false;
  kus = mi->dm->kernel_user_setting_lookup(calc_name);

  if ( kus )
    if ( kus->tile_y > 1 && kus->tile_z > 1
         || kus->tile_y > 1 && kus->tile_yy > 1
         || kus->tile_z > 1 && kus->tile_zz > 1 )
      CCTK_VWarn
        (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "Parameter file error: in parameter dynamic_compile_tile_size_overrides\n"
         "the setting that matches calculation \"%s\" has a tile configuration that "
         "can't be handled: %d by %d by %d  y iterations %d, z iterations %d.\n"
         "The y or z dimension must be 1. If the number of y iterations is "
         "> 1 the y dimension must be 1, the same holds for z.\n"
         "See CaKernel/CaCUDALib/par/cacudalib.par for details.",
         calc_name.c_str(),
         kus->tile_x, kus->tile_y, kus->tile_z,
         kus->tile_yy, kus->tile_zz);
}

void
Chemora_CG_Calc::nd_print_sinks(Node *nd_start)
{
  Nodes sinks;
  Nodes stack = nd_start;
  const int marker = next_marker++;
  while ( Node* const nd = stack.pop() )
    {
      if ( nd->marker_set(marker) ) continue;
      if ( nd->is_sink() )
        {
          printf(" %d",nd->idx);
          sinks += nd;
        }
      stack += nd->dests;
    }
  if ( !sinks ) printf("None!?!");
  printf("\n");
}


Chemora_CG_Kernel_Info::~Chemora_CG_Kernel_Info()
{
  if ( fi ) delete fi;
}


Mapping_Node::Mapping_Node()
{
  ik_kno = MAX_KERNELS;
  ik_needed = IK_none;
  serial = -1;
}

void
Chemora_CG_Calc_Mapping::init
(Chemora_CG_Calc *cp, int num_kernelsp, string nickname)
{
  c = cp;
  ct = c->ct;
  chemora = c->chemora;
  mapping_nickname = nickname;
  config_version = c->config_version;
  modd = c->modd;
  accept_mp_serial = -1;
  mapping_serial = 1;
  ki_free_contents();
  const int num_nodes = c->nodes.size();
  assert( num_kernelsp <= 8 * sizeof( A_Vector_Type ) );
  num_kernels = num_kernelsp;
  kernel_info.clear();
  kernel_info.resize(num_kernels);
  assignment_vector.resize(num_nodes);
  for ( int i=0; i<num_nodes; i++ ) assignment_vector[i] = A_Vector_Type(0);
  if ( !mapping_node.empty() ) mapping_node.clear();
  mapping_node.resize(num_nodes);
  t_scaled_num_approx = 0;
  t_scaled_approx = 0;
  for ( int kno=0; kno<num_kernels; kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      pStringF function_name
        ("%s_%02d", c->kernel_function_name.c_str(), kno);
      ki->should_run = false;
      ki->etc.init(this);
      ki->function_name = function_name.s;
      assert( !ki->fi );
      if ( !ki->fi )
        {
          ki->fi =  new Function_Info;
          ki->fi->function_name = function_name.s;
          ki->fi->klp = c->kernel_launch_parameters;
        }
    }
}

void
Chemora_CG_Calc_Mapping::ki_free_contents()
{
  for ( int kno=0; kno<kernel_info.size(); kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->fi ) continue;
      assert( report_started || !ki->fi->copied );
      delete ki->fi;
      ki->fi = NULL;
    }
}

Chemora_CG_Calc_Mapping::~Chemora_CG_Calc_Mapping()
{
  ki_free_contents();
  if ( report_cg2_ptr ) delete report_cg2_ptr;
}

Static_Op_Info*
Chemora_CG_Calc_Mapping::soi_get(Node *nd)
{ return c->chemora->static_op_info_get(nd->no); }

Static_Op_Info*
Chemora_CG_Calc::soi_get(Node *nd)
{ return chemora->static_op_info_get(nd->no); }

Mapping_Node*
Chemora_CG_Calc_Mapping::mapping_node_get(Node *nd)
{
  assert( mapping_node.size() > nd->idx );
  return &mapping_node[nd->idx];
}

Mapping_Node*
Chemora_CG_Calc_Mapping::mapping_node_get(Move_Proposal *mp, Node *nd)
{
  Mapping_Node* const md = &mapping_node[nd->idx];
  if ( !mp ) return md;
  Mapping_Node* const mpd = &mp->mapping_node[nd->idx];
  if ( mpd->serial != mp->serial )
    {
      *mpd = *md;
      mpd->serial = mp->serial;
    }
  return mpd;
}

void
Chemora_CG_Calc_Mapping::assign_to(Node *nd, int assign_top)
{
  assert( assign_top < num_kernels );
  Chemora_CG_Kernel_Info* const ki = &kernel_info[assign_top];
  ki->nodes += nd;
  assignment_vector[nd->idx] |= A_Vector_Type(1) << assign_top;
}

void
Chemora_CG_Calc_Mapping::unassign_from(Nodes& nodes, int kno)
{
  assert( kno < num_kernels );
  Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];

  Nodes fewer_nodes;
  A_Vector_Type unmask = ~ ( A_Vector_Type(1) << kno );

  for ( NIter nd(nodes); nd; ) assignment_vector[nd->idx] &= unmask;
  for ( NIter nd(ki->nodes); nd; )
    if ( is_assigned_to(nd,kno) ) fewer_nodes += nd;
  ki->nodes = fewer_nodes;
}

int
Chemora_CG_Calc_Mapping::num_dests_at(Node *nd, int kno)
{
  int n = 0;
  for ( NIter dst(nd->dests); dst; ) if ( is_assigned_to(dst,kno) ) n++;
  return n;
}

bool
Chemora_CG_Calc_Mapping::has_dests_at(Node *nd, int kno)
{
  for ( NIter dst(nd->dests); dst; ) 
    if ( is_assigned_to(dst,kno) ) return true;
  return false;
}

bool
Chemora_CG_Calc_Mapping::has_dests_at(Move_Proposal *mp, Node *nd, int kno)
{
  for ( NIter dst(nd->dests); dst; )
    if ( is_assigned_to(mp,dst,kno) ) return true;
  return false;
}

A_Vector_Type
Chemora_CG_Calc_Mapping::assignment_vector_get(Move_Proposal *mp, Node *nd)
{
  if ( !mp || !nd->marker_check_mp(mp->marker_mp) )
    return assignment_vector_get(nd);
  const A_Vector_Type rv = nd->remove_vector;
  const A_Vector_Type tv = nd->examined_to == ET_Added ? AV(mp->to) : 0;
  const A_Vector_Type av = assignment_vector_get(nd) & ~rv | tv;
  return av;
}

A_Vector_Type
Chemora_CG_Calc_Mapping::nd_consumers_vector_get(Node *nd)
{
  A_Vector_Type cv = 0;
  for ( auto dst: nd->dests ) cv |= assignment_vector[dst->idx];
  return cv;
}

A_Vector_Type
Chemora_CG_Calc_Mapping::nd_consumers_vector_get(Move_Proposal *mp, Node *nd)
{
  if ( !mp ) return nd_consumers_vector_get(nd);
  A_Vector_Type cv = 0;
  for ( auto dst: nd->dests ) cv |= assignment_vector_get(mp,dst);
  return cv;
}

A_Vector_Type
Chemora_CG_Calc_Mapping::ik_consumers_vector_get
(Node *nd, A_Vector_Type mask)
{
  if ( nd->ik_cv_version == accept_mp_serial ) return nd->ik_consumers_vector;
  nd->ik_consumers_vector =
    nd_consumers_vector_get(nd) & ( ~assignment_vector[nd->idx] | mask );
  if ( accept_mp_serial > 0 )
    nd->ik_cv_version = accept_mp_serial;
  return nd->ik_consumers_vector;
}

A_Vector_Type
Chemora_CG_Calc_Mapping::ik_consumers_vector_get(Move_Proposal *mp, Node *nd)
{
  if ( !mp ) return ik_consumers_vector_get(nd,0);
  if ( nd->ik_mp_cv_version == mp->version ) return nd->ik_mp_consumers_vector;
  nd->ik_mp_consumers_vector =
    nd_consumers_vector_get(mp,nd) & ~assignment_vector_get(mp,nd);
  nd->ik_mp_cv_version = mp->version;
  return nd->ik_mp_consumers_vector;
}

int
Chemora_CG_Calc_Mapping::ik_kno_update(Node *nd)
{ return ik_kno_update(NULL,nd); }

int
Chemora_CG_Calc_Mapping::ik_kno_update(Move_Proposal *mp, Node *nd)
{
  Mapping_Node* const md = mapping_node_get(mp,nd);

  if ( !nd->visited || nd->dests == 0
       || nd->ntype == NT_Special || nd->compile_time_constant )
    {
      md->ik_kno = num_kernels;
    }
  else
    {
      const A_Vector_Type producers = assignment_vector_get(mp,nd);
      assert( producers );
      md->ik_kno = ctz(producers);
    }
  return md->ik_kno;
}

Interkernel_Source_Type
Chemora_CG_Calc_Mapping::ik_needed_update(Node *nd)
{ return ik_needed_update(NULL,nd); }

Interkernel_Source_Type
Chemora_CG_Calc_Mapping::ik_needed_update(Move_Proposal *mp, Node *nd)
{
  Mapping_Node* const md = mapping_node_get(mp,nd);
  const A_Vector_Type consumers = ik_consumers_vector_get(mp,nd);

  // IK_simple may be changed in code that executes later.
  md->ik_needed = md->ik_kno < num_kernels && consumers ? IK_simple : IK_none;
  md->ik_grp_sz = 1;

  return md->ik_needed;
}

Interkernel_Source_Type
Chemora_CG_Calc_Mapping::ik_update(Node *nd)
{
  ik_kno_update(nd);
  return ik_needed_update(nd);
}

Interkernel_Source_Type
Chemora_CG_Calc_Mapping::ik_update(Move_Proposal *mp, Node *nd)
{
  ik_kno_update(mp,nd);
  return ik_needed_update(mp,nd);
}

void
Chemora_CG_Calc_Mapping::random_split(int nk)
{
  assert( nk <= num_kernels );
  for ( NIter nd(c->nodes); nd; )
    {
      int na = 0; // Number of assignments.

      if ( !nd->visited ) continue;

      switch ( nd->ntype ) {
      case NT_Special:
        // Ignored cost :-) . (Iteration.)
      case NT_Constant_Expr: case NT_Parameter: case NT_Literal:
        // No cost.
        na = 0;
        break;
      case NT_If:
      case NT_Switch:
      case NT_Expression:
      case NT_GFO_Load:
        na = random() % nk + 1;
        break;
      case NT_GF_Store:
        na = 1;
        break;
      default:
        assert( false );
      }

      int um[nk];
      for ( int i=0; i<nk; i++ ) um[i] = i;
      for ( int i=0; i<na; i++ )
        {
          const int limit = nk - i;
          const int mpi = random() % limit;
          const int mp = um[mpi];
          assign_to(nd,mp);
          um[mpi] = um[limit-1];
        }
    }
  fixup();
}

int
Chemora_CG_Calc_Mapping::split(int num_kernelsp)
{
  int next_kno = 0;
  int num_kernels_used = 0;
  for ( NIter nd(c->nodes); nd; )
    {
      if ( !nd->visited || nd->ntype != NT_GF_Store ) continue;
      assign_to(nd,next_kno);
      next_kno++;
      set_max(num_kernels_used,next_kno);
      if ( next_kno == num_kernelsp ) next_kno = 0;
    }
  fixup();
  return num_kernels_used;
}

void
Chemora_CG_Calc_Mapping::fixup()
{
  // Repair current mapping.
  //
  // - Assign nodes to kernels to satisfy dependencies.
  // - Remove unneeded nodes.
  //
  // Because of the amount of computation involved, this routine
  // should be called only after a mapping is created for the first
  // time. This routine should not have to be called after a call to
  // move_propose and accept.

  // If a non-load source for a node is not produced in the same
  // kernel nor a predecessor kernel, insert a node providing that
  // source.
  //
  for ( int kno = 0;  kno < num_kernels;  kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      Nodes nodes = ki->nodes;
      const A_Vector_Type pred_mask = ( A_Vector_Type(1) << kno + 1 ) - 1;

      while ( Node* const nd = nodes.pop() )
        for ( NIter src(nd->sources); src; )
          {
            if ( src->compile_time_constant ) continue;
            if ( src->mem_accesses == 1 ) continue;
            A_Vector_Type a = assignment_vector[src->idx];
            if ( !( a & pred_mask ) ) { assign_to(src,kno);  nodes += src; }
          }
    }

  // Assign loads.
  for ( int kno = 0;  kno < num_kernels;  kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      Nodes nodes = ki->nodes;
      while ( Node* const nd = nodes.pop() )
        for ( NIter src(nd->sources); src; )
          {
            if ( is_assigned_to(src,kno) ) continue;
            if ( src->mem_accesses != 1 ) continue;
            assign_to(src,kno);
            nodes += src;
          }
    }

  // Assign compile-time constants.
  for ( int kno = 0;  kno < num_kernels;  kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      Nodes nodes = ki->nodes;
      while ( Node* const nd = nodes.pop() )
        for ( NIter src(nd->sources); src; )
          {
            if ( is_assigned_to(src,kno) ) continue;
            if ( !src->compile_time_constant ) continue;
            assign_to(src,kno);
            nodes += src;
          }
    }

  for ( int kno = 0;  kno < num_kernels;  kno++ )
    assign_to(c->node_loop_start,kno);

  Nodes ik_candidates[num_kernels];

  // Insert loads and stores for inter-kernel transfers.
  //
  for ( NIter nd(c->nodes); nd; ) 
    if ( nd->visited )
      {
        const int p_k = ik_kno_update(nd);
        if ( p_k < num_kernels ) ik_candidates[p_k] += nd;
      }

  // Remove unnecessary nodes from each kernel.
  //
  const int num_nodes = c->nodes.size();
  Nodes v2[num_kernels];

  int visited[num_nodes];
  for ( int i=0; i<num_nodes; i++ ) visited[i] = -1; 
  for ( int kno = num_kernels-1;  kno >= 0;  kno-- )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      const A_Vector_Type k_mask = A_Vector_Type(1) << kno;
      const A_Vector_Type maskout = ~k_mask;

      for ( NIter nd(ki->nodes); nd; )
        if ( nd->is_sink() ) v2[kno] += nd;

      while ( Node* const nd = ik_candidates[kno].pop() )
        if ( ik_needed_update(nd) ) v2[kno] += nd;

      for ( NIter nd(v2[kno]); nd; )
        visited[nd->idx] = kno;

      ki->should_run = v2[kno].size();

      Nodes vnodes;
      while ( Node* const nd = v2[kno].pop() )
        {
          vnodes += nd;
          for ( NIter src(nd->sources); src; )
            {
              Mapping_Node* const mrc = mapping_node_get(src);
              if ( visited[src->idx] == kno ) continue;
              visited[src->idx] = kno;
              if ( is_assigned_to(src,kno) )
                {
                  v2[kno] += src;
                  continue;
                }
              assert( mrc->ik_kno < kno );
            }
        }

      while ( Node* nd = ki->nodes.pop() )
        assignment_vector[nd->idx] &= maskout;

      while ( Node* const nd = vnodes.pop() )
        {
          assert( !is_assigned_to(nd,kno) );
          assign_to(nd,kno);
        }
    }
  for ( NIter nd(c->nodes); nd; ) ik_update(nd);

  reduction_update();

  verify(NULL,true);
}

void
Chemora_CG_Calc::reducible_set()
{
  nds_red = 0;
  nds_red_srcs = 0;
  for ( NIter nd(nodes); nd; )
    {
      nd->reducible = false;  // May be changed below.
      if ( !nd->visited ) continue;
      if ( nd->no != NO_Sum ) continue;
      if ( nd->ctc_val_set ) continue;
      if ( !nd->visited ) continue;
      int mult_dest_srcs = 0;
      int reducible_srcs = 0;
      for ( NIter src(nd->sources); src; )
        {
          if ( src->ntype == NT_Special || src->ctc_val_set ) continue;
          if ( src->dests == 1 ) reducible_srcs++; else mult_dest_srcs++;
        }
      if ( reducible_srcs > 2 )
        {
          nd->reducible = true;
          nds_red++;
          nds_red_srcs += reducible_srcs;
        }
    }
}

void
Chemora_CG_Calc_Mapping::reduction_update(Node *nd)
{reduction_update(NULL, nd);}

void
Chemora_CG_Calc_Mapping::reduction_update(Move_Proposal *mp, Node *nd)
{
  // Update leader/follower status of the sources of ND.
  //

  // If it's not a reduction node then there's nothing to update.
  if ( !nd->reducible ) return;

  if ( mp )
    {
      nd->marker_set_mp(mp->marker_mp);
      mp->red_change += nd;
    }

  Mapping_Node* const md = mapping_node_get(mp,nd);
  Node* leaders[num_kernels]; bzero(leaders,sizeof(leaders));
  int num_followers = 0;

  for ( NIter src(nd->sources); src; )
    {
      Mapping_Node* const mrc = mapping_node_get(mp,src);
      mrc->ik_ldr_pos = -1; // May be modified below.
      if ( mp ) ik_update(mp,src);
      if ( !mrc->ik_needed ) continue;
      if ( src->dests != 1 ) continue;
      const int kno = mrc->ik_kno;
      const bool silly = is_assigned_to(mp,nd,kno);
      assert ( !silly );
      if ( !leaders[kno] )
        {
          assert( mrc->ik_needed == IK_simple );
          leaders[kno] = src;
          mrc->reduction_coverage = 0;
          mrc->ik_grp_sz = 0;
          mrc->ik_ldr_pos = int(src);
        }
      Node* const leader = leaders[kno];
      Mapping_Node* const meader = mapping_node_get(mp,leader);
      mrc->ik_ldr_pos = meader->ik_ldr_pos;
      meader->ik_grp_sz++;
      const int grp_pos = meader->ik_grp_sz;
      if ( grp_pos == 2 ) meader->ik_needed = IK_leader;
      if ( grp_pos > 1 )
        {
          mrc->ik_needed = IK_follower;
          num_followers++;
        }
    }
  const int special_count = 1;
  md->red_root_srcs = nd->sources - num_followers - special_count;
  A_Vector_Type av = assignment_vector_get(mp,nd);
  md->red_kno = ctz(av);
  assert( md->red_root_srcs > 0 );
}

void
Chemora_CG_Calc_Mapping::reduction_update()
{
  // Check for reduction opportunities.
  //
  for ( NIter nd(c->nodes); nd; ) if ( nd->visited ) ik_update(nd);
  for ( NIter nd(c->nodes); nd; ) reduction_update(nd);
}

void
Chemora_CG_Calc_Mapping::reduction_check(Move_Proposal *mp, Node *nd)
{
  // Add and remove additional nodes to the move proposal so that
  // reduction node nd and its sources are well placed.

  if ( !nd->reducible ) return;

  A_Vector_Type av = assignment_vector_get(mp,nd);
  A_Vector_Type av_1st = ctz(av);
  A_Vector_Type sav_complete = -1;

  for ( NIter src(nd->sources); src; )
    {
      A_Vector_Type sav = assignment_vector_get(mp,src);
      sav_complete &= sav;
      const int kno_1st = ctz(sav);
      assert( kno_1st <= av_1st );
    }

  A_Vector_Type av_full = sav_complete & av;
  A_Vector_Type av_keep = av_full | AV(av_1st);
  A_Vector_Type av_rm = av & ~av_keep;
  A_Vector_Type tbit = AV(mp->to);
  assert( av_keep );

  assert( nd->examined_to != ET_Added || !( av_rm & tbit ) );

  for ( KNOIter kno(av_rm); kno; ) mp->k_remove_insert_maybe(nd,kno);

  for ( NIter src(nd->sources); src; )
    {
      if ( src->ntype == NT_Special ) continue;
      if ( src->dests != 1 ) continue;
      src->marker_set_mp(mp->marker_mp);
      A_Vector_Type sav = assignment_vector_get(mp,src);
      const int kno_1st = ctz(sav);
      const int kno_use =
        src->examined_to == ET_Added
        && mp->to <= av_1st ? mp->to : kno_1st;
      A_Vector_Type okay = AV(kno_use) | av_full;
      A_Vector_Type not_okay = sav & ~okay;
      for ( KNOIter kno(not_okay); kno; )
        mp->k_remove_insert_maybe(src,kno);
    }
}

bool
Chemora_CG_Calc_Mapping::verify(Move_Proposal *mp, bool ignore_cost)
{
  // Perform sanity checks on mapping.
  //
  // This routine is compute intensive.

  // For debugging.
  static int serial = 0; serial++;

  // Errors that are likely to occur during finalization.
  //
  for ( NIter nd(c->nodes); nd; )
    assert( !nd->visited || !nd->cse_remapped && !nd->opt_replaced );

  // Prepare vector of live kernels.
  //
  A_Vector_Type k_live_vec = 0;
  for ( int kno = 0; kno < num_kernels; kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( ! ki->should_run ) continue;
      k_live_vec |= A_Vector_Type(1) << kno;
    }

  // Make sure that every live kernel has a loop start node.
  //
  const A_Vector_Type loop_av = assignment_vector_get(c->node_loop_start);
  assert( loop_av == k_live_vec );

  if ( mp )
    {
      for ( NIter nd(c->nodes); nd; )
        {
          if ( !nd->reducible ) continue;
          Mapping_Node* const md = mapping_node_get(nd);
          Mapping_Node* const mpd = mapping_node_get(mp,nd);
          assert( md->red_kno == mpd->red_kno );
          assert( md->red_root_srcs == mpd->red_root_srcs );
        }
    }


  // Make sure that every node's sources are available at each
  // kernel to which it is mapped and that every node's result is used.
  // 
  for ( int kno = 0; kno < num_kernels; kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( ki->nodes.size() == 0 )
        {
          assert( !ki->should_run );
          continue;
        }
      assert( ki->should_run );

      ET_Components etc;
      etc.init(this);
      int marker = c->next_marker++;
      Nodes nds_red;

      for ( NIter nd(ki->nodes); nd; )
        {
          Mapping_Node* const md = mapping_node_get(nd);
          assert( is_assigned_to(nd,kno) );
          assert( nd->visited );
          assert( nd->issue_usage >= ZLO );

          // Make sure that node has a purpose.
          //
          int num_consumers = md->ik_needed && md->ik_kno == kno ? 1 : 0;
          for ( NIter dst(nd->dests); dst; )
            if ( is_assigned_to(dst,kno) ) num_consumers++;
          assert( bool(num_consumers) ^ nd->is_sink() );
          
          switch ( nd->ntype ){
          case NT_GF_Store: etc.simple_st++; break;
          default: break;
          }

          if ( md->ik_needed && md->ik_kno == kno && md->ik_active() )
            {
              etc.ik_st++;
              etc.ik_sts_pre += nd;
            }

          // Check whether each source is either here, or an upstream
          // inter-kernel node.
          //
          for ( NIter src(nd->sources); src; )
            {
              Mapping_Node* const mrc = mapping_node_get(src);
              if ( is_assigned_to(src,kno) ) continue;
              assert( mrc->ik_needed && mrc->ik_kno < kno );

              if ( !src->marker_set(marker) && mrc->ik_active() )
                {
                  etc.ik_lds_pre += src;
                  etc.ik_ld++;
                }
              switch ( src->ntype ){
              case NT_Expression: case NT_Switch: case NT_If:
                break;
              default:
                assert( false );
              }
            }

          if ( ! nd->compile_time_constant )
            {
              if ( nd->reducible && md->red_kno == kno )
                {
                  const int num_insn = md->red_root_srcs - 1;
                  etc.issue_usage += num_insn * nd->issue_usage;
                  etc.ops += num_insn;
                  nds_red += nd;
                }
              else
                {
                  etc.ops += nd->ops;
                  etc.issue_usage += nd->ops * nd->issue_usage;
                }

              if ( md->ik_kno == kno && md->ik_needed == IK_follower )
                {
                  etc.ops++;
                  etc.issue_usage += nd->dests[0]->issue_usage;
                }
            }
        }

      if ( !ignore_cost )
        {
          et_gf_update(&etc,kno);
          et_tile_update(&etc,kno);

#define CH(m) etc.m == ki->etc.m
          assert( fabs( etc.issue_usage - ki->etc.issue_usage ) < 1e-6 );
          assert( CH(ops) );
          assert( CH(ik_ld) && CH(ik_st) );
          assert( CH(stencil_ld) && CH(unique_stencil_ld) );
          assert( CH(simple_ld) && CH(simple_st) );
#undef CH
        }
    }

  // Interkernel placement.
  //
  for ( NIter nd(c->nodes); nd; )
    {
      Mapping_Node* const md = mapping_node_get(nd);
      A_Vector_Type av = assignment_vector_get(nd);
      assert( nd->visited == bool( av & k_live_vec ) );
      if ( md->ik_needed )
        {
          A_Vector_Type consumers = ik_consumers_vector_get(nd);
          const int k_min = ctz(av);
          A_Vector_Type avail = A_Vector_Type(1) << ( md->ik_kno + 1 );
          assert( md->ik_kno == k_min );
          assert( nd->mem_accesses != 1 );
          assert( ! nd->is_sink() );
          assert( consumers >= avail );
        }
    }

  // Check reduction nodes.
  for ( NIter nd(c->nodes); nd; )
    {
      if ( !nd->reducible ) continue;
      A_Vector_Type av = assignment_vector_get(nd);
      A_Vector_Type av_complete = -1;
      for ( NIter src(nd->sources); src; )
        av_complete &= assignment_vector_get(src);
      A_Vector_Type av_full = av_complete & assignment_vector_get(nd);
      const int av_full_1st = ctz(av_full);
      A_Vector_Type av_partial = av & ~av_full;
      assert( pop(av_partial) <= 1 );
      assert( !av_full || !av_partial || av_partial < AV(av_full_1st) );

      for ( NIter src(nd->sources); src; )
        {
          if ( src->ntype == NT_Special ) continue;
          if ( src->dests > 1 ) continue;
          if ( src->ctc_val_set ) continue;
          Mapping_Node* const mrc = mapping_node_get(src);
          A_Vector_Type sav = assignment_vector_get(src);
          A_Vector_Type okay = ( AV(mrc->ik_kno) ) | av_full;
          A_Vector_Type not_okay = sav & ~okay;
          assert( !not_okay );
        }
    }

  return true;
}

bool
Chemora_CG_Calc_Mapping::is_assigned_to(Node *nd, int kno)
{
  return assignment_vector[nd->idx] & A_Vector_Type(1) << kno;
}

bool
Chemora_CG_Calc_Mapping::is_assigned_to(Move_Proposal *mp, Node *nd, int kno)
{
  if ( !mp ) return is_assigned_to(nd,kno);
  return assignment_vector_get(mp,nd) & A_Vector_Type(1) << kno;
}

Move_Proposal::Move_Proposal():red_change(NLO_red_change)
{
  k_remove = NULL;
  etc = NULL;
  k_remove_size = 0;
  mapping_node_size = 0;
  mapping_node = NULL;
  m = NULL;
  version = 0;
}

void
Move_Proposal::init(Chemora_CG_Calc_Mapping *mp)
{
  m = mp;
  version = m->c->mp_version_next++;
  const int kno_max = m->num_kernels;
  if ( kno_max >= k_remove_size )
    {
      if ( k_remove ) 
        { 
          delete[] k_remove;
          delete[] etc;
        }
      k_remove_size = kno_max + 1;
      k_remove = new Nodes[k_remove_size];
      etc = new ET_Components[k_remove_size];
    }
  else
    {
      for ( int i=0; i<=kno_max; i++ ) k_remove[i].clear();
    }

  if ( m->c->nodes.size() > mapping_node_size )
    {
      if ( mapping_node ) delete[] mapping_node;
      mapping_node_size = m->c->nodes.size();
      mapping_node = new Mapping_Node[mapping_node_size];
    }

  feasible = false;
  to_add.clear();
  nds_affected.clear();
  ik_remove.clear();
  ik_add.clear();
  ik_update.clear();
  red_change.reset();
}

Move_Proposal::~Move_Proposal()
{
  if ( k_remove ) { delete[] k_remove;  delete [] etc; }
  if ( mapping_node ) delete[] mapping_node;
}

bool
Move_Proposal::k_remove_insert_maybe_maybe(Node *nd, int kno, bool maybe)
{
  A_Vector_Type kv = A_Vector_Type(1) << kno;
  nd->marker_set_mp(marker_mp);
  const bool was_in = kv & nd->remove_vector;
  if ( maybe && was_in ) return true;
  assert( m->is_assigned_to(nd,kno) );
  if ( !nd->remove_vector && nd->examined_to != ET_Added ) nds_affected += nd;

  nd->remove_vector += kv;
  k_remove[kno] += nd;
  version = m->c->mp_version_next++;
  return was_in;
}

bool
Move_Proposal::k_remove_insert_maybe(Node *nd, int kno)
{ return k_remove_insert_maybe_maybe(nd,kno,true); }
bool
Move_Proposal::k_remove_insert(Node *nd, int kno)
{ return k_remove_insert_maybe_maybe(nd,kno,false); }
void
Move_Proposal::k_remove_insert_strict(Node *nd, int kno)
{ 
  const bool was = k_remove_insert_maybe_maybe(nd,kno,false);
  assert( !was );
}


const Stncl_Packed SP_No_Reuse =
  Stncl_Packed(1) << ( stencil_pattern_bits + 6 * stencil_radius_bits );
const Stncl_Packed SP_Force_Global =
  Stncl_Packed(2) << ( stencil_pattern_bits + 6 * stencil_radius_bits );
const Stncl_Packed SP_Force_RO =
  Stncl_Packed(4) << ( stencil_pattern_bits + 6 * stencil_radius_bits );
const Stncl_Packed SP_Constant =
  Stncl_Packed(6) << ( stencil_pattern_bits + 6 * stencil_radius_bits );

Stncl_Packed
stncl_pack(u_int8_t pattern, CStncl& stncl)
{
  Stncl_Packed sh = 0;
  const int mask = ( 1 << stencil_radius_bits ) - 1;
  for ( int i=0; i<3; i++ )
    {
      sh = ( sh << stencil_radius_bits ) + ( mask & stncl.omin[i] );
      sh = ( sh << stencil_radius_bits ) + ( mask & stncl.omax[i] );
    }
  sh = ( sh << stencil_pattern_bits ) + pattern;
  return sh;
}


void
ET_Components::stencil_reset()
{
  stncl.reset();
  stencil_ld = 0;
  pattern = 0;
  unique_stencil_ld = 0;
  sh_ld_pt = 0;
  gl_ld_pt = buf_ls_pt = 0;
  plus_style = pin_style = block_style = 0;
  buffer_group_map.clear();
  buffer_groups.clear();
}

Pattern
Chemora_CG_Calc_Mapping::pattern_find(const Nodes& nds)
{
  Pattern p;
  CStncl& stncl = p.stncl_true;
  u_int8_t& pattern_true = p.pattern_true;
  pattern_true = 0;

  const Offset UNSET = 120;

  for ( auto nd: nds )
    {
      stncl.include(nd->offset);
      pattern_true |= nd->pattern;
    }

  int sten_axes = 0;
  for ( int a=0; a<=SX_z; a++ )
    if ( stncl.halo_width(a) ) sten_axes += 1 << a;
  Offsets ozero;

  switch ( sten_axes ) {
  case AX_0:
    p.ref = ozero;
    break;
  case AX_x: case AX_y: case AX_z:
    p.ref = nds[0]->offset;
    for ( auto nd: nds ) if ( nd->offset == ozero ) { p.ref = ozero; break; }
    break;
  default:
    {
      assert( sten_axes < 8 );
      p.ref = { UNSET, UNSET, UNSET };
      for ( int axis = 0;  axis < 2;  axis++ )
        {
          const int ax_e = axis + 1;
          const int ax_f = ax_e == SX_y ? SX_z : SX_y;
          map<Offset,int> e_cand;
          for ( auto nd: nds ) e_cand[nd->offset[ax_e]]++;
          for ( auto& ac: e_cand )
            {
              Offsets cand( UNSET, UNSET, UNSET );
              const Offset ax_o = ac.first;
              bool good_so_far = true;
              for ( auto nd: nds )
                {
                  if ( nd->offset[ax_e] == ax_o ) continue;
                  if ( cand[SX_x] == UNSET ) { cand = nd->offset;  continue; }
                  if ( cand[SX_x] == nd->offset.i &&
                       cand[ax_f] == nd->offset[ax_f] ) continue;
                  good_so_far = false;
                  break;
                }
              if ( !good_so_far ) continue;
              if ( cand[SX_x] == UNSET ) break;
              cand[ax_e] = ax_o;
              p.ref = cand;
              break;
            }
          if ( p.ref.i != UNSET ) break;
        }
      if ( p.ref.i == UNSET ) p.ref = ozero;
    }
    break;
  }

  u_int8_t pattern = 0;
  memset(p.n_shape,0,sizeof(p.n_shape));
  for ( auto nd: nds )
    {
      const int nd_pattern_pos = pattern_pos_compute(nd->offset,p.ref);
      p.n_shape[nd_pattern_pos]++;
      pattern |= 1 << nd_pattern_pos;
    }

  p.gf = nds[0]->gf;
  p.pattern = pattern;
  p.stncl = CStncl(p.stncl_true,p.ref);

  return p;
}

void
Chemora_CG_Calc_Mapping::et_gf_update(ET_Components *etc, int kno)
{ et_gf_update(NULL,etc,kno); }

void
Chemora_CG_Calc_Mapping::et_gf_update
(Move_Proposal *mp, ET_Components *etc, int kno)
{
  int gf_here = 0;
  int stencil_ld = 0;
  CStncl& stncl = etc->stncl;
  etc->stencil_reset();

  for ( GFIter gf(c->grid_functions); gf; )
    {
      Nodes nds;
      for ( NIter nd(gf->nodes); nd; )
        if ( nd->ntype == NT_GFO_Load && is_assigned_to(mp,nd,kno) )
            nds += nd;

      const int this_gf_here = nds.size();
      if ( !this_gf_here ) continue;

      Pattern p = pattern_find(nds);
      CStncl& gf_bb = p.stncl;
      stncl.include(gf_bb);

      const int n_p0 = p.n_shape[AX_0];
      const int n_py = p.n_shape[AX_y];
      const int n_pz = p.n_shape[AX_z];

      const u_int8_t pattern = p.pattern;
      Array_Axes axes = gf->nodes[0]->gf->axes;
      const bool no_reuse = this_gf_here == 1;
      etc->pattern |= pattern;
      assert(pattern);
      gf_here++;
      stencil_ld += this_gf_here;
      Stncl_Packed idx_raw = stncl_pack(pattern,gf_bb);
      Stncl_Packed idx_special =
        gf->constant_space ? SP_Constant :
        gf->force == 'g' ? SP_Force_Global :
        gf->force == 'r' ? SP_Force_RO : Stncl_Packed(0);
      Stncl_Packed idx =
        ( no_reuse || axes != AX_xyz
          ? axes|SP_No_Reuse : idx_raw|Stncl_Packed(1) ) | idx_special;
      Buffer_Group* const bg = &etc->buffer_group_map[idx];
      if ( !bg->inited )
        {
          bg->stncl = gf_bb;
          bg->n_lds = bg->n_py = bg->n_pz = bg->n_p0 = 0;
          bg->pattern = pattern;
          bg->axes = axes;
          bg->idx = etc->buffer_groups.size();
          etc->buffer_groups += bg;
          bg->inited = true;
          bg->no_reuse = no_reuse;
          bg->force = gf->force;
        }
      assert( bg->force == gf->force );
      bg->pattern |= pattern; // Since LSB of pattern ignored in hash.
      bg->gfs += gf;
      bg->gf_patterns.push_back(p);
      bg->n_lds += this_gf_here;
      bg->n_py += n_py;
      bg->n_pz += n_pz;
      bg->n_p0 += n_p0;
    }

  etc->stencil_ld = stencil_ld;
  etc->unique_stencil_ld = gf_here;
}

static int
my_prod(int start, int vec[3], int parts)
{
  int rv = start;
  for ( int i=0; i<3; i++ ) if ( parts & ( 1 << i ) ) rv *= vec[i];
  return rv;
}

void
Chemora_CG_Calc_Mapping::et_tile_update(ET_Components *etc, int kno)
{
  Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
  CaCUDALib_GPU_Info& gi = *c->gpu_info;

  Function_Info* const fi = ki->fi;

  const bool tile_recompute = true;
  Tile& tile = etc->tile;
  Kernel_User_Setting*& kus = c->kus;

  const bool open_acc = c->opt_code_target_acc;

  if ( tile_recompute )
    {
      const int assumed_elt_size = sizeof(CCTK_REAL);
      const int elt_size_lg = fl1(assumed_elt_size) - 1;
      assert( ( 1 << elt_size_lg ) == assumed_elt_size );
      const int shared_size_bytes = gi.shared_sz_per_bl_bytes;
      const int shared_size_elts = shared_size_bytes >> elt_size_lg;
      const int shared_size_mp_elts = gi.shared_sz_per_mp_bytes >> elt_size_lg;
      const int regs_per_block = gi.max_registers_per_bl;
      const int regs_per_mp = gi.max_registers_per_mp;
      const int regs_per_real = max(1,assumed_elt_size / 4);
      const bool roc_avail = gi.roc_sz_bytes;
      const int roc_sz_elts = gi.roc_sz_bytes / assumed_elt_size;
      const bool halo2 = c->opt_halo2;

      const int i_length_x = fi->klp.i_length_x;  // Length of global grid.
      const int max_overrun_z =
        chemora_cg_overrun_amt
        / ( i_length_x * fi->klp.i_length_y * assumed_elt_size );

      const int user_axis = !kus ? -1 :
        kus->tile_yy > 1 || kus->tile_z > 1 ? 0 : 1;

      int regs_per_thd_guess_a[2], shared_elts_used[2];
      Tile tile_auto[2];
      int block_cnt[2], plus_cnt[2], pin_cnt[2], sh_ld_pt[2], ro_ld_pt[2];
      double gl_ld_pt[2], buf_ls_pt[2], ro_fills_pt[2], ro_unique_pt[2];
      int blocks_per_mp[2], f_halo[2];
      ET_Components etca[2];

      for ( int axis=0; axis<2; axis++ )
        {
          const bool iter_y = axis == 0;
          const Array_Axes e_axv = iter_y ? AX_y : AX_z;
          const Array_Axes f_axv = iter_y ? AX_z : AX_y;
          const Array_Axes xf_axv = AX_x + f_axv; // Plane normal to iter dir.
          const Pattern_Type pt_plus = iter_y ? PT_Plus_Y : PT_Plus_Z;
          const Pattern_Type pt_pin =  iter_y ? PT_Pin_Y  : PT_Pin_Z;
          const int i_length_f =
            !iter_y ? fi->klp.i_length_y : fi->klp.i_length_z;
          const int i_length_e =
             iter_y ? fi->klp.i_length_y : fi->klp.i_length_z;

          int ssboxes[8];  // Shared [memory] stencil boxes.
          int dsboxes[8];  // Data [anything loaded] stencil boxes, use pattern.
          int diboxes[8];  // Data [anything loaded] interior boxes.
          int rsboxes[8];  // RO cache Stencil boxes, use pattern.
          int riboxes[8];  // RO cache interior boxes.
          BZERO(ssboxes); BZERO(dsboxes); BZERO(diboxes);
          BZERO(rsboxes); BZERO(riboxes);
          block_cnt[axis] = plus_cnt[axis] = pin_cnt[axis] = 0;
          sh_ld_pt[axis] = ro_ld_pt[axis] = 0;
          int gl_ld_pt_ax[8]; BZERO(gl_ld_pt_ax);
          gl_ld_pt[axis] = 0;
          int pin_stncl = 0;
          int plus_stncl = 0;
          int num_plus = 0, num_block = 0;
          bool h2_irreg = false;
          bool h2_t0 = false;
          enum Pattern_Axes
          { P1 = 0, Px = 1, Pe = 2, Pf = 4,
            Pxe = 3, Pxf = 5, Pef = 6, Pxef = 7 };
          assert( int(Px) == int(AX_x) );
          int tough_cases = 0;

          // Number of registers to hold address bases for loading ghost zones.
          int regs_addr = 0;

          // Set to false if more shared storage is allocated than is needed.
          bool sh_storage_tight = true;

          for ( BGIter bg(etc->buffer_groups); bg; )
            {
              assert( bg->inited );
              const u_int8_t pattern = 1 | bg->pattern;
              const int ngfs = int(bg->gfs);

              diboxes[bg->axes] += ngfs;

              if ( bg->gfs[0]->constant_space )
                {
                  regs_addr++;  // Actually, at most 3.
                  bg->style_try[axis] = BS_Constant;
                  gl_ld_pt_ax[bg->axes] += bg->n_lds;
                  if ( bg->n_lds != ngfs ) tough_cases++;
                  continue;
                }

              // Low-Dim Array
              //
              if ( bg->axes != AX_xyz )
                {
                  const bool roc = roc_avail
                    && c->opt_buf_low_dim == 'r'
                    && ( bg->axes & xf_axv ) != xf_axv;
                  // Style may be changed if tile.f == 1.
                  bg->style_try[axis] =
                    roc ? BS_Read_Only_Cache : BS_Not_Cached;
                  if ( roc )
                    {
                      ro_ld_pt[axis] += bg->n_lds;
                      riboxes[bg->axes] += ngfs;
                    }
                  else
                    {
                      gl_ld_pt_ax[bg->axes] += bg->n_lds;
                      if ( bg->n_lds != ngfs ) tough_cases++;
                    }
                  continue;
                }

              if ( bg->no_reuse && c->opt_buf_single_use != 's' )
                {
                  const bool roc = roc_avail && c->opt_buf_single_use == 'r';
                  bg->style_try[axis] =
                    roc ? BS_Read_Only_Cache : BS_Not_Cached;
                  if ( roc )
                    {
                      ro_ld_pt[axis] += bg->n_lds;
                      riboxes[bg->axes] += ngfs;
                    }
                  else
                    {
                      gl_ld_pt_ax[bg->axes] += bg->n_lds;
                    }
                  assert( ngfs == bg->n_lds );
                  continue;
                }

              int dsize[3] = { bg->stncl.x(), bg->stncl.y(), bg->stncl.z() };
              for ( int i=0; i<8; i++ )
                if ( pattern & ( 1 << i ) ) dsboxes[i] += my_prod(ngfs,dsize,i);

              const bool is_plus = pattern_type_is( pattern, pt_plus );
              const bool is_pin = pattern_type_is( pattern, pt_pin );
              const int stncl_e = iter_y ? bg->stncl.y() : bg->stncl.z();
              const int stncl_f = iter_y ? bg->stncl.z() : bg->stncl.y();

              if ( roc_avail && bg->force == 'r'
                  || !is_pin && roc_avail &&
                   ( c->opt_buf_sten_x == 'r'
                     && bg->stncl.x() && !bg->stncl.y() && !bg->stncl.z()
                     || c->opt_buf_sten_y == 'r'
                     && bg->stncl.y() && !bg->stncl.z()
                     || c->opt_buf_sten_z == 'r'
                     && bg->stncl.z() ) )
                {
                  bg->style_try[axis] = BS_Read_Only_Cache;
                  for ( int i=0; i<8; i++ )
                    if ( pattern & ( 1 << i ) )
                      rsboxes[i] += my_prod(ngfs,dsize,i);
                  ro_ld_pt[axis] += bg->n_lds;
                  riboxes[bg->axes] += ngfs;
                  continue;
                }

              if ( pattern <= 1
                   || bg->force == 'g'
                   || !is_pin && open_acc
                   || !is_pin &&
                   ( c->opt_buf_sten_x == 'g'
                     && bg->stncl.x() && !bg->stncl.y() && !bg->stncl.z()
                     || c->opt_buf_sten_y == 'g'
                     && bg->stncl.y() && !bg->stncl.z()
                     || c->opt_buf_sten_z == 'g'
                     && bg->stncl.z() ) )
                {
                  bg->style_try[axis] = BS_Not_Cached;
                  if ( bg->n_lds != ngfs ) tough_cases++;
                  gl_ld_pt_ax[bg->axes] += bg->n_lds;
                  continue;
                }

              if ( bool(bg->stncl.x()) + bool(stncl_e) + bool(stncl_f) > 1 )
                sh_storage_tight = false;

              int ssize[3] = { bg->stncl.x(), is_plus ? 0 : stncl_e, stncl_f };

              if ( is_pin )
                {
                  bg->style_try[axis] = BS_Pin;
                  pin_cnt[axis] += ngfs;
                  pin_stncl += ngfs * stncl_e;
                }
              else if ( is_plus )
                {
                  bg->style_try[axis] = BS_Plus;
                  plus_cnt[axis] += ngfs;
                  plus_stncl += ngfs * stncl_e;
                  sh_ld_pt[axis] +=
                    bg->n_lds - bg->n_p0 - ( iter_y ? bg->n_py : bg->n_pz );

                  const int halo_rounds = 1;
                  // ptxr_halo_gl_base[halo_rounds], ptxr_halo_sh_addr
                  if ( !halo2 ) regs_addr += halo_rounds * ( 2 + 1 );
                  if ( bg->stncl.x() ) h2_irreg = true;
                  if ( stncl_f ) h2_t0 = true;
                  num_plus++;
                }
              else
                {
                  bg->style_try[axis] = BS_Block;
                  block_cnt[axis] += ngfs;
                  sh_ld_pt[axis] += bg->n_lds;
                  // ptxr_sc_gl_thd_base and ptxr_sc_sc_thd_base;
                  if ( !halo2 ) regs_addr += 2 + 1;
                  h2_t0 = true;
                  num_block++;
                }

              // Collect factors needed to compute number of shared
              // elements needed based on block-shaped shared storage.
              // (That is, don't omit corners even when bg->pattern
              // shows that the corners won't be accessed.
              if ( !is_pin )
                for ( int i=0; i<8; i++ ) ssboxes[i] += my_prod(ngfs,ssize,i);
            }

          int total_halo = 0;
          for ( int i=1; i<8; i++ ) total_halo += ssboxes[i];

          // Does not include lo-d accesses that use constant memory.
          int n_lo_dim_bypass_elts = 0;
          for ( Array_Axes i=1; i<8; i++ )
            if ( gl_ld_pt_ax[i] || riboxes[i] ) {
              if ( i & e_axv ) regs_addr += 2;
              else             n_lo_dim_bypass_elts += diboxes[i];  }

          // Overlapped computation of load address. ROC insn don't have
          // offsets in CC < 5.0;
          if ( gi.cc_major < 5 )
            regs_addr += 2 * min(ro_ld_pt[axis],4);

          // Instruction sets starting at Maxwell have a smaller offset
          // and so more address computation is necessary.
          if ( gi.cc_major >= 5 )
            regs_addr +=
              2 * min( etc->simple_st + ro_ld_pt[axis]
                       + gl_ld_pt[axis] + sh_ld_pt[axis], 16.0);

          if ( halo2 && h2_irreg ) regs_addr += 3;
          if ( halo2 && h2_t0 ) regs_addr += 3;

          const int regs_per_thd_guess = regs_per_thd_guess_a[axis] =
            regs_addr
            + 4
            + regs_per_real * etc->ops / 16.0
            + ( total_halo ?
                ( bool(etc->simple_st + etc->simple_ld) + 1 ) * 2 : 2 )
            + etc->ik_ld * 2
            + ( etc->ik_ld + etc->ik_st ? 2 : 0 )
            + regs_per_real
            * ( pin_stncl + pin_cnt[axis] + plus_stncl + plus_cnt[axis] +
                n_lo_dim_bypass_elts );

          // Observed on Kepler: Maybe number of regs rounded up to mult of 8.
          const int regs_per_thd_clamped =
            min( gi.max_registers_per_th, max(1,(regs_per_thd_guess+7)>>3<<3) );

          const int max_threads_raw =
            min( gi.max_threads_per_bl, regs_per_block / regs_per_thd_clamped );

          const int max_threads_p_mp_raw =
            min( gi.max_threads_per_mp, regs_per_mp / regs_per_thd_clamped );

          // Observed on Kepler: Max number of threads a multiple of 128.
          const int max_threads = max_threads_raw >> 7 << 7;
          const int max_threads_mp = max_threads_p_mp_raw >> 7 << 7;

          // Interior and xf face.  Num elts = qxf * tile_x * tile_f.
          const int qxf = ssboxes[P1] + ssboxes[Pe];
          const int qx = ssboxes[Pf] + ssboxes[Pef];
          const int qf = ssboxes[Px] + ssboxes[Pxe];
          const int q1 = ssboxes[Pxf] + ssboxes[Pxef];

          if ( axis == user_axis )
            {
              tile_auto[axis].x = kus->tile_x;
              tile_auto[axis].y = kus->tile_y;
              tile_auto[axis].z = kus->tile_z;
              tile_auto[axis].yy = kus->tile_yy;
              tile_auto[axis].zz = kus->tile_zz;
            }
          else if ( open_acc )
            {
              tile_auto[axis].x = i_length_x;
              tile_auto[axis].y = 1;
              tile_auto[axis].z = 1;
              tile_auto[axis].yy = 1;
              tile_auto[axis].zz = i_length_e;
            }
          else
            {
              const int sb_tile_x = min(i_length_x,32); // Small-block tile x.
              int td_sb[3] = { sb_tile_x, 1, 1 };
              int fh = 0;
              for ( int i=1; i<8; i++ )
                if ( i & f_axv ) fh += my_prod(dsboxes[i],td_sb,i^7);

              const int num_mp = gi.num_multiprocessors;
              const double clock_freq_hz = gi.clock_freq_hz;
              const double thpt_data_p_chip_Bps = gi.chip_bw_Bps;
              const double thpt_data_p_mp =
                thpt_data_p_chip_Bps
                / ( assumed_elt_size * num_mp * clock_freq_hz );

              const double load_latency = gi.gl_mem_lat_cyc.val;
              const double f_pen = max(1,fh) / thpt_data_p_mp;
              const double n_blocks_balance = load_latency / f_pen;

              const bool f_sten = ssboxes[Pf] + ssboxes[Pef] + ssboxes[Pxef];

              const int min_thds = 256;
              const int min_thds_f =
                f_sten ? int(0.6+sqrt(min_thds/2)) : 1;
              const int min_thds_x = f_sten ? min_thds_f * 2 : 1;
              const int sm_min =
                qxf * min_thds_x * min_thds_f
                + qf * min_thds_f + qx * min_thds_x + q1;

              const int n_blks_lim_sm = shared_size_mp_elts / max(1,sm_min);
              const int n_blks_lim_thds = max_threads_mp / min_thds;
              const bool small_blocks =
                c->opt_small_block * n_blocks_balance > 10;

              const int n_blks_goal =
                small_blocks
                ? min(4,max(1, min(min(n_blks_lim_sm,n_blks_lim_thds),
                                   int(n_blocks_balance)))) : 1;

              const int thd_goal =
                min( max_threads, max_threads_mp / n_blks_goal );

              const int shared_goal_elts =
                min( shared_size_elts, shared_size_mp_elts / n_blks_goal );

              const int x_max_1 = thd_goal;
              const int x_max_2 =
                ( shared_goal_elts - q1 - qf ) / max( 1, qx + qxf );
              const int x_max = min( min(x_max_1,x_max_2), i_length_x);

              const int x_bias = f_sten ? 2 : x_max;
              const double f_max_regs = sqrt(double(thd_goal)/x_bias);

              // Quadratic formula ssboxes coefficients.
              const double qfa = x_bias * qxf;
              const double qfb = x_bias * qx + qf;
              const double qfc = q1 - shared_goal_elts;
              const double rterm = qfb * qfb - 4 * qfa * qfc;
              assert( rterm >= 0 );
              const double rad = sqrt( rterm );
              const double edge_raw =
                qfa > 0.1 ? ( - qfb + rad ) / ( 2 * qfa ) : 1;
              const double edge = min(edge_raw,f_max_regs);

              const int tile_x_maybe =
                min( max( min(x_max,sb_tile_x), int( x_bias * edge ) ),
                     i_length_x );
              const int tile_x_n = divup( i_length_x, tile_x_maybe );
              tile_auto[axis].x = divup( i_length_x, tile_x_n );
              assert( tile_auto[axis].x>0 && tile_auto[axis].x<=tile_x_maybe );
              const int tile_f_max = thd_goal / tile_auto[axis].x;
              const int tile_f_raw =
                ( shared_goal_elts - q1 - qx * tile_auto[axis].x )
                / max(1, ( qf + qxf * tile_auto[axis].x ));
              const int tile_f_maybe
                = min( min( tile_f_raw, i_length_f ), tile_f_max );
              const int tile_f_n = divup( i_length_f, tile_f_maybe );
              const int tile_f_nu = divup( i_length_f, tile_f_n );
              assert( tile_f_nu > 0 );

              const int overrun_f = rndup(i_length_f,tile_f_nu) - i_length_f;
              const int tile_f =
                iter_y && overrun_f > max_overrun_z
                ? max_overrun_z + 1 : tile_f_nu;

              tile_auto[axis].y =  iter_y ? 1 : tile_f;
              tile_auto[axis].z = !iter_y ? 1 : tile_f;
            }

          const int tile_x = tile_auto[axis].x;
          const int tile_y = tile_auto[axis].y;
          const int tile_z = tile_auto[axis].z;
          const int tile_f = iter_y ? tile_z : tile_y;
          const int n_thds = tile_x * tile_y * tile_z;

          // Determine maximum number of iterations that will fit
          // within the instruction cache.
          //
          const u_int32_t num_wp = ( n_thds + warp_sz - 1 ) >> warp_lg;
          const int max_regs_per_thd = gi.max_registers_per_th;
          const int avail_regs_per_thd =
            gi.max_registers_per_bl / ( num_wp << warp_lg );
          const int regs_per_thd = etca[axis].regs_per_thd =
            min(max_regs_per_thd,avail_regs_per_thd);
          const int over_regs = etca[axis].over_regs =
            max(0, regs_per_thd_guess - regs_per_thd );

          // Number of instruction outside of main (unrolled) loop.
          //
          const int nonloop_insn = etca[axis].code_nonloop_insn =
            50 + 17 * ( num_plus + num_block );
          const int nonloop_code_bytes =
            nonloop_insn * gi.code_size_per_insn_bytes;

          // Instructions for Plus- and Block-Style Halo Buffering

          const int length_xm = tile_x + etc->stncl.x();
          const int wp_per_t0 = divup( length_xm, 32 );
          int num_insn_buff = 0;
          for ( BGIter bg(etc->buffer_groups); bg; )
            {
              const int stncl_f = iter_y ? bg->stncl.z() : bg->stncl.y();
              const int length_f = tile_f + stncl_f;
              if ( bg->style_try[axis] == BS_Plus )
                num_insn_buff +=
                  bg->gfs.size() * 2 *
                  ( 1 + divup( bg->stncl.x() * length_f, 32 )
                    + stncl_f * wp_per_t0 );
              else if ( bg->style_try[axis] == BS_Block )
                num_insn_buff += bg->gfs.size() * 2 * length_f * wp_per_t0;

            }

          const int num_wg = min( int(num_wp), num_insn_buff/2 );
          num_insn_buff += num_wg * 3;

          etca[axis].code_buff_insn = num_insn_buff;

          // Instructions per iteration.
          //
          const int loop_body_insn = etca[axis].code_iter_insn =
            etc->ops + num_insn_buff + etc->ik_ld + etc->ik_st
            + 2 * over_regs
            + 4 * ro_ld_pt[axis];

          if ( axis != user_axis )
            {
              const double iter_code_size_bytes =
                loop_body_insn * gi.code_size_per_insn_bytes;

              // Upper bound on the number of iterations for which
              // the entire kernel will fit in the instruction cache.
              //
              const int ee_limit_icache =
                ( c->icache_bytes - nonloop_code_bytes )
                / max( 1, iter_code_size_bytes );

              const int e_faces_elts
                = my_prod(dsboxes[e_axv],&tile_auto[axis].x,e_axv^7);
              const int e_faces_bytes = e_faces_elts * assumed_elt_size;
              const int k_nonloop_bytes = e_faces_bytes + nonloop_code_bytes;

              // Preferred number of iterations, ignoring instruction
              // cache.
              //
              const int tile_ee_1 = min(10,i_length_e);

              const bool ee_crawlout_possible =
                c->icache_bytes < k_nonloop_bytes;

              // Lower bound on the number of iterations for which
              // data volume is lower despite instruction cache
              // misses.
              //
              const double ee_crawlout =
                !ee_crawlout_possible ? tile_ee_1 + 1.0 :
                k_nonloop_bytes
                / ( iter_code_size_bytes
                    * ( e_faces_bytes / ( c->icache_bytes - nonloop_code_bytes )
                        - 1 ) );

              assert( !ee_crawlout_possible || ee_crawlout > ee_limit_icache );

              const int tile_ee =
                ee_limit_icache == 0 ? tile_ee_1 :
                ee_crawlout <= tile_ee_1 ? tile_ee_1 :
                min( ee_limit_icache, tile_ee_1 );

              assert( tile_ee > 0 );

              tile_auto[axis].yy = !iter_y ? 1 : tile_ee;
              tile_auto[axis].zz =  iter_y ? 1 : tile_ee;
            }

          const int tile_ee = iter_y ? tile_auto[axis].yy : tile_auto[axis].zz;
          const int tile_yyy = tile_auto[axis].yy * tile_auto[axis].y;
          const int tile_zzz = tile_auto[axis].zz * tile_auto[axis].z;

          shared_elts_used[axis] =
            qxf * tile_x * tile_f + qx * tile_x + qf * tile_f + q1;

          const int bmp_lim_shared =
            gi.shared_sz_per_mp_bytes
            / ( assumed_elt_size * max(1,shared_elts_used[axis]) );
          const int bmp_lim_wps = gi.max_threads_per_mp / n_thds;
          const int bmp_lim_regs =
            gi.max_registers_per_mp / ( n_thds * regs_per_thd_clamped );
          blocks_per_mp[axis] =
            c->opt_force_one_block_per_mp ? 1 : gi.max_blocks_per_mp;
          set_min(blocks_per_mp[axis],bmp_lim_shared);
          set_min(blocks_per_mp[axis],bmp_lim_wps);
          set_min(blocks_per_mp[axis],bmp_lim_regs);

          int ttsize[3] = { tile_x, tile_ee, tile_f };
          int tszxyz[3] = { tile_x, tile_yyy, tile_zzz };
          int tileszxyz[3] = { tile_x, tile_y, tile_z };
          const int num_gps = my_prod(1,ttsize,7);
          const double num_gps_inv = 1.0 / num_gps;

          int fh = 0;
          for ( int i=1; i<8; i++ )
            if ( i & f_axv ) fh += my_prod(dsboxes[i],tileszxyz,i^7);
          f_halo[axis] = fh;

          // Low-Dim Special Case: Buffering along a length-1 dimension, f.
          //
          if ( tile_f == 1 )
            for ( BGIter bg(etc->buffer_groups); bg; )
              if ( ( bg->axes == AX_xy || bg->axes == AX_xz )
                   && bg->style_try[axis] != BS_Not_Cached )
                bg->style_try[axis] = BS_Not_Cached;

          assert( gl_ld_pt[axis] == 0 );
          int gl_ld = 0;
          for ( int i=1; i<8; i++ )
            gl_ld += my_prod(gl_ld_pt_ax[i],tszxyz,i);
          gl_ld_pt[axis] = gl_ld * num_gps_inv;
          gl_ld_pt[axis] += pin_cnt[axis] + double(pin_stncl) / tile_ee;
          gl_ld_pt[axis] += double(plus_stncl) / tile_ee;

          int gf_elts_per_block = 0;
          for ( int i=1; i<8; i++ )
            gf_elts_per_block += (   my_prod(dsboxes[i],tszxyz,i^7)
                                   + my_prod(diboxes[i],tszxyz,i)  );

          int buf_elts_per_block = 0;
          for ( int i=0; i<8; i++ )
            buf_elts_per_block += my_prod(ssboxes[i],ttsize,i^7);

          buf_ls_pt[axis] = buf_elts_per_block * num_gps_inv;

          int ro_unique_per_block = 0;
          for ( int i=1; i<8; i++ )
            ro_unique_per_block += (  my_prod(rsboxes[i],tszxyz,i^7)
                                    + my_prod(riboxes[i],tszxyz,i)  );
          ro_unique_pt[axis] = ro_unique_per_block * num_gps_inv;
          int ro_sz_needed_per_block = 0;
          for ( int i=1; i<8; i++ )
            ro_sz_needed_per_block += (   my_prod(rsboxes[i],tileszxyz,i^7)
                                       + my_prod(riboxes[i],tileszxyz,i)  );
          const int ro_sz_needed = ro_sz_needed_per_block * blocks_per_mp[axis];

          // This is a wild guess.
          const double roc_oversubscription_raw =
            double(ro_sz_needed) / roc_sz_elts;
          const double roc_over =
            min(1.0, max(0.0,roc_oversubscription_raw-1) );
          ro_fills_pt[axis] =
            (1-roc_over) * ro_unique_pt[axis] + roc_over * ro_ld_pt[axis];

          const double gl_2_pt = gf_elts_per_block * num_gps_inv;
          const double gl_check =
            gl_ld_pt[axis] + buf_ls_pt[axis] + ro_unique_pt[axis];
          const double err = gl_2_pt - gl_check;
          assert( tough_cases
                  || (  sh_storage_tight
                        ? fabs(err) < 0.0001 : err <= 0.0001 ) );
          assert( shared_elts_used[axis] <= shared_size_elts );
        }

      const bool auto_iter_y =
        shared_elts_used[0] == shared_elts_used[1]
        ? pin_cnt[0] > pin_cnt[1]
        : shared_elts_used[0] < shared_elts_used[1];
      const int ai = open_acc ? 1 :
        user_axis >= 0 ? user_axis : auto_iter_y ? 0 : 1;

      for ( BGIter bg(etc->buffer_groups); bg; ) bg->style = bg->style_try[ai];
      etc->pin_style = pin_cnt[ai];
      etc->plus_style = plus_cnt[ai];
      etc->block_style = block_cnt[ai];
      etc->groupless_style =
        c->opt_buf_single_use == 'r' ? BS_Read_Only_Cache : BS_Not_Cached;
      etc->gl_ld_pt = gl_ld_pt[ai];
      etc->ro_ld_pt = ro_ld_pt[ai];
      etc->ro_fills_pt = ro_fills_pt[ai];
      etc->ro_unique_pt = ro_unique_pt[ai];
      etc->sh_ld_pt = sh_ld_pt[ai];
      etc->buf_ls_pt = buf_ls_pt[ai];
      etc->regs_per_thd_guess = regs_per_thd_guess_a[ai];
      etc->blocks_per_mp = blocks_per_mp[ai];
      etc->f_halo = f_halo[ai];
#     define C(m) etc->m = etca[ai].m;
      C(code_nonloop_insn); C(code_iter_insn); C(code_buff_insn);
      C(regs_per_thd); C(over_regs);
#     undef C
      tile = tile_auto[ai];
      const bool iter_y = etc->iter_y = tile.yy > 1 || tile.z > 1;

      assert( etc->iter_y && tile.y == 1 || !etc->iter_y && tile.z == 1 );

      const int i_length_e =  iter_y ? fi->klp.i_length_y : fi->klp.i_length_z;
      const int i_length_f = !iter_y ? fi->klp.i_length_y : fi->klp.i_length_z;
      const int tile_f = iter_y ? tile.z : tile.y;
      const int tile_ee = iter_y ? tile.yy : tile.zz;

      const int blocks_x = divup( i_length_x, tile.x );
      const int blocks_f = divup( i_length_f, tile_f );
      const int full_blocks_f = i_length_f / tile_f;
      const int blocks_e = divup( i_length_e, tile_ee );
      const int grid_xf = blocks_x * blocks_f;
      const int grid = etc->num_blocks = grid_xf * blocks_e;
      etc->load_balance = double(grid) / rndup(grid,gi.num_multiprocessors);

      const int thds_per_block = tile.x * tile_f;
      const int thds_per_grid = thds_per_block * grid_xf;

      const double work_xf = i_length_x * i_length_f;
      etc->utilization_thds = work_xf / thds_per_grid;

      const int thds_per_block_gr = rndup( thds_per_block, gi.thd_granularity );
      const int thds_fl_xf_gr = thds_per_block_gr * blocks_x * full_blocks_f;
      const int len_f_part = i_length_f % tile_f;
      const int thds_pt_xf_gr =
        rndup(tile.x * len_f_part, gi.thd_granularity) * blocks_x;

      const double util_xf_gr =
        double(work_xf) / ( thds_fl_xf_gr + thds_pt_xf_gr );
      etc->utilization_slots = util_xf_gr;
    }
}

void
Chemora_CG_Calc_Mapping::mp_add_plus(Move_Proposal *mp, Node *nd)
{
  if ( is_assigned_to(nd,mp->to) ) return;
  nd->marker_set_mp(mp->marker_mp); // Initialize proposal-related members.
  if ( nd->examined_to == ET_Added ) return;
  mp_add_plus_strict(mp,nd);
}

void
Chemora_CG_Calc_Mapping::mp_add_plus_strict(Move_Proposal *mp, Node *nd_start)
{
  const int to = mp->to;
  const int marker_mp = mp->marker_mp;
  const bool upstream = mp->to < mp->from;
  Nodes to_stack = nd_start;

  // At to kernel, add constant sources and scalar load sources.
  // Use inter-kernel loads for the remaining sources.
  //
  while ( Node* const nd = to_stack.pop() )
    {
      mp->to_add += nd;
      mp->version = c->mp_version_next++;
      if ( !nd->remove_vector ) mp->nds_affected += nd;
      nd->examined_to = ET_Added;
      bool add_all_srcs = false;
      if ( nd->reducible )
        {
          A_Vector_Type av = assignment_vector_get(mp,nd);
          const int av_1st = ctz(av);
          if ( mp->to > av_1st ) add_all_srcs = true;
        }

      for ( NIter src(nd->sources); src; )
        {
          // Check whether the source should also be added to kernel TO.

          if ( is_assigned_to(src,to) ) continue;
          src->marker_set_mp(marker_mp); // Initialize proposal-related members.
          if ( src->examined_to ) continue;

          src->examined_to = ET_Examined; // May be re-assigned below.

          Mapping_Node* const mrc = mapping_node_get(src);

          // If true, node has zero cost and it doesn't matter where
          // it is. Add it to the to_add list so its sources can be
          // examined, which might be better off at the TO kernel.
          const bool lite =
            src->mem_accesses == 0 && src->ops == 0
            && src->sources.size() < 3;

          switch ( src->ntype ) {
          case NT_Parameter: case NT_Constant_Expr: case NT_Literal:
          case NT_GFO_Load:
            break;

          case NT_Expression:
          case NT_Switch: case NT_If:
            if ( add_all_srcs ) break;
            if ( lite ) break;
            if ( !upstream ) continue;
            if ( !mrc->ik_needed ) ik_kno_update(src);
            if ( mrc->ik_kno < to ) continue;
            break;

          case NT_Special:
            break;

          default:
            assert( false );
            continue;
          }

          to_stack += src;
        }
    }
}

double
Chemora_CG_Calc_Mapping::move_propose(Move_Proposal *mp)
{
  // Return change in cost of moving candidate.
  // Populate Move_Proposal object with information about proposed
  // change, which will be used if move is accepted.

  const int from = mp->from;
  const int to = mp->to;
  Node* const candidate = mp->candidate;

  const bool debug = true;

  // The variable serial is used to help in setting debugger breakpoints,
  // which is why it's declared static.
  static int serial = 0;  serial++;

  const bool upstream = from > to;

  mp->serial = ++mapping_serial;
  mp->feasible = false; // Possibly reassigned.

  // Check for some impossible or pointless moves.
  //
  switch ( candidate->ntype ){
  case NT_GFO_Load:
    if ( upstream ) return -1;
    // Fall through.
  case NT_GF_Store:
  case NT_Expression: case NT_Switch: case NT_If:
    break;
  default:
    return -1;
  }

  mp->init(this);

  Nodes& to_add = mp->to_add;

  const A_Vector_Type vec_from = A_Vector_Type(1) << from;
  const A_Vector_Type vec_un_from = ~ vec_from;
  const A_Vector_Type vec_to = A_Vector_Type(1) << to;

  int to_min = num_kernels;

  const int marker_mp = mp->marker_mp = c->next_marker++;

  int num_sinks = 0;  // Number of sinks being moved.

  // Collective assignment vector of destinations on frontier of
  // removed nodes. Setting TO downstream of the most downstream
  // element of remove_descendants_av would result in all of the added
  // nodes being removed by DCE unless the remove set included a
  // sink. The code here uses a stricter test: if num_sinks ==0, the
  // destination kernel, TO, must be one in which the corresponding
  // bit of remove_descendants_av is 1.
  A_Vector_Type remove_descendants_av = 0;

  // Collect list of nodes to add to to kernel.
  // Collect initial list of nodes to remove from from kernel.
  // Determine whether moved nodes would break an ik transfer.
  //
  Nodes rm_stack;

  const bool copy_from = mp->copy_from;

  if ( copy_from )
    {
      Nodes add_stack = candidate;
      remove_descendants_av = vec_to;
      while ( Node* const nd = add_stack.pop() )
        {
          if ( is_assigned_to(nd,to) ) continue;
          mp_add_plus(mp,nd);
          add_stack += nd->sources;
        }
    }
  else
    {
      rm_stack += candidate;
    }

  if ( mp->gang_move && candidate->ntype == NT_GFO_Load )
    {
      assert( !copy_from );
      Grid_Function* const gf = candidate->gf;
      bool found_cand = false;
      for ( NIter nd(gf->nodes); nd; )
        {
          if ( nd == candidate ) { found_cand = true; continue; }
          if ( ! is_assigned_to(nd,from) ) continue;
          if ( nd->ntype != NT_GFO_Load ) continue;
          rm_stack += nd;
        }
      assert( found_cand );
    }

  // Find additional nodes to remove from FROM node.
  //
  while ( Node* const nd = rm_stack.pop() )
    {
      nd->marker_set_mp(marker_mp);
      if ( nd->examined_from ) continue;
      nd->examined_from = true;
      Mapping_Node* const md = mapping_node_get(nd);
      const bool produces_here = md->ik_needed && md->ik_kno == from;
      if ( nd->is_sink() ) num_sinks++;

      mp->k_remove_insert(nd,from);

      if ( !is_assigned_to(nd,to) )
        {
          Examined_To eto = ET_False;

          if ( upstream )
            {
              if ( has_dests_at(nd,to) || nd->is_sink() ) eto = ET_Added;
              else
                {
                  A_Vector_Type cv = nd_consumers_vector_get(mp,nd);
                  assert( cv );
                  const int c_1st = ctz(cv);
                  const int a_1st = ik_kno_update(nd);
                  if ( a_1st > to && ( a_1st < c_1st || c_1st == from ) )
                    eto = ET_Added;
                }
            }
          else
            {
              A_Vector_Type av = assignment_vector_get(nd) & vec_un_from;
              const int alt_ik_kno = ctz(av);

              if ( produces_here && av && alt_ik_kno <= to
                   && !has_dests_at(nd,from) && !has_dests_at(nd,to) )
                set_min(to_min,alt_ik_kno);

              eto = ET_Added;
            }

          if ( eto == ET_Added ) mp_add_plus(mp,nd);

        }
      if ( upstream ) break;
      if ( produces_here )
        {
          A_Vector_Type consumers = ik_consumers_vector_get(nd) & vec_un_from;
          if ( consumers )
            {
              const int first_consumer = ctz(consumers);
              assert( first_consumer > from );
              set_min(to_min,first_consumer);
            }
        }
      for ( NIter dst(nd->dests); dst; )
          if ( is_assigned_to(dst,from) ) rm_stack += dst;
          else remove_descendants_av |= assignment_vector_get(dst);
    }

  if ( mp->immobile_stores && num_sinks ) return -1;

  // Check whether nodes are being moved to a place where their destinations
  // are not needed.
  //
  if ( !upstream && !num_sinks && ! ( vec_to & remove_descendants_av ) )
    {
      // We could use remove_descendants_av to suggest an alternative.
      return -1;
    }
  
  // Mark move infeasible if moving candidate would violate data dependencies
  // in kernels between from and to.
  //
  mp->feasible = to_min >= to;
  if ( !mp->feasible ) return -1;

  // Initialize ET_Components, used for computing change in estimated time.
  // Also prepare debugging lists.
  //
  for ( int kno=0; kno<num_kernels; kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->should_run && kno != to ) continue;
      ET_Components& etc = mp->etc[kno];
      if ( etc.mapping_serial != ki->etc.mapping_serial )
        etc = ki->etc;
      etc.modified = false;
      if ( !debug ) continue;
      etc.ik_lds_pre.clear(); etc.ik_lds_add.clear(); etc.ik_lds_sub.clear();
      etc.ik_sts_pre.clear(); etc.ik_sts_add.clear(); etc.ik_sts_sub.clear();
      const int marker = c->next_marker++;
      for ( NIter nd(ki->nodes); nd; )
        {
          Mapping_Node* const md = mapping_node_get(nd);

          if ( md->ik_active() && md->ik_kno == kno ) etc.ik_sts_pre += nd;

          for ( NIter src(nd->sources); src; )
            if ( !is_assigned_to(src,kno) && !src->marker_set(marker) )
              {
                Mapping_Node* const mrc = mapping_node_get(src);
                if ( mrc->ik_active() ) etc.ik_lds_pre += src;
              }
        }
    }

  // Find reduction nodes that might be affected by the proposal
  // and call reduction_check to make any necessary modifications
  // to the proposal.
  //
  ENodes ik_check(NLO_ik_check);
  ENodes red_check(NLO_red_check);

  for ( NIter nd(mp->nds_affected); nd; )
    {
      if ( nd->reducible ) red_check += nd;
      for ( NIter dst(nd->dests); dst; ) if ( dst->reducible ) red_check += dst;
    }

  for ( NIter nd(red_check); nd; )
    {
      for ( NIter src(nd->sources); src; )
        {
          ik_update(mp,src);
          src->marker_set_mp(marker_mp);
          if ( ! src->examined_to && ! src->remove_vector )
            ik_check += src;
        }
      reduction_check(mp,nd);
    }


  // Find initial set of changes in ik producer status.
  {
    Nodes nds = mp->nds_affected;
    for ( NIter nd(mp->nds_affected); nd; ) nds += nd->dests;
    for ( NIter nd(to_add); nd; ) nds += nd->sources;

    for ( NIter nd(nds); nd; )
      {
        if ( !nd->marker_check_mp(marker_mp) ) continue;
        if ( nd->dests_here_kno != Node::KNO_OVER ) continue;

        Mapping_Node* const md = mapping_node_get(nd);

        // Compute consumers vector based on initial add/remove set;
        // it can be updated further during DCE.
        //
        ik_consumers_vector_get(mp,nd);

        // Avoid re-execution of this code for duplicates in nds.
        nd->dests_here_kno--;

        // Compute change in ik status.
        //
        nd->delta_ik = bool(nd->ik_mp_consumers_vector) - bool(md->ik_needed);

        if ( !md->ik_needed ) ik_kno_update(nd);

        if ( nd->delta_ik < 0 )
          {
            mp->ik_remove += nd;
            if ( !has_dests_at(mp,nd,md->ik_kno) )
              mp->k_remove_insert_maybe(nd,md->ik_kno);
          }
        else if ( nd->delta_ik == 0 && md->ik_needed )
          {
            // Check for change in ik_kno.

            A_Vector_Type av_so_far = assignment_vector_get(mp,nd);
            assert(av_so_far);
            const int kno_after = ctz(av_so_far);
            if ( md->ik_kno == kno_after ) continue;

            mp->ik_update += nd;
            if ( !has_dests_at(mp,nd,md->ik_kno) )
              mp->k_remove_insert_maybe(nd,md->ik_kno);
          }
      }
  }

  //
  // Perform DCE starting at from.
  //
  for ( int kno = from; kno >= 0; kno-- )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      const A_Vector_Type k_bit = A_Vector_Type(1) << kno;

      if ( !ki->should_run )
        {
          assert( mp->k_remove[kno].size() == 0 );
          continue;
        }

      Nodes dce_stack = mp->k_remove[kno];

      while ( Node* const nd = dce_stack.pop() )
        {
          // Add node to list of nodes to be removed from kernel kno ..
          // .. if it's our first time here.
          //
          if ( ! ( nd->remove_vector & k_bit ) ) mp->k_remove_insert(nd,kno); 

          assert( nd->marker_check_mp(marker_mp) );
          assert( kno != mp->to || nd->examined_to != ET_Added );
          assert( nd->dce_kno != kno );
          nd->dce_kno = kno;

          // Check if sources need to be removed too.
          //
          for ( NIter src(nd->sources); src; )
            {
              Mapping_Node* const mrc = mapping_node_get(src);
              src->marker_set_mp(marker_mp);

              // Decrement number of locally consumed destinations ..
              // .. and add to remove list if it reaches zero.
              //

              // Initialize number of locally consumed and ik destinations.
              //
              if ( src->dests_here_kno != kno )
                {
                  if ( src->dests_here_kno == Node::KNO_OVER && mrc->ik_needed )
                    ik_consumers_vector_get(mp,src);

                  src->dests_here = num_dests_at(src,kno);
                  src->dests_here_kno = kno;
                }

              assert( src->dests_here > 0 );
              src->dests_here--;

              if ( src->dests_here ) continue;

              // At this point nothing on kno needs src node.

              if ( is_assigned_to(src,kno) )
                {
                  // Don't remove src if it is or will be an ik producer here.
                  //
                  if ( mrc->ik_kno == kno
                       && ! ( src->examined_to == ET_Added && to < kno ) )
                    {
                      A_Vector_Type cv = nd_consumers_vector_get(mp,src);
                      A_Vector_Type av = assignment_vector_get(mp,src);
                      A_Vector_Type kbit = A_Vector_Type(1) << kno;
                      A_Vector_Type nav = av & ~kbit;
                      if ( !nav ) continue;
                      const int first_c = ctz(cv);
                      const int first_p = ctz(nav);
                      if ( first_c < first_p )
                        continue;

                      if ( mrc->ik_needed ) mp->ik_update += src;

                    }
                  if ( ! ( src->remove_vector & k_bit ) )
                    dce_stack += src;
                  if ( !mrc->ik_needed && src->delta_ik < 1 ) continue;
                }
              else { assert( mrc->ik_needed ); }

              ik_consumers_vector_get(mp,src);

              if ( src->ik_mp_consumers_vector ) continue;

              // There are no more consumers.

              if ( src->delta_ik < 0 ) continue;

              if ( mrc->ik_needed )
                {
                  mp->ik_remove += src;
                  src->delta_ik = -1;
                }
              else
                {
                  src->delta_ik = 0;
                  continue;
                }

              // At this point src is no longer an ik producer, meaning
              // it does not have to write global memory (at mrc->ik_kno).
              // If nothing at mrc->ik_kno uses src result, then add
              // src to the remove list for mrc->ik_kno.
              //
              int dests_there = 0;
              for ( NIter dst(src->dests); dst; )
                if ( is_assigned_to(dst,mrc->ik_kno)
                     || ( mrc->ik_kno == to 
                          && dst->marker_check_mp(marker_mp)
                          && dst->examined_to == ET_Added ) )
                  dests_there++;
              if ( dests_there ) continue;
              mp->k_remove_insert_maybe(src,mrc->ik_kno);
            }
        }
    }

  //
  // Okay, so now that we've figured out exactly what needs to be
  // changed, we can now compute the change in cost.
  //

  double delta_et_total = 0;

  Nodes ik_look = mp->nds_affected + mp->ik_remove + ik_check;
  for ( NIter nd(mp->nds_affected); nd; ) ik_look += nd->sources;

  const int marker_ik_look = c->next_marker++;

  Nodes nds_rupdate;
  Nodes nds_ik_update;

  for ( NIter nd(ik_look); nd; )
    {
      if ( nd->marker_set(marker_ik_look) ) continue;
      if ( !nd->marker_check_mp(marker_mp) ) continue;
      if ( nd->dests == 0 ) continue;
      nds_ik_update += nd;
      Mapping_Node* const mpd = mapping_node_get(mp,nd);
      A_Vector_Type cv_before = ik_consumers_vector_get(nd);
      A_Vector_Type cv_after = ik_consumers_vector_get(mp,nd);
      assert( nd->delta_ik == bool(cv_after) - bool(cv_before) );
      mpd->ik_needed = cv_after ? IK_simple : IK_none;
      mpd->ik_kno = num_kernels;  // Reassigned below.
      if ( !cv_before && !cv_after ) continue;
      A_Vector_Type av_after = assignment_vector_get(mp,nd);
      const int ik_kno_after = cv_after ? ctz(av_after) : num_kernels;
      mpd->ik_kno = ik_kno_after;

      if ( nd->delta_ik > 0 ) mp->ik_add += nd;
    }

  const int marker_ru = c->next_marker++;
  for ( NIter nd(nds_ik_update); nd; )
    {
      for ( NIter dst(nd->dests); dst; )
        if ( !dst->marker_set(marker_ru) && dst->reducible )
          reduction_update(mp,dst);
    }

  for ( NIter nd(nds_ik_update); nd; )
    {
      Mapping_Node* const md = mapping_node_get(nd);
      Mapping_Node* const mpd = mapping_node_get(mp,nd);
      if ( !md->ik_needed && !mpd->ik_needed ) continue;

      const int ik_kno_before = md->ik_kno;
      const int ik_kno_after = mpd->ik_kno;

      A_Vector_Type cv_before = ik_consumers_vector_get(nd);
      A_Vector_Type cv_after = ik_consumers_vector_get(mp,nd);
      A_Vector_Type new_ik =  cv_after & ~cv_before;
      A_Vector_Type rem_ik = ~cv_after &  cv_before;
      A_Vector_Type same_ik = cv_after &  cv_before;
      const int delta = bool(mpd->ik_active()) - bool(md->ik_active());
      const int delta_fl = ( mpd->ik_needed == IK_follower )
        - ( md->ik_needed == IK_follower );

      if ( ik_kno_before != ik_kno_after )
        {
          if ( md->ik_active() )
            {
              assert( mp->etc[md->ik_kno].ik_st > 0 );
              mp->etc[md->ik_kno].ik_st--;
              if ( debug ) mp->etc[md->ik_kno].ik_sts_sub += nd;
              mp->etc[md->ik_kno].modified = true;
            }
          if ( md->ik_needed == IK_follower )
            {
              mp->etc[md->ik_kno].issue_usage -= nd->dests[0]->issue_usage;
              mp->etc[md->ik_kno].ops--;
            }

          if ( mpd->ik_active() )
            {
              mp->etc[ik_kno_after].ik_st++;
              if ( debug ) mp->etc[ik_kno_after].ik_sts_add += nd;
              mp->etc[ik_kno_after].modified = true;
            }
          if ( mpd->ik_needed == IK_follower )
            {
              mp->etc[ik_kno_after].issue_usage += nd->dests[0]->issue_usage;
              mp->etc[ik_kno_after].ops++;
            }
        }
      else
        {
          if ( delta )
            {
              mp->etc[md->ik_kno].ik_st += delta;
              if ( debug )
                {
                  if ( delta < 0 ) mp->etc[md->ik_kno].ik_sts_sub += nd;
                  else             mp->etc[md->ik_kno].ik_sts_add += nd;
                }
              mp->etc[md->ik_kno].modified = true;
            }
          if ( delta_fl )
            {
              mp->etc[md->ik_kno].issue_usage +=
                delta_fl * nd->dests[0]->issue_usage;
              mp->etc[md->ik_kno].ops += delta_fl;
            }
        }

      if ( mpd->ik_active() )
        for ( KNOIter kno(new_ik); kno; )
          {
            mp->etc[kno].ik_ld++;
            mp->etc[kno].modified = true;
            if ( debug ) mp->etc[kno].ik_lds_add += nd;
          }

      if ( md->ik_active() )
        for ( KNOIter kno(rem_ik); kno; )
          {
            assert( mp->etc[kno].ik_ld > 0 );
            if ( debug ) mp->etc[kno].ik_lds_sub += nd;
            mp->etc[kno].ik_ld--;
            mp->etc[kno].modified = true;
          }

      if ( delta )
        for ( KNOIter kno(same_ik); kno; )
          {
            mp->etc[kno].ik_ld += delta;
            mp->etc[kno].modified = true;
            if ( debug )
              {
                if ( delta < 0 ) mp->etc[kno].ik_lds_sub += nd;
                else             mp->etc[kno].ik_lds_add += nd;
              }
          }
    }

  if ( to_add ) mp->etc[mp->to].modified = true;

  for ( NIter nd(mp->red_change); nd; )
    {
      Mapping_Node* const md = mapping_node_get(nd);
      Mapping_Node* const mpd = mapping_node_get(mp,nd);
      nd->red_fp_update_marker_mp = mp->marker_mp;
      nd->red_fp_update = 0;
      if ( md->red_root_srcs == mpd->red_root_srcs
           && md->red_kno == mpd->red_kno ) continue;
      nd->red_fp_update = AV(md->red_kno) | AV(mpd->red_kno);

      const int delta_insn_fr =
        - ( md->red_root_srcs - 1 )
        + ( is_assigned_to(mp,nd,md->red_kno) ? nd->ops : 0 );

      mp->etc[md->red_kno].issue_usage += delta_insn_fr * nd->issue_usage;
      mp->etc[md->red_kno].ops += delta_insn_fr;
      assert( mp->etc[md->red_kno].issue_usage >= ZLO );
      assert( mp->etc[md->red_kno].ops >= 0 );
      mp->etc[md->red_kno].modified = true;

      const int delta_insn_to =
        mpd->red_root_srcs - 1
        - ( is_assigned_to(nd,mpd->red_kno) ? nd->ops : 0 );

      mp->etc[mpd->red_kno].issue_usage += delta_insn_to * nd->issue_usage;
      mp->etc[mpd->red_kno].ops += delta_insn_to;
      mp->etc[mpd->red_kno].modified = true;
    }

  // Update operation counts and et.
  //
  for ( int kno=0; kno<num_kernels; kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->should_run && kno != mp->to ) continue;

      ET_Components& etc = mp->etc[kno];
      if ( mp->k_remove[kno] ) etc.modified = true;
      if ( !etc.modified ) continue;
      etc.mapping_serial = mapping_serial;

      const int marker = c->next_marker++;

      for ( NIter nd(mp->k_remove[kno]); nd; )
        {
          if ( nd->compile_time_constant ) continue;
          Mapping_Node* const md = mapping_node_get(nd);
          Mapping_Node* const mpd = mapping_node_get(mp,nd);
          if ( !mp->red_change.member(nd) ||
               ( md->red_kno != kno && mpd->red_kno != kno ) )
            {
              assert( nd->red_fp_update_marker_mp != marker_mp
                      || !( nd->red_fp_update & AV(kno) ) );
              etc.ops -= nd->ops;
              etc.issue_usage -= nd->ops * nd->issue_usage;
            }
          else
            assert( nd->red_fp_update_marker_mp == marker_mp
                    && nd->red_fp_update & AV(kno) );

          const bool been_here = nd->marker_set(marker);
          assert( !been_here );
          if ( nd->mem_accesses == 0 ) continue;
          if ( nd->mem_accesses == 1 && nd->ntype != NT_GFO_Load )
            {
              if ( nd->ntype == NT_GF_Store )
                etc.simple_st--; else etc.simple_ld--;
              continue;
            }
          etc.stencil_ld--;
        }

      if ( kno == mp->to )
        for ( NIter nd(to_add); nd; )
          {
            if ( nd->compile_time_constant ) continue;
            Mapping_Node* const mpred = mapping_node_get(nd);
            Mapping_Node* const md = mapping_node_get(mp,nd);
            if ( !mp->red_change.member(nd) ||
                 ( mpred->red_kno != kno && md->red_kno != kno ) )
              {
                assert( nd->red_fp_update_marker_mp != marker_mp
                        || !( nd->red_fp_update & AV(kno) ) );
                etc.ops += nd->ops;
                etc.issue_usage += nd->ops * nd->issue_usage;
              }
            else
              assert( nd->red_fp_update_marker_mp == marker_mp
                      && nd->red_fp_update & AV(kno) );

            const bool been_here = nd->marker_set(marker);
            assert( !been_here );
            if ( nd->mem_accesses == 0 ) continue;
            if ( nd->mem_accesses == 1 && nd->ntype != NT_GFO_Load )
              {
                if ( nd->ntype == NT_GF_Store )
                  etc.simple_st++; else etc.simple_ld++;
                continue;
              }
            etc.stencil_ld++;
          }

      if ( fabs( etc.issue_usage ) < 1e-6 ) etc.issue_usage = 0;

      assert( etc.issue_usage >= 0 && etc.ops >= 0 && etc.simple_ld >= 0 );
      assert( etc.stencil_ld >= 0 );

      if ( etc.stencil_ld == 0 ) etc.stencil_reset();
      else                       et_gf_update(mp,&etc,kno);

      assert( !etc.stencil_ld == !etc.unique_stencil_ld );

      et_tile_update(&etc,kno);
      const double delta_et = etc.et_compute() - ki->etc.et;

      assert( mp->k_remove[kno] || kno == mp->to && to_add
              || delta_et == 0
              || ki->etc.ik_st != etc.ik_st
              || ki->etc.ik_ld != etc.ik_ld
              || mp->red_change );

      delta_et_total += delta_et;
    }

  mp->score = delta_et_total;

  return mp->score;
}

ET_Components&
ET_Components::operator =(ET_Components etc)
{
  char buf[sizeof(etc)];
  memcpy(buf,(char*)&etc,sizeof(etc));
  memcpy((char*)&etc,(char*)this,sizeof(etc));
  memcpy((char*)this,buf,sizeof(etc));
  return *this;
}

ET_Components::ET_Components(const ET_Components& etc)
{
  m = etc.m;
  const ptrdiff_t size = ((char*)&this[1]) - ((char*)&et);
  memcpy(&this->et,&etc.et,size);
  mapping_serial = etc.mapping_serial;
  modified = etc.modified;
  ik_lds_pre.clear(); ik_lds_sub.clear(); ik_lds_add.clear();
  ik_sts_pre.clear(); ik_sts_sub.clear(); ik_sts_add.clear();
  buffer_group_map = etc.buffer_group_map;
  buffer_groups.clear();
  for ( Buffer_Group_Map::iterator bi = buffer_group_map.begin();
        bi != buffer_group_map.end(); ++ bi )
    {
      Buffer_Group* const bg = &bi->second;
      assert( bg->inited );
      buffer_groups += bg;
    }
}

void
ET_Components::init(Chemora_CG_Calc_Mapping* mp)
{
  m = mp;
  const ptrdiff_t size = ((char*)&this[1]) - ((char*)&et);
  memset(&this->et,0,size);
  mapping_serial = mp->mapping_serial;
  ik_lds_pre.clear(); ik_lds_sub.clear(); ik_lds_add.clear();
  ik_sts_pre.clear(); ik_sts_sub.clear(); ik_sts_add.clear();
  buffer_group_map.clear();
  buffer_groups.clear();
}

double
ET_Components::et_compute()
{
  // Compute execution time per grid point per MP.

  CaCUDALib_GPU_Info& gi = *m->c->gpu_info;
  CaCUDA_Kernel_Launch_Parameters& klp = m->c->kernel_launch_parameters;

  const bool assume_dp = sizeof(CCTK_REAL) == 8;
  const int assumed_elt_size = assume_dp ? 8 : 4;

  const int kernel_launch_overhead_ns = 10000;
  const int mem_lat = gi.gl_mem_lat_cyc.val;
  FP_Type default_op = FP_MADD;
  const int op_lat =
    ( assume_dp ? gi.dp_fp_op_lat_cyc : gi.sp_fp_op_lat_cyc ) [default_op].val;

  const int num_mp = gi.num_multiprocessors;
  const double thpt_data_p_chip_Bps = gi.chip_bw_Bps;
  const double clock_freq_hz = gi.clock_freq_hz;

  const int num_gp = klp.i_length_x * klp.i_length_y * klp.i_length_z;
  const int gp_per_mp = num_gp / num_mp;
  const int num_threads = tile.x * tile.y * tile.z;
  const int num_threads_per_mp = num_threads * blocks_per_mp;
  const u_int32_t num_wp = ( num_threads + warp_sz - 1 ) >> warp_lg;

  const bool empty = (issue_usage>0) + ops + ik_st == 0;

  // Number of elements loaded or stored per grid point.
  //
  amt_elts = ik_ld + ik_st + simple_ld + simple_st
    + gl_ld_pt + buf_ls_pt + ro_fills_pt;
  const double amt_elts_lim =
    simple_ld + simple_st + gl_ld_pt + buf_ls_pt + ro_unique_pt;

  const int sh_ops = sh_ld_pt + buf_ls_pt;

  // Number of additional instructions needed for each RO cache
  // load. These include address arithmetic and texture barrier
  // instructions.
  const int ro_insn_extra = 2 * ro_ld_pt;

  amt_insn = ops + sh_ops + over_regs + ro_insn_extra;

  // Number of instructions from producer to consumer.
  const int lambda = min(3, max(1,ops / 2) );

  int max_outstanding_lds = 8;

  // Crude estimate of exposed latency due to loads from global memory.
  // The first term models a consumer instruction blocking because
  // one of its sources, a register written by a load instruction,
  // is not yet ready. The second term models blocking due to some
  // kind of limit on the maximum number of outstanding loads per
  // thread having been reached.
  const double chained_lds_raw =
    double( over_regs + ik_ld + ik_st + ro_fills_pt + simple_ld )
    / lambda +
    double( ik_ld + gl_ld_pt + buf_ls_pt + simple_ld + ro_fills_pt )
    / max_outstanding_lds;
  chained_lds = chained_lds_raw ? max(1.0,chained_lds_raw) : 0.0;


  // Interpolate barrier latency.
  const int sync_sz = sizeof(gi.sh_mem_sync_cyc)/sizeof(gi.sh_mem_sync_cyc[0]);
  const int num_wp_lg_lo = fl1(num_wp) - 1;
  const int num_wp_lo = 1 << num_wp_lg_lo;
  const int num_wp_lg_hi = num_wp_lg_lo + 1;
  const double sh_ml_lo = gi.sh_mem_sync_cyc[num_wp_lg_lo].val;
  const double sh_ml_hi = gi.sh_mem_sync_cyc[min(sync_sz-1,num_wp_lg_hi)].val;
  const double sh_ml_slope = ( sh_ml_hi - sh_ml_lo ) / num_wp_lo;
  const double sh_ml = sh_ml_lo + sh_ml_slope * ( num_wp - num_wp_lo );
  assert( num_wp_lg_lo >= 0 && num_wp_lg_lo < sync_sz );

  const double bar_lat = ( sh_ops ? 2 : 1 ) * sh_ml;

  // Latency per grid point, assuming grid point computations within a
  // thread don't overlap.
  latency = empty ? 0 :
    chained_lds * mem_lat + amt_insn / lambda * op_lat
    + bar_lat + double(sh_ops) / lambda * gi.sh_mem_lat_cyc[1].val;

  const double ts_launch_overhead =
    empty ? 0
    : ( kernel_launch_overhead_ns * 1e-9 * clock_freq_hz ) / gp_per_mp;

  // Compute throughput in elements per cycle per MP.
  const double thpt_data_p_mp = 
    thpt_data_p_chip_Bps / ( assumed_elt_size * num_mp * clock_freq_hz );

  const double thpt_insn_ls_sh = gi.dev_cap.fu_ls;

  // Global load and stores.
  gls_insn = ik_ld + ik_st + simple_ld + simple_st + buf_ls_pt + gl_ld_pt;

  // Per grid point.

  ts_insn_lim_fp = issue_usage;

  ts_insn =
    ( issue_usage + ( sh_ops + gls_insn + over_regs ) / thpt_insn_ls_sh )
    * m->c->opt_issue_per_exec;

  ts_data_lim = amt_elts_lim / thpt_data_p_mp;

  ts_f_penalty = f_halo * ( blocks_per_mp - 1 )
    / ( num_threads_per_mp * thpt_data_p_mp );

  ts_load_latency = gi.gl_mem_lat_cyc.val / num_threads_per_mp;

  ts_data = amt_elts / thpt_data_p_mp;
  ts_latency = double(latency) / num_threads_per_mp;

  ts_insn_ut = ts_insn / utilization_slots;
  ts_latency_ut = ts_latency / utilization_thds;

  const double max_factor = max( max( ts_insn_ut, ts_data ), ts_latency_ut );

  et = ts_launch_overhead +
    ( ( ts_insn_ut + ts_data + ts_latency_ut - max_factor ) * 0.35
      + max_factor )
    / load_balance;

  assert( et == 0 == empty );

  return et;
}

void
Chemora_CG_Calc_Mapping::klp_update(int kno)
{
  Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
  Function_Info* const fi = ki->fi;
  CaKernel_Kernel_Config* const kc = &c->mi->kc;

  const int x_length = kc->i_length[0];
  const int y_length = kc->i_length[1];
  const int z_length = kc->i_length[2];

  Tile& tile = ki->etc.tile;

  const int tile_yyy = tile.y * tile.yy;
  const int tile_zzz = tile.z * tile.zz;

  fi->klp.tile_x = tile.x;
  fi->klp.tile_y = tile.y;
  fi->klp.tile_z = tile.z;
  fi->klp.tile_yy = tile.yy;
  fi->klp.tile_zz = tile.zz;
  fi->klp.cagh_blocky =  iDivUp(y_length, tile_yyy);
  fi->klp.block_size = tile.x * tile.y * tile.z;
  fi->klp.force_one_block_per_mp = c->opt_force_one_block_per_mp;

  fi->lc.grid_x = iDivUp( x_length, tile.x );
  fi->lc.grid_y = iDivUp( z_length, tile_zzz ) * fi->klp.cagh_blocky;
  fi->lc.rationale = "klp_update.";
}

void
Chemora_CG_Calc_Mapping::move_accept(Move_Proposal *mp)
{
  // Modify this mapping based on mp.

  const bool debug = false;
  if ( debug ) assignment_vector_prev = assignment_vector;

  // Remove nodes from kernel from, and from kernels that no longer
  // need to compute inter-kernel values.
  //
  for ( int kno=0; kno<num_kernels; kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      const int rsize = mp->k_remove[kno].size();
      assert( !bool(rsize) || ki->should_run );
      if ( !ki->should_run )
        {
          if ( kno != mp->to ) continue;
          if ( mp->to_add.size() == 0 ) continue;
          ki->should_run = true;
        }
      if ( !mp->etc[kno].modified )
        {
          assert( mp->etc[kno].mapping_serial == ki->etc.mapping_serial );
          continue;
        }
      ki->etc = mp->etc[kno];
      klp_update(kno);
      if ( !rsize ) continue;
      const int marker = c->next_marker++;
      for ( NIter nd(mp->k_remove[kno]); nd; )
        {
          assert( is_assigned_to(nd,kno) );
          assert( !nd->marker_set(marker) );
        }
      assert( rsize <= ki->nodes.size() );
      if ( rsize == ki->nodes.size() ) ki->should_run = false;
      unassign_from(mp->k_remove[kno],kno);
    }

  for ( NIter nd(mp->to_add); nd; ) assign_to(nd,mp->to);

  accept_mp_serial = mp->serial;

  // Perform sanity checks on ik_FOO lists.
  //
  for ( NIter nd(mp->ik_remove); nd; ) assert(mapping_node_get(nd)->ik_needed);
  for ( NIter nd(mp->ik_add); nd; ) assert(!mapping_node_get(nd)->ik_needed);

  // Update ik nodes that are expected to change.
  //
  for ( NIter nd(mp->ik_add); nd; )
    {
      const bool is_prod = ik_update(nd);
      assert( is_prod );
    }
  for ( NIter nd(mp->ik_remove); nd; )
    {
      const bool is_prod = ik_update(nd);
      assert( !is_prod );
    }
  for ( NIter nd(mp->ik_update); nd; ) ik_update(nd);

  reduction_update();

  t_scaled_approx += mp->score;
  t_scaled_num_approx++;

  verify(mp);
}

double
Chemora_CG_Calc_Mapping::compute_cost(Move_Proposal *mp = NULL)
{
  t_scaled = 0;

  mapping_serial++;

  for ( int kno = 0;  kno < num_kernels;  kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->should_run ) continue;

      const int marker = c->next_marker++;

      ki->etc.init(this);

      for ( NIter nd(ki->nodes); nd; )
        {
          if ( nd->compile_time_constant ) continue;
          Mapping_Node* const md = mapping_node_get(nd);
          if ( nd->reducible && md->red_kno == kno )
            {
              const int num_insn = md->red_root_srcs - 1;
              ki->etc.ops += num_insn;
              ki->etc.issue_usage += num_insn * nd->issue_usage;
            }
          else
            {
              ki->etc.ops += nd->ops;
              ki->etc.issue_usage += nd->ops * nd->issue_usage;
            }

          if ( md->ik_kno == kno )
            {
              if ( md->ik_active() ) ki->etc.ik_st++;
              if ( md->ik_needed == IK_follower )
                {
                  ki->etc.ops++;
                  ki->etc.issue_usage += nd->dests[0]->issue_usage;
                }
            }

          switch ( nd->ntype ){
          case NT_Special:
          case NT_Constant_Expr: case NT_Parameter:
            break;
          case NT_GF_Store:
            ki->etc.simple_st++;
            // Fall through.
          case NT_Switch: case NT_If:
          case NT_Expression:
            for ( NIter src(nd->sources); src; )
              if ( !is_assigned_to(src,kno)
                   && mapping_node_get(src)->ik_active()
                   && !src->marker_set(marker) )
                ki->etc.ik_ld++;
            break;
          case NT_GFO_Load:
            // Note: stencil_ld and unique_stencil_ld computed in
            // et_gf_update.
            break;

          default:
            assert( false );
          }
        }

      et_gf_update(&ki->etc,kno);
      et_tile_update(&ki->etc,kno);

      assert( !ki->etc.stencil_ld == !ki->etc.unique_stencil_ld );

      t_scaled += ki->etc.et_compute();
      klp_update(kno);
    }

  buffer_style_table_key_make();

  pString report;
  double ts_insn_tot = 0, ts_data_tot = 0, ts_latency_tot = 0;
  double amt_elts_tot = 0;
  int ik_st_tot = 0, ik_ld_tot = 0;
  int ops_tot = 0;
  int live_k = 0;
  double issue_usage_tot = 0;
  double et_err_tot = 0;
  for ( int kno = 0;  kno < num_kernels;  kno++ )
    {
      if ( kno == 0 )
        report.sprintf
          (" %2s: %9s %5s %1s  %4s/%4s %3s %6s  %5s %5s %5s"
           "  %5s  %2s %4s\n",
           "", "Tile", "Iter", "B",  "IS", "Insn", "IK", "Elts", 
           "Issue", "Data", "Lat",
           "ET", "OV", "H/I");
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->should_run ) continue;

      ET_Components* const etc = &ki->etc;
      Tile& tile = etc->tile;

      live_k++;

#define T(m) m##_tot += etc->m;
      T(ts_insn); T(ts_data); T(ts_latency); T(amt_elts);
      T(issue_usage); T(ops); T(ik_st); T(ik_ld);
#undef T

      const double err = mp && kno <= max(mp->from,mp->to) 
        ? etc->et - mp->etc[kno].et : 0;
      assert( fabs(err) < 0.0001 );
      et_err_tot += fabs(err);
  
      report.sprintf
        (" %2d: %3d,%2d,%2d %3d %1s %1c  %4.1f/%4d %3d %6.1f  "
         "%5.1f %5.1f %5.1f  "
         "%5.1f%1s %2d %4.2f\n",
         kno,
         tile.x, tile.y, tile.z, 
         max(tile.yy, tile.zz), tile.yy > tile.zz ? "y" : "z",
         etc->style_label,
         etc->issue_usage, etc->ops,
         etc->ik_st + etc->ik_ld, etc->amt_elts,
         etc->ts_insn, etc->ts_data, etc->ts_latency,
         etc->et,  fabs( err ) > 0.0001 ? "E" : " ",
         etc->over_regs,
         ( etc->gl_ld_pt + etc->buf_ls_pt ) / max(1,etc->unique_stencil_ld)
         );
    }

  const double approximation_error =
    t_scaled_num_approx ? t_scaled - t_scaled_approx : 0;

  report.sprintf
    (" Total of %2d  (%5d)   %4.1f/%4d %3d %6.1f  %5.1f %5.1f %5.1f  "
     "%5.1f\n",
     live_k, t_scaled_num_approx,
     issue_usage_tot, ops_tot,
     ik_st_tot + ik_ld_tot, amt_elts_tot,
     ts_insn_tot, ts_data_tot, ts_latency_tot, t_scaled);
  if ( table_style_key.size() ) report += table_style_key;

  c->mi->dm->msg_tune
    ("Chemora Code Generation Report for %s c%d\n%s",
     c->calc_name.c_str(), config_version, report.s);

  assert( fabs(approximation_error) < 0.001 );
  assert( et_err_tot < 0.001 );

  t_scaled_num_approx = 0;
  t_scaled_approx = t_scaled;

  return t_scaled;
}

string
Chemora_CG_Calc_Mapping::make_ik_name(Node *nd)
{
  Mapping_Node* const md = mapping_node_get(nd);
  switch ( md->ik_needed ) {
  case IK_leader:
    return string
      ( pstringf("%s_ik_reduction_%d",
                 nd->lhs_name_dis.c_str(), md->ik_ldr_pos) );
    // Fall Through.
  case IK_follower: case IK_simple:
    return nd->lhs_name_dis + "_interkernel";
  default:
    assert(false);
  }
  return string("This can't happen.");
}

string
Chemora_CG_Calc_Mapping::ik_make(Node *nd)
{
  Mapping_Node* const md = mapping_node_get(nd);
  assert( md->ik_needed );

  string ik_name = make_ik_name(nd);
  pStringF cmt(" // prod %d", md->ik_kno);

  nd->producer_c_code = 
    string("I3D_ik(") + ik_name + ") = " + nd->lhs_name + ";";
  nd->consumer_c_code = 
    nd->lhs_name + " = I3D_ik(" + ik_name + ");" + cmt.s;
  return ik_name;
}


void
Chemora_CG_Calc_Mapping::ptx_include(const char* path)
{
  assert( !ptx_in_body );
  ptx_linef("// Include from file %s",path);
  FILE* const inc = fopen(path,"r");
  assert( inc );
  while ( true )
    {
      int ch = getc(inc);
      if ( ch == EOF ) break;
      fputc(ch,emit_ptx_fh);
  }
  fclose(inc);
}

void
Chemora_CG_Calc_Mapping::ptx_line(const char* line)
{
  if ( strlen(line) == 0 )
    {
      if ( ptx_in_body ) ptx_body_items += "\n";
      else               fprintf(emit_ptx_fh, "\n");
      return;
    }

  const char* const spaces =
    "                                                  ";
  const int sp_len = strlen(spaces);
  const int idx = sp_len - ptx_indent;
  assert( idx >= 0 );
  assert( ptx_indent >= 0 );
  pStringF ind_line("%s%s\n",&spaces[idx],line);
  if ( ptx_in_body ) ptx_body_items += ind_line.s;
  else               fprintf(emit_ptx_fh,"%s",ind_line.s);
}

void
Chemora_CG_Calc_Mapping::ptx_lines(const char* line1, ...)
{
  va_list ap;
  if ( !line1 ) return;
  ptx_line(line1);
  va_start(ap,line1);
  while ( const char* const line = va_arg(ap,const char*) ) ptx_line(line);
  va_end(ap);
}

void
Chemora_CG_Calc_Mapping::ptx_line0f(const char* fmt, ...)
{
  va_list ap;
  pString line;
  va_start(ap,fmt);
  line.vsprintf(fmt,ap);
  va_end(ap);
  ptx_indent_push_abs(0);
  ptx_line(line.s);
  ptx_indent_pop();
}

void
Chemora_CG_Calc_Mapping::ptx_linef(const char* fmt, ...)
{
  va_list ap;
  pString line;
  va_start(ap,fmt);
  line.vsprintf(fmt,ap);
  va_end(ap);
  ptx_line(line.s);
}

void
Chemora_CG_Calc_Mapping::ptx_comment_line(const char* linep)
{
  char *line = strdup(linep);
  char *st = line;
  char *en = line;
  const int ptx_indent_prev = ptx_indent;
  ptx_indent = 0;
  const char eol = 10;
  while ( *st )
    {
      while ( *en && *en != eol ) en++;
      const char e0 = en[0];
      en[0] = 0;
      ptx_linef("// %s",st);
      if ( !e0 ) break;
      en = st = en + 1;
    }
  ptx_indent = ptx_indent_prev;
  free(line);
}

void
Chemora_CG_Calc_Mapping::ptx_indent_push(int amt)
{
  ptx_indent_stack += ptx_indent;
  ptx_indent += amt;
}

void
Chemora_CG_Calc_Mapping::ptx_indent_push_abs(int amt)
{
  ptx_indent_stack += ptx_indent;
  ptx_indent = amt;
}

void
Chemora_CG_Calc_Mapping::ptx_indent_pop()
{
  assert( !ptx_indent_stack.empty() );
  ptx_indent = ptx_indent_stack.pop();
}

void
Chemora_CG_Calc_Mapping::ptx_body_start()
{
  bzero(ptx_rr_next,sizeof(ptx_rr_next));
  ptx_body_items.clear();
  ptx_insns.clear();
  ptx_in_body = true;
  ptx_indent = 0;
}

void
Chemora_CG_Calc_Mapping::ptx_body_end()
{
  assert( ptx_emit_insns_pending == 0 );
  ptx_in_body = false;
  ptx_indent = 0;
  ptx_line("{");
  if ( !emit_clike )
  for ( int i=0; i<PT_SIZE; i++ )
    {
      if ( ptx_rr_next[i] == 0 ) continue;
      ptx_linef("\t.reg .%s\t%%%s<%d>;",
                ptx_rr_type[i], ptx_rr_str[i], ptx_rr_next[i] );
    }
  ptx_line("");
  for ( auto& l: ptx_body_items ) fprintf(emit_ptx_fh,"%s",l.c_str());
  ptx_line("}");
}


void
PTX_RR::reset()
{
 num = -1;
 global_num = -1;
 ctc = false;
 single_assign = true;
 ik_load = false;
 orig = NULL;
}

PTX_RR::PTX_RR(bool literal_val)
{
  num = 0;
  global_num = -1;
  orig = NULL;
  ctc = true;
  ctc_vali = literal_val;
  rt = PT_pred;
  txt = pstringf("%d",literal_val);
}

pair<int,int>
Chemora_CG_Calc_Mapping::code_path_sib_range_get(int code_path)
{
  if ( code_path == 1 || code_path == ek.code_path_1d )
    return {code_path,code_path+1};
  assert( code_path > 1 && code_path < ek.code_path_h2_nnodes );
  const int code_path_level = fl1(code_path);
  const int limit = 1 << code_path_level;
  return {limit>>1,limit};
}

int
Chemora_CG_Calc_Mapping::code_path_pool_idx_get(int code_path)
{
  if ( code_path == 1 || code_path == ek.code_path_1d ) return 1;
  return code_path;
}
PTX_Reg_Pool&
Chemora_CG_Calc_Mapping::code_path_pool_get(int code_path)
{
  return ek.code_path_reg_pool[code_path_pool_idx_get(code_path)];
}

PTX_RR
Chemora_CG_Calc_Mapping::ptx_make_reg(PTX_Reg_Type rt)
{
  auto& regs_shareable = ek.code_path_regs_shareable[rt][ek.code_path];

  if ( ek.code_path == 1 && ek.regs_shareable_code_path_last != 1 )
    {
      for ( auto& rs_rt: ek.code_path_regs_shareable )
        for ( auto& rs: rs_rt ) rs.clear();
    }
  ek.regs_shareable_code_path_last = ek.code_path;

  if ( regs_shareable.empty() )
    {
      PTX_RR* const reg = new PTX_RR;
      reg->global_num = ptx_all_regs.size();
      ptx_all_regs += reg;
      reg->orig = reg;
      reg->uses = 0;
      reg->num = ptx_rr_next[rt]++;
      reg->rt = rt;
      reg->txt = pstringf("%s%s%d",emit_clike?"":"%",ptx_rr_str[rt],reg->num);

      const pair<int,int> sibs_range = code_path_sib_range_get(ek.code_path);
      for ( int i=sibs_range.first; i<sibs_range.second; i++ )
        ek.code_path_regs_shareable[rt][i].push_back(reg);
    }

  assert( regs_shareable.size() );

  PTX_RR* const rv = regs_shareable.front();  regs_shareable.pop_front();
  assert( rv->rt == rt );
  return *rv;
}

#if 0
PTX_RR
Chemora_CG_Calc_Mapping::ptx_make_reg_ctc(PTX_Reg_Type rt, int val)
{
  PTX_RR reg;
  reg.num = 0;
  reg.rt = rt;
  reg.ctc = true;
  reg.txt = pstringf("%d",val);
  return reg;
}
PTX_RR
Chemora_CG_Calc_Mapping::ptx_make_reg_ctc(PTX_Reg_Type rt, double val)
{
  PTX_RR reg;
  reg.num = 0;
  reg.rt = rt;
  reg.ctc = true;
  reg.txt = pstringf("%.20e",val);
  return reg;
}
#endif

int
Chemora_CG_Calc_Mapping::ptx_make_gp_offset_global
(int axes, int di, int dj, int dk)
{
  int accum = 0;
  int offset[] = { 0, di, dj, 0, dk };

  for ( int a_vec = AX_z;  a_vec >= AX_x;  a_vec >>= 1 )
    if ( axes & a_vec )
      accum = accum * ek.axes_len_global[a_vec] + offset[a_vec];
  return accum;
}

PTX_RR_Addr_Base*
Chemora_CG_Calc_Mapping::ptx_addr_base_new
(PTX_RR& ptxr_thd_offset, int elt_size_bytes, Array_Axes_Enum axes)
{
  PTX_RR_Addr_Base* const ab = new PTX_RR_Addr_Base;
  ek.addr_bo_all += ab;
  ab->ptxr_thd_offset = ptxr_thd_offset;
  ab->offset_scale_factor = elt_size_bytes ?: ek.assumed_elt_size;
  ab->axes = axes;
  return ab;
}

void
PTX_Reg_Pool::reset(int pool_idxp, int max_livep, Chemora_CG_Calc_Mapping *mp)
{
  m = mp;
  pool_idx = pool_idxp;
  max_live = max_livep;
  rp_items.clear();
  seq_to_grno_rebuild();
  seq_num_next = 1;
  n_reg_defs = 0;
}

bool
PTX_Reg_Pool::check(const PTX_RR& r) const
{ return rp_items.find(r.global_num) != rp_items.end(); }
bool
PTX_Reg_Pool::check(const PTX_RR_Base_Extension& be) const
{ return check(be.reg); }
int
PTX_Reg_Pool::check_seq(int grno) const
{
  auto rpii = rp_items.find(grno);
  return rpii == rp_items.end() ? 0 : rpii->second.seq_num;
}

const PTX_RR_Base_Extension
PTX_Reg_Pool::use(const PTX_RR_Base_Extension& be)
{
  use(be.reg);
  return be;
}
void
PTX_Reg_Pool::use(const PTX_RR& r)
{
  // Update the register pool based on use of r.

  auto rpii = rp_items.find(r.global_num);
  assert( rpii != rp_items.end() );
  PTX_Reg_Pool_Item& rpi = rpii->second;
  assert( rpi.grno == r.global_num );
  const bool ptx_reg_pool_comments = false;

  int n = 0;

  if ( m->c->opt_addr_regs_opt )
    {
      if ( ptx_reg_pool_comments )
        for ( auto i=seq_to_grno.rbegin(); i->second != r.global_num; i++,n++);

      // Find register r and update hopovers_remaining of registers
      // that have been used between now and the last use of r.
      //
      auto sni = seq_to_grno.rbegin();
      while ( sni->second != r.global_num &&
              --rp_items[sni->second].hopovers_remaining > 0 ) sni++;

      // The hopovers_remaining count of register sni reached zero, meaning
      // that here were max_live registers live when sni last accessed.
      // Therefore registers older than sni must be removed, otherwise
      // there could be > max_live regs when sni last accessed.
      //
      if ( sni->second != r.global_num )
        {
          while ( ++sni != seq_to_grno.rend() )
            if ( sni->second != r.global_num ) rp_items.erase(sni->second);
          seq_to_grno_rebuild();
        }
    }
  seq_to_grno.erase(rpi.seq_num);
  rpi.seq_num = seq_num_next++;
  rpi.hopovers_remaining = max_live - 1;
  seq_to_grno[rpi.seq_num] = rpi.grno;

  if ( m->c->opt_addr_regs_opt && ptx_reg_pool_comments )
    {
      vector<PTX_Reg_Pool_Item> rpd;
      for ( auto i = seq_to_grno.rbegin(); i != seq_to_grno.rend(); i++ )
        rpd.push_back(rp_items[i->second]);
      for ( auto ri: rp_items ) assert( ri.second.hopovers_remaining >= 0 );

      if ( n >= 1 )
        {
          m->ptx_linef("// pool, using llru reg %%rd%d",r.num);
          for ( auto ri: rpd )
            m->ptx_linef
              ("//    %%rd%d %d",
               m->ptx_all_regs[ri.grno]->num, ri.hopovers_remaining);
        }
    }
}

void
PTX_Reg_Pool::tree_check(PTX_Reg_Pool* rp_curr)
{
  // If execution changed to or from the main code path (code_path 1)
  // update the pool of live address registers.

  assert( pool_idx == 1 );
  const int curr_pool_idx = rp_curr->pool_idx;
  const bool belady = m->c->opt_addr_regs_opt;

  if ( curr_pool_idx == 1 )
    {
      if ( children.empty() ) return;

      // Code Path Reconvergence
      //

      const int seq_num_parent = seq_num_next;

      vector<PTX_Reg_Pool*> kinder;
      for ( auto ch_e: children ) kinder.push_back(ch_e.first);

      // Remove a code-path-1 register if it is not live in all children.
      // Set path-1 sequence number to the maximum of child code paths.
      //
      for ( auto rp_ch: kinder )
        {
          set_max(seq_num_next,rp_ch->seq_num_next);
          pVector<int> grno_remove;
          for ( auto& e: rp_items )
            if ( int ch_seq_num = rp_ch->check_seq(e.first) )
              set_max(e.second.seq_num,ch_seq_num);
            else
              grno_remove += e.first;
          for ( int grno: grno_remove ) rp_items.erase(grno);
        }

      vector<int> grno_sorted;
      if ( belady )
        {
          // Find the lowest-numbered register still live in all code-path
          // children. Lower-numbered registers will not be reused.
          //
          int grno_min_common = 0;
          for ( auto& ch: kinder )
            if ( !ch->rp_items.empty() )
              set_max(grno_min_common,ch->rp_items.begin()->first);

          // Create a list of registers sorted so that registers used
          // by more code-path children appear earlier.
          //
          map<int,int> grno_to_n_paths;
          for ( auto& ch: kinder )
            for ( auto& rpi: ch->rp_items )
              if ( rpi.first >= grno_min_common
                   && rpi.second.seq_num >= seq_num_parent )
                grno_to_n_paths[rpi.first]++;
          for ( auto& e: grno_to_n_paths )
            grno_sorted.push_back(e.first);
          sort(grno_sorted.begin(),grno_sorted.end(),
               [&](int a, int b)
               { return
                   grno_to_n_paths[a] == grno_to_n_paths[b] ? a < b :
                   grno_to_n_paths[a] > grno_to_n_paths[b];
                 }
               );

          // Put prioritized registers in code-path 1 pool.
          // NOTE: Priorities are currently meaningless since, if there
          // is space for x registers, the first x registers touched
          // will be used.  8 June 2018, 13:33:27 CDT
          //
          for ( auto grno: grno_sorted ) rinsert(grno);
          for ( auto& ch: kinder ) ch->rp_items.clear();
        }

      children.clear();
      seq_to_grno_rebuild();
    }
  else
    {
      // Execution switched to a child code path of code path 1.
      // Inherit pool of live registers.
      if ( !children[rp_curr]++ ) rp_curr->inherit(*this);
    }
}

void
PTX_Reg_Pool::seq_to_grno_rebuild()
{
  seq_to_grno.clear();
  for ( auto e: rp_items ) seq_to_grno[e.second.seq_num] = e.first;
}

void
PTX_Reg_Pool::inherit(const PTX_Reg_Pool& rp)
{
  assert( seq_num_next <= rp.seq_num_next );
  assert( pool_idx != 1 );
  seq_num_next = rp.seq_num_next;
  for ( auto e: rp.rp_items ) rp_items[e.first] = e.second;
  seq_to_grno_rebuild();
  cull();
}

void
PTX_Reg_Pool::cull()
{
  if ( m->c->opt_addr_regs_opt && max_live > 1 ) return;
  while ( seq_to_grno.size() > max_live )
    {
      const auto lru_elt = *seq_to_grno.begin();
      seq_to_grno.erase(lru_elt.first);
      rp_items.erase(lru_elt.second);
    }
}

void
PTX_Reg_Pool::rinsert(int grno)
{
  PTX_Reg_Pool_Item& rpi = rp_items[grno];
  rpi.grno = grno;
  rpi.seq_num = seq_num_next++;
  rpi.hopovers_remaining = max_live - 1;
}

void
PTX_Reg_Pool::rnew(const PTX_RR& r)
{
  PTX_Reg_Pool_Item& rpi = rp_items[r.global_num];
  assert( rpi.grno == 0 );
  rpi.grno = r.global_num;
  rpi.seq_num = seq_num_next++;
  rpi.hopovers_remaining = max_live - 1;
  seq_to_grno[rpi.seq_num] = rpi.grno;
  cull();
  n_reg_defs++;
}
void
PTX_Reg_Pool::rnew(const PTX_RR_Base_Extension& be) { rnew(be.reg); }


PTX_RR_Base_Extension
Chemora_CG_Calc_Mapping::ptx_make_addr_base
(PTX_RR_Addr_Base *base_bo, size_t addr, int acc_offset)
{
  CaCUDALib_GPU_Info& gi = *c->gpu_info;

  auto& bev = base_bo->base_extensions;
  PTX_RR_Base_Extension *be_ref = NULL;

  auto& reg_pool = code_path_pool_get(ek.code_path);

  ek.code_path_reg_pool[1].tree_check(&reg_pool);

  int nstale = 0;

  // Look for a register that can reach offset.
  //
  for ( auto& be: bev )
    {
      if ( be.stale ) { nstale++; continue; }
      if ( be.code_path != 1 && be.code_path != ek.code_path ) continue;
      const size_t br_addr = be.addr;
      assert( br_addr );
      if ( !c->opt_array_bases_share && br_addr != addr ) continue;
      const int64_t boffset = acc_offset + addr - br_addr;

      // Check whether register's base address is in a different 4 GiB
      // region of memory.
      //
      const bool x4G = br_addr >> 32 != addr >> 32;
      if ( !c->opt_4Gx_okay && x4G ) continue;

      // If boffset too large for offset field in SASS load and store
      // instructions we need an extension register for base_reg,
      // which is what ptx_rr_base_extensions is for.
      //
      const bool reachable =
        boffset <= gi.sass_ldst_offset_max
        && boffset >= gi.sass_ldst_offset_min;

      if ( !reachable && be_ref ) continue;

      if ( !reg_pool.check(be) ) { be.stale = 1; nstale++; continue; }
      if ( reachable ) return reg_pool.use(be);
      be_ref = &be;
    }

  PTX_RR_Base_Extension be_new;

  be_new.addr = addr;
  be_new.code_path = ek.code_path;
  be_new.stale = false;

  pStringF cpmsg("Code path %d, pool %d",ek.code_path,reg_pool.pool_idx);

  if ( be_ref )
    {
      reg_pool.use(*be_ref);
      assert( be_ref->code_path == 1 || be_ref->code_path == ek.code_path );
      be_new.reg = ptx_make(PT_s64, "add.s64" )
        << be_ref->reg << addr - be_ref->addr << TCOMMENT(cpmsg);
    }
  else
    {
      PTX_Insn insn_base_new = ptx_make(PT_s64, "mad.wide.s32")
        << base_bo->ptxr_thd_offset << base_bo->offset_scale_factor
        << TCOMMENT(cpmsg);
      Grid_Function* const gf = c->addr_to_gf[addr];
      if ( c->opt_addr_literals || !gf )
        {
          be_new.reg = insn_base_new << addr;
        }
      else
        {
          PTX_RR &ptxr_gf_base = ek.gf_addr_to_ptxr[addr + ek.code_path];
          if ( !reg_pool.check(ptxr_gf_base) )
            {
              ptxr_gf_base = ptx_make(PT_s64, "ld.param.s64")
                << pstringf("[%s_param_addr]",gf->name.c_str());
              reg_pool.rnew(ptxr_gf_base);
            }
          else
            reg_pool.use(ptxr_gf_base);
          be_new.reg = insn_base_new << ptxr_gf_base;
        }
    }

  if ( nstale > 5 )
    bev.erase
      ( remove_if
        ( bev.begin(), bev.end(),
          [](const PTX_RR_Base_Extension& be){ return be.stale; }),
        bev.end() );

  reg_pool.rnew(be_new);
  bev += be_new;
  return be_new;
}

PTX_Insn_Addr_Offs
Chemora_CG_Calc_Mapping::ptx_make_addr(PTX_RR base_reg, int64_t offset)
{
  assert( base_reg.num >= 0 );

  return PTX_Insn_Addr_Offs(base_reg,offset);
}

PTX_Insn_Addr_Offs
Chemora_CG_Calc_Mapping::ptx_make_addr(WG_Round& wgr, int64_t iter_offs_sc)
{
  return
    ptx_make_addr
    ( ek.halo2_data.addr_bo_halo2[wgr.base_reg_idx], wgr.gf->device_addr,
      iter_offs_sc + wgr.gl_offset_sc );
}

PTX_Insn_Addr_Offs
Chemora_CG_Calc_Mapping::ptx_make_addr
(PTX_RR_Addr_Base *addr_bo, size_t addr, int acc_offset)
{
  PTX_RR_Base_Extension be_use =
    ptx_make_addr_base(addr_bo,addr,acc_offset);
  const int64_t offset = addr - be_use.addr + acc_offset;

  return PTX_Insn_Addr_Offs( be_use.reg, offset );
}

PTX_Insn::PTX_Insn(Chemora_CG_Calc_Mapping *mp):m(mp)
{
  assert( m->ptx_in_body );
  comma = true;
  predicate_present = false;
  predicated_off = false;
  operands = 0;
  emitted = false;
  text_indent_adj = 0;
  text_inter_op = ", ";
  text_close = ";";
  for ( auto& t: src_types ) t = PT_SIZE;
  m->ptx_emit_insns_pending++;
}

PTX_Insn
Chemora_CG_Calc_Mapping::ptx_make_insn_new(PTX_RR& dest)
{
  PTX_Insn insn(this);
  if ( dest.num == 0 ) dest = ptx_make_reg(dest.rt);
  assert( dest.num > 0 );
  dest.single_assign = false;
  insn.dest = dest;
  return insn;
}


PTX_Insn
Chemora_CG_Calc_Mapping::ptx_make(PTX_RR& dest, const string mnemonic)
{
  PTX_Insn&& insn = ptx_make_insn_new(dest);
  ptx_make_1(insn,dest.rt,mnemonic);
  return insn;
}

void
Chemora_CG_Calc_Mapping::ptx_make_1
(PTX_Insn& insn, PTX_Reg_Type rt_src, const string mnemonic)
{
  if ( emit_clike ) insn_cli_make_1(insn,rt_src,mnemonic);
  else              insn_ptx_make_1(insn,rt_src,mnemonic);
}

void
Chemora_CG_Calc_Mapping::insn_ptx_make_1
(PTX_Insn& insn, PTX_Reg_Type rt_src, const string mnemonic)
{
  const bool no_prefix = mnemonic.substr(0,3) == "cvt";
  for ( auto& t: insn.src_types ) t = rt_src;

  insn.text = mnemonic;
  if ( !no_prefix )
  switch ( rt_src ) {
    case PT_f32: insn.text += ".f32"; break;
    case PT_f64: insn.text += ".f64"; break;
    default: break; }
  const int len = insn.text.length();
  if ( len < 8 ) insn.text += "\t\t";
  else if ( len < 16 ) insn.text += "\t";
  else insn.text += "  ";
  insn.text += insn.dest.txt;
}

string
Chemora_CG_Calc_Mapping::insn_cli_lhs_make
(PTX_Insn& insn)
{
  string lhs = insn.dest.txt + " =";
  if ( ! ptx_reg_declared[insn.dest.txt] )
    {
      lhs = string(ptx_rr_ctype[insn.dest.rt]) + " " + lhs;
      ptx_reg_declared[insn.dest.txt] = true;
    }
  return lhs;
}

void
Chemora_CG_Calc_Mapping::insn_cli_make_1
(PTX_Insn& insn, PTX_Reg_Type rt_src, const string mnemonic)
{
  insn.text_close = ");";
  insn.text = insn_cli_lhs_make(insn) + " " + mnemonic + "( ";
  insn.comma = false;
}

PTX_Insn
Chemora_CG_Calc_Mapping::ptx_make(PTX_Reg_Type rt, const string mnemonic)
{
  PTX_Insn insn(this);
  insn.dest = ptx_make_reg(rt);
  ptx_make_1(insn,rt,mnemonic);
  return insn;
}

PTX_Insn
Chemora_CG_Calc_Mapping::insn_cli_op_make(PTX_Reg_Type rt, const string op)
{ return insn_cli_op_make(rt,rt,op); }

PTX_Insn
Chemora_CG_Calc_Mapping::insn_cli_op_make
(PTX_Reg_Type rt_dest, PTX_Reg_Type rt_src, const string op)
{
  PTX_Insn insn(this);
  insn.dest = ptx_make_reg(rt_dest);
  insn.text_inter_op = " " + op;
  insn.text = insn_cli_lhs_make(insn);
  insn.comma = false;
  return insn;
}

PTX_Insn
Chemora_CG_Calc_Mapping::ptx_make
(PTX_Reg_Type rt_dst, PTX_Reg_Type rt_src, const char* mnemonic)
{
  PTX_Insn insn(this);
  insn.dest = ptx_make_reg(rt_dst);
  ptx_make_1(insn,rt_src,mnemonic);
  return insn;
}

PTX_Insn
Chemora_CG_Calc_Mapping::ptx_make_lhs
(PTX_Reg_Type rt_dst, const string sep)
{
  PTX_Insn insn(this);
  insn.text_inter_op = sep;
  insn.text_close = ";";
  insn.dest = ptx_make_reg(rt_dst);
  insn.text = insn_cli_lhs_make(insn);
  insn.comma = false;
  assert( emit_clike );
  return insn;
}

PTX_Insn
Chemora_CG_Calc_Mapping::ptx_make_no_dest
(PTX_Reg_Type rt, const string mnemonic)
{
  PTX_Insn insn(this);
  for ( auto& t: insn.src_types ) t = rt;

  assert( !emit_clike );

  insn.text = mnemonic;
  switch ( rt ){
    case PT_f32: insn.text += ".f32"; break;
    case PT_f64: insn.text += ".f64"; break;
    default: break; }
  const int len = insn.text.length();
  if ( len < 9 ) insn.text += "\t\t";
  else if ( len < 17 ) insn.text += "\t";
  else insn.text += "  ";
  insn.comma = false;
  return insn;
}

PTX_Insn
Chemora_CG_Calc_Mapping::ptx_make(const char* mnemonic)
{
  PTX_Insn insn(this);
  insn.comma = false;
  insn.text = mnemonic;
  assert( !emit_clike );
  return insn;
}

PTX_Insn
Chemora_CG_Calc_Mapping::ptx_make_opaque_eq_reg(const string& mnemonic)
{
  PTX_Insn insn(this);
  insn.text = mnemonic + " =";
  insn.comma = false;
  return insn;
}

PTX_Insn
Chemora_CG_Calc_Mapping::ptx_makef(const char* fmt, ...)
{
  va_list ap;
  pString line;
  va_start(ap,fmt);
  line.vsprintf(fmt,ap);
  va_end(ap);
  PTX_Insn insn(this);
  insn.comma = false;
  insn.text = line;
  assert( !emit_clike );
  return insn;
}

void
PTX_Insn::operand_insert_start()
{
  assert( operands < sizeof(src_types)/sizeof(src_types[0]) );
  operands++;
  if ( comma ) text += text_inter_op;
  comma = true;
}

PTX_Insn&
PTX_Insn::predicate(PTX_RR reg, bool negate = false)
{
  assert( !predicate_present );
  assert( reg.num >= 0 );
  reg.use();
  predicate_present = true;
  string pred_indicator = ( negate ? "@!" : "@" ) + reg.txt;
  if ( !reg.ctc )
    {
      srcs += reg;
      text = pred_indicator + " " + text;
      text_indent_adj = -1 -int(pred_indicator.size());
    }
  else if ( bool(reg.ctc_vali) ^ negate )
    {
      text = "/*" + pred_indicator + "*/ " + text;
    }
  else
    {
      text = "// " + pred_indicator + text;
      predicated_off = true;
    }
  return *this;
}

PTX_Insn&
PTX_Insn::npredicate(PTX_RR reg)
{
  return predicate(reg,true);
}

PTX_Insn&
PTX_Insn::operator << (Node *nd)
{
  return nd->ctc_val_set ? operator <<(nd->ctc_val) : operator <<(nd->ptxr);
}

PTX_Insn&
PTX_Insn::operator << (PTX_RR reg)
{
  assert( reg.num >= 0 );
  if ( !reg.ctc ) srcs += reg;
  reg.use();
  operand_insert_start();
  text += " " + reg.txt;
  return *this;
}

PTX_Insn&
PTX_Insn::operator << (double immed)
{
  assert( rt_is_fp(src_types[operands]) ); // Looking for a testcase.
  operand_insert_start();
  text += pstringf(" %.20e", immed);
  return *this;
}

PTX_Insn&
PTX_Insn::operator << (int immed)
{
  if ( rt_is_fp(src_types[operands]) ) return operator<<(double(immed));
  operand_insert_start();
  text += pstringf(" %d", immed);
  return *this;
}

PTX_Insn&
PTX_Insn::operator << (int64_t immed)
{
  if ( rt_is_fp(src_types[operands]) ) return operator<<(double(immed));
  operand_insert_start();
  text += pstringf("%s%#lx", immed < 0 ? "-" : "", labs(immed) );
  return *this;
}

PTX_Insn&
PTX_Insn::operator << (size_t immed)
{
  operand_insert_start();
  text += pstringf(" %#zx", immed);
  return *this;
}

PTX_Insn&
PTX_Insn::operator << (void *immed)
{
  operand_insert_start();
  text += pstringf(" %#zx", size_t(immed));
  return *this;
}

PTX_Insn&
PTX_Insn::operator << (const char* opaque)
{
  operand_insert_start();
  text += string(" ") + opaque;
  return *this;
}

PTX_Insn_Text operator "" _insn_txt (const char *s, size_t len)
{ return PTX_Insn_Text(s); }

PTX_Insn&
PTX_Insn::operator << (const PTX_Insn_Addr_Offs ao)
{
  operator <<("[");
  comma = false;
  operator <<(ao.base_reg);
  comma = false;
  pStringF offs("+ %s%#lx", ao.offset < 0 ? "-" : "", labs(ao.offset) );
  pStringF rest(" %s ]", ao.offset ? offs.s : "");
  operator <<(rest);
  return *this;
}

PTX_Insn&
PTX_Insn::operator << (PTX_Insn_Text not_an_operand)
{
  text += not_an_operand;
  return *this;
}

PTX_Insn&
PTX_Insn::operator << (const string& opaque)
{
  return operator << (opaque.c_str());
}

PTX_Insn&
PTX_Insn::operator << (PTX_Insn_Tail_Comment tc)
{
  tail_comment += tc;
  return *this;
}

PTX_Insn::operator PTX_RR()
{
  emit();
  return dest;
}

PTX_Insn&
PTX_Insn::operator << (PTX_Insn_Emit dummy)
{
  emit();
  return *this;
}

void
PTX_Insn::emit()
{
  assert( m->ptx_emit_insns_pending > 0 );
  assert( text.length() > 0 );
  assert( !emitted );
  assert( !predicated_off || dest.num < 0 || !dest.single_assign );
  emitted = true;
  const int ind_amt = max(0,m->ptx_indent + text_indent_adj);
  string ftxt = "";
  ftxt += string(ind_amt,' ') + text + text_close;
  if ( tail_comment.length() )
    ftxt += string(" // ") + tail_comment;
  ftxt += "\n";
  m->ptx_emit_insns_pending--;
  bidx = m->ptx_body_items.size();
  m->ptx_insns.emplace_back(*this);
  m->ptx_body_items += ftxt;
}

PTX_RR
Chemora_CG_Calc_Mapping::ptx_make_gfo_roc_or_gl_load
(PTX_Reg_Type pt, Grid_Function *gf, PTX_RR_Addr_Base *addr_bo,
 ptrdiff_t addr_offset, int di, int dj, int dk, char choice)
{
  assert( choice == 'r' || choice == 'g' );
  string mnem =
    choice == 'r' ? chemora->ptx_mnem_ld_roc : chemora->ptx_mnem_ld_offset;
  return
    ptx_make_gfo_load_mnemonic
    (pt,gf,addr_bo,addr_offset,di,dj,dk,mnem);
}

PTX_RR
Chemora_CG_Calc_Mapping::ptx_make_gfo_load_mnemonic
(PTX_Reg_Type pt, Grid_Function *gf, PTX_RR_Addr_Base *addr_bo,
 ptrdiff_t addr_offset, int di, int dj, int dk, const string mnemonic)
{
  const int assumed_elt_size = ptx_rr_bytes[pt];
  const int gp_offset = ptx_make_gp_offset_global(addr_bo->axes,di,dj,dk);

  PTX_RR rv = ptx_make(pt,mnemonic)
    << ptx_make_addr
    ( addr_bo, gf->device_addr, addr_offset + assumed_elt_size * gp_offset );

  return rv;
}

void
Chemora_CG_Calc_Mapping::ptx_lib_start(string dir)
{
  ptx_lib_path_base = c->temporary_file_path_prefix_get() + "-ptx-lib";
  ptx_lib_nfuncs = 0;
  ptx_lib_cu_path = ptx_lib_path_base + ".cu";
  ptx_lib_cu_fh = fopen(ptx_lib_cu_path.c_str(),"w");
  assert( ptx_lib_cu_fh );
}

void
Chemora_CG_Calc_Mapping::ptx_lib_entry_insert
(const char *func, int argc, const char *dtype)
{
  ptx_lib_nfuncs++;
  fprintf(ptx_lib_cu_fh,"extern \"C\" __device__ %s\n",dtype);
  fprintf(ptx_lib_cu_fh,"chemora_wrapper__%s(",func);
  for ( int i=0; i<argc; i++ )
    fprintf(ptx_lib_cu_fh,"%s%s arg%d", i ? ", " : "", dtype, i);
  fprintf(ptx_lib_cu_fh,")\n{ return %s(", func);
  for ( int i=0; i<argc; i++ )
    fprintf(ptx_lib_cu_fh,"%sarg%d", i?", ":"",i);
  fprintf(ptx_lib_cu_fh,"); }\n\n");
}

void
Chemora_CG_Calc_Mapping::ptx_lib_emit()
{
  assert( ptx_lib_cu_fh );
  fclose(ptx_lib_cu_fh);
  if ( !ptx_lib_nfuncs ) return;
  CaCUDALib_CUDA_Driver_Manager* const dm = CaCUDALib_CUDA_driver_manager_get();
  CaCUDALib_GPU_Info& gi = *c->gpu_info;
  pStringF ptx_path("%s.ptx",ptx_lib_path_base.c_str());

  // Make sure that the host compiler does not use a dialect of C++ that
  // nvcc does not understand by default.
  const char* const c_version =
#if __GNUC_PREREQ(7,0)
    "c++11";
#else
    "gnu++98";
#endif
  const char* const flag_std = pstringf("-Xcompiler -std=%s", c_version);
  assert( gi.cuda_driver_version >= 8000 ); // Test c_version and remove assert.

  // -dc: Generate relocatable code.

  pStringF cmd
    ("%s -ptx --gpu-architecture=sm_%d%d %s %s %s -o %s",
     dm->nvcc_path_get(), gi.cc_major, gi.cc_minor,
     flag_std,
     gi.cuda_driver_version >= 8000 ? "-dc" : "",
     ptx_lib_cu_path.c_str(), ptx_path.s);
  int rv = system(cmd.s);
  assert( !rv );
  ifstream inc(ptx_path.s,ios_base::in);
  assert( inc.is_open() );
  const char* end_skip_cue = ".address_size";
  while ( !inc.eof() )
    {
      string line;
      getline(inc,line);
      if ( line.find(end_skip_cue) != string::npos ) break;
  }

  ptx_lines("\n\n","//","// Library Function Definitions","//","",NULL);

  while ( !inc.eof() )
    {
      string line;
      getline(inc,line);
      ptx_line(line);
  }
  inc.close();

  ptx_lines("//","// End of Library Function Definitions","\n\n",NULL);
}


void
Chemora_CG_Calc_Mapping::ptx_cf_shared_block_setup(Buffer_Group *bg)
{
  if ( !bg->shared_vars ) return;

  PTX_RR &ptxr_tid = ek.ptxr_tid;

  const int rows_per_block = bg->rows_per_block =
    ( bg->f_length + bg->rounds_block - 1 ) / bg->rounds_block;
  const bool frac_round = rows_per_block * bg->rounds_block > bg->f_length;

  PTX_RR ptxr_off_g_r =
    ptx_make(PT_s32,"div.s32") << ptxr_tid << bg->sc_y_stride;
  PTX_RR ptxr_off_g_c =
    ptx_make(PT_s32,"rem.s32") << ptxr_tid << bg->sc_y_stride;
  bg->ptxr_fetch_participant =
    ptx_make(PT_pred,"setp.lt.s32") << ptxr_off_g_r << rows_per_block;
  bg->sc_fetch_rounds = bg->rounds_block;
  bg->sc_fetch_whole_rounds = bg->rounds_block - frac_round;

  CStncl& stncl = bg->stncl;

  ptx_line("// tilegs_origin_idx.");
  PTX_RR ptxr_reg = ptx_make(PT_s32,"mad.lo.s32")
    << ek.ptxr_gj_blk << ek.cagh_ni
    << ek.ptxr_gi_blk;
  ptxr_reg = ptx_make(PT_s32,"mad.lo.s32")
    << ek.ptxr_gk_blk << ek.gl_z_stride
    << ptxr_reg;
  PTX_RR ptxr_tilegs_origin_idx = ptx_make(PT_s32,"add.s32")
    << ptxr_reg
    << ( stncl.imin + stncl.jmin * ek.cagh_ni + stncl.kmin * ek.gl_z_stride );

  PTX_RR ptxr_sc_off_g_start = ptx_make(PT_s32,"mad.lo.s32")
    << ptxr_off_g_r << ek.gl_f_stride << ptxr_off_g_c;
  PTX_RR ptxr_sc_off_s_start = ptx_make(PT_s32,"mad.lo.s32")
    << ptxr_off_g_r << bg->sc_f_stride << ptxr_off_g_c;
  bg->ptxr_sc_sc_thd_base = ptx_make(PT_s32,"mad.lo.s32")
    << ptxr_sc_off_s_start << ek.assumed_elt_size
    << ek.ptxr_shared_buffer_addr;

  PTX_RR ptxr_gl_thd_offset = ptx_make(PT_s32,"add.s32")
    << ptxr_tilegs_origin_idx << ptxr_sc_off_g_start;
  bg->addr_bo = ptx_addr_base_new(ptxr_gl_thd_offset);

  if ( bg->sc_extra_x_tile )
    bg->ptxr_sc_extra_x_thd =
      ptx_make(PT_pred,"setp.lt.s32")
      << ek.ptxr_tid << bg->sc_y_stride - ek.block_size;

  PTX_RR ptxr_row_last_round =
    ptx_make(PT_s32,"add.s32")
    << ptxr_off_g_r << bg->sc_fetch_whole_rounds * rows_per_block;
  ptx_line("// sc_fetch_participant_frac_round");
  bg->ptxr_sc_fetch_participant_frac_round =
    ptx_make(PT_pred,"setp.lt.s32") << ptxr_row_last_round << bg->f_length;

  ptxr_reg =
    ptx_make(PT_s32, "add.s32") << ek.ptxr_li_sc << ek.ptxr_shared_buffer_addr;
  ptxr_reg =
    ptx_make(PT_s32, "mad.lo.s32")
    << ek.ptxr_lj << bg->sc_y_stride * ek.assumed_elt_size << ptxr_reg;
  ptxr_reg =
    ptx_make(PT_s32, "mad.lo.s32")
    << ek.ptxr_lk << bg->sc_z_stride * ek.assumed_elt_size << ptxr_reg;
  bg->ptxr_sh_base_common =
    ptx_make(PT_s32, "sub.s32")
    << ptxr_reg
    << ( stncl.imin + stncl.jmin * bg->sc_y_stride
         + stncl.kmin * bg->sc_z_stride ) * ek.assumed_elt_size;
}

enum Chemora__Transfer { CT_g_to_s, CT_g_to_l, CT_l_to_s };

void
Chemora_CG_Calc_Mapping::ptx_cf_shared_block_load_loop
(Buffer_Group *bg, bool pre_loop, int xfer)
{
  if ( !bg->shared_vars ) return;
  pStringF line_cache_loop_start_end
    ("cache_loop_done_%d", ptx_line_label_cnt++);

  ptx_make("bra").npredicate(bg->ptxr_fetch_participant)
    << line_cache_loop_start_end.s << EMIT;

  const int e_start = pre_loop ? 0 : bg->stncl_e;

  for ( int round = 0; round < bg->sc_fetch_rounds; round++ )
    {
      const bool frac_round = round >= bg->sc_fetch_whole_rounds;
      if ( frac_round )
        ptx_make("bra").npredicate(bg->ptxr_sc_fetch_participant_frac_round)
          << line_cache_loop_start_end.s << EMIT;

      for ( int e = e_start; e < bg->e_length; e++ )
        {
          const int e_s =
            pre_loop ? e : ( e + ek.tt * ek.tile_e ) % bg->e_length;
          for ( GFVIter gf(bg->gfs_b); gf; )
            ptx_cf_cak_fetch_to_cache_round(bg, gf, e_s, round, e, xfer);
        }
    }

  ptx_line0f("%s:",line_cache_loop_start_end.s);
}

void
Chemora_CG_Calc_Mapping::ptx_cf_shared_block_load_pre_loop(Buffer_Group *bg)
{
  if ( !bg->shared_vars ) return;
  ptx_cf_shared_block_load_loop(bg,true,CT_g_to_s);
  ptx_make("bar.sync 0") << EMIT;
}

void
Chemora_CG_Calc_Mapping::ptx_cf_cak_fetch_to_cache_round
(Buffer_Group *bg, Grid_Function *gf, int e_s, int round, int e, int transfer)
{
  const int k_var_idx = gf->shared_var_idx;

  Offsets ref = gf->pattern_info.ref;
  const int ref_adj = ref.i + ref.j * ek.cagh_ni + ref.k * ek.gl_z_stride;

  const int g_load_offset =
    ( round * bg->rows_per_block * ek.gl_f_stride
      + e * ek.gl_e_stride + ek.iter_offs + ref_adj ) * ek.assumed_elt_size;

  const int s_load_offset =
    ( round * bg->rows_per_block * bg->sc_f_stride
      + e_s * bg->sc_e_stride ) * ek.assumed_elt_size
    + bg->shared_array_base_byte
    + gf->bg_sh_var_idx * bg->sc_var_stride_bytes;

  //  const int idx_g_clamped =
    //  cak__.gf_have_overrun ? idx_g : min(idx_g,cak__.lsh_size-1);

  const bool have_overrun = true; // Living dangerously.
  assert( have_overrun ); // See, I checked. :-)

  const int idx_l_raw = ( bg->e_length - e - 1 ) * bg->sc_fetch_rounds + round;
  const int idx_l1 = bg->sc_extra_x_tile ? 2 * idx_l_raw : idx_l_raw;

  const bool from_g = transfer == CT_g_to_s || transfer == CT_g_to_l;
  const bool to_s =   transfer == CT_g_to_s || transfer == CT_l_to_s;
  PTX_RR* const ptxr_l = &ek.ptxr_l[ k_var_idx * ek.local_elts_max ];

  PTX_RR ptxr_val1 =
    from_g ? ( ptx_make(ek.pt_real,chemora->ptx_mnem_ld_sm_populate)
          << ptx_make_addr( bg->addr_bo, gf->device_addr, g_load_offset ) )
    : ptxr_l[ idx_l1 ];

  if ( to_s )
    ptx_make_no_dest(ek.pt_real,"st.shared")
      << ptx_make_addr( bg->ptxr_sc_sc_thd_base, s_load_offset )
      << ptxr_val1 << EMIT;
  else
    ptxr_l[ idx_l1 ] = ptxr_val1;

  if ( bg->sc_extra_x_tile )
    {
      const int idx_l2 = idx_l1 + 1;
      PTX_RR ptxr_val2 =
        from_g ? ( ptx_make(ek.pt_real,chemora->ptx_mnem_ld_sm_populate)
                   .predicate(bg->ptxr_sc_extra_x_thd)
                   << ptx_make_addr
                   ( bg->addr_bo, gf->device_addr,
                     g_load_offset + ek.block_size * ek.assumed_elt_size ) )
        : ptxr_l[ idx_l2 ];

      if ( to_s )
        ptx_make_no_dest(ek.pt_real,"st.shared")
          .predicate(bg->ptxr_sc_extra_x_thd)
          << ptx_make_addr
          ( bg->ptxr_sc_sc_thd_base,
            s_load_offset + ek.block_size * ek.assumed_elt_size )
          << ptxr_val2 << EMIT;
      else
        ptxr_l[ idx_l2 ] = ptxr_val2;
    }
}


string
Chemora_CG_Calc::generate_code()
{
  /// Decompose calculation into kernels and emit code.
  // Return the name of the file in which to find the code.

  Chemora_CG_Calc_Mapping* const m_1k = new Chemora_CG_Calc_Mapping;
  Chemora_CG_Calc_Mapping& mapping_1k = *m_1k;

  srandom(rand_seed);  srand48(rand_seed);

  /// Perform a one-kernel mapping.
  //
  //  This code is here for debugging purposes. Usually the one-kernel
  //  mapping is ignored and the one-kernel code is overwritten.
  //  During debugging one might examine the one-kernel code,
  //  or even use it instead of the multi-kernel mapping.

  mapping_1k.init(this,1,"1k");
  for ( NIter nd(nodes); nd; )
    if ( nd->visited ) mapping_1k.assign_to(nd,0);
  mapping_1k.fixup();
  const double et_1k = mapping_1k.compute_cost();
  mapping_1k.verify();
  if ( opt_code_target_acc )
    {
      string onek_acc_name = mapping_1k.emit_code_clike();
      return onek_acc_name;
    }
  string onek_name = mapping_1k.emit_code();
  if ( opt_num_kernels == 1 ) return onek_name;

  Chemora_CG_Calc_Mapping* const m = new Chemora_CG_Calc_Mapping;
  const int max_kernels = opt_num_kernels ?abs(opt_num_kernels): 30;

  // Initialize mapping structure.
  //
  m->init(this,max_kernels,"op");

  // As a starting point for optimization map to kernels.
  //
  const int num_kernels_start = m->split(max_kernels);

  // Compute a cost based on the random split.
  //
  double et_curr = m->compute_cost();
  m->verify();

  const bool immobile_stores = opt_move_constraint == 's';
  const bool num_k_exact = opt_move_constraint == 'n';
  const bool no_new_kernels = immobile_stores || num_k_exact;

  if ( no_new_kernels && num_kernels_start == 1 )
    {
      delete m_1k;
      m->compute_cost();
      return m->emit_code();
    }

  //
  // Iteratively Improve Mapping
  //

  // Move_Proposal is relatively heavy, but re-usable.
  Move_Proposal move_proposals[2];
  Move_Proposal *mp_accepted = NULL;
  Move_Proposal *mp_free = &move_proposals[0];
  Move_Proposal *mp = &move_proposals[1];

  move_proposals[0].immobile_stores = move_proposals[1].immobile_stores =
    immobile_stores;

  int impr_distance = 0;
  int vnodes = 0;
  for ( NIter nd(nodes); nd; ) if ( nd->visited ) vnodes++;
  const int num_nodes_use = min(opt_opt_nodes_max,vnodes);
  const int no_impr_limit = num_nodes_use;
  const int cycle_len = opt_opt_props_per_node_per_cycle * num_nodes_use;
  const int num_cyc = opt_opt_num_cycles;
  const int num_iter = 1000 * cycle_len * num_cyc;
  int cycle_num = 0;
  const double cycle_len_inv = 1.0 / cycle_len;
  int cycle_i = 0;
  const int num_kernels = m->num_kernels;

  Chemora_CG_Calc_Mapping best;
  const bool onek_better = !no_new_kernels && et_1k < et_curr;
  double et_best = onek_better ? et_1k : et_curr;
  best.quick_copy( onek_better ? &mapping_1k : m );

  const double time_start = time_wall_fp();

  const bool ik_relax_do = true;
  bool table_header_printed = false;

  int ik_relax_to_go = 0;
  if ( ik_relax_do )
    for ( int kno=0; kno<num_kernels; kno++ )
      if ( m->kernel_info[kno].should_run )
        ik_relax_to_go += 2 * m->kernel_info[kno].etc.ik_ld;

  for ( int i=0; i<num_iter && cycle_num < num_cyc; )
    {
      // Though m can hold as many as m->num_kernels kernels, only a
      // subset of those can be non-empty. To make it easier to
      // propose a random change prepare array kno such that if there
      // are L non-empty (live) kernels, m->kernel_info[kno[0]]
      // returns the first non-empty kernel, m->kernel_info[kno[1]]
      // returns the second non-empty kernel, etc.
      //
      int live_k = 0;
      int empty_k = 0;
      int pos_to_kno[num_kernels];  // Consecutive list of live kernels.
      int pos_to_knon[num_kernels]; // Consecutive list of empty spaces.
      for ( int kno=0; kno<num_kernels; kno++ ) 
        if ( m->kernel_info[kno].should_run ) pos_to_kno[live_k++] = kno;
        else                                  pos_to_knon[empty_k++] = kno;

      enum Move_Type { MT_Move, MT_IK_Relax, MT_New_Kernel, MT_SIZE };

      int num_iks = 0;
      for ( int j=0; j<live_k; j++ )
        num_iks += m->kernel_info[pos_to_kno[j]].etc.ik_st;

      if ( num_iks == 0 ) ik_relax_to_go = 0;
      const bool ik_relax =
        num_iks && ( ik_relax_to_go || random() % vnodes < 4 * num_iks );

      const bool new_kernel =
        !no_new_kernels && empty_k > 0
        && ( live_k < 2 || ( random() & 0x3 ) == 0 );

      const int move_type =
        ik_relax ? MT_IK_Relax : new_kernel ? MT_New_Kernel : MT_Move;

      switch ( move_type ) {

      case MT_Move: case MT_New_Kernel:
        {
          const int from_i = random() % live_k;
          const int from = pos_to_kno[from_i];
          const int dist = 1 + random() % max(1, live_k - 1 );
          const int to_i = ( from_i + dist ) % live_k;
          const int to_e = random() % max(1,empty_k);
          const int to = new_kernel ? pos_to_knon[to_e] : pos_to_kno[to_i];
          assert( from != to );

          Chemora_CG_Kernel_Info* const fki = &m->kernel_info[from];

          // Pick a node to move.
          //
          const int nidx = random() % fki->nodes.size();
          Node* const candidate = fki->nodes[nidx];

          mp->gang_move = random() & 1;
          mp->from = from;
          mp->to = to;
          mp->candidate = candidate;
          mp->copy_from = false;
        }
        break;

      case MT_IK_Relax:
        {
          Nodes ik_relax_cand;
          if ( ik_relax_to_go > 0 ) ik_relax_to_go--;
          for ( NIter nd(nodes); nd; )
            {
              if ( !nd->visited ) continue;
              Mapping_Node* const md = m->mapping_node_get(nd);
              if ( ! md->ik_needed ) continue;
              ik_relax_cand += nd;
            }
          const int ik_idx = random() % ik_relax_cand.size();
          Node* const nd_from = ik_relax_cand[ik_idx];
          Mapping_Node* const md_from = m->mapping_node_get(nd_from);
          const int from = md_from->ik_kno;
          const A_Vector_Type cv = m->ik_consumers_vector_get(nd_from);
          const int nc = pop(cv);
          int c_idx = random() % nc;
          int to = -1;
          for ( KNOIter kno(cv); kno; )
            if ( c_idx-- <= 0 ) { to = kno; break; }
          assert( to > 0 );
          mp->gang_move = false;
          mp->from = from;
          mp->to = to;
          mp->candidate = nd_from;
          mp->copy_from = true;
        }
        break;
      default:
        assert( false );
      }

      // Compute the change in cost for this move.
      // Negative numbers are good, the more negative the better.
      //
      const double score = m->move_propose(mp);
      i++;
      if ( !mp->feasible ) continue;

      if ( num_k_exact )
        {
          int num_emptied = 0;
          for ( int kno = 0; kno < num_kernels; kno++ )
            if ( m->kernel_info[kno].nodes.size() &&
                 m->kernel_info[kno].nodes.size() == mp->k_remove[kno].size() )
              num_emptied++;
          if ( num_emptied ) continue;
        }

      opt_proposals++;
      if ( !ik_relax_to_go ) cycle_i++;

      const double et_next = et_curr + score;
      assert( et_next > 0 );

      // Compute how many nodes can be removed from kernels other
      // than from.
      //
      const double et_rat = et_next / et_curr;
      const double epsilon = 1e-5;
      const double f = cycle_i * cycle_len_inv;

      const double thresh = cycle_i >= cycle_len ? 1 : pow(f,0.2);
      const bool accept = score < -epsilon || thresh * et_rat < drand48();

      if ( !accept ) impr_distance++; else impr_distance = 0;
      if ( cycle_i > cycle_len && impr_distance > no_impr_limit )
        { cycle_i = 0; impr_distance = 0; cycle_num++; }

      const bool new_low = et_best == 0 || et_next + epsilon < et_best;

      if ( new_low )
        {
          et_best = et_next;
          assert( accept );
        }

      if ( accept )
        {
          opt_accepts++;
          m->move_accept(mp);
          mp_accepted = mp;
          et_curr = et_next;
        }

      if ( new_low )
        {
          int new_ik = 0;
          double ts_insn = 0, ts_data = 0, ts_latency = 0;
          int num_active = 0;
          for ( int kno=0; kno<num_kernels; kno++ )
            {
              Chemora_CG_Kernel_Info* const ki = &m->kernel_info[kno];
              if ( !ki->should_run ) continue;
              num_active++;
              new_ik += ki->etc.ik_ld + ki->etc.ik_st;
              ts_insn += ki->etc.ts_insn;
              ts_data += ki->etc.ts_data;
              ts_latency += ki->etc.ts_latency;
            }

          if ( !table_header_printed )
          printf
            ("%2s %2s -> %2s  %2s %3s  %2s %4s %3s  %6s %6s %6s   %6s  %5s\n",
             "Ty", "Fr", "To",  "NK", "IK",
             "Cy", "P#", "Th",
             "Issue", "Data", "Lat", "ET", "Diff"
             );
          table_header_printed = true;
          printf
            (" %c %2d -> %2d  %2d %3d  %2d %4d %3.0f  "
             "%6.2f %6.2f %6.2f = %6.2f  %+5.2f\n",
             move_type == MT_New_Kernel ? 'N' :
             move_type == MT_Move ? 'M' :
             move_type == MT_IK_Relax ? 'R' : 'X',
             mp->from, mp->to,  num_active, new_ik,
             cycle_num, cycle_i, thresh*100,
             ts_insn, ts_data, ts_latency, et_next, score
             );
          assert( accept );
        }

      if ( accept ) swap(mp,mp_free);
      if ( new_low ) best.quick_copy(m);

    }

  const double elapsed_time_s = time_wall_fp() - time_start;
  opt_wall_time_s += elapsed_time_s;

  printf
    ("Optimization: Proposal settings: %d cyc * %.1f * min(nnodes,%d)\n",
     opt_opt_num_cycles, opt_opt_props_per_node_per_cycle,
     opt_opt_nodes_max);

  printf
    ("            : %.3f s for %d props, %d accepts (%%%.2f accept rate)\n",
     elapsed_time_s, opt_proposals, opt_accepts,
     100.0 * opt_accepts / max(1,opt_proposals));
  printf
    ("            : %.1f props/s,  %.1f accepts/s, for %d nodes.\n",
     opt_proposals / elapsed_time_s,
     opt_accepts / elapsed_time_s, vnodes);

  m->compute_cost(mp_accepted);
  m->regenerate(&best);
  m->compute_cost();

  delete m_1k;

  return m->emit_code();
}

void
Chemora_CG_Calc_Mapping::quick_copy(Chemora_CG_Calc_Mapping *m)
{
  assignment_vector = m->assignment_vector;
  c = m->c;
  num_kernels = m->num_kernels;
  mapping_nickname = m->mapping_nickname;
}

void
Chemora_CG_Calc_Mapping::regenerate(Chemora_CG_Calc_Mapping *m)
{
  init(m->c, m->num_kernels, m->mapping_nickname);
  assignment_vector = m->assignment_vector;
  for ( NIter nd(c->nodes); nd; )
    {
      if ( !nd->visited ) continue;
      int kno = 0;
      const A_Vector_Type av = assignment_vector[nd->idx];
      for ( A_Vector_Type avi = av; avi; )
        {
          const int tz = ctz(avi);
          kno += tz;
          assert( kno < num_kernels );
          Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
          assert( av & A_Vector_Type(1) << kno );
          ki->nodes += nd;
          avi >>= ( tz + 1 );
          kno++;
        }
    }

  for ( int kno=0; kno<num_kernels; kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->nodes.size() ) continue;
      ki->should_run = true;
    }

  for ( NIter nd(c->nodes); nd; )
    {
      if ( !nd->visited ) continue;
      ik_update(nd);
    }
  for ( NIter nd(c->nodes); nd; )
    {
      if ( !nd->visited ) continue;
      reduction_update(nd);
    }

  verify(NULL,true);
}

string
Chemora_CG_Calc_Mapping::emit_code()
{
  emit_clike = false;

  CaCUDALib_GPU_Info& gi = *c->gpu_info;
  CaCUDA_Kernel_Launch_Parameters& klp = modd->mi->klp;

  string dir = c->temporary_files_dir_get();

  string path_base =
    c->temporary_file_path_prefix_get()
    + "-" + mapping_nickname
    + "-" + to_string(config_version);

  string ptx_path = path_base + ".ptx";
  FILE* const ptx_fh = fopen(ptx_path.c_str(),"w");
  assert( ptx_fh );
  emit_ptx_fh = ptx_fh;
  ptx_indent = 0;
  ptx_line_label_cnt = 0;
  ptx_in_body = false;
  bzero(ptx_rr_next,sizeof(ptx_rr_next));
  ptx_emit_insns_pending = 0;

  c->mi->constant_gfs.clear();

  ptx_lines
    ("//",
     "// Generated by Chemora, part of the Cactus Framework.",
     "//",
     pstringf("// Module Key: %s", c->mi->module_key.c_str()),
     pstringf
     ("// Config Version: %d   Mapping Nickname %s",
      config_version, mapping_nickname.c_str()),
     pstringf("// For GPU-local grid %d x %d x %d",
              klp.cagh_ni, klp.cagh_nj, klp.cagh_nk),
     "//",
     pstringf
     (".version %s",
      gi.cc_major > 5 ? "5.0" : gi.cc_major > 3 ? "4.3" : "4.0"),
     NULL);

  ptx_linef(".target sm_%d%d",gi.cc_major,gi.cc_minor);

  ptx_line(".address_size 64");

  modd->m = this;

  //
  // Emit code for library functions.
  //
  map<Node_Operation,bool> seen_funcs;
  ptx_lib_start(dir);
  for ( NIter nd(c->nodes); nd; )
    {
      if ( !nd->visited ) continue;
      if ( seen_funcs[nd->no] ) continue;
      switch( nd->no ){
      case NO_Sin: ptx_lib_entry_insert("sin",1,c->real_type); break;
      case NO_Cos: ptx_lib_entry_insert("cos",1,c->real_type); break;
      case NO_Power: ptx_lib_entry_insert("pow",2,c->real_type); break;
      default: continue;
      }
      seen_funcs[nd->no] = true;
    }
  ptx_lib_emit();

  //
  // Allocate storage for inter-kernel data and emit pointers.
  //

  const int gf_size_elts = klp.cagh_ni * klp.cagh_nj * klp.cagh_nk;
  const int gf_size_bytes = gf_size_elts * sizeof( CCTK_REAL );
  modd->gf_size_bytes = gf_size_bytes;

  map<string,int> ik_names;

  Nodes ik_nds[num_kernels];

  for ( NIter nd(c->nodes); nd; )
    {
      Mapping_Node* const md = mapping_node_get(nd);

      nd->implied_assignment_kno = -1;
      nd->ik_device_addr = NULL;
      md->ik_st_emitted = false;

      if ( !md->ik_needed || md->ik_needed == IK_follower ) continue;

      string ik_name = ik_make(nd);
      if ( ik_names[ik_name]++ ) continue;

      ik_nds[md->ik_kno] += nd;
    }

  pVector<CUdeviceptr> free_list[num_kernels+1];
  size_t free_list_bytes[num_kernels+1];

  pVector<CUdeviceptr> fl_contents = ct->gpu_alloc_addrs;

  size_t ik_allocated_before = 0;
  int ik_allocated_undersized_cnt = 0;

  while ( CUdeviceptr d_addr = fl_contents.pop() )
    {
      CUdeviceptr pbase;
      size_t psize;
      CE( cuMemGetAddressRange(&pbase, &psize, d_addr) );
      ik_allocated_before += psize;
      if ( psize >= gf_size_bytes ) free_list[0] += d_addr;
      else                          ik_allocated_undersized_cnt++;
    }

  size_t ik_allocated_here = 0;
  size_t ik_used = 0;
  size_t ik_used_max = 0;

  for ( int kno=0; kno<=num_kernels; kno++ ) free_list_bytes[kno] = 0;

  for ( int kno=0; kno<num_kernels; kno++ )
    {
      for ( NIter nd(ik_nds[kno]); nd; )
        {
          if ( free_list[kno].size() == 0 )
            {
              CUdeviceptr ik_storage_d;
              CE( cuMemAlloc( &ik_storage_d, gf_size_bytes ) );
              CE( cuMemsetD8( ik_storage_d, 0xaa, gf_size_bytes ) );
              ik_allocated_here += gf_size_bytes;
              ct->gpu_alloc_addrs += ik_storage_d;
              free_list[kno] += ik_storage_d;
            }
          CUdeviceptr ik_storage_d = free_list[kno].pop();
          nd->ik_device_addr = (void*)ik_storage_d;

          const A_Vector_Type cv = ik_consumers_vector_get(nd);
          int free_kno = 
            ( nd->predecessor || nd->successor )
            ? num_kernels : min( num_kernels, fl1(cv) );
          free_list[free_kno] += ik_storage_d;
          free_list_bytes[free_kno] += gf_size_bytes;
          ik_used += gf_size_bytes;
          assert( free_kno > kno );
        }
      set_max(ik_used_max,ik_used);
      ik_used -= free_list_bytes[kno+1];
      free_list[kno+1] += free_list[kno];
    }

  assert(ik_used == 0);

  assert( free_list[num_kernels].size() + ik_allocated_undersized_cnt
          == ct->gpu_alloc_addrs.size() );

  printf(" -- Used %zd MiB of storage here, total %zd MiB.\n",
         ik_used_max >> 20,
         ( ik_allocated_before + ik_allocated_here ) >> 20 );

  // Emit code for each kernel.
  //
  for ( int kno = 0; kno < num_kernels; kno++ )
    emit_code_kernel(kno);

  c->mi->function_info_reset();
  for ( int kno = 0; kno < num_kernels; kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->should_run ) continue;
      assert( !ki->fi->copied );
      c->mi->function_info_insert( ki->fi );
    }

  fclose(ptx_fh);

  printf("\nAnother Code Generation Report\n%s\n",report_cg2_ptr->body_get());

  return path_base;
}

string
Chemora_CG_Calc_Mapping::emit_code_clike()
{
  emit_clike = true;

  CaCUDA_Kernel_Launch_Parameters& klp = modd->mi->klp;

  string path_base = c->temporary_file_path_prefix_get()
    + "-" + c->calc_name
    + "-" + mapping_nickname
    + "-" + to_string(config_version);
  string cli_path = path_base + "-acc.cc";
  FILE* const cli_fh = fopen(cli_path.c_str(),"w");
  assert( cli_fh );
  emit_ptx_fh = cli_fh;
  ptx_indent = 0;
  ptx_line_label_cnt = 0;
  ptx_in_body = false;
  bzero(ptx_rr_next,sizeof(ptx_rr_next));
  ptx_emit_insns_pending = 0;

  const int gf_size_elts = klp.cagh_ni * klp.cagh_nj * klp.cagh_nk;
  const int gf_size_bytes = gf_size_elts * sizeof( CCTK_REAL );
  modd->gf_size_bytes = gf_size_bytes;

  c->mi->constant_gfs.clear();

  ptx_lines
    ("//",
     "// Generated by Chemora, part of the Cactus Framework.",
     "//",
     pstringf("// Module Key: %s", c->mi->module_key.c_str()),
     pstringf
     ("// Config Version: %d   Mapping Nickname %s",
      config_version, mapping_nickname.c_str()),
     pstringf("// For GPU-local grid %d x %d x %d",
              klp.cagh_ni, klp.cagh_nj, klp.cagh_nk),
     "//",
     NULL);

  modd->m = this;

  ptx_indent_push_abs(0);
  ptx_lines( "#include <cstddef>",
             "#include <cstdint>",
             "#include <algorithm>",
             "#include <math.h>",
             "using namespace std;",
             NULL );
  ptx_indent_pop();

  // Emit code for each kernel.
  //
  for ( int kno = 0; kno < num_kernels; kno++ )
    emit_code_clike_kernel(kno);

  c->mi->function_info_reset();
  for ( int kno = 0; kno < num_kernels; kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->should_run ) continue;
      assert( !ki->fi->copied );
      c->mi->function_info_insert( ki->fi );
    }

  fclose(cli_fh);

  return path_base;
}

static const int dest_ignore = -1;

void
Chemora_CG_Calc_Mapping::sched_pos_max_update_or_verify
(char option, Nodes& cnodes)
{
  const bool filtered = option == 'f';
  const bool update = option == 'u' || filtered;
  assert( update || option == 'v' );
  if ( option == 'u' ) for ( NIter nd(cnodes); nd; ) nd->ss_root_idx = nd;

  for ( NIter nd(cnodes); nd; )
    {
      if ( filtered && !nd->emittable ) continue;
      int pos_max = dest_ignore;
      for ( NIter dst(nd->dests); dst; )
        if ( dst->emittable ) set_max(pos_max,dst->ss_root_idx);
      if ( update ) nd->ss_dest_pos_max = pos_max;
      else          assert( nd->ss_dest_pos_max == pos_max );
    }
}

void
Chemora_CG_Calc_Mapping::sched_pos_live_verify(Nodes& rnodes)
{
  int max_regs = 0;
  double avg_regs = 0;
  sched_pos_live_update_or_verify('v',rnodes,max_regs,avg_regs);
}

void
Chemora_CG_Calc_Mapping::sched_pos_live_update_or_verify
(char option, Nodes& rnodes, int& max_regs, double& avg_regs)
{
  sched_pos_max_update_or_verify(option, rnodes);

  const bool update = option == 'u';
  max_regs = 0;
  int sum_regs = 0;
  Node_IMMap live_regs;
  for ( NIter nd(rnodes); nd; )
    {
      set_max(max_regs,live_regs);
      sum_regs += live_regs;
      if ( update ) nd->ss_num_live_regs = live_regs;
      else          assert( nd->ss_num_live_regs == live_regs );
      while ( Node* const ln = live_regs.peek() )
        if ( ln->ss_dest_pos_max <= nd ) live_regs.pop(); else break;
      if ( nd->ss_dest_pos_max > nd ) live_regs.insert(-nd->ss_dest_pos_max,nd);
    }
  avg_regs = double(sum_regs)/int(rnodes);
  assert( live_regs.empty() );
}

Nodes
Chemora_CG_Calc_Mapping::sched_set_examine(Nodes sched_set_rootsp)
{
  Node_IMMap ssr_sorted;
  for ( NIter nd(sched_set_rootsp); nd; ) ssr_sorted.insert(nd->idx,nd);
  Nodes sched_set_roots = ssr_sorted;

  const bool overf = sched_set_roots > 8 * sizeof(Node().sched_set_vector);
  if ( overf ) return sched_set_roots;

  const int marker = c->next_marker++;

  Nodes roots_uniq;
  Nodes stack;

  for ( NIter nd(sched_set_roots); nd; )
    {
      assert( nd->emittable );

      // Skip duplicated source.
      if ( nd->marker_set(marker) ) continue;

      nd->sched_set_vector = RV(roots_uniq);
      roots_uniq += nd;

      int du = 0;
      for ( NIter dst(nd->dests); dst; )
        if ( dst->emittable && !dst->emitted ) du++;
      if ( !du ) stack += nd;
      nd->ss_dests_unemitted = du;
    }

  assert( stack.size() );
  const int nroots = roots_uniq;
  const R_Vector_Type ss_mask = RV(nroots) - 1;

  Nodes sched_set_plus;
  Nodes sched_set;
  ENodes live_outs(1);

  while ( Node* const nd = stack.pop() )
    {
      assert( !nd->emitted );
      assert( nd->emittable );
      assert( nd->sched_set_vector & ss_mask );
      assert( ! ( nd->sched_set_vector & ~ss_mask ) );
      sched_set += nd;
      for ( NIter src(nd->sources); src; )
        {
          if ( !src->emittable ) continue;
          if ( !src->marker_set(marker) )
            {
              src->sched_set_vector = 0;
              sched_set_plus += src;
              int du = 0;
              for ( NIter dst(src->dests); dst; )
                if ( dst->emittable )
                  { if ( dst->emitted ) live_outs += src; else du++; }
              src->ss_dests_unemitted = du;
            }
          assert( src->ss_dests_unemitted > 0 );
          src->sched_set_vector |= nd->sched_set_vector;
          src->ss_dests_unemitted--;
          if ( src->ss_dests_unemitted == 0 ) stack += src;
        }
    }

  Nodes live_ins;
  for ( NIter nd(sched_set_plus); nd; )
    if ( nd->ss_dests_unemitted ) live_ins += nd;

  int sz[nroots]; BZERO(sz);
  int uq[nroots]; BZERO(uq);
  int li[nroots]; BZERO(li);
  int lo[nroots]; BZERO(lo);
  R_Vector_Type mix_vector[nroots]; BZERO(mix_vector);

  for ( NIter nd(live_ins); nd; )
    for ( int i=0; i<nroots; i++ ) if ( nd->sched_set_vector & RV(i) ) li[i]++;
  for ( NIter nd(live_outs); nd; )
    for ( int i=0; i<nroots; i++ ) if ( nd->sched_set_vector & RV(i) ) lo[i]++;
  for ( NIter nd(sched_set); nd; )
    {
      const R_Vector_Type ssv = nd->sched_set_vector;
      for ( RVIter i(nd->sched_set_vector); i; )
        {
          mix_vector[i] |= ssv;
          sz[i]++;
        }
      if ( pop(ssv) == 1 ) uq[ctz(ssv)]++;
    }

  map<R_Vector_Type,Nodes> mv_sets;
  pVector<R_Vector_Type> mv_uniq;

  for ( int i=0; i<nroots; i++ ) mv_sets[mix_vector[i]] += roots_uniq[i];

  Node_U64MMap emit_order;
  int dl_min = c->nodes.size();
  int dl_max = -dl_min;
  int max_pop = 0;

  for ( int i=0; i<nroots; i++ )
    {
      const int delta_l = lo[i] - li[i];
      set_min(dl_min,delta_l);
      set_max(dl_max,delta_l);
      set_max(max_pop,pop(mix_vector[i]));
    }

  const bool use_mix = mv_sets.size() > 1 && max_pop > ( dl_max - dl_min );

  for ( int i=0; i<nroots; i++ )
    {
      roots_uniq[i]->ss_root_idx = i;
      u_int32_t ordering = use_mix ? mix_vector[i] : 10000 + lo[i]-li[i] ;
      emit_order.insert(ordering,roots_uniq[i]);
    }
  Nodes eo(emit_order);

  pString comment;
  for ( NIter nd(eo); nd; )
    {
      const int i = nd->ss_root_idx;
      comment.sprintf("// %4d  %3d -> %3d  %4d  %4d  %#18zx\n",
                      nd->idx, li[i], lo[i], sz[i], uq[i], mix_vector[i]);
    }

  if ( 0 ) ptx_line(comment.s);

  return eo;
}


///
 /// Halo 2
///
//  Code for loading shared memory for Plus and Block style buffer groups.
//

void
Chemora_CG_Calc_Mapping::halo2_setup_and_emit_arrays()
{
  Chemora_Halo2_Data& h2 = ek.halo2_data = Chemora_Halo2_Data();
  pVectorI<Work_Group>& work_groups = h2.work_groups;
  Buffer_Groups& buffer_groups = ek.etc->buffer_groups;
  Chemora_CG_Kernel_Info* const ki = &kernel_info[ek.kno];
  const int tile_f = ek.tile_f;
  const Tile tile = ek.etc->tile;
  const int block_size = ek.block_size;

  if ( !c->opt_halo2 )
    {
      for ( BGIter bg(buffer_groups); bg; )
        bg->interior =
          bg->style == BS_Block ? BSI_Block :
          bg->style == BS_Plus ? BSI_Self : BSI_None;

      h2.have_t0_rounds = false;
      return;
    }

  assert( c->opt_halo2 > 0 && c->opt_halo2 <= 3 );

  int wg_reg_idx_next = 0;

  class WG_Setting {
  public:
    WG_Setting(Grid_Functions gfsp):gfs(gfsp){init();};
    WG_Setting(Grid_Function *gf):gfs(gf){init();};
    WG_Setting(){init();};
    void init() { f_rnd_start = 0; num_f_rnds = 0; }
    int f_rnd_start, f_per_rnd, num_f_rnds;
    Grid_Functions gfs;
  };

  auto rounds_get = [&](WG_Setting& wgs)
    { return wgs.gfs.size() * wgs.num_f_rnds; };

  auto wg_setting_new = [&](Grid_Functions& gfs)
    {
      WG_Setting wgs(gfs);
      assert(gfs.size());
      Buffer_Group* const bg = gfs[0]->bg;
      const int sten_x_len = bg->stncl.x();
      const int f_per_rnd_max = warp_sz / sten_x_len;
      wgs.num_f_rnds = divup( tile_f, f_per_rnd_max );
      wgs.f_per_rnd = divup( tile_f, wgs.num_f_rnds );
      return wgs;
    };


  int h2_irreg_grp_thds_safe = 0;

  Buffer_Groups bgs_halo_f;          // Plus-style bg with f-axis stencil.
  Buffer_Groups bgs_blocks4;         // Block-style buffer groups.
  pVector<WG_Setting> wg_settings;   // For bg requiring irregular accesses.

  for ( BGIter bg(buffer_groups); bg; )
    {
      const Buffer_Style style = bg->style;
      const int sten_x_len = bg->stncl.x();
      const bool have_sten_x = sten_x_len;
      const bool have_sten_f = bg->stncl_f;

      if ( style != BS_Plus && style != BS_Block )
        { bg->interior = BSI_None;   continue; }

      if ( style == BS_Block )
        { bg->interior = BSI_T0;  bgs_blocks4 += bg;  continue; }

      assert( style == BS_Plus );

      bg->interior = BSI_Self;

      // Check whether GP-relative offsets should be used.
      //
      if ( have_sten_f ) bgs_halo_f += bg;
      if ( !have_sten_x ) continue;

      set_max(h2_irreg_grp_thds_safe, bg->stncl.x() * tile_f);
      wg_settings += wg_setting_new(bg->gfs);
    }


  Buffer_Groups bgs_t0 = bgs_blocks4 + bgs_halo_f;
  h2.have_t0_rounds = bgs_t0;
  int bl_xn_max = 0;
  int bl_xp_max = 0;
  h2.x_width_max = tile.x;
  for ( BGIter bg(bgs_t0); bg; )
    if ( bg->style == BS_Block || ek.etc->need_corners_get(bg) )
      {
        set_max(h2.x_width_max,tile.x+bg->stncl.x());
        set_max(bl_xn_max,-bg->stncl.imin);
        set_max(bl_xp_max,bg->stncl.imax);
      }

  pVector<WG_Round> wg_rounds_b, wg_rounds_f;
  const bool h2_split_work = h2.x_width_max > block_size;
  const int h2_num_pieces = h2_split_work ? 2 : 1;
  const int h2_delta_x = divup(h2.x_width_max,h2_num_pieces);

  for ( BGIter bg(bgs_blocks4); bg; )
    for ( auto gf : bg->gfs )
      for ( int f = bg->stncl_fmin; f < tile_f + bg->stncl_fmax; f++ )
        for ( int i=0; i<h2_num_pieces; i++ )
          wg_rounds_b += WG_Round( gf, f, i );

  for ( BGIter bg(bgs_halo_f); bg; )
    for ( auto gf : bg->gfs )
      for ( int fs = bg->stncl_fmin; fs < bg->stncl_fmax; fs++ )
        for ( int i=0; i<h2_num_pieces; i++ )
          wg_rounds_f += WG_Round( gf, fs<0?fs:tile_f+fs, i );

  // Upper bound on the number of work groups. Used for tuning.
  const int wg_limit = 500;

  const int h2_num_irreg_grps_whole_max = block_size >> warp_lg;
  const int h2_num_irreg_grps_max =
    min(wg_limit,h2_num_irreg_grps_whole_max)
    + bool( ( block_size & ( warp_sz - 1 ) ) >= h2_irreg_grp_thds_safe );

  const int t0_grp_nwps_whole = divup(h2_delta_x,warp_sz);

  auto num_t0_max_get =
    [&](int irreg_used)
    {
      const int nused = rndup(irreg_used,t0_grp_nwps_whole) * warp_sz;
      const int thds_avail = max(0, block_size - nused );
      const int num_t0_grps_whole =
        ( thds_avail >> warp_lg ) / t0_grp_nwps_whole;
      const int leftover_thds =
        thds_avail - num_t0_grps_whole * t0_grp_nwps_whole * warp_sz;
      assert( leftover_thds >= 0 );
      const int num_t0 = num_t0_grps_whole + ( h2_delta_x <= leftover_thds );
      return min( wg_limit - irreg_used, num_t0 );
    };

  if ( wg_settings.size() )
    {
      // Split irregular work groups to reduce or eliminate load
      // imbalance.

      assert( h2_delta_x );

      // Find the amount of work contributed by block and halo f faces.
      //
      const int rounds_t0 = wg_rounds_b.size() + wg_rounds_f.size();

      // Find an acceptable maximum number of irregular work
      // groups. (Additional registers would be needed if this maximum
      // were exceeded.)
      //
      const int h2_num_irreg_wg_max =
        rndup( wg_settings.size(), h2_num_irreg_grps_max );

      while ( true )
        {
          // Number of additional irregular work groups that can be allocated.
          //
          const int avail_wg = h2_num_irreg_wg_max - wg_settings.size();

          // The number of t0 work groups that can be allocated. (Not
          // including those overlapping already allocated irregular
          // work groups.)
          //
          const int avail_t0 = num_t0_max_get( wg_settings.size() );

          // Sort irregular work groups so that the one with the most
          // rounds (the one taking the most time) is first.
          //
          sort( wg_settings.begin(), wg_settings.end(),
                [&](WG_Setting a, WG_Setting b)
                { return rounds_get(a) >= rounds_get(b); } );

          const int rounds_max = rounds_get(wg_settings[0]);
          if ( rounds_max == 1 ) break;
          assert( rounds_max > 0 );

          // Compute the number of t0 rounds that can be performed
          // in the time it takes to complete irregular work rounds.
          //
          int rounds_hidable = avail_t0 * rounds_max;
          for ( int i=0; i<wg_settings.size(); i+=t0_grp_nwps_whole )
            rounds_hidable += rounds_max - rounds_get(wg_settings[i]);

          // If true, irregular groups are on the critical path.
          //
          const bool protruding = rounds_t0 < rounds_hidable;

          const bool flatten_protrusion =
            c->opt_halo2 == 2 || ( c->opt_halo2 == 1 && protruding );

          if ( flatten_protrusion )
            {
              // An irregular work group is protruding, meaning it will
              // finish after (based on the number of rounds) any other
              // work group, including those with a mix of irregular and
              // t0 groups.
              //
              // Move work in protruding work group (and other work
              // groups representing the same buffer group as the
              // protruding work group) to a newly allocated work group.
              //
              if ( !avail_wg ) break;
              const int ngfs = wg_settings[0].gfs.size();
              Buffer_Group* const bg = wg_settings[0].gfs[0]->bg;
              const int f_rnd_start =  wg_settings[0].f_rnd_start;
              const int num_f_rnds = wg_settings[0].num_f_rnds;

              // For the moment, only attempt to move work based on a
              // per-grid-functions basis. In principle one can also
              // split an individual grid function to multiple work groups.
              if ( ngfs == 1 )
                {
                  if ( num_f_rnds == 1 ) break;
                  wg_settings += wg_setting_new(wg_settings[0].gfs);
                  WG_Setting& wg_old = wg_settings[0];
                  WG_Setting& wg_new = wg_settings.back();
                  wg_old.num_f_rnds = num_f_rnds / 2;
                  wg_new.f_rnd_start = f_rnd_start + wg_old.num_f_rnds;
                  wg_new.num_f_rnds = num_f_rnds - wg_old.num_f_rnds;
                  continue;
                }

              wg_settings += wg_setting_new(bg->gfs);
              pVectorI<WG_Setting> bg_wgs;
              for ( auto& wgs : wg_settings )
                if ( wgs.gfs[0]->bg == bg )
                  {
                    wgs.gfs.clear();  bg_wgs += &wgs;
                    assert( wgs.f_rnd_start == 0 );
                  }
              const int n = bg_wgs.size();
              assert( bg->gfs.size() >= n );
              for ( int i=0; i<bg->gfs.size(); i++ )
                bg_wgs[i%n]->gfs += bg->gfs[i];
            }
          else
            {
              break;
              // All irregular work groups are submerged, meaning that
              // all irregular rounds groups will finish at the same
              // time or before (based on the number of rounds) the last
              // t0 work round.

              // There's no imbalance to fix.
              if ( t0_grp_nwps_whole == 1 ) break;

              // Under construction.

            }
        }
    }

  const int h2_num_irreg_sizes = h2.num_irreg_sizes = wg_settings.size();
  const int h2_num_irreg_grps = min( h2_num_irreg_grps_max, h2_num_irreg_sizes);

  // Divide into warp-sized work groups.
  h2.num_reg_sets = divup(h2_num_irreg_sizes,h2_num_irreg_grps_max);

  h2.gl_base_t0_idx = h2.num_reg_sets;
  const int h2_num_base_regs = h2.num_reg_sets + h2.have_t0_rounds;

  const int h2_irreg_grp_nwps = 1;
  h2.irreg_grp_num_thds = min(block_size,warp_sz * h2_irreg_grp_nwps);
  h2.irreg_part_thd_max = h2.irreg_grp_num_thds * h2_num_irreg_grps;

  auto h2_wg_new = [&](int size_min, int size_prefer)
    {
      const int tidi = work_groups ? work_groups.back()->tid_gnext : 0;
      assert( tidi + size_min <= block_size );
      Work_Group* const wg = new Work_Group();
      wg->idx = work_groups.size();
      wg->tid_gstart = tidi;
      wg->tid_gnext = min(block_size, wg->tid_gstart + size_prefer);
      work_groups += wg;
      return wg;
    };

  h2.shared_offsets_array_name =
    pstringf("h2_shared_offsets_%s", ki->function_name.c_str());
  pStringF h2_shared_offsets
    (".global .s16 %s[] =\n{ ", h2.shared_offsets_array_name.c_str());
  h2.global_offsets_array_name =
    pstringf("h2_global_offsets_%s", ki->function_name.c_str());
  pStringF h2_global_offsets
    (".global .s32 %s[] =\n{ ", h2.global_offsets_array_name.c_str());

  // Construct offset arrays and work rounds for irregular work groups.
  //
  for ( int idx = 0;  idx < wg_settings.size();  idx++ )
    {
      WG_Setting* const wgs = &wg_settings[idx];
      Grid_Functions& gfs = wgs->gfs;
      assert ( !gfs.empty() );
      Buffer_Group* const bg = gfs[0]->bg;
      CStncl &stncl = bg->stncl;
      const int ngfs = gfs;
      const int irreg_x_len = stncl.x();

      const int reg_set = idx / h2_num_irreg_grps;
      const int wg_idx = idx % h2_num_irreg_grps;
      if ( !reg_set ) h2_wg_new(irreg_x_len,h2.irreg_grp_num_thds);

      Work_Group* const wg = work_groups[wg_idx];
      const int num_thds_max = wg->tid_gnext - wg->tid_gstart;
      assert( num_thds_max >= irreg_x_len );

      int tidi = wg->tid_gstart;

      auto eol = [&]()
        { if ( ( tidi & 0x7 ) == 0 ) {
            h2_shared_offsets += "\n  "; h2_global_offsets += "\n  ";}
          else if ( !( tidi & 0x3 ) ) {
            h2_shared_offsets += "    ";   h2_global_offsets += "    ";}
        };

      const int num_f_full = wgs->num_f_rnds * wgs->f_per_rnd;
      const int f_start = wgs->f_rnd_start * wgs->f_per_rnd;
      const int f_stop = min(tile_f, f_start + num_f_full);
      const int num_f = f_stop - f_start;
      assert( f_start < tile_f );
      assert( f_stop > 0 );
      const int f_per_round_max = num_thds_max / irreg_x_len;
      const int num_rounds = divup(num_f,f_per_round_max);
      assert( num_rounds == wgs->num_f_rnds || num_thds_max < warp_sz );
      const int f_per_round = divup(num_f,num_rounds);
      const int thd_per_round = f_per_round * irreg_x_len;
      const int tid_stop = wg->tid_gstart + thd_per_round;

      assert( tid_stop <= wg->tid_gnext );

      pVector<WG_Round> offsets;

      for ( int f = f_start;  f < f_stop;  f++ )
        for ( int x = stncl.imin; x < tile.x + stncl.imax; x++ )
          {
            if ( x == 0 ){ x = tile.x - 1;  continue; }

            const int sh_idx =
              -stncl.imin + x + ( f - bg->stncl_fmin ) * bg->sc_f_stride;
            const int gl_offset = x + f * ek.gl_f_stride;
            const int sm_offset = sh_idx * ek.assumed_elt_size;
            offsets += WG_Round(gl_offset,sm_offset,f);
          }

      assert( offsets.size() == num_f * irreg_x_len ) ;

      for ( int i=0; i<thd_per_round; i++ )
        {
          tidi++;
          h2_global_offsets.sprintf("%7d,",offsets[i].gl_offset_sc);
          h2_shared_offsets.sprintf("%7d,",offsets[i].sm_offset_sc);
          eol();
        }
      assert( tidi <= wg->tid_gnext );
      for ( ; tidi++ < wg->tid_gnext; )
        {
          h2_global_offsets.sprintf("%7d,", -1);
          h2_shared_offsets.sprintf("%7d,", -1);
          eol();
        }

      pVector<WG_Round> wg_rounds;

      const int overlap = ( num_rounds * f_per_round - num_f ) * irreg_x_len;
      WG_Round wgr_ref = offsets[0];

      wg_rounds += WG_Round(0,0,0);

      for ( int i=1; i<num_rounds; i++ )
        {
          const int idxi = i * thd_per_round - overlap;
          const int dgl =
            ek.assumed_elt_size
            * ( offsets[idxi].gl_offset_sc - wgr_ref.gl_offset_sc );
          const int dsm = offsets[idxi].sm_offset_sc - wgr_ref.sm_offset_sc;
          wg_rounds += WG_Round(dgl,dsm,i);
        }

      const int n_mem_ops = num_rounds * ngfs;

      pStringF ending_comment
        ("// set %d, wg %d, bg %d   %d wp%s for %2d gf%s "
         "rnd %d over %2d rnd%s, %2d op%s. tid %4d\n\n  ",
         reg_set, wg->idx, bg->idx,
         h2_irreg_grp_nwps, plural(h2_irreg_grp_nwps), ngfs, plural(ngfs),
         wgs->f_rnd_start,
         num_rounds, plural(num_rounds), n_mem_ops, plural(n_mem_ops),
         wg->tid_gstart);

      h2_shared_offsets += ending_comment;
      h2_global_offsets += ending_comment;

      for ( int i=thd_per_round; i < thd_per_round * num_rounds; i++ )
        {
          WG_Round off_targ = offsets[i-overlap];
          WG_Round off_base = offsets[i%thd_per_round];
          WG_Round wgr = wg_rounds[i/thd_per_round];
          const int dsm = off_base.sm_offset_sc + wgr.sm_offset_sc;
          const int dgl =
            off_base.gl_offset_sc + wgr.gl_offset_sc / ek.assumed_elt_size;
          assert( dsm == off_targ.sm_offset_sc );
          assert( dgl == off_targ.gl_offset_sc );
        }

      WG_Round last_offset_all = offsets[offsets.size()-1];
      WG_Round last_offset_thd = offsets[thd_per_round-1];
      WG_Round last_round = wg_rounds[num_rounds-1];
      assert( last_offset_all.sm_offset_sc
              == last_offset_thd.sm_offset_sc + last_round.sm_offset_sc );
      assert( last_offset_all.gl_offset_sc
              == last_offset_thd.gl_offset_sc
              + last_round.gl_offset_sc / ek.assumed_elt_size );

      for ( auto gf : gfs )
        for ( auto wgr : wg_rounds )
          {
            Offsets ref = gf->pattern_info.ref;
            const int ref_adj =
              ref.i + ref.j * ek.cagh_ni + ref.k * ek.gl_z_stride;
            wgr.gf = gf;
            wgr.base_reg_idx = reg_set;
            wgr.reg_idx = wg_reg_idx_next++;
            wgr.sm_offset_sc +=
              bg->shared_array_base_byte
              + gf->bg_sh_var_idx * bg->sc_var_stride_bytes;
            wgr.gl_offset_sc += ek.assumed_elt_size * ref_adj;
            wgr.tid_start = wg->tid_gstart;
            wgr.tid_stop = tid_stop;
            *wg += wgr;
          }
    }

  if ( h2.have_t0_rounds )
    {
      // Assign t0 work rounds to work groups.
      //

      // Determine maximum number of elements that a t0 work group
      // can handle.
      //
      const int x_wid_need = h2_split_work ? h2_delta_x : h2.x_width_max;
      assert( x_wid_need <= block_size );

      // First, initialize additional work groups for the remaining warps.
      //
      const int wg_size_needed = min(block_size,t0_grp_nwps_whole*warp_sz);
      if ( work_groups )
        {
          const int tid_gnext_last = work_groups.back()->tid_gnext;
          const int psize = tid_gnext_last % wg_size_needed;
          if ( tid_gnext_last < block_size && psize )
            h2_wg_new(1,wg_size_needed-psize);
        }

      while ( !work_groups ||
              ( work_groups.size() < wg_limit
                && work_groups.back()->tid_gnext < block_size ) )
        h2_wg_new(1,wg_size_needed);

      pVector<WG_Round> wg_rounds = wg_rounds_b + wg_rounds_f;
      int rnds_max_irr = -1;

      // Group existing work groups into sets that can be used for
      // thread-0-relative work rounds.
      //
      pVector< Work_Group_GP_Set > wg_gp_sets;
      for ( int i = 0; i < work_groups.size(); )
        {
          const int tid_start = work_groups[i]->tid_gstart;
          const int tid_stop = tid_start + x_wid_need;
          Work_Group_GP_Set gps;
          gps.tid_start = tid_start;
          gps.tid_stop = tid_stop;
          int rnds_max = -1;
          while ( i < work_groups.size() )
            {
              Work_Group* const wgi = work_groups[i++];
              gps.work_groups += wgi;
              if ( set_max(rnds_max,int(wgi->wg_rounds.size())) )
                gps.wg_critical = wgi;
              if ( wgi->tid_gnext < tid_stop ) continue;
              wg_gp_sets += gps;
              break;
            }
          set_max(rnds_max_irr,rnds_max);
        }
      assert( wg_gp_sets.size() );

      // Compute maximum number of work rounds in a work group assuming
      // that work groups are evenly distributed.
      //
      const int num_gps = wg_gp_sets.size();
      int num_rounds_irr_eff = 0;
      for ( auto& gpi : wg_gp_sets )
        num_rounds_irr_eff += gpi.wg_critical->wg_rounds.size();
      const int num_rounds_eff = num_rounds_irr_eff + wg_rounds.size();
      const int height_max_expected =
        max( rnds_max_irr, divup( num_rounds_eff, num_gps ) );

      // Find number of work rounds operating on each grid function.
      //
      map< Grid_Function*, int > gf_n;
      for ( auto& wgr : wg_rounds ) gf_n[wgr.gf]++;

      // Sort so that (1) grid functions are bunched together and
      // (2) so that the smaller bunches appear before larger bunches.
      //
      sort(wg_rounds.begin(),wg_rounds.end(),
           [&](const WG_Round& a, const WG_Round& b)
           {
             int dir = gf_n[a.gf] - gf_n[b.gf];
             return dir ? dir < 0 : a.gf != b.gf ? a.gf < b.gf : a.f < b.f;
           });

      // Distribute thread-0-relative work rounds to work groups.
      //
      for ( int i=0; i<wg_rounds.size(); )
        {
          Grid_Function* const gf = wg_rounds[i].gf;
          const Work_Round_Type wr_type = wg_rounds[i].wr_type;

          // Number of work rounds operating on the same gf in wg_rounds.
          const int n_remaining = gf_n[gf];

          pVectorI<Work_Group_GP_Set> viable_sets;
          for ( auto& gpi : wg_gp_sets )
            if ( gpi.wg_critical->wg_rounds.size() < height_max_expected )
              viable_sets += &gpi;

          Work_Group_GP_Set *gps = NULL;

          while ( !gps )
            {
              // First, look for a work group operating on same gf.
              //
              for ( auto gpi : viable_sets )
                if ( gpi->wg_critical->gf_n[wr_type].count(gf) )
                  { gps = gpi; break; }
              if ( gps ) break;

              // Next, look for a work group that this gf can fill.
              // Find the one with the least space.
              //
              int space_max = -1;
              for ( auto gpi : viable_sets )
                {
                  const int n = gpi->wg_critical->wg_rounds.size();
                  if ( n + n_remaining < height_max_expected ) continue;
                  if ( set_max(space_max, n) ) gps = gpi;
                }
              if ( gps ) break;

              // Finally, look for a work group which has been assigned
              // the fewest number of gfs.
              //
              int ngmin = 1 << 30;
              for ( auto& gpi : viable_sets )
                {
                  Work_Group* const wg = gpi->wg_critical;
                  const int ng_here =
                    wg->gf_n[WR_T0_Offsets].size() +
                    wg->gf_n[WR_Irregular_Offsets].size();
                  if ( set_min( ngmin, ng_here ) ) gps = gpi;
                }
              if ( gps ) break;
              assert( false );
            }

          const int amt =
            min( n_remaining,
                 height_max_expected
                 - int(gps->wg_critical->wg_rounds.size()) );
          assert( amt > 0 );
          gf_n[gf] -= amt;
          const int stop = i + amt;
          Buffer_Group* const bg = wg_rounds[i].gf->bg;
          const bool is_block = bg->style == BS_Block;
          for ( ;  i < stop;  i++ )
            {
              WG_Round& wgr = wg_rounds[i];
              assert( wgr.gf == gf );
              const bool excl_corners =
                bg->stncl.x() && ( wgr.f < 0 || wgr.f >= tile_f )
                && !ek.etc->need_corners_get(bg);
              const int x_length = excl_corners ? tile.x : bg->x_length;

              const int lf = 0;
              // Lane 0 thread always writes to first element for row,
              // global address is shifted.
              const int dx = excl_corners ? 0 : bg->stncl.imin;
              const int dx_sh = excl_corners ? -bg->stncl.imin : 0;
              const int df = ( wgr.f - lf );
              const int de = is_block ? bg->stncl_emax : 0;

              const int row_x_start = dx;
              const int row_x_end = dx + x_length;
              const int piece_x_start = row_x_start + wgr.p_num * h2_delta_x;
              const int piece_x_end = min(row_x_end, piece_x_start+h2_delta_x);
              assert( piece_x_start == row_x_start || piece_x_end==row_x_end );
              assert( h2_delta_x < x_length ||
                      piece_x_start == row_x_start && piece_x_end==row_x_end );
              const int piece_x_len = piece_x_end - piece_x_start;
              assert( h2_split_work || piece_x_len == x_length );
              const int dx_gl_piece = piece_x_start;
              const int dx_sh_piece = piece_x_start - dx + dx_sh;
              Offsets ref = gf->pattern_info.ref;
              const int ref_adj =
                ref.i + ref.j * ek.cagh_ni + ref.k * ek.gl_z_stride;

              wgr.gl_offset_sc =
                ( dx_gl_piece + df * ek.gl_f_stride + de * ek.gl_e_stride
                  + ref_adj )
                * ek.assumed_elt_size;

              wgr.sm_offset_sc =
                bg->shared_array_base_byte
                + wgr.gf->bg_sh_var_idx * bg->sc_var_stride_bytes
                + ( dx_sh_piece
                    + ( -bg->stncl_fmin + df ) * bg->sc_f_stride
                    + ( is_block ? -bg->stncl_emin * bg->sc_e_stride : 0 ) )
                * ek.assumed_elt_size;

              wgr.reg_idx = wg_reg_idx_next++;
              wgr.wr_type = WR_T0_Offsets;
              const int tid_stop = gps->tid_start + piece_x_len;
              for ( auto wg : gps->work_groups )
                {
                  wgr.tid_start = wg->tid_gstart;
                  wgr.tid_stop = min(wg->tid_gnext,tid_stop);
                  wgr.base_reg_idx = h2.gl_base_t0_idx;
                  *wg += wgr;
                }
            }
        }
    }

  while ( !work_groups.empty() && work_groups.back()->wg_rounds.empty() )
    work_groups.pop();

  for ( auto wg : work_groups )
    {
      vector<int> ntype(WR_ENUM_SIZE);
      for ( auto& wgr : wg->wg_rounds ) ntype[wgr.wr_type]++;
      const int num_thds = wg->tid_gnext - wg->tid_gstart;
      const int nwps = num_thds >> warp_lg;
      pStringF gp_rel_comment
        ("  // wg %2d using %2d wp%s over (irr,t0) (%2d,%2d) round.  "
         " ugfs (%2zd,%2zd)  "
         " tid %d\n",
         wg->idx, nwps, plural(nwps),
         ntype[WR_Irregular_Offsets], ntype[WR_T0_Offsets],
         wg->gf_n[WR_Irregular_Offsets].size(),
         wg->gf_n[WR_T0_Offsets].size(),
         wg->tid_gstart);
      h2_global_offsets += gp_rel_comment;
      h2_shared_offsets += gp_rel_comment;
    }

  h2_shared_offsets += "0 };\n";
  h2_global_offsets += "0 };\n";
  ptx_line(h2_shared_offsets.s);
  ptx_line(h2_global_offsets.s);

  h2.ptxr_h2.resize(wg_reg_idx_next);
  h2.ptxr_halo2_sh_base.resize(h2_num_base_regs);
  h2.addr_bo_halo2.resize(h2_num_base_regs);
}


void
Chemora_CG_Calc_Mapping::halo2_emit_pre_loop_code()
{
  // Emit code to compute memory addresses and assign predicates
  // needed by the halo2 (shared memory loading) code.

  Chemora_Halo2_Data& h2 = ek.halo2_data;
  pVectorI<Work_Group>& work_groups = h2.work_groups;
  Buffer_Groups& buffer_groups = ek.etc->buffer_groups;
  const int block_size = ek.block_size;

  if ( !c->opt_halo2 ) return;

  if ( h2.num_irreg_sizes )
    {
      // Emit code to load global and shared base address used by
      // irregular rounds.

      ptx_linef("// Loading base registers for irregular accesses.");
      pStringF line_irreg_done("irregular_offsets_loaded");
      const int num_thds_part_iter = h2.num_irreg_sizes * h2.irreg_grp_num_thds;
      const int last_thd = num_thds_part_iter % h2.irreg_part_thd_max;

      if ( h2.irreg_part_thd_max < block_size )
        {
          PTX_RR ptxr_not_us = ptx_make(PT_pred,"setp.ge.s32")
            << ek.ptxr_tid << h2.irreg_part_thd_max;
          ptx_make("bra.uni").predicate(ptxr_not_us) << line_irreg_done << EMIT;
        }

      PTX_RR ptxr_halo2_so_addr =
        ptx_make(PT_s64,"mov.s64") << h2.shared_offsets_array_name;
      PTX_RR ptxr_halo2_go_addr =
        ptx_make(PT_s64,"mov.s64") << h2.global_offsets_array_name;
      const int halo2_so_elt_size = 2;
      const int halo2_go_elt_size = 4;
      PTX_RR ptxr_h2sop_thd_base =
        ptx_make(PT_s64,"mad.wide.s32")
        << ek.ptxr_tid << halo2_so_elt_size << ptxr_halo2_so_addr;
      PTX_RR ptxr_h2gop_thd_base =
        ptx_make(PT_s64,"mad.wide.s32")
        << ek.ptxr_tid << halo2_go_elt_size << ptxr_halo2_go_addr;

      for ( int i=0; i<h2.num_reg_sets; i++ )
        {
          const bool last_iter = i+1 >= h2.num_reg_sets;

          if ( i && last_iter && last_thd && last_thd < block_size )
            {
              PTX_RR ptxr_not_us = ptx_make(PT_pred,"setp.ge.s32")
                << ek.ptxr_tid << last_thd;
              ptx_make("bra.uni").predicate(ptxr_not_us)
                << line_irreg_done << EMIT;
            }

          if ( i )
            {
              ptx_make(ptxr_h2sop_thd_base,"add.s64")
                << ptxr_h2sop_thd_base
                << i * h2.irreg_part_thd_max * halo2_so_elt_size << EMIT;
              ptx_make(ptxr_h2gop_thd_base,"add.s64")
                << ptxr_h2gop_thd_base
                << i * h2.irreg_part_thd_max * halo2_go_elt_size << EMIT;
            }
          PTX_RR ptxr_halo2_so_chars_h =
            ptx_make(PT_s16,"ld.global.s16")
            << ptx_make_addr( ptxr_h2sop_thd_base );
          PTX_RR ptxr_halo2_so_chars =
            ptx_make(PT_s32,"cvt.s32.s16") << ptxr_halo2_so_chars_h;
          PTX_RR ptxr_halo2_go_elts =
            ptx_make(PT_s32,"ld.global.s32")
            << ptx_make_addr( ptxr_h2gop_thd_base );
          PTX_RR ptxr_elt_gl_idx = ptx_make(PT_s32,"add.s32")
            << ptxr_halo2_go_elts << ek.ptxr_elt_blk_idx;
          h2.addr_bo_halo2[i] = ptx_addr_base_new( ptxr_elt_gl_idx);

          h2.ptxr_halo2_sh_base[i] =
            ptx_make(PT_s32,"add.s32")
            << ek.ptxr_shared_buffer_addr << ptxr_halo2_so_chars;
        }

      ptx_line0f("%s:",line_irreg_done.s);
    }

  PTX_RR& ptxr_sh_block4_base = h2.ptxr_halo2_sh_base[h2.gl_base_t0_idx];

  if ( h2.have_t0_rounds )
    {
      // Emit code to compute global and shared base address used by
      // t0 rounds.

      const int thds_per_b4grp = rndup(h2.x_width_max,warp_sz);
      PTX_RR ptxr_t0_pos =
        ptx_make(PT_s32,"rem.s32") << ek.ptxr_tid << thds_per_b4grp;

      PTX_RR ptxr_elt_gl_idx = ptx_make(PT_s32,"add.s32")
        << ptxr_t0_pos << ek.ptxr_elt_blk_idx;
      h2.addr_bo_halo2[h2.gl_base_t0_idx] = ptx_addr_base_new(ptxr_elt_gl_idx);

      ptxr_sh_block4_base =
        ptx_make(PT_s32,"mad.lo.s32") << ptxr_t0_pos << ek.assumed_elt_size
        << ek.ptxr_shared_buffer_addr;
    }

  //
  // Emit Participating Predicate Setting Code
  //

  // Code will be emitted for each work group (here, to set
  // predicates, and later to perform buffering). A thread executes a
  // series of branches to reach the code for its work group. The
  // branches and code are organized as a binary tree.
  //

  static PTX_RR ptxr_pred_true(true);
  h2.ptxr_participation.resize(buffer_groups+2);
  for ( auto& ptxr : h2.ptxr_participation )
    ptxr = ptx_make(PT_pred,"setp.eq.s32") << 0 << 1;

  const int nleaves = work_groups.size();
  const int tree_height = nleaves <= 1 ? nleaves - 1 : fl1(nleaves-1);
  h2.ptxr_tree_nodes.resize(tree_height+1);
  for ( auto& ptxr : h2.ptxr_tree_nodes )
    ptxr = ptx_make(PT_pred,"setp.eq.s32") << 0 << 1;
  h2.ptxr_halo2_participation = ptx_make(PT_pred,"setp.eq.s32") << 0 << 1;
  int tree_max_pred_per_leaf = 0;
  h2.tree_max_cond_per_leaf = 0; // Like pred_per_leaf but count always_true.

  if ( work_groups )
    {
      PTX_RR ptxr_tid = ptx_make(PT_s32, "mov.u32") << "%tid.x";
      const int height = nleaves == 1 ? 0 : fl1(nleaves-1);
      ek.code_num_pred_regs += height;
      pVector<int> stack;
      stack += 1;
      const int mask = ( 1 << height ) - 1;
      const int nnodes = 2 << height; // For a complete tree.

      // Prepare labels for branch targets.
      //
      pVector<pString> labels(nnodes+1);
      ptx_line_label_cnt++;
      for ( int i=0; i<=nnodes; i++ )
        labels[i].sprintf("work_group_node_%d_%d",i,ptx_line_label_cnt);
      const char* const h2_label_last = labels[nnodes].s;

      for ( int pos; stack.pop(pos); )
        {
          ek.code_path = pos;
          ptx_line0f("%s:",labels[pos].s);
          const int level = fl1(pos) - 1;
          const int alt = height - level;
          if ( alt )
            {
              // For an internal node, emit a branch instruction.
              //
              const int lchild = pos << 1;
              const int rchild = lchild + 1;
              const int wg_idx = ( rchild << alt - 1 ) & mask;
              if ( wg_idx >= nleaves ){ stack += lchild;  continue; }
              Work_Group* const wg = work_groups[wg_idx];
              ptx_make(h2.ptxr_tree_nodes[alt],"setp.ge.s32")
                << ptxr_tid << wg->tid_gstart << EMIT;
              ptx_make("bra.uni").predicate(h2.ptxr_tree_nodes[alt])
                << labels[rchild] << EMIT;
              stack += rchild;
              stack += lchild;
              continue;
            }

          // For a leaf node emit code to compute participation.

          const int wg_idx = pos & mask;
          assert( wg_idx < nleaves );
          Work_Group* const wg = work_groups[wg_idx];
          int num_pred = 0, num_cond = 0;
          const int max_thds_lg = 10;

          map<int,PTX_RR*> ptxr_part_map;

          for ( auto& wgr : wg->wg_rounds )
            {
              const int key = ( wgr.tid_stop << max_thds_lg ) + wgr.tid_start;

              wgr.ptxr_pred = ptxr_part_map[key];

              if ( wgr.ptxr_pred ) continue;

              const bool always_true =
                ( wg->idx+1 < nleaves || wg->tid_gnext >= block_size )
                && wgr.tid_start == wg->tid_gstart
                && wgr.tid_stop == wg->tid_gnext;

              num_cond++;
              wgr.ptxr_pred = ptxr_part_map[key] =
                always_true
                ? &ptxr_pred_true : &h2.ptxr_participation[num_pred++];

              assert( wgr.ptxr_pred );
              ptx_linef("// bg %d  wg %d  %s",
                        wgr.gf->bg->idx, wg->idx, wrt_str[wgr.wr_type]);
              if ( !always_true )
                {
                  PTX_RR ptxr_pred1 =
                    ptx_make(PT_pred,"setp.ge.s32")
                    << ptxr_tid << wgr.tid_start;
                  ptx_make(*wgr.ptxr_pred,"setp.lt.and.s32")
                    << ptxr_tid << wgr.tid_stop << ptxr_pred1 << EMIT;
                }
              ptx_make(h2.ptxr_halo2_participation,"setp.eq.and.s32")
                << 1 << 1 << *wgr.ptxr_pred
                << TCOMMENT("Halo 2 participation.") << EMIT;
            }

          set_max(tree_max_pred_per_leaf,num_pred);
          set_max(h2.tree_max_cond_per_leaf,num_cond);

          if ( wg_idx != nleaves-1 )
            ptx_make("bra") << h2_label_last << EMIT;
        }
      ptx_line0f("%s:",h2_label_last);
      ek.code_path = 1;
    }

  ek.code_num_pred_regs += tree_max_pred_per_leaf;
}


void
Chemora_CG_Calc_Mapping::emit_code_kernel(int kno)
{
  Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
  Function_Info* const fi = ki->fi;
  CaCUDALib_GPU_Info& gi = *c->gpu_info;
  CaCUDA_Kernel_Launch_Parameters& klp = fi->klp;
  const int assumed_elt_size = ek.assumed_elt_size = sizeof(CCTK_REAL);
  PTX_Reg_Type pt_real = assumed_elt_size == 8 ? PT_f64 : PT_f32;
  const char* const pt_real_str = ptx_rr_type[pt_real];

  if ( !ki->should_run ) return;

  static int serial = 0;  serial++;

  /// Make a backup copy of nodes.
  //
  // The backup copy is used to restore nodes on return. Existing
  // nodes can be changed and new nodes can be added without affecting
  // code that runs after this routine returns.
  //
  // The situation for mapping_node and assignment_vector is different.
  // These vectors can be expanded. When this routine returns they
  // will be shortened to their original length. However, changes to
  // existing elements will survive.
  //
  Node_Backup nbu(this);

  ET_Components& etc = ki->etc;
  Tile& tile = etc.tile;
  CStncl& k_stncl = etc.stncl;
  Buffer_Groups& buffer_groups = etc.buffer_groups;
  Chemora_Halo2_Data& h2 = ek.halo2_data;
  Nodes& knodes = ki->nodes;

  ki->rr_base_extra = 0;
  assert( ptx_all_regs.size() == 0 );
  ek.code_path = 1;
  ek.code_path_regs_shareable.clear();
  ek.code_path_regs_shareable.resize(PT_SIZE);
  ek.regs_shareable_code_path_last = 1;
  ek.gf_addr_to_ptxr.clear();

  const int max_warps = 32;
  ek.code_path_nnodes = 0;
  ek.code_path_nnodes += ek.code_path_h2_nnodes = max_warps*2;
  ek.code_path_1d = ek.code_path_nnodes++;
  for ( auto& vv: ek.code_path_regs_shareable ) vv.resize(ek.code_path_nnodes);
  ek.kno = kno;
  ek.pt_real = pt_real;
  ek.cagh_ni = klp.cagh_ni;
  ek.etc = &ki->etc;
  ek.code_num_pred_regs = 0; // The maximum number of live pred registers.

  ek.code_path_reg_pool.clear();
  ek.code_path_reg_pool.resize(ek.code_path_nnodes);
  for ( int i=1; i<ek.code_path_reg_pool.size(); i++ )
    ek.code_path_reg_pool[i].reset(i,c->opt_addr_regs_max,this);

  assert( ek.addr_bo_all.empty() );

  int k_shared_vars = 0;
  int k_local_vars = 0;
  int num_inter_iter_elts = 0;

  for ( GFIter gf(c->grid_functions); gf; )
    {
      gf->shared_var_idx = -1;
      gf->local_var_idx = -1;
      gf->bg = NULL;
      gf->bg_sh_var_idx = -1;
      gf->is_shared = false;
    }

  CaCUDA_Kernel_Launch_Parameters& params = fi->klp;

  const int block_size = ek.block_size = tile.x * tile.y * tile.z;
  const bool iter_y = etc.iter_y;
  auto iy = [&](int y, int z) { return iter_y ? y : z; };
  const Array_Axes e_axv = iter_y ? AX_y : AX_z;
  const int tile_e = ek.tile_e = !iter_y ? tile.z : tile.y;
  const int tile_f = ek.tile_f = iter_y ? tile.z : tile.y;
  const int tile_ee = !iter_y ? tile.zz : tile.yy; 
  const int stride_f = klp.cagh_ni * ( iter_y ? klp.cagh_nj : 1 );

  const int cak__tile_sy = tile.y * tile.yy;
  const int cak__tile_sz = tile.z * tile.zz;

  const int cak__gl_y_stride = params.cagh_ni;
  const int cak__gl_z_stride = ek.gl_z_stride = params.cagh_ni * params.cagh_nj;

  ek.gl_e_stride =  iter_y ? cak__gl_y_stride : cak__gl_z_stride;
  ek.gl_f_stride = !iter_y ? cak__gl_y_stride : cak__gl_z_stride;

  ptx_linef("\n// Generating kernel for tile size: %d x %d x %d  %s %d\n",
            tile.x,tile.y,tile.z,iter_y ? "y" : "z", tile_ee);

  ptx_linef
    ("// Code size estimates:\n"
     "//   nonloop %4d insn, %#7x B;  buff  %4d insn, %#7x B;\n"
     "//   iter    %4d insn, %#7x B;  total %4d insn, %#7x B\n",
     etc.code_nonloop_insn,
     int(etc.code_nonloop_insn * gi.code_size_per_insn_bytes),
     etc.code_buff_insn,
     int(etc.code_buff_insn * gi.code_size_per_insn_bytes),
     etc.code_iter_insn,
     int(etc.code_iter_insn * gi.code_size_per_insn_bytes),
     etc.code_nonloop_insn + etc.code_iter_insn * tile_ee,
     int( ( etc.code_nonloop_insn + etc.code_iter_insn * tile_ee )
          * gi.code_size_per_insn_bytes) );

  pString bg_report;

  bg_report.sprintf
    ("Pre-merge buffer group report for kernel %s\n",
     ki->function_name.c_str());

  for ( BGIter bg(buffer_groups); bg; )
    {
      CStncl stncl = bg->stncl; stncl.unset_to_zero();
      bg_report.sprintf
        ("Buffer group %2d %2d %c%c%c %s pat 0x%02x  "
         "(%+d%+d)x(%+d%+d)x(%+d%+d)\n",
         int(bg), int(bg->gfs),
         bg->axes & AX_x ? 'x' : '_',
         bg->axes & AX_y ? 'y' : '_',
         bg->axes & AX_z ? 'z' : '_',
         buffer_style_abbr[bg->style],
         bg->pattern,
         stncl.imin, stncl.imax,
         stncl.jmin, stncl.jmax, stncl.kmin, stncl.kmax);
    }
  bg_report += "\n";

  // Merge compatible buffer groups.
  //
  Buffer_Groups bg_new;
  map<Stncl_Packed,Buffer_Group*> bg_new_map;
  int ngfs_check = 0;
  for ( auto bg : buffer_groups ) ngfs_check += bg->gfs;
  for ( BGIter bg(buffer_groups); bg; )
    {
      bg_new += bg;  // May be popped.
      if ( bg->style != BS_Block || bg->axes != AX_xyz ) continue;
      const Stncl_Packed idx = stncl_pack(0,bg->stncl);
      Buffer_Group* const bgp = bg_new_map[idx];
      if ( !bgp ) { bg_new_map[idx] = bg;  continue; }
      bg_new.pop();
#define ACCUM(m) bgp->m += bg->m
      ACCUM(gfs); ACCUM(n_lds); ACCUM(n_py); ACCUM(n_pz); ACCUM(n_p0);
      ACCUM(gf_patterns);
      bgp->pattern |= bg->pattern;
#undef ACCUM
    }
  buffer_groups = bg_new;
  for ( auto bg : buffer_groups ) ngfs_check -= bg->gfs;
  assert( ngfs_check == 0 );

  int total_bytes = 0;
  int k_shared_elts_all = 0;  // Sum of all shared arrays.
  int e_length_max = 0;
  int& local_elts_max = ek.local_elts_max = 0;

  for ( BGIter bg(buffer_groups); bg; )
    {
      CStncl stncl = bg->stncl;

      bg->shared_vars = 0;
      bg->shared_elts = 0;
      bg->halo_rounds = 0;
      const bool uses_shared =
        bg->style == BS_Block || bg->style == BS_Plus;
      const bool uses_local =
        bg->style == BS_Plus || bg->style == BS_Pin
        || bg->style == BS_Block;

      // Stub code, to be removed when gf's can individually be assigned
      // to shared memory.
      if ( uses_shared ) bg->gfs_b = bg->gfs; else bg->gfs_b.clear();
      for ( GFVIter gf(bg->gfs); gf; ) gf->is_shared = uses_shared;

      if ( uses_shared )
        for ( GFVIter gf(bg->gfs_b); gf; )
          {
            gf->bg_sh_var_idx = int(gf);
            gf->shared_var_idx = k_shared_vars++;
          }

      for ( auto& p: bg->gf_patterns )
        {
          Grid_Function* const gf = p.gf;
          gf->pattern_info = p;
          gf->bg = bg;
          if ( uses_local ) gf->local_var_idx = k_local_vars++;
        }

      if ( bg->style == BS_Not_Cached || bg->style == BS_Read_Only_Cache
           || bg->style == BS_Constant )
        continue;

      const int e_ax = iter_y ? SX_y : SX_z;
      const int f_ax = iter_y ? SX_z : SX_y;

      const int stncl_e = bg->stncl_e = stncl.halo_width(e_ax);
      const int stncl_fmin = bg->stncl_fmin = stncl.omin[f_ax];
      const int stncl_fmax = bg->stncl_fmax = stncl.omax[f_ax];
      bg->stncl_emin = stncl.omin[e_ax];
      bg->stncl_emax = stncl.omax[e_ax];
      const int stncl_f = bg->stncl_f = stncl.halo_width(f_ax);

      const int x_length = bg->x_length = tile.x + stncl.halo_width(SX_x);
      const int sc_y_stride = bg->sc_y_stride = x_length;
      const int e_length = bg->e_length = tile_e + stncl_e;
      set_max(e_length_max,e_length);
      const int f_length = bg->f_length = tile_f + stncl_f;
      bg->sc_z_stride =
        bg->style == BS_Block
        ? sc_y_stride * ( tile.y + stncl.halo_width(SX_y) )
        : x_length;
      bg->sc_e_stride =  ek.etc->iter_y ? bg->sc_y_stride : bg->sc_z_stride;
      bg->sc_f_stride = !ek.etc->iter_y ? bg->sc_y_stride : bg->sc_z_stride;

      const bool plus_style = bg->style == BS_Plus;
      const bool pin_style = bg->style == BS_Pin;

      if ( bg->style == BS_Plus || bg->style == BS_Pin )
        num_inter_iter_elts += stncl_e * int(bg->gfs);

      const int shared_vars = bg->shared_vars = pin_style ? 0 : int(bg->gfs_b);

      if ( bg->style == BS_Pin || shared_vars == 0 ) continue;

      assert( plus_style || bg->style == BS_Block );

      assert( bg->shared_vars );
      bg->shared_array_base_byte = k_shared_elts_all * assumed_elt_size;

      const int rows_per_block_max = max(1,block_size / sc_y_stride);
      const int rounds_block = bg->rounds_block =
        plus_style ? 2 :
        ( f_length + rows_per_block_max -1 ) / rows_per_block_max;
      const int blocks_per_row =
        ( sc_y_stride + block_size - 1 ) / block_size;
      const bool sc_extra_x_tile = bg->sc_extra_x_tile = blocks_per_row > 1;
      const int local_elts_block = 
        rounds_block * tile_e * ( sc_extra_x_tile ? 2 : 1 );

      const int shared_elts_block = x_length * e_length * f_length;
      const int shared_elts_plus = x_length * f_length;

      const int shared_elts = bg->shared_elts =
        plus_style ? shared_elts_plus : shared_elts_block;
      k_shared_elts_all += shared_elts * shared_vars;

      const int sc_var_stride_bytes = bg->sc_var_stride_bytes =
        shared_elts * assumed_elt_size;

      const int gr_total_bytes = sc_var_stride_bytes * shared_vars;
      total_bytes += gr_total_bytes;
      assert( total_bytes <= 3 << 14 );

      pStringF halo_pos_array_name
        ("halo_pos_%s_%d", ki->function_name.c_str(), int(bg));
      bg->halo_pos_array_name = halo_pos_array_name.s;
      bg->halo_pos_array_size = 0;
      if ( plus_style )
        {
          // The offset array.
          int tidi = 0;

          ptx_linef(".global .s32 %s[][2] =", halo_pos_array_name.s);
          ptx_indent = 2;

          pString ptx_text("{ ");
          for ( int f = stncl_fmin; f < tile_f + stncl_fmax; f++ )
            for ( int x = stncl.imin; x < tile.x + stncl.imax; x++ )
              {
                if ( !etc.need_corners_get(bg) &&
                     ( f < 0 || f >= tile_f ) && ( x < 0 || x >= tile.x ) )
                  continue;

                if ( f >= 0 && f < tile_f && x == 0 )
                  { x = tile.x - 1; continue; }

                const int tid = tidi++;
                const int li = tid % tile.x;
                const int lj = ( tid / tile.x ) % tile.y;
                const int lk = tid / ( tile.x * tile.y ) % tile.z;
                const int lf = iter_y ? lk : lj;
                const int sh_idx =
                  -stncl.imin + x + ( f - stncl_fmin ) * x_length;
                const int di = x - li;
                const int df = f - lf;
                const int gl_offset = di + df * stride_f;
                ptx_text.sprintf("{%d,%d},",sh_idx*assumed_elt_size,gl_offset);
                if ( ptx_text.len() > 60 )
                  {
                    ptx_line(ptx_text.s);
                    ptx_text = "  ";
                  }
              }

          bg->halo_pos_array_size = tidi;
          ptx_text += "{0,0} };";
          if ( bg->style == BS_Pin )
            ptx_text += " // This array intentionally left empty.";
          ptx_lines(ptx_text.s,"",NULL);
          ptx_indent = 0;
        }

      bg->halo_rounds = divup( bg->halo_pos_array_size, block_size );
      const int local_elts_plus = bg->halo_rounds;
      const int local_elts = plus_style ? local_elts_plus : local_elts_block;
      set_max(local_elts_max,local_elts);
    }

  for ( BGIter bg(buffer_groups); bg; )
    {
      if ( bg->style != BS_Constant ) continue;
      for ( GFVIter gf(bg->gfs); gf; )
        {
          MI_Constant_Init_Item& cii = c->mi->constant_gfs[gf->name];
          if ( cii.gf_name == gf->name ) continue;
          assert( cii.gf_name.size() == 0 );
          cii.gf_name = gf->name;
          pStringF c_name("%s_const", gf->name.c_str());
          cii.ptx_symbol = c_name.ss();
          cii.device_addr = gf->device_addr;
          ptx_linef
            (".const .%s %s[%d];",
             pt_real_str, c_name.s, gf->cs_local_size_elts);
        }
    }

  bg_report.sprintf
    ("Buffer group report for kernel %s\n", ki->function_name.c_str());

  int check_total_shared_elts = 0;
  int check_sh_gf = 0;

  for ( BGIter bg(buffer_groups); bg; )
    {
      CStncl gf_stncl = bg->stncl;
      const int nbgfs = bg->gfs_b;
      const int shared_elts_all_gf = bg->shared_elts * nbgfs;
      if ( bg->shared_elts ) check_sh_gf += nbgfs;
      check_total_shared_elts += shared_elts_all_gf;
      gf_stncl.unset_to_zero();
      bg_report.sprintf
        ("BG %2d %2d %c%c%c %s pat 0x%02x "
         " (%+d%+d)x(%+d%+d)x(%+d%+d)  %2d * %4d = %5d elts.\n",
         int(bg), int(bg->gfs),
         bg->axes & AX_x ? 'x' : '_',
         bg->axes & AX_y ? 'y' : '_',
         bg->axes & AX_z ? 'z' : '_',
         buffer_style_abbr[bg->style],
         bg->pattern,
         gf_stncl.imin, gf_stncl.imax,
         gf_stncl.jmin, gf_stncl.jmax, gf_stncl.kmin, gf_stncl.kmax,
         nbgfs, bg->shared_elts, shared_elts_all_gf);
    }

  bg_report.sprintf
    ("For kernel %2d  pat 0x%02x "
     " (%+d%+d)x(%+d%+d)x(%+d%+d)  %2d ------ = %5d elts\n",
     kno, etc.pattern,
     k_stncl.imin, k_stncl.imax, k_stncl.jmin, k_stncl.jmax,
     k_stncl.kmin, k_stncl.kmax,
     check_sh_gf, check_total_shared_elts);

  printf("%s",bg_report.s);
  ptx_comment_line(bg_report.s);

  Grid_Function_Map gf_used;

  for ( auto p: c->grid_functions )
    for ( Node* const nd: p.second->nodes )
      if ( !nd->cse_remapped && is_assigned_to(nd,kno) )
        {
          gf_used[p.first] = p.second;
          break;
        }

  map<void*,int> ik_addr_used;

  // Find inter-kernel load addresses.
  for ( auto nd: knodes )
    for ( auto src: nd->sources )
      {
        if ( is_assigned_to(src,kno) ) continue;
        Mapping_Node* const mrc = mapping_node_get(src);
        if ( mrc->ik_needed == IK_simple || mrc->ik_needed == IK_leader )
          ik_addr_used[src->ik_device_addr]++;
      }

  // Find inter-kernel store addresses.
  for ( auto nd: knodes )
    {
      Mapping_Node* const md = mapping_node_get(nd);
      if ( ( md->ik_needed == IK_simple || md->ik_needed == IK_leader )
           && md->ik_kno == kno )
        ik_addr_used[nd->ik_device_addr]++;
    }
  CCTK_REAL* const ik_addr_min =
    ik_addr_used.empty()
    ? (CCTK_REAL*)-1l : (CCTK_REAL*)ik_addr_used.begin()->first;

  // Emit kernel declaration.
  //

  halo2_setup_and_emit_arrays();

  ptx_linef(".visible .entry %s(",ki->function_name.c_str());
  ptx_indent_push(8);
  for ( KAAIter ka(c->kernel_arg_arrays); ka; )
    ptx_linef(".param .u64 %s_param_addr, // %p",
              ka->name.c_str(),ka->device_addr);
  ptx_lines(".param .u32 cctk_it_param_addr,",
            ".param .f64 cctk_time_param_addr",
            NULL);
  ptx_indent_pop();
  ptx_line(")");

  const int k_shared_bytes_all = k_shared_elts_all * assumed_elt_size;
  const int bl_per_mp_by_sm =
    c->gpu_info->shared_sz_per_mp_bytes / max(1,k_shared_bytes_all);

  ptx_linef(".reqntid %d",block_size);
  etc.bl_per_mp_hint_used =
    etc.blocks_per_mp <= c->opt_block_occ_no_hint_threshold
    &&  bl_per_mp_by_sm <= etc.blocks_per_mp + 1;

  if ( etc.bl_per_mp_hint_used )
    ptx_linef(".minnctapersm %d",etc.blocks_per_mp);

  ptx_body_start();
  ptx_indent_push(8);

  CCTK_REAL *gf_addr_min = (CCTK_REAL*)-1l;
  Grid_Function *gf_addr_min_gf = NULL;
  for ( KAAIter ka(c->kernel_arg_arrays); ka; )
    {
      Grid_Function* const gf = gf_used.lookup(ka->name);
      if ( !gf ) continue;
      assert( ka->device_addr == gf->device_addr );
      if ( set_min(gf_addr_min,(CCTK_REAL*)ka->device_addr) )
        gf_addr_min_gf = gf;
    }
  assert( !gf_addr_min_gf
          || gf_addr_min == (CCTK_REAL*)gf_addr_min_gf->device_addr );

  // Set to true to reduce variability in SASS due to dynamic storage address
  // variability.
  const bool opt_addr_base_cm = false;

  CCTK_REAL* const addr_min = min(gf_addr_min,ik_addr_min);

  CCTK_REAL* const addr_base =
    !gf_addr_min_gf && ik_addr_used.empty() ? NULL :
    opt_addr_base_cm || gf_used.size() + ik_addr_used.size() == 1 ? addr_min
    : (CCTK_REAL*)
    ( ( int64_t(addr_min) - gi.sass_ldst_offset_min ) & ~ size_t(0xff) );
  ek.addr_base = addr_base;

  map<string,ptrdiff_t> name_to_offset;
  for ( KAAIter ka(c->kernel_arg_arrays); ka; )
    name_to_offset[ ka->name ] =
      assumed_elt_size * (((CCTK_REAL*)ka->device_addr) - addr_base);

  for ( GFIter gf(c->grid_functions); gf; )
    {
      map<string,ptrdiff_t>::iterator it = name_to_offset.find(gf->name);
      assert( it != name_to_offset.end() );
      gf->offset_byte = it->second;
    }

  // Keep track of the number of times a variable appears on an expression's
  // left-hand side.
  //
  map<string,int> lhs_count;   // Number of times on LHS.
  map<string,int> lhs_so_far;  // Number of times emitted so far.
 
  for ( NIter nd(knodes); nd; )
    if ( nd->assigns_lhs() ) lhs_count[nd->lhs_name_dis]++;

  // Prepare to keep track of the number of unsatisfied dependencies.
  //
  for ( NIter nd(c->nodes); nd; )
    {
      nd->occ = 0;  // Workaround to Move_Proposal abuse of ENodes.
      nd->sources_unscheduled = nd->sources.size();
      nd->emitted = false;  // Needed for external sources.
      nd->ptxr.reset();
    }

  // Initialize node representing the loop iteration variable and
  // other top-of-loop calculations, which do not have nodes of their
  // own.
  //
  c->node_loop_start->sources_unscheduled = 1;

  PTX_RR ptxr_ptrue = ptx_make(PT_s32,"mov.s32") << 1;
  PTX_RR ptxr_pfalse = ptx_make(PT_s32,"mov.s32") << 0;

  // Used to store information about the most recent buffering verification
  // failure.
  PTX_RR ptxr_faulting_nd_idx = ptx_make(PT_s32,"mov.s32") << 0;

  PTX_RR ptxr_tid = ek.ptxr_tid = ptx_make(PT_s32, "mov.u32") << "%tid.x";
  PTX_RR ptxr_li = ek.ptxr_li =
    ptx_make(PT_s32, "rem.s32") << ptxr_tid << tile.x;
  PTX_RR ptxr_li_sc = ek.ptxr_li_sc =
    ptx_make(PT_s32, "mul.lo.s32") << ptxr_li << assumed_elt_size;
  PTX_RR ptxr_reg;
  ptxr_reg = ptx_make(PT_s32, "div.s32") << ptxr_tid << tile.x;
  PTX_RR ptxr_lj = ek.ptxr_lj =
    ptx_make(PT_s32, "rem.s32") << ptxr_reg << tile.y;

  PTX_RR ptxr_lk = ek.ptxr_lk =
    ptx_make(PT_s32, "div.s32") << ptxr_tid << tile.x * tile.y;

  PTX_RR ptxr_bIx = ptx_make(PT_s32, "mov.u32") << "%ctaid.x";

  const PTX_RR ptxr_gi_blk = ek.ptxr_gi_blk =
    ptx_make(PT_s32, "mad.lo.s32") << ptxr_bIx << tile.x << params.bwid_xn;

  PTX_RR ptxr_gi = ptx_make(PT_s32, "add.s32") << ptxr_gi_blk << ptxr_li;
  PTX_RR ptxr_bIy = ptx_make(PT_s32, "mov.u32") << "%ctaid.y";
  ptxr_reg = ptx_make(PT_s32,"rem.s32") << ptxr_bIy << params.cagh_blocky;
  PTX_RR ptxr_gj_blk = ek.ptxr_gj_blk =
    ptx_make(PT_s32,"mad.lo.s32") << ptxr_reg << cak__tile_sy << params.bwid_yn;
  PTX_RR ptxr_gj_first =
    ptx_make(PT_s32,"add.s32") << ptxr_gj_blk << ptxr_lj;
  ptxr_reg = ptx_make(PT_s32,"div.s32") << ptxr_bIy << params.cagh_blocky;
  PTX_RR ptxr_gk_blk = ek.ptxr_gk_blk =
    ptx_make(PT_s32,"mad.lo.s32") << ptxr_reg << cak__tile_sz << params.bwid_zn;
  PTX_RR ptxr_gk_first = ptx_make(PT_s32,"add.s32") << ptxr_gk_blk << ptxr_lk;

#define I3D_idx_local(di,dj,dk,tt) \
        ( ( stncl_en + (dj) + (dk) + (tt) ) % e_length )


  const PTX_RR *elt_coords[] =
    { NULL, &ptxr_gi, &ptxr_gj_first, NULL, &ptxr_gk_first };
  const PTX_RR *elt_blk_coords[] =
    { NULL, &ptxr_gi_blk, &ptxr_gj_blk, NULL, &ptxr_gk_blk };
  BZERO(ek.axes_len_global);
  ek.axes_len_global[AX_x] = params.cagh_ni;
  ek.axes_len_global[AX_y] = params.cagh_nj;
  ek.axes_len_global[AX_z] = params.cagh_nk;
  PTX_RR ptxr_gf_base_axis[AX_SIZE];
  PTX_RR_Addr_Base* addr_bo_axes[AX_SIZE];
  PTX_RR_Addr_Base* addr_bo_axes_char[AX_SIZE];

  bool axes_used[AX_SIZE]; BZERO(axes_used);
  for ( BGIter bg(buffer_groups); bg; ) axes_used[bg->axes] = true;
  axes_used[AX_xyz] = true; // Too much trouble to code for this special case.
  auto make_compute_idx = [&](const PTX_RR **coords, int ax)
    {
      PTX_RR accum;
      for ( int a_vec = AX_z;  a_vec >= AX_x;  a_vec >>= 1 )
        {
          if ( ! ( ax & a_vec ) ) continue;
          if ( accum.num >= 0 )
            accum = ptx_make(PT_s32,"mad.lo.s32")
              << accum << ek.axes_len_global[a_vec] << *coords[a_vec];
          else
            accum = *coords[a_vec];
        }
      return accum;
    };

  ek.ptxr_elt_blk_idx = make_compute_idx(elt_blk_coords,AX_xyz);
  PTX_RR ptxr_addr_base =
    gf_addr_min_gf
    ? ptx_make(PT_s64, "ld.param.s64")
    << pstringf("[%s_param_addr]",gf_addr_min_gf->name.c_str())
    : ptx_make(PT_s64, "add.s64") << 0 << 0;

  for ( int ax = AX_x; ax < AX_SIZE; ax++ )
    {
      if ( !axes_used[ax] ) continue;
      PTX_RR ptxr_elt_idx = make_compute_idx(elt_coords,ax);
      addr_bo_axes_char[ax] =
        ptx_addr_base_new(ptxr_elt_idx,1,Array_Axes_Enum(ax));
      ptxr_gf_base_axis[ax] =
        ptx_make(PT_s32,"mul.lo.s32") << ptxr_elt_idx << assumed_elt_size;
      addr_bo_axes[ax] =
        ptx_addr_base_new(ptxr_elt_idx,assumed_elt_size,Array_Axes_Enum(ax));
    }

  PTX_Insn insn_gf_base =
    ptx_make(PT_s64,"mad.wide.s32")
    << addr_bo_axes_char[AX_xyz]->ptxr_thd_offset << assumed_elt_size
    << TCOMMENT("Preparing gf_base_common_axis");
  PTX_RR ptxr_gf_base_common = opt_addr_base_cm
    ? insn_gf_base << ptxr_addr_base : insn_gf_base << addr_base;

  PTX_RR ptxr_lf = iter_y ? ptxr_lk : ptxr_lj;

  for ( BGIter bg(buffer_groups); bg; )
    {
      if ( !bg->shared_vars ) continue;

      // Shouldn't we be adding on the starting address of sm?
      PTX_RR ptxr_regm =
        ptx_make(PT_s32, "mad.lo.s32")
        << ptxr_lf << bg->sc_y_stride * assumed_elt_size << ptxr_li_sc;
      bg->ptxr_sh_base_common =
        ptx_make(PT_s32, "sub.s32")
        << ptxr_regm
        << assumed_elt_size
        * ( bg->stncl.imin + bg->stncl_fmin * bg->sc_y_stride );
    }

  const int cak__gi_limit = params.cagh_ni - params.bwid_xp;
  const int cak__gj_limit = params.cagh_nj - params.bwid_yp;
  const int cak__gk_limit = params.cagh_nk - params.bwid_zp;
  const int cak__ge_limit = iter_y ? cak__gj_limit : cak__gk_limit;
  const int cak__gf_limit = iter_y ? cak__gk_limit : cak__gj_limit;

  const bool partial_i_possible = klp.i_length_x % tile.x;
  const int i_length_e = iter_y ? klp.i_length_y : klp.i_length_z;
  const int i_length_f = iter_y ? klp.i_length_z : klp.i_length_y;
  const bool partial_f_possible = i_length_f % tile_f;
  const int tt_stop_maybe = i_length_e % tile_ee;
  const bool partial_loop_possible = tt_stop_maybe;
  const int partial_tt_min = partial_loop_possible ? tt_stop_maybe : tile_ee;
  PTX_RR& ptxr_ge_first = iter_y ? ptxr_gj_first : ptxr_gk_first;
  PTX_RR& ptxr_gf_first = iter_y ? ptxr_gk_first : ptxr_gj_first;

  ptx_line("// Maybe computing okay_i.");
  PTX_RR ptxr_okay_i =
    partial_i_possible
    ? ptx_make(PT_pred,"setp.lt.s32") << ptxr_gi << cak__gi_limit
    : PTX_RR(true);

  ptx_line("// Maybe computing okay_i_and_f.");
  PTX_RR ptxr_okay_i_and_f =
    partial_f_possible
    ? ( ptx_make(PT_pred,"setp.lt.and.s32")
        << ptxr_gf_first << cak__gf_limit << ptxr_okay_i )
    : ptxr_okay_i;

  ptx_line("// Maybe computing okay_e.");
  PTX_RR ptxr_okay_e =
    partial_loop_possible
    ? ( ptx_make(PT_pred,"setp.le.s32")
        << ptxr_ge_first << cak__ge_limit - tile_ee )
    : PTX_RR(true);

  if ( !ptxr_okay_i_and_f.ctc ) ek.code_num_pred_regs++;
  if ( !ptxr_okay_e.ctc ) ek.code_num_pred_regs++;

  for ( GFIter gf(c->grid_functions); gf; )
    {
      CStncl stncl;
      int nc = 0;
      int all_k_n_lds = 0;
      int all_k_n_sts = 0;
      int n_sts = 0;
      int all_k_pattern = 0;
      int pattern = 0;
      for ( NIter nd(gf->nodes); nd; )
        {
          if ( nd->cse_remapped ) continue;
          if ( nd->ntype == NT_GFO_Load )      all_k_n_lds++;
          else if ( nd->ntype == NT_GF_Store ) all_k_n_sts++;
          else assert( false );
          all_k_pattern |= nd->pattern;
          if ( is_assigned_to(nd,kno) )
            {
              if ( nd->ntype == NT_GFO_Load )
                {
                  nc++;
                  pattern |= nd->pattern;
                  stncl.include(nd->offset);
                }
              else
                n_sts++;
            }
        }

      assert( all_k_n_sts <= 1 );

      if ( gf == 1 )
        ptx_linef
          ("// %-12s %2s %3s %2s  %-9s %-9s %-17s %s",
           "GF Name", "", "Dim", "BG", "Sten Pat",
           "Insn", "Sten BBox", "Style");

      if ( nc + n_sts == 0 ) continue;

      stncl.unset_to_zero();

      ptx_linef
        ("// %-12s %2s %c%c%c %2d  0x%02x/0x%02x %2d/%2d %s/%s "
         "(%+d%+d)x(%+d%+d)x(%+d%+d) %s%s",
         gf->name.c_str(),
         gf->force ? pstringf("F%c",gf->force) : "  ",
         gf->axes & AX_x ? 'x' : '_',
         gf->axes & AX_y ? 'y' : '_',
         gf->axes & AX_z ? 'z' : '_',
         gf->bg ? gf->bg->idx : -1,
         pattern, all_k_pattern,
         nc, all_k_n_lds,
         n_sts ? "w" : "_",
         all_k_n_sts ? "w" : "_",
         stncl.imin, stncl.imax, stncl.jmin, stncl.jmax,
         stncl.kmin, stncl.kmax,
         gf->bg ? buffer_style_str[gf->bg->style] : "No BG",
         gf->bg && ( gf->bg->style == BS_Block || gf->bg->style == BS_Plus )
         && !gf->is_shared ? " But Not Shared" : "");
      pStringF second_line("// %12p", gf->device_addr);
      Offsets ref = gf->pattern_info.ref;

      if ( ref.i || ref.j || ref.k )
        second_line.sprintf
          ("%2s %3s %2s   0x%02x/     %4s %3s  "
           "( %+d )x( %+d )x( %+d )",
           "", "", "",
           gf->pattern_info.pattern,
           "", "",
           ref.i, ref.j, ref.k);
      ptx_line(second_line);
    }

  // Used by shared buffer loading code that executes before the main loop.
  ek.iter_offs = 0;

  PTX_RR ptxr_lo[k_local_vars][e_length_max];
  ek.ptxr_lo = (PTX_RR*) ptxr_lo;
  PTX_RR ptxr_l[k_shared_vars][local_elts_max];
  ek.ptxr_l = (PTX_RR*) ptxr_l;
  PTX_RR ptxr_le[k_local_vars];
  PTX_RR& ptxr_shared_buffer_addr = ek.ptxr_shared_buffer_addr;

  for ( BGIter bg(buffer_groups); bg; )
    if ( bg->halo_rounds )
      bg->ptxr_use_last_hp =
        ptx_make(PT_pred,"setp.lt.s32")
        << ptxr_tid
        << bg->halo_pos_array_size - (bg->halo_rounds-1) * params.block_size;

  if ( k_shared_elts_all )
    {
      // CAK_Plus_Setup
      ptx_linef(".shared %s cak__shared[%d];",
                assumed_elt_size == 8 ? ".f64" : ".f32",
                max(1,k_shared_elts_all));
      ptxr_shared_buffer_addr = ptx_make(PT_s32,"mov.s32") << "cak__shared";
    }

  for ( BGIter bg(buffer_groups); bg; )
    {
      if ( bg->style == BS_Block )
        {
          ptx_cf_shared_block_setup(bg);
          ptx_cf_shared_block_load_pre_loop(bg);
          continue;
        }
      if ( bg->style == BS_Plus && bg->shared_vars )
        {
          assert( bg->halo_rounds );
          PTX_RR ptxr_halo_pos_addr =
            ptx_make(PT_s64,"mov.s64") << bg->halo_pos_array_name;
          const int halo_pos_elt_size = 8;  // Both integers.

          PTX_RR ptxr_hp_thd_base =
            ptx_make(PT_s64,"mad.wide.s32")
            << ptxr_tid << halo_pos_elt_size << ptxr_halo_pos_addr;

          bg->ptxr_halo_sh_addr.resize(bg->halo_rounds);
          bg->ptxr_halo_gl_base.resize(bg->halo_rounds);

          for ( int i=0; i<bg->halo_rounds; i++ )
            {
              const int halo_elt_offset =
                i * params.block_size * halo_pos_elt_size;
              PTX_Insn sh_insn =
                ptx_make(PT_s32,"ld.global.s32")
                << ptx_make_addr( ptxr_hp_thd_base, halo_elt_offset );

              PTX_Insn gl_insn =
                ptx_make(PT_s32,"ld.global.s32")
                << ptx_make_addr
                ( ptxr_hp_thd_base, halo_elt_offset + halo_pos_elt_size / 2);

              if ( i + 1 == bg->halo_rounds )
                {
                  sh_insn.predicate( bg->ptxr_use_last_hp );
                  gl_insn.predicate( bg->ptxr_use_last_hp );
                }

              PTX_RR ptxr_gl_off = gl_insn;
              PTX_RR ptxr_sh_idx_sc = sh_insn;

              // Optimization note: ptxr_use_last_hp could also guard the
              // instructions below on the last i iteration.

              bg->ptxr_halo_sh_addr[i] =
                ptx_make(PT_s32,"add.s32")
                << ptxr_shared_buffer_addr << ptxr_sh_idx_sc;

              // Note: An add would require a convert, so mad is not so bad.
              bg->ptxr_halo_gl_base[i] =
                ptx_make(PT_s64, "mad.wide.s32")
                << ptxr_gl_off << assumed_elt_size << ptxr_gf_base_common;
            }
        }
    }

  halo2_emit_pre_loop_code();

  Nodes ready;  // Nodes with all dependencies satisfied.

  for ( NIter nd(knodes); nd; )
    if ( nd->sources_unscheduled == 0 ) ready += nd;

  // Emit everything that does not depend on the loop iteration variable.
  //
  while ( Node* const nd = ready.pop() )
    {

      // Update dependent nodes and add to ready list if ready.
      //
      for ( NIter dest(nd->dests); dest; )
        {
          if ( !dest->visited ) continue;
          if ( !is_assigned_to(dest,kno) ) continue;
          assert( dest->sources_unscheduled > 0 );
          dest->sources_unscheduled--;
          if ( dest->sources_unscheduled == 0 ) ready += dest;
        }

      if ( nd->emitted ) continue;

      nd->emitted = true;
      if ( nd->ntype == NT_Literal ) continue;
      assert( !nd->c_code.empty() );
      const bool is_scalar = nd->ntype == NT_GFO_Load && nd->gf->axes == AX_0;
      assert( nd->compile_time_constant || nd->ops == 0 || is_scalar );
      assert( nd->mem_accesses == 0 || is_scalar );

      const int lc = lhs_count[nd->lhs_name_dis];
      const int sf = lhs_so_far[nd->lhs_name_dis]++;
      assert( lc == 1 && sf == 0 );
      pStringF comment
        ("// idx %4d  thpt %.1f ops %2d %3s mem acc %2d  srcs %2zd  dsts %2zd",
         nd->idx, nd->issue_usage ? 1.0/nd->issue_usage :0, nd->ops,
         nd->compile_time_constant ? "ctc" : "",
         nd->mem_accesses, nd->sources.size(), nd->dests.size());
      Mapping_Node* const md = mapping_node_get(nd);
      assert( md->ik_kno >= num_kernels );

      if ( is_scalar )
        {
          assert( false ); // Not yet tested.
          PTX_RR base_addr =
            ptx_make(PT_s32,"mov.s32")
            << pstringf("%s_const",nd->gf->name.c_str());
          nd->ptxr = ptx_make(pt_real,"ld.const")
            << ptx_make_addr( base_addr, 0 );
          continue;
        }

      ptx_linef("// %s  =  %f",nd->c_code.c_str(),nd->ctc_val);
      assert( nd->compile_time_constant );
      assert( nd->ctc_val_set );
    }

  // Emit loop head.
  //

  Nodes body_ik_nodes;
  Nodes body_nodes;

  // Find inter-kernel load instructions.
  //
  for ( NIter nd(knodes); nd; )
    {
      for ( NIter src(nd->sources); src; )
        {
          if ( is_assigned_to(src,kno) ) continue;
          Mapping_Node* const mrc = mapping_node_get(src);
          if ( mrc->ik_needed == IK_follower ) continue;

          // At this point we found a source that's not assigned to
          // this kernel.

          assert( mrc->ik_kno < kno );
          assert( mrc->ik_needed );
          assert( nd->sources_unscheduled > 0 );
          assert( src->assigns_lhs() );
          assert( mrc->ik_grp_sz > 0 );

          nd->sources_unscheduled -= mrc->ik_grp_sz;
          assert( nd->sources_unscheduled >= 0 );
          if ( src->emitted ) continue;
          if ( mrc->ik_needed != IK_simple ) continue;

          body_ik_nodes += src;
          src->emitted = true;

          lhs_count[src->lhs_name_dis]++;
        }
    }

  c->node_loop_start->emitted = false;
  c->node_loop_start->sources_unscheduled = 0;

  // Find Implied Assignments
  //
  Nodes implied_nds;
  for ( NIter nd(knodes); nd; )
    {
      Mapping_Node* const md = mapping_node_get(nd);
      if ( md->ik_needed != IK_leader || md->ik_kno != kno ) continue;
      Node* const dst = nd->dests[0];
      Mapping_Node* const mst = mapping_node_get(dst);
      assert( !is_assigned_to(dst,kno) );
      assert( mst->red_kno > kno );

      Nodes srcs_rem;
      for ( NIter src(dst->sources); src; )
        {
          if ( src->ntype == NT_Special ) continue;
          Mapping_Node* const mrc = mapping_node_get(src);
          if ( !is_assigned_to(src,kno) ) continue;
          if ( mrc->ik_needed != IK_leader
               && mrc->ik_needed != IK_follower ) continue;
          assert( mrc->ik_kno == kno );
          srcs_rem += src;
        }
      assert( srcs_rem.size() == md->ik_grp_sz );
      srcs_rem += c->node_loop_start;
      c->node_srcs_replace(dst,srcs_rem);
      dst->leader = nd;
      dst->implied_assignment_kno = kno;
      dst->sources_unscheduled = dst->sources;
      implied_nds += dst;
    }

  // Find sinks. (GF stores and ik stores with no local destinations.)
  //
  Nodes body_sinks(implied_nds);
  for ( NIter nd(knodes); nd; )
    {
      if ( nd->ntype == NT_GF_Store ) { body_sinks += nd;  continue; }
      Mapping_Node* const md = mapping_node_get(nd);
      if ( md->ik_needed != IK_simple ) continue;
      if ( md->ik_kno != kno ) continue;
      int dests_here = 0;
      for ( NIter dst(nd->dests); dst; )
        if ( dst->visited && is_assigned_to(dst,kno) ) dests_here++;
      if ( dests_here ) continue;
      body_sinks += nd;
    }

  // Identify nodes for which PTX instructions will be emitted.
  //
  for ( NIter nd(c->nodes); nd; )
    nd->emittable = nd->visited && !nd->ctc_val_set && is_assigned_to(nd,kno)
      && nd->ntype != NT_Special;
  for ( NIter nd(implied_nds); nd; ) nd->emittable = true;

  for ( NIter nd(knodes); nd; )
    {
      Mapping_Node* const md = mapping_node_get(nd);
      if ( md->red_kno != kno ) continue;
      assert( is_assigned_to(nd,kno) );

      Nodes srcs_rem;
      for ( NIter src(nd->sources); src; )
        {
          Mapping_Node* const mrc = mapping_node_get(src);
          if ( mrc->ik_needed == IK_follower ) continue;
          srcs_rem += src;
        }
      c->node_srcs_replace(nd,srcs_rem);
    }

  int ptx_insn_estimate = 0;
  for ( NIter nd(knodes); nd; )
    if ( nd->visited && !nd->compile_time_constant )
      ptx_insn_estimate += max(1,int(nd->sources.size())-2);

  int max_regs_before = 0;
  double avg_regs_before = 0;
  int max_regs_after = 0;
  double avg_regs_after = 0;
  int max_regs_crack = 0;
  double avg_regs_crack = 0;

  {
    // Order the initial set of root nodes.
    //
    Nodes ostores = sched_set_examine(body_sinks);
    Nodes stack = ostores;
    Nodes bbody_nodes; // Backward body nodes.

    // A node is scheduled only when all of its destination nodes
    // have been scheduled. Initialize the dests_unemitted member which
    // is used to detect that.
    //
    Nodes knodes_plus = knodes + implied_nds;
    for ( NIter nd(knodes_plus); nd; )
      {
        int ndests = 0;
        for ( NIter dst(nd->dests); dst; ) if ( dst->emittable ) ndests++;
        nd->dests_unemitted = ndests;
      }

    // Walk the expression DAG in depth-first fashion starting at
    // store nodes and ordering a node's sources to reduce register
    // pressure.
    //
    while ( Node* const nd = stack.pop() )
      {
        if ( nd->dests_unemitted || nd->emitted ) continue;
        nd->emitted = true;
        bbody_nodes += nd;
        ENodes sset(1);
        for ( NIter src(nd->sources); src; )
          {
            if ( !src->emittable ) continue;
            assert( src->dests_unemitted > 0 );
            src->dests_unemitted--;
            if ( src->dests_unemitted == 0 ) sset += src;
          }
        if ( sset.empty() ) continue;
        Nodes ssset = sched_set_examine(sset);
        while ( Node* src = ssset.pop() ) stack += src;
      }

      c->node_loop_start->emitted = true;

      //
      // Schedule instructions to reduce the number of live registers.
      //

      Nodes rnodes;
      for ( int i=bbody_nodes-1; i>=0; i-- ) rnodes += bbody_nodes[i];

      sched_pos_live_update_or_verify
        ('u', rnodes, max_regs_before, avg_regs_before);

      int swaps = 0;
      int sweeps = 0;
      const int sweeps_early = 10;
      const int sweeps_limit = sweeps_early + rnodes.size();

      for ( ; sweeps < sweeps_limit; sweeps++ )
        {
          int swaps_before = swaps;
          const bool verify_lr = false; // Verify live regs after each change.

          // Forward moves.
          //
          for ( int i=rnodes-2; i>=0; i-- )
            {
              Node* const na = rnodes[i];
              if ( na->ss_dest_pos_max == dest_ignore ) continue;

              // Determine how many of na's sources are end-of-life,
              // meaning that na is the last node to use them. Moving
              // na forward will increase register pressure by the
              // number of eol sources. Actually, the code currently
              // just determines whether there is at least one eol
              // source.
              //
              size_t a_src_eol_min = rnodes;

              for ( NIter src(na->sources); src; )
                if ( src->emittable )
                  set_min(a_src_eol_min,src->ss_dest_pos_max);

              if ( a_src_eol_min == i ) continue;
              assert( a_src_eol_min > i );

              size_t na_first_dep = rnodes;
              for ( NIter dst(na->dests); dst; )
                if ( dst->emittable )
                  set_min(na_first_dep, dst->ss_root_idx);

              const int j_limit = min({na_first_dep, a_src_eol_min, rnodes-1});
              int lr_maxima_pos = 0;
              int lr_maxima = 0;
              int lr_max_min = 0;

              for ( int j = i + 1; j < j_limit; j++ )
                {
                  Node* const nb = rnodes[j];
                  Node* const nc = rnodes[j+1];
                  const int lr_b = nb->ss_num_live_regs;
                  const int lr_c = nc->ss_num_live_regs;

                  if ( lr_b > lr_c && lr_b > lr_maxima )
                    {
                      lr_maxima = lr_b;
                      lr_max_min = lr_b;
                    }

                  if ( lr_c < lr_max_min )
                    {
                      lr_max_min = lr_c;
                      lr_maxima_pos = j;
                    }
                }

              if ( !lr_maxima ) continue;

              //
              // Move nodes i+1 to lr_maxima_pos back by one position.
              //

              const int delta_r = -1;

              swaps++;

              // List of nodes whose ss_dest_pos_max fields need to be updated.
              ENodes ch_srcs(1);
              ch_srcs += na->sources;

              for ( int j=i; j<lr_maxima_pos; j++ )
                {
                  Node* const nd = rnodes[j+1];
                  ch_srcs += nd->sources;
                  rnodes[j] = nd;
                  assert( nd->ss_num_live_regs > 0 );
                  nd->ss_num_live_regs += delta_r;
                  nd->ss_root_idx = j;
                }
              rnodes[lr_maxima_pos] = na;
              na->ss_root_idx = lr_maxima_pos;
              na->ss_num_live_regs =
                rnodes[lr_maxima_pos+1]->ss_num_live_regs-1;
              sched_pos_max_update_or_verify('f', ch_srcs);
              if ( verify_lr ) sched_pos_live_verify(rnodes);
            }

          // Move nodes backward.
          //
          for ( int i=1; i<rnodes; i++ )
            {
              Node* const na = rnodes[i];

              Node_IMMap a_src_eols;
              int a_src_pos_max = 0;
              ENodes uniq_srcs(2);
              for ( NIter src(na->sources); src; )
                if ( src->emittable ) uniq_srcs += src;
              for ( NIter src(uniq_srcs); src; )
                {
                  set_max(a_src_pos_max,src->ss_root_idx);
                  int src_dest_2nd_max = 0;
                  for ( NIter dst(src->dests); dst; )
                    if ( dst != na && dst->emittable )
                      set_max(src_dest_2nd_max,dst->ss_root_idx);
                  if ( src_dest_2nd_max < i )
                    a_src_eols.insert(src_dest_2nd_max,src);
                }

              const int dest_delta_r =
                na->ss_dest_pos_max == dest_ignore ? 0 : 1;

              int lr_maxima_pos = 0;
              int lr_maxima = 0;
              pVector<int> delta_rs;

              for ( int j = i - 1; j > a_src_pos_max; j-- )
                {
                  Node* const nb = rnodes[j];
                  const int lr_b = nb->ss_num_live_regs;

                  while ( !a_src_eols.empty() && a_src_eols.key() == j )
                    a_src_eols.pop();

                  const int delta_r = dest_delta_r - a_src_eols.size();
                  if ( delta_r > 0 ) break;
                  if ( delta_r == 0 && sweeps > sweeps_early ) break;

                  delta_rs += delta_r;

                  if ( lr_b > lr_maxima ){lr_maxima = lr_b; lr_maxima_pos = j;}
                }

              if ( !lr_maxima ) continue;

              //
              // Move nodes lr_maxima_pos to i-1 forward by one position.
              //

              const int na_delta_r =
                rnodes[lr_maxima_pos]->ss_num_live_regs - na->ss_num_live_regs;

              if ( na_delta_r > 0 && delta_rs.size() == 1 ) continue;

              swaps++;

              // List of nodes whose ss_dest_pos_max fields need to be updated.
              ENodes ch_srcs(1);
              ch_srcs += na->sources;

              na->ss_num_live_regs = rnodes[lr_maxima_pos]->ss_num_live_regs;

              for ( int j = i; j > lr_maxima_pos; j-- )
                {
                  Node* const nd = rnodes[j-1];
                  ch_srcs += nd->sources;
                  rnodes[j] = nd;
                  nd->ss_root_idx = j;
                  nd->ss_num_live_regs += delta_rs[i-j];
                  assert( nd->ss_num_live_regs >= 0 );
                }

              rnodes[lr_maxima_pos] = na;
              na->ss_root_idx = lr_maxima_pos;
              sched_pos_max_update_or_verify('f', ch_srcs);
              if ( verify_lr ) sched_pos_live_verify(rnodes);
            }
          if ( sweeps > sweeps_early && swaps_before == swaps ) break;
        }

      ptx_linef("// Sweeps %d, Swaps %d.", sweeps, swaps);

      // Verify ss_num_live_regs and compute new maximum and average.
      //
      sched_pos_live_update_or_verify
        ('v', rnodes, max_regs_after, avg_regs_after);

      // In preparation for cracking associative expression nodes into
      // smaller nodes, number the existing nodes based on execution
      // order and collect a list of nodes to crack.
      //
      Nodes crack_nds;
      Node_IMMap sbody_nodes;
      int n_insn_estimate = c->nodes.size() + 8;
      for ( NIter nd(rnodes); nd; )
        {
          nd->ss_root_idx = 2 * sbody_nodes.size();
          sbody_nodes.insert( nd->ss_root_idx, nd );
          if ( !c->opt_reduce_emit_dataflow ) continue;
          if ( nd->no != NO_Sum ) continue;
          if ( nd->sources < 4 ) continue;
          crack_nds += nd;
          n_insn_estimate += nd->sources - 2;
        }

      mapping_node.resize( n_insn_estimate );
      assignment_vector.resize( n_insn_estimate );

      for ( NIter nd(crack_nds); nd; )
        {
          Node_IMMap srcs;
          Nodes other_srcs;
          for ( NIter src(nd->sources); src; )
            {
              if ( !src->emittable )
                {
                  if ( src->ntype != NT_Special ) other_srcs += src;
                  continue;
                }
              int pos_max = src->ss_root_idx;

              for ( NIter dst(src->dests); dst; )
                if ( dst->emittable && dst->idx != nd->idx )
                  set_max(pos_max,dst->ss_root_idx);

              if ( pos_max == src->ss_root_idx ) srcs.insert(pos_max,src);
              else                               other_srcs += src;
            }
          if ( srcs < 3 ) continue;

          Node *accum = nd;
          c->node_srcs_clear(nd);
          c->node_srcs_add(nd,other_srcs);
          c->node_src_add(nd,srcs.pop());

          while ( Node* const src = srcs.pop() )
            {
              Node *n_sum = NULL;
              if ( srcs.size() == 0 ){ n_sum = accum; }
              else
                {
                  pStringF name("chemora__red_piece_%d_n_%zd",
                                c->var_interm_next++, nd->sources.size());
                  n_sum = c->node_new(name.s,NT_Expression);
                  assignment_vector[n_sum->idx] = AV(kno);
                  n_sum->oidx = nd->idx;
                  n_sum->no = NO_Sum;
                  n_sum->loop_body = true;
                  n_sum->leader = NULL;
                  n_sum->implied_assignment_kno = nd->implied_assignment_kno;
                  n_sum->emittable = true;
                  n_sum->ss_root_idx = src->ss_root_idx + 1;
                  c->node_src_add(accum,n_sum);
                }

              c->node_src_add(n_sum,src);
              c->node_src_add(accum,c->node_loop_start);
              pStringF tc("Reduction piece from %d.",nd->idx);
              c->node_c_code_make(accum,tc.s);
              if ( accum != nd ) sbody_nodes.insert(accum->ss_root_idx,accum);
              accum = n_sum;
            }
        }

      body_nodes = sbody_nodes;

      sched_pos_live_update_or_verify
        ('u', body_nodes, max_regs_crack, avg_regs_crack);

      for ( NIter nd(body_nodes); nd; )
        {
          assert( nd->loop_body );
          c->node_perf_update(nd,&etc);
        }
  }

  // Schedule nodes to hide latency.
  //
  const int nnodes_calc = c->nodes;
  const int marker_n = c->next_marker++;

  for ( NIter nd(body_ik_nodes); nd; ) nd->marker_set(marker_n);
  for ( NIter nd(body_nodes); nd; ) nd->dests_unemitted = 0;

  const int regs_per_real = max(1,assumed_elt_size / 4);
  const int regs_inter_iter = num_inter_iter_elts * regs_per_real;
  const int regs_other = 4;  // Two address base regs.
  const int elt_regs_per_thd =
    max(0, etc.regs_per_thd - regs_inter_iter - regs_other ) / regs_per_real;

  const int wait_queue_limit = 700;
  const int live_reg_limit = elt_regs_per_thd;
  const int dest_emitted_wait = 0x2;
  const int dest_emitted_true = 0x1;
  const int dest_emitted_both = dest_emitted_wait | dest_emitted_true;

  ptx_linef("// Latency sched factors: reg limit %d, inter regs %d.",
            live_reg_limit, regs_inter_iter);

  {
      Nodesd stage_new;
      Node_IMap wait;
      int t = 0;
      int live_regs = 0;
      int live_regs_w = live_regs;

      while ( !wait.empty() || !body_nodes.empty() )
        {
          Node *nd = NULL;
          if ( !wait.empty()
               && ( wait.peek()->t_sched >= t
                    || wait.size() > wait_queue_limit
                    || live_regs_w >= live_reg_limit
                    || body_nodes.empty() ) )
            {
              nd = wait.pop();
              for ( NIter dst(nd->dests); dst; )
                assert( !dst->emittable || dst->marker_check(marker_n) );
              Node* const nd_next = wait.peek();
              assert( !nd_next || nd_next->t_sched <= nd->t_sched );

              nd->t_sched = t;
            }

          if ( !nd && !body_nodes.empty() )
            {
              Node* const cn = body_nodes.pop();
              // Latency must be above zero to keep instructions ordered.
              const int lat = max(1,cn->latency);
              const int max_latency = 1000000;
              int dst_t_sched_min = max_latency;
              int unplaced_dests = 0;

              for ( NIter dst(cn->dests); dst; )
                {
                  if ( !dst->emittable ) continue;
                  if ( !dst->marker_check(marker_n) ) unplaced_dests++;
                  set_min(dst_t_sched_min,dst->t_sched);
                }

              const int t_sched = dst_t_sched_min - lat;

              cn->t_sched = min(t, t_sched);
              const int key = cn->t_sched * nnodes_calc + cn->idx;
              if ( c->opt_sched_latency && cn->t_sched < t )
                {
                  for ( NIter src(cn->sources); src; )
                    {
                      if ( !src->emittable ) continue;
                      if ( !src->dests_unemitted ) live_regs_w++;
                      src->dests_unemitted |= dest_emitted_wait;
                    }
                  wait[key] = cn;
                }
              else
                {
                  assert( !unplaced_dests ); // What about IK stores? Ok?
                  nd = cn;
                }
            }

          if ( !nd ) continue;

          nd->marker_set(marker_n);
          if ( nd->ss_dest_pos_max != dest_ignore )
            { live_regs--;  live_regs_w--; }
          assert( live_regs >= 0 && live_regs_w >= 0 );
          for ( NIter src(nd->sources); src; )
            {
              if ( !src->emittable ) continue;
              if ( !( src->dests_unemitted & dest_emitted_true ) ) live_regs++;
              if ( !( src->dests_unemitted & dest_emitted_wait ) )
                live_regs_w++;
              src->dests_unemitted |= dest_emitted_both;
            }

          nd->dests_here = live_regs;
          stage_new.push_front(nd);
          const int issue_dur_cyc =
            params.block_size * nd->ops * nd->issue_usage + 0.9999;
          t -= issue_dur_cyc;

        }
      assert( live_regs == live_regs_w );
      body_nodes.assign(stage_new.begin(),stage_new.end());
    }

  // Compute estimated issue time.
  //
  int t = 0;
  for ( NIter nd(body_nodes); nd; )
    {
      const int issue_dur_cyc =
        params.block_size * nd->ops * nd->issue_usage + 0.9999;
      // Latency must be above zero to keep instructions ordered.

      int src_t_sched_max = 0;
      for ( NIter src(nd->sources); src; )
        if ( src->emittable )
          set_max(src_t_sched_max,src->t_sched+src->latency);
      const int t_sched = nd->t_sched = max(t,src_t_sched_max);
      const int t_prev = t;
      t = t_sched + issue_dur_cyc;
      assert( t >= t_prev );
    }

  int max_regs_lat = 0;
  double avg_regs_lat = 0;
  sched_pos_live_update_or_verify('u', body_nodes, max_regs_lat, avg_regs_lat);
  ki->live_reg_max = max_regs_lat;

  ptx_linef("// Live Reg Max:  %d -> %d -> %d -> %d,  "
            "Avg: %.1f -> %.1f -> %.1f -> %.1f",
            max_regs_before, max_regs_after, max_regs_crack, max_regs_lat,
            avg_regs_before, avg_regs_after, avg_regs_crack, avg_regs_lat);

  for ( NIter nd(knodes); nd; ) assert( nd->visited == nd->emitted );
  for ( NIter nd(implied_nds); nd; ) assert( nd->emitted );

  pStringF line_loop_exit("loop_exit");

  const bool mandatory_sync = false;
  bool sync_post_buffer = mandatory_sync;

  // Allow any register allocated at this point to go unused.
  for ( PTX_RR *rr: ptx_all_regs ) rr->maybe_unused_set();

  for ( int tt = 0;  tt < tile_ee;  tt++ )
    {
      ek.tt = tt;

      if ( tt == partial_tt_min )
        ptx_make("bra").npredicate(ptxr_okay_e) << line_loop_exit << EMIT;

      int iter_offs_axis[AX_SIZE];
      int iter_offs_sc_axis[AX_SIZE];
      for ( int ax = AX_x; ax < AX_SIZE; ax++ )
        {
          int offset = 0;
          for ( int a_vec = AX_z;  a_vec >= AX_x;  a_vec >>= 1 )
            {
              if ( ! ( ax & a_vec ) ) continue;
              offset *= ek.axes_len_global[a_vec];
              if ( a_vec & e_axv ) offset += tt;
            }
          iter_offs_axis[ax] = offset;
          iter_offs_sc_axis[ax] = offset * assumed_elt_size;
        }
      ek.iter_offs = iter_offs_axis[AX_xyz];
      const int iter_offs_sc = iter_offs_sc_axis[AX_xyz];

      const bool split_load = true;
      const bool sync_pre_buffer = tt && k_shared_elts_all;

      if ( sync_pre_buffer ) ptx_make("bar.sync 0") << EMIT;

      if ( h2.work_groups )
        {
          // Use halo2 method for loading shared memory.

          sync_post_buffer = true;

          // If true, emit code to re-compute predicates.
          //
          const bool regenerate_preds = ek.code_num_pred_regs > 5;

          const int nleaves = h2.work_groups.size();
          const int height = nleaves == 1 ? 0 : fl1(nleaves-1);
          pVector<int> stack;
          stack += 1;
          const int mask = ( 1 << height ) - 1;
          const int nnodes = 2 << height; // For a complete tree.
          pVector<pString> labels(nnodes+1);
          ptx_line_label_cnt++;
          for ( int i=0; i<=nnodes; i++ )
            labels[i].sprintf("work_group_node_%d_%d",i,ptx_line_label_cnt);
          const char* const h2_label_last = labels[nnodes].s;

          if ( regenerate_preds )
            ptx_make(ek.ptxr_tid, "mov.u32") << "%tid.x" << EMIT;

          if ( h2.tree_max_cond_per_leaf < 2 )
            ptx_make("bra").npredicate(h2.ptxr_halo2_participation)
              << h2_label_last << EMIT;

          for ( int pos; stack.pop(pos); )
            {
              ek.code_path = pos;

              ptx_line0f("%s:",labels[pos].s);

              const int level = fl1(pos) - 1;
              const int alt = height - level;
              if ( alt )
                {
                  const int lchild = pos << 1;
                  const int rchild = lchild + 1;
                  const int wg_idx = ( rchild << alt - 1 ) & mask;
                  if ( wg_idx >= nleaves ){ stack += lchild;  continue; }
                  Work_Group* const wg = h2.work_groups[wg_idx];

                  if ( regenerate_preds )
                    ptx_make(h2.ptxr_tree_nodes[alt],"setp.ge.s32")
                      << ptxr_tid << wg->tid_gstart << EMIT;

                  ptx_make("bra.uni").predicate(h2.ptxr_tree_nodes[alt])
                    << labels[rchild] << EMIT;

                  stack += rchild;
                  stack += lchild;
                  continue;
                }
              const int wg_idx = pos & mask;
              assert( wg_idx < nleaves );
              Work_Group* const wg = h2.work_groups[wg_idx];
              ptx_linef("// wg %d.", wg->idx);

              // Emit global load instructions.
              //
              for ( auto& wgr : wg->wg_rounds )
                {
                  ptx_linef("// Base %s  BG %2d  f=%d  GF %s",
                            wrt_str[wgr.wr_type], wgr.gf->bg->idx,
                            wgr.f, wgr.gf->name.c_str());
                  PTX_Insn insn_ld =
                    ptx_make(pt_real, chemora->ptx_mnem_ld_sm_populate)
                    << ptx_make_addr( wgr, iter_offs_sc);
                  if ( h2.tree_max_cond_per_leaf > 1 )
                    insn_ld.predicate( *wgr.ptxr_pred );
                  h2.ptxr_h2[wgr.reg_idx] = insn_ld;
                }

              // Emit shared store instructions.
              //
              for ( auto& wgr : wg->wg_rounds )
                {
                  Buffer_Group* const bg = wgr.gf->bg;
                  const bool is_block = bg->style == BS_Block;
                  const int de = bg->stncl_emax;
                  const int e_s =
                    ( de - bg->stncl_emin + ek.tt ) % bg->e_length;
                  const int delta_e = e_s + bg->stncl_emin;
                  const int bl_offset =
                    !is_block ? 0 :
                    assumed_elt_size * delta_e * bg->sc_e_stride;

                  PTX_Insn insn_st =
                    ptx_make_no_dest(pt_real,"st.shared")
                    << ptx_make_addr
                    ( h2.ptxr_halo2_sh_base[wgr.base_reg_idx],
                      bl_offset + wgr.sm_offset_sc )
                    << h2.ptxr_h2[wgr.reg_idx]
                    << TCOMMENT("Halo 2");
                  if ( h2.tree_max_cond_per_leaf > 1 )
                    insn_st.predicate( *wgr.ptxr_pred );
                  insn_st.emit();
                }

              if ( wg_idx != nleaves-1 )
                ptx_make("bra") << h2_label_last << EMIT;
            }
          ptx_line0f("%s:",h2_label_last);
          ek.code_path = 1;
        }

      for ( BGIter bg(etc.buffer_groups); bg; )
        {
          const int stncl_en = -bg->stncl_emin;
          const int stncl_ep = bg->stncl_emax;
          const int e_length = bg->e_length;

          if ( bg->style == BS_Not_Cached || bg->style == BS_Read_Only_Cache
               || bg->style == BS_Constant )
            continue;

          if ( bg->interior == BSI_Irreg ) continue;
          if ( bg->interior == BSI_T0 ) continue;

          if ( bg->interior == BSI_Block )
            {
              if ( tt )
                {
                  ptx_cf_shared_block_load_loop
                    (bg,false, split_load ? CT_g_to_l : CT_g_to_s );
                }
              continue;
            }

          for ( GFVIter gf(bg->gfs); gf; )
            {
              pStringF tc("%s",gf->name.c_str());
              Offsets ref = gf->pattern_info.ref;

              // CAK_Fetch_to_Cache_Plus_1e(v)
              if ( tt == 0 && bg->style != BS_Block )
                for ( int de = -stncl_en; de < stncl_ep; de++ )
                  {
                    ptxr_lo[gf->local_var_idx][I3D_idx_local(0,de,0,tt)] =
                      ptx_make_gfo_roc_or_gl_load
                      ( pt_real, gf, addr_bo_axes[AX_xyz], iter_offs_sc,
                        ref.i, iy(de+ref.j,ref.j), iy(ref.k,de+ref.k), 'g' );
                  }
              ptxr_le[gf->local_var_idx] =
                ptx_make_gfo_roc_or_gl_load
                ( pt_real, gf, addr_bo_axes[AX_xyz], iter_offs_sc,
                  ref.i, iy(stncl_ep+ref.j,ref.j), iy(ref.k,stncl_ep+ref.k),
                  'g' );

              // CAK_Fetch_to_Cache_Plus_1xf(v)
              if ( c->opt_halo2 ) continue;

              for ( int i = 0; i < bg->halo_rounds; i++ )
                {
                  PTX_Insn insn =
                    ptx_make(pt_real, chemora->ptx_mnem_ld_sm_populate)
                    << ptx_make_addr
                    ( bg->ptxr_halo_gl_base[i],
                      gf->offset_byte + iter_offs_sc )
                    << TCOMMENT(tc.s);
                  if ( i + 1 == bg->halo_rounds )
                    insn.predicate(bg->ptxr_use_last_hp);
                  ptxr_l[gf->shared_var_idx][i] = insn;
                }
            }
        }

      for ( BGIter bg(etc.buffer_groups); bg; )
        {
          const int stncl_en = -bg->stncl_emin;
          const int stncl_ep = bg->stncl_emax;
          const int e_length = bg->e_length;

          if ( bg->style == BS_Not_Cached || bg->style == BS_Read_Only_Cache
               || bg->style == BS_Constant )
            continue;

          if ( bg->style == BS_Block )
            {
              if ( bg->interior == BSI_Block )
                {
                  if ( tt && split_load )
                    {
                      ptx_cf_shared_block_load_loop(bg,false, CT_l_to_s);
                      sync_post_buffer = true;
                    }
                  continue;
                }
              assert( bg->interior == BSI_T0 );
              continue;
            }

          assert( bg->style == BS_Plus || bg->style == BS_Pin );

          for ( GFVIter gf(bg->gfs); gf; )
            {
              // CAK_Fetch_to_Cache_Plus_2e(v)
              {
                const int dj =  iter_y ? stncl_ep : 0;
                const int dk = !iter_y ? stncl_ep : 0;
                ptxr_lo[gf->local_var_idx][I3D_idx_local(0,dj,dk,tt)]
                  = ptxr_le[gf->local_var_idx];
              }

              if ( bg->style == BS_Plus && bg->shared_vars )
                {
                  // CAK_Fetch_to_Cache_Plus_2xf(v)
                  const int offset_bytes =
                    bg->shared_array_base_byte
                    + gf->bg_sh_var_idx * bg->sc_var_stride_bytes;

                  assert( bg->halo_rounds );
                  sync_post_buffer = true;

                  ptx_make_no_dest(pt_real,"st.shared")
                    << ptx_make_addr( bg->ptxr_sh_base_common, offset_bytes )
                    << ptxr_lo[gf->local_var_idx][I3D_idx_local(0,0,0,tt)]
                    << EMIT;

                  if ( c->opt_halo2 ) continue;

                  for ( int i=0; i<bg->halo_rounds; i++ )
                    {
                      PTX_Insn insn =
                        ptx_make_no_dest(pt_real,"st.shared")
                        << ptx_make_addr
                        ( bg->ptxr_halo_sh_addr[i], offset_bytes )
                        << ptxr_l[gf->shared_var_idx][i];
                      if ( i + 1 == bg->halo_rounds )
                        insn.predicate(bg->ptxr_use_last_hp);
                      insn.emit();
                    }
                }
            }
        }

      if ( sync_post_buffer )
        {
          ptx_make("bar.sync 0") << EMIT;
        }

      pStringF line_end_of_iteration("end_of_iter_%d",tt);
      // Note: ptx_make handles constant predicates.
      ptx_make("bra").npredicate(ptxr_okay_i_and_f)
        << line_end_of_iteration << EMIT;
      if ( !ptxr_okay_i_and_f.ctc ) ek.code_path = ek.code_path_1d;

      map<string,int> lhs_so_far_ik;  // Number of times emitted so far.

      // Emit inter-kernel load instructions.
      //
      for ( NIter src(body_ik_nodes); src; )
        {
          Mapping_Node* const mrc = mapping_node_get(src);

          assert( mrc->ik_kno < kno );
          assert( mrc->ik_needed );
          assert( src->assigns_lhs() );

          const int sf = lhs_so_far_ik[src->lhs_name_dis]++;

          const int ik_ops = 2;

          pStringF comment("// idx %d  ops %d/%d  mem acc %d",
                           src->idx, 0, ik_ops, 1);
          assert( sf == 0 );

          ptx_line(comment.s);
          assert( mrc->ik_st_emitted );
          if ( src->result_is_boolean() )
            {
              PTX_RR val_int =
                ptx_make(PT_s32,"ld.global.b8")
                << ptx_make_addr
                ( addr_bo_axes_char[AX_xyz], src->ik_device_addr, ek.iter_offs);
              src->ptxr =
                ptx_make(PT_pred,"setp.eq.s32") << val_int << ptxr_ptrue;
            }
          else
            {
              src->ptxr =
                ptx_make(pt_real,chemora->ptx_mnem_ld_single)
                << ptx_make_addr
                ( addr_bo_axes[AX_xyz], src->ik_device_addr, iter_offs_sc );
            }
        }

      // Emit instructions.
      //
      for ( NIter nd(body_nodes); nd; )
        {
          Mapping_Node* const md = mapping_node_get(nd);
          Static_Op_Info* const soi = soi_get(nd);
          const char* const mnem = soi->ptx_mnemonic.c_str();

          assert( !nd->c_code.empty() );

          pStringF comment
            ("// idx %4d  ops %d  issue %6.2f  lat %3.f t=%5d  "
             "s,d  %2zd,%2zd   live %3d",
             nd->idx,
             nd->ops, block_size * nd->ops * nd->issue_usage, nd->latency,
             nd->t_sched,
             nd->sources.size(), nd->dests.size(), nd->ss_num_live_regs);

          ptx_line(comment.s);

          if ( nd->identity )
            {
              // Yes, this should be done long before mapping. 23 June 2015
              assert( false ); // 25 May 2016, 10:56:24 CDT
              ptx_linef
                ("// Identity. %s", nd->c_code.c_str());
              nd->ptxr = nd->sources[nd->identity_src]->ptxr;
            }
          else if ( nd->compile_time_constant )
            {
              assert( nd->ctc_val_set );
              ptx_linef
                ("// Compile-time constant, val %f: %s",
                 nd->ctc_val, nd->c_code.c_str());
            }
          else
            {
              ptx_linef("// %s",nd->c_code.c_str());
              const int sh_var_idx =
                nd->gf && nd->gf->is_shared ? nd->gf->bg_sh_var_idx : -1;
              Buffer_Group* const bg = nd->gf ? nd->gf->bg : NULL;
              Buffer_Style buffer_style = bg ? bg->style : etc.groupless_style;
              const int stncl_en = bg ? -bg->stncl_emin : 0;
              const int e_length = bg ? bg->e_length : 0;
              Offsets ref;
              if ( bg ) ref = nd->gf->pattern_info.ref;
              const bool local_acc =
                ( buffer_style == BS_Pin || buffer_style == BS_Plus )
                && nd->di() == ref.i &&
                ( iter_y ? nd->dk() == ref.k : nd->dj() == ref.j );

              const bool ld_reuse =
                nd->no == NO_GFO_Load && bg && ( bg->axes & e_axv ) == 0
                && bg->style != BS_Constant && nd->ptxr.num >= 0;

              auto gfo_roc_or_gl_load = [&](char g_or_r)
                { return ptx_make_gfo_roc_or_gl_load
                   ( pt_real, nd->gf, addr_bo_axes[nd->gf->axes],
                     iter_offs_sc_axis[nd->gf->axes],
                     nd->di(), nd->dj(), nd->dk(), g_or_r); };

              auto verify_val = [&]()
                {
                  if ( !c->opt_verify_buffering ) return;
                  ptx_linef("// Buffering check for bg %d, gf %s.",
                            bg->idx, nd->gf->name.c_str());
                  PTX_RR ptxr_check = gfo_roc_or_gl_load('g');
                  PTX_Reg_Type rt_bits =
                    assumed_elt_size == 4 ? PT_b32 : PT_b64;
                  pStringF comp_insn("setp.eq.%s",ptx_rr_type[rt_bits]);
                  PTX_RR ptxr_okay =
                    ptx_make(PT_pred,comp_insn.s) << nd->ptxr << ptxr_check;
                  ptx_make(ptxr_faulting_nd_idx,"mov.s32").npredicate(ptxr_okay)
                    << nd->idx << EMIT;
                  ptx_make("st.global.s32").npredicate(ptxr_okay)
                    << ptx_make_addr(ptxr_gf_base_common, 16 * bg->idx + 1)
                    << ptxr_faulting_nd_idx << EMIT;
                };

              switch ( nd->no ) {

              case NO_CopySign:
                nd->ptxr = ptx_make(pt_real, mnem )
                  << nd->sources[1] << nd->sources[0]; // [sic]
                break;

              case NO_And: case NO_Or:
                nd->ptxr = ptx_make(PT_pred, mnem)
                  << nd->sources[0] << nd->sources[1];
                break;

              case NO_Not:
                nd->ptxr = ptx_make(PT_pred, mnem) << nd->sources[0];
                break;

              case NO_Less: case NO_LessEqual:
              case NO_Equal: case NO_NotEqual:
              case NO_Greater: case NO_GreaterEqual:
                nd->ptxr = ptx_make(PT_pred, pt_real, mnem)
                  << nd->sources[0] << nd->sources[1];
                break;

              case NO_Product:
              case NO_Sum:
              case NO_Max:
              case NO_Min:
              case NO_Abs:
              case NO_Negate:
                {
                  assert( mnem && mnem[0] );
                  assert( !nd->cse_remapped );

                  const bool implied_assignment =
                    nd->implied_assignment_kno == kno;

                  if ( nd->no == NO_Sum )
                    {
                      Nodes iks_to_load;
                      for ( NIter src(nd->sources); src; )
                        {
                          if ( src->ntype == NT_Special ) continue;
                          if ( is_assigned_to(src,kno) ) continue;
                          Mapping_Node* const mrc = mapping_node_get(src);
                          if ( mrc->ik_needed != IK_leader ) continue;

                          pStringF tc("IK %2d leader", mrc->ik_kno);
                          tc.sprintf
                            (" %2d/%2d/%2zd",
                             mrc->ik_ldr_pos, mrc->ik_grp_sz,
                             nd->sources.size());
                          tc += " [" + ik_make(src) + "]";
                          assert( mrc->ik_st_emitted );
                          assert( tt || src->ptxr.num == -1 );
                          src->ptxr =
                            ptx_make(pt_real,chemora->ptx_mnem_ld_single)
                            << TCOMMENT(tc.s)
                            << ptx_make_addr
                            ( addr_bo_axes[AX_xyz], src->ik_device_addr,
                              iter_offs_sc );
                          src->ptxr.ik_load = true;
                        }
                    }

                  Nodes insn_srcs;
                  for ( NIter src(nd->sources); src; )
                    if ( src->ntype != NT_Special ) insn_srcs += src;

                  if ( insn_srcs == 1 &&
                       ( nd->no == NO_Sum || nd->no == NO_Product ) )
                    {
                      assert( !implied_assignment );
                      nd->ptxr = insn_srcs[0]->ptxr;
                      break;
                    }

                  PTX_Insn insn = ptx_make(pt_real,mnem);

                  for ( NIter src(insn_srcs); src; )
                    {
                      if ( insn.operands == 2 )
                        {
                          if ( implied_assignment )
                            insn << TCOMMENT("Implied assignment.");
                          insn = ptx_make(pt_real,mnem) << insn;
                        }

                      if ( src->compile_time_constant )
                        {
                          assert( src->ctc_val_set );
                          insn << src->ctc_val;
                        }
                      else
                        {
                          assert( src->ptxr.num >= 0 );
                          insn << src->ptxr;
                        }
                    }
                  if ( implied_assignment )
                    insn << TCOMMENT("Implied assignment.");
                  nd->ptxr = insn;

                  if ( implied_assignment && nd->leader )
                    {
                      Node* const leader = nd->leader;
                      Mapping_Node* const meader = mapping_node_get(leader);
                      assert( tt || !meader->ik_st_emitted );
                      if ( !tt ) meader->ik_st_emitted = true;
                      ptx_make_no_dest(pt_real,chemora->ptx_mnem_st_ik)
                        << ptx_make_addr
                        ( addr_bo_axes[AX_xyz], leader->ik_device_addr,
                          iter_offs_sc )
                        << nd->ptxr << EMIT;
                    }
                }
                break;

              case NO_Power:
                ptx_line("{");
                ptx_makef(".param .%s param_0",pt_real_str) << EMIT;
                ptx_makef(".param .%s param_1",pt_real_str) << EMIT;
                ptx_makef(".param .%s retval",pt_real_str) << EMIT;
                ptx_make_no_dest(pt_real,"st.param")
                  << "[param_0]" << nd->sources[0] << EMIT;
                ptx_make_no_dest(pt_real,"st.param")
                  << "[param_1]" << nd->sources[1] << EMIT;
                ptx_make
                  ("call.uni (retval),"
                   "chemora_wrapper__pow,(param_0,param_1)")
                  << EMIT;
                nd->ptxr = ptx_make(pt_real,"ld.param") << "[retval]";
                ptx_line("}");
                break;

              case NO_Reciprocal:
                nd->ptxr = ptx_make(pt_real,mnem) << nd->sources[0]->ptxr;
                break;

              case NO_SquareRoot:
                nd->ptxr = ptx_make(pt_real,mnem) << nd->sources[0]->ptxr;
                break;

              case NO_Sin: case NO_Cos:
                {
                  const char* fname = nd->no == NO_Sin ? "sin" : "cos";
                  ptx_line("{");
                  ptx_makef(".param .%s param_0",pt_real_str) << EMIT;
                  ptx_makef(".param .%s retval_0",pt_real_str) << EMIT;
                  ptx_make_no_dest(pt_real,"st.param")
                    << "[param_0]" << nd->sources[0] << EMIT;
                  ptx_makef
                    ("call.uni (retval_0),chemora_wrapper__%s,(param_0)",fname)
                    << EMIT;
                  nd->ptxr = ptx_make(pt_real,"ld.param") << "[retval_0]";
                  ptx_line("}");
                }
                break;

              case NO_GFO_Load:

                if ( ld_reuse )
                  {
                    // Do nothing, value is in register.
                    ptx_linef("// Value in register %s.",nd->ptxr.txt.c_str());
                  }
                else if ( local_acc )
                  {
                    nd->ptxr = ptxr_lo[nd->gf->local_var_idx]
                      [I3D_idx_local(0,nd->dj(ref),nd->dk(ref),tt)];
                    verify_val();
                  }
                else if ( bg->style == BS_Constant )
                  {
                    // Note: The three PTX instructions below compile
                    // into one SASS instruction, for example,
                    // LDC.64 R110, c[0x3][R0+0x480];
                    PTX_RR base_addr =
                      ptx_make(PT_s32,"mov.s32")
                      << pstringf("%s_const",nd->gf->name.c_str());
                    if ( nd->gf->axes == AX_0 )
                      {
                        nd->ptxr = ptx_make(pt_real,"ld.const")
                          << ptx_make_addr( base_addr, 0 );
                        break;
                      }
                    PTX_RR elt_addr =
                      ptx_make(PT_s32,"add.s32")
                      << base_addr
                      << ptxr_gf_base_axis[nd->gf->axes];
                    const int gp_offset =
                      ptx_make_gp_offset_global
                      (nd->gf->axes,nd->di(), nd->dj(), nd->dk())
                      * assumed_elt_size;

                    nd->ptxr = ptx_make(pt_real,"ld.const")
                      << ptx_make_addr
                      ( elt_addr, iter_offs_sc_axis[nd->gf->axes] + gp_offset);
                  }
                else if ( sh_var_idx < 0
                     || bg->style == BS_Not_Cached
                     || bg->style == BS_Read_Only_Cache )
                  {
                    assert( sh_var_idx >= 0 || c->opt_buf_single_use != 's' );
                    const bool use_roc = buffer_style == BS_Read_Only_Cache;
                    nd->ptxr = gfo_roc_or_gl_load( use_roc ? 'r' : 'g');
                  }
                else if ( bg->style == BS_Pin )
                  {
                    assert( false );
                  }
                else if ( bg->style == BS_Plus )
                  {
                    assert( nd->gf->is_shared );
                    assert( nd->gf->axes == AX_xyz );
                    nd->ptxr =
                      ptx_make(pt_real,"ld.shared")
                      << ptx_make_addr
                      ( bg->ptxr_sh_base_common,
                        bg->shared_array_base_byte
                        + nd->gf->bg_sh_var_idx * bg->sc_var_stride_bytes
                        + assumed_elt_size *
                        ( nd->di(ref)
                          + iy(nd->dk(ref),nd->dj(ref)) * bg->sc_y_stride ) );
                    verify_val();
                  }
                else if ( bg->style == BS_Block )
                  {
                    assert( tile_e == 1 );
                    assert( nd->gf->is_shared );
                    assert( nd->gf->axes == AX_xyz );
                    const int de =  iy(nd->dj(ref),nd->dk(ref));
                    const int df =  iy(nd->dk(ref),nd->dj(ref));
                    const int e_s = ( stncl_en + de + ek.tt ) % e_length;
                    const int delta_e = e_s - stncl_en;
                    nd->ptxr =
                      ptx_make(pt_real,"ld.shared")
                      << ptx_make_addr
                      ( bg->ptxr_sh_base_common,
                        bg->shared_array_base_byte
                        + nd->gf->bg_sh_var_idx * bg->sc_var_stride_bytes
                        + assumed_elt_size *
                        ( nd->di(ref) +
                          delta_e * bg->sc_e_stride + df * bg->sc_f_stride ) );

                    verify_val();
                  }
                else
                  {
                    assert( false );
                  }
                break;

              case NO_GF_Store:
                {
                  Node* const src = nd->sources[0];
                  PTX_RR st_val = src->compile_time_constant
                    ? ptx_make(pt_real,"mov") << src->ctc_val : src->ptxr;
                  ptx_make_no_dest(pt_real,chemora->ptx_mnem_st_global)
                    << ptx_make_addr
                    ( addr_bo_axes[AX_xyz], nd->gf->device_addr, iter_offs_sc)
                    << st_val << EMIT;
                }
                break;

              case NO_Literal:
                assert(false);
                break;

              case NO_If:
                {
                  assert( nd->sources == 4 );
                  assert( nd->sources[0]->ptxr.rt == PT_pred );
                  PTX_Reg_Type rt = pt_real; // Yuck.
                  for ( int i=1; i<3; i++ )
                    if ( !nd->sources[i]->ctc_val_set )
                      rt = nd->sources[i]->ptxr.rt;
                  nd->ptxr = ptx_make(rt,mnem)
                    << nd->sources[1]
                    << nd->sources[2]
                    << nd->sources[0]->ptxr;
                }
                break;

              default:
                if ( nd->assigns_lhs() )
                  nd->ptxr = ptx_make_reg(pt_real);

                ptx_linef
                  ("// To Do: nt %d, no %d  fn %s  %s",
                   nd->ntype, nd->no,
                   nd->no == NO_Function ? nd->func_name.c_str() : "na",
                   nd->c_code.c_str());
                assert( false );
                break;
              }
            }

          // If this node produces a value read by another kernel then
          // emit an interkernel store.
          //
          if ( md->ik_kno == kno && md->ik_needed == IK_simple )
            {
              assert( tt || !md->ik_st_emitted );
              if ( !tt ) md->ik_st_emitted = true;
              ptx_linef("// Interkernel (ik) store to %p.",nd->ik_device_addr);
              if ( nd->result_is_boolean() )
                {
                  PTX_RR val_int =
                    ptx_make(PT_s32,"selp.s32")
                    << ptxr_ptrue << ptxr_pfalse << nd->ptxr;
                  ptx_make_no_dest(PT_s32,chemora->ptx_mnem_st_ik+".b8")
                    << ptx_make_addr
                    ( addr_bo_axes_char[AX_xyz], nd->ik_device_addr,
                      ek.iter_offs )
                    << val_int << EMIT;
                }
              else
                {
                  ptx_make_no_dest(pt_real,chemora->ptx_mnem_st_ik)
                    << ptx_make_addr
                    ( addr_bo_axes[AX_xyz], nd->ik_device_addr, iter_offs_sc )
                    << nd->ptxr << EMIT;
                }
            }
        }

      ptx_line0f("%s:",line_end_of_iteration.s);
      ek.code_path = 1;

    }

  ptx_line0f("%s:",line_loop_exit.s);

  if ( c->opt_verify_buffering )
    {
      // Prevent DCE of register holding idx of last node failing verification.
      ptx_linef
        ("// Prevent ptxas from DCE'ing register holding faulting node idx, %s",
         ptxr_faulting_nd_idx.txt.c_str());
      PTX_RR ptxr_tidy = ptx_make(PT_s32, "mov.u32") << "%tid.y";
      PTX_RR ptxr_false = ptx_make(PT_pred,"setp.gt.s32") << ptxr_tidy << 0;
      ptx_make("st.global.s32").predicate(ptxr_false)
        << ptx_make_addr( ptxr_gf_base_common, 1234 ) << ptxr_faulting_nd_idx
        << EMIT;
    }

  ptx_make("ret") << EMIT;
  ptx_indent_pop();

  const int nregs = ptx_all_regs.size();
  vector<bool> ii_dead(ptx_insns.size());
  vector<vector<int> > ii_live_change(ptx_insns.size());
  int dce_rnds = 0;
  for ( ; dce_rnds < 100; dce_rnds++ )
    {
      vector<int> reg_ii_last(nregs), reg_ii_first(nregs);
      for ( int ii=0; ii<ptx_insns.size(); ii++ )
        if ( ! ii_dead[ii] )
          for ( auto& s: ptx_insns[ii].srcs ) reg_ii_last[s.global_num] = ii;


      int nlost = 0, nlive_start = 0;
      for ( int ii=0; ii<ptx_insns.size(); ii++ )
        {
          if ( ii_dead[ii] ) continue;
          PTX_Insn& insn = ptx_insns[ii];
          if ( !insn.dest ) continue;
          const int dg = insn.dest.global_num;
          if ( !reg_ii_first[dg] ) { reg_ii_first[dg] = ii; nlive_start++; }
          if ( !reg_ii_last[dg] )
            {
              pStringF ntxt
                ("// DEAD %d: %s", dce_rnds, ptx_body_items[insn.bidx].c_str());
              ptx_body_items[insn.bidx] = ntxt;
              ii_dead[ii] = true;
              nlost++;
            }
        }
      if ( nlost ) continue;
      int nused = 0;
      for ( int r=0; r<reg_ii_last.size(); r++ )
        if ( const int ii = reg_ii_last[r] )
          {
            nused++;
            ii_live_change[ii].push_back(r);
          }
      assert( nlive_start == nused );
      break;
    }

  pVector<int> reg_nwrites(ptx_all_regs.size());
  pVector<int> nlive_t(PT_SIZE), nlive_t_max(PT_SIZE), nlive_t_max_bi(PT_SIZE);
  pVector<int> nlive_t_max32; // Number of live regs at 32 reg max.
  int nlive = 0, nlive_max = 0;
  int nlive32 = 0, nlive_max32 = 0, nlive_max32_bi = 0;

  for ( int ii=0; ii<ptx_insns.size(); ii++ )
    {
      if ( ii_dead[ii] ) continue;
      PTX_Insn& insn = ptx_insns[ii];
      for ( auto& ri: ii_live_change[ii] )
        {
          PTX_RR* const r = ptx_all_regs[ri];
          assert( nlive_t[r->rt] > 0 );
          nlive_t[r->rt]--;
          if ( ptx_rr_bytes[r->rt] == 8 ) nlive32 -= 2;
          else if ( r->rt != PT_pred ) nlive32--;
          assert( nlive32 >= 0 );
        }
      if ( insn.dest && !reg_nwrites[insn.dest.global_num]++ )
        {
          PTX_RR& r = insn.dest;
          nlive++;
          assert( r.rt == r.orig->rt );
          int& nl = nlive_t[r.rt];
          nl++;
          if ( set_max(nlive_t_max[r.rt],nl) ) nlive_t_max_bi[r.rt] = insn.bidx;
          if ( ptx_rr_bytes[r.rt] == 8 ) nlive32 += 2;
          else if ( r.rt != PT_pred ) nlive32++;
          if ( set_max(nlive_max32,nlive32) )
            {
              nlive_t_max32 = nlive_t;
              nlive_max32_bi = insn.bidx;
            }
        }
      nlive -= ii_live_change[ii].size();
      set_max(nlive_max,nlive);
      assert( nlive >= 0 );
    }

  ptx_body_items[nlive_max32_bi] += "// Maximum live GPR registers here.\n";
  ptx_body_items[nlive_t_max_bi[PT_s64]]
    += pstringf("// Maximum live s64, %d, here.\n",nlive_t_max[PT_s64]);

  ptx_body_end();

  ki->ptx_nlive_32 = nlive_max32;

  ptx_linef("// Maximum number of live GPR registers: %d", nlive_max32);
  for ( int i = 0; i<PT_SIZE; i++ )
    if ( nlive_t_max[i] )
      ptx_linef("// %-4s  %4d max live  %4d live at max 32.",
                ptx_rr_type[i], nlive_t_max[i], nlive_t_max32[i]);
  assert( nlive == 0 );

  pString base_ext_msg = "";

  map<int,int> nbe;
  auto is_h2 = [&](int cp) { return cp > 1 && cp < ek.code_path_1d; };

  for ( auto b: ek.addr_bo_all )
    for ( auto& e: b->base_extensions )
      if ( code_path_pool_get(e.code_path).check(e) )
        nbe[e.code_path]++;

  const int num_addr_regs_main = ek.code_path_reg_pool[1].occ();
  const int num_addr_rdefs_main = ek.code_path_reg_pool[1].n_reg_defs;
  const int num_base_extensions_main = nbe[1];
  int num_addr_regs_tree = 0;  // Number of live registers.
  int num_addr_rdefs_tree = 0; // Number of distinct regs.
  for ( int cp = 2; cp < ek.code_path_1d; cp++ )
    {
      set_max(num_addr_regs_tree,ek.code_path_reg_pool[cp].occ());
      set_max(num_addr_rdefs_tree,ek.code_path_reg_pool[cp].n_reg_defs);
    }

  int max_nbe_tree = 0;
  for ( auto& elt: nbe )
    if ( is_h2(elt.first) ) set_max(max_nbe_tree,elt.second);
  const int num_base_extensions = num_base_extensions_main + max_nbe_tree;

  if ( !report_cg2_ptr ) report_cg2_ptr = new pTable();
  pTable& report_cg2 = *report_cg2_ptr;

  report_cg2.row_start();
  report_cg2.entry(" :","%2d",kno);
  pStringF rpt_prefix("%3d,%2d,%2d", tile.x, tile.y, tile.z);
  report_cg2.entry("Tile",rpt_prefix.s);
  report_cg2.entry("Itr","%3d",max(tile.yy, tile.zz));
  report_cg2.entry("D","%1s", tile.yy > tile.zz ? "y" : "z");

  report_cg2.entry("ADM","%3d",num_addr_rdefs_main);
  report_cg2.entry("ADT","%3d",num_addr_rdefs_tree);
  report_cg2.entry("ADR","%3d",num_addr_rdefs_tree+num_addr_rdefs_main);

  report_cg2.entry("ARM","%3d",num_addr_regs_main);
  report_cg2.entry("ART","%3d",num_addr_regs_tree);

  report_cg2.entry("BEM","%3d",num_base_extensions_main);
  report_cg2.entry("BET","%3d",max_nbe_tree);
  report_cg2.entry("BER","%3d",num_base_extensions);

  report_cg2.entry("L32","%3d",nlive_max32);
  report_cg2.entry("s32","%3d",nlive_t_max32[PT_s32]);
  report_cg2.entry("s64","%3d",nlive_t_max32[PT_s64]);
  report_cg2.entry("f32","%3d",nlive_t_max32[PT_f32]);
  report_cg2.entry("f64","%3d",nlive_t_max32[PT_f64]);

  int unused = 0;
  string unused_txt;
  for ( PTX_RR *rr: ptx_all_regs )
    if ( rr->uses == 0 ) { unused_txt += rr->txt + ", "; unused++; }

  if ( unused )
    printf("Regs created %zd, unused %d\n %s\n",
           ptx_all_regs.size(), unused, unused_txt.c_str());
  assert( unused == 0 );

  for ( PTX_RR *rr: ptx_all_regs ) delete rr;
  ptx_all_regs.clear();

  for ( auto e: ek.addr_bo_all ) delete e;
  ek.addr_bo_all.clear();
}

void
Chemora_CG_Calc_Mapping::emit_code_clike_kernel(int kno)
{
  Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
  Function_Info* const fi = ki->fi;
  CaCUDA_Kernel_Launch_Parameters& params = fi->klp;
  //  CaCUDALib_GPU_Info& gi = *c->gpu_info;
  CaCUDA_Kernel_Launch_Parameters& klp = fi->klp;
  const int assumed_elt_size = ek.assumed_elt_size = sizeof(CCTK_REAL);
  PTX_Reg_Type pt_real = assumed_elt_size == 8 ? PT_f64 : PT_f32;
  const char* const pt_real_ctype = ptx_rr_ctype[pt_real];

  const bool single_array = true;

  if ( !ki->should_run ) return;

  static int serial = 0;  serial++;

  Node_Backup nbu(this);

  ET_Components& etc = ki->etc;

  assert( ptx_all_regs.size() == 0 );

  ek.code_path = 1;
  ek.code_path_regs_shareable.clear();
  ek.code_path_regs_shareable.resize(PT_SIZE);
  const int max_warps = 1;
  for ( auto& vv: ek.code_path_regs_shareable ) vv.resize(max_warps*2);

  // C-like Code Emit
  ptx_linef("\n// Generating kernel for local grid size: %d x %d x %d\n",
            klp.cagh_ni, klp.cagh_nj, klp.cagh_nk);

  const int ka_size = c->kernel_arg_arrays.size();
  ptx_linef("extern \"C\" void\n%s(void* cak__args[%d])",
            ki->function_name.c_str(), ka_size+2 );

  ptx_body_start();
  ptx_indent_push( 2 );

  for ( GFIter gf(c->grid_functions); gf; )
    {
      gf->shared_var_idx = -1;
      gf->local_var_idx = -1;
      gf->bg = NULL;
      gf->bg_sh_var_idx = -1;
      gf->is_shared = false;
    }

  for ( BGIter bg(etc.buffer_groups); bg; )
    {
      bg->shared_vars = 0;
      bg->shared_elts = 0;
      bg->halo_rounds = 0;
      // Stub code, to be removed when gf's can individually be assigned
      // to shared memory.
      bg->gfs_b.clear();
      for ( GFVIter gf(bg->gfs); gf; )
        {
          gf->is_shared = false;
          gf->bg = bg;
        }
    }

  if ( !single_array )
    for ( KAAIter ka(c->kernel_arg_arrays); ka; )
      ptx_linef("%s* const __restrict %s = *(%s**) cak__args[%d];",
                pt_real_ctype, ka->name.c_str(), pt_real_ctype, int(ka) );

  string acc_dev_ptr_attr;
  for ( Kernel_Arg_Array* ka: c->kernel_arg_arrays )
    acc_dev_ptr_attr += ka->name + ",";
  acc_dev_ptr_attr.pop_back();

  if ( false )
    {
      ptx_linef("const int32_t* cctk_it = (int32_t*) cak__args[%d];", ka_size);
      ptx_linef("const %s * cctk_time = (%s*) cak__args[%d];",
                pt_real_ctype, pt_real_ctype, ka_size + 1);
    }

  CCTK_REAL *addr_min = (CCTK_REAL*) -1l;
  int addr_min_idx = -1;
  for ( KAAIter ka(c->kernel_arg_arrays); ka; )
    {
      if ( set_min(addr_min,(CCTK_REAL*)ka->device_addr) )
        addr_min_idx = int(ka);
    }
  assert( addr_min > 0 );

  CCTK_REAL* const addr_base = addr_min;
  ek.addr_base = addr_base;

  map<string,ptrdiff_t> name_to_offset;
  for ( KAAIter ka(c->kernel_arg_arrays); ka; )
    name_to_offset[ ka->name ] =
      assumed_elt_size * (((CCTK_REAL*)ka->device_addr) - addr_base);

  for ( GFIter gf(c->grid_functions); gf; )
    {
      map<string,ptrdiff_t>::iterator it = name_to_offset.find(gf->name);
      assert( it != name_to_offset.end() );
      gf->offset_byte = it->second;
    }

  if ( single_array )
    ptx_linef("%s* const __restrict cak__addr_base = *(%s**) cak__args[%d];",
              pt_real_ctype, pt_real_ctype, addr_min_idx);

  Nodes& knodes = ki->nodes;

  // Keep track of the number of times a variable appears on an expression's
  // left-hand side.
  //
  map<string,int> lhs_count;   // Number of times on LHS.
  map<string,int> lhs_so_far;  // Number of times emitted so far.

  for ( NIter nd(knodes); nd; )
    if ( nd->assigns_lhs() ) lhs_count[nd->lhs_name_dis]++;

  // Prepare to keep track of the number of unsatisfied dependencies.
  //
  for ( NIter nd(c->nodes); nd; )
    {
      nd->occ = 0;  // Workaround to Move_Proposal abuse of ENodes.
      nd->sources_unscheduled = nd->sources.size();
      nd->emitted = false;  // Needed for external sources.
      nd->ptxr.reset();
    }

  // Initialize node representing the loop iteration variable and
  // other top-of-loop calculations, which do not have nodes of their
  // own.
  //
  c->node_loop_start->sources_unscheduled = 1;

  for ( GFIter gf(c->grid_functions); gf; )
    {
      CStncl stncl;
      int nc = 0;
      int all_k_n_lds = 0;
      int all_k_n_sts = 0;
      int n_sts = 0;
      int all_k_pattern = 0;
      int pattern = 0;
      for ( NIter nd(gf->nodes); nd; )
        {
          if ( nd->cse_remapped ) continue;
          if ( nd->ntype == NT_GFO_Load )      all_k_n_lds++;
          else if ( nd->ntype == NT_GF_Store ) all_k_n_sts++;
          else assert( false );
          all_k_pattern |= nd->pattern;
          if ( is_assigned_to(nd,kno) )
            {
              if ( nd->ntype == NT_GFO_Load )
                {
                  nc++;
                  pattern |= nd->pattern;
                  stncl.include(nd->offset);
                }
              else
                n_sts++;
            }
        }

      assert( all_k_n_sts <= 1 );

      if ( gf == 1 )
        ptx_linef
          ("// %-12s %2s %3s %2s  %-9s %-9s %-17s %s",
           "GF Name", "", "Dim", "BG", "Sten Pat",
           "Insn", "Sten BBox", "Style");

      if ( nc + n_sts == 0 ) continue;

      stncl.unset_to_zero();

      ptx_linef
        ("// %-12s %2s %c%c%c %2d  0x%02x/0x%02x %2d/%2d %s/%s "
         "(%+d%+d)x(%+d%+d)x(%+d%+d) %s%s",
         gf->name.c_str(),
         gf->force ? pstringf("F%c",gf->force) : "  ",
         gf->axes & AX_x ? 'x' : '_',
         gf->axes & AX_y ? 'y' : '_',
         gf->axes & AX_z ? 'z' : '_',
         gf->bg ? gf->bg->idx : -1,
         pattern, all_k_pattern,
         nc, all_k_n_lds,
         n_sts ? "w" : "_",
         all_k_n_sts ? "w" : "_",
         stncl.imin, stncl.imax, stncl.jmin, stncl.jmax, stncl.kmin, stncl.kmax,
         gf->bg ? buffer_style_str[gf->bg->style] : "No BG",
         gf->bg && ( gf->bg->style == BS_Block || gf->bg->style == BS_Plus )
         && !gf->is_shared ? " But Not Shared" : "");
    }

  Nodes ready;  // Nodes with all dependencies satisfied.

  for ( NIter nd(knodes); nd; )
    if ( nd->sources_unscheduled == 0 ) ready += nd;

  // Emit everything that does not depend on the loop iteration variable.
  //
  while ( Node* const nd = ready.pop() )
    {

      // Update dependent nodes and add to ready list if ready.
      //
      for ( NIter dest(nd->dests); dest; )
        {
          if ( !dest->visited ) continue;
          if ( !is_assigned_to(dest,kno) ) continue;
          assert( dest->sources_unscheduled > 0 );
          dest->sources_unscheduled--;
          if ( dest->sources_unscheduled == 0 ) ready += dest;
        }

      if ( nd->emitted ) continue;

      nd->emitted = true;
      if ( nd->ntype == NT_Literal ) continue;
      assert( !nd->c_code.empty() );
      assert( nd->compile_time_constant || nd->ops == 0 );
      assert( nd->mem_accesses == 0 );

      const int lc = lhs_count[nd->lhs_name_dis];
      const int sf = lhs_so_far[nd->lhs_name_dis]++;
      assert( lc == 1 && sf == 0 );
      pStringF comment
        ("// idx %4d  thpt %.1f ops %2d %3s mem acc %2d  srcs %2zd  dsts %2zd",
         nd->idx, nd->issue_usage ? 1.0/nd->issue_usage :0, nd->ops,
         nd->compile_time_constant ? "ctc" : "",
         nd->mem_accesses, nd->sources.size(), nd->dests.size());
      Mapping_Node* const md = mapping_node_get(nd);
      assert( md->ik_kno >= num_kernels );

      ptx_linef("// %s  =  %f",nd->c_code.c_str(),nd->ctc_val);
      assert( nd->compile_time_constant );
      assert( nd->ctc_val_set );
    }

  // Emit loop head.
  //

  c->node_loop_start->emitted = false;
  c->node_loop_start->sources_unscheduled = 0;

  // Find sinks. (GF stores and ik stores with no local destinations.)
  //
  Nodes body_sinks;
  for ( NIter nd(knodes); nd; )
    {
      if ( nd->ntype == NT_GF_Store ) { body_sinks += nd;  continue; }
      Mapping_Node* const md = mapping_node_get(nd);
      if ( md->ik_needed != IK_simple ) continue;
      if ( md->ik_kno != kno ) continue;
      int dests_here = 0;
      for ( NIter dst(nd->dests); dst; )
        if ( dst->visited && is_assigned_to(dst,kno) ) dests_here++;
      if ( dests_here ) continue;
      body_sinks += nd;
    }

  // Identify nodes for which PTX instructions will be emitted.
  //
  for ( NIter nd(c->nodes); nd; )
    nd->emittable = nd->visited && !nd->ctc_val_set && is_assigned_to(nd,kno)
      && nd->ntype != NT_Special;

  for ( NIter nd(knodes); nd; )
    {
      int ndests = 0;
      for ( NIter dst(nd->dests); dst; ) if ( dst->emittable ) ndests++;
      nd->dests_unemitted = ndests;
    }

  Nodes body_nodes;
  Nodes stack = body_sinks;
  while ( Node* const nd = stack.pop() )
    {
      if ( nd->dests_unemitted || nd->emitted ) continue;
      nd->emitted = true;
      body_nodes += nd;
      for ( NIter src(nd->sources); src; )
        {
          if ( !src->emittable ) continue;
          assert( src->dests_unemitted > 0 );
          src->dests_unemitted--;
          if ( src->dests_unemitted == 0 ) stack += src;
        }
    }
  c->node_loop_start->emitted = true;
  reverse(body_nodes.begin(),body_nodes.end());

  for ( NIter nd(knodes); nd; ) assert( nd->visited == nd->emitted );

  pStringF line_loop_exit("loop_exit");

  const int cak__gi_limit = params.cagh_ni - params.bwid_xp;
  const int cak__gj_limit = params.cagh_nj - params.bwid_yp;
  const int cak__gk_limit = params.cagh_nk - params.bwid_zp;

  ptx_linef("const ptrdiff_t cak__ni = %d; // Stride ", params.cagh_ni);
  ptx_linef("const ptrdiff_t cak__nj = %d;", params.cagh_nj);

  ptx_indent_push_abs(0);

  const char* const visible_arrays =
    single_array ? "cak__addr_base" : acc_dev_ptr_attr.c_str();

  ptx_linef("#pragma acc parallel deviceptr(%s)",visible_arrays);
  ptx_indent_pop();

  ptx_line0f("#pragma acc loop independent seq");
  ptx_linef
    ("for ( int cak__gk = %d;  cak__gk < %d;  cak__gk++ )",
     params.bwid_zn, cak__gk_limit);
  ptx_indent_push( 2 );
  ptx_line0f("#pragma acc loop independent gang");
  ptx_linef
    ("for ( int cak__gj = %d;  cak__gj < %d;  cak__gj++ )",
     params.bwid_yn, cak__gj_limit);
  ptx_indent_push( 2 );
  ptx_line0f("#pragma acc loop independent vector");
  ptx_linef
    ("for ( int cak__gi = %d;  cak__gi < %d;  cak__gi++ )",
     params.bwid_xn, cak__gi_limit);
  ptx_indent_push( 2 );
  ptx_line("{");
  ptx_indent_push( 2 );

  const char* svar[] = { "cak__ni", "cak__nj", "cak__nk" };

  auto make_idx_base =
    [&](int axes)
    {
      if ( axes == 0 ) return pString("0");
      pString idx_expr;
      pString curr_stride;
      const char* ivar[] = { "cak__gi", "cak__gj", "cak__gk" };
      for ( int i=0; i<3; i++ )
        {
          if ( ! ( axes & ( 1 << i ) ) ) continue;
          if ( idx_expr.len() ) idx_expr += " + ";
          if ( curr_stride ) idx_expr += curr_stride;
          idx_expr += ivar[i];
          curr_stride.sprintf("%s*",svar[i]);
        }
      return idx_expr;
    };

  auto make_idx_offset =
    [&](int axes, int di, int dj, int dk)
    {
      pString idx_expr;
      pString curr_stride;
      int d[] = { di, dj, dk };
      for ( int i=0; i<3; i++ )
        {
          if ( ! ( axes & ( 1 << i ) ) ) continue;
          if ( d[i] != 0 )
            {
              idx_expr += " + ";
              if ( curr_stride ) idx_expr += curr_stride;
              idx_expr.sprintf("%d", d[i]);
            }
          curr_stride.sprintf("%s*",svar[i]);
        }
      return idx_expr;
    };

  auto make_idx_nd_axes = [&](Node *nd, int axes)
    {
      pString idx;
      if ( single_array )
        idx.sprintf("%zdll + ", nd->gf->offset_byte/assumed_elt_size);
      idx.sprintf("cak__elt_idx[%d]", axes);
      const int di = nd->di();
      const int dj = nd->dj();
      const int dk = nd->dk();
      if ( di || dj || dk ) idx += make_idx_offset(axes,di,dj,dk);
      return idx;
    };

  auto make_idx_nd = [&](Node *nd)
    { return make_idx_nd_axes(nd,nd->gf->axes); };

  ptx_line("const ptrdiff_t cak__elt_idx[] = {");
  ptx_indent_push(2);
  for ( int axes=0; axes<= AX_xyz; axes++ )
    {
      ptx_linef
        ("/* %c%c%c */  %s%s",
         axes & AX_x ? 'x' : '_',
         axes & AX_y ? 'y' : '_',
         axes & AX_z ? 'z' : '_',
         make_idx_base(axes).s,
         axes < AX_xyz ? "," : "");
    }
  ptx_indent_pop();
  ptx_line("};");
  {
    // Emit instructions.
    //
    for ( NIter nd(body_nodes); nd; )
      {
        Static_Op_Info* const soi = soi_get(nd);
        const string mnem = soi->c_op_or_func;

        assert( !nd->c_code.empty() );

        pStringF comment
          ("// idx %4d  ops %d   s,d  %2zd,%2zd",
           nd->idx, nd->ops, nd->sources.size(), nd->dests.size() );

        ptx_line(comment.s);

        if ( nd->identity )
          {
            // Yes, this should be done long before mapping. 23 June 2015
            assert( false ); // 25 May 2016, 10:56:24 CDT
            ptx_linef
              ("// Identity. %s", nd->c_code.c_str());
            nd->ptxr = nd->sources[nd->identity_src]->ptxr;
          }
        else if ( nd->compile_time_constant )
          {
            assert( nd->ctc_val_set );
            ptx_linef
              ("// Compile-time constant, val %f: %s",
               nd->ctc_val, nd->c_code.c_str());
          }
        else
          {
            ptx_linef("// %s",nd->c_code.c_str());

            switch ( nd->no ) {

            case NO_CopySign:
              nd->ptxr = ptx_make(pt_real, mnem)
                << nd->sources[0] << nd->sources[1];
              break;

            case NO_And: case NO_Or:
              assert( nd->sources == 3 );
              nd->ptxr = insn_cli_op_make(PT_pred, mnem)
                << nd->sources[0] << nd->sources[1];
              break;

            case NO_Less: case NO_LessEqual:
            case NO_Equal: case NO_NotEqual:
            case NO_Greater: case NO_GreaterEqual:
              nd->ptxr = insn_cli_op_make(PT_pred, pt_real, mnem)
                << nd->sources[0] << nd->sources[1];
              break;

            case NO_Power:
              nd->ptxr = ptx_make(pt_real, mnem)
                << nd->sources[0] << nd->sources[1];
              break;

            case NO_Max:
            case NO_Min:
              {
                PTX_Insn insn = ptx_make_lhs(pt_real,"");
                Nodes srcs;
                for ( auto src: nd->sources )
                  if ( src->ntype != NT_Special ) srcs += src;
                Node* const last = srcs.pop();
                for ( auto src: srcs )
                  insn << mnem << "("_insn_txt << src << ","_insn_txt;
                insn << last << PTX_Insn_Text(string(srcs.size(),')'));
                nd->ptxr = insn;
              }
              break;

            case NO_Sin: case NO_Cos:
            case NO_SquareRoot:
            case NO_Abs:
              nd->ptxr = ptx_make(pt_real, mnem) << nd->sources[0];
              break;

            case NO_Negate:
              nd->ptxr = ptx_make(pt_real, mnem) << nd->sources[0];
              break;

            case NO_Product:
            case NO_Sum:
              {
                PTX_Insn insn = insn_cli_op_make(pt_real,mnem);
                for ( Node* src : nd->sources )
                  if ( src->ntype != NT_Special ) insn << src;
                nd->ptxr = insn;
              }
              break;

            case NO_Reciprocal:
              nd->ptxr = ptx_make(pt_real,"1/") << nd->sources[0]->ptxr;
              break;

            case NO_GFO_Load:
              nd->ptxr =
                ptx_make_lhs(pt_real)
                << pstringf
                ( "%s[%s]",
                  single_array ? "cak__addr_base" : nd->gf->name.c_str(),
                  make_idx_nd(nd).s);
              break;

            case NO_GF_Store:
              ptx_make_opaque_eq_reg
                ( pstringf
                  ( "%s[%s]",
                    single_array ? "cak__addr_base" : nd->gf->name.c_str(),
                    make_idx_nd_axes(nd,AX_xyz).s ) )
                << nd->sources[0] << EMIT;
              break;

            case NO_Literal:
              assert(false);
              break;

            case NO_If:
              {
                assert( nd->sources == 4 );
                assert( nd->sources[0]->ptxr.rt == PT_pred );
                PTX_Reg_Type rt = pt_real; // Yuck.
                for ( int i=1; i<3; i++ )
                  if ( !nd->sources[i]->ctc_val_set )
                    rt = nd->sources[i]->ptxr.rt;
                nd->ptxr = insn_cli_op_make(rt,"")
                  << nd->sources[0]->ptxr
                  << "?" << nd->sources[1]
                  << ":" << nd->sources[2];
              }
              break;

            default:
              if ( nd->assigns_lhs() )
                nd->ptxr = ptx_make_reg(pt_real);

              ptx_linef
                ("// To Do: nt %d, no %d  fn %s  %s",
                 nd->ntype, nd->no,
                 nd->no == NO_Function ? nd->func_name.c_str() : "na",
                 nd->c_code.c_str());
              assert( false );
              break;
            }
          }

      }
  }

  ptx_indent_pop();
  ptx_line("}");
  ptx_indent_pop(); ptx_indent_pop(); ptx_indent_pop();

  ptx_indent_push_abs(0);
  ptx_indent_pop();

  ptx_body_end();

  int unused = 0;
  string unused_txt;
  for ( PTX_RR *rr: ptx_all_regs )
    if ( rr->uses == 0 ) { unused_txt += rr->txt + ", "; unused++; }

  if ( unused )
    printf("Vars created %zd, unused %d\n %s\n",
           ptx_all_regs.size(), unused, unused_txt.c_str());
  assert( unused == 0 );

  for ( PTX_RR *rr: ptx_all_regs ) delete rr;
  ptx_all_regs.clear();

}


void
Chemora_CG_Calc_Mapping::buffer_style_table_key_make()
{
  pString style_key("");
  map<string,int> key_text_map;
  map<char,int> key_abbrev_seen;

  for ( int kno = 0;  kno < num_kernels;  kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->should_run ) continue;
      ET_Components* const etc = &ki->etc;
      if ( !etc->buffer_groups.size() )
        {
          etc->style_label =
            etc->groupless_style == BS_Read_Only_Cache ? 'r' :
            etc->groupless_style == BS_Not_Cached ? 'g' : '?';
          assert( etc->style_label != '?' );
          continue;
        }

      int style_usage[BS_ENUM_SIZE];
      bzero(style_usage,sizeof(style_usage));
      vector<int> style_bgs(BS_ENUM_SIZE);

      for ( BGIter bg(etc->buffer_groups); bg; )
        {
          style_usage[bg->style] += bg->gfs.size();
          style_bgs[bg->style]++;
        }

      map<int,bool> style_sorted;
      for ( int i=0; i<BS_ENUM_SIZE; i++ )
        if ( style_usage[i] )
          style_sorted[ -( style_usage[i] * BS_ENUM_SIZE + i ) ] = true;

      if ( style_sorted.size() == 1 )
        {
          Buffer_Group* const bg = etc->buffer_groups[0];
          const bool stncl_x = bg->stncl.x();
          const bool stncl_y = bg->stncl.y();
          const bool stncl_z = bg->stncl.z();
          etc->style_label = bg->style != BS_Block ?
            buffer_style_abbr[bg->style][0] :
            stncl_x && !stncl_y && !stncl_z ? 'x' :
            stncl_x && ( stncl_y || stncl_z ) ? 'B' : 'b';
          continue;
        }

      pString key_str;
      for ( map<int,bool>::iterator it = style_sorted.begin();
            it != style_sorted.end(); ++it )
        {
          const int key = -it->first;
          const int style = key % BS_ENUM_SIZE;
          const int ngfs = key / BS_ENUM_SIZE;
          key_str.sprintf
            ("%d-%d-%s  ", ngfs, style_bgs[style], buffer_style_str[style]);
        }
      int& idx = key_text_map[key_str.ss()];
      if ( idx == 0 )
        {
          idx = key_text_map.size();
          style_key.sprintf(" %c: %s\n", '0' + idx, key_str.s);
        }
      etc->style_label = '0' + idx;
    }
  table_style_key = style_key.s;
}

void
Chemora_CG_Calc_Mapping::report_compile_print()
{
  pString report;
  double ts_insn_tot = 0, ts_data_tot = 0, ts_latency_tot = 0;
  double amt_elts_tot = 0;
  int issue_usage_tot = 0, ops_tot = 0;
  int live_k = 0;
  const int register_size_bytes = 4;

  const bool target_cuda = !c->opt_code_target_acc;
  const int num_kernels_cuda = target_cuda ? num_kernels : 0;

  report.sprintf("Resource usage for %s c%d\n",
                 c->calc_name.c_str(), config_version);

  if ( !target_cuda )
    report.sprintf(" -- Compile info for non-CUDA target not yet available.\n");

  // Possibly use the Elts (elements) column for some other purpose.
  // Intended for debugging or tuning.
  const bool elt_col = false;

  pTable cr;

  for ( int kno = 0;  kno < num_kernels_cuda;  kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->should_run ) continue;

      Function_Info* const fi = ki->fi;
      ET_Components* const etc = &ki->etc;
      Tile& tile = etc->tile;
 
      live_k++;

#define T(m) m##_tot += etc->m;
      T(ts_insn); T(ts_data); T(ts_latency); T(amt_elts);
      T(issue_usage); T(ops);
#undef T
      const int regs_per_thd = fi->num_regs_get();
      const int blocks_per_mp = fi->blocks_per_mp_get();
      const int max_threads = fi->max_threads_get();

      cr.row_start();
      cr.entry("KNo"," %2d:",kno);
      pStringF rpt_prefix("%3d,%2d,%2d", tile.x, tile.y, tile.z);
      cr.entry("Tile",rpt_prefix.s);
      cr.entry("N","%1d",blocks_per_mp);
      cr.entry("Itr","%3d",max(tile.yy, tile.zz));
      cr.entry("D","%1s", tile.yy > tile.zz ? "y" : "z");
      cr.entry("B",etc->style_label);

      cr.entry("IS","%4.1f/", etc->issue_usage);
      cr.entry("Insn","%4d", etc->ops);
      if ( elt_col )
        cr.entry("Amt","%5.1f",etc->amt_elts);
      else
        cr.entry("OvRe","%5s",
                 etc->over_regs
                 ? pstringf("%+4d",etc->over_regs)
                 : pstringf("=%4d",etc->regs_per_thd_guess));

      cr.entry("ST","%3d", etc->simple_st);
      cr.entry("IKS","%3d", etc->ik_st);
      cr.entry("Rat","%4.2f",
               double(regs_per_thd) / max(1,etc->regs_per_thd_guess));
      cr.entry("Regs","%3d", regs_per_thd);
      cr.entry("L32","%3d", ki->ptx_nlive_32 );
      cr.entry("Live","%3d", ki->live_reg_max);
      cr.entry("Thds","%4d", max_threads);
      cr.entry("LoRe","%4d", fi->local_size_bytes_get() / register_size_bytes);
    }

  report += cr.body_get();

  if ( table_style_key.size() ) report += table_style_key;

  c->mi->dm->msg_tune("%s", report.s);

  c->ct->code_compile_report_str += report;

}

void
Chemora_CG_Calc_Mapping::report_print()
{
  pString report;
  double ts_insn_tot = 0, ts_data_tot = 0, ts_latency_tot = 0;
  double ts_insn_lim_fp_tot = 0, ts_data_lim_tot = 0;
  double amt_elts_tot = 0;
  double total_time_ms_tot = 0, scaled_time_tot = 0, abs_err_tot = 0;
  int ops_tot = 0;
  int live_k = 0;
  CaCUDA_Kernel_Launch_Parameters& klp = modd->mi->klp;
  CaCUDALib_GPU_Info& gi = *c->gpu_info;
  int num_gp = klp.i_length_x * klp.i_length_y * klp.i_length_z;
  int num_launches = modd->mi->num_launches;
  double gp_scale = 1e-3 * gi.clock_freq_hz
    * gi.num_multiprocessors / ( double(num_gp) * num_launches );
  const int assumed_elt_size = sizeof(CCTK_REAL);

  double total_time_ms_max = 0;

  report_started = true;

  for ( int kno = 0;  kno < num_kernels;  kno++ )
    {
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->should_run ) continue;

      Function_Info* const fi = ki->fi;
      set_max(total_time_ms_max,fi->total_time_ms());
    }

  const int unit_idx = 
    total_time_ms_max < 1.0 ? 0 : total_time_ms_max < 1000.0 ? 1 : 2;
  const char* unit_str_a[] = { "µs", "ms", "s" };
  const double unit_mul_a[] = { 1e3, 1.0, 1e-3 };
  const char* const unit_str = unit_str_a[unit_idx];
  const double unit_mul = unit_mul_a[unit_idx];
  const int64_t num_gpt = int64_t(num_gp) * modd->mi->num_launches;
  pStringF rt_head("MT/%s",unit_str);

  pString resource_data;

  const bool show_ut = false;
  const bool show_f_pen = false;

  map<double,string> rpt_lines;

  pString header, line, body;
  int num_lines = 0;

  auto entry = [&](const char* h, const char* fmt, double val)
    {
      pStringF row(fmt,val);
      const char* sep = line.len() ? " " : "";
      line += sep + row;
      if ( num_lines ) return;
      header.sprintf("%s%*s",sep,row.len(),h);
    };

  auto entrys = [&](const char* h, const char* fmt, const char* val)
    {
      pStringF row(fmt,val);
      const char* sep = line.len() ? " " : "";
      line += sep + row;
      if ( num_lines ) return;
      header.sprintf("%s%*s",sep,row.len(),h);
    };

  for ( int kno = 0;  kno < num_kernels;  kno++ )
    {
      if ( kno == 0 )
        report.sprintf
          ("%2s:%8s %2s %2s %4s "
           "%4s %5s  "
           "%5s %4s %5s  "
           "%5s  %5s %8s %5s\n",
           "", "Tile", "Na", "Np", "Iter",
           show_ut ? "UT" : show_f_pen ? "FPen" : "L-FP",
           show_ut ? "BL" : show_f_pen ? "LdLat" : "L-Dat",
           "Issue", "Data", "Lat",
           "ET", "SMT", rt_head.s, "Ratio");
      Chemora_CG_Kernel_Info* const ki = &kernel_info[kno];
      if ( !ki->should_run ) continue;

      Function_Info* const fi = ki->fi;
      ET_Components* const etc = &ki->etc;
      Tile& tile = etc->tile;
      const char* const kernel_name = fi->function_name.c_str();

      live_k++;

      const double total_time_ms = fi->total_time_ms();
      double scaled_time = total_time_ms * gp_scale;

#define T(m) m##_tot += etc->m;
      T(ts_insn); T(ts_data); T(ts_latency); T(amt_elts);
      T(ops); T(ts_insn_lim_fp); T(ts_data_lim);
#undef T
      total_time_ms_tot += total_time_ms;
      scaled_time_tot += scaled_time;

      pStringF tile_str("%d,%d,%d",tile.x, tile.y, tile.z);

      const double et_pred_rat = scaled_time / ( etc->et ?: 1.0 );
      abs_err_tot += fabs( scaled_time - etc->et );

      pStringF prefix("%2d:%8s", kno, tile_str.s);

      rpt_lines[kno] = pstringf
        ("%s %2d %2s %2d%1s%1c "
         "%4.1f %5.1f  "
         "%5.1f %4.1f %5.1f  "
         "%5.1f  %5.1f %8.3f %5.2f\n",
         prefix.s,
         fi->blocks_per_mp_get(),
         etc->bl_per_mp_hint_used ? "h" : pstringf("%2d",etc->blocks_per_mp),
         max(tile.yy, tile.zz), tile.yy > tile.zz ? "y" : "z",
         etc->style_label,

         show_ut ? 100.0 * etc->utilization_slots :
         show_f_pen ? 10*etc->ts_f_penalty : etc->ts_insn_lim_fp,
         show_ut ? 100.0 * etc->load_balance :
         show_f_pen ? 10*etc->ts_load_latency : etc->ts_data_lim,

         etc->ts_insn, etc->ts_data, etc->ts_latency,
         etc->et,
         scaled_time,
         total_time_ms * unit_mul,
         et_pred_rat
         );

      NPerf_Status nperf_status = NPerf_metric_data_status(kernel_name);

      if ( nperf_status == NPerf_Status_OK )
        {
          auto met_get = [&](const char* metric_name)
            { return NPerf_metric_value_get(kernel_name,metric_name); };

          const double width = 60;

          // Normalize some statistics to an "iter". An "iter" is the
          // time needed to compute one grid point, plus a fraction of
          // the pre-loop instructions. It is computed assuming that
          // all active blocks execute concurrently.

          const int block_size = tile.x * tile.y * tile.z;
          const double wp_util = met_get("warp_execution_efficiency") / 100;
          const double wp_util_inv = 1.0 / ( wp_util ?: 1.0 );
          const int blocks_per_mp =
            c->opt_force_one_block_per_mp ? 1 : fi->blocks_per_mp_get();
          const int thread_overlap_mp = blocks_per_mp * block_size;
          const int thread_overlap = thread_overlap_mp * gi.num_multiprocessors;
          const double s_per_iter =
            total_time_ms * 1e-3 * thread_overlap / ( num_launches * num_gp );
          const double wid_per_s = width / s_per_iter;
          const double cyc_per_iter = s_per_iter * gi.clock_freq_hz;
          const double cyc_per_char = cyc_per_iter / width;
          const double char_per_cyc = width / cyc_per_iter;

          const char* const stalls[] =
            {
              "Istall_inst_fetch",
              "Pstall_pipe_busy",
              "nstall_not_selected",
              "dstall_exec_dependency",
              "Cstall_constant_memory_dependency",
              "Ystall_sync",
              "Xstall_texture",
              "Mstall_memory_dependency",
              "Tstall_memory_throttle",
              "Ostall_other" };

          const double inst_executed = met_get("inst_executed"); // Warps.
          const double inst_issued = met_get("inst_issued");     // Warps.

          const double issued_per_exec = inst_issued / max(1.0,inst_executed);

          const double iter_insn_per_total_insn =
            double(thread_overlap_mp) / num_gp;
          string tryout_exec_segs, tryout_rply_segs;
          double inst_exec_examined = 0;
          double inst_exec_issue_cyc_examined = 0;
          double inst_rply_issue_cyc_examined = 0;

          auto tryout_make = [&](const char* mname, char sym, int rate)
            {
              assert( rate );
              const double insn_iter =
                 met_get(mname) * wp_util_inv * iter_insn_per_total_insn;
              inst_exec_examined += insn_iter;
              const double inst_exec_issue_cyc = insn_iter / rate;
              inst_exec_issue_cyc_examined += inst_exec_issue_cyc;
              tryout_exec_segs +=
                string(0.5+inst_exec_issue_cyc*char_per_cyc,sym);
              const double inst_rply_issue_cyc =
                inst_exec_issue_cyc * max(0.0,issued_per_exec-1);
              inst_rply_issue_cyc_examined += inst_rply_issue_cyc;
              tryout_rply_segs +=
                string(0.5+inst_rply_issue_cyc*char_per_cyc,sym-32);
            };

          tryout_make("inst_compute_ld_st",'m',gi.dev_cap.fu_ls);
          tryout_make("inst_fp_32",'s',gi.dev_cap.fu_sp);
          tryout_make("inst_fp_64",'d',gi.dev_cap.fu_dp);
          tryout_make("inst_integer",'i',gi.dev_cap.fu_int_al);

          // Need to replace this when individual instruction replays used.
          double inst_rply_examined =
            inst_exec_examined * max(0.0, issued_per_exec-1);

          auto tryout_other =
            [&](string& tryout_segs, double inst_wps,
                double& inst_examined,
                double& inst_issue_cyc_examined )
            {
              const double inst_exec_iter =
                inst_wps * 32 * iter_insn_per_total_insn;
              const double inst_exec_other =
                inst_exec_iter - inst_examined;
              assert( inst_exec_other >= 0 );
              const double inst_exec_other_cyc =
                inst_exec_other / gi.dev_cap.fu_int_al;
              inst_issue_cyc_examined += inst_exec_other_cyc;
              const int inst_exec_wid =
                0.5 + inst_issue_cyc_examined * char_per_cyc;
              const int inst_exec_other_wid =
                inst_exec_wid - tryout_segs.length();
              if ( inst_exec_other_wid > 0 )
                tryout_segs += string(inst_exec_other_wid,'o');
            };
          tryout_other
            (tryout_exec_segs, inst_executed, inst_exec_examined,
             inst_exec_issue_cyc_examined);
          tryout_other
            (tryout_rply_segs, inst_issued - inst_executed, inst_rply_examined,
             inst_rply_issue_cyc_examined);

          string tryout_ruler = tryout_exec_segs + tryout_rply_segs;

          const double inst_issue_cyc_total =
            inst_exec_issue_cyc_examined + inst_rply_issue_cyc_examined;

          const double stall_cyc = cyc_per_iter - inst_issue_cyc_total;

          pVector<string> slot_usages;

          slot_usages += string(tryout_exec_segs.length(),'-');
          slot_usages += string(tryout_rply_segs.length(),'+');

          const double mod_scale =
            width / ( et_pred_rat * etc->et * etc->load_balance );

          const double issue_iter_mod_cyc = etc->ts_insn * thread_overlap_mp;
          const int issue_iter_mod_char = issue_iter_mod_cyc / cyc_per_char;

          const int issue_wid_mod_raw = etc->ts_insn_ut * mod_scale;
          const int data_wid_mod_raw = etc->ts_data * mod_scale;
          const int other_wid_mod_raw =
            width / et_pred_rat - issue_wid_mod_raw - data_wid_mod_raw;
          const int other_wid_mod = max(0, other_wid_mod_raw);
          const int other_wid_mod_extra = -min(0, other_wid_mod_raw);
          const int issue_wid_mod = issue_wid_mod_raw - other_wid_mod_extra;
          const int data_wid_mod = data_wid_mod_raw - other_wid_mod_extra;

          string slot_usage_mod =
            string(issue_wid_mod, '-')
            + string( other_wid_mod_extra, 'm' )
            + string
            ( max(0, width
                  - issue_wid_mod - data_wid_mod - other_wid_mod
                  - other_wid_mod_extra), ' ')
            + string( other_wid_mod, 'd' )
            + string( data_wid_mod, 'M' );

          const double stall_to_char = width * stall_cyc / cyc_per_iter / 100;

          double stall_pct_missing = 100;
          for ( const char* mname : stalls )
            {
              const double stall_pct = met_get(&mname[1]);
              stall_pct_missing -= stall_pct;
              const double seg_wid = stall_pct * stall_to_char;
              const int seg_wid_q = 0.5 + seg_wid;
              if ( !seg_wid_q ) continue;
              slot_usages += string(seg_wid_q,mname[0]);
            }

          assert( stall_pct_missing >= -0.001 );
          const int seg_stall_missing_wid_q =
            0.5 + stall_pct_missing * stall_to_char;
          if ( seg_stall_missing_wid_q )
            slot_usages += string(seg_stall_missing_wid_q,'?');

          string *longest = NULL;
          int slot_usage_len = 0;
          for ( string& segment : slot_usages )
            {
              slot_usage_len += segment.length();
              if ( !longest || segment.length() > longest->length() )
                longest = &segment;
            }
          const int slot_usage_err = slot_usage_len - width;
          const int longest_len_new = longest->length() - slot_usage_err;
          assert( longest_len_new >= 0 );
          if ( slot_usage_err )
            *longest = string(longest_len_new,longest->data()[0]);
          string slot_usage;
          for ( string& str : slot_usages ) slot_usage += str;

          const double data_r_x = met_get("dram_read_transactions");

          // Divide by two because it *seems* to be twice as large as it should.
          const double data_w_x = met_get("dram_write_transactions")/2.0;
          const int trans_size_bytes = 32;
          const double data_bytes = trans_size_bytes * ( data_r_x + data_w_x );
          const double data_bytes_pt = data_bytes / num_gp;
          const double model_data_bytes_pt = etc->amt_elts * assumed_elt_size;
          const double data_pt_ratio =
            data_bytes_pt / max(1,model_data_bytes_pt);
          const double l2_bytes_pt =
            double(trans_size_bytes) / num_gp *
            ( met_get("l2_read_transactions")
              + met_get("l2_write_transactions") );
          const double l2_pt_ratio = l2_bytes_pt / max(1,model_data_bytes_pt);

          const int sh_ops = etc->sh_ld_pt + etc->buf_ls_pt;
          const int mod_num_insn = etc->ops + sh_ops + etc->gls_insn;
          double insn_ratio =
            32 * inst_executed / ( num_gp * max(1,mod_num_insn) );

          const double gl_mem_lat_s = gi.gl_mem_lat_cyc.val / gi.clock_freq_hz;
          const double gl_mem_lat_wid = gl_mem_lat_s * wid_per_s;
          assert( gl_mem_lat_wid <= width );
          const double lat_mod_s =
            etc->ts_latency * block_size * etc->blocks_per_mp
            / gi.clock_freq_hz;
          pStringF gl_lat_ruler
            ("%s  %s  Model * %6.3f  (%6.2f) cyc/char %.1f",
             string(issue_iter_mod_char,'-').c_str(),
             string(gl_mem_lat_wid+0.5,'G').c_str(),
             lat_mod_s/gl_mem_lat_s, etc->chained_lds, cyc_per_char);

          entrys("Tile","%s",prefix.s);
          entry("LD", "%2.0f", etc->unique_stencil_ld+etc->ik_ld);
          entry("ST", "%2.0f", etc->simple_st+etc->ik_st);
          entry("I/E", "%4.2f", issued_per_exec);
          entry("IsUt", "%4.1f", met_get("issue_slot_utilization"));
          entry("Wp/Cy", "%5.2f", met_get("eligible_warps_per_cycle"));
          entry("WpEf", "%4.2f", met_get("warp_nonpred_execution_efficiency"));
          entry("L2L1", "%5.1f", met_get("l2_l1_read_hit_rate"));
          entry("ROC", "%5.1f", met_get("nc_cache_global_hit_rate"));
          entry("IRat", "%4.2f", insn_ratio);
          entry("LdEf", "%4.2f", met_get("gld_efficiency"));
          entry("L2Rat", "%5.2f", l2_pt_ratio);
          entry("DRat", "%5.2f", data_pt_ratio);
          entry("Ratio", "%5.2f", et_pred_rat);
          num_lines++;
          body += line + "\n"; line = "";
          body += prefix + "  " + slot_usage.c_str() + "\n";
          body += prefix + "  " + slot_usage_mod.c_str() + "\n";
          body += prefix + "  " + gl_lat_ruler.s + "\n";
          body += prefix + "  " + tryout_ruler.c_str() + "\n";
        }
    }

  for ( map<double,string>::iterator it = rpt_lines.begin();
        it != rpt_lines.end(); it++ )
    report += it->second;

  const double abs_err_ratio = abs_err_tot / ( t_scaled?: 1 );

  report.sprintf
    (" Total of %2d (%5d)  %5.1f %5.1f  %5.1f %4.1f %5.1f  "
     "%5.1f  %5.1f %8.3f %5.2f\n",
     live_k, modd->mi->num_launches,
     ts_insn_lim_fp_tot, ts_data_lim_tot,
     ts_insn_tot, ts_data_tot, ts_latency_tot, t_scaled,
     scaled_time_tot,
     total_time_ms_tot * unit_mul,
     scaled_time_tot >= t_scaled ? 1 + abs_err_ratio : 1 - abs_err_ratio );

  if ( num_lines )
    report += header + "\n" + body;

  if ( table_style_key.size() ) report += table_style_key;

  const bool show_sizes = true;
  Node* const nd_dx = c->name_to_node.lookup("dx");
  Node* const nd_delta_time = c->name_to_node.lookup("dt");

  if ( c->config_version > 1 || show_sizes )
    report.sprintf
      (" Local grid %d, %d, %d.  Interior %d x %d x %d = %d.  IL %ld\n",
       klp.cagh_ni, klp.cagh_nj, klp.cagh_nk,
       klp.i_length_x, klp.i_length_y, klp.i_length_z,
       num_gp, num_gpt);
  if ( c->config_version > 1 )
    report.sprintf
      (" dx %.3f (%s)  xmin %.3f  delta_time %.6f (%s)\n",
       klp.cagh_dx, nd_dx->visited ? "used" : "unused",
       klp.cagh_xmin, klp.cagh_dt, nd_delta_time->visited ? "used" : "unused");

  const double ins_inv =
    1.0 / max(1.0,0.0 + c->fp_ops + c->fp_not + c->fp_other);
  report.sprintf(" FP Rate %.3f GFLOP/s  FP %.1f%%  FP ignored %.1f%%\n",
                 1e-9 * num_gpt * c->fp_ops / ( 1e-3 * total_time_ms_tot ),
                 100.0 * c->fp_ops * ins_inv,
                 100.0 * c->fp_other * ins_inv
                 );

  pString icache;
  if ( c->opt_icache_size == 0 )
    icache = "ignore";
  else
    icache.sprintf("%d kiB",c->icache_bytes>>10);

  report.sprintf
    (" ROC %d kiB  Options: (s,l, x,y,z) (%c,%c, %c,%c,%c) NK %s %d %s "
     " Red %d  CSE %d\n Sched Latency %d  Small Blocks %d  B/MP H %d "
     " I/E %3.1f  H2 %d  IC %s\n",
     gi.roc_sz_bytes >> 10,
     c->opt_buf_single_use, c->opt_buf_low_dim,
     c->opt_buf_sten_x, c->opt_buf_sten_y, c->opt_buf_sten_z,
     c->opt_move_constraint == '0' ? "<=" : "==", c->opt_num_kernels,
     c->opt_move_constraint == 's' ? "peg strs"
     : c->opt_move_constraint == '0' ? "peg none" : "peg kern",
     c->opt_reduce_emit_dataflow, c->opt_cse,
     c->opt_sched_latency, c->opt_small_block,
     c->opt_block_occ_no_hint_threshold, c->opt_issue_per_exec,
     c->opt_halo2, icache.s);
  report.sprintf
    (" H2 %d  ABS %d %d%d  RMAX %d OPT %d\n",
     c->opt_halo2, c->opt_array_bases_share,
     c->opt_4Gx_okay, c->opt_addr_literals, c->opt_addr_regs_max,
     c->opt_addr_regs_opt);
  report.sprintf
    (" Code %s  Kernel mapping opt: %d cyc * %.1f * min(nnodes,%d)\n",
     c->opt_code_target_acc ? "ACC" : "PTX",
     c->opt_opt_num_cycles, c->opt_opt_props_per_node_per_cycle,
     c->opt_opt_nodes_max);
  if ( c->opt_verify_buffering )
    report.sprintf
      (" ** Buffering verification is on, this hurts performance.\n"
       "    If you can read this, buffered values are correct. If they\n"
       "    were not, program would have exited with a memory error.\n"
       "    Set experimental parameter verify_buffering=0 to turn off.\n");

  modd->mi->dm->msg_tune
    ("Chemora Code Execution Report for %s c%d\n%s",
     c->calc_name.c_str(), config_version, report.s);

  modd->mi->dm->report_total_et += total_time_ms_tot;
}


void
Chemora_CG_Calc::report_print()
{
  mi = NULL; // Catch errors.
  for ( auto moddi: modd_vector ) moddi->m->report_print();
}

void
Chemora_CG_Calc::report_compile_print()
{
  for ( auto moddi: modd_vector ) moddi->m->report_compile_print();
}


Linger_Char linger_char;


Chemora_CG_Calc*
Chemora_CG_Thorn::calc_new(const char* calc_name)
{
  DECLARE_CCTK_PARAMETERS;
  if ( use_kranc_c ) return NULL;
  assert( !chemora_calcs.count(calc_name) );
  Chemora_CG_Calc* const c = &chemora_calcs[calc_name];
  c->init(this,calc_name,&diff_operations_calc);
  return c;
}

Chemora_CG_Calc*
Chemora_CG_Thorn::calc_get_try(const char* calc_name)
{
  Chemora_CG_Calcs::iterator iter = chemora_calcs.find(calc_name);
  if ( iter == chemora_calcs.end() ) return NULL;
  return &iter->second;
}

Chemora_CG_Calc*
Chemora_CG_Thorn::calc_get(const char* calc_name)
{
  Chemora_CG_Calc* const rv = calc_get_try(calc_name);
  assert( rv );
  return rv;
}

void
Chemora_CG_Thorn::init(Chemora_CG *chemorap, const char* thorn_namep)
{
  DECLARE_CCTK_PARAMETERS;
  chemora = chemorap;
  thorn_name = thorn_namep;
  mi = NULL;
  if ( use_kranc_c ) return;
  diff_operations_calc.init(this,"# Differencing Operations",NULL);
}

Chemora_CG::Chemora_CG()
{
  at_cuda_ctx_available_ran = false;
  first_thorn_init_time_s = 0;

  // Make sure that packed stencil type is large enough.
  const int spacked_val_bits =
    spacked_special_bits + stencil_pattern_bits + stencil_radius_bits * 6;
  const int spacked_var_bits = 8 * sizeof(Stncl_Packed);
  assert( spacked_var_bits >= spacked_val_bits );

  static_op_info_lt_inited = false;
  static_op_info_op_init();

  assert( string("SIZE") == node_operation_str[NO_SIZE] );

  rand_seed_set = false;
  rand_seed_printed = false;
}

void
Chemora_CG::static_op_info_op_init()
{
  static_op_info_new("pow", NO_Power,"","pow");
  static_op_info_alias_set(NO_Power,"Power");
  static_op_info_new("Abs", NO_Abs, "abs","fabs");
  static_op_info_new("ChemoraOpAnd", NO_And, "and.pred","&&");
  static_op_info_new("ChemoraOpOr", NO_Or, "or.pred","||");
  static_op_info_new("Not", NO_Not, "not.pred","!");
  static_op_info_new("ChemoraOpNotEqual", NO_NotEqual,"setp.ne","!=");
  static_op_info_new("ChemoraOpEqual", NO_Equal, "setp.eq","==");
  static_op_info_new(NO_If, "selp");
  static_op_info_new("Less", NO_Less, "setp.lt","<");
  static_op_info_new("LessEqual", NO_LessEqual, "setp.le","<=");
  static_op_info_new("Greater", NO_Greater, "setp.gt",">");
  static_op_info_new("GreaterEqual", NO_GreaterEqual,"setp.ge",">=");
  static_op_info_new("copysign", NO_CopySign,"copysign","copysign");
  static_op_info_new(NO_Negate, "neg","-");
  static_op_info_new("Max", NO_Max, "max","max");
  static_op_info_new("Min", NO_Min, "min","min");
  static_op_info_new("Times", NO_Product, "mul","*");
  static_op_info_new("Rational", NO_Divide, "div","/");
  static_op_info_new("Sin", NO_Sin,"","sin");
  static_op_info_new("Cos", NO_Cos,"","cos");
  static_op_info_new("Plus", NO_Sum, "add","+");
  static_op_info_new(NO_Reciprocal, "rcp.rn");
  static_op_info_new(NO_SquareRoot, "sqrt.rn","sqrt");

  Node_Operation are_c[]
    = { NO_And, NO_Or, NO_Max, NO_Min, NO_Sum, NO_Product };
  for ( int i=0; i<sizeof(are_c)/sizeof(are_c[0]); i++ )
    static_op_info_get(are_c[i])->is_commutative = true;
}

void
Chemora_CG::static_op_info_lt_init(CaCUDALib_GPU_Info *gi)
{
  // Note: Re-initialized each time because some calculations might use
  // static gpu_info and others might use dynamically collected gpu_info.
  static_op_info_lt_inited = true;

  ptx_mnem_ld_roc = "ld.global.nc";
  ptx_mnem_ld_global = "ld.global";
  ptx_mnem_st_global = "st.global";
  ptx_mnem_ld_generic = "ld";
  ptx_mnem_st_generic = "st";

  ptx_mnem_ld_single = ptx_mnem_ld_global;
  ptx_mnem_ld_offset = ptx_mnem_ld_global;
  ptx_mnem_ld_sm_populate = ptx_mnem_ld_global;
  ptx_mnem_ld_ik = ptx_mnem_ld_global;
  ptx_mnem_st_ik = ptx_mnem_st_global;

  const bool assume_dp = sizeof(CCTK_REAL) == 8;
  MB_Value* const op_lat =
    assume_dp ? gi->dp_fp_op_lat_cyc : gi->sp_fp_op_lat_cyc;
  MB_Value* const op_thpt =
    assume_dp ? gi->dp_fp_thpt_op_p_cyc : gi->sp_fp_thpt_op_p_cyc;

  const double lat_default = op_lat[FP_MADD].val;

# define LTS(no,mbop) \
  assert( op_thpt[mbop].val ); \
  static_op_info_lt_set(no, op_lat[mbop].val, op_thpt[mbop].val);

  LTS(NO_Sum, FP_MADD);
  LTS(NO_Product, FP_MADD);
  LTS(NO_Divide, FP_DIV);
  LTS(NO_Reciprocal, FP_RECIP);
  LTS(NO_SquareRoot, FP_SQRT);
  LTS(NO_Power, FP_POW);
  LTS(NO_Cos, FP_SIN);
  LTS(NO_Sin, FP_SIN);

#undef LTS

  const double copysign_ops = 2;
  static_op_info_lt_set
    (NO_CopySign, copysign_ops * lat_default,
     gi->dev_cap.fu_int_al/copysign_ops);

  static_op_info_lt_set(NO_If, lat_default, gi->dev_cap.fu_int_al);
  static_op_info_lt_set(NO_Negate,0,0);
  static_op_info_lt_set(NO_Abs,0,0);
  static_op_info_lt_set(NO_Min,lat_default,op_thpt[FP_MADD].val);
  static_op_info_lt_set(NO_Max,lat_default,op_thpt[FP_MADD].val);

  for ( int no_logic = NO_BOOLEAN_START+1;
        no_logic < NO_BOOLEAN_END; no_logic++ )
    static_op_info_lt_set
      (Node_Operation(no_logic), lat_default, gi->dev_cap.fu_int_al);
}

Static_Op_Info*
Chemora_CG::static_op_info_new
(Node_Operation no, string ptx_mnemonic, string c_op)
{
  Static_Op_Info* const soi = &static_op_info[no];
  assert( soi->no == NO_Unset );
  soi->no = no;
  soi->ptx_mnemonic = ptx_mnemonic;
  static_op_info_c_op_set(soi,c_op);
  return soi;
}

void
Chemora_CG::static_op_info_c_op_set(Static_Op_Info *soi, string c_op)
{
  soi->c_op_or_func = c_op;
  if ( c_op == "" ) return;
  // Note: Red Hat Legacy Linux 7.3 uses a version of gcc that doesn't
  // correctly implement C++11 regex.
  //  soi->is_c_operator = regex_search(c_op,regex("^[a-zA-Z_]"));
  const int c = c_op[0];
  soi->is_c_operator = isalpha(c) || c == '_';
}

void
Chemora_CG::static_op_info_lt_set
(Node_Operation no, double latency_cyc, double througput_cps)
{
  Static_Op_Info* const soi = &static_op_info[no];
  assert( soi->no == no );
  soi->latency = latency_cyc;
  soi->throughput = througput_cps;
  soi->lt_set = true;
}

Static_Op_Info*
Chemora_CG::static_op_info_new
(const char *kranc_name, Node_Operation no, string ptx_mnemonic, string c_op)
{
  Static_Op_Info* const no_soi = static_op_info_lookup(kranc_name);
  assert( !no_soi );
  Static_Op_Info* const soi = &static_op_info[no];
  assert( soi->kranc_name.size() == 0 );
  assert( soi->no == NO_Unset );
  static_op_info_map[kranc_name] = soi;
  soi->kranc_name = kranc_name;
  soi->no = no;
  soi->ptx_mnemonic = ptx_mnemonic;
  static_op_info_c_op_set(soi,c_op);
  return soi;
}

void
Chemora_CG::static_op_info_alias_set
(Node_Operation no, const char *kranc_name)
{
  Static_Op_Info* const no_soi = static_op_info_lookup(kranc_name);
  assert( !no_soi );
  Static_Op_Info* const soi = &static_op_info[no];
  assert( soi->kranc_name.size() != 0 );
  static_op_info_map[kranc_name] = soi;
}

Static_Op_Info*
Chemora_CG::static_op_info_lookup(const char *kranc_name)
{
  Static_Op_Info_Map::iterator iter = static_op_info_map.find(kranc_name);
  if ( iter == static_op_info_map.end() ) return NULL;
  return iter->second;
}

Static_Op_Info*
Chemora_CG::static_op_info_get(Node_Operation no)
{ return &static_op_info[no]; }

void
Chemora_CG::at_cuda_ctx_available(bool opt_metrics_extra)
{
  if ( at_cuda_ctx_available_ran ) return;
  at_cuda_ctx_available_ran = true;
  NPerf_metric_collect("shared_efficiency");
  NPerf_metric_collect("sm_efficiency");
  NPerf_metric_collect("stall_memory_throttle");
  NPerf_metric_collect("stall_memory_dependency");
  NPerf_metric_collect("stall_constant_memory_dependency");
  NPerf_metric_collect("stall_memory_throttle");
  NPerf_metric_collect("stall_not_selected");
  NPerf_metric_collect("stall_pipe_busy");
  NPerf_metric_collect("stall_texture");
  NPerf_metric_collect("stall_sync");
  NPerf_metric_collect("stall_inst_fetch");
  NPerf_metric_collect("stall_exec_dependency");
  NPerf_metric_collect("stall_other");

  NPerf_metric_collect("inst_misc");
  NPerf_metric_collect("inst_executed");
  NPerf_metric_collect("inst_integer");
  NPerf_metric_collect("inst_control");
  NPerf_metric_collect("inst_issued");
  NPerf_metric_collect("inst_per_warp");
  NPerf_metric_collect("inst_compute_ld_st");
  NPerf_metric_collect("inst_fp_32");
  NPerf_metric_collect("inst_fp_64");
  NPerf_metric_collect("flop_count_sp_special");
  NPerf_metric_collect("flop_count_dp_fma");
  NPerf_metric_collect("flop_count_dp");

  NPerf_metric_collect("global_cache_replay_overhead");
  NPerf_metric_collect("global_replay_overhead");
  NPerf_metric_collect("shared_replay_overhead");

  NPerf_metric_collect("dram_write_transactions");
  NPerf_metric_collect("dram_read_transactions");
  NPerf_metric_collect("l2_read_transactions");
  NPerf_metric_collect("l2_write_transactions");

  NPerf_metric_collect("issue_slot_utilization");
  NPerf_metric_collect("eligible_warps_per_cycle");
  NPerf_metric_collect("warp_nonpred_execution_efficiency");

  NPerf_metric_collect("l2_l1_read_hit_rate");
  NPerf_metric_collect("nc_cache_global_hit_rate");
  NPerf_metric_collect("gld_efficiency");
  NPerf_metric_collect("warp_execution_efficiency");

  if ( !opt_metrics_extra ) return;


  NPerf_metric_collect("branch_efficiency");
  NPerf_metric_collect("inst_replay_overhead");

  NPerf_metric_collect("dram_read_throughput");
  NPerf_metric_collect("dram_utilization");
  NPerf_metric_collect("dram_write_throughput");
  NPerf_metric_collect("flop_count_dp");
  NPerf_metric_collect("ipc");
  NPerf_metric_collect("issue_slot_utilization");
  NPerf_metric_collect("issue_slots");
  NPerf_metric_collect("issued_ipc");
  NPerf_metric_collect("l1_cache_global_hit_rate");
  NPerf_metric_collect("l2_l1_read_throughput");
  NPerf_metric_collect("l2_l1_read_transactions");
  NPerf_metric_collect("l2_l1_write_transactions");
  NPerf_metric_collect("l2_read_throughput");
  NPerf_metric_collect("l2_utilization");
  NPerf_metric_collect("local_load_transactions");
  NPerf_metric_collect("local_store_transactions");
  NPerf_metric_collect("nc_l2_read_transactions");
  NPerf_metric_collect("sysmem_read_transactions");
  NPerf_metric_collect("sysmem_write_transactions");
  NPerf_metric_collect("sysmem_utilization");

}

Chemora_CG_Thorn*
Chemora_CG::thorn_new(const char* thorn_name)
{
  assert( !chemora_thorns.count(thorn_name) );
  Chemora_CG_Thorn* const ct = &chemora_thorns[thorn_name];
  ct->init(this,thorn_name);
  if ( first_thorn_init_time_s == 0 )
    first_thorn_init_time_s = time_wall_fp();
  return ct;
}

Chemora_CG_Thorn*
Chemora_CG::thorn_get_try(const char* thorn_name)
{
  Chemora_CG_Thorns::iterator iter = chemora_thorns.find(thorn_name);
  if ( iter == chemora_thorns.end() ) return NULL;
  return &iter->second;
}

Chemora_CG_Thorn*
Chemora_CG::thorn_get(const char* thorn_name)
{
  Chemora_CG_Thorn* const rv = thorn_get_try(thorn_name);
  assert( rv );
  return rv;
}

void
Chemora_CG::report_print()
{
  for ( ThIter th(chemora_thorns); th; ) th->report_print();
}

void
Chemora_CG_Thorn::report_print()
{
  if ( !mi ) return;  // For example, if Chemora was never actually used.
  mi->dm->msg_tune
    ("Chemora Code Compilation Report\n%s",
     code_compile_report_str.c_str());

  for ( CIter c(chemora_calcs); c; ) c->report_print();
}


Chemora_CG chemora_global_static;
Chemora_CG *chemora_global = &chemora_global_static;
