// -*- c++ -*-
#ifndef LAUNCH_H
#define LAUNCH_H
#include <cctk.h>
#include <string>

#define UN __attribute__ ((unused))

// Define markers to be placed in code next to variables which need to
// be compile-time constants. The source code here assigns a
// placeholder value to these variables (1). The preprocessed code
// will be read by the cactus executable and, using the markers, the
// placeholder values will be replaced by actual values. The
// preprocessed file will then be compiled.
//
#define REPL_TARGET_PREFIX const int UN QQQ_start_marker_
#define REPL_TARGET_MARKER(f) REPL_TARGET_MARKER_1(f)
#define REPL_TARGET_MARKER_1(f) const int UN QQQ_start_marker_##f=0;
#define REPL_TARGET_END const int UN QQQ_end_marker = 0;
#define REPL_TARGET_END_W(f) const int UN QQQ_end_marker_##f = 0;

#define REPL_KEY_GRID_HIERARCHY grid_hierarchy
#define REPL_TARGET_GRID_HIERARCHY REPL_TARGET_MARKER(REPL_KEY_GRID_HIERARCHY)
#define REPL_KEY_DYNAMIC_COMPILE dynamic_compile
#define REPL_TARGET_DYNAMIC_COMPILE REPL_TARGET_MARKER(REPL_KEY_DYNAMIC_COMPILE)
#define REPL_KEY_CACTUS_PARAMS cactus_params
#define REPL_TARGET_CACTUS_PARAMS REPL_TARGET_MARKER(REPL_KEY_CACTUS_PARAMS)
#define REPL_KEY_CACTUS_ARGS cactus_args
#define REPL_TARGET_CACTUS_ARGS REPL_TARGET_MARKER(REPL_KEY_CACTUS_ARGS)
#define REPL_TARGET_CACTUS_ARGS_END REPL_TARGET_END_W(REPL_KEY_CACTUS_ARGS)
#define REPL_KEY_SHARED_DECL shared_decl
#define REPL_TARGET_SHARED_DECL REPL_TARGET_MARKER(REPL_KEY_SHARED_DECL)
#define REPL_TARGET_SHARED_DECL_END REPL_TARGET_END_W(REPL_KEY_SHARED_DECL)
#define REPL_KEY_BOUND_XYZ bound_xyz
#define REPL_TARGET_BOUND_XYZ REPL_TARGET_MARKER(REPL_KEY_BOUND_XYZ)
#define REPL_TARGET_BOUND_XYZ_END REPL_TARGET_END_W(REPL_KEY_BOUND_XYZ)
#define REPL_KEY_UNROLL_MAIN unroll_main
#define REPL_TARGET_UNROLL_MAIN REPL_TARGET_MARKER(REPL_KEY_UNROLL_MAIN)

#define REPL_VALUE_DYNAMIC_COMPILE \
"const bool cakernel_dynamic_compile = true;"

/* CaCUDA grid hierarchy parameters to launch CaCUDA kernel */

// Parameters visible to kernel.  Constants for dynamically compiled code.
#include "common_cg_kernel_launch_params.h"

struct CAK_Var_Src_Info {
  const char *name;
  const bool stencil_data_okay;
  int stencil[6];
  int pattern_vector;
};

struct CAK_Var_Tmpl_Info {
  const char *name;
  const int vari;  // CCTK Variable Index.
  const char *intent;
  const char *c_datatype;
  bool cached;                  // Setting from cakernel.ccl.
  int stencil[6];
};

struct CAK_Var_Analysis_Data {
  CAK_Var_Src_Info *info_src;
  CAK_Var_Tmpl_Info *info_tmpl;
  const char *name;
  const char *intent;
  const char *c_datatype;
  bool plus_y; // non-zero y offset implies zero x and z.
  bool plus_z; // non-zero z offset implies zero x and y.
  bool stencil_access;   // If true, gf accessed at non-zero offset.
  bool shared_candidate; // If true, gf could use shared memory.
};

// Stencil shape, grid hierarchy, and other info associated with kernel.
//
struct CaKernel_Kernel_Config
{
  const char *kernel_name;
  const char *kernel_function_name;
  const char *thorn_name;

  int want_physbnd[6];  // If 1, should iterate in respective phys boundary.
  int sten_xn;  // DON'T rearrange stencil widths.
  int sten_xp;
  int sten_yn;
  int sten_yp;
  int sten_zn;
  int sten_zp;

  // Width of boundaries based on iteration preferences for this
  // kernel. If a face has a physical boundary and the kernel
  // requested iteration into that boundary, then the respective width
  // will be zero.
  //
  int bwid_n[3];  // Smallest index of iteration region point ..
                  // .. or number of points before 1st iteration point.
  int bwid_p[3];  // Number of points between iteration and local upper limit.
  int i_stop[3];  // Smallest index outside of iteration region.
  int i_length[3];

  int gf_count;     // Number of grid functions, regardless of direction.
  int gf_out_count; // Number of grid functions declared with intent out.
  int shared_var_count;
  bool gf_use_offsets;

  // Information about grid function variables, includes links
  // to the _Tmpl_ and _Src_ structures below.
  CAK_Var_Analysis_Data *gf_var_analysis_data;
  int gf_var_size;

  // Information about grid function variables provided by the
  // template expansion code, based on cakernel.ccl.
  CAK_Var_Tmpl_Info *var_tmpl_info;
  int var_tmpl_info_size;

  // Stencil information extracted from cakernel source files.
  CAK_Var_Src_Info *var_src_info;
  int gf_stencil_src_info_size;
};

// Information associated with a kernel's launch configuration.
//
struct CaKernel_Launch_Config
{
  bool l1_local_only;
  bool unroll_main;
  int grid_x, grid_y;
  double score;
  std::string rationale;
};

#endif
