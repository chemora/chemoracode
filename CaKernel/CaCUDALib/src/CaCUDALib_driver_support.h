 /*@@  -*- c++ -*-
   @file      CaCUDALib_driver_support.h
   @author    David Koppelman
   @desc
   Support routines for using the CUDA driver interface, including
   support for dynamic compilation.
 @@*/

#ifndef CaCUDALib_driver_support_h
#define CaCUDALib_driver_support_h

#include <cuda.h>
#include <cctk.h>
#include <string>
#include <vector>
#include <map>
#include "util-containers.h"
#include "launch.h"

using namespace std;

#include "chemora_cg.h"

#define STR_EXPAND(t) #t
#define STR(t) STR_EXPAND(t)

#define ASSERTS( expr )                                                       \
{ if ( !(expr) )                                                              \
    CCTK_VWarn                                                                \
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,                  \
       "Assertion failure."); }

// CUDA Device API Error Checking Wrapper
#define CE(rv)                                                                \
 {                                                                            \
   const CUresult result = rv;                                                \
   if ( result != CUDA_SUCCESS )                                              \
     {                                                                        \
       const char* err_str = NULL;                                            \
       cuGetErrorString(result,&err_str);                                     \
       CCTK_VWarn(CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,       \
                  "Error %d for CUDA Driver API call: %s\n",                  \
                  result, err_str ?: "Could not find error description." );   \
     }                                                                        \
 }

// CUDA Runtime API Error Checking Wrapper
#define CR( call )                                                           \
  {                                                                           \
    const cudaError err = call;                                               \
    if ( err != cudaSuccess )                                                 \
      CCTK_VWarn(CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,       \
            "CUDA error: %s", cudaGetErrorString(err));                       \
  }


// Copy VAR to device into a symbol also named VAR.
#define CaCUDALib_CUDA_DM_Var_To_Device(dm,var) \
  dm->var_to_device(#var,&var,sizeof(var));

// Copy VAR to device into a symbol also named VAR.
#define CaCUDALib_Var_To_Device(gpu_var,host_var) \
  CR( cudaMemcpyToSymbol(gpu_var, &host_var, sizeof(gpu_var)) );

inline double
time_wall_fp()
{
  struct timespec now;
  clock_gettime(CLOCK_REALTIME,&now);
  return now.tv_sec + ((double)now.tv_nsec) * 1e-9;
}

enum DM_Compile_Type { DM_Static, DM_Dynamic };

class CaCUDALib_CUDA_Driver_Manager;
class Module_Info;

typedef map<string,Module_Info> Mod_Map;

enum CC_Enum {
  CC_1x, CC_20, CC_21, CC_30, CC_35, CC_50, CC_52, CC_53,
  CC_60, CC_61, CC_62,
  CC_Unknown,
  CC_ENUM_SIZE
 };

enum FP_Type
{
  FP_MADD, FP_RECIP, FP_DIV, FP_POW, FP_SIN, FP_SQRT, FP_SQRT_RECIP,
  FP_TYPE_ENUM_SIZE
};
const char* const mb_fp_type[] =
  { "FP_MADD", "FP_RECIP", "FP_DIV", "FP_POW", "FP_SIN", "FP_SQRT",
    "FP_SQRT_RECIP" };

struct CaCUDALib_Device_Capabilities {
  int fu_sp;
  int fu_dp;
  int fu_int_al;  // Integer 32-bit add, logical.
  int fu_int_sc;  // Integer 32-bit shift.
  int fu_int_m;   // Integer 32-bit multiply, mad, sum of absolute difference.
  int fu_conv;    // Type conversions.
  int fu_special; // FP 32-bit reciprocal, etc.
  int fu_ls;      // Load Store
};

struct MB_Value {
  MB_Value() { min = 0;  max = min - 1; }
  void init(double minp, double maxp, double fbkp)
    {
      min = minp; max = maxp; fbk = fbkp;
      assert( min <= max );
      assert( fbk >= min );
      assert( fbk <= max );
    }
  void init_scaled(double scale_factor, double lo, double hi, double ref)
    {
      assert( scale_factor > 0 );
      assert( lo < hi );
      assert( ref > 0 );
      fbk = ref * scale_factor;
      min = fbk * lo;
      max = fbk * hi;
    }

  double val;
  double min;
  double max;
  double fbk;
};

struct CaCUDALib_GPU_Info {

  bool device_data_collected;
  static const int gpu_name_size = 80;
  char gpu_name[gpu_name_size+1];
  int cc_major, cc_minor;
  CC_Enum cc_idx;
  bool is_geforce;  // Not a good thing.

  int cuda_driver_version; // Strictly speaking, not a GPU characteristic.

  int num_multiprocessors;

  int max_registers_per_th;
  int max_registers_per_bl;
  int max_registers_per_mp;
  int max_threads_per_bl;
  int max_threads_per_mp;
  int max_blocks_per_mp;

  // Number of bits in offset field for SASS global load and store instructions.
  int64_t sass_ldst_offset_bits;
  int64_t sass_ldst_offset_max;
  int64_t sass_ldst_offset_min;  // A negative number for negative offsets.

  size_t global_mem_sz_bytes;
  int shared_sz_per_bl_bytes;
  int shared_sz_per_mp_bytes;
  int constant_sz_bytes;
  int l2_cache_sz_bytes;
  int roc_sz_bytes;
  int insn_cache_sz_bytes;
  int insn_size_bytes;              // Instruction size in bytes.
  // Typical result of dividing code size by number of instructions,
  // takes into account mystery instructions inserted by ptxas.
  double code_size_per_insn_bytes;

  int global_mem_bus_width_bits;

  double chip_bw_Bps;
  double clock_freq_hz;
  double mem_clock_freq_hz;

  // Below, values that are not directly provided by the API.
  CaCUDALib_Device_Capabilities dev_cap;

  MB_Value l2_cache_lat_cyc;
  MB_Value gl_mem_lat_cyc;
  MB_Value sh_mem_lat_cyc[6];   // stride in 8 bytes: 1, 2, 4, 8, 16 ,32
  MB_Value sh_mem_sync_cyc[6];  // warps/block: 1, 2, 4, 8, 16, 32

  MB_Value sp_fp_op_lat_cyc[FP_TYPE_ENUM_SIZE];
  MB_Value dp_fp_op_lat_cyc[FP_TYPE_ENUM_SIZE];
  MB_Value sp_fp_thpt_op_p_cyc[FP_TYPE_ENUM_SIZE];
  MB_Value dp_fp_thpt_op_p_cyc[FP_TYPE_ENUM_SIZE];
  // Multiple of threads for which instruction issue is fully utilized.
  int thd_granularity;
};


struct Kernel_User_Setting {
  string name_match_pattern;      // Matched against kernel name.
  int tile_x, tile_y, tile_z;     // Tile size per iteration.
  int tile_xx, tile_yy, tile_zz;  // Number of iterations.
};

class MI_Constant_Init_Item {
public:
  MI_Constant_Init_Item():valid(false),constant(false){};
  MI_Constant_Init_Item(string gf_namep, string ptx_symp, void *addr):
    gf_name(gf_namep),ptx_symbol(ptx_symp),device_addr(addr),
    valid(false),constant(false){};
  string gf_name;
  string ptx_symbol;
  void *device_addr;
  bool valid;    // Latest values copied into constant memory.
  bool constant; // Values do not change.
};

typedef void (*Func_Calc)(void **args);

class Function_Info
{
public:
  Function_Info()
  { inited = copied = false;  mi = NULL;  launch_end_ev = NULL;
    cu_function = NULL;  c_function = NULL; }
  ~Function_Info();
  CUfunction cu_function_get() { return cu_function; }

  int func_attribute_get(CUfunction_attribute attr)
  {
    if ( !cu_function ) return -1;
    int value = -1;
    CE ( cuFuncGetAttribute(&value, attr, cu_function) );
    return value;
  }

  int max_threads_get()
  { return func_attribute_get(CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK); }
  int static_shared_memory_get()
  { return func_attribute_get(CU_FUNC_ATTRIBUTE_SHARED_SIZE_BYTES); }
  int local_size_bytes_get()
  { return func_attribute_get(CU_FUNC_ATTRIBUTE_LOCAL_SIZE_BYTES); }
  int num_regs_get()
  { return func_attribute_get(CU_FUNC_ATTRIBUTE_NUM_REGS); }
  int blocks_per_mp_get()
  {
    int num_blocks = 0;
    if ( !cu_function ) return 0;
    assert( klp.block_size > 0 && klp.block_size <= 2048 );
#   if CUDA_VERSION >= 6050
    CE( cuOccupancyMaxActiveBlocksPerMultiprocessor
        (&num_blocks, cu_function, klp.block_size, 0) );
#   endif
    return num_blocks;
  }

  friend class CaCUDALib_CUDA_Driver_Manager;
  friend class Module_Info;

  string function_name;
  Func_Calc c_function;
  CUfunction cu_function;
  Module_Info *mi;
  bool inited;
  bool copied; // If true, don't delete until module destroyed.

  CaCUDA_Kernel_Launch_Parameters klp;
  CaKernel_Launch_Config lc;

  CUevent launch_end_ev;
  double total_time_ms();
  double total_time_all_ms;
  double total_time_notrace_ms;
};

typedef pVectorI_Iter<Function_Info> FIter;

class Module_Info
{
public:
  Module_Info();
  const string lookup(const string s)
  {
    map<string,string>::iterator it = replacements.find(s);
    return it == replacements.end() ? "" : it->second;
  }

  // Based on microbenchmark-collected data. Expect small run-to-run variations.
  CaCUDALib_GPU_Info& gpu_info_get();
  // Based on typical values.  Same from run to run on same device.
  CaCUDALib_GPU_Info& gpu_info_static_get();

  void kernel_launch_params_init(CaKernel_Kernel_Config *kcp, CCTK_ARGUMENTS);
private:
  void launch_config_decide();
  void launch_config_decide_1
  (int warp_limit, bool prefer_factors, Kernel_User_Setting *kus);
public:
  void kernel_launch_params_set_or_reset(bool reset);
  void launch(void *args[], CCTK_ARGUMENTS);
  void cakernel_launch(void *args[], CCTK_ARGUMENTS);
  void chemora_launch(void *args[], CCTK_ARGUMENTS);

  friend class CaCUDALib_CUDA_Driver_Manager;
  CaCUDALib_CUDA_Driver_Manager *dm;
  bool inited;
  bool module_loaded;
  CUmodule cu_module;
  string module_name;

  // Key used to look up this module. A hash of the calculation name
  // and whatever parameters the dynamic build system uses to generate
  // code (so that there will be distinct modules for distinct
  // parameter values).
  string module_key;

  int config_decide_iteration;
  CaKernel_Kernel_Config kc;
  CaKernel_Launch_Config lc;
  int gf_in_stencil;      // Based on per-variable stencil info.
  int gf_in_no_stencil;   // Based on per-variable stencil info.

  CaCUDA_Kernel_Launch_Parameters klp;

  // Build time path to the source needed for current module. Used to
  // identify unpacked files that can be used by multiple modules
  // because they were all generated from the same source. (Build-time
  // paths may not be reachable at run time.)
  string src_buildtime_path;
  string src_unpacked_path;
  string unpacked_out_files_base;
  string cmd_generate_asm; // Command needed to generate assembler. 

  string dynamic_out_files_base;
  string dynamic_src_file_path;

  map<string,string> replacements;

  //
  // Information for OpenACC, and maybe later other linkable forms.
  //

  void *dll_handle;


  void function_info_reset();
  void function_info_insert(Function_Info *fi);

  CUevent launch_start_ev;
  int num_launches;
  int num_launches_notrace;
  map<string,Function_Info> functions;
  pVectorI<Function_Info> function_info;

  map<string,MI_Constant_Init_Item> constant_gfs;
};

#include <unistd.h>  /// MOVE TO TOP OF FILE IT STILL HERE AT CHECKIN!
template<typename T> ssize_t pWrite(int fd, T& str)
{ return write(fd,&str,sizeof(str)); }
template<typename T> ssize_t pRead(int fd, T& str)
{ return read(fd,&str,sizeof(str)); }
struct DM_Trace_Trace_Header {
  int ni, nj, nk;
  int elt_size_bytes;
  int sample_size_bytes;
};
struct DM_Trace_Bunch_Header {
  int num_gf;
  char direction;
  char pad[3];
};
const int DM_Trace_Sample_Name_Size = 64;
struct DM_Trace_Sample_Header {
  int gf_index;
  char gf_name[DM_Trace_Sample_Name_Size];
};

class CaCUDALib_CUDA_Driver_Manager {
 public:
  CaCUDALib_CUDA_Driver_Manager();
  ~CaCUDALib_CUDA_Driver_Manager();
  void init_device(CCTK_ARGUMENTS);

  void gpu_info_init(int dev);
  void microbench_range_clamp(const char *param_name, MB_Value* v);
  void gpu_info_set_with_microbenchmark(int dev);
  void gpu_info_freeze(); // Make static version and validation copy.
  int myproc_get() { return myproc; }
  int devidx_get() { return devidx; }

  const char* nvcc_path_get() { return compile_nvcc_path; }

  bool trace_gf_inited;
  string trace_gf_path;
  char opt_trace_gf; // 'r', read; 'w', write; 0 off.
  int opt_trace_gf_lim_nk;
  int opt_trace_gf_sample_size_max_bytes;
  int trace_gf_fd;
  off_t trace_gf_eof_fo;
  bool trace_gf_done;
  CCTK_REAL *trace_gf_gf_buffer;
  size_t trace_gf_gf_buffer_size_bytes;
  int trace_gf_sample_size_elts;
  DM_Trace_Trace_Header trace_gf_trace_header;
  void trace_gf_init(CCTK_ARGUMENTS);

  void nan_scan_dtoh(int gsize_bytes, int copy_sz_bytes, CUdeviceptr d_addr);
  void nan_scan(CCTK_ARGUMENTS, int gsize_points, CUdeviceptr d_addr);
  void nan_scan
  (CCTK_ARGUMENTS, CaKernel_Kernel_Config *kc,
   void **karg_addrs, char dir_to_examine);
  CCTK_REAL *nan_scan_buffer;
  size_t nan_scan_buffer_size;

  bool dynamic_compile_possible(){ return compile_nvcc_path; }

  string cakernel_module_key_make(CCTK_ARGUMENTS, CaKernel_Kernel_Config *kc);

  // Make named module the current module. If values are needed for
  // a dynamic compile return true.
  bool module_get
  (CCTK_ARGUMENTS, CaKernel_Kernel_Config *kc, const string src_file_path,
   DM_Compile_Type compile_type = DM_Dynamic);

  CaCUDALib_GPU_Info* gpu_info_get(){ return &gpu_info; }
  CaCUDALib_GPU_Info* gpu_info_static_get(){ return &gpu_info_static; }

  void kernel_launch_params_init
  (CaKernel_Kernel_Config *kcp, CCTK_ARGUMENTS)
  { mi->kernel_launch_params_init(kcp,CCTK_PASS_CTOC); };

  void tile_size_overrides_parse(const char *tile_size_overrides_param);

  static bool kernel_user_setting_match(string pattern, string kernel_name);
  Kernel_User_Setting* kernel_user_setting_lookup(string kernel_name);

  void kernel_launch_params_set()
  { mi->kernel_launch_params_set_or_reset(false); }

  void launch(void *args[],CCTK_ARGUMENTS);

  void module_src_set
  (int src_uncomp_chars, int src_comp_chars, char *src_words);

  // Compile using flag that prevents global data from using l1 cache.
  void module_set_l1_local_only(bool local_only);

  // Specify a set of values to replace those following a line matching TARGET.
  // Throw error if TARGET already exists.
  void module_vals_set(const string target, const string replacement)
  { module_vals_set_or_reset(target, replacement, false); }
  // Throw error if TARGET doesn't already exist.
  void module_vals_reset(const string target, const string replacement)
  { module_vals_set_or_reset(target, replacement, true); }
  void module_vals_set_or_reset
  (const string target, const string replacement, bool reset);

  // Compile and load current module.
  void module_load();
  void module_load_cuda(Chemora_CG_Calc *c); // Handled by nvcc.
  void module_load_acc(Chemora_CG_Calc *c); // Handled by, for now, pgc++, ld

  void chemora_dyn_build();
  void cakernel_dyn_build();

  // Generate assembler code for most recently compiled module.
  void module_generate_assembler();
  // Remove the module (presumably to be replaced with a better one).
  // If no module is currently loaded, do nothing.
  void module_unload();

  // If not already present, load named function from the current
  // module, return object describing function.
  Function_Info* function_info_get(const string func_name);
  CUfunction function_load(const string func_name);

  // Copy data on host at VAR_HOST_ADDR of length VAR_HOST_SIZE to a
  // symbol named VAR_NAME on device. 
  // See macro CaCUDALib_CUDA_DM_Var_To_Device
  void var_to_device
  (const char *var_name, void *var_host_addr, size_t var_host_size);


  void msg_tune(const char  *fmt,...) __attribute__ ((format(printf,2,3)));
  void msg_info(const char  *fmt,...) __attribute__ ((format(printf,2,3)));
  void report_print();

  friend class Module_Info;

private:
  Mod_Map modules;
  const char *compile_nvcc_path;
  string compile_nvdisasm_path;
  const char *compile_cucc_target;  // Target, including "-arch".
  const char *compile_cucc_options; // Other options.
  bool allow_recompile_anytime, allow_recompile_init;
  Module_Info *mi; // Current module.
  bool init_called;
  int myproc, devidx;
  CUdevice cu_device;
  CaCUDALib_Device_Capabilities device_capabilities[CC_ENUM_SIZE];
  CaCUDALib_GPU_Info gpu_info, gpu_info_cpy, gpu_info_static;
  CUcontext cu_context;
  map<CUdeviceptr,string> symbols;

  // User-provided values which are to be used instead of automatically
  // determined tile sizes.
  vector<Kernel_User_Setting> kernel_user_settings;

  // Path to most recently unpacked source file.
  string src_unpacked_path;
  // Path to use for generated files if unpacked files are used.
  string unpacked_out_files_base;
  // Build time path to source (.cu) of most recently unpacked
  // file. Used to identify unpacked files that can be used by
  // multiple modules because they were all generated from the same
  // source. (Build-time paths may not be reachable at run time.)
  string src_unpacked_buildtime_path;

public:
  int param_gf_offset_var_min;
  int param_l1_restrict_gf_min;
  int param_warps_max;
  int param_warps_l1_limited_max;
  bool param_gf_offset_tolerant;

  CUevent sc_launch_start_ev;  // For statically compiled code.
  CUevent sc_launch_end_ev;    // For statically compiled code.

  double report_total_et;

};

inline double Function_Info::total_time_ms()
{ return mi->num_launches_notrace
    ? total_time_notrace_ms / mi->num_launches_notrace * mi->num_launches
    : total_time_all_ms; }


extern CaCUDALib_CUDA_Driver_Manager *cacudalib_cuda_driver_manager;

inline CaCUDALib_CUDA_Driver_Manager*
CaCUDALib_CUDA_driver_manager_get()
{ return cacudalib_cuda_driver_manager; }

inline void CaCUDALib_InitDevice(CCTK_ARGUMENTS)
{
  if ( !cacudalib_cuda_driver_manager )
    cacudalib_cuda_driver_manager = new CaCUDALib_CUDA_Driver_Manager();
  cacudalib_cuda_driver_manager->init_device(CCTK_PASS_CTOC);
}

void
CaCUDALib_driver_support_set_lc(CCTK_ARGUMENTS, CaKernel_Kernel_Config &kc);

extern "C" void CaCUDALib_DM_InitDev(CCTK_ARGUMENTS);

#endif
