: # -*- perl -*-
  eval 'exec perl -w -S $0 "$@"'
    if 0;

 #  @@
 #   @file      nvcc-wrap
 #   @date      $Date:$
 #   @author    David Koppelman
 #   @desc 
 #   Wrapper to drive execution of nvcc.  Called as if generating
 #   a CUDA object file for CUDA runtime use.  Runs nvcc in that way
 #   but also generates ptx and sass files, for debugging and tuning,
 #   and .cup (preprocessed) files for dynamic optimizer.
 #   @enddesc 
 #   @version   $Revision:$
 # @@

sub env_or_config;
sub src_analyze_and_embed;

# Read Cactus config variables.
#
$config_dir = $ENV{TOP} || '';
$config_info_file = "$config_dir/config-info";
if ( -r $config_info_file )
  {
    if ( open CONFIG, $config_info_file )
      {
        while ( <CONFIG> )
          {
            next if m/^\s*\#/;
            my ($var,$val) = m/\s*([A-Za-z_0-9]+)\s*=\s*(.*)/;
            $val =~ s/\s+$// if $val;
            $config{$var} = $val if $var;
          }
        close CONFIG;
      }
  }

# Write commands and other info to a log file in Cactus or home directory.
$write_log_file = env_or_config 'NVCC_WRAP_WRITE_LOG_FILE';

# Generate ptx and sass assembler code.
$generate_assembler = env_or_config 'NVCC_WRAP_GENERATE_ASSEMBLER';

$dry_run = 0;


# In CUDA Toolkit 4.1 (at least) cudafe emits warnings about unreachable
# code which is only unreachable because of the instantiation of a template.
# There is no flag to suppress that particular warning, so suppress them
# all. :-(
$suppress_stupid_warnings = 1;

my $log_file_name = 'nvcc-wrap-log.txt';

sub which_dir {
  my $target = shift;
  my @path = split /:/, $ENV{PATH};
  foreach $dir ( @path ) {
    return $dir if -e "$dir/$target" && ! -d "$dir/$target";
  }
  return "";
}

my $nvcc_from_epath = which_dir "nvcc";
my $cuda_dir_from_epath =
  $nvcc_from_epath &&
  do {my @parts = split(/\//,$nvcc_from_epath); pop @parts; join("/",@parts);};
my @cuda_possible_dirs;

$config_cuda_dir = env_or_config 'CUDA_DIR';

push @cuda_possible_dirs, $config_cuda_dir if $config_cuda_dir;
push @cuda_possible_dirs, $cuda_dir_from_epath if $cuda_dir_from_epath;
push @cuda_possible_dirs,
  qw ( /usr/local/cuda /opt/local/cuda /usr/cuda /opt/cuda
       /usr/local/cuda/4.1 );

die "Could not find CUDA dir.\n" unless @cuda_possible_dirs;
my ( $cuda_dir ) = grep { -e "$_/bin/nvcc" } @cuda_possible_dirs;

my $cuda_bin_dir = "$cuda_dir/bin";
my $nvcc_path = "$cuda_bin_dir/nvcc";
my $cuobjdump = "$cuda_bin_dir/cuobjdump";

my $prog = $nvcc_path;

my $bzip2_path = "bzip2";

sub compress_file_to_c_source;
sub run_and_exit;
# Run nvcc just as it would have run without this wrapper and exit.
sub run_as_invoked_and_exit;

if ( $dry_run )
  {
    $prog = "echo";
    $cuobjdump = "echo";
  }

my $cmd = "$prog ".join(" ",@ARGV);

my $log_file_dir = $ENV{CCTK_HOME} || glob('~');
my $log_file_path = "$log_file_dir/$log_file_name";

if ( 0 )
  {
    # write environment variables to log file.
    open LOG, ">>$log_file_path" or die "Could not open log file for output.";
    while ( my($k,$v) = each %ENV ) { print LOG "$k -> $v\n"; }
    close LOG;
  }

if ( $write_log_file )
  {
    open LOG, ">>$log_file_path" or die "Could not open log file for output.";
    print LOG "$cmd\n";
    close LOG;
  }

# Finish here if nvcc just called for dependencies.
#
run_as_invoked_and_exit if $ARGV[0] eq '-M';

#
# Filter out certain command-line arguments.
#

my $arg_action_found = 0;  # Check for compile switch.
my $arg_outfile;           # Name of output file.
my @args_filtered = ();
my @args_cpy = @ARGV;

while ( @args_cpy )
  {
    $_ = shift @args_cpy;
    if ( $_ eq '-c' ) { $arg_action_found = 1; }
    elsif ( $_ eq '-o' ) { $arg_outfile = shift @args_cpy; }
    else
      {
        push @args_filtered, $_;
      }
  }

push @args_filtered, "-Xcudafe", "-w" if $suppress_stupid_warnings;

run_as_invoked_and_exit("Could not find output file.") if !$arg_outfile;


my $args_filtered = join(' ',map { "'".$_."'" } @args_filtered);
my ($out_file_no_ext) = $arg_outfile =~ m/\A(.*)\.o\Z/;

run_as_invoked_and_exit "Could not find output file ending in .o."
  if !$out_file_no_ext;

my $cubin_file = "'" . $out_file_no_ext . ".cubin'";
my $ptx_file = $out_file_no_ext . ".ptx";
my $sass_file = $out_file_no_ext . ".sass";
my $cup_file = $out_file_no_ext . ".cup";

# Preprocessed source with dummy I3D wrappers, for obtaining stencil shapes.
my $cup_file_si = $out_file_no_ext . "-stencil-info.cup";

my $cup_src_needed = $out_file_no_ext =~ m/gpu_cuda_dc/;
my $cup_c_src_file = $out_file_no_ext . "-embed-cup.cc";

my $cmd_make_ptx = "$prog --ptx $args_filtered -o '$out_file_no_ext.ptx'";
my $cmd_make_cubin = "$prog --cubin $args_filtered -o $cubin_file";
my $cmd_make_sass = "$cuobjdump -sass $cubin_file > '$out_file_no_ext.sass'";
my $cmd_make_i = "$prog --preprocess $args_filtered -o '$cup_file'";
my $cmd_make_si = "$prog -DSTENCIL_CHECK --preprocess $args_filtered -o '$cup_file_si'";

system "echo '// Intentionally blank' > $cup_c_src_file"
  unless -e $cup_c_src_file || !$cup_src_needed;

system $cmd_make_i;

#  "$cmd_make_cubin && $cmd_make_ptx && $cmd_make_sass && $cmd_make_i &";

# Stencil Info
my $debug_si = 0;

if ( $cup_src_needed )
  {
    compress_file_to_c_source $cup_file, $cup_c_src_file;
    system $cmd_make_si;
    src_analyze_and_embed $cup_file_si, $cup_c_src_file;
  }

my @cmd_make_asm = ( $cmd_make_cubin, $cmd_make_ptx, $cmd_make_sass );

my $cmd_make_asm_bck = join(" && ", @cmd_make_asm) . " &";

system $cmd_make_asm_bck if $generate_assembler;
if ( $write_log_file && $generate_assembler )
  {
    open LOG, ">>$log_file_path" or die "Could not open log file for output.";
    print LOG "SAM:  $cmd_make_asm_bck\n";
    close LOG;
  }

my @args_run = @ARGV;
push @args_run, "-DCAK__EMBED_SOURCE=$cup_c_src_file" if $cup_src_needed;

run_and_exit "Main Compile", @args_run;

exit(0);

# Return value of environment variable or, if not present, config-file var.
#
sub env_or_config
  {
    my $key = shift;
    return $ENV{$key} if exists $ENV{$key};
    return $config{$key} if exists $config{$key};
    return "";
  }

sub run_as_invoked_and_exit {
  my $msg = shift;
  run_and_exit $msg, @ARGV;
}

sub run_and_exit {
  my ($msg,@args) = @_;
  if ( $write_log_file && $msg )
    {
      my $cmd = join ' ', $prog, @args;
      open LOG,
        ">>$log_file_path" or die "Could not open log file for output.";
      print LOG "** $msg:  $cmd\n";
      close LOG;
    }
  exec $prog, @args;
}

sub compress_file_to_c_source {
  my $file_in = shift;
  my $file_out = shift;

  die "File $file_in is not readable\n" unless -r $file_in;

  my $file_in_size = -s $file_in;

  open COMPIN, "$bzip2_path -z -c '$file_in' |"
    or die "Could not pipe $file_in through $bzip2_path.\n";
  open FOUT, ">$file_out" or die "Could not open $file_out for output.";

  print FOUT <<"EOS;";
//  -*- fundamental -*-
static const int cak__src_uncomp_chars = $file_in_size;
static const int32_t cak__src_words[] =
  {
EOS;

  my $comp_data_size = 0;

  while ( !eof(COMPIN) )
    {
      my $word;
      my $amt = read COMPIN, $word, 4;
      last unless $amt;
      my $value = 0;
      $comp_data_size += $amt;
      if ( $amt == 8 )
        {
          $value = unpack "L", $word;
        }
      else
        {
          my @bytes = unpack "C*", $word;
          for ( my $i=0; $i < $amt; $i++ )
            {
              # Having a problem on a big-endian system? Sorry, we had
              # a deadline and forgot to tie up this loose end.
              $value += $bytes[$i] << ( $i << 3 );
            }
        }
      printf FOUT "   %#10x%s", $value, eof(COMPIN) ? "" : ",";
      printf FOUT "\n" unless $comp_data_size & 0xf;
      last if $amt < 4;
    }

  close COMPIN;

  print FOUT "  };\n";
  print FOUT "static const int cak__src_comp_chars = $comp_data_size;\n";

  close FOUT;
}

sub dim_update {
  my $i = shift;
  my $dirp = shift;
  my $dirn = shift;
  my $val = shift;
  $i->{$dirn} = $val if $val < 0 && $val < $i->{$dirn};
  $i->{$dirp} = $val if $val > 0 && $val > $i->{$dirp};
}

sub src_analyze_and_embed {
  my $cup_file_si = shift;
  my $cup_c_src_file = shift;

  my $error_count_si = 0;
  my %stencil_var_info = ();

  open SCH, "$cup_file_si" or die "Could not open $cup_file_si for input.\n";

  while ( <SCH> )
    {
      while ( m/cak__stencil_check_i3d\s*\(([^,]+),([^,]+),([^,]+),([^()]+)\)/g )
        {
          my ($var,$dx,$dy,$dz) = ($1,$2,$3,$4);
          $dx =~ s/^\s+//; $dx =~ s/\s+$//;
          $dy =~ s/^\s+//; $dy =~ s/\s+$//;
          $dz =~ s/^\s+//; $dz =~ s/\s+$//;
          warn "Indices for $var are \"$dx\", \"$dy\", \"$dz\"\n" if $debug_si;

          my $indices_okay =
            $dx =~ m/^-?[0-9]+$/ && $dy =~ m/^-?[0-9]+$/ 
            && $dz =~ m/^-?[0-9]+$/;
          warn "$.: problem with indices in \"$_\"\n"
            unless !$debug_si || $indices_okay 
            || $error_count_si++ > 4 && !$debug_si;

          my $info = $stencil_var_info{$var} ||
            { xp => 0, yp => 0, zp => 0, xn => 0, yn => 0, zn => 0,
              pattern_vector => 0,
              prob => 0 };

          $stencil_var_info{$var} = $info;

          $info->{prob} = 1 unless $indices_okay;

          next unless $indices_okay;
          my $pattern = ( $dx ? 1 : 0 ) + ( $dy ? 2 : 0 ) + ( $dz ? 4 : 0 );
          $info->{pattern_vector} |= 1 << $pattern;

          dim_update $info, 'xp', 'xn', $dx;
          dim_update $info, 'yp', 'yn', $dy;
          dim_update $info, 'zp', 'zn', $dz;
        }
    }
  close SCH;

  while ( my ($var,$i) = each %stencil_var_info )
    {
      warn "Var $var  $i->{xn} $i->{xp}, $i->{yn} $i->{yp}, $i->{zn} $i->{zp}"
        ." Pat $i->{pattern_vector}\n"
        if $debug_si;
    }

  open CSRC, ">>$cup_c_src_file" 
    or die "Could not open $cup_c_src_file for appending.\n";

  print CSRC <<"EOS;";
static CAK_Var_Src_Info cak__src_stencil_info[] =
{
EOS;

 while ( my ($var,$i) = each %stencil_var_info )
 {
   my ($xna,$yna,$zna) = (-$i->{xn},-$i->{yn},-$i->{zn});
   my $pat_txt = "";
   my $pat = $i->{pattern_vector};
   for ( my $i=0; $i<8; $i++ )
     {
       next unless $pat & ( 1 << $i );
       $pat_txt .= ", " if $pat_txt;
       $pat_txt .= "x" if $i & 1;
       $pat_txt .= "y" if $i & 2;
       $pat_txt .= "z" if $i & 4;
     }
   print CSRC  "  {\"$var\", ";
   print CSRC $i->{prob} ? "false, " : "true, ";
   print CSRC  "$xna,$i->{xp}, $yna,$i->{yp}, $zna,$i->{zp},\n";
   printf CSRC "   0x%x  // Pattern Vector: $pat_txt \n", $i->{pattern_vector};
   print CSRC  "   },\n";
   }
 print CSRC <<"EOS;";
};
EOS;
  close CSRC;
}
