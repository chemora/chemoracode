#! /bin/bash

################################################################################
# Prepare
################################################################################

# Set up shell
if [ "$(echo ${VERBOSE} | tr '[:upper:]' '[:lower:]')" = 'yes' ]; then
    set -x                      # Output commands
fi
set -e                          # Abort on errors

# This might need to be changed if GNU unzip doesn't work for you
UNZIP=unzip

################################################################################
# Search
################################################################################

if [ -z "${SQLITE_DIR}" ]; then
    echo "BEGIN MESSAGE"
    echo "SQLite selected, but SQLITE_DIR not set. Checking some places..."
    echo "END MESSAGE"
    
    FILES="sqlite3.h libsqlite3.so"
    DIRS="/usr /usr/local /usr/local/sqlite /opt/local ${HOME}/sqlite"
    # look into each directory
    for dir in $DIRS; do
        # libraries might have different file extensions
        for libext in a so dylib; do
            # libraries can be in /lib or /lib64
            for libdir in lib64 lib; do
                FILES="include/sqlite3.h $libdir/libsqlite3.${libext}"
                    # assume this is the one and check all needed files
                SQLITE_DIR="$dir"
                for file in $FILES; do
                    # discard this directory if one file was not found
                    if [ ! -r "$dir/$file" ]; then
                        unset SQLITE_DIR
                        break
                    fi
                done
                # don't look further if all files have been found
                if [ -n "$SQLITE_DIR" ]; then
                    break
                fi
           done
           # don't look further if all files have been found
           if [ -n "$SQLITE_DIR" ]; then
               break
           fi
        done
        # don't look further if all files have been found
        if [ -n "$SQLITE_DIR" ]; then
            break
        fi
    done
    
    if [ -z "$SQLITE_DIR" ]; then
        echo "BEGIN MESSAGE"
        echo "SQLite not found"
        echo "END MESSAGE"
    else
        echo "BEGIN MESSAGE"
        echo "Found SQLite in ${SQLITE_DIR}"
        echo "END MESSAGE"
    fi
fi


################################################################################
# Build
################################################################################

if [ -z "${SQLITE_DIR}"                                                   \
     -o "$(echo "${SQLITE_DIR}" | tr '[a-z]' '[A-Z]')" = 'BUILD' ]
then
    echo "BEGIN MESSAGE"
    echo "Building SQLite..."
    echo "END MESSAGE"
    
    # Set locations
    THORN=SQLite
    NAME=sqlite-amalgamation-3071100
    SRCDIR=$(dirname $0)
    BUILD_DIR=${SCRATCH_BUILD}/build/${THORN}
    if [ -z "${SQLITE_INSTALL_DIR}" ]; then
        INSTALL_DIR=${SCRATCH_BUILD}/external/${THORN}
    else
        echo "BEGIN MESSAGE"
        echo "Installing SQLite into ${SQLITE_INSTALL_DIR}"
        echo "END MESSAGE"
        INSTALL_DIR=${SQLITE_INSTALL_DIR}
    fi
    DONE_FILE=${SCRATCH_BUILD}/done/${THORN}
    SQLITE_DIR=${INSTALL_DIR}
    
    if [ -e ${DONE_FILE} -a ${DONE_FILE} -nt ${SRCDIR}/dist/${NAME}.zip \
                         -a ${DONE_FILE} -nt ${SRCDIR}/SQLite.sh ]
    then
        echo "BEGIN MESSAGE"
        echo "SQLite has already been built; doing nothing"
        echo "END MESSAGE"
    else
        echo "BEGIN MESSAGE"
        echo "Building SQLite"
        echo "END MESSAGE"

        # Build in a subshell
        (
        exec >&2                # Redirect stdout to stderr
        if [ "$(echo ${VERBOSE} | tr '[:upper:]' '[:lower:]')" = 'yes' ]; then
            set -x              # Output commands
        fi
        set -e                  # Abort on errors
        cd ${SCRATCH_BUILD}
        
        # Set up environment
        export LDFLAGS
        unset LIBS
        if echo '' ${ARFLAGS} | grep 64 > /dev/null 2>&1; then
            export OBJECT_MODE=64
        fi
        
        echo "SQLite: Preparing directory structure..."
        mkdir build external done 2> /dev/null || true
        rm -rf ${BUILD_DIR} ${INSTALL_DIR}
        mkdir ${BUILD_DIR} ${INSTALL_DIR}
        
        echo "SQLite: Unpacking archive..."
        pushd ${BUILD_DIR}
        ${UNZIP} ${SRCDIR}/dist/${NAME}.zip
        
        echo "SQLite: Compiling..."
        cd ${NAME}
        ${CC} -fPIC -shared -Wl,-soname,${SQLITE_DIR}/lib/libsqlite3.so -o libsqlite3.so -DSQLITE_THREADSAFE=0 -DSQLITE_OMIT_LOAD_EXTENSION sqlite3.c
        
        mkdir ${SQLITE_DIR}/include ${SQLITE_DIR}/lib
        cp ${BUILD_DIR}/${NAME}/sqlite3.h ${SQLITE_DIR}/include
        cp ${BUILD_DIR}/${NAME}/libsqlite3.so ${SQLITE_DIR}/lib
        popd
        
        echo "SQLite: Cleaning up..."
#        rm -rf ${BUILD_DIR}
        
        date > ${DONE_FILE}
        echo "SQLite: Done."
        
        )
        
        if (( $? )); then
            echo 'BEGIN ERROR'
            echo 'Error while building SQLite. Aborting.'
            echo 'END ERROR'
            exit 1
        fi
    fi
    
fi



################################################################################
# Check for additional libraries
################################################################################

# Set options
if [ "${SQLITE_DIR}" = '/usr' -o "${SQLITE_DIR}" = '/usr/local' ]; then
    SQLITE_INC_DIRS=''
    SQLITE_LIB_DIRS=''
else
    SQLITE_INC_DIRS="${SQLITE_DIR}/include"
    SQLITE_LIB_DIRS="${SQLITE_DIR}/lib"
fi
SQLITE_LIBS='sqlite3'



################################################################################
# Configure Cactus
################################################################################

# Pass options to Cactus

echo "BEGIN MAKE_DEFINITION"
echo "HAVE_SQLITE     = 1"
echo "SQLITE_DIR      = ${SQLITE_DIR}"
echo "SQLITE_INC_DIRS = ${SQLITE_INC_DIRS}"
echo "SQLITE_LIB_DIRS = ${SQLITE_LIB_DIRS}"
echo "SQLITE_LIBS     = ${SQLITE_LIBS}"
echo "END MAKE_DEFINITION"

echo 'INCLUDE_DIRECTORY $(SQLITE_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(SQLITE_LIB_DIRS)'
echo 'LIBRARY           $(SQLITE_LIBS)'
