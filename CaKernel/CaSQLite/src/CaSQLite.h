#ifndef _CASQLITE_H_
#define _CASQLITE_H_

#include "cctk.h"
#include "sqlite3.h"

#ifdef __cplusplus

extern "C"
{
#endif

#define SQLITE_SAFE_CALL( call )                                           \
  {                                                                        \
    unsigned short err = call;                                             \
    if(err != SQLITE_OK)                                                   \
    {                                                                      \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,   \
            "SQLite error: %s", sqlite3_errmsg(casqlite_db));              \
    }                                                                      \
  }
  /* Malloc Util */

#define MALLOC_SAFE_CALL( call )                                           \
    {                                                                      \
      void *err = call;                                                    \
      if(err == NULL)                                                      \
      {                                                                    \
        CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING, \
              "Malloc error: %s", "failed to allocate memory !");          \
        exit(-1);                                                          \
      }                                                                    \
    }

  /* execute an SQL statement other than a select statement */
  void CaSQLite_Exec(const char* statement);

  /* execute an SQL select statement */
  void CaSQLite_Select(const char* statement,
      int callback(void *, int, char **, char **));

  /* print a given table in the global database */
  void CaSQLite_PrintTable(const char* tablename);

  /* Internal Functions */
  int CaSQLitei_PrintRow(void *data, int num_fields, char **fields,
      char **field_names);

/* these shall be wrapped up in a structure after SC12 */
  extern sqlite3* casqlite_db;
  extern int col_header;
  extern char **colname_list;

#ifdef __cplusplus
}
#endif

#endif                          //_CASQLITE_H_
