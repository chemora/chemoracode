#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Schedule.h"

#include "util_String.h"
#include "CaSQLite.h"

#define DEBUGCASQLITE

sqlite3 *casqlite_db;
int tbl_header = 0;
char **colname_list;

int CaSQLite_Startup(void)
{
  const char *banner = ""
      "\n=================================================\n"
      "       o-o      o-o   o-o  o       o\n"
      "      /        |     o   o |    o  |\n"
      "     O      oo  o-o  |   | |      -o- o-o\n"
      "      \\    | |     | o   O |    |  |  |-o\n"
      "       o-o o-o-o--o   o-O\\ O---o|  o  o-o\n\n"
      "  Enpower Cactus with Structured Query Language \n\n"

      "         (c) Copyright The Authors \n"
      "         GNU Licensed. No Warranty \n"
      "=================================================\n\n";
  CCTK_RegisterBanner(banner);
  return 0;
}

void CaSQLite_DatabaseInit( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  char *scratch_filename = NULL;
  const int myproc = CCTK_MyProc(cctkGH);
  const int nprocs = CCTK_nProcs(cctkGH);

  /* determine the file name */
  if (nprocs == 1)
  {
    scratch_filename = Util_Strdup(sqlite_filename);
  }

  /* create files under current dir if only one database file is requested */
  else if (one_database_file)
  {
    Util_asprintf(&scratch_filename, "%s.%06d", sqlite_filename, myproc);
  }
  else
  {
    Util_asprintf(&scratch_filename, "%s.%06d", sqlite_filename, myproc);
  }

  /* before we create the database files we want to remove
   * existing ones */

  remove(sqlite_filename);
  remove(scratch_filename);

  /* open the global database */
  if (scratch_filename == NULL)
  {
    CCTK_INFO("No SQLite database filename was specified"
        "An in-memory database will be created !");

    SQLITE_SAFE_CALL(sqlite3_open(":memory:", &casqlite_db));
  }
  else
  {
    SQLITE_SAFE_CALL(sqlite3_open(scratch_filename, &casqlite_db));
  }
}

/* merge SQLite database files from all MPI processes into one big database file */

void CaSQLite_DatabaseMerge( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  char *scratch_filename;
  char *sql_stmt;
  const int nprocs = CCTK_nProcs(cctkGH);
  const int myproc = CCTK_MyProc(cctkGH);

  int nrow, ncol;

  /* only do the merge on the root process */
  /* DatabaseMerge shall only be called when one output database file
   * is requested but we just want to put the condition here to
   * avoid potential issues in case the schedule condition got changed */

  if (nprocs != 1 && one_database_file)
  {
    /* close the old connection to process-wise database to release the lock*/

    SQLITE_SAFE_CALL(sqlite3_close(casqlite_db));
    Util_asprintf(&scratch_filename, "%s.%06d", sqlite_filename, myproc);

    CCTK_Barrier(cctkGH);
    CCTK_INFO("Start merging SQLite database files:");

    if (myproc == 0)
    {
      /* rename the proc 0 output as the main file */
      rename(scratch_filename, sqlite_filename);

      /* reopen the global SQLite databas file to merge TODO*/
      SQLITE_SAFE_CALL(sqlite3_open(sqlite_filename, &casqlite_db));

      for (int i = 1; i < nprocs; i++)
      {
        Util_asprintf(&scratch_filename, "%s.%06d", sqlite_filename, i);
        /* attach the process-wise database to the global database */
        Util_asprintf(&sql_stmt, "attach database '%s' as ext", scratch_filename);
        CaSQLite_Exec(sql_stmt);

        CaSQLite_Exec("begin");

        SQLITE_SAFE_CALL(
            sqlite3_get_table(casqlite_db,
                "SELECT name FROM sqlite_master WHERE type='table' ORDER BY name",
                &colname_list, &nrow, &ncol, NULL));

        for (int j = 1; j <= nrow; j++)
        {
          Util_asprintf(&sql_stmt, "insert into %s select * from ext.%s",
              colname_list[j], colname_list[j]);
          CaSQLite_Exec(sql_stmt);
        }
        CaSQLite_Exec("commit");

        /* detach the database */
        CaSQLite_Exec("detach database ext");

        CCTK_VInfo(CCTK_THORNSTRING, "Adding %s to %s", scratch_filename, sqlite_filename);
      }
    }

    CCTK_Barrier(cctkGH);
    CCTK_VInfo(CCTK_THORNSTRING, "Finish merging %d database files into %s !",
        nprocs, sqlite_filename);

    if (myproc != 0)
    {
      SQLITE_SAFE_CALL(sqlite3_open(scratch_filename, &casqlite_db));
    }
  }
}

void CaSQLite_DatabasePrint( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int nrow, ncol;
  SQLITE_SAFE_CALL(
      sqlite3_get_table(casqlite_db,
          "SELECT name FROM sqlite_master WHERE type='table' ORDER BY name",
          &colname_list, &nrow, &ncol, NULL));
  for (int i = 1; i <= nrow; i++)
  {
    CCTK_VInfo(CCTK_THORNSTRING, "Table %s in the database file %s",
        colname_list[i], sqlite_filename);
    CaSQLite_PrintTable(colname_list[i]);
  }
}

void CaSQLite_DatabaseShutdown( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  SQLITE_SAFE_CALL(sqlite3_close(casqlite_db));
}

/* execute an SQL statement other than a select statement */
void CaSQLite_Exec(const char* statement)
{
#ifdef DEBUGCASQLITE
  CCTK_VInfo(CCTK_THORNSTRING, "Executing SQL statement : %s", statement);
#endif
  SQLITE_SAFE_CALL(sqlite3_exec( casqlite_db, statement, NULL, NULL, NULL));
}

/* execute an SQL select statement
 * the callback function will be used to deal with the returned data;
 * callback(void *a_param, int argc, char **argv, char **column);
 * */

void CaSQLite_Select(const char* statement,
    int callback(void *, int, char **, char **))
{
  tbl_header = 1;
  SQLITE_SAFE_CALL(sqlite3_exec( casqlite_db, statement, callback, NULL, NULL));
}

/* print a table in the global database cactus.sqlite */
void CaSQLite_PrintTable(const char* tablename)
{
  char * sql_stmt;
  Util_asprintf(&sql_stmt, "select * from %s", tablename);
  CaSQLite_Select(sql_stmt, CaSQLitei_PrintRow);
}

int CaSQLitei_PrintRow(void *data, int ncol, char **cols, char **col_header)
{
  int i;

  if (tbl_header)
  {
    tbl_header = 0;

    for (i = 0; i < ncol; i++)
    {
      printf("%20s", col_header[i]);
    }
    printf("\n");
    for (i = 0; i < ncol * 20; i++)
    {
      printf("=");
    }
    printf("\n");
  }
  for (i = 0; i < ncol; i++)
  {
    printf("%20s", cols[i]);
  }
  printf("\n");
  return 0;
}
