#include <string.h>
#include <stdio.h>

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "util_String.h"
#include "CaSQLite.h"

void insert_entries(CCTK_ARGUMENTS);

void CaSQLite_TestInit( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CaSQLite_Exec(
      "create table if not exists arrangement (procid, arrangement, thorn)");
}

void CaSQLite_TestComplete( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  insert_entries(CCTK_PASS_CTOC);
}

void insert_entries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  char * sql_stmt;
  Util_asprintf(&sql_stmt, "insert into arrangement values (%d, 'CaKernel','CaSQLite')", CCTK_MyProc(cctkGH));
  CaSQLite_Exec("begin");
  CaSQLite_Exec(sql_stmt);
  CaSQLite_Exec("commit");
}
