#include <string.h>
#include <stdio.h>
#include "sqlite3.h"

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Schedule.h"

#include "util_String.h"

#include "CaSQLite.h"

/* for each run the table can only be created once */

void CaSQLite_TimerReportInit( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  char *sql_stmt;
  Util_asprintf(&sql_stmt, "create table if not exists %s (%s text, %s real, %s integer)",
      "timerreport", "timer", "value", "proc");
  CaSQLite_Exec(sql_stmt);
}

void CaSQLite_TimerReportComplete( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const int ntimers = CCTK_NumTimers();
  cTimerData * const td = CCTK_TimerCreateData();
  CCTK_REAL value;

  if (ntimers <= 0)
  {
    CCTK_WARN(1, "There are no timers to be output !");
  }

  char *sql_stmt;

  CaSQLite_Exec("begin");

  for (int i = 0; i < ntimers; i++)
  {

    CCTK_TimerI(i, td);
    const cTimerVal * const tv = CCTK_GetClockValue(timers_clock, td);
    const char * const name = CCTK_TimerName(i);

    if (tv)
    {
      value = CCTK_TimerClockSeconds(tv);
    }
    else
    {
      CCTK_VWarn(1, __LINE__, __FILE__, CCTK_THORNSTRING,
          "Clock \"%s\" not found for timer #%d \"%s\"", timers_clock, i,
          CCTK_TimerName(i));
    }

    Util_asprintf(&sql_stmt, "insert into %s values ('%s', %lf, %d)",
        "timerreport", name, value, CCTK_MyProc(cctkGH));

    CaSQLite_Exec(sql_stmt);
  }
  CaSQLite_Exec("commit");
  CCTK_TimerDestroyData(td);
}
