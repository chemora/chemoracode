 /*@@  -*- c++ -*-
   @file      util.h
   @date      Fri Jan 10 15:35:28 2014
   @author    David Koppelman
   @desc

   @enddesc
 @@*/

#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <map>
#include <string>

 /// CUDA API Error-Checking Wrapper
///
#define CE(call)                                                              \
 {                                                                            \
   const cudaError_t rv = call;                                               \
   if ( rv != cudaSuccess )                                                   \
     {                                                                        \
       printf("CUDA error %d, %s\n",rv,cudaGetErrorString(rv));               \
       exit(1);                                                               \
     }                                                                        \
 }


/// Device Address Convenience Functions
//
// These functions and macros enable host code to obtain device
// addresses of CUDA global-scope symbols using their names (as strings).

typedef std::map<std::string,const char*> Map_String_to_Addr;
extern Map_String_to_Addr name_to_dev_sym;

//
// Return the Device Address of CUDA Symbol NAME
//
//
inline const char*
p_cuda_sym_get_addr(const char* name)
{
  // Earlier versions of CUDA can use strings.
  //
  if ( CUDA_VERSION < 5000 ) return (const char *) name;

  Map_String_to_Addr::iterator it = name_to_dev_sym.find(name);
  if ( it == name_to_dev_sym.end() )
    {
      CCTK_VWarn
        (CCTK_WARN_ALERT, __LINE__,__FILE__, CCTK_THORNSTRING,
         "CUDA variable %s not registered.\n"
         "  Maybe add the following to an initialization routine in an nvcc-compiled\n"
         "  file (one typically ending in .cu):\n\n"
         "    __host__ void my_init_routine() {\n"
         "      GET_SYM(%s);\n"
         "    }\n",
         name,name);
      return NULL;
    }

  return it->second;
}

//
// Copy Host Memory into CUDA Symbol
//
inline void
p_cuda_to_sym(const char* name, const void *host_addr, size_t host_size)
{
  const void *sym_dev = p_cuda_sym_get_addr(name);
  CE( cudaMemcpyToSymbol
      ((const char *)sym_dev, (const void *)host_addr, host_size, 0, cudaMemcpyHostToDevice) );
}

#define GET_SYM(sym) name_to_dev_sym[#sym] = (const char*) &sym;
#define TO_SYM(sym, host_var) p_cuda_to_sym(sym, &host_var, sizeof(host_var) )


 /// Convenience Class for Passing Arguments to CUDA Kernel.
///
class MB_Cuda_Setup_Args
{
public:
  MB_Cuda_Setup_Args() { offset = 0; }
  template <typename T> void operator += (T& v)
  {
    const int size = sizeof(v);
    CE( cudaSetupArgument(&v,size,offset) );
    offset += size;
  }
private:
  int offset;
};


#endif
