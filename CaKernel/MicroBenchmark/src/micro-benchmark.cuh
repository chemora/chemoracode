 /*@@
   @file      micro-benchmark.cuh
   @date      Fri Jan 10 15:34:57 2014
   @author    David Koppelman
   @desc 
   
   @enddesc 
 @@*/

#ifndef MICRO_BENCHMARK_CUH
#define MICRO_BENCHMARK_CUH

#include <cuda.h>
#include <cctk.h>
#include <assert.h>
#include <map>
#include <string>

#include "util.h"

// If 1, use the read-only cache if available.
#define USE_RO_CACHE 1

#define CACHE_LATENCY_UNROLL_DEGREE 2
#define CACHE_LATENCY_BLOCK_LG 6

typedef double Real;


struct Constant_Data_Verification
{
  Real *a;
  Real *c;
  int array_size;
  int local_size;  // Number of elements of local array that are accessed.
  int local_size_max;  // Number of elements in (statically alloc) local array.
  int iters_back;
  int shared_load_guard_mask;
  int local_store_guard_mask;

};


__host__ void init_vars();
__host__ void launch_verification(dim3 dg, dim3 db);

#endif
