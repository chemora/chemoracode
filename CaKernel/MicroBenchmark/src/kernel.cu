 /*@@
   @file      kernel.cu
   @date      Fri Jan 10 15:36:18 2014
   @author    David Koppelman
   @desc

   @enddesc
 @@*/

#include "micro-benchmark.cuh"

#define UN __attribute__ ((unused))


// Test whether the use of a level 1 read-only cache is possible based
// on target provided to the compiler.
//
#if ( defined(__CUDACC__) && __CUDA_ARCH__ >= 350 )
#define TARGET_HAS_RO_CACHE 1
#else
#define TARGET_HAS_RO_CACHE 0
#endif

// Provide a macro indicating whether we will be trying to use
// the read-only cache. This means we want to and we can based on
// compile-time information.  If the compile-time target has a RO
// cache but the run-time target does not execution will fail.
//
#define USING_RO_CACHE ( USE_RO_CACHE && TARGET_HAS_RO_CACHE )

__constant__ int iteration_count;
__constant__ int *ai;
__constant__ int *ci;

__constant__ int zero; //  Shhh! Don't tell the compiler its value!

// Constants used by the verification kernel.
__constant__ Constant_Data_Verification cdv;

extern "C" __global__ void microbm_cache_size_and_lat(int size_per_block_lg);
extern "C" __global__ void microbm_verification();


// Remember devices addresses of symbols for use by host-side code.
//
__host__ void
init_vars()
{
  GET_SYM(ai); GET_SYM(ci);
  GET_SYM(iteration_count);
  GET_SYM(zero);
  GET_SYM(microbm_cache_size_and_lat);
  GET_SYM(microbm_verification);
  GET_SYM(cdv);
}


//
// Read-Only Cache Macros
//
//
// These macros will read from the read-only cache if that is possible
// and wanted.
//
__device__ inline double ptx_load(const double *addr)
{
#if USING_RO_CACHE
  double rv;
  asm("ld.global.nc.f64 %0, [%1];" : "=d"(rv) : "l"(addr) );
  return rv;
#else
  return *addr;
#endif
}

__device__ inline float ptx_load(const float *addr)
{
#if USING_RO_CACHE
  float rv;
  asm("ld.global.nc.f32 %0, [%1];" : "=f"(rv) : "l"(addr) );
  return rv;
#else
  return *addr;
#endif
}

__device__ inline int ptx_load(const int *addr)
{
#if USING_RO_CACHE
  int rv;
  asm("ld.global.nc.s32 %0, [%1];" : "=r"(rv) : "l"(addr) );
  return rv;
#else
  return *addr;
#endif
}


///
/// Cache Size and Latency
///
extern "C" __global__ void
microbm_cache_size_and_lat(int size_per_block_lg)
{
  const int blockDim_x = 1 << CACHE_LATENCY_BLOCK_LG;
  const bool blocks_disjoint = true;
  const int size_per_block = 1 << size_per_block_lg;
  const int idx_mask = size_per_block - 1;

  const int tidx = threadIdx.x + blockIdx.x * blockDim_x;
  const size_t block_base = blocks_disjoint ? blockIdx.x * size_per_block : 0;

  int sum = 0;

#pragma unroll 1
  for ( int i = 0; i < iteration_count; i += CACHE_LATENCY_UNROLL_DEGREE )
    {
      const size_t base =
        block_base + ( threadIdx.x + i * blockDim_x & idx_mask );

      // Note: Code written so that load instructions within loop will
      // use constant offsets. For this to happen the block size and
      // unroll degree must be compile-time constants.

#pragma unroll
      for ( int j = 0; j < CACHE_LATENCY_UNROLL_DEGREE; j++ )
        sum += ptx_load( &ai[ base + j * blockDim_x ] );
    }

  ci[tidx] = sum;
}


///
/// Verification
///
 // Verification of performance monitoring code.

__host__ void
launch_verification(dim3 dg, dim3 db)
{
  const int shared = 0;
  microbm_verification<<<dg,db,shared>>>();
}

extern "C" __global__ void
microbm_verification()
{
  int idx_start = threadIdx.x + blockIdx.x * blockDim.x;
  int thread_count = blockDim.x * gridDim.x;

  __shared__ Real shared_data[1024];

  Real csum = 0;
  const int lane UN = threadIdx.x & 0x1f;
  int iter = -1;

  const int iters_back = 1;
  assert( iters_back == cdv.iters_back );

  //
  // Global Loads
  //
  for ( int idx = idx_start; idx < cdv.array_size; idx += thread_count )
    {
      iter++;
      // This access expected to miss.
      Real aval = ptx_load(&cdv.a[idx]);

      // This access expected to hit (if iters_back is not too small).
      Real bval = iter >= iters_back
        ? ptx_load(&cdv.a[idx - iters_back * thread_count + zero]) : 1.234f;

      csum += aval + Real(2.0) * bval;
    }


  //
  // Initialize Local Array
  //
  const int max_local_size = 512 / sizeof(Real);
  assert( cdv.local_size <= max_local_size );
  Real k[max_local_size];
  for ( int i=0; i<cdv.local_size; i++ ) k[i] = csum + threadIdx.x;

  __syncthreads();
  iter = -1;

  //
  // Local Loads and Stores, and Shared Loads and Stores
  //
  for ( int idx = idx_start; idx < cdv.array_size; idx += thread_count )
    {
      iter++;

      Real t1 = csum + idx;
      Real t2 = csum * idx;

      // Local load, expect a hit.
      t1 += k[0];

      // Local load, expect a miss on most accesses.
      t1 += k[ iter % cdv.local_size ];

      // Execute local store every few iterations of the idx loop
      // (based on the value of cdv.local_store_guard_mask). The loop
      // below prevents the compiler from predicating the local store,
      // which makes it easier to verify that the performance
      // monitoring code is correctly counting local stores.
      for ( int i = zero; i < ( iter&cdv.local_store_guard_mask ? 0 : 1 ); i++ )
        k[int(csum)%cdv.local_size] = csum + i;

      for ( int i = zero; i < ( iter&cdv.shared_load_guard_mask ? 0 : 1 ); i++ )
        t1 += shared_data[(threadIdx.x<<2)&0x3ff];

      shared_data[threadIdx.x] = t2;

      const Real aval = lane & 1 ? t1 + t2 : t2 - t1;

      csum += aval;
    }

  // Global Store
  cdv.c[idx_start] = csum;
}
