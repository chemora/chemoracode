 /*@@
   @file      micro-benchmark.cc
   @date      Fri Jan 10 15:34:01 2014
   @author    David Koppelman
   @desc

   @enddesc
 @@*/

#include <stdio.h>
#include <assert.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include <pstring.h>

#include "util.h"
#include "micro-benchmark.cuh"
#include "nvidiaperf.h"

using namespace std;

Map_String_to_Addr name_to_dev_sym;

static void run_cache_size_and_lat(CCTK_ARGUMENTS);
static void run_verification(CCTK_ARGUMENTS);


extern "C" int
MicroBenchmark_Startup()
{
  NPerf_event_tracing_off();
  init_vars();  // Collect data needed to map CUDA symbols to addresses.
}

extern "C" void
MicroBenchmark_Run(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_PARAMETERS;

  string bm_name(microbenchmark_bm_name);

  if ( bm_name == "cache_size_and_lat" )
    run_cache_size_and_lat(CCTK_PASS_CTOC);
  else if ( bm_name == "verification" )
    run_verification(CCTK_PASS_CTOC);    
  else
    CCTK_VWarn
      (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,
       "Unknown benchmark name, \"%s\", "
       "assigned to parameter microbenchmark_bm_name.",
       microbenchmark_bm_name);
}

extern "C" void
MicroBenchmark_Terminate(CCTK_ARGUMENTS)
{
  CCTK_VInfo(CCTK_THORNSTRING, "The microbenchmark run has completed.\n");
}

static cudaDeviceProp
get_device_prop()
{
  int device;
  CE( cudaGetDevice(&device) );
  cudaDeviceProp device_prop;
  CE( cudaGetDeviceProperties(&device_prop, device) );
  return device_prop;
}


 /// Provide Timing Data for Determining Cache Sizes and Latencies
//
static void
run_cache_size_and_lat(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // Run a kernel in which each thread repeatedly accesses a region of
  // global memory. The kernel is run multiple times, with the amount
  // of data per CUDA block doubling each time.

  // Execution parameters are chosen so that the execution time of the
  // kernel is about the same as the execution time of a thread. The
  // code is written so that the number of overlapped loads per thread
  // is equal to the loop unroll degree.
  //
  // In the fastest running kernel the load latency is defined to be
  // zero. (In reality it will be about 24 cycles on CC 2.X devices
  // and 12 cycles on CC 3.X devices.) Let I denote the number of
  // loads performed by a thread (iteration_count), and let D be the
  // unroll degree (CACHE_LATENCY_UNROLL_DEGREE). The code was written
  // so that the execution time of a thread is I/D load latencies,
  // plus code overhead. Let E_i denote the execution time of a kernel
  // accessing data of size i, and let the i for the minimum E_i be
  // m. The latency computed by the code for data size i is:
  //
  // Lat_i = ( E_i - E_m ) / ( I/D )

  // Limitations:
  //
  // The code does not try to determine L1 cache hit latency.
  // Results are only useful if there is a high-speed cache, this
  // is so for CC 2.X devices and CC 3.5, but NOT for CC 3.0.

  // Note:
  //
  // See USE_RO_CACHE, CACHE_LATENCY_BLOCK_LG, and CACHE_LATENCY_UNROLL_DEGREE
  // in file micro-benchmark.cuh.

  const cudaDeviceProp device_prop(get_device_prop());
  const int max_report_iteration = 5; // Maximum number of iterations to report.
  const bool report = cctk_iteration < max_report_iteration;

  const int size_lg_max = 23;
  const int block_lg = CACHE_LATENCY_BLOCK_LG;

  const bool have_l1rw_cache = device_prop.major == 2;
  const bool have_ro_cache = device_prop.major == 3 && device_prop.minor >= 5;
  const bool using_ro_cache = USE_RO_CACHE && have_ro_cache;

  dim3 dg, db;
  // Just one block, to make it easier to determine L2 size and
  // to avoid interference effects when determining L2 latency.
  dg.x = 1;
  dg.y = dg.z = 1;

  // Block size should be small enough to: (1) avoid reaching
  // bandwidth limits and (2) keep the code execution limited (so that
  // instructions dependent on a load have to wait even on a cache
  // hit).
  db.x = 1 << block_lg;
  db.y = db.z = 1;

  const int thread_count = dg.x * db.x;
  const int iteration_count = 1 << 18;

  const int array_size = dg.x << size_lg_max;
  static bool inited = false;
  const int elt_size_lg = 2;
  const double clock_freq_kHz = device_prop.clockRate;

  const int size_lg_start = block_lg + elt_size_lg;

  static int *a, *c;
  static void *a_dev, *c_dev;
  assert( 1 << elt_size_lg == sizeof(*a) );

  if ( !inited )
    {
      inited = true;

      // Initialize a Constant
      //
      TO_SYM("iteration_count",iteration_count);

      // Prepare data arrays.
      //
      a = (int*) malloc( array_size * sizeof(a[0]) );
      c = (int*) malloc( array_size * sizeof(c[0]) );

      CE(cudaMalloc(&a_dev, array_size * sizeof(a[0]) ));
      CE(cudaMalloc(&c_dev, array_size * sizeof(c[0]) ));

      TO_SYM("ai",a_dev);  TO_SYM("ci",c_dev);

      // Initialize input arrays.
      //
      int xi = random();
      for ( int i=0; i<array_size; i++ ) { a[i] = xi;  xi += 1; }

      // Move input array to CUDA.
      //
      CE(cudaMemcpy(a_dev, a, array_size * sizeof(a[0]),
                    cudaMemcpyHostToDevice));
    }

  cudaEvent_t bm_start, bm_end;
  CE( cudaEventCreate(&bm_start) );
  CE( cudaEventCreate(&bm_end) );

  const int64_t request_size =
    int64_t(thread_count) * iteration_count << elt_size_lg;

  // The minimum time kernel takes to execute.
  float min_et = 1000;

  const char * microbm_cache_latency_d =
    p_cuda_sym_get_addr("microbm_cache_size_and_lat");

  CE( cudaFuncSetCacheConfig(microbm_cache_latency_d,cudaFuncCachePreferL1) );

  if ( report )
    printf
      ("Block Size: %4d,  Block Count: %2d,  Unroll Degree: %2d,  L1: %2s\n",
       db.x, dg.x, CACHE_LATENCY_UNROLL_DEGREE,
       using_ro_cache ? "RO" : have_l1rw_cache ? "RW" : "NONE!!");

  for ( int size_lg = size_lg_start; size_lg < size_lg_max; size_lg++ )
    {
      // Number of elements per block.
      const int size_per_block_lg = size_lg - elt_size_lg;
      const int size_per_block = 1 << size_per_block_lg;
      assert( size_per_block * dg.x <= array_size );
      CE( cudaConfigureCall(dg, db, 1 << 14) );
      CE( cudaSetupArgument(&size_per_block_lg, sizeof(size_per_block_lg), 0) );
      CE( cudaEventRecord(bm_start,0) );
      if ( size_lg == size_lg_max - 1 ) NPerf_event_tracing_on();
      CE( cudaLaunch(microbm_cache_latency_d) );
      NPerf_event_tracing_off();
      CE( cudaEventRecord(bm_end,0) );
      CE( cudaEventSynchronize(bm_end) );
      float cuda_time_ms = 0;
      CE( cudaEventElapsedTime(&cuda_time_ms,bm_start,bm_end) );
      if ( !report ) continue;
      min_et = min( cuda_time_ms, min_et );
      const double tput_GBps = request_size / ( 1e6 * cuda_time_ms );
      const double extra_time_ms = cuda_time_ms - min_et;

      const double load_latency_ms =
             CACHE_LATENCY_UNROLL_DEGREE * extra_time_ms / iteration_count;

      printf("Size_lg %2d  %7.3f ms  %5.2f GB/s  Lat %6.1f cyc  %6.1f ns\n",
             size_lg, cuda_time_ms, tput_GBps,
             load_latency_ms * clock_freq_kHz, load_latency_ms * 1e6);
    }

  CE( cudaEventDestroy(bm_start) );
  CE( cudaEventDestroy(bm_end) );
  CE( cudaMemcpy(c, c_dev, array_size * sizeof(c[0]), cudaMemcpyDeviceToHost) );
}


static void
run_verification(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  static Constant_Data_Verification cd;

  const cudaDeviceProp device_prop(get_device_prop());

  const bool have_l1rw_cache = device_prop.major == 2;
  const bool have_ro_cache = device_prop.major == 3 && device_prop.minor >= 5;
  const bool using_ro_cache = USE_RO_CACHE && have_ro_cache;

  dim3 db;
  db.x = 256;
  db.y = db.z = 1;

  // One block per MP, to avoid L1 and RO cache interference.
  dim3 dg;
  dg.x = device_prop.multiProcessorCount;
  dg.y = dg.z = 1;

  const int array_elt_pt = 1000;  // Elements per thread.

  const int array_size = db.x * dg.x * array_elt_pt;
  const int array_size_prep = array_size + 128;
  static bool inited = false;

  static Real *a, *c;
  static void *a_dev, *c_dev;

  if ( !inited )
    {
      inited = true;

      // Instantiate data arrays.
      //
      a = (Real*) malloc( array_size_prep * sizeof(a[0]) );
      c = (Real*) malloc( array_size_prep * sizeof(c[0]) );

      CE(cudaMalloc(&a_dev, array_size_prep * sizeof(a[0]) ));
      CE(cudaMalloc(&c_dev, array_size_prep * sizeof(c[0]) ));
      cd.a = (Real*) a_dev;
      cd.c = (Real*) c_dev;

      // Initialize input array.
      //
      Real xi = drand48();
      for ( int i=0; i<array_size_prep; i++ ) { a[i] = xi;  xi += 0.1; }

      // Move input array to CUDA.
      //
      CE(cudaMemcpy(a_dev, a, array_size_prep * sizeof(a[0]),
                    cudaMemcpyHostToDevice));

      const int zero = 0;
      TO_SYM("zero",zero);
      cd.array_size = array_size;

      const int iters_back = cd.iters_back = 1;

      // Masks below must be one minus a power of 2. E.g., 1, 3, 7, 15, ...
      const int shared_load_guard_mask = cd.shared_load_guard_mask = 0x1;
      const int local_store_guard_mask = cd.local_store_guard_mask = 0x3;

      const int local_size_max = cd.local_size_max = 25;
      const int blockDim_x = db.x;
      const int l1_size = 3 << 14;
      const int l1_size_per_thread = l1_size / blockDim_x;
      const int l1_size_per_thread_elts = l1_size_per_thread / sizeof(Real);
      const int local_size = cd.local_size = 2 * l1_size_per_thread_elts;

      const int array_load_cnt = array_elt_pt + array_elt_pt - iters_back;
      const int array_load_miss = array_elt_pt;
      const int global_rw_cnt = using_ro_cache ? 0 : array_load_cnt;
      const int global_rw_miss = 
        using_ro_cache ? 0 : have_l1rw_cache ? array_load_miss : array_load_cnt;
      const int global_store_cnt = 1;
      const int global_ro_cnt =
        using_ro_cache ? array_load_cnt : 0;
      const int global_ro_miss = using_ro_cache ? array_load_miss : 0;
      const int shared_load_cnt = array_elt_pt / ( shared_load_guard_mask + 1 );
      const int shared_store_cnt = array_elt_pt;
      const int local_load_cnt = 2 * array_elt_pt;
      const int local_store_cnt = 
        local_size + array_elt_pt / ( local_store_guard_mask + 1 );
      const int local_load_miss = array_elt_pt - 1;

      TO_SYM("cdv",cd);

      pString exp_outcomes;

      exp_outcomes.sprintf("Microbenchmark \"%s\" Selected\n",
                           microbenchmark_bm_name);
      exp_outcomes += "Expected Outcomes of Load and Store Instructions\n\n";
      exp_outcomes += "  \"Acc\" is number of accesses per thread.\n";
      exp_outcomes += 
        "  Number of misses is to L1 cache, is approximate,\n";
      exp_outcomes += 
        "  and assumes for Fermi that cached globals not deactivated.\n\n";

      exp_outcomes.sprintf
        ("Type-----   ----Loads-----     ---Stores---\n");
      exp_outcomes.sprintf
        ("              --Acc  -Miss     --Acc\n");
      exp_outcomes.sprintf
        ("Global RW     %5d  %5d     %5d\n",
         global_rw_cnt,
         global_rw_miss,
         global_store_cnt);
      exp_outcomes.sprintf
        ("Global RO     %5d  %5d\n",
         global_ro_cnt,
         global_ro_miss);
      exp_outcomes.sprintf
        ("Shared        %5d            %5d\n",
         shared_load_cnt,
         shared_store_cnt);

      exp_outcomes.sprintf
        ("Local         %5d  %5d     %5d\n",
         local_load_cnt,
         local_load_miss,
         local_store_cnt);

      CCTK_VInfo(CCTK_THORNSTRING, "%s", exp_outcomes.s);

    }

  CE( cudaFuncSetCacheConfig
      (p_cuda_sym_get_addr("microbm_verification"), cudaFuncCachePreferL1) );

  NPerf_event_tracing_on();
  launch_verification(dg,db);

  CE(cudaMemcpy(c, c_dev, array_size_prep * sizeof(c[0]),
                cudaMemcpyDeviceToHost));
}

