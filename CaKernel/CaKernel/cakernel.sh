#!/bin/bash


# Provide include directory needed by template-generated code.
#
this_dir=$(dirname ${0})
expandee_include_dir=${this_dir}/src
echo "INCLUDE_DIRECTORY ${expandee_include_dir}"

# CaKernel depends on Piraha which checks the existence of Java.
# We might skip the Java section below.
# Set up shell
if [ "$SILENT" = "no" ]; then
    set -x                          # Output commands
fi
set -e                          # Abort on errors
have_jre=0

if java -version 2> /dev/null; then
    have_jre=1
else
    echo 'BEGIN ERROR'
    echo 'You will need a java runtime environment to parse the cakernel.ccl file. Abort !'
    echo 'END ERROR'
    exit 1
fi

if [ -z "$HAVE_PIRAHA" ]; then
    echo 'BEGIN ERROR'
    echo 'You need piraha. Abort !'
    echo 'END ERROR'
    exit 1
fi

# set locations
#echo BEGIN MESSAGE
#env
#echo END MESSAGE
#exit 5
THORN=CaKernel
NAME=cakernel
SRCDIR=$(dirname $0)
DONE_FILE=${SCRATCH_BUILD}/done/${THORN}
DIST=${SRCDIR}/dist
DIST_JAR=${DIST}/${NAME}.jar
DEPS=${PACKAGE_DIR}/CaKernel/CaKernel/src/templates/deps.ccl
AUXDIR=${TOP}/scratch/CaKernelAUX
MK=${AUXDIR}/cakernel.mk
mkdir -p ${AUXDIR}
mkdir -p ${DIST}

# Default to build
echo 'BEGIN MESSAGE'
if [ "z${CAKERNEL_JAR}" = z ]
then
  CAKERNEL_JAR=NO_BUILD
fi

if [ "${CAKERNEL_JAR}" = BUILD ]
then
  pushd ${SRCDIR}/src >/dev/null 2>/dev/null
  echo ${DIST_JAR} : ${PACKAGE_DIR}/CaKernel/CaKernel/src/edu/lsu/cct/pegs/*.peg ./edu/lsu/cct/cakernel/*.java ${PIRAHA_JAR} >${MK} 
  echo -e "\techo Building cakernel.jar" >> ${MK}
  echo -e "\tjavac -target 1.5 -source 1.5 -Xlint:unchecked -d ${DIST} ./edu/lsu/cct/cakernel/*.java -cp ${PIRAHA_JAR}:." >> ${MK}
  echo -e "\tmkdir -p `dirname ${DIST_JAR}`" >> ${MK}
  echo -e "\tmkdir -p ${DIST}/edu/lsu/cct/pegs" >> ${MK}
  echo -e "\tcp ${PACKAGE_DIR}/CaKernel/CaKernel/src/edu/lsu/cct/pegs/*.peg ${DIST}/edu/lsu/cct/pegs/" >> ${MK}
  echo -e "\tmkdir -p ${SCRATCH_BUILD}/build/CaKernel/cakernel/templates" >> ${MK}
  echo -e "\t(cd ${DIST}; jar cf ${DIST_JAR} edu)" >> ${MK}
  ${MAKE} -f ${MK}
fi

mkdir -p ${SCRATCH_BUILD}/build/CaKernel/cakernel/templates
cp -r ${PACKAGE_DIR}/CaKernel/CaKernel/src/templates ${SCRATCH_BUILD}/build/CaKernel/cakernel

for file in `echo ${PACKAGE_DIR}/*/*/cakernel.ccl`
do
  if [ -r $file ]
  then
    java -cp ${DIST_JAR}:${PIRAHA_JAR} edu.lsu.cct.cakernel.CCLParser "${file}" ${DEPS} $TOP/ThornList ${CCTK_HOME}/arrangements ${CCTK_HOME} ${TOP} 2>&1
  fi
done
echo END MESSAGE

echo "BEGIN MAKE_DEFINITION"
echo "HAVE_CAKERNEL = 1"
if [ $CAKERNEL_JAR = BUILD ]
then
# Preserve build designation
echo "CAKERNEL_JAR = BUILD"
else
echo "CAKERNEL_JAR = ${DIST_JAR}"
fi
echo "END MAKE_DEFINITION"
echo "BEGIN MESSAGE"
echo "Configured CaKernel"
echo "END MESSAGE"
