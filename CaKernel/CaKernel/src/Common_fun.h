#pragma once
/* 
  Function copy/pasted from GenericFD.c file. The reason of copying it is that we don't
  want CaKernel to be dependent of GenericFD.
*/
extern "C"
int CaKernel_GetBoundaryInfo(const void * cctkGH, const int * cctk_lsh,
                               const int * cctk_bbox, const int * cctk_nghostzones, int * imin, 
			                         int * imax, int * is_symbnd, int * is_physbnd, int * is_ipbnd);

