
#include "MoL_interface.hpp"


int getOptimalNumBlocks(){
  return 60;
}

void OpSetVar(CCTK_REAL *restrict const var, CCTK_REAL const val, CCTK_INT const npoints)
{
  int blocks = getOptimalNumBlocks();  
  OpSetVar_dev<<<blocks, TILE>>>(var, val, npoints);
}



void OpCopyVar(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src, 
               CCTK_INT const npoints)
{
  int blocks = getOptimalNumBlocks();  
  OpCopyVar_dev<<<blocks, TILE>>>(var, src, npoints);
}


void OpUpdateVar(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src,
                 CCTK_REAL const fact, CCTK_INT const npoints)
{
  int blocks = getOptimalNumBlocks();  
   OpUpdateVar_dev<<<blocks, TILE>>>(var, src, fact, npoints);
}


void OpUpdateScaleVar(CCTK_REAL *restrict const var, CCTK_REAL const scale,
                      CCTK_REAL const *restrict const src, CCTK_REAL const fact, 
                      CCTK_INT const npoints)
{
  int blocks = getOptimalNumBlocks();  
  OpUpdateScaleVar_dev<<<blocks, TILE>>>(var, scale, src, fact, npoints);
}


void OpUpdateVar2(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src1,
                  CCTK_REAL const fact1, CCTK_REAL const *restrict const src2,
                  CCTK_REAL const fact2, CCTK_INT const npoints)
{
  int blocks = getOptimalNumBlocks();  
  OpUpdateVar2_dev<<<blocks, TILE>>>(var, src1, fact1, src2, fact2, npoints);
}


void OpUpdateScaleVar2(CCTK_REAL *restrict const var, CCTK_REAL const scale,
                       CCTK_REAL const *restrict const src1, CCTK_REAL const fact1,
                       CCTK_REAL const *restrict const src2, CCTK_REAL const fact2,
                       CCTK_INT const npoints)
{
  int blocks = getOptimalNumBlocks();  
  OpUpdateScaleVar2_dev<<<blocks, TILE>>>(var, scale, src1, fact1, src2, fact2, npoints);
}


void OpUpdateVar3(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src1,
                  CCTK_REAL const fact1, CCTK_REAL const *restrict const src2,
                  CCTK_REAL const fact2, CCTK_REAL const *restrict const src3,
                  CCTK_REAL const fact3, CCTK_INT const npoints)
{
  int blocks = getOptimalNumBlocks();  
  OpUpdateVar3_dev<<<blocks, TILE>>>(var, src1, fact1, src2, fact2, src3, fact3, npoints);
}


void OpUpdateScaleVar3(CCTK_REAL *restrict const var, CCTK_REAL const scale,
                       CCTK_REAL const *restrict const src1, CCTK_REAL const fact1,
                       CCTK_REAL const *restrict const src2, CCTK_REAL const fact2,
                       CCTK_REAL const *restrict const src3, CCTK_REAL const fact3,
                       CCTK_INT const npoints)
{
  int blocks = getOptimalNumBlocks();  
  OpUpdateScaleVar3_dev<<<blocks, TILE>>>(var, scale, src1, fact1, src2, fact2, src3, fact3, npoints);
}
