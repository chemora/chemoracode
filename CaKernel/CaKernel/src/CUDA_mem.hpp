#pragma once

#if HAVE_CUDA == 1

#include "Common_mem.hpp"

int CaCUDA_RegisterTl(CCTK_ARGUMENTS, int vi, int tl);

void * CaCUDA_GetVarI(CCTK_ARGUMENTS, int vi, int tl);

int CaCUDA_RegisterTl(CCTK_ARGUMENTS, int vi, int tl);

int CaCUDA_RegisterMem(CCTK_ARGUMENTS, int vi, int num_tl);

int CaCUDA_UnRegisterMem(CCTK_ARGUMENTS, int vi);

void * CaCUDA_AllocPtr(size_t dataSize);

void CaCUDA_FreePtr(void ** ptr);

#endif
