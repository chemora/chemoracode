
#include "Common_mem.hpp"
#include "CUDA_mem.hpp"
#include "cctk.h"

#include<stdio.h>
#include<vector>
#include<algorithm>
#include<deque>
#include<string>

using std::vector;
using std::deque;
using std::string;

memory_t pointers;
typedef void (*thorn_fun_t)(CCTK_ARGUMENTS);

deque<thorn_fun_t> functions;
map<string, int> * cakernel_thorns = 0;
       
extern "C"
void CaKernel_CompileOnly(CCTK_ARGUMENTS){
  DECLARE_CCTK_ARGUMENTS
  *compile_only=1;
}

extern "C"
void CaKernel_CompileNotOnly(CCTK_ARGUMENTS){
  DECLARE_CCTK_ARGUMENTS
  *compile_only=0;
}

int CaKernel_VarTypeHelper(const cGH* cctkGH, int vi, size_t * np, size_t *var_size, 
                           size_t offset[], size_t length[], size_t * vector_length, 
                           size_t * max_tl, int * type, int * gi, int * vars_in_group,
                           int * first_vi){

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  *np = cctkGH->cctk_ash[0] * cctkGH->cctk_ash[1] * cctkGH->cctk_ash[2];

  int var_type = CCTK_VarTypeI(vi);
  *var_size = CCTK_VarTypeSize(var_type);
  
  assert(var_size>0);

  for (int d=0; d<dim; ++d) {
    offset[d] = 0;
    length[d] = cctkGH->cctk_lsh[d];
  }

  offset[0] *= *var_size;
  length[0] *= *var_size;
  
  int group_index = CCTK_GroupIndexFromVarI(vi);

  assert(group_index>=0);

  cGroup group_data_buffer;

  int ierr = CCTK_GroupData(group_index, &group_data_buffer);
  assert(!ierr);

//  #warning "TODO: trying to support different variables types. Don't know if it will work.."
//  assert(group_data_buffer.grouptype == CCTK_GF); //ONLY GF SUPPORTED AT THIS POINT
  if(group_data_buffer.grouptype == CCTK_SCALAR)
    *np = 1;
  else if(group_data_buffer.grouptype == CCTK_ARRAY){
    *np = 1;
    int dims = 1; 
    dims = CCTK_GroupDimI(group_index);
    assert(dims >= 0);
    CCTK_INT ** sizes;
    sizes = CCTK_GroupSizesI(group_index);
    assert(sizes);
    for(int i = 0; i < dims; i++)
      *np *= sizes[0][i];
  }
  *type = group_data_buffer.grouptype;
  *gi = group_index;
  *vector_length = 1;

  if(group_data_buffer.vectorgroup) 
    *vector_length = group_data_buffer.vectorlength;

//We want to copy groups as whole groups only if they are not GF. That is why we change
//the truth here a little bit... 
  if(*type == CCTK_GF){
    *first_vi = vi;
    *vars_in_group = 1;
  }
  else{
    *first_vi = CCTK_FirstVarIndexI(group_index);
    assert(*first_vi >= 0);
    *vars_in_group = group_data_buffer.numvars / *vector_length;
  }

  *max_tl = group_data_buffer.numtimelevels;
  return 0;
}

extern "C"
int CaKernel_Cycle_rl(cGH * cctkGH, int vi, int rl){
  DECLARE_CCTK_PARAMETERS;
  assert(vi >= 0);
  pointer_t type = cuda;
  memory_it_t it = pointers.find(key_ptr_t(type, vi, rl, 0)), it2;
  assert(it != pointers.end());
  int count = 0;
  for(it2 = it; it2 != pointers.end() && it->first.equals(it2->first, 3); ++it2, count++);
  --it2;
  assert(count == it2->first.tl - it->first.tl + 1);

  if (verbose)
    CCTK_VInfo(CCTK_THORNSTRING, "Cycling %s on device", CCTK_FullName(vi));

//  if(veryverbose) {
//    printf("Pointers befor: ");
//    for(map<int, ptr_t *>::iterator tmp = it->second->pointers.begin(); tmp != it->second->pointers.end(); tmp++){
//      if(veryverbose) printf("%lx ", (long)tmp->second);
//    }
//  }

  for(; it2 != it ;)
    std::swap(it2->second, (--it2)->second);
  

//  if(veryverbose) {
//    printf("pointers after: ");
//    for(map<int, ptr_t *>::iterator tmp = it->second->pointers.begin(); tmp != it->second->pointers.end(); tmp++){
//      printf("%lx ", (long)tmp->second);
//    }
//    printf("\n");
//  }
//  it->second->pointers.begin()->second = ptr;

  return 0; 
}

extern "C"
int CaKernel_Cycle(cGH * cctkGH, int vi){
  int rl = GetRefinementLevel(cctkGH);
  CaKernel_Cycle_rl(cctkGH, vi, rl);
  return 0;
}

int CaKernel_RegisterTl_rl(CCTK_ARGUMENTS, int vi, int rl, int tl, pointer_t type){
  assert(vi>=0);
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  if(!*device_process) return 0;

  void * ptr = 0;
  size_t np, var_size, vector_length = 1, max_tl;
  size_t offset[dim], length[dim];
  
  int gtype, gi, vars_in_group, first_vi;

  int ierr = CaKernel_VarTypeHelper(CCTK_PASS_CTOC, vi, &np, &var_size, offset,
               length, &vector_length, &max_tl, &gtype, &gi, &vars_in_group, &first_vi);
  assert(!ierr);
  assert (tl <= int(max_tl));

  for (int vi = first_vi; vi < first_vi + vars_in_group * vector_length; vi++){
    pair<memory_it_t, bool> insit =
      pointers.insert(std::pair<key_ptr_t, ptr_t *>(key_ptr_t(type, vi, rl, tl), (ptr_t*)0));

  
    if(insit.second == true){
      if(vi == first_vi){
        switch (type){
          case cuda:
#if HAVE_CUDA == 1
              ptr = CaCUDA_AllocPtr(np * var_size * vector_length * vars_in_group);
#endif
            break;
        }
      }
      assert(ptr);
      for(int tmpvi = 0; tmpvi < vector_length; ++tmpvi){
        if(veryverbose) CCTK_VInfo(CCTK_THORNSTRING, 
          "Registering variable in CaKernel: %s:%d:%d:%d", 
          CCTK_FullName(vi + tmpvi), vi + tmpvi, rl, tl);

        pointers[key_ptr_t(type, vi + tmpvi, rl, tl)] = new 
            ptr_t(&((char *)ptr)[np * var_size * (vi - first_vi + tmpvi)], 
               np, var_size, vector_length, gtype, gi, vars_in_group, ptr, 
               cctk_ash, cctk_lsh, cctk_nghostzones, cctk_bbox);
      }
      vi += vector_length;
    }else
      break;
  }
  return 0;  
}


int CaKernel_RegisterTl(CCTK_ARGUMENTS, int vi, int tl, pointer_t type){
  int rl = GetRefinementLevel(cctkGH);
  CaKernel_RegisterTl_rl(CCTK_PASS_CTOC, vi, rl, tl, type);
}


int CaKernel_SwapVarsI_rl(void *cctkGH_, int vi, int rl, int tl, int vi2, int rl2, int tl2){
  cGH * cctkGH = (cGH *) cctkGH_;
  assert(vi >= 0 && tl >= 0 && rl >= 0);
  assert(vi2>= 0 && tl2>= 0 && rl >= 0);
  pointer_t type = cuda;
  DECLARE_CCTK_ARGUMENTS;

  memory_it_t it = pointers.find(key_ptr_t(type, vi, rl, tl)),
              it2= pointers.find(key_ptr_t(type,vi2,rl2,tl2));
  assert(it != pointers.end() && it2!= pointers.end());

  std::swap(it->second, it2->second);
  return 0;
}

int CaKernel_SwapVarsI(void *cctkGH_, int vi, int tl, int vi2, int tl2){
  int rl = GetRefinementLevel(cctkGH_); 
  CaKernel_SwapVarsI_rl(cctkGH_, vi, rl, tl, vi2, tl2, rl);
}

extern "C"
const ptr_t * CaKernel_GetVarPtrI_rl(void *cctkGH_, int vi, int rl, int tl){
  cGH * cctkGH = (cGH *) cctkGH_;
  assert(vi >= 0 && tl >= 0 && rl >= 0);
  pointer_t type = cuda;

  memory_it_t it = pointers.find(key_ptr_t(type, vi, rl, tl));

  if (it == pointers.end())
  {
    deque<string> thorns;
    for(map<string, int>::iterator it = cakernel_thorns->begin(), 
                        it2 = cakernel_thorns->end(); it != it2; ++it){
      if(!it->second) thorns.push_back(it->first);
    }
    char* const out = (char*) malloc(5000 + thorns.size() * 100 );
    if(thorns.size()){
      int count = sprintf(out, 
      "\nThere are several CaKernel thorns which have been compiled but "
      "have not been initialized:\n\t");
      for(int i = 0; i < thorns.size(); i++){
        if(i) count += sprintf(&out[count], ", "); 
        count += sprintf(&out[count], "%s", thorns[i].c_str());
      }
      count += sprintf(&out[count], "\n"
      "You should schedule the init function for all CaKernel thorns in "
      "corresponding schedule.ccl files.\n"
      "For instance:\n"
      "schedule %" "s_Init in CCTK_BASEGRID after "                
      "Accelerator_SetDevice\n{\n"                                              
      "  LANG: C\n"                                                             
      "} \"Initialize CUDA Device\"\n", thorns[0].c_str());
    }
    CCTK_VWarn(CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,
               "internal error: cannot find a cuda pointer to variable %s "
               "timelevel %d refinement level %d;%s",
               CCTK_FullName(vi), tl, rl, out);
    free(out);
  }

  return it->second;
}

extern "C"
const ptr_t * CaKernel_GetVarPtrI(void *cctkGH_, int vi, int tl){
  int rl = GetRefinementLevel(cctkGH_); 
  return CaKernel_GetVarPtrI_rl(cctkGH_, vi, rl, tl);
}

extern "C"
void * CaKernel_GetVarI_rl(void *cctkGH_, int vi, int rl, int tl){
  return CaKernel_GetVarPtrI_rl(cctkGH_, vi, rl, tl)->ptr;
}

extern "C"
void * CaKernel_GetVarI(void *cctkGH_, int vi, int tl){
  int rl = GetRefinementLevel(cctkGH_); 
  return CaKernel_GetVarI_rl(cctkGH_, vi, rl, tl);
}

extern "C"
void * CaKernel_SafeGetVarI_rl(void *cctkGH_, int vi, int rl, int tl){
  int ierr = CaKernel_RegisterTl_rl((cGH *)cctkGH_, vi, rl, tl, cuda);
  assert(!ierr);
  return  CaKernel_GetVarI_rl(cctkGH_, vi, rl, tl);
}

extern "C"
void * CaKernel_SafeGetVarI(void *cctkGH_, int vi, int tl){
  int ierr = CaKernel_RegisterTl((cGH *)cctkGH_, vi, tl, cuda);
  assert(!ierr);
  return  CaKernel_GetVarI(cctkGH_, vi, tl);
}

static void disable_variable(int const varindex, char const * const optstring, void * const arg)
{
  vector<int> *disabled_variables = (vector<int> *) arg;
  disabled_variables->push_back(varindex);
}

bool CaKernel_VariableDisabled(int vi)
{
  DECLARE_CCTK_PARAMETERS;
  vector<int> disabled;
  CCTK_TraverseString(disable_storage, disable_variable, &disabled, CCTK_GROUP);
  return std::find(disabled.begin(), disabled.end(), vi) != disabled.end();
}

int CaKernel_RegisterMem_rl(void * cctkGH_, int vi, int rl, int tl){
  cGH * cctkGH = (cGH *) cctkGH_;
  pointer_t type = cuda;
  assert(vi>=0);
  
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if(!*device_process) return 0;

  if (CaKernel_VariableDisabled(vi)) {
    CCTK_VInfo (CCTK_THORNSTRING, "Skipping allocation of disabled variable '%s'", CCTK_FullName (vi));
    return 0;
  }

  for(int i = 0; i < tl; i++)
    CaKernel_RegisterTl_rl(CCTK_PASS_CTOC, vi, rl, i, type);
  
  return 0;
}

extern "C"
int CaKernel_RegisterMem(void * cctkGH_, int vi, int tl){
  int rl = GetRefinementLevel(cctkGH_); 
  CaKernel_RegisterMem_rl(cctkGH_, vi, rl, tl);
}

extern "C"
int CaKernel_UnRegisterMem_rl(void * cctkGH_, int vi, int rl){
  pointer_t type = cuda;
  cGH * cctkGH = (cGH *) cctkGH_;
  assert(vi>=0);
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  if(!*device_process) return 0;

  if(veryverbose) printf("Unregistering var %d\n", vi);
  memory_it_t it = pointers.find(key_ptr_t(type, vi, rl, 0)), it2 = it;
  if(it != pointers.end()){
    do{
      it = it2;
      if(it2->second->ptr == it2->second->gptr) 
        switch(type){
          case cuda:
#if HAVE_CUDA == 1
          CaCUDA_FreePtr(&it2->second->ptr);
          delete it2->second;
          it2->second = 0;
#endif
          break;
        }
      it2++;
    }while(it->first.equals(it2->first));
    delete it->second;
    it->second = 0;
  }
  
  return 0;
}

extern "C"
int CaKernel_UnRegisterMem(void * cctkGH_, int vi){
  int rl = GetRefinementLevel(cctkGH_); 
  CaKernel_UnRegisterMem_rl(cctkGH_, vi, rl);
  return 0;
}


void CaKernel_SchedulePreCompileFunction(void * cctkGH_, void (*funptr)(CCTK_ARGUMENTS)){
  functions.push_back(funptr);
  return;
}

extern "C"
void CaKernel_PreCompileFunction(CCTK_ARGUMENTS){
  DECLARE_CCTK_ARGUMENTS;
  if(*device_process){
    for(int i = 0, iend = functions.size(); i < iend; ++i){
      functions[i](CCTK_PASS_CTOC);
    }
  }
  return;
}

int CaKernel_RegisterThorn(string name){
  static int allocated = 0;
  if(!allocated){
    cakernel_thorns = new map<string, int>();
    allocated = true;
  }
  cakernel_thorns->insert(std::pair<string, int>(name, 0));
  return name.length();
}

void CaKernel_ThornInitialized(string name){
  if(!cakernel_thorns) CCTK_VWarn(CCTK_WARN_ABORT, __LINE__, __FILE__,
      CCTK_THORNSTRING, "None thorn was registered and %s is reporting as"
      " initialized", CCTK_THORNSTRING);
  if(cakernel_thorns->find(name) == cakernel_thorns->end())
    CCTK_VWarn(CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING, 
      "Thorn %s was not registered and is reporting as initialized",
      CCTK_THORNSTRING);
  (*cakernel_thorns)[name] = 1;
}

