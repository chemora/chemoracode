#pragma once

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include <assert.h>
#include <map>
using std::map;
using std::pair;

#define dim 3

typedef int pointer_t;
//values of pointer_T
#define cuda  1
#define opencl 2
#define host 3


struct key_ptr_t {
  union{
    struct{
      pointer_t type;
      int vi;
      int rl;
      int tl;
    };
    int v[4];
  };
  bool operator < (const key_ptr_t & second) const{
    const int iend = sizeof(v) / sizeof(int);
    int i;
    for (i = 0; i < iend && this->v[i] == second.v[i]; ++i);
    if(i == iend) return false;
    return this->v[i] < second.v[i];
  }

  bool equals(const key_ptr_t & key_ptr, int vals = 4) const{
    for(int i = 0; i < vals; i++)
      if(this->v[i] != key_ptr.v[i])
        return false;
    return true;
  }

  key_ptr_t() : type(0), vi(0), rl(0), tl(0) {}

  key_ptr_t(int type, int vi, int rl, int tl) : 
            type(type), vi(vi), rl(rl), tl(tl) { }
};

struct ptr_t {
  //Pointer
  void * ptr;

  //number of elements
  size_t np;

  //size of element
  size_t var_size;

  //size of element
  size_t vector_length;

  //CCTK_TYPE of variable
  int type;

  //group index
  int gi;

  //elements in the group
  int vars_in_group;

  //pointer to the first element of the group
  void * gptr;

  //the shape of the variable of the certain refinement level
  int ash[3];
  int lsh[3];
  int nghostzones[3];
  int bbox[6];
  
  void init_tabs( const int *ash = 0, 
                  const int *lsh = 0, const int * nghostzones = 0, 
                  const int * bbox = 0){
    for(int i = 0; i < 3; i++){
      this->ash[i] = ash ? ash[i] : 0;
      this->lsh[i] = lsh ? lsh[i] : 0;
      this->nghostzones[i] = nghostzones ? nghostzones[i] : 0;
      this->bbox[i*2] = bbox ? bbox[i*2] : 0;
      this->bbox[i*2+1] = bbox ? bbox[i*2+1] : 0;
    }
  }

  ptr_t() : ptr(0), np(0), var_size(0), vector_length(0), type(0), gi(0), 
    vars_in_group(0), gptr(0) {
   init_tabs();
  }

  ptr_t(void * _ptr, size_t _np, size_t _var_size, size_t _vector_length, 
      int _type, int _gi, int _vars_in_group, void * _gptr, const int * ash, 
      const int * lsh, const int * nghostzones, const int * bbox) : 
      ptr(_ptr), np(_np), var_size(_var_size), vector_length(_vector_length), 
      type(_type), gi(_gi), vars_in_group(_vars_in_group), gptr(_gptr) {
    init_tabs(ash, lsh, nghostzones, bbox);
  }
};

typedef map<key_ptr_t, ptr_t*> memory_t;
typedef memory_t::iterator memory_it_t;
extern memory_t pointers;

int CaKernel_RegisterTl(CCTK_ARGUMENTS, int vi, int tl, pointer_t type = cuda);
int CaKernel_RegisterTl_rl(CCTK_ARGUMENTS, int vi, int rl, int tl, pointer_t type);

extern "C"
void * CaKernel_GetVarI(void *cctkGH, int vi, int tl);

extern "C"
void * CaKernel_GetVarI_rl(void *cctkGH_, int vi, int rl, int tl);

extern "C"
void * CaKernel_SafeGetVarI(void *cctkGH_, int vi, int tl);

extern "C"
void * CaKernel_SafeGetVarI_rl(void *cctkGH_, int vi, int rl, int tl);

extern "C"
const ptr_t * CaKernel_GetVarPtrI(void *cctkGH_, int vi, int tl);

extern "C"
const ptr_t * CaKernel_GetVarPtrI_rl(void *cctkGH_, int vi, int rl, int tl);

extern "C"
int CaKernel_Cycle(cGH * cctkGH, int vi);

extern "C"
int CaKernel_Cycle_rl(cGH * cctkGH, int vi, int rl);

int CaKernel_VarTypeHelper(const cGH* cctkGH, int vi, size_t * np, size_t *var_size, 
                           size_t offset[], size_t length[], size_t * vector_length, 
                           size_t * max_tl, int * type, int * gi, int * vars_in_group,
                           int * first_vi);

bool CaKernel_VariableDisabled(int vi);

int inline iDivUp(int a, int b){
  return (a + b - 1) / b;
}

//extern "C"
//int CaKernel_RegisterMem(void * cctkGH_, int vi, int tl);
//
//extern "C"
//int CaKernel_UnRegisterMem(void * cctkGH_, int vi);
//
