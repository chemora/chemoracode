
#include"CUDA_mem.hpp"
#include"CUDA_defs.hpp"
#include"cctk.h"
#include<stdio.h>

extern size_t chemora_cg_overrun_amt;

void * CaCUDA_AllocPtr(size_t dataSize){
  DECLARE_CCTK_PARAMETERS
  assert(dataSize);
  void * ptr;
  CUDA_SAFE_CALL(cudaMalloc(&ptr, dataSize + chemora_cg_overrun_amt));
  CUDA_SAFE_CALL( cudaMemset( ptr, 0x0, dataSize) );
  if(veryverbose)
    fprintf(stderr, "Allocating GPU ptr.: %p\n", ptr);
  return ptr;
}

void CaCUDA_FreePtr(void ** ptr){
  assert(ptr);
  assert(cudaFree(*ptr) == cudaSuccess);
  *ptr = 0;
}

int CaCUDA_RegisterTl(CCTK_ARGUMENTS, int vi, int tl){
   return CaKernel_RegisterTl(CCTK_PASS_CTOC, vi, tl, cuda);  
}

void * CaCUDA_GetVarI(CCTK_ARGUMENTS, int vi, int tl){
  return CaKernel_GetVarI((void *)cctkGH, vi, tl);//, cuda);
}

int CaCUDA_RegisterMem(CCTK_ARGUMENTS, int vi, int tl){
  return CaKernel_RegisterMem(CCTK_PASS_CTOC, vi, tl);
//  , cuda);
}

int CaCUDA_UnRegisterMem(CCTK_ARGUMENTS, int vi){
  return CaKernel_UnRegisterMem(CCTK_PASS_CTOC, vi);//, cuda);
}

