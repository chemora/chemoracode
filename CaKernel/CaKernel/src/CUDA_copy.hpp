#pragma once


#if HAVE_CUDA == 1
#include"Common_mem.hpp"
#include"Common_copy.hpp"
#include"CUDA_mem.hpp"
#include"CUDA_copy.hpp"

int CaCUDA_CopyToDev(void * from, void * to, size_t datasize);

int CaCUDA_CopyFromDev(void * from, void * to, size_t datasize);

int CaCUDA_CopyInDev(void * from, void * to, size_t datasize);

int CaCUDA_CopyToDev_Rect(void * from, void * to, size_t pitch_src[2], size_t pitch_dst[2], 
    size_t offset_src[3], size_t offset_dst[3], size_t elements[3]);

int CaCUDA_CopyFromDev_Rect(void * from, void * to, size_t pitch_src[2], size_t pitch_dst[2], 
    size_t offset_src[3], size_t offset_dst[3], size_t elements[3]);


int CaCUDA_CopyToDev(const cGH *cctkGH, int vi, int tl);

int CaCUDA_CopyFromDev(const cGH *cctkGH, int vi, int tl);

int CaCUDA_CopyToDev_Boundary( const cGH *cctkGH, int vi, int tl);

int CaCUDA_CopyFromDev_Boundary( const cGH *cctkGH, int vi, int tl);

#endif
