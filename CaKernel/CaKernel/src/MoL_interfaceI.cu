
#include "MoL_interface.hpp"
#include "Common_mem.hpp"
#include "Common_copy.hpp"
#include <stdio.h>
#include <cmath>
#include <limits>
#include <cfloat>
#include <iostream>

bool is_it_nan(double c) {
  using namespace std;
  return isnan(c);
}
extern "C"
CCTK_INT
CaKernel_LinearCombination(void const*const cctkGH_,
                           CCTK_INT   const vari,
                           CCTK_INT   const rl,
                           CCTK_INT   const tl,
                           CCTK_REAL  const scale,
                           CCTK_INT   const srcsi[],
                           CCTK_INT   const tlsi[],
                           CCTK_REAL  const facts[],
                           CCTK_INT   const nsrcs)
{
  // Forward call to aliased function, if it is defined
  // Obtain pointer to variable data
  cGH * cctkGH = (cGH *) cctkGH_;
  DECLARE_CCTK_PARAMETERS;

  if(verbose) printf("Applying linear combination: vari=%d, rl=%d, tl=%d ",vari,rl,tl);

  assert(nsrcs <= 3);
  assert(CCTK_VARIABLE_REAL == CCTK_VarTypeI(vari));
  CCTK_REAL * var =   (CCTK_REAL *) CaKernel_SafeGetVarI_rl(cctkGH, vari, rl, tl);
  assert(var); 

  if(verbose) {
    printf("%s@%d:%d <- ", CCTK_VarName(vari), rl, tl);
    if(scale) printf("%s*%f ", CCTK_VarName(vari), scale);
    else if (!nsrcs) printf("%g ", scale);
  }
  Accelerator_RequireInvalidData(cctkGH, &vari, &rl, &tl, 1, 1);

  CCTK_REAL * srcs[3] = {0, 0, 0};
  for(int i = 0; i < nsrcs; i++){
    assert(CCTK_VARIABLE_REAL == CCTK_VarTypeI(srcsi[i]));
    srcs[i] = (CCTK_REAL *) CaKernel_SafeGetVarI_rl(cctkGH, srcsi[i], rl, tlsi[i]);
    if(verbose) printf("+ %s@%d:%d*%f i=%d ", CCTK_VarName(srcsi[i]), rl, tlsi[i], facts[i], i);
  }
  const int dim2 = CCTK_GroupDimFromVarI(vari);
  int lsh[dim2];
  int const ierr = CCTK_GrouplshVI(cctkGH, dim2, lsh, vari);
  assert(!ierr);
  int rlsi[nsrcs];
  for(int i=0;i<nsrcs;i++)
    rlsi[i] = rl;
  Accelerator_RequireValidData(cctkGH,srcsi,rlsi,tlsi,nsrcs,1);

  int npoints = 1;
  for (int d=0; d<dim2; ++d) {
    npoints *= lsh[d];
  }
  
  if(verbose) printf("points: %d\n", npoints);

  switch(nsrcs){
    case 0:
      if(scale == 0) OpSetVar(var, 0, npoints);
      else assert(0); //Not implemented in former interface. TODO, to be implemented
      break;
    case 1: 
      if(scale == 0) OpUpdateVar     (var, srcs[0], facts[0], npoints);
      else           OpUpdateScaleVar(var, scale, srcs[0], facts[0], npoints);
      break;
    case 2: 
      if(scale == 0) OpUpdateVar2     (var, srcs[0], facts[0], srcs[1], facts[1], npoints);
      else           OpUpdateScaleVar2(var, scale, srcs[0], facts[0], srcs[1], facts[1], npoints);
      break;
    case 3: 
      if(scale == 0) OpUpdateVar3     (var, srcs[0], facts[0], srcs[1], facts[1], srcs[2], facts[2], npoints);
      else           OpUpdateScaleVar3(var, scale, srcs[0], facts[0], srcs[1], facts[1], srcs[2], facts[2], npoints);
      break;
    default :
      assert(0); //Havn't implemented that case....
  }
  
  assert(cudaDeviceSynchronize() == cudaSuccess);

  Accelerator_NotifyDataModified(cctkGH, &vari, &rl, &tl, 1, 1);

  Accelerator_RequireValidData(cctkGH,&vari, &rl, &tl, 1, 0);

  if(veryverbose) {
    Accelerator_RequireValidData(cctkGH,srcsi,rlsi,tlsi,nsrcs,0);
    int rlevel = GetRefinementLevel(cctkGH);
    assert(rl == rlevel);

    CCTK_REAL * var2 = (CCTK_REAL *) CCTK_VarDataPtrI(cctkGH, tl, vari);

    printf("Out dest (of npts: %d) for %s:tl=%d:rl=%d : ", npoints,CCTK_VarName(vari),tl,rl);
    for(int i = 0; i < lsh[0]; i++){
      if(!(i%10)) printf("\n\t");
      printf("%.5e\t", var2[CCTK_GFINDEX3D(cctkGH, i, lsh[1] / 2, lsh[2]/2)]);
    }
    int inan = 0, nnan = 0;
    for(int k=0;k<lsh[2];k++) {
      for(int j=0;j<lsh[1];j++) {
        for(int i=0;i<lsh[0];i++) {
          int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
          if(is_it_nan(var2[cc]))
            inan++;
          else
            nnan++;
        }
      }
    }
    printf(" inan=%d nnan=%d\n",inan,nnan);

    for(int si = 0; si < nsrcs; si++){
      printf("-->debug:si=%d\n",si);
      CCTK_REAL * var2 = (CCTK_REAL *) CCTK_VarDataPtrI(cctkGH, tlsi[si], srcsi[si]);
      printf("Out srcs[%d] for %s:tl=%d:rl=%d : ", si, CCTK_VarName(srcsi[si]),tlsi[si],rl);
      for(int i = 0; i < lsh[0]; i++){
        if(!(i%10)) printf("\n\t");
        printf("%.5e\t", var2[CCTK_GFINDEX3D(cctkGH, i, lsh[1] / 2, lsh[2]/2)]);
      } 
      printf("\n");
    }
  }

  // Done
  return 0;
}
