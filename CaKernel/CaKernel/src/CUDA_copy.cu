
#include<cuda_runtime.h>
#include"CUDA_copy.hpp"

#include<stdio.h>

int CaCUDA_CopyToDev(void * from, void * to, size_t datasize){
  assert(from);
  assert(to);
  assert(datasize);
  //printf("->device:%d", datasize);
  cudaError_t stat = cudaMemcpy(to, from, datasize, cudaMemcpyHostToDevice);  
  assert(stat == cudaSuccess);  
  return 0;
}

int CaCUDA_CopyFromDev(void * from, void * to, size_t datasize){
  assert(from);
  assert(to);
  assert(datasize);
  //printf("->host:%d", datasize);
  cudaError_t stat = cudaMemcpy(to, from, datasize, cudaMemcpyDeviceToHost);
  assert(stat == cudaSuccess);
  return 0;
}

int CaCUDA_CopyInDev(void * from, void * to, size_t datasize){
  assert(from); assert(to); assert(datasize);

  cudaError_t stat = cudaMemcpy(to, from, datasize, cudaMemcpyDeviceToDevice); 
  assert(stat == cudaSuccess);
  return 0;
}

int CaCUDA_CopyToDev_Rect(void * from, void * to, size_t pitch_src[2], size_t pitch_dst[2], 
    size_t offset_src[3], size_t offset_dst[3], size_t elements[3])
{
  DECLARE_CCTK_PARAMETERS;
  assert(from && to);
  assert(elements[0] > 0 && elements[1] > 0 && elements[2] > 0);
  cudaPitchedPtr 
    srcPtr = make_cudaPitchedPtr(from, pitch_src[0], pitch_src[0], pitch_src[1] / pitch_src[0]),
    dstPtr = make_cudaPitchedPtr(to, pitch_dst[0], pitch_dst[0], pitch_dst[1] / pitch_dst[0]);

  cudaPos srcPos = make_cudaPos(offset_src[0], offset_src[1], offset_src[2]);
  cudaPos dstPos = make_cudaPos(offset_dst[0], offset_dst[1], offset_dst[2]);
  cudaExtent cudaExt= make_cudaExtent(elements[0], elements[1], elements[2]);
  cudaMemcpy3DParms params = {0};
  params.srcPtr = srcPtr; params.dstPtr = dstPtr;
  params.dstPos = dstPos; params.srcPos = srcPos;
  params.kind   = cudaMemcpyHostToDevice;
  params.extent = cudaExt;
  if(verbose )
  {
    printf("Copying To Dev: from: %ld, to: %ld, pitch_src (%ld, %ld), pitch_dst (%ld, %ld),"
           " offset_src (%ld, %ld, %ld), offset_dst (%ld, %ld, %ld), elements (%ld, %ld, %ld)\n",
           long(from), long(to), pitch_src[0], pitch_src[1], pitch_dst[0], pitch_dst[1], 
           offset_src[0], offset_src[1], offset_src[2], offset_dst[0], offset_dst[1], offset_dst[2],
           elements[0], elements[1], elements[2]);
  }
  cudaError_t stat = cudaMemcpy3D(&params);
  assert(stat == cudaSuccess);
  return 0;
}

#define CUDA_SAFE_CALL( call )                                             \
  {                                                                        \
    cudaError err = call;                                                  \
    if(err != cudaSuccess)                                                 \
    {                                                                      \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,   \
            "CUDA error: %s", cudaGetErrorString( err));                   \
      exit(EXIT_FAILURE);                                                  \
    }                                                                      \
  }

int CaCUDA_CopyFromDev_Rect(void * from, void * to, size_t pitch_src[2], size_t pitch_dst[2], 
    size_t offset_src[3], size_t offset_dst[3], size_t elements[3])
{
  DECLARE_CCTK_PARAMETERS;
  assert(from && to);
  assert(elements[0] > 0 && elements[1] > 0 && elements[2] > 0);
  cudaPitchedPtr 
    srcPtr = make_cudaPitchedPtr(from, pitch_src[0], pitch_src[0], pitch_src[1] / pitch_src[0]),
    dstPtr = make_cudaPitchedPtr(to, pitch_dst[0], pitch_dst[0], pitch_dst[1] / pitch_dst[0]);

  cudaPos srcPos = make_cudaPos(offset_src[0], offset_src[1], offset_src[2]);
  cudaPos dstPos = make_cudaPos(offset_dst[0], offset_dst[1], offset_dst[2]);
  cudaExtent cudaExt= make_cudaExtent(elements[0], elements[1], elements[2]);
  cudaMemcpy3DParms params = {0};
  params.srcPtr = srcPtr; params.dstPtr = dstPtr;
  params.dstPos = dstPos; params.srcPos = srcPos;
  params.kind   = cudaMemcpyDeviceToHost;
  params.extent = cudaExt;
  if(verbose )
  {
    printf("Copying From Dev: from: %ld, to: %ld, pitch_src (%ld, %ld), pitch_dst (%ld, %ld),"
           " offset_src (%ld, %ld, %ld), offset_dst (%ld, %ld, %ld), elements (%ld, %ld, %ld)\n",
           long(from), long(to), pitch_src[0], pitch_src[1], pitch_dst[0], pitch_dst[1], 
           offset_src[0], offset_src[1], offset_src[2], offset_dst[0], offset_dst[1], offset_dst[2],
           elements[0], elements[1], elements[2]);
  }
  CUDA_SAFE_CALL(cudaMemcpy3D(&params));
  return 0;
}

/* int CaCUDA_CopyToDev(const cGH *cctkGH, int vi, int tl) */
/* { */
/*   return CaKernel_CopyToDev(cctkGH, vi, tl, cuda); */
/* }  */

/* int CaCUDA_CopyFromDev(const cGH *cctkGH, int vi, int tl) */
/* { */
/*   return CaKernel_CopyFromDev(cctkGH, vi, tl, cuda); */
/* } */

/* int CaCUDA_CopyToDev_Boundary( const cGH *cctkGH, int vi, int tl) */
/* { */
/*   return CaKernel_CopyToDev_Boundary(cctkGH, vi, tl, cuda); */
/* } */

/* int CaCUDA_CopyFromDev_Boundary( const cGH *cctkGH, int vi, int tl) */
/* { */
/*   return CaKernel_CopyFromDev_Boundary(cctkGH, vi, tl, cuda); */
/* } */

