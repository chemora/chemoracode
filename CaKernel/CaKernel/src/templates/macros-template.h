%t%[
%set_macro(num_vars, %[ %var_loop_ifno(true) ]%)

%set_macro(num_pars, %[ %par_loop_ifno(true) ]%)

%set_macro(num_vars_sepinout, %[ (%var_loop_ifno(true) + %var_loop_ifno(%[ '%varAttr("%vname", intent)'=='separateinout' ]%)) ]%)

%set_macro(var_ispointer, %[ %eval(%[ '%gvar("%vname", type)'=='Array' || %gvar("%vname", isVector) ]%) ]%)

%set_macro(var_numelements, %[  
%ifthen(%[ %gvar("%vname", isVector) ]%,%[ ( %gvar("%vname", vectorExpression) ) * ]%) %ifthen(%[ '%gvar("%vname", type)'=='array' ]%, %[ %set(tmpmacroi, 0) %while_loop(%[ %var(tmpmacroi) < %gvar("%vname",dim) ]%)%[( %gvar("%vname", "size:%var(tmpmacroi)") ) * %set(tmpmacroi, '%eval("%var(tmpmacroi) + 1")') ]% ]%) 1 ]% )

%set_macro(par_numelements, %[ %ifelse(%[ %gpar("%vname", isVector) ]%,%[ ( %gpar("%vname", vectorExpression) ) ]%, 1)]%)

%set_macro(run1_global_indexing, %[  
#define CAKERNEL_GFINDEX3D_%{name}(ptr, i, j, k)                       \
 ptr[(i + gi) + params.cagh_ni * ((j + gj) + params.cagh_nj * (k + gk))]

#define CAKERNEL_GFINDEX3DV_%{name}(ptr, i, j, k, vind)        \
 CAKERNEL_GFINDEX3D_%{name}((&ptr[(vind) * vstride]), i, j, k)

#define I3DV CAKERNEL_GFINDEX3DV_%{name}
#define I3D CAKERNEL_GFINDEX3D_%{name}
]%)

%set_macro(run1_3dblock_indexing, %[  
/** Element  */
#define CAKERNEL_GFINDEX3D_%{name}_l(ptr, i, j, k)             \
  (ptr##_sh[k + lk][j + lj][i + li])

#define CAKERNEL_GFINDEX3DV_%{name}_l(ptr, i, j, k, vind)      \
  (ptr##_sh[vind][k + lk][j + lj][i + li])

#define I3D_l CAKERNEL_GFINDEX3D_%{name}_l
#define I3DV_l CAKERNEL_GFINDEX3DV_%{name}_l
]%)

%set_macro(run1_3dblock_indexing_spec_cached, %[  
/** Element  */
#define CAKERNEL_GFINDEX3D_%{name}_%{vname}_l(ptr, i, j, k)             \
  (ptr##_sh[k + lk][j + lj][i + li])

#define CAKERNEL_GFINDEX3DV_%{name}_%{vname}_l(ptr, i, j, k, vind)      \
  (ptr##_sh[vind][k + lk][j + lj][i + li])

//#define I3D_l(ptr, i, j, k)  CAKERNEL_GFINDEX3D_%{name}_ ## ptr ## _l (ptr, i, j, k)
//#define I3DV_l(ptr, i, j, k, vind) CAKERNEL_GFINDEX3DV_%{name}_ ## ptr ## _l (ptr, i, j, k, vind)
]%)

%set_macro(run1_3dblock_indexing_spec, %[  
/** Element  */
#define CAKERNEL_GFINDEX3D_%{name}_%{vname}_l I3D
#define CAKERNEL_GFINDEX3DV_%{name}_%{vname}_l I3DV

//#define I3D_l(ptr, i, j, k) CAKERNEL_GFINDEX3D_%{name}_ ## ptr ## _l (ptr, i, j, k)
//#define I3DV_l CAKERNEL_GFINDEX3DV_%{name}_ ## ptr ## _l (ptr, i, j, k, vind)
]%)

%set_macro(run1_3dcommon_indexing_spec, %[  
#define I3D_l(ptr, i, j, k) CAKERNEL_GFINDEX3D_%{name}_ ## ptr ## _l (ptr, i, j, k)
#define I3DV_l(ptr, i, j, k, vind) CAKERNEL_GFINDEX3DV_%{name}_ ## ptr ## _l (ptr, i, j, k, vind)
]%)

%set_macro(run1_defvars, %[                                       
%var_loop_if(%[ '%gvar("%vname", type)'!='gf' ]%,'#define %vname ((%gvar("%vname", CCTK_TYPE) *) &vars_ptr[%{vname}_offset])\n')
]%)

%set_macro(run1_undefvars, %[                                       
  %var_loop_if(%[ '%gvar("%vname", type)'!='gf' ]%,'#undef %vname \n')
]%)

%set_macro(run1_defpars, %[                                       
%par_loop_if(%[ %gpar("%vname", isVector) ]%,'#define %vname (   (%gpar("%vname", CCTK_TYPE) *) &pars_ptr[%{vname}_offset])\n')
%par_loop_if(%[!%gpar("%vname", isVector) ]%,'#define %vname (+*((%gpar("%vname", CCTK_TYPE) *) &pars_ptr[%{vname}_offset]))\n')
]%)

%set_macro(run1_undefpars, %[                                       
  %par_loop('#undef %vname \n')
]%)

%set_macro(append_p,%[%set(tmpi,0)%while_loop(%[%var(tmpi)<%var(arg1)]%,%[%t("%inc(tmpi)")_p]%)]%)

%set_macro(compile_in_init,%[ 
  static int ignoreme = %{thornname}::push_back_for_compilation(CAKERNEL_Launch_%{name}); ]%)

%set_macro(schedule_in_init,%[ 
int CaKernel_RegisterThorn(std::string name);
namespace %{thornname} {
  #include<string>
  int %{path_parent_lower}_ignoreme  = 
    %{thornname}::push_back(%{thornname}_%{path_parent_lower}_AllocDevMem) +
    CaKernel_RegisterThorn(CCTK_THORNSTRING);
    // + printf("Initializing: %s\n", "%{thornname}::%{path_parent_lower}_ignoreme"); 
} ]% )


]%
