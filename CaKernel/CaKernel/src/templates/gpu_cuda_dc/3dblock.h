/*@@   -*- c++ -*-
 * @file    %{file}
 * @date    
 * @author  David Koppelman
 * @desc

 * @enddesc
 * @version  $Header$
 *
 @@*/

#ifndef CAKERNEL_DC_3DBLOCK_H
#define CAKERNEL_DC_3DBLOCK_H

enum cak__AX { cak__AX_0 = 0, cak__AX_x = 1, cak__AX_y = 2, cak__AX_xy = 3,
       cak__AX_z = 4, cak__AX_xz = 5, cak__AX_yz = 6, cak__AX_xyz = 7 };

#define max(a,b) ((a)<(b)?b:a)
#define min(a,b) ((a)>(b)?b:a)

#ifdef __CUDACC__

struct Info
{
  CaCUDA_Kernel_Launch_Parameters params;
  int lsh_size;
  bool sc_used;
  int shared_cache_size;
  bool sc_fetch_participant;
  bool sc_fetch_participant_frac_round;

  // If true, during shared memory (iterate cache) operations, the
  // thread copies 2 elements per iteration.
  bool sc_extra_x_thd;
  int sc_fetch_rounds;
  int sc_fetch_whole_rounds;
  int sc_rows_per_block;

  // For shared memory (iterate cache) operations, the .._start variables
  // specify the starting index that the thread will start operating on.
  int sc_off_g_start;
  int sc_off_s_start;
  int sc_z_stride;
  int sc_y_stride;
  // Note: e refers to dimension along which iteration occurs.
  //         For example, e refers to y dim if tile_yy > 1.
  //       f refers to the dimension orthogonal to e and x.
  //         For example, f refers to z if tile_yy > 1 (z is orthog to x and y).
  bool iter_y;                  // Iterate along y dimension.
  int sc_e_stride;
  int sc_f_stride;
  int stncl_en, stncl_ep;
  int stncl_fn, stncl_fp;
  int e_length;
  int tile_e;
  int tile_f;
  int tilegs_origin_idx;
  int blockgs_origin_idx;
  int gl_z_stride;
  int gl_y_stride;
  int gl_e_stride;
  int gl_f_stride;
  int tt;                       // Dimension e loop index variable.
  int tid;
  int li;
  int lj;
  int lk;
};


 /// Declare Cache for Variable v of Type t
//
// v__sh:         Shared array for variable v.
// cak__.sc_used: Indicates that shared memory is being used for at
//                  least one variable.
// v__s_me:       Pointer to zero-offset element for this thread.
//                  That is, a pointer to the element that I3D(v,0,0,0)
//                  would return.  This variable has been removed, but
//                  is still referred to in the bitrotted I3DV macros.
//
#define DECLARE_CACHE(t,v)                                                    \
 __shared__ UN t v##__sh[cak__shared_cache_size];                             \
 const bool v##__is_shared = true;                                            \
 cak__.sc_used = true;

#define DECLARE_NO_CACHE(t,v)                                                 \
 const bool v##__is_shared = false;                                           \
 t* const UN v##__sh = NULL;

// Global memory index relative to thread's element.  This is
// used as an index into v (a pointer to global memory).
//
#define I3D_idx_device(di,dj,dk) \
( cak__block_elt_idx + (di) + (dj) * params.cagh_ni + (dk) * cak__gl_z_stride )

// Access global memory via texture cache, but indicate data should
// not be cached.  CC 3.5 only, even so this may not be useful.
#define I3D_tnc(var,di,dj,dk) ptx_load_tnc(&var[ I3D_idx_device(di,dj,dk) ])

// Access global memory via texture cache so that data will be
// cached. Data is expected to be read-only. CC 3.5 only.
#define I3D_tro(var,di,dj,dk) ptx_load_tro(&var[ I3D_idx_device(di,dj,dk) ])

// Access global memory normally, but indicate that it should not
// be cached.  Probably not useful for Kepler devices.
#define I3D_gnc(var,di,dj,dk) ptx_load_gnc(&var[ I3D_idx_device(di,dj,dk) ])

// Perform an offset-read for variable (grid function) var at offsets
// di, dj, dk.  Access may be performed directly, via shared memory,
// or via the read-only cache.
//
#define I3D(var,di,dj,dk)  I3D_shared(var,di,dj,dk)

#define I3D_l(var,di,dj,dk)  I3D_shared_l(var,di,dj,dk)


#if defined( __CUDA_ARCH__ ) && __CUDA_ARCH__ >= 350
#define USING_RO_CACHE 1
#else
#define USING_RO_CACHE 0
#endif

#ifdef STENCIL_CHECK
// If code being prepared for a quick and dirty stencil analysis,
// use a dummy function name for I3D. The resulting code will
// never be compiled.
#undef I3D
#define I3D cak__stencil_check_i3d
#endif

// Global access targeting (read-only) texture cache.
#if USING_RO_CACHE
__device__ inline double ptx_load_tro(const double *addr)
{
  double rv;
  asm("ld.global.nc.f64 %0, [%1];" : "=d"(rv) : "l"(addr) );
  return rv;
}
__device__ inline float ptx_load_tro(const float *addr)
{
  float rv;
  asm("ld.global.nc.f32 %0, [%1];" : "=f"(rv) : "l"(addr) );
  return rv;
}
#endif

// Fallback read-only access function. This accesses global
// data normally, not through the read-only cache.
//
template <typename T>
__device__ inline T ptx_load_tro(const T *addr){ return *addr; }


// Global access targeting (read-only) texture cache, but value not cached.
__device__ inline double ptx_load_tnc(const double *addr)
{
#if USING_RO_CACHE
  double rv;
  asm("ld.global.cg.nc.f64 %0, [%1];" : "=d"(rv) : "l"(addr) );
  return rv;
#else
  return *addr;
#endif
}

// Global access, but value not cached.
__device__ inline double ptx_load_gnc(const double *addr)
{
#if USING_RO_CACHE
  double rv;
  asm("ld.global.cg.f64 %0, [%1];" : "=d"(rv) : "l"(addr) );
  return rv;
#else
  return *addr;
#endif
}

// Class used for variable (grid function) global memory
// pointers. This class enables the same type of variables to be on
// the left-hand side of an expression and yet still access the
// read-only cache (which is ultimately done through inline assembler,
// which cannot form an lvalue).
template <typename T>
class Cak__Var_Wrapper {
public:
  __device__ Cak__Var_Wrapper(bool roc_load_p):roc_load(roc_load_p){}
  T val;
  const bool roc_load;  // Direct load to read-only cache.
  __device__ T& idx(T *addr)
  {
    if ( !roc_load ) return *addr;
    val = ptx_load_tro(addr);
    return val;
  }
};

#define DECLARE_COMMON(t,v) \
  Cak__Var_Wrapper<t> cak__var_wrapped_##v(cak__var_read_only_cache_##v);

#define DECLARE_VAR_PLACEMENT(var,roc_default) \
 const bool cak__var_read_only_cache_##var = roc_default;


#define I3D_device(var,di,dj,dk) I3D_device_l(var,di,dj,dk)
#define I3D_device_l(var,di,dj,dk) \
      cak__var_wrapped_##var.idx(&var[I3D_idx_device(di,dj,dk)])

#define I3D_shared(var,di,dj,dk)                                              \
( var##__is_shared                                                            \
  ? var##__sh[I3D_idx_shared(cak__,di,dj,dk)]                                 \
  : I3D_device(var,di,dj,dk) )

__device__ static int 
I3D_idx_shared(Info& cak__, const int di, const int dj, const int dk)
{
  const int e = cak__.stncl_en
    + ( cak__.iter_y ? ( cak__.lj + dj ) : ( cak__.lk + dk ) );
  const int f = cak__.stncl_fn
    + ( !(cak__.iter_y) ? ( cak__.lj + dj ) : ( cak__.lk + dk ) );

  const int e_s = ( e + cak__.tt * cak__.tile_e ) % cak__.e_length;
  const int x = stncl_xn + cak__.li + di;
  const int sm_addr = x + e_s * cak__.sc_e_stride + f * cak__.sc_f_stride;
  return sm_addr;
}

#define I3DV_device(var,di,dj,dk,dv)                                          \
  var[ I3D_idx_device(di,dj,dk) + (dv) * vstride ]

#define I3DV(var,di,dj,dk,dv)  I3DV_shared(var,di,dj,dk,dv)
#define I3DV_l(var,di,dj,dk,dv) I3DV_shared(var,di,dj,dk,dv)

#define I3DV_shared(var,di,dj,dk,dv)                                          \
( var##__is_shared                                                            \
  ? var##__s_me[ I3D_idx_me_shared(di,dj,dk) + vstride_s ] :                  \
  I3DV_device(var,di,dj,dk,dv))


#define CAK_Fetch_to_Cache(v,round)                                           \
for ( int e = 0; e < cak__.tile_e + cak__.stncl_en + cak__.stncl_ep; e++ )    \
  {                                                                           \
    const int global_e_origin =                                               \
      cak__.tilegs_origin_idx + e * cak__.gl_e_stride;                        \
    CAK_Fetch_to_Cache_egr(v,e,global_e_origin,round);                        \
  }

#define CAK_Fetch_to_Cache_egr(v,e,global_origin,round)                       \
 if ( v##__is_shared )                                                        \
  { cak_fetch_to_cache_round(cak__,v##__sh,v,e,global_origin,round); }


__device__ static void
cak_cs_fetch_setup(Info& cak__)
{
  const bool iter_y = cak__.params.tile_yy > 1;
  cak__.iter_y = iter_y;
  cak__.tile_e =  iter_y ? cak__.params.tile_y : cak__.params.tile_z;
  cak__.tile_f = !iter_y ? cak__.params.tile_y : cak__.params.tile_z;
  cak__.stncl_en =  iter_y ? stncl_yn : stncl_zn;
  cak__.stncl_ep =  iter_y ? stncl_yp : stncl_zp;
  cak__.stncl_fn = !iter_y ? stncl_yn : stncl_zn;
  cak__.stncl_fp = !iter_y ? stncl_yp : stncl_zp;
  cak__.sc_e_stride =  iter_y ? cak__.sc_y_stride : cak__.sc_z_stride;
  cak__.sc_f_stride = !iter_y ? cak__.sc_y_stride : cak__.sc_z_stride;
  cak__.gl_e_stride =  iter_y ? cak__.gl_y_stride : cak__.gl_z_stride;
  cak__.gl_f_stride = !iter_y ? cak__.gl_y_stride : cak__.gl_z_stride;
  cak__.e_length = cak__.tile_e + cak__.stncl_en + cak__.stncl_ep;
  const int f_length = cak__.tile_f + cak__.stncl_fn + cak__.stncl_fp;

  const int rows_per_block_max =
    max(1,cak__.params.block_size / cak__.sc_y_stride);
  const int rounds = ( f_length + rows_per_block_max -1 ) / rows_per_block_max;
  const int rows_per_block = ( f_length + rounds - 1) / rounds;
  const int blocks_per_row =
    ( cak__.sc_y_stride + cak__.params.block_size - 1 )
    / cak__.params.block_size;
  const bool frac_round = rows_per_block * rounds > f_length;
  const int off_g_r = cak__.tid / cak__.sc_y_stride;
  const int off_g_c = cak__.tid % cak__.sc_y_stride;

  cak__.sc_rows_per_block = rows_per_block;
  cak__.sc_fetch_participant = off_g_r < rows_per_block;
  cak__.sc_fetch_rounds = rounds;
  cak__.sc_fetch_whole_rounds = rounds - frac_round;
  cak__.sc_off_g_start = off_g_c + off_g_r * cak__.gl_f_stride;
  cak__.sc_off_s_start = off_g_c + off_g_r * cak__.sc_f_stride;

  const bool sc_extra_x_tile = blocks_per_row > 1;
  cak__.sc_extra_x_thd =
    sc_extra_x_tile && cak__.tid + cak__.params.block_size < cak__.sc_y_stride;

  const int row_last_round =
    cak__.sc_fetch_whole_rounds * rows_per_block + off_g_r;

  cak__.sc_fetch_participant_frac_round = row_last_round < f_length;
}


template <typename T1, typename T2> __device__ static void
cak_fetch_to_cache_round
(Info& cak__, T1 *shared, T2 *global, int e, int tilegs_origin, int round)
{
  const int eoffset = e * cak__.sc_e_stride;
  const int off_g =
    cak__.sc_off_g_start + round * cak__.sc_rows_per_block * cak__.gl_f_stride;
  const int off_s =
    cak__.sc_off_s_start + round * cak__.sc_rows_per_block * cak__.sc_f_stride;
  const int idx_g = tilegs_origin + off_g;
  const int idx_s = eoffset + off_s;
  const int idx_g_clamped = min(idx_g,cak__.lsh_size-1);

  shared[ idx_s ] = global[ idx_g_clamped ];
  if ( cak__.sc_extra_x_thd )
    shared[ idx_s + cak__.params.block_size ]
      = global[ idx_g_clamped + cak__.params.block_size ];
}


#define CAK_Front_Tile_to_Cache(v,round)                                      \
 for ( int e = cak__.stncl_en + cak__.stncl_ep; e < cak__.e_length; e++ )     \
   {                                                                          \
      int e_s = ( e + cak__tt * cak__.tile_e ) % cak__.e_length;              \
      CAK_Fetch_to_Cache_egr                                                  \
        (v, e_s, cak__.blockgs_origin_idx + e * cak__.gl_e_stride, round);    \
   }


#define AX_AX(ax)                                                             \
        ( (ax) & cak__AX_x ? cak__AX_x :                                      \
          (ax) & cak__AX_y ? cak__AX_y : (ax) & cak__AX_z ? cak__AX_z : 0 )

#define AX_0(ax) AX_AX(ax)
#define AX_1(ax) AX_AX((ax)-AX_0(ax))
#define AX_2(ax) AX_AX((ax)-AX_0(ax)-AX_1(ax))

#define AX_IDX(ax) \
   ( (ax) & cak__AX_x ? 1 : (ax) & cak__AX_y ? 2 : (ax) & cak__AX_z ? 3 : 0 )
#define AX_IDX_0(ax) AX_IDX(AX_0(ax))
#define AX_IDX_1(ax) AX_IDX(AX_1(ax))
#define AX_IDX_2(ax) AX_IDX(AX_2(ax))

#define AX_OFFS(ax,di,dj,dk) \
   ( (ax) & cak__AX_x ? di : (ax) & cak__AX_y ? dj : (ax) & cak__AX_z ? dk : 0 )

#define AX_OFFS_0(ax,di,dj,dk) AX_OFFS(AX_0(ax),di,dj,dk)
#define AX_OFFS_1(ax,di,dj,dk) AX_OFFS(AX_1(ax),di,dj,dk)
#define AX_OFFS_2(ax,di,dj,dk) AX_OFFS(AX_2(ax),di,dj,dk)

#define IVD_idx_device(ax,di,dj,dk)                                           \
  (   cak__g_idx_vec[AX_IDX_0(ax)] + AX_OFFS_0(ax,di,dj,dk)                   \
    + cak__g_str_vec[AX_IDX_0(ax)] *                                          \
      ( cak__g_idx_vec[AX_IDX_1(ax)] + AX_OFFS_1(ax,di,dj,dk) )               \
    + cak__g_str_vec[AX_IDX_0(ax)] * cak__g_str_vec[AX_IDX_1(ax)] *           \
      ( cak__g_idx_vec[AX_IDX_2(ax)] + AX_OFFS_2(ax,di,dj,dk) ) )


#define IVD(var,axes) var[ IVD_idx_device(axes,0,0,0) ]
#define ILD(var,axes,di,dj,dk) var[ IVD_idx_device(axes,di,dj,dk) ]

#endif

#endif
