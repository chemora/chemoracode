 /*@@
   @file      cacuda.3dall-template.cu
   @date      $Date:$
   @author    Marek Blazewicz, Steven R. Brandt, later David M. Koppelman
   @desc 
   Template for generation of block and stencil host code, including
   dynamic compilation.
   @enddesc 
   @version   $Revision:$
   @id        $Id:$
 @@*/

#include "CaKernel.h" 
#include <CaCUDALib_driver_support.h>
#include <string>
#include <iostream>
#include <sstream>
#include <assert.h>
using namespace::std;

%evaluate(macros)

#define exterior_xn %{exterior_xn}
#define exterior_xp %{exterior_xp}
#define exterior_yn %{exterior_yn}
#define exterior_yp %{exterior_yp}
#define exterior_zn %{exterior_zn}
#define exterior_zp %{exterior_zp}

class %{thornname}_GF_Offset_%{name} {
 public:
  %{thornname}_GF_Offset_%{name}(CaCUDALib_CUDA_Driver_Manager *dm):dm(dm) {
    base_addr = NULL;
    deltas = NULL;
    gf_device_addr = NULL;
    warned = false;
    use_gf_offsets = false;
    use_gf_offsets_decided = false;
    gf_names = NULL;
    visit_count = 0;
  }
 private:
  void update_device_addr(CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    %macro(run1_declare_gf_dev_addr);

    CCTK_REAL* const gfs[] = {
      %var_loop_if(%['%gvar("%vname",type)'=='GF']%,
                   %[(CCTK_REAL*) d_%{vname},
                     %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)", %[ %set(arg1,"%var(fi)") (CCTK_REAL*) d_%{vname}%macro(append_p),]% )]% ]% )
      NULL
    };
    numvars = sizeof(gfs) / sizeof(gfs[0]) - 1;

    if ( !gf_device_addr )
      {
        char* const gfn[] = {
          %var_loop_if(%['%gvar("%vname",type)'=='GF']%,
                       %[ strdup("%{vname}"),
                          %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)", %[ %set(arg1,"%var(fi)") strdup("%{vname}%macro(append_p)"),]% )]% ]% )
          NULL
        };
        gf_names = (char**) malloc( sizeof(gfn) );
        memcpy(gf_names,gfn,sizeof(gfn));
      }

    if ( !gf_device_addr )
      gf_device_addr = (CCTK_REAL**) malloc( sizeof(gfs) );
    memcpy(gf_device_addr,gfs,sizeof(gfs));
  }

  CaCUDALib_CUDA_Driver_Manager* const dm;

  int numvars;
  CCTK_REAL **gf_device_addr;
  char **gf_names;
  CCTK_REAL *base_addr;
  ptrdiff_t *deltas;
  bool use_gf_offsets;
  bool use_gf_offsets_decided;
  bool warned;
  int visit_count;

 public:
  bool turned_on(CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    if ( use_gf_offsets_decided && !use_gf_offsets ) return false;
    update_device_addr(CCTK_PASS_CTOC);
    if ( !use_gf_offsets_decided )
      {
        use_gf_offsets_decided = true;
        use_gf_offsets = numvars >= dm->param_gf_offset_var_min;
      }
    return use_gf_offsets;
  }

  int get_gf_count() { return numvars; }

  void setup_offsets()
  {
    if ( !deltas ) deltas = new ptrdiff_t[numvars];
    base_addr = gf_device_addr[0];

    for ( int i=0; i<numvars; i++ )
      deltas[i] = gf_device_addr[i] - base_addr;

    ostringstream rep;
    for ( int i=0; i<numvars; i++ )
        rep << "const ptrdiff_t cak__gf_base_offset_" << gf_names[i] << " = "
            << gf_device_addr[i] - base_addr << "; ";

    dm->module_vals_set(STR(REPL_KEY_CACTUS_ARGS), rep.str());
  }

  CCTK_REAL* get_base_and_verify()
  {
    if ( !use_gf_offsets ) return NULL;

#if 0
    if ( visit_count++ < 10 &&
         string("%{name}") == "ML_BSSN_RHS_DPDstandardNthalpha1" )
      {
        printf("For %{name} addresses are:");
        for ( int i=0; i<numvars; i++ )
          printf(" %s 0x%lx,",gf_names[i],gf_device_addr[i]);
        printf("\n");
      }
#endif

    int mismatch_count = 0;
    ostringstream mismatch_vars;
    for ( int i=0; i<numvars; i++ )
      if ( deltas[i] != gf_device_addr[i] - base_addr )
        mismatch_count++;
    if ( !warned && mismatch_count )
      {
        CCTK_VWarn
          ( dm->param_gf_offset_tolerant ? CCTK_WARN_ALERT : CCTK_WARN_ABORT,
           __LINE__,__FILE__, CCTK_THORNSTRING,
           "There are %d grid functions with variable offsets in %{name}.",
           mismatch_count);
        warned = true;
        return NULL;
      }
    return base_addr;
  }
};


extern "C"
void CAKERNEL_Launch_%{name}(CCTK_ARGUMENTS)
{
  /// ... following line is required because of using *.a archives for thorns,
  ///     the variables are not initializied at all, unless used somewhere...
  %{thornname}::%{path_parent_lower}_ignoreme+=1;

    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;

    CHECK_THORN_INIT

    const bool compile_only =
      *(int *)CCTK_VarDataPtr(cctkGH, 0, "CaKernel::compile_only");

    const char* const kernel_name = "%{name}";
    const char* const kernel_function_name = "CAKERNEL_%{name}";
    const char* const thorn_name = "%{thornname}";

    int cak__vi = 0;
    CaKernel_Kernel_Config kernel_config;
    int cak__wp[6] =
      { exterior_xn, exterior_xp, exterior_yn, exterior_yp, 
        exterior_zn, exterior_zp};
    memcpy
      (kernel_config.want_physbnd,cak__wp,sizeof(kernel_config.want_physbnd));
    kernel_config.sten_xn = stncl_xn;
    kernel_config.sten_xp = stncl_xp;
    kernel_config.sten_yn = stncl_yn;
    kernel_config.sten_yp = stncl_yp;
    kernel_config.sten_zn = stncl_zn;
    kernel_config.sten_zp = stncl_zp;
    kernel_config.kernel_name = kernel_name;
    kernel_config.kernel_function_name = kernel_function_name;
    kernel_config.thorn_name = thorn_name;

    CaCUDALib_driver_support_set_lc(CCTK_PASS_CTOC,kernel_config);

    %var_loop %[
      %ifelse("%gvar('%vname', isVector)",%[ 
      cak__vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname[0]");]%,%[
      cak__vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname");]%)
      if ( %tlevel == 0) {
        assert(cak__vi >= 0);
        if (CaKernel_VariableDisabled(cak__vi)) {
          CCTK_VInfo(CCTK_THORNSTRING,
                   "Skipping launch of kernel %{name} due to disabled variable %s",
                   CCTK_FullName(cak__vi));
          return;
        }
      }
    ]%

    int shared_var_count = 0;
    %var_loop_if(%[ ('%varAttr("%vname", intent)' == 'inout' || '%varAttr("%vname", intent)' == 'in') && '%gvar("%vname", type)'=='gf' ]%)
    %[
      %ifelse(%[ '%varAttr("%vname", cached)' == 'yes' ]%, 
              %[ shared_var_count++; ]%, %[  ]%) ]%

    %macro(run1_declare_gf_dev_addr);

    kernel_config.shared_var_count = shared_var_count;

    CaCUDALib_CUDA_Driver_Manager* const cak__dm =
      CaCUDALib_CUDA_driver_manager_get();

    const bool use_dynamic_compilation =
      dynamic_compilation && cak__dm->dynamic_compile_possible();

    if ( !use_dynamic_compilation && compile_only ) return;

    static %{thornname}_GF_Offset_%{name} *gf_offset_info = NULL;
    if ( !gf_offset_info ) gf_offset_info = new %{thornname}_GF_Offset_%{name}(cak__dm);
    const int gf_use_offsets = 
      use_dynamic_compilation &&
      gf_offset_info->turned_on(CCTK_PASS_CTOC);
    const int gf_count = gf_offset_info->get_gf_count();

    static CAK_Var_Tmpl_Info gf_stencil[] = {
      %var_loop_if(%['%gvar("%vname", type)'=='gf' ]%)%[
      { "%vname", CCTK_VarIndex("%gvar('%vname',impl)::%vname"),
        "%varAttr('%vname',intent)",
        "%gvar('%vname',CCTK_TYPE)",
        %ifelse(%[ '%varAttr("%vname", cached)' == 'yes' ]%, 
                %[ true ]%, %[ false ]% ),
        { %for_loop(i,0,6)%[ %stencil(%vname,%var(i)), ]% } } , ]%  
      { NULL, 0, NULL, NULL, false, { 0,0,0, 0,0,0 } }
    };

    kernel_config.var_tmpl_info = gf_stencil;
    kernel_config.var_tmpl_info_size = 
      sizeof(gf_stencil)/sizeof(gf_stencil[0]) - 1;

    kernel_config.gf_use_offsets = gf_use_offsets;
    kernel_config.gf_count = gf_offset_info->get_gf_count();

#ifdef CAK__EMBED_SOURCE

    // Include compressed kernel source code and stencil information.
    // The include file prepared at build time, before this file.
#include STR(CAK__EMBED_SOURCE)
    kernel_config.var_src_info = &cak__src_stencil_info[0];
    kernel_config.gf_stencil_src_info_size =
      sizeof(cak__src_stencil_info)/sizeof(cak__src_stencil_info[0]);
#else
    kernel_config.var_src_info = NULL;
    kernel_config.gf_stencil_src_info_size = 0;
#endif

    // Look for module (compiled collection of kernels) corresponding
    // to this file (after macro expansion).
    //
    const bool module_init_needed =
      use_dynamic_compilation
      && cak__dm->module_get(CCTK_PASS_CTOC, &kernel_config, STR(__FILE__));

    //
    // Possibly Compile and Load Kernel Code
    //

    if ( module_init_needed )
      {
        cak__dm->kernel_launch_params_init(&kernel_config,CCTK_PASS_CTOC);

        // If present, un-compress kernel source stored in included array.
#ifdef CAK__EMBED_SOURCE
        cak__dm->module_src_set
          (cak__src_uncomp_chars, cak__src_comp_chars,
           (char*)&cak__src_words[0]);
#endif

        %{thornname}_Set_Cactus_Params(cak__dm);
        // Replace offsets even if feature is turned off to avoid
        // an unsubstituted marker error message.
        gf_offset_info->setup_offsets();
      }

    CCTK_REAL *gf_base_address = 
      gf_offset_info->get_base_and_verify();

    // Running would raise a fatal error and avoid warnings from
    // other kernels.
    if ( gf_use_offsets && !gf_base_address ) return;


    //
    // Collect variables to pass as kernel arguments.
    //

    int cctk_iteration_cpy = cctk_iteration;
    CCTK_REAL cctk_time_cpy = cctk_time;

    void *args[] =
      {
        %var_loop_if(%['%gvar("%vname", type)'=='GF']%) %[
        &d_%{vname},
        %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)")
            %[ %set(arg1,"%var(fi)") &d_%{vname}%macro(append_p),]% ]% ]%
        %var_loop_if(%['%gvar("%vname", type)'!='GF']%) %[
        &d_%{vname},
        ]%
        &gf_base_address, &cctk_iteration_cpy, &cctk_time_cpy
      };

    {
      CHEMORA_AT_LAUNCH(thorn_name,kernel_name,kernel_function_name);
      if ( CHEMORA_SHOULD_INITIALIZE )
        {
          %par_loop(%[  CHEMORA_AT_LAUNCH_PARAM(%vname) ]% );

          %var_loop_if(%['%gvar("%vname", type)'=='GF']%) %[
            CHEMORA_AT_LAUNCH_GF_ARG("%{vname}",d_%{vname});
            %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)")
              %[ %set(arg1,"%var(fi)") 
                 CHEMORA_AT_LAUNCH_ARG("%{vname}%macro(append_p)");]% ]% ]%
          %var_loop_if(%['%gvar("%vname", type)'!='GF']%) %[
            CHEMORA_AT_LAUNCH_ARG("%{vname}",d_%{vname});]%

          CHEMORA_AT_LAUNCH_INIT_END;
        }
    }

    cak__dm->nan_scan(CCTK_PASS_CTOC, &kernel_config, args, 'i');

    if ( use_dynamic_compilation )
      {
        cak__dm->launch(args,CCTK_PASS_CTOC);
      }
    else
      {
        const int cagh_ni = cctk_lsh[0];
        const int cagh_nj = cctk_lsh[1];
        const int cagh_nk = cctk_lsh[2];

        const int tile_x = %{tile_x};
        const int tile_y = %{tile_y};
        const int tile_z = 1;
        const int tile_zz = %{tile_z};
        const int tile_yyy = tile_y;
        const int tile_zzz = tile_z * tile_zz;
        const int tile_thds = tile_x * tile_y * tile_z;

        const int x_length = kernel_config.i_length[0];
        const int y_length = kernel_config.i_length[1];
        const int z_length = kernel_config.i_length[2];
        const int cagh_blocky =  iDivUp(y_length, tile_yyy);
        const int grid_x = iDivUp( x_length, tile_x );
        const int grid_y = iDivUp( z_length, tile_zzz ) * cagh_blocky;

        dim3 dg, db;
        db.x = tile_thds; db.y = db.z = 1;
        dg.x = grid_x; dg.y = grid_y; dg.z = 1;

        Cactus_Parameters cp;

#       define CPY(m) memcpy(&cp.m[0],&m[0],sizeof(cp.m));
        %par_loop(%[%ifelse("%gpar('%vname',isVector)",%[CPY(%vname)]%,%[cp.%vname = %vname]%);]%);
#       undef CPY
        CaCUDALib_Var_To_Device(cactus_parameters,cp);

        CaCUDA_Kernel_Launch_Parameters klp;
#define A(m) klp.m = m;
        klp.cagh_iteration = cctk_iteration_cpy;
        klp.cagh_time = cctk_time_cpy;
        A(cagh_ni); A(cagh_nj); A(cagh_nk);
        A(cagh_blocky);

        klp.cagh_dx = CCTK_DELTA_SPACE(0);
        klp.cagh_dy = CCTK_DELTA_SPACE(1);
        klp.cagh_dz = CCTK_DELTA_SPACE(2);
        klp.cagh_dt = CCTK_DELTA_TIME;
        klp.cagh_xmin = CCTK_ORIGIN_SPACE(0);
        klp.cagh_ymin = CCTK_ORIGIN_SPACE(1);
        klp.cagh_zmin = CCTK_ORIGIN_SPACE(2);

        klp.bwid_xn = kernel_config.bwid_n[0];
        klp.bwid_xp = kernel_config.bwid_p[0];
        klp.bwid_yn = kernel_config.bwid_n[1];
        klp.bwid_yp = kernel_config.bwid_p[1];
        klp.bwid_zn = kernel_config.bwid_n[2];
        klp.bwid_zp = kernel_config.bwid_p[2];
        klp.i_length_x = kernel_config.i_length[0];
        klp.i_length_y = kernel_config.i_length[1];
        klp.i_length_z = kernel_config.i_length[2];

        // WARNING: These values currently ignored, instead
        // the values provided by the cakernel.ccl file will
        // be used in the expanded template so that they will
        // be compile-time constants.
        A(tile_x); A(tile_y); A(tile_z); A(tile_zz);
        klp.tile_yy = 1;
        klp.gf_use_offsets = false;
        klp.block_size = db.x;

#undef A

        CaCUDALib_Var_To_Device(kernel_launch_parameters,klp);

        CE( cuCtxSetCacheConfig( CU_FUNC_CACHE_PREFER_L1 ) );

        CUstream stream = NULL;
        CE( cuEventRecord( cak__dm->sc_launch_start_ev, stream ) );

        CAKERNEL_%{name}<<<dg,db>>>
        ( %var_loop_if(%['%gvar("%vname", type)'=='GF']%) %[
            d_%{vname},
            %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)")
              %[ %set(arg1,"%var(fi)") d_%{vname}%macro(append_p),]% ]% ]%
          %var_loop_if(%['%gvar("%vname", type)'!='GF']%) %[
            d_%{vname}, ]%
          gf_base_address, cctk_iteration_cpy, cctk_time_cpy
          );

        CE( cuEventRecord( cak__dm->sc_launch_end_ev, stream ) );
        CE( cuEventSynchronize(cak__dm->sc_launch_end_ev) );
        float et_ms = -1;
        CE( cuEventElapsedTime( &et_ms, cak__dm->sc_launch_start_ev,
                                cak__dm->sc_launch_end_ev ) );
        assert( et_ms >= 0 );
        cak__dm->report_total_et += et_ms;

        CE( cuCtxSetCacheConfig( CU_FUNC_CACHE_PREFER_SHARED ) );

      }

    cak__dm->nan_scan(CCTK_PASS_CTOC, &kernel_config, args, 'o');
    CUDA_CHECK_LAST_CALL("Failed while executing the function.");
}

%macro(compile_in_init)
