// -*- c++ -*-
#ifndef _CACUDAUTIL_H_
#define _CACUDAUTIL_H_

/* CaCUDAUtil.h shall be visible to all CaCUDA developers at some point */

#include "cctk.h"
#include <typeinfo>
#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
using namespace std;

#include <CaCUDALib_driver_support.h>
#include "CaKernel.h"

%evaluate(macros)

#ifdef __CUDACC__
#include <cuda.h>
extern "C"
{
#endif

#define ATOMIC_ADD                                                            \
  atomicAdd 

#define SHARED_TILE_I3D_l(name, type)                                         \
  SHARED_TILE_NAME_I3D_l(name, type, CCTK_CAKERNEL_NAME) 
  
#define SHARED_TILE_NAME_I3D_l(name, type, fun_name)                          \
  SHARED_TILE_NAME2_I3D_l(name, type, fun_name) 

#define SHARED_TILE_NAME2_I3D_l(name, type, fun_name)                         \
template<typename t> __device__ inline type &                                 \
   CAKERNEL_GFINDEX3D_ ## fun_name ## _ ## name ## _l                         \
      (t name, int i, int j, int k) {                                         \
      return name[k][j + threadIdx.y][i + threadIdx.x];                       \
   }

  
#define SHARED_TILE(name, width_z, type)                                      \
         __shared__ type name[width_z][CAKERNEL_Tiley][CAKERNEL_Tilex];       \


#define REDUCE0(name, fun, store)                                             \
  {for(int j = CAKERNEL_Tiley, i = CAKERNEL_Tiley / 2 + CAKERNEL_Tiley % 2;   \
                                            i > 0; j = i, i = i / 2 + i % 2)  \
  {                                                                           \
    if(lj + i < j){                                                           \
      I3D_l(name, 0, 0, 0) = fun((I3D_l(name,0,0,i)), (I3D_l(name, 0, 0, 0)));\
    }                                                                         \
    __syncthreads();                                                          \
  }                                                                           \
  for(int j = CAKERNEL_Tilex, i = CAKERNEL_Tilex / 2 + CAKERNEL_Tilex % 2;    \
                                            i > 0; j = i, i = i / 2 + i % 2)  \
  {                                                                           \
    if(CAKERNEL_Tilex <= 32 || lj + i < j){                                   \
      I3D_l(name,0,0,0) = fun((I3D_l(name,0,0,i)), (I3D_l(name,0,0,0)));      \
    }                                                                         \
    __syncthreads();                                                          \
  } if(!lk && !lj && !li) {store = I3D_l(name, 0, 0, 0);}}
 

#define CUDA_CHECK_LAST_CALL(msg)           __cutilCheckMsg(msg, __FILE__, __LINE__)

inline void __cutilCheckMsg(const char *errorMessage, const char *file,
    const int line)
{
  cudaError_t err = cudaGetLastError();
  if (cudaSuccess != err)
  {
    fprintf(stderr, "%s(%i) : cutilCheckMsg() CUTIL CUDA error : %s : %s.\n",
        file, line, errorMessage, cudaGetErrorString(err));
    exit(-1);
  }
  err = cudaDeviceSynchronize();
  if (cudaSuccess != err)
  {
    fprintf(stderr,
        "%s(%i) : cutilCheckMsg cudaDeviceSynchronize error: %s : %s.\n", file,
        line, errorMessage, cudaGetErrorString(err));
    exit(-1);
  }
}

#ifdef __CUDACC__
}
#endif

#define SYNC_BLOCK() __syncthreads()


/* MPI Util */

#ifdef CCTK_MPI

#define MPI_SAFE_CALL( call )                                              \
  {                                                                        \
    int err = call;                                                        \
    if (err != MPI_SUCCESS)                                                \
    {                                                                      \
      char mpi_error_string[MPI_MAX_ERROR_STRING+1];                       \
      int resultlen;                                                       \
      MPI_Error_string (err, mpi_error_string, &resultlen);                \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,   \
            "MPI error: %s", mpi_error_string);                            \
      exit(-1);                                                            \
     }                                                                     \
   }
#endif

/* Malloc Util */

#define MALLOC_SAFE_CALL( call )                                           \
  {                                                                        \
    void *err = call;                                                      \
    if(err == NULL)                                                        \
    {                                                                      \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,   \
            "Malloc error: %s", "failed to allocate memory !");            \
      exit(-1);                                                            \
    }                                                                      \
  }

#ifdef CCTK_REAL_PRECISION_4
  #define COPYSIGN copysignf
#else
  #define COPYSIGN copysign
#endif

/* util functions used in Kerner launcher */

int inline iDivUp(int a, int b){
  return (a + b - 1) / b;
}

const bool dynamic_compile = true;

REPL_TARGET_DYNAMIC_COMPILE
const bool cakernel_dynamic_compile = false;


// Placeholder declaration of grid-hierarchy variables.
//
// The values below will be replaced by actual values during
// execution. The substitution will be performed using a pre-processed
// version of the file (with a name like
// CaKernel__gpu_cuda_dc__3dblock__${name}.cu.cup). The pragma itself
// is only recognized by the CaCUDALib thorn, it's not a real compiler
// pragma. A pragma was used because pragmata are left untouched by
// the various tools processing this file.
//
// IMPORTANT:
//  (1) Do not modify the pragma unless corresponding changes made elsewhere.
//  (2) The declarations following the pragma must all be on one line.
//  (3) Initial values chosen to avoid compiler errors (though this code
//      should not really be compiled before substitution).
REPL_TARGET_GRID_HIERARCHY
const int params_cagh_ni=1; const int params_cagh_nj=1; const int params_cagh_nk=1; const int params_cagh_blocky=1; const CCTK_REAL params_cagh_dx=1; const CCTK_REAL params_cagh_dy=1; const CCTK_REAL params_cagh_dz=1; const CCTK_REAL params_cagh_dt=1; const CCTK_REAL params_cagh_xmin=1; const int params_bwid_xn = 1; const int params_bwid_yn = 1; const int params_bwid_zn = 1; const int params_bwid_xp = 1; const int params_bwid_yp = 1; const int params_bwid_zp = 1; const CCTK_REAL params_cagh_ymin=1; const CCTK_REAL params_cagh_zmin=1; const int params_cagh_boundsPhys = 63; const int params_tile_x = 8; const int params_tile_y = 4; const int params_tile_z = 32; const int params_block_size = 32; const int params_gf_use_offsets = 0; const int params_tile_yy = 1;  const int params_tile_zz = 1; const int params_i_length_x = 64; const int params_i_length_y = 8; const int params_i_length_z = 1;



// For use by statically compiled kernels.
//
__constant__ CaCUDA_Kernel_Launch_Parameters kernel_launch_parameters;

//
// Copy constant grid-hierarchy values into a structure.
//
__device__ inline void
cakernel_piraha_set_params
(CaCUDA_Kernel_Launch_Parameters& p, int cagh_it, CCTK_REAL cagh_time,
 int ccl_tile_x = 0, int ccl_tile_y = 0, int ccl_tile_z = 0 )
{
  // Copy constant values into a structure. This routine would not be
  // necessary if cudafe handled const structure declarations
  // correctly.
# define A(m) \
  p.m = cakernel_dynamic_compile ? params_##m : kernel_launch_parameters.m
  A(cagh_ni); A(cagh_nj); A(cagh_nk);
  A(cagh_blocky);
  A(cagh_dx); A(cagh_dy); A(cagh_dz); A(cagh_dt);
  A(cagh_xmin); A(cagh_ymin); A(cagh_zmin);
  A(bwid_xn); A(bwid_xp); A(bwid_yn); A(bwid_yp); A(bwid_zn); A(bwid_zp);
  A(i_length_x); A(i_length_y); A(i_length_z);
  A(cagh_boundsPhys);
# undef A
  p.cagh_iteration = cagh_it;
  p.cagh_time = cagh_time;

# define A(m,build_time_val) \
  p.m = cakernel_dynamic_compile ? params_##m : build_time_val;

  // WARNING: These values must agree with the ones assumed in
  // cacuda.3d-template.cu.
  A(tile_x,ccl_tile_x);  A(tile_y,ccl_tile_y);  A(tile_z,1); 
  A(tile_yy,1);  A(tile_zz,ccl_tile_z);
  A(gf_use_offsets,false);
  A(block_size,ccl_tile_x*ccl_tile_y);
# undef A
}

struct Cactus_Parameters
{%par_loop(%[  %gpar("%vname",CCTK_TYPE) %vname %ifelse("%gpar('%vname',isVector)",%[[%gpar(%vname,vectorExpression)]]%,%[]%);
]%)
};

// Used by statically compiled kernels.
__constant__ Cactus_Parameters cactus_parameters;

void %{thornname}_Set_Cactus_Params(CaCUDALib_CUDA_Driver_Manager *dm);


  struct P3 {
   CCTK_REAL x, y, z;
   inline P3(CCTK_REAL _x, CCTK_REAL _y, CCTK_REAL _z):x(_x), y(_y), z(_z){}
   inline P3():x(0.0f), y(0.0f), z(0.0f){}
 };
 typedef CCTK_REAL P;

#ifndef T3Dx
# define T3Dx(z, y, x) (((z) * DIMY + (y)) * DIMX + (x))
# define T3Dy(z, y, x) (T3Dx(z, y, x) + (DIMX * DIMY * DIMZ))
# define T3Dz(z, y, x) (T3Dx(z, y, x) + (DIMX * DIMY * DIMZ * 2))
#endif

#define T3Dx_val(ptr, z, y, x, pitchx, pitchy)             (((__typeof__(ptr))((char *)(ptr) + (z) * (pitchy)  + (y) * (pitchx)))[(x)])
#define T3Dy_val(ptr, z, y, x, pitchx, pitchy, next_table) (__typeof__(ptr)(((char *)&T3Dx_val(ptr, z, y, x, pitchx, pitchy)) + (next_table)))[0]
#define T3Dz_val(ptr, z, y, x, pitchx, pitchy, next_table) (__typeof__(ptr)(((char *)&T3Dx_val(ptr, z, y, x, pitchx, pitchy)) + (next_table) * 2))[0]



extern bool CaKernel_VariableDisabled(int vi);


#endif                          /* _CACUDAUTIL_H_ */
