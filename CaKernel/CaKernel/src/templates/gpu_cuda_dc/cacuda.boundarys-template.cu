
%evaluate(macros)
%macro(run1_undefvars)
%macro(run1_undefpars)

#include "CaKernel.h" 
#include<assert.h>
#include<cctk.h>

#define STREAMS_NO 8

/**
 * This is a function that gets the information about boundaries;
 * It is declared here, because otherwise, we would have to modify 
 * the interface.ccl file
 */
extern "C" 
int CaKernel_GetBoundaryInfo(const void * cctkGH_, const int * cctk_lsh, /*const int * cctk_lssh,*/
                             const int * cctk_bbox, const int * cctk_nghostzones, int * imin,
                             int * imax, int * is_symbnd, int * is_physbnd, int * is_ipbnd);

struct Launch_Info
{
  int v;                        // For debugging.
  string name;                  // For debugging.
  CUstream stream;
  CUfunction cu_function;
  dim3 cuda_grid, cuda_block;
};

static Launch_Info launch_info[27];


extern "C"
void CAKERNEL_Launch_%{name}(CCTK_ARGUMENTS)
{
  /// ... following line is required because of using *.a archives for thorns,
  ///     the variables are not initializied at all, unless used somewhere...
  %{thornname}::%{path_parent_lower}_ignoreme+=1;

    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;

    CHECK_THORN_INIT

    int vi;
    %var_loop %[
      %ifelse("%gvar('%vname', isVector)",%[ 
      vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname[0]");]%,%[
      vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname");]%)
      assert(vi >= 0);
      if (CaKernel_VariableDisabled(vi))
        return;
      void * d_%{vname} = Device_GetVarI(cctkGH, vi, 0); 
      %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)")%[ %set(arg1,"%var(fi)")
      void * d_%{vname}%macro(append_p) = Device_GetVarI(cctkGH, vi, %var(fi)); ]% ]%
    ]%

#define TS_ERROR(d)                                                           \
    if ( CAKERNEL_Tile##d <= stncl_##d##n + stncl_##d##p )                    \
      CCTK_VWarn                                                              \
       (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,                 \
        "Tile size for %{name} along %s axis (%d) too small for stencil %d -- %d\n",\
        #d, CAKERNEL_Tile##d, stncl_##d##n, stncl_##d##p);

    TS_ERROR(x); TS_ERROR(y); TS_ERROR(z);
#   undef TS_ERROR

    //
    // Prepare grid hierarchy values.
    //

    CaCUDA_Kernel_Launch_Parameters prms =
      {cctk_iteration,
       cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
       cctk_nghostzones[0], cctk_nghostzones[1], cctk_nghostzones[2],
       0,                 // blocky
       CCTK_DELTA_SPACE(0), CCTK_DELTA_SPACE(1), CCTK_DELTA_SPACE(2),
       CCTK_DELTA_TIME,
       CCTK_ORIGIN_SPACE(0), CCTK_ORIGIN_SPACE(1), CCTK_ORIGIN_SPACE(2),
       cctk_time,
       0, 0, 0, 0, 0, 0,  // Boundary widths
       0                  // Physical boundary bit vector.
      };

    {
      // Initialize boundary width members of prms, such as cagh_boundWidthxp.
      //
      int imin[3], imax[3], is_symbnd[6], is_physbnd[6], is_ipbnd[6];

      const int rv = 
        CaKernel_GetBoundaryInfo
        (cctkGH, cctk_lsh, cctk_bbox,
         cctk_nghostzones, imin, imax, is_symbnd, is_physbnd, is_ipbnd);
      assert( rv == 0 );

      // Avoid union which forces CUDA to use memory instead of regs.
      //
      CCTK_INT* const cagh_boundWidth = &prms.cagh_boundWidthxn;

      int bounds = 0;
      for (int dir = 0; dir < 3; dir++){
        if ( is_physbnd[dir * 2    ] ) bounds|= 1 << (dir * 2);
        if ( is_physbnd[dir * 2 + 1] ) bounds|= 1 << (dir * 2 + 1);
        if ( is_physbnd[dir * 2 + 1] )
          cagh_boundWidth[dir * 2 + 1] = cctk_lsh[dir] - imax[dir];
        if ( is_physbnd[dir * 2    ] )
          cagh_boundWidth[dir * 2    ] = imin[dir];
      }
      prms.cagh_boundsPhys = bounds;
    }

    //
    // Possibly Compile and Load Kernel Code
    //

    CaCUDALib_CUDA_Driver_Manager* const dm =
      CaCUDALib_CUDA_driver_manager_get();

    static bool inited = false;
    static int li_stop = 0; // Number of functions prepared.

    if ( !inited )
      {
        inited = true;
        int li_idx = 0;

        // Create a pool of streams.
        //
        CUstream streams[STREAMS_NO];
        for (int i = 0; i < STREAMS_NO; i++)
          CE( cuStreamCreate( &streams[i], 0 ) );
        int stream_next = 0;

        // Iterate through each boundary combination and compile
        // kernel only if that combination is needed.
        //
        for ( int v=1; v<27; v++ )
          {
            const int bound_x = (v+1)%3-1;
            const int bound_y=(v/3+1)%3-1;
            const int bound_z=(v/9+1)%3-1;

            // Prepare a text version of boundary configuration.
            // Currently used for debugging, but could be used as
            // part of kernel function name.
            //
            string name;
            const char* const dir = "n0p";
            name = "%{name}_";
            name += dir[bound_x+1];
            name += dir[bound_y+1];
            name += dir[bound_z+1];

            if ( !( (!bound_x
                     || prms.cagh_boundsPhys & (1 << ((bound_x+1)/2))) &&
                    (!bound_y
                     || prms.cagh_boundsPhys & (1 << ((bound_y+1)/2 + 2))) &&
                    (!bound_z
                     || prms.cagh_boundsPhys & (1 << ((bound_z+1)/2 + 4)))))
              continue;

            Launch_Info* const li = &launch_info[li_idx];
            li_idx++;
            li->v = v;
            li->name = name;

            unsigned int width[] = {16, 4, 4};
            if(bound_x < 0) width[0] = prms.cagh_boundWidthxn;
            if(bound_x > 0) width[0] = prms.cagh_boundWidthxp;
            if(bound_y < 0) width[1] = prms.cagh_boundWidthyn;
            if(bound_y > 0) width[1] = prms.cagh_boundWidthyp;
            if(bound_z < 0) width[2] = prms.cagh_boundWidthzn;
            if(bound_z > 0) width[2] = prms.cagh_boundWidthzp;
   
            unsigned int blocks[] = {1, 1, 1};
            if (bound_x == 0)
              blocks[0] =
                iDivUp( prms.cagh_ni
                        - (prms.cagh_boundWidthxn + prms.cagh_boundWidthxp), 
                        width[0]);
            if ( bound_y == 0) blocks[1] = iDivUp(prms.cagh_nj - (prms.cagh_boundWidthyn + prms.cagh_boundWidthyp), width[1]);
            if (bound_z == 0) blocks[2] = iDivUp(prms.cagh_nk - (prms.cagh_boundWidthzn + prms.cagh_boundWidthzp), width[2]);

            prms.cagh_blocky = blocks[1];

            ///
            /// Parameters should not be changed below this line.
            ///

            // Create module (compiled collection of kernels) corresponding
            // to this file (after macro expansion).  This will be done
            // once for each boundary configuration.
            //
            const bool module_vals_needed =
              dm->module_get(name,STR(__FILE__),
                             dynamic_compile ? DM_Dynamic : DM_Static );
            assert( module_vals_needed );

            // If present, un-compress kernel source stored in
            // included array. This will be done once per source file
            // named in PIRAHA_THORN_BUILD_FILE (not once per
            // iteration).
            //
#ifdef CAK__EMBED_SOURCE
#include STR(CAK__EMBED_SOURCE)
            dm->module_src_set
              (cak__src_uncomp_chars, cak__src_comp_chars,
               (char*)&cak__src_words[0]);
#endif

            // Specify run-time values for variables that were declared
            // const and set to dummy values.
            //
            dm->module_vals_set
              (STR(REPL_KEY_DYNAMIC_COMPILE),REPL_VALUE_DYNAMIC_COMPILE);
            %{thornname}_Set_Cactus_Params(dm);
            assert( false ); // Need replacement for line below.
            //  %{thornname}_Set_Kernel_Launch_Params(dm,prms);
            ostringstream repl;
#define MI(m) Mg("int",m)
#define Mg(t,m)                                                         \
            repl << "const " << t << " " << #m << " = " << m << ";\n";
            MI(bound_x); MI(bound_y); MI(bound_z);
            dm->module_vals_set(STR(REPL_KEY_BOUND_XYZ),repl.str());
#undef MI
#undef Mg

            if ( verbose )
              CCTK_VInfo
                (CCTK_THORNSTRING,
                 "Compiling code for boundary(%2d,%2d,%2d) update, "
                 "with blocks(%2d,%2d,%2d), stream(%d) and grid(%2d,%2d,%2d)",
                 bound_x, bound_y, bound_z, width[0], width[1], width[2],
                 stream_next, blocks[0], blocks[1], blocks[2]);

            li->cuda_grid.x = blocks[0];
            li->cuda_grid.y = blocks[1] * blocks[2];
            li->cuda_block.x = width[0];
            li->cuda_block.y = width[1];
            li->cuda_block.z = width[2];
            li->stream = streams[stream_next];
            li->cu_function = dm->function_load("CAKERNEL_%{name}");

            stream_next = (stream_next + 1) % STREAMS_NO;
          }
        li_stop = li_idx;
      }

    //
    // Collect variables to pass as kernel arguments.
    //

    if(*((int *)CCTK_VarDataPtr(cctkGH, 0, "CaKernel::compile_only"))) 
      return;
      
    int cctk_iteration_cpy = cctk_iteration;
    CCTK_REAL cctk_time_cpy = cctk_time;
    void *args[] =
      {
        %var_loop_if(%['%gvar("%vname", type)'=='GF']%) %[
        &d_%{vname},
        %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)")
            %[ %set(arg1,"%var(fi)") &d_%{vname}%macro(append_p),]% ]% ]%
        %var_loop_if(%['%gvar("%vname", type)'!='GF']%) %[
        &d_%{vname},]%
        &cctk_iteration_cpy, &cctk_time_cpy
      };

    //
    // Launch Kernels
    //

    for ( int i=0; i<li_stop; i++ )
      {
        Launch_Info* const li = &launch_info[i];
        CE( cuLaunchKernel
            (li->cu_function, li->cuda_grid.x, li->cuda_grid.y, 1,
             li->cuda_block.x, li->cuda_block.y, li->cuda_block.z, 
             0, // Dynamic shared memory
             li->stream,
             args, //  kernel_args,
             NULL) );
      }

  CUDA_CHECK_LAST_CALL("Failed while executing the boundary update.");
}
 
%macro(compile_in_init)
