 /*@@
   @file      cacuda.boundary-template.cu
   @date      $Date:$
   @author    Marek Blazewicz, Steven R. Brandt, later David M. Koppelman
   @desc 
   Template for generation of boundary management host code, including
   dynamic compilation.
   @enddesc 
   @version   $Revision:$
   @id        $Id:$

 @@*/

#include "CaKernel.h" 
#include <CaCUDALib_driver_support.h>
#include <string>
using namespace::std;

%evaluate(macros)

#include<assert.h>

extern "C"
void CAKERNEL_Launch_%{name}(CCTK_ARGUMENTS)
{
  /// ... following line is required because of using *.a archives for thorns,
  ///     the variables are not initializied at all, unless used somewhere...
  %{thornname}::%{path_parent_lower}_ignoreme+=1;

    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;

    CHECK_THORN_INIT
    
    int vi;
    %var_loop %[
      %ifelse("%gvar('%vname', isVector)",%[ 
      vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname[0]");]%,%[
      vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname");]%)
      assert(vi >= 0);
      if (CaKernel_VariableDisabled(vi))
        return;
      void * d_%{vname} = Device_GetVarI(cctkGH, vi, 0); 
      %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)")%[ %set(arg1,"%var(fi)")
      void * d_%{vname}%macro(append_p) = Device_GetVarI(cctkGH, vi, %var(fi)); ]% ]%
    ]%

#define TS_ERROR(d)                                                           \
    if ( CAKERNEL_Tile##d <= stncl_##d##n + stncl_##d##p )                    \
      CCTK_VWarn                                                              \
       (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,                 \
        "Tile size for %{name} along %s axis (%d) too small for stencil %d -- %d\n",\
        #d, CAKERNEL_Tile##d, stncl_##d##n, stncl_##d##p);

    TS_ERROR(x); TS_ERROR(y); TS_ERROR(z);
#   undef TS_ERROR

    //
    // Prepare grid hierarchy values.
    //

    const int blocky = 0;
    CaCUDA_Kernel_Launch_Parameters prms =
      {cctk_iteration,
       cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
       cctk_nghostzones[0], cctk_nghostzones[1], cctk_nghostzones[2],
       blocky,
       CCTK_DELTA_SPACE(0), CCTK_DELTA_SPACE(1), CCTK_DELTA_SPACE(2),
       CCTK_DELTA_TIME,
       CCTK_ORIGIN_SPACE(0), CCTK_ORIGIN_SPACE(1), CCTK_ORIGIN_SPACE(2),
       cctk_time,
       0, 0, 0, 0, 0, 0,  // Boundary widths
       0                  // Physical boundary bit vector.
      };

    // Quick-and-dirty storage.
    static CUstream streams[6];
    static CUfunction cu_function[6];

    //
    // Possibly Compile and Load Kernel Code
    //

    CaCUDALib_CUDA_Driver_Manager* const dm =
      CaCUDALib_CUDA_driver_manager_get();

    // Look for module (compiled collection of kernels) corresponding
    // to this file (after macro expansion).
    //
    const bool module_vals_needed =
      dm->module_get("%{name}",STR(__FILE__),
                     dynamic_compile ? DM_Dynamic : DM_Static );
    if ( module_vals_needed )
      {
        // If present, un-compress kernel source stored in included array.
#ifdef CAK__EMBED_SOURCE
#include STR(CAK__EMBED_SOURCE)
        dm->module_src_set
          (cak__src_uncomp_chars, cak__src_comp_chars,
           (char*)&cak__src_words[0]);
#endif

        // Values for cactus, launch, and other parameters are needed
        // for dynamic compilation.  Provide them here.
        //

        dm->module_vals_set
          (STR(REPL_KEY_DYNAMIC_COMPILE),REPL_VALUE_DYNAMIC_COMPILE);
        
        %{thornname}_Set_Cactus_Params(dm);
        assert( false ); // Need replacement for line below.
        //  %{thornname}_Set_Kernel_Launch_Params(dm,prms);

        // Create a stream for each face.
        for (int i = 0; i < 6; i++) CE( cuStreamCreate( &streams[i], 0 ) );

        // Copy function handles to quick-and-dirty static storage.
#define GETFUN(face,coor) \
        cu_function[face] = dm->function_load("CAKERNEL_%{name}_" coor);
        GETFUN(1,"p00"); GETFUN(0,"n00");
        GETFUN(3,"0p0"); GETFUN(2,"0n0");
        GETFUN(5,"00p"); GETFUN(4,"00n");
#undef GETFUN
      }

    //
    // Collect variables to pass as kernel arguments.
    //

    int cctk_iteration_cpy = cctk_iteration;
    CCTK_REAL cctk_time_cpy = cctk_time;

    void *args[] =
      {
        %var_loop %[ &d_%{vname}, ]%
        &cctk_iteration_cpy, &cctk_time_cpy
      };

    CE( cuStreamSynchronize(0) );

    const int gridx1 = iDivUp(prms.cagh_nj, CAKERNEL_Tiley);
    const int gridy1 = iDivUp(prms.cagh_nk, CAKERNEL_Tilez);

    if (cctkGH->cctk_bbox[1] == 1)
      CE( cuLaunchKernel
          (cu_function[1], gridx1, gridy1, 1,
           CAKERNEL_Tiley, CAKERNEL_Tilez, 1,
           0, streams[1], args, NULL));

    if (cctkGH->cctk_bbox[0] == 1)
      CE( cuLaunchKernel
          (cu_function[0], gridx1, gridy1, 1,
           CAKERNEL_Tiley, CAKERNEL_Tilez, 1,
           0, streams[0], args, NULL));

    const int gridx3 = iDivUp(prms.cagh_ni, CAKERNEL_Tilex);
    const int gridy3 = iDivUp(prms.cagh_nk, CAKERNEL_Tilez);

    if (cctkGH->cctk_bbox[3] == 1)
      CE( cuLaunchKernel
          (cu_function[3], gridx3, gridy3, 1,
           CAKERNEL_Tilex, CAKERNEL_Tilez, 1,
           0, streams[3], args, NULL));

    if (cctkGH->cctk_bbox[2] == 1)
      CE( cuLaunchKernel
          (cu_function[2], gridx3, gridy3, 1,
           CAKERNEL_Tilex, CAKERNEL_Tilez, 1,
           0, streams[2], args, NULL));

    const int gridx5 = iDivUp(prms.cagh_ni, CAKERNEL_Tilex);
    const int gridy5 = iDivUp(prms.cagh_nj, CAKERNEL_Tiley);

    if (cctkGH->cctk_bbox[5] == 1)
      CE( cuLaunchKernel
          (cu_function[5], gridx5, gridy5, 1,
           CAKERNEL_Tilex, CAKERNEL_Tiley, 1,
           0, streams[5], args, NULL));

    if (cctkGH->cctk_bbox[4] == 1)
      CE( cuLaunchKernel
          (cu_function[4], gridx5, gridy5, 1,
           CAKERNEL_Tilex, CAKERNEL_Tiley, 1,
           0, streams[4], args, NULL));

    CUDA_CHECK_LAST_CALL("Failed while executing the boundary update.");
}

%macro(compile_in_init)
