/* Assume Piraha will generate this file and this file will be pushed here as well */

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "CaKernel.h"
#include <CaCUDALib_driver_support.h>
#include <cuda.h> 

#include <assert.h>
#include <algorithm>
#include <string>
#include <iostream>
#include <sstream>
using namespace std;
%evaluate(macros) 


static Vars UN d_vars;

void %{thornname}_AllocDevVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' != 'GF' ]%) > 0 ]%, %[
  
  int numvars = %macro(num_vars_sepinout);
  size_t sizes[%macro(num_vars_sepinout) + 1] = {
    %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      sizeof (%gvar("%vname", CCTK_TYPE)), ]%
    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      sizeof (%gvar("%vname", CCTK_TYPE)), ]%
    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      sizeof (%gvar("%vname", CCTK_TYPE)), ]%
      16 // to make sure that the "structure" is aligned to 16 bytes
    };   

  size_t elements[%macro(num_vars_sepinout) + 1] = {
    %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)

    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)

    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)
      0
    };   

  int offsets[%macro(num_vars_sepinout) + 1];   
  
  offsets[0] = 0;
  for (int i = 1; i <= numvars; i++){
    int oldoffset = offsets[i - 1] + sizes[i - 1] * elements[i - 1];
    for(int b = 2; b <= 16 && b <= sizes[i]; b <<= 1)
      oldoffset += (b - (oldoffset % b)) % b;
    offsets[i] =  oldoffset;
  }
  d_vars.sepinout_out = d_vars.sepinout = d_vars.all = offsets[numvars];
  %set(tmpi, 0)
  %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    d_vars.%{vname}_offset = offsets[(int)%inc(tmpi)]; ]%)

  %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    d_vars.sepinout = min(offsets[(int) %var(tmpi)], d_vars.sepinout);
    d_vars.%{vname}_offset = offsets[(int)%inc(tmpi)]; ]%)

  %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    d_vars.sepinout_out = min(offsets[(int) %var(tmpi)], d_vars.sepinout);
    d_vars.%{vname}_offset = offsets[(int)%inc(tmpi)]; ]%)

  CE( cuMemAlloc( &d_vars.ptr, offsets[numvars]));
  ]%)
}

#if 0
void %{thornname}_FreeDevVars(){
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' != 'GF' ]%) > 0 ]%, %[
 CE( cuMemFree(d_vars.ptr) );
  ]%)
}
#endif

void %{thornname}_AllocDevPars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const int numvars = %macro(num_pars);

  struct { const string type; const char *name; } vinfo[] =
  {%par_loop(%[
    {"%gpar("%vname",CCTK_TYPE)", "%vname"},]%)
    {"The","end"}
  };

  ostringstream unrec_types;
  int err_count = 0;
  for ( int i=0; i<numvars; i++ )
    {
      if ( vinfo[i].type == "CCTK_REAL" 
           || vinfo[i].type == "CCTK_INT" ) continue;
      if ( err_count ) unrec_types << ", ";
      err_count++;
      unrec_types << vinfo[i].type << " " << vinfo[i].name;
    }
  if ( err_count )
    {
      string err_msg
        = string("The bad news: the CaCUDA parameter types below can not ")
        + "yet be handled."
        + "\nThe good news: you have a test case for us."
        + "\n " + unrec_types.str();
      CCTK_WARN(CCTK_WARN_ABORT, err_msg.c_str());
    }
}

// Used only for sizeof calculations for our friend cudafe.
static Cactus_Parameters UN cactus_parameters_dummy;

// Return a string initializer, used by dynamic compilation code.
template<typename T> static string
make_initializer(T *base, int size)
{
  ostringstream izer;
  izer << "[" << size << "] = {";
  for ( int i=0; i<size; i++ )
    {
      if ( i ) izer << ", ";
      izer << base[i];
    }
  izer << "}; ";
  return izer.str();
}

#define MAKE_IZER(m) make_initializer\
  (m,sizeof(cactus_parameters_dummy.m)/sizeof(cactus_parameters_dummy.m[0]))

void
%{thornname}_Set_Cactus_Params(CaCUDALib_CUDA_Driver_Manager *dm)
{
  DECLARE_CCTK_PARAMETERS;

  ostringstream rep;
  %par_loop(%[
rep << "const %gpar("%vname",CCTK_TYPE) %vname " << %ifelse("%gpar('%vname',isVector)",%[MAKE_IZER(%vname)]%,%[" = " << %vname << "; "]%);]%)

  // Specify values for Cactus parameters.
  dm->module_vals_set(STR(REPL_KEY_CACTUS_PARAMS), rep.str());
}

void %{thornname}_%{path_parent_lower}_AllocDevMem (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int cak__num_tl = 1;
  int cak__vi = -1;
  %var_loop %[
    %ifelse("%gvar('%vname', isVector)",%[ 
    cak__vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname[0]");]%,%[
    if ( %tlevel == 0 ) {
      cak__vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname");]%)
      assert(cak__vi >= 0);
      cak__num_tl = CCTK_DeclaredTimeLevelsVI(cak__vi);
      Device_RegisterMem(CCTK_PASS_CTOC, cak__vi, cak__num_tl); 
    }
  ]%
  %com%[%ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' == 'GF' ]%) > 0 ]%, %[
  const size_t dataSize = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2] ;

  %var_loop_if(%['%gvar("%vname", "type")'=='GF']%) 
  %[ %ifelse("%gvar('%vname', isVector)", %[
    CE( cuMemAlloc
        ( &d_%{vname},
          dataSize*sizeof(*%{vname}) * (%gvar("%vname", vectorExpression))));
      ]%, %[
    CE( cuMemAlloc( &d_%{vname}, dataSize * sizeof(*%{vname}))); ]%)]% 

  %var_loop_if(%['%gvar("%vname", "type")'=='GF' && '%varAttr("%vname", "intent")'=='separateinout']%) %[
  CE( cuMemAlloc( &d_%{vname}_out, dataSize * sizeof(*%{vname})));]%

  CCTK_INFO ("device memory for CaCUDA variables successfully allocated");
  ]%)]%
  %{thornname}_AllocDevPars(CCTK_PASS_CTOC);
}


%macro(schedule_in_init)


