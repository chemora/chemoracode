#ifndef %{name_upper}
#define %{name_upper}

/* definition of CCTK_REAL */
#include "cctk.h"
#include <cuda.h>

extern "C" void * Device_GetVarI(void *cctkGH, int vi, int tl);
extern "C" int Device_RegisterMem(void * cctkGH_, int vi, int tl);
extern "C" int Device_UnRegisterMem(void * cctkGH_, int vi);

#ifdef __CUDACC__

/* device pointers */
/* All the external variables will be declared here.
 * We will generate the this file from the parser directly but we
 * need to generate the memory allocation routines as well.
 */

struct Vars {
  CUdeviceptr ptr;
  union
  {
    struct
    {
      %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      unsigned int %{vname}_offset; ]%
   
      %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      unsigned int %{vname}_offset; ]%
    
      %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      unsigned int %{vname}_out_offset; ]%
    
      unsigned int all;
      unsigned int sepinout;
      unsigned int sepinout_out;
    };
    unsigned int field[];
  };
};

namespace %{thornname} {
  extern int %{path_parent_lower}_ignoreme;
}

//extern Vars d_vars;

#endif

#endif /* %{name_upper} */
