#include "cctk.h"

#include "CaKernel.h"

#include<list>
#include<string> 

using std::list;
using std::string;

void CaKernel_ThornInitialized(string name);
void CaKernel_SchedulePreCompileFunction(void * cctkGH_, void (*funptr)(CCTK_ARGUMENTS));

namespace %{thornname} {
  
  static list<thorn_fun_t> * funlist = 0;
  static list<thorn_fun_t> * funlist_comp = 0;
  bool thorn_initialized = false;
  
  int push_back(thorn_fun_t thorn_fun){
    static int allocated = 0;
    if(!allocated){
      funlist = new list<thorn_fun_t>();
      allocated = 1;
    }
    funlist->push_back(thorn_fun);
    return 0;
  }

  int push_back_for_compilation(thorn_fun_t thorn_fun){
    static int allocated = 0;
    if(!allocated){
      funlist_comp = new list<thorn_fun_t>();
      allocated = 1;
    }
    funlist_comp->push_back(thorn_fun);
    return 0;
  }
   
  extern "C"
  void %{thornname}_Init(CCTK_ARGUMENTS){
    CaKernel_ThornInitialized(CCTK_THORNSTRING);
    if(funlist){
      for(list<thorn_fun_t>::iterator it = funlist->begin(), it2 = funlist->end();
          it != it2; ++it){
        (*it)(CCTK_PASS_CTOC);
      }
//      delete funlist;
//      funlist = 0;
    }
    if(funlist_comp){
      for(list<thorn_fun_t>::iterator it = funlist_comp->begin(), 
          it2 = funlist_comp->end(); it != it2; ++it){
        CaKernel_SchedulePreCompileFunction(CCTK_PASS_CTOC, *it);
      }
//      delete funlist_comp;
//      funlist_comp = 0;
    }
    thorn_initialized = true;
  }
}



