#include "CaKernel.h" 



%evaluate(macros)

extern "C"
void CAKERNEL_Launch_%{path_lower_parent}_%{name}(CCTK_ARGUMENTS)
{
  /// ... following line is required because of using *.a archives for thorns,
  ///     the variables are not initializied at all, unless used somewhere...
  %{thornname}::%{path_parent_lower}_ignoreme+=1;

  DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;

    CHECK_THORN_INIT

    Kernel_Launch_Parameters prms(cctk_iteration, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
    cctk_nghostzones[0], cctk_nghostzones[1], cctk_nghostzones[2],
    cctk_delta_space[0], cctk_delta_space[1], cctk_delta_space[2], cctk_delta_time,
    cctk_origin_space[0], cctk_origin_space[1], cctk_origin_space[2], cctk_time);

    #pragma omp parallel sections 
    {
      #pragma omp section 
      if(cctkGH->cctk_bbox[0] == 1){
        int i = 0;
        %set(arg1, "CAKERNEL_%{path_lower_parent}_%{name}<-1, 0, 0>(%macro(run1_pass_args) i, j, k, prms);") 
        %macro(LOOP_yz);
      }

      #pragma omp section 
      if(cctkGH->cctk_bbox[1] == 1){
        int i = cctk_lsh[0] - 1;
        %set(arg1, "CAKERNEL_%{path_lower_parent}_%{name}<1, 0, 0>(%macro(run1_pass_args) i, j, k, prms);") 
        %macro(LOOP_yz);
      }

      #pragma omp section 
      if(cctkGH->cctk_bbox[2] == 1){
        int j = 0;
        %set(arg1, "CAKERNEL_%{path_lower_parent}_%{name}<0,-1, 0>(%macro(run1_pass_args) i, j, k, prms);") 
        %macro(LOOP_xz);
      }

      #pragma omp section 
      if(cctkGH->cctk_bbox[3] == 1){
        int j = cctk_lsh[1] - 1;
        %set(arg1, "CAKERNEL_%{path_lower_parent}_%{name}<0, 1, 0>(%macro(run1_pass_args) i, j, k, prms);") 
        %macro(LOOP_xz); 
      }

      #pragma omp section 
      if(cctkGH->cctk_bbox[4] == 1){
        int k = 0;
        %set(arg1, "CAKERNEL_%{path_lower_parent}_%{name}<0, 0,-1>(%macro(run1_pass_args) i, j, k, prms);") 
        %macro(LOOP_xy);
      }

      #pragma omp section 
      if(cctkGH->cctk_bbox[5] == 1){
        int k = cctk_lsh[2] - 1;
        %set(arg1, "CAKERNEL_%{path_lower_parent}_%{name}<0, 0, 1>(%macro(run1_pass_args) i, j, k, prms);") 
        %macro(LOOP_xy);
      }
    }

    %com%[%var_loop("intent=separateinout",'std::swap(%{vname}, %{vname}_out);')]%
}

