%t%[
%evaluate(macros_group)

%set_macro(run1_3dblock_dec_cache, %[                                         \
  %var_loop_if(%[ '%varAttr("%vname", cached)'=='yes' && '%gvar("%vname", type)'=='gf' ]%,' %gvar("%vname", CCTK_TYPE) %{vname}_sh[CAKERNEL_Tilez][CAKERNEL_Tiley][CAKERNEL_Tilex] = {0};\\\n')\
]%)

%set_macro(run1_3dblock_indexing, %[                                          \
#define CAKERNEL_GFINDEX3D_%{name}_l(ptr, i, j, k)                            \
 (ptr##_sh[k + lk][j + lj][i + li])

#define I3D_l CAKERNEL_GFINDEX3D_%{name}_l

]%)

%set_macro(run1_3dindexing_specific, %[  
 %ifthen(%[ %var_loop_ifno(%[ '%varAttr("%vname",intent)' == 'separateinout' ]%) > 0 ]%, 
  '#error "separateinout variables are not supported in this type of templates"')

 //This macros should be defined in runtime changing the state of being cached or not 
  %var_loop_if(%[ ('%varAttr("%vname", intent)' == 'inout' || '%varAttr("%vname", intent)' == 'in') && '%gvar("%vname", type)'=='gf' ]%)%[
    %ifelse(%[ '%varAttr("%vname", cached)' == 'yes' ]%, %[ #define CAKERNEL_VAR_%{vname}_CACHED 1 ]%, 
    %[ #define CAKERNEL_VAR_%{vname}_CACHED 0 ]%)
  ]%

  %var_loop_if(%['%gvar("%vname", type)'=='gf']%) %[

#if CAKERNEL_VAR_%{vname}_CACHED       

%macro(run1_3dblock_indexing_spec_cached) 
#define CAKERNEL_VAR_%{vname}_DECLARE_CACHE             \
    %macro(run1_3dblock_dec_cache_spec) 

%com%[#define CAKERNEL_VAR_%{vname}_FETCH_TO_CACHE                            \
  %macro(run1_fetch_to_cache_spec)]%

%com%[#define CAKERNEL_VAR_%{vname}_ITERATE_CACHE                                   \
  %macro(run1_iterate_cache_spec)]%

%com%[#define CAKERNEL_VAR_%{vname}_FETCH_FRONT_TILE_CACHE                          \
  %macro(run1_fetch_front_tile_to_cache_spec)]%

#else
  %macro(run1_3dblock_indexing_spec) 

  #define CAKERNEL_VAR_%{vname}_DECLARE_CACHE             
%com%[#define CAKERNEL_VAR_%{vname}_FETCH_TO_CACHE]%                                  
%com%[#define CAKERNEL_VAR_%{vname}_ITERATE_CACHE]%                                   
%com%[#define CAKERNEL_VAR_%{vname}_FETCH_FRONT_TILE_CACHE]%                          
#endif
  ]%

  %macro(run1_3dcommon_indexing_spec)
]%)


%set_macro(run1_3dblock_dec_cache_spec, %[                                         \
  %gvar("%vname", CCTK_TYPE) %{vname}_sh[CAKERNEL_Tilez][CAKERNEL_Tiley][CAKERNEL_Tilex];\
]%)


%set_macro(run1_pass_args, %[
%var_loop_if(%['%varAttr("%vname",intent)'=='separateinout' && '%gvar("%vname",type)'=='GF']%,'%{vname}, (typeof(%{vname})) %{vname}_out,')
%var_loop_if(%['%varAttr("%vname",intent)'!='separateinout' && '%gvar("%vname",type)'=='GF']%,'%{vname},')
%var_loop_if(%['%varAttr("%vname",intent)'=='separateinout' && '%gvar("%vname",type)'!='GF']%,'%{vname}, (typeof(%{vname})) %{vname}_out, ')
%var_loop_if(%['%varAttr("%vname",intent)'!='separateinout' && '%gvar("%vname",type)'!='GF']%,'%{vname}, ')
%par_loop('(typeof(%{vname})) %{vname}, ')
]%)

%set_macro(run1_declare_args, %[                                              \
%var_loop_if(%['%varAttr("%vname",intent)'=='separateinout' && '%gvar("%vname",type)'=='GF']%,' const %gvar("%vname",CCTK_TYPE) * %{vname}, %gvar("%vname",CCTK_TYPE) * %{vname}_out,')\
%var_loop_if(%['%varAttr("%vname",intent)'!='separateinout' && '%gvar("%vname",type)'=='GF']%,' %gvar("%vname",CCTK_TYPE) * %{vname},')\
%var_loop_if(%['%varAttr("%vname",intent)'=='separateinout' && '%gvar("%vname",type)'!='GF']%,' %gvar("%vname",CCTK_TYPE) * %{vname}, %gvar("%vname",CCTK_TYPE) * %{vname}, ')\
%var_loop_if(%['%varAttr("%vname",intent)'!='separateinout' && '%gvar("%vname",type)'!='GF']%,' %gvar("%vname",CCTK_TYPE) * %{vname}, ')\
%par_loop()%[const %gpar("%vname",CCTK_TYPE) %ifthen(%[ %gpar("%vname", isVector) ]%, " * const ") %{vname}, ]% \
]%)

%set_macro(X_gl) %[                                                          
  for(int i = 0; i < cctk_lsh[0]; i++) ]%

%set_macro(Y_gl) %[                                                          
  for(int j = 0; j < cctk_lsh[1]; j++) ]%

%set_macro(Z_gl) %[                                                          
  for(int k = 0; k < cctk_lsh[2]; k++) ]%

%set_macro(LOOP) %[    
  do{ %macro(Z_gl)                  
      %macro(Y_gl)                  
      %macro(X_gl)                  
        %var(arg1) }while(0) ]%

%set_macro(LOOP_xy) %[ 
  do{ %macro(Y_gl)                  
      %macro(X_gl)                  
        %var(arg1) }while(0) ]%
  
%set_macro(LOOP_xz) %[ 
  do{ %macro(Z_gl)                  
      %macro(X_gl)                  
        %var(arg1) }while(0) ]%

%set_macro(LOOP_yz) %[      
  do{ %macro(Z_gl)                  
      %macro(Y_gl)                  
        %var(arg1) }while(0) ]%


%set_macro(X_tgl) %[                                                   
  for(int i = 0; i <= cctk_lsh[0] - CAKERNEL_Tilex; i+= cmpt_x) ]%

%set_macro(Y_tgl) %[                                                   
  for(int j = 0; j <= cctk_lsh[1] - CAKERNEL_Tiley; j+= cmpt_y) ]%

%set_macro(Z_tgl) %[                                                   
  for(int k = 0; k <= cctk_lsh[2] - CAKERNEL_Tilez; k+= cmpt_z) ]%

%set_macro(X_rgl) %[                                                   
  int div = (cctk_lsh[0] - stncl_xn - stncl_xp) % cmpt_x;              
  int i = cctk_lsh[0] - stncl_xn - stncl_xp - div;                     
  if(div) ]%    
                                                                       
%set_macro(Y_rgl) %[                                                   
  int div = (cctk_lsh[1] - stncl_yn - stncl_yp) % cmpt_y;              
  int j = cctk_lsh[1] - stncl_yn - stncl_yp - div;                     
  if(div) ]%
                                                                       
%set_macro(Z_rgl) %[                                                   
  int div = (cctk_lsh[2] - stncl_zn - stncl_zp) % cmpt_z;              
  int k = cctk_lsh[2] - stncl_zn - stncl_zp - div;                     
  if(div) ]%


%set_macro(LOOP_t) %[           
      #pragma omp parallel
      {
        { 
          #pragma omp for
          %macro(Z_tgl) %macro(Y_tgl) %macro(X_tgl)       
             %var(arg1)
        }           
        
        {
          %macro(X_rgl) 
          {
            #pragma omp for
            %macro(Z_tgl) %macro(Y_tgl)       
            %var(arg2)
          }
        }           
        
        {
          %macro(Y_rgl)  
          { 
            #pragma omp for
            %macro(Z_tgl) %macro(X_tgl)       
              %var(arg2)
          }
        }           
        
        {
          %macro(Z_rgl) 
          {
            #pragma omp for
            %macro(Y_tgl) %macro(X_tgl)       
              %var(arg2)
          }
        }           
        #pragma omp sections
        {
          #pragma omp section
          {%macro(Y_rgl) {%macro(X_rgl) %macro(Z_tgl)      
             %var(arg2)}}          

          #pragma omp section
          {%macro(Z_rgl) {%macro(X_rgl) %macro(Y_tgl)      
             %var(arg2)}}          

          #pragma omp section
          {%macro(Z_rgl) {%macro(Y_rgl) %macro(X_tgl)      
             %var(arg2)}}          

          #pragma omp section
          {%macro(Z_rgl) {%macro(Y_rgl) {%macro(X_rgl)     
           %var(arg2)}}}         
        }
      } ]%
]%
