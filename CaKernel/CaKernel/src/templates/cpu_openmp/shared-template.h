
#ifndef %{name_upper}
#define %{name_upper}

#include "cctk.h"
#include <typeinfo>
#include <stdio.h>

#include<string.h>
#include "CaKernel.h"

struct Kernel_Launch_Parameters{
  int cagh_it;
  int cagh_ni;
  int cagh_nj;
  int cagh_nk;
  int cagh_nghostsi;
  int cagh_nghostsj;
  int cagh_nghostsk;
  CCTK_REAL cagh_dx;
  CCTK_REAL cagh_dy;
  CCTK_REAL cagh_dz;
  CCTK_REAL cagh_dt;
  CCTK_REAL cagh_xmin;
  CCTK_REAL cagh_ymin;
  CCTK_REAL cagh_zmin;
  CCTK_REAL cagh_time;
  Kernel_Launch_Parameters():cagh_it(0), cagh_ni(0), cagh_nj(0),
    cagh_nk (0), cagh_nghostsi (0), cagh_nghostsj (0), cagh_nghostsk (0), 
    cagh_dx (0), cagh_dy (0), cagh_dz (0), cagh_dt (0), cagh_xmin (0), cagh_ymin (0),
    cagh_zmin (0), cagh_time (0)
  {
  }

  Kernel_Launch_Parameters(int _cagh_it, int _cagh_ni, int _cagh_nj, int _cagh_nk,
          int _cagh_nghostsi, int _cagh_nghostsj, int _cagh_nghostsk, 
          CCTK_REAL _cagh_dx, CCTK_REAL _cagh_dy, CCTK_REAL _cagh_dz, CCTK_REAL _cagh_dt,
          CCTK_REAL _cagh_xmin, CCTK_REAL _cagh_ymin, CCTK_REAL _cagh_zmin, CCTK_REAL _cagh_time) :
    cagh_it (_cagh_it), cagh_ni (_cagh_ni), cagh_nj (_cagh_nj), cagh_nk (_cagh_nk),
    cagh_nghostsi (_cagh_nghostsi), cagh_nghostsj (_cagh_nghostsj), cagh_nghostsk (_cagh_nghostsk),
    cagh_dx (_cagh_dx), cagh_dy (_cagh_dy), cagh_dz (_cagh_dz),
    cagh_dt (_cagh_dt), cagh_xmin (_cagh_xmin), cagh_ymin (_cagh_ymin), cagh_zmin (_cagh_zmin),
    cagh_time (_cagh_time)
  {
  }
};



#ifdef CCTK_REAL_PRECISION_4
  #define COPYSIGN copysignf
#else
  #define COPYSIGN copysign
#endif

#define __shared__

#define SYNC_BLOCK()                            \
}COMPUTE_LOOP{

#define __syncthreads() SYNC_BLOCK()

#define SYNC_CALLS()    


/* util functions used in Kerner launcher */

int inline iDivUp(int a, int b){
  return (a + b - 1) / b;
}


  struct P3 {
   CCTK_REAL x, y, z;
   inline P3(CCTK_REAL _x, CCTK_REAL _y, CCTK_REAL _z):x(_x), y(_y), z(_z){}
   inline P3():x(0.0f), y(0.0f), z(0.0f){}
 };
 typedef CCTK_REAL P;

#ifndef T3Dx
# define T3Dx(z, y, x) (((z) * DIMY + (y)) * DIMX + (x))
# define T3Dy(z, y, x) (T3Dx(z, y, x) + (DIMX * DIMY * DIMZ))
# define T3Dz(z, y, x) (T3Dx(z, y, x) + (DIMX * DIMY * DIMZ * 2))
#endif

#define T3Dx_val(ptr, z, y, x, pitchx, pitchy)             (((__typeof__(ptr))((char *)(ptr) + (z) * (pitchy)  + (y) * (pitchx)))[(x)])
#define T3Dy_val(ptr, z, y, x, pitchx, pitchy, next_table) (__typeof__(ptr)(((char *)&T3Dx_val(ptr, z, y, x, pitchx, pitchy)) + (next_table)))[0]
#define T3Dz_val(ptr, z, y, x, pitchx, pitchy, next_table) (__typeof__(ptr)(((char *)&T3Dx_val(ptr, z, y, x, pitchx, pitchy)) + (next_table) * 2))[0]



#endif                          /* %{name_upper} */
