#include "CaKernel.h" 

#include<algorithm>
#include<omp.h>

%evaluate(macros)

extern "C"
void CAKERNEL_Launch_%{path_lower_parent}_%{name}(CCTK_ARGUMENTS)
{
  /// ... following line is required because of using *.a archives for thorns,
  ///     the variables are not initializied at all, unless used somewhere...
  %{thornname}::%{path_parent_lower}_ignoreme+=1;

  DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;

    CHECK_THORN_INIT

    size_t datasize = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2];

    Kernel_Launch_Parameters prms(cctk_iteration, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
    cctk_nghostzones[0], cctk_nghostzones[1], cctk_nghostzones[2],
    cctk_delta_space[0], cctk_delta_space[1], cctk_delta_space[2], cctk_delta_time,
    cctk_origin_space[0], cctk_origin_space[1], cctk_origin_space[2], cctk_time);

    int exterior[6] = {0};
    for(int i = 0; i < 6; ++i)
      exterior[i] = !::exterior[i] ? cctk_nghostzones[i / 2] : stencil[i];

    #pragma omp parallel 
    {
      #pragma omp for collapse(2)
      for(int k = exterior[4]; k < cctk_lsh[2] - exterior[5]; ++k)
      for(int j = exterior[2]; j < cctk_lsh[1] - exterior[3]; ++j)
      for(int i = exterior[0]; i < cctk_lsh[0] - exterior[1]; ++i){
        CAKERNEL_%{path_lower_parent}_%{name}(%macro(run1_pass_args) i, j, k, prms);
      }
    }
}
