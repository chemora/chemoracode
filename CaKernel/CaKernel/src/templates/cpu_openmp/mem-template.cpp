/* Assume Piraha will generate this file and this file will be pushed here as well */

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "CaKernel.h"


#include<stdlib.h>

%evaluate(macros)

%var_loop_if(%['%gvar("%vname", "type")'=='GF' && '%varAttr("%vname","intent")'=='separateinout']%)%[
CCTK_REAL * %{vname}_out; ]%

void %{thornname}_%{path_parent_lower}_InitDevice (CCTK_ARGUMENTS){
}

void %{thornname}_%{path_parent_lower}_AllocDevMem (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  const size_t dataSize = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2] ;

  %var_loop_if(%['%gvar("%vname", "type")'=='GF' && '%varAttr("%vname","intent")'=='separateinout']%) 
  %[ %ifelse("%gvar('%vname', isVector)", %[
   %{vname}_out = (CCTK_REAL *) malloc(dataSize * sizeof(*%{vname}) * (%gvar("%vname", vectorExpression)));]%, %[
   %{vname}_out = (CCTK_REAL *) malloc(dataSize * sizeof(*%{vname}));]%)]% 

  CCTK_INFO ("device memory for CaCUDA variables successfully allocated");
}


int %{thornname}_%{path_parent_lower}_FreeDevMem ()
{
  %var_loop_if(%['%gvar("%vname", "type")'=='GF' && '%varAttr("%vname","intent")'=='separateinout']%)%[
  free(%{vname}_out);]%

  CCTK_INFO ("device memory for CaCUDA variables successfully freed");
}

%macro(schedule_in_init)

