#ifndef %{name_upper}
#define %{name_upper}

/* definition of CCTK_REAL */
#include "cctk.h"


/* device pointers */
/* All the external variables will be declared here.
 * We will generate the this file from the parser directly but we
 * need to generate the memory allocation routines as well.
 */

%var_loop_if(%['%gvar("%vname", "type")'=='GF' && '%varAttr("%vname","intent")'=='separateinout']%)%[
extern CCTK_REAL * %{vname}_out; ]%

namespace %{thornname} {
  extern int %{path_parent_lower}_ignoreme; 
}

#endif /* %{name_upper} */
