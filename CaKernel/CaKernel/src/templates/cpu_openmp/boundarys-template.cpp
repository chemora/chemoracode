#include <assert.h>
#include "CaKernel.h" 

extern "C" 
int CaKernel_GetBoundaryInfo(const void * cctkGH_, const int * cctk_lsh, /* const int * cctk_lssh, */
                             const int * cctk_bbox, const int * cctk_nghostzones, int * imin,
                             int * imax, int * is_symbnd, int * is_physbnd, int * is_ipbnd);

%evaluate(macros)


extern "C"
void CAKERNEL_Launch_%{path_lower_parent}_%{name}(CCTK_ARGUMENTS)
{
  /// ... following line is required because of using *.a archives for thorns,
  ///     the variables are not initializied at all, unless used somewhere...
  %{thornname}::%{path_parent_lower}_ignoreme+=1;

  DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;
  
  CHECK_THORN_INIT

    Kernel_Launch_Parameters prms(cctk_iteration, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
    cctk_nghostzones[0], cctk_nghostzones[1], cctk_nghostzones[2],
    cctk_delta_space[0], cctk_delta_space[1], cctk_delta_space[2], cctk_delta_time,
    cctk_origin_space[0], cctk_origin_space[1], cctk_origin_space[2], cctk_time);

    int imin[3], imax[3], is_symbnd[6],                                      
        is_physbnd[6], is_ipbnd[6];                                          
                                                                             
    assert(!CaKernel_GetBoundaryInfo(cctkGH, cctk_lsh, /*cctk_lssh,*/ cctk_bbox, 
                                     cctk_nghostzones, imin, imax, is_symbnd,
                                     is_physbnd, is_ipbnd));                 
   

    #pragma omp parallel for schedule(dynamic)
    for(int v = 1; v < 27; v++){
      const int b[] ={(v + 1) % 3 - 1,
                      (v / 3 + 1) % 3 - 1, 
                      (v / 9 + 1) % 3 - 1};  
      if((!b[0] || is_physbnd[(b[0] + 1)/2]) &&
         (!b[1] || is_physbnd[(b[1] + 1)/2 + 2]) &&
         (!b[2] || is_physbnd[(b[2] + 1)/2 + 4])){
        int imin2[3] = {imin[0], imin[1], imin[2]}, 
            imax2[3] = {imax[0], imax[1], imax[2]};
        for(int dir = 0; dir < 3; dir++){
          if(b[dir] < 0){
            imin2[dir] = 0;
            imax2[dir] = imin[dir];
          }else if(b[dir] > 0){
            imin2[dir] = imax[dir];
            imax2[dir] = cctk_lsh[dir];
          }
        }
//        printf("b={%d, %d, %d} mins={%d, %d, %d} maxs={%d, %d, %d}\n", 
//                b[0],b[1],b[2], imin2[0],imin2[1],imin2[2], imax2[0],imax2[1],imax2[2])
        for(int k = imin2[2]; k < imax2[2]; k++)
        for(int j = imin2[1]; j < imax2[1]; j++)
        for(int i = imin2[0]; i < imax2[0]; i++){
          CAKERNEL_%{path_lower_parent}_%{name}(b[0], b[1], b[2], %macro(run1_pass_args) i, j, k, prms);
        }
      }
    }
}

