#include <CL/cl.h>
#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "CaKernel.h"
const char*
cl_error_str(cl_int code)
{
  switch ( code ) {
  case CL_SUCCESS: return "SUCCESS";
  case CL_DEVICE_NOT_FOUND: return "DEVICE NOT FOUND";
  case CL_DEVICE_NOT_AVAILABLE: return "DEVICE NOT AVAILABLE";
  case CL_COMPILER_NOT_AVAILABLE: return "COMPILER NOT AVAILABLE";
  case CL_MEM_OBJECT_ALLOCATION_FAILURE: return "MEM OBJECT ALLOCATION FAILURE";
  case CL_OUT_OF_RESOURCES: return "OUT OF RESOURCES";
  case CL_OUT_OF_HOST_MEMORY: return "OUT OF HOST MEMORY";
  case CL_PROFILING_INFO_NOT_AVAILABLE: return "PROFILING INFO NOT AVAILABLE";
  case CL_MEM_COPY_OVERLAP: return "MEM COPY OVERLAP";
  case CL_IMAGE_FORMAT_MISMATCH: return "IMAGE FORMAT MISMATCH";
  case CL_IMAGE_FORMAT_NOT_SUPPORTED: return "IMAGE FORMAT NOT SUPPORTED";
  case CL_BUILD_PROGRAM_FAILURE: return "BUILD PROGRAM FAILURE";
  case CL_MAP_FAILURE: return "MAP FAILURE";
  case CL_MISALIGNED_SUB_BUFFER_OFFSET: return "MISALIGNED SUB BUFFER OFFSET";
  case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST: return "EXEC STATUS ERROR FOR EVENTS IN WAIT LIST";
  case CL_INVALID_VALUE: return "INVALID VALUE";
  case CL_INVALID_DEVICE_TYPE: return "INVALID DEVICE TYPE";
  case CL_INVALID_PLATFORM: return "INVALID PLATFORM";
  case CL_INVALID_DEVICE: return "INVALID DEVICE";
  case CL_INVALID_CONTEXT: return "INVALID CONTEXT";
  case CL_INVALID_QUEUE_PROPERTIES: return "INVALID QUEUE PROPERTIES";
  case CL_INVALID_COMMAND_QUEUE: return "INVALID COMMAND QUEUE";
  case CL_INVALID_HOST_PTR: return "INVALID HOST PTR";
  case CL_INVALID_MEM_OBJECT: return "INVALID MEM OBJECT";
  case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR: return "INVALID IMAGE FORMAT DESCRIPTOR";
  case CL_INVALID_IMAGE_SIZE: return "INVALID IMAGE SIZE";
  case CL_INVALID_SAMPLER: return "INVALID SAMPLER";
  case CL_INVALID_BINARY: return "INVALID BINARY";
  case CL_INVALID_BUILD_OPTIONS: return "INVALID BUILD OPTIONS";
  case CL_INVALID_PROGRAM: return "INVALID PROGRAM";
  case CL_INVALID_PROGRAM_EXECUTABLE: return "INVALID PROGRAM EXECUTABLE";
  case CL_INVALID_KERNEL_NAME: return "INVALID KERNEL NAME";
  case CL_INVALID_KERNEL_DEFINITION: return "INVALID KERNEL DEFINITION";
  case CL_INVALID_KERNEL: return "INVALID KERNEL";
  case CL_INVALID_ARG_INDEX: return "INVALID ARG INDEX";
  case CL_INVALID_ARG_VALUE: return "INVALID ARG VALUE";
  case CL_INVALID_ARG_SIZE: return "INVALID ARG SIZE";
  case CL_INVALID_KERNEL_ARGS: return "INVALID KERNEL ARGS";
  case CL_INVALID_WORK_DIMENSION: return "INVALID WORK DIMENSION";
  case CL_INVALID_WORK_GROUP_SIZE: return "INVALID WORK GROUP SIZE";
  case CL_INVALID_WORK_ITEM_SIZE: return "INVALID WORK ITEM SIZE";
  case CL_INVALID_GLOBAL_OFFSET: return "INVALID GLOBAL OFFSET";
  case CL_INVALID_EVENT_WAIT_LIST: return "INVALID EVENT WAIT LIST";
  case CL_INVALID_EVENT: return "INVALID EVENT";
  case CL_INVALID_OPERATION: return "INVALID OPERATION";
  case CL_INVALID_GL_OBJECT: return "INVALID GL OBJECT";
  case CL_INVALID_BUFFER_SIZE: return "INVALID BUFFER SIZE";
  case CL_INVALID_MIP_LEVEL: return "INVALID MIP LEVEL";
  case CL_INVALID_GLOBAL_WORK_SIZE: return "INVALID GLOBAL WORK SIZE";
  default: return "Unknown code.";
  }
}

size_t roundUp(int global_size, int group_size)
{
  int r = global_size % group_size;
  if(r == 0) {
    return global_size;
  } else {
    return global_size + group_size - r;
  }
}



void clMemset(void * ptr, char c, size_t size){

  static cl_program program = 0;
  static cl_kernel kernel = 0;
  char log[10000];
  size_t ret_size;
  cl_uint ret_val;
  int err = 0;

  if(!program){
    program = clCreateProgramWithSource(CLContext, 1,  &CAKERNEL_%{name}_code_str, 0, &err); 
    CL_CHECK(err);
    if(!program) CL_CHECK(1);
    std::string options;
    if(sizeof(CCTK_REAL) == 8) options =" -D CCTK_REAL=double ";
    else                       options =" -D CCTK_REAL=float "; 
    err = clBuildProgram(program, 1, &CLDevice, options.c_str(), 0, 0);
    if(err){
      err = clGetProgramBuildInfo(program, CLDevice, CL_PROGRAM_BUILD_LOG, 10000, (void *)log, &ret_size);
      fprintf(stderr, "After build failed, log(%lu) is:\n%s\n", ret_size, ret_size ? log : "");
    }          
    kernel = clCreateKernel(program, "memset0_32", &err);  
    CL_CHECK(err);
  }
  
  int arg = 0;
  char aLaInt[] = {c, c, c, c};
  int to = iDivUp(size, sizeof(int));
  CL_CHECK(clGetKernelInfo(kernel, CL_KERNEL_NUM_ARGS, sizeof(ret_val), &ret_val, &ret_size));
  CL_CHECK(clSetKernelArg(kernel, arg++, sizeof(cl_mem), &ptr));
  CL_CHECK(clSetKernelArg(kernel, arg++, sizeof(int), &aLaInt));
  CL_CHECK(clSetKernelArg(kernel, arg++, sizeof(int), &to));
  
  if(ret_val != arg) {
    CCTK_VInfo("CCTK_THORNSTRING", "wrong number of kernel arguments: %d, should be: %lu", arg, ret_val);
    exit(-1);
  }
  
  size_t localWorkSize[] = { 128 };
  size_t globalWorkSize[] = { roundUp(to, localWorkSize[0]) };

  CL_CHECK(clEnqueueNDRangeKernel(CLQueue, kernel, 1, 0, globalWorkSize, localWorkSize, 0, 0, 0));  
}
