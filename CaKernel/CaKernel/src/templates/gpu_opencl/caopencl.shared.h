
#ifndef CAOPENCL_SHARED_H_
#define CAOPENCL_SHARED_H_

#include"cctk.h"
#include<CL/cl.h>
#include<stdio.h>

#include "CaKernel.h"
/* Return string corresponding to OpenCL error code. */
const char* cl_error_str(cl_int code);

size_t roundUp(int global_size, int group_size);

void clMemset(void * ptr, char c, size_t);

inline void cl_check(cl_int err, int line, const char * file){
  if(err){
    fprintf
      (stderr,
       "Failed while executing cl function... "
       "[FILE: %s; LINE: %d; ERROR: %d %s]\n",
       file, line, err, cl_error_str(err));
    exit(-1);
  }
}

%include($defs)

#define __local

#define CL_CHECK(X) cl_check(X, __LINE__, __FILE__)


int inline iDivUp(int a, int b){
    return (a + b - 1) / b;
}


#endif /*CAOPENCL_SHARED_H_*/
