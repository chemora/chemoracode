#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include <string>
#include <algorithm>
#include <CL/cl.h>
#include <stdio.h>
#include "CaKernel.h" 

extern "C"
void CAKERNEL_Launch_%{name}(CCTK_ARGUMENTS)
{
	DECLARE_CCTK_ARGUMENTS;

    CHECK_THORN_INIT

    static cl_program program = 0;
    static cl_kernel kernel = 0;
    char log[100000];
    size_t ret_size;
    cl_uint ret_val;
    int err = 0;
    size_t datasize = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2] * sizeof(CCTK_REAL);

    if(!program){
      program = clCreateProgramWithSource(CLContext, 1, &CAKERNEL_%{name}_code_str, 0, &err); 
      CL_CHECK(err);
      if(!program) CL_CHECK(1);
      std::string options = CAOPENCL_COMPILE_OPTS; 

      if(sizeof(CCTK_REAL) == 8) options +=" -D CCTK_REAL=double ";
      else                       options +=" -D CCTK_REAL=float "; 
      err = clBuildProgram(program, 1, &CLDevice, options.c_str(), 0, 0);
      if(err){
        fprintf(stderr, "Failed bulding program. Error num: %d\n", err);
        CL_CHECK(clGetProgramBuildInfo(program, CLDevice, CL_PROGRAM_BUILD_LOG, sizeof(log), (void *)log, &ret_size));
        fprintf(stderr, "After build failed, log(%lu) is:\n%s\n", ret_size, ret_size ? log : "");
        CL_CHECK(err);
      }          
      kernel = clCreateKernel(program, "CAKERNEL_%{name}", &err);  
      CL_CHECK(err);
    }

    %var_loop("intent=separateinout",'clMemset(dcl_%{vname}_out, 0, datasize);\n');

    CaOpenCL_Kernel_Launch_Parameters prms(cctk_iteration,
    		cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
    		cctk_nghostzones[0], cctk_nghostzones[1], cctk_nghostzones[2],
        cctk_delta_space[0], cctk_delta_space[1], cctk_delta_space[2],
        cctk_delta_time,
        cctk_origin_space[0], cctk_origin_space[1], cctk_origin_space[2],
        cctk_time);

    int arg = 0;
    CL_CHECK(clGetKernelInfo(kernel, CL_KERNEL_NUM_ARGS, sizeof(ret_val), &ret_val, &ret_size)); 

    %var_loop("intent=separateinout",'CL_CHECK(clSetKernelArg(kernel, arg++, sizeof(cl_mem), &dcl_%{vname}));CL_CHECK(clSetKernelArg(kernel, arg++, sizeof(cl_mem), &dcl_%{vname}_out));')
    %var_loop("intent=inout",'CL_CHECK(clSetKernelArg(kernel, arg++, sizeof(cl_mem), &dcl_%{vname}));')
    %var_loop("intent=in",'CL_CHECK(clSetKernelArg(kernel, arg++, sizeof(cl_mem), &dcl_%{vname}));')
    %var_loop("intent=out",'CL_CHECK(clSetKernelArg(kernel, arg++, sizeof(cl_mem), &dcl_%{vname}));')
    CL_CHECK(clSetKernelArg(kernel, arg++, sizeof(prms), &prms));
    
    if(ret_val != arg) {
      CCTK_VInfo("CCTK_THORNSTRING", "wrong number of kernel arguments: %d, should be: %lu", arg, ret_val);
      exit(-1);
    }
   
    size_t localWorkSize[] = {CAKERNEL_Threadsx, CAKERNEL_Threadsy, CAKERNEL_Threadsz};
    size_t globalWorkSize[] = 
    { iDivUp(prms.cagh_ni - stncl_xn - stncl_xp, CAKERNEL_Tilex - stncl_xn - stncl_xp) * localWorkSize[0],
      iDivUp(prms.cagh_nj - stncl_yn - stncl_yp, CAKERNEL_Tiley - stncl_yn - stncl_yp) * localWorkSize[1],
      iDivUp(prms.cagh_nk - stncl_zn - stncl_zp, CAKERNEL_Tilez - stncl_zn - stncl_zp) * localWorkSize[2] };

    CL_CHECK(clEnqueueNDRangeKernel(CLQueue, kernel, 3, 0, globalWorkSize, localWorkSize, 0, 0, 0));  
   
    SYNC_CALLS();
    
    %var_loop("intent=separateinout",'std::swap(dcl_%{vname}, dcl_%{vname}_out);\n')

}
