/* Assume Piraha will generate this file and this file will be pushed here as well */

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include <CL/cl.h>
%evaluate(macros)

void CaKernel_CopyToDevVars(CCTK_ARGUMENTS)
{
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' != 'GF' ]%) > 0 ]%, %[
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  size_t elements[%macro(num_vars_sepinout) + 1] = {
    %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)

    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)

    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)
      0
    };
  char * tmpptr = (char *) malloc(dcl_vars.all);
  %set(tmpi, 0)
  %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    { 
      %gvar("%vname", CCTK_TYPE) * ptr = (%gvar("%vname", CCTK_TYPE) *) &tmpptr[vars.%{vname}_offset];
      for(int i = 0; i < elements[%inc(tmpi)]; i++)
        ptr[i] = %vname[i]; 
    } ]%)

  %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    { 
      %gvar("%vname", CCTK_TYPE) * ptr = (%gvar("%vname", CCTK_TYPE) *) &tmpptr[vars.%{vname}_offset];
      for(int i = 0; i < elements[%inc(tmpi)]; i++)
        ptr[i] = %vname[i]; 
    } ]%)

  %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    { 
      %gvar("%vname", CCTK_TYPE) * ptr = (%gvar("%vname", CCTK_TYPE) *) &tmpptr[vars.%{vname}_offset];
      for(int i = 0; i < elements[%inc(tmpi)]; i++)
        ptr[i] = %vname[i]; 
    } ]%)

  CL_CHECK(clEnqueueWriteBuffer(CLQueue, dcl_vars.ptr, true, 0, dcl_vars.all, tmpptr, 0, 0, 0));
  free(tmpptr);
  ]%)
}

void CaKernel_CopyFromDevVars(CCTK_ARGUMENTS)
{
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' != 'GF' ]%) > 0 ]%, %[
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  size_t elements[%macro(num_vars_sepinout) + 1] = {
    %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)

    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)

    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)
      0
  };
  char * tmpptr = (char *) malloc(dcl_vars.all);
  CL_CHECK(clEnqueueReadBuffer (CLQueue, dcl_vars.ptr, true, 0, dcl_vars.all, tmpptr, 0, 0, 0));
  %set(tmpi, 0)
  %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    { 
      %gvar("%vname", CCTK_TYPE) * ptr = (%gvar("%vname", CCTK_TYPE) *) &tmpptr[vars.%{vname}_offset];
      for(int i = 0; i < elements[%inc(tmpi)]; i++)
        %vname[i] = ptr[i]; 
    } ]%)

  %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    { 
      %gvar("%vname", CCTK_TYPE) * ptr = (%gvar("%vname", CCTK_TYPE) *) &tmpptr[vars.%{vname}_offset];
      for(int i = 0; i < elements[%inc(tmpi)]; i++)
        %vname[i] = ptr[i]; 
    } ]%)

  %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    { 
      %gvar("%vname", CCTK_TYPE) * ptr = (%gvar("%vname", CCTK_TYPE) *) &tmpptr[vars.%{vname}_offset];
      for(int i = 0; i < elements[%inc(tmpi)]; i++)
        %vname[i] = ptr[i]; 
    } ]%)
  free(tmpptr);
  ]% )
}
/* now we just copy everything back and forward for each time step */
/* we may schedule one communication for each kernel */
void CaKernel_CopyToDev(CCTK_ARGUMENTS)
{
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' == 'GF' ]%) > 0 ]%, %[
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  const size_t dataSize = cctkGH->cctk_lsh[0] * cctkGH->cctk_lsh[1] * cctkGH->cctk_lsh[2];
%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[ %ifelse( "%gvar('%vname', isVector)", %[ 
      CL_CHECK(clEnqueueWriteBuffer(CLQueue, dcl_%{vname}, true, 0, dataSize * sizeof(*%{vname}) * (%gvar("%vname", "vectorExpression")), %{vname}, 0, 0, 0));]%, %[
      CL_CHECK(clEnqueueWriteBuffer(CLQueue, dcl_%{vname}, true, 0, dataSize * sizeof(*%{vname}), %{vname}, 0, 0, 0));]%) ]%
  ]%)
  CaKernel_CopyToDevVars(CCTK_PASS_CTOC);
//  CCTK_INFO("CaCUDA variables %var_loop('delimit=,','%vname') have been successfully copied to device");
}

void CaKernel_CopyFromDev(CCTK_ARGUMENTS)
{
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' == 'GF' ]%) > 0 ]%, %[
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  const size_t dataSize = cctkGH->cctk_lsh[0] * cctkGH->cctk_lsh[1] * cctkGH->cctk_lsh[2];

%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[ %ifelse( "%gvar('%vname', isVector)", %[ 
      CL_CHECK(clEnqueueReadBuffer (CLQueue, dcl_%{vname}, true, 0, dataSize * sizeof(*%{vname}) * (%gvar("%vname", "vectorExpression")), %{vname}, 0, 0, 0));]%, %[
      CL_CHECK(clEnqueueReadBuffer (CLQueue, dcl_%{vname}, true, 0, dataSize * sizeof(*%{vname}), %{vname}, 0, 0, 0));]%) ]%
  ]%)
  CaKernel_CopyFromDevVars(CCTK_PASS_CTOC);
//  CCTK_INFO("CaCUDA variables %var_loop('delimit=,','%vname') have been successfully copied from device");
}

void CaKernel_CopyToDev_Boundary( CCTK_ARGUMENTS)
{
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' == 'GF' ]%) > 0 ]%, %[
  DECLARE_CCTK_ARGUMENTS;

  long szp = sizeof(CCTK_REAL);
 

  // I USED THAT ORDER in former code; I didn't change it: N, E, F, S, W, B
  int t[] = {3, 1, 5, 2, 0, 4};
  int ghost_zones[6] = {0};
  for (int i = 0; i< 6; i++){
    if(cctkGH->cctk_bbox[t[i]] != 1) ghost_zones[i] = cctk_nghostzones[t[i] / 2]; 
  }	
  
  long  dimxg = cctk_lsh[0],
        dimyg = cctk_lsh[1],
        dimzg = cctk_lsh[2];
  long  dimxl = dimxg - ghost_zones[1] - ghost_zones[4],
        dimyl = dimyg - ghost_zones[0] - ghost_zones[3],
        dimzl = dimzg - ghost_zones[2] - ghost_zones[5];

/////  variables pointing to places in the array from/where the ghost zone should be copied // N, E, F, S, W, B
/// The data copy is desigined to minimize the amount of data copied unaligned (E and W ghost-zones).

  // I USED THAT ORDER in former code; I didn't change it: N, E, F, S, W, B

//  printf ("Begin to dev copy\n");
 
  long dimx_out[] = {dimxg, ghost_zones[1], dimxg, dimxg, ghost_zones[4], dimxg};
  long dimy_out[] = {ghost_zones[0], dimyl, dimyl, ghost_zones[3], dimyl, dimyl};
  long dimz_out[] = {dimzg, dimzl, ghost_zones[2], dimzg, dimzl, ghost_zones[5]};

  long offx_out[] = {0, (dimxl + ghost_zones[4]), 0, 0, 0, 0};
  long offy_out[] = {dimyl + ghost_zones[3], ghost_zones[3], ghost_zones[3], 0, ghost_zones[3], ghost_zones[3]};
  long offz_out[] = {0, ghost_zones[5], ghost_zones[5] + dimzl, 0, ghost_zones[5], 0};

  long pitchx = dimxg; 
  long pitchy = pitchx * dimyg;

  size_t sizes[] = {%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[sizeof(*%{vname}), ]% 0};
  void * var[]   = {%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[%{vname}, ]% 0};
  int    vector_count[] = {%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[ %ifelse(
       %[%gvar( "%vname", isVector)]%, "(%gvar( \"%vname\", vectorExpression))", 1), ]% 0};
  cl_mem dcl_var[] = {%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[ dcl_%{vname}, ]% 0};
  char * names[] = {%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %["%{vname}", ]% 0};
  int vstride = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2];

  for(int k = 0; var[k] != 0; k++){
    for(int v = 0; v < vector_count[k]; v++){
      for(int i = 0; i < 6; i++) {
        if(cctkGH->cctk_bbox[t[i]] != 1 && dimx_out[i] > 0 && dimy_out[i] > 0 && dimz_out[i] > 0){
          size_t origin[] = { offx_out[i] * sizes[k] + v * vstride, offy_out[i], offz_out[i] };  
          size_t region[] = { dimx_out[i] * sizes[k], dimy_out[i], dimz_out[i] };  
  //	printf("Copying var[%ld], side[%ld], origin[%ld, %ld, %ld], region[%ld, %ld, %ld], pitch[%ld, %ld]\n",
  //               origin[0], origin[1], origin[2], region[0], region[1], region[2], pitchx, pitchy);
          clEnqueueWriteBufferRect(CLQueue, dcl_var[k], CL_TRUE, origin, origin, region, 
              pitchx * sizes[k], pitchy, pitchx * sizes[k], pitchy, var[k], 0, 0, 0); 	
        }
      }
    }
  }
  ]%)
  CaKernel_CopyToDevVars(CCTK_PASS_CTOC);
}


void CaKernel_CopyFromDev_Boundary(CCTK_ARGUMENTS)
{
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' == 'GF' ]%) > 0 ]%, %[
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
//  long szp = sizeof(CCTK_REAL);

  // I USED THAT ORDER in former code; I didn't change it: N, E, F, S, W, B
  int t[] = {3, 1, 5, 2, 0, 4};
  int ghost_zones[6] = {0};
  for (int i = 0; i< 6; i++){
    if(cctkGH->cctk_bbox[t[i]] != 1) ghost_zones[i] = cctk_nghostzones[t[i] / 2]; 
  }	

  long  dimxg = cctk_lsh[0],
        dimyg = cctk_lsh[1],
        dimzg = cctk_lsh[2];
  long  dimxl = dimxg - ghost_zones[1] - ghost_zones[4],
        dimyl = dimyg - ghost_zones[0] - ghost_zones[3],
        dimzl = dimzg - ghost_zones[2] - ghost_zones[5];

/// local dimension without inner ghost-zone
  long //dimxn = dimxl - ghost_zones[1] - ghost_zones[0],
       dimyn = dimyl -  ghost_zones[0] - ghost_zones[3],
       dimzn = dimzl -  ghost_zones[2] - ghost_zones[5];

///  variables pointing to places in the array from/where the ghost zone should be copied // N, E, F, S, W, B
/// The data copy is desigined to minimize the amount of data copied unaligned (E and W ghost-zones).

  // I USED THAT ORDER in former code; I didn't change it: N, E, F, S, W, B
 
  long dimx_in[]  = {dimxl, ghost_zones[1], dimxl, dimxl, ghost_zones[4], dimxl};
  long dimy_in[]  = {ghost_zones[0], dimyn, dimyn, ghost_zones[3], dimyn, dimyn};
  long dimz_in[]  = {dimzl, dimzn, ghost_zones[2], dimzl, dimzn, ghost_zones[5]};

  long offx_in[] = {ghost_zones[4], (ghost_zones[4] + dimxl - ghost_zones[1]), ghost_zones[4], ghost_zones[4], ghost_zones[4], ghost_zones[4]};
  long offy_in[] = {dimyl + ghost_zones[3] - ghost_zones[0], ghost_zones[3] + ghost_zones[3], ghost_zones[3] + ghost_zones[3], ghost_zones[3], ghost_zones[3] + ghost_zones[3], ghost_zones[3] + ghost_zones[3]};
  long offz_in[] = {ghost_zones[5], ghost_zones[5] + ghost_zones[5], ghost_zones[5] + dimzl - ghost_zones[2], ghost_zones[5], ghost_zones[5] + ghost_zones[5], ghost_zones[5]};

  long pitchx = dimxg; 
  long pitchy = pitchx * dimyg;

  size_t sizes[] = {%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[sizeof(*%{vname}), ]% 0};
  void * var[]   = {%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[%{vname}, ]% 0};
  int    vector_count[] = {%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[ %ifelse(
       %[%gvar( "%vname", isVector)]%, "(%gvar( \"%vname\", vectorExpression))", 1), ]% 0};
  cl_mem  dcl_var[] = {%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[ dcl_%{vname}, ]% 0};
  char * names[] = {%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %["%{vname}", ]% 0};
  int vstride = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2];

  for(int k = 0; var[k] != 0; k++){
    for(int v = 0; v < vector_count[k]; v++){
      for(int i = 0; i < 6; i++) {
        if(cctkGH->cctk_bbox[t[i]] != 1 && dimx_in[i] > 0 && dimy_in[i] > 0 && dimz_in[i] > 0){
          size_t origin[] = { offx_in[i] * sizes[k] + v * vstride, offy_in[i], offz_in[i] };  
          size_t region[] = { dimx_in[i] * sizes[k], dimy_in[i], dimz_in[i] };  
  //	CCTK_VInfo(CCTK_THORNSTRING, "Copying var[%ld], side[%ld], origin[%ld, %ld, %ld], region[%ld, %ld, %ld], pitch[%ld, %ld]\n",
  //              k, i, origin[0], origin[1], origin[2], region[0], region[1], region[2], pitchx, pitchy);
          cl_event event;
          clEnqueueReadBufferRect(CLQueue, dcl_var[k], CL_TRUE, origin, origin, region, 
              pitchx * sizes[k], pitchy, pitchx * sizes[k], pitchy, var[k], 0, 0, &event); 	
  //	CCTK_VInfo(CCTK_THORNSTRING, "After copying var[%ld], side[%ld], origin[%ld, %ld, %ld], region[%ld, %ld, %ld], pitch[%ld, %ld]\n",
  //              k, i, origin[0], origin[1], origin[2], region[0], region[1], region[2], pitchx, pitchy);
        }
      }
    }
  }
  ]%)
  CaKernel_CopyFromDevVars(CCTK_PASS_CTOC);
}


