%t%[
%evaluate(macros_group)

%set_macro(run1_declare_args, %[                                              \
%var_loop_if(%['%varAttr("%vname",intent)'=='separateinout' && '%gvar("%vname",type)'=='GF']%,'__global const %gvar("%vname",CCTK_TYPE) * %{vname}, __global %gvar("%vname",CCTK_TYPE) * %{vname}_out,')\
%var_loop_if(%['%varAttr("%vname",intent)'!='separateinout' && '%gvar("%vname",type)'=='GF']%,'__global %gvar("%vname",CCTK_TYPE) * %{vname},')\
%ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' != 'GF' ]%) > 0 ]%, %[__global char * vars_ptr, ]%)\
%var_loop_if(%['%varAttr("%vname",intent)'=='separateinout' && '%gvar("%vname",type)'!='GF']%,'const unsigned int %{vname}_offset, const unsigned int %{vname}_out_offset, ')\
%var_loop_if(%['%varAttr("%vname",intent)'!='separateinout' && '%gvar("%vname",type)'!='GF']%,'const unsigned int %{vname}_offset, ')\
%ifthen(%[ %par_loop_ifno(true) > 0 ]%, %[const __global char * pars_ptr, ]%) \
%par_loop('const unsigned int %{vname}_offset, ')                             \
]%)

]%
