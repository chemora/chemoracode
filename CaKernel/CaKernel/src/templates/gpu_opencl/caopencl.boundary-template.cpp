#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include <string>
#include <algorithm>
#include <CL/cl.h>
#include <stdio.h>
#include "CaKernel.h" 

extern "C"
void CAKERNEL_Launch_%{name}(CCTK_ARGUMENTS)
{
    DECLARE_CCTK_ARGUMENTS;

    CHECK_THORN_INIT

    static cl_program * programs = 0;
    static cl_kernel * kernels = 0;
    char log[10000];
    char options[10000] = {0};
    size_t ret_size;
    cl_uint ret_val;
    int err = 0;
    size_t datasize = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2] * sizeof(CCTK_REAL);
    int i = 0;
    if(!kernels){
      programs = (cl_program *)malloc (6 * sizeof(cl_program));
      kernels = (cl_kernel *)malloc (6 * sizeof(cl_kernel));
      for (int b = 0x1; b < 0x1 << 6; b<<=1)
      {
        int x = -(b & 0x1) + ((b >> 0x1) & 0x1);
        int y = -((b >> 0x2) & 0x1) + ((b >> 0x3) & 0x1);
        int z = -((b >> 0x4) & 0x1) + ((b >> 0x5) & 0x1);
        programs[i] = clCreateProgramWithSource(CLContext, 1, &CAKERNEL_%{name}_code_str, 0, &err); 
        CL_CHECK(err);
        if(!programs[i]) CL_CHECK(1);

        if(sizeof(CCTK_REAL) == 8) ret_size = sprintf(options, " -D CCTK_REAL=double ");
        else                       ret_size = sprintf(options, " -D CCTK_REAL=float "); 
        sprintf(&options[ret_size], " %s -D x=%d -D y=%d -D z=%d ", CAOPENCL_COMPILE_OPTS, x, y, z);
        //fprintf(stderr, "Building boundary program, opts: %s\n", options);
        err = clBuildProgram(programs[i], 0, 0, options, 0, 0);
        if(err){
          err = clGetProgramBuildInfo(programs[i], CLDevice, CL_PROGRAM_BUILD_LOG, 10000, (void *)log, &ret_size);
          fprintf(stderr, "After build failed, log(%lu) is:\n%s\n", ret_size, ret_size ? log : "");
          CL_CHECK(err);
        }          
        kernels[i] = clCreateKernel(programs[i], "CAKERNEL_%{name}", &err);  
        CL_CHECK(err);
        i++;
      }
    }

    %var_loop("intent=separateinout",'clMemset(dcl_%{vname}_out, 0, datasize);\n');
    CaOpenCL_Kernel_Launch_Parameters prms(cctk_iteration,
    		cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
    		cctk_nghostzones[0], cctk_nghostzones[1], cctk_nghostzones[2],
        cctk_delta_space[0], cctk_delta_space[1], cctk_delta_space[2],
        cctk_delta_time,
        cctk_origin_space[0], cctk_origin_space[1], cctk_origin_space[2],
        cctk_time);

    size_t localWorkSizes[][3] = { 
          {CAKERNEL_Tiley, CAKERNEL_Tilez, 1}, {CAKERNEL_Tiley, CAKERNEL_Tilez, 1},
          {CAKERNEL_Tilex, CAKERNEL_Tilez, 1}, {CAKERNEL_Tilex, CAKERNEL_Tilez, 1},
          {CAKERNEL_Tilex, CAKERNEL_Tiley, 1}, {CAKERNEL_Tilex, CAKERNEL_Tiley, 1} };
    size_t globalWorkSizes[][3] ={ 
          {roundUp(prms.cagh_nj, CAKERNEL_Tiley), roundUp(prms.cagh_nk, CAKERNEL_Tilez), 1},
          {roundUp(prms.cagh_nj, CAKERNEL_Tiley), roundUp(prms.cagh_nk, CAKERNEL_Tilez), 1},
          {roundUp(prms.cagh_ni, CAKERNEL_Tilex), roundUp(prms.cagh_nk, CAKERNEL_Tilez), 1},
          {roundUp(prms.cagh_ni, CAKERNEL_Tilex), roundUp(prms.cagh_nk, CAKERNEL_Tilez), 1},
          {roundUp(prms.cagh_ni, CAKERNEL_Tilex), roundUp(prms.cagh_nj, CAKERNEL_Tiley), 1},
          {roundUp(prms.cagh_ni, CAKERNEL_Tilex), roundUp(prms.cagh_nj, CAKERNEL_Tiley), 1} };

    for (int i = 0; i < 6; i++){ 
      if(cctkGH->cctk_bbox[i] == 1){
        int arg = 0;
        CL_CHECK(clGetKernelInfo(kernels[i], CL_KERNEL_NUM_ARGS, sizeof(ret_val), &ret_val, &ret_size)); 
        %var_loop("intent=separateinout",'CL_CHECK(clSetKernelArg(kernels[i], arg++, sizeof(cl_mem), &dcl_%{vname}));\nCL_CHECK(clSetKernelArg(kernels[i], arg++, sizeof(cl_mem), &dcl_%{vname}_out));\n')
        %var_loop("intent=inout",'CL_CHECK(clSetKernelArg(kernels[i], arg++, sizeof(cl_mem), &dcl_%{vname}));\n')
        %var_loop("intent=out",'CL_CHECK(clSetKernelArg(kernel[i], arg++, sizeof(cl_mem), &dcl_%{vname}));\n')
        CL_CHECK(clSetKernelArg(kernels[i], arg++, sizeof(prms), &prms));

        if(ret_val != arg) {
          CCTK_VInfo("CCTK_THORNSTRING", "wrong number of kernel arguments: %d, should be: %lu", arg, ret_val);
          exit(-1);
        }

//        fprintf(stderr, "Updating boundary[%d]: local={%d, %d, %d}, global={%d, %d, %d}\n", i, 
//                localWorkSizes[i][0], localWorkSizes[i][1], localWorkSizes[i][2], 
//                globalWorkSizes[i][0], globalWorkSizes[i][1], globalWorkSizes[i][2]);
        CL_CHECK(clEnqueueNDRangeKernel(CLQueue, kernels[i], 3, 0, globalWorkSizes[i], 
                            localWorkSizes[i], 0, 0, 0));  
        
        %var_loop("intent=separateinout",'std::swap(dcl_%{vname}, dcl_%{vname}_out);\n')
      }
    }
     
    SYNC_CALLS();
 
//    CUDA_SAFE_CALL(cudaThreadSynchronize());
    %var_loop("intent=separateinout",'std::swap(d_%{vname}, d_%{vname}_out);')
    
}

