/* Assume Piraha will generate this file and this file will be pushed here as well */

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include <CL/cl.h>

#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

%evaluate(macros)

cl_context CLContext = 0;
cl_command_queue CLQueue = 0;
cl_device_id CLDevice = 0;

%var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[
cl_mem dcl_%{vname};]%

%var_loop_if(%['%gvar("%vname", "type")'=='GF' && '%varAttr("%vname","intent")'=='separateinout']%)%[
cl_mem dcl_%{vname}_out; ]%

int wait = 1;

void signal_handler (int sig){
	wait = 0;	
}
 
Vars_cl dcl_vars;
Pars_cl dcl_pars;

void CaKernel_AllocDevVars(CCTK_ARGUMENTS)
{
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' != 'GF' ]%) > 0 ]%, %[
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  int numvars = %macro(num_vars_sepinout);
  size_t sizes[%macro(num_vars_sepinout) + 1] = {
    %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      sizeof (%gvar("%vname", CCTK_TYPE)), ]%
    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      sizeof (%gvar("%vname", CCTK_TYPE)), ]%
    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      sizeof (%gvar("%vname", CCTK_TYPE)), ]%
      16 // to make sure that the "structure" is aligned to 16 bytes
    };   

  size_t elements[%macro(num_vars_sepinout) + 1] = {
    %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)

    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)

    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)
      0
    };   

  int offsets[%macro(num_vars_sepinout) + 1];   
  
  offsets[0] = 0;
  for (int i = 1; i <= numvars; i++){
    int oldoffset = offsets[i - 1] + sizes[i - 1] * elements[i - 1];
    for(int b = 2; b <= 16 && b <= sizes[i]; b <<= 1)
      oldoffset += (b - (oldoffset % b)) % b;
    offsets[i] =  oldoffset;
  }
  dcl_vars.sepinout_out = dcl_vars.sepinout = dcl_vars.all = offsets[numvars];
  %set(tmpi, 0)
  %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    dcl_vars.%{vname}_offset = offsets[(int)%inc(tmpi)]; ]%)

  %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    dcl_vars.sepinout = min(offsets[(int) %var(tmpi)], dcl_vars.sepinout);
    dcl_vars.%{vname}_offset = offsets[(int)%inc(tmpi)]; ]%)

  %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    dcl_vars.sepinout_out = min(offsets[(int) %var(tmpi)], dcl_vars.sepinout);
    dcl_vars.%{vname}_offset = offsets[(int)%inc(tmpi)]; ]%)

//  d_vars = vars;
//  vars.ptr = (char *)malloc (offsets[numvars]);
  int err = 0;
  dcl_vars.ptr = clCreateBuffer(CLContext, CL_MEM_READ_WRITE, offsets[numvars], 0, &err);
  CL_CHECK(!(!err && dcl_vars.ptr)); 
  ]%)
}

void CaKernel_FreeDevVars(){
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' != 'GF' ]%) > 0 ]%, %[
//  free(vars.ptr);
  CL_CHECK (clReleaseMemObject(dcl_vars.ptr));
  ]%)
}

void CaKernel_AllocDevPars(CCTK_ARGUMENTS)
{
  %ifthen(%[ %par_loop_ifno(true) > 0 ]%, %[
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  int err=0;
  int numvars = %macro(num_pars);
  size_t sizes[%macro(num_pars) + 1] = {
    %par_loop() %[
      sizeof (%gpar("%vname", CCTK_TYPE)), ]%
      16 // to make sure that the "structure" is aligned to 16 bytes
    };   

  size_t elements[%macro(num_pars) + 1] = {
    %par_loop(%[ %macro(par_numelements), ]%)
      0
    };   

  int offsets[%macro(num_pars) + 1];   
  
  offsets[0] = 0;
  for (int i = 1; i <= numvars; i++){
    int oldoffset = offsets[i - 1] + sizes[i - 1] * elements[i - 1];
    for(int b = 2; b <= 16 && b <= sizes[i]; b <<= 1)
      oldoffset += (b - (oldoffset % b)) % b;
    offsets[i] =  oldoffset;
  } 
  %set(tmpi, 0)
  %par_loop(%[ 
    dcl_pars.%{vname}_offset = offsets[(int)%inc(tmpi)]; ]%)

//  d_pars = pars;
  char * tmpptr = (char *)malloc (offsets[numvars]);
  dcl_pars.ptr = clCreateBuffer(CLContext, CL_MEM_READ_WRITE, offsets[numvars], 0, &err);
  CL_CHECK(!(!err && dcl_pars.ptr)); 

  %set(tmpi, 0)
  %par_loop(%[ 
  { 
    %gpar("%vname", CCTK_TYPE) * ptr = (%gpar("%vname", CCTK_TYPE) *) &tmpptr[dcl_pars.%{vname}_offset];
    %ifelse("%gpar('%vname', isVector)",%[%com%[ 
]%for(int i = 0; i < elements[%inc(tmpi)]; i++)
      ptr[i] = %vname[i]; ]%, %[*ptr = %vname; %t%[%inc(tmpi)]% ]%)  
  } ]%)
  
  CL_CHECK(clEnqueueWriteBuffer(CLQueue, dcl_pars.ptr, true, 0, offsets[numvars], tmpptr, 0, 0, 0));
  free(tmpptr);
  ]%)
}

void CaKernel_FreeDevPars()
{
  %ifthen(%[ %par_loop_ifno(true) > 0 ]%, %[
  CL_CHECK (clReleaseMemObject(dcl_pars.ptr));
  ]%)
}



void CaKernel_AllocDevMem (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' == 'GF' ]%) > 0 ]%, %[
  const size_t dataSize = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2];
  int err;

  %var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[
  %ifelse("%gvar('%vname', isVector)", %[
  dcl_%{vname} = clCreateBuffer(CLContext, CL_MEM_READ_WRITE, dataSize * sizeof(*%{vname}) * (%gvar("%vname", vectorExpression)), 0, &err); ]%, %[
  dcl_%{vname} = clCreateBuffer(CLContext, CL_MEM_READ_WRITE, dataSize * sizeof(*%{vname}), 0, &err); ]%)
  CL_CHECK(!(!err && dcl_%{vname})); ]%

  %var_loop_if(%['%gvar("%vname", "type")'=='GF' && '%varAttr("%vname", "intent")'=='separateinout']%) 
  %[ %ifelse("%gvar('%vname', isVector)", %[
  dcl_%{vname}_out = clCreateBuffer(CLContext, CL_MEM_READ_WRITE, dataSize * sizeof(*%{vname}) * (%gvar("%vname", vectorExpression)), 0, &err); ]%, %[
  dcl_%{vname}_out = clCreateBuffer(CLContext, CL_MEM_READ_WRITE, dataSize * sizeof(*%{vname}), 0, &err); ]%)
  CL_CHECK(!(!err && dcl_%{vname}_out)); ]%
  CCTK_INFO ("Device memory for OpenCL variables successfully allocated");
  ]%)
  CaKernel_AllocDevPars(CCTK_PASS_CTOC);
  CaKernel_AllocDevVars(CCTK_PASS_CTOC);
}


int CaKernel_FreeDevMem() {
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' == 'GF' ]%) > 0 ]%, %[
  int err;
  %var_loop_if(%['%gvar("%vname", "type")'=='GF']%) %[
  CL_CHECK (clReleaseMemObject(dcl_%{vname}));]%

  %var_loop_if(%['%gvar("%vname", "type")'=='GF' && '%varAttr("%vname", "intent")'=='separateinout']%) %[
  CL_CHECK (clReleaseMemObject (dcl_%{vname}_out));]%

  CCTK_INFO ("device memory for CaCUDA variables successfully freed");
  ]%)
  CaKernel_FreeDevPars();
  CaKernel_FreeDevVars();
}


void CaKernel_InitDevice(CCTK_ARGUMENTS){
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int err;
  cl_uint num_entries; 
  cl_platform_id *platforms = 0; 
  cl_uint num_platforms; 
  cl_device_id * devices = 0;
  cl_uint device_nm = 0;
  CL_CHECK(clGetPlatformIDs (0, 0, &num_platforms));
//  printf("There are %d platforms in the system\n", num_platforms);
  for(int i = 0; i < num_platforms; i++){
      if (!platforms){
        CL_CHECK(clGetPlatformIDs (num_platforms, platforms = (cl_platform_id *) 
          malloc(sizeof(cl_platform_id) * num_platforms), &num_platforms));
    }
      
	  devices = (cl_device_id *) malloc(sizeof(cl_device_id) * 100);
	  clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_GPU, 100, devices, &(device_nm = 0));
//	  CL_CHECK(clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_GPU, 0, 0, &device_nm));
	  if(device_nm <= 0) continue;
    
	  CLContext = clCreateContext(0, device_nm, devices, 0, 0, &err);
    CL_CHECK(err); if(!CLContext) CL_CHECK(1);
	  break;
  }
  if(!CLContext) {
		fprintf(stderr, "No OpenCL processing devices found, quiting...\n");
		exit(-1);
	}
  int myproc = CCTK_MyProc(cctkGH);
  int devidx = myproc % device_nm;
  CLDevice = devices[devidx];

	CLQueue = clCreateCommandQueue(CLContext, CLDevice, CL_QUEUE_PROFILING_ENABLE, &err); 
  CL_CHECK(err); if(!CLQueue) CL_CHECK(1);
}
