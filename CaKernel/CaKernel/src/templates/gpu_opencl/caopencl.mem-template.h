#ifndef %{name_upper}
#define %{name_upper}

/* definition of CCTK_REAL */
#include "cctk.h"


/* device pointers */
/* All the external variables will be declared here.
 * We will generate the this file from the parser directly but we
 * need to generate the memory allocation routines as well.
 */

#include <CL/cl.h>
extern cl_context CLContext;
extern cl_command_queue CLQueue;
extern cl_device_id CLDevice;

%var_loop_if(%['%gvar("%vname", type)'=='GF']%) %[
extern cl_mem dcl_%{vname};
]%

%var_loop_if(%['%gvar("%vname", type)'=='GF' && '%varAttr("%vname", "intent")'=='separateinout']%) %[
extern cl_mem dcl_%{vname}_out;
]%

struct Vars_cl {
  cl_mem ptr;
  union
  {
    struct
    {
      %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      unsigned int %{vname}_offset; ]%
   
      %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      unsigned int %{vname}_offset; ]%
    
      %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      unsigned int %{vname}_out_offset; ]%
    
      unsigned int all;
      unsigned int sepinout;
      unsigned int sepinout_out;
    };
    unsigned int field[];
  };
} ;

extern Vars_cl dcl_vars;

struct Pars_cl {
 cl_mem ptr;

  %par_loop(%[
  unsigned int %{vname}_offset; ]%)
  unsigned int all;
} ;

extern Pars_cl dcl_pars;




#endif /* %{name_upper} */
