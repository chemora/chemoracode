
#ifndef _CACUDAUTIL_H_
#define _CACUDAUTIL_H_

/* CaCUDAUtil.h shall be visible to all CaCUDA developers at some point */

#include "cctk.h"
#include <typeinfo>
#include <stdio.h>

#include "CaKernel.h"

#ifdef __CUDACC__
#include <cuda.h>
#include <cuda_runtime.h>
extern "C"
{
#endif

#define ATOMIC_ADD                                                            \
  atomicAdd 

#define SHARED_TILE_I3D_l(name, type)                                         \
  SHARED_TILE_NAME_I3D_l(name, type, CCTK_CAKERNEL_NAME) 
  
#define SHARED_TILE_NAME_I3D_l(name, type, fun_name)                          \
  SHARED_TILE_NAME2_I3D_l(name, type, fun_name) 

#define SHARED_TILE_NAME2_I3D_l(name, type, fun_name)                         \
template<typename t> __device__ inline type &                                 \
   CAKERNEL_GFINDEX3D_ ## fun_name ## _ ## name ## _l                         \
      (t name, int i, int j, int k) {                                         \
      return name[k][j + threadIdx.y][i + threadIdx.x];                       \
   }

  
#define SHARED_TILE(name, width_z, type)                                      \
         __shared__ type name[width_z][CAKERNEL_Tiley][CAKERNEL_Tilex];       \


#define REDUCE0(name, fun, store)                                             \
  {for(int j = CAKERNEL_Tiley, i = CAKERNEL_Tiley / 2 + CAKERNEL_Tiley % 2;   \
                                            i > 0; j = i, i = i / 2 + i % 2)  \
  {                                                                           \
    if(lj + i < j){                                                           \
      I3D_l(name, 0, 0, 0) = fun((I3D_l(name,0,0,i)), (I3D_l(name, 0, 0, 0)));\
    }                                                                         \
    __syncthreads();                                                          \
  }                                                                           \
  for(int j = CAKERNEL_Tilex, i = CAKERNEL_Tilex / 2 + CAKERNEL_Tilex % 2;    \
                                            i > 0; j = i, i = i / 2 + i % 2)  \
  {                                                                           \
    if(CAKERNEL_Tilex <= 32 || lj + i < j){                                   \
      I3D_l(name,0,0,0) = fun((I3D_l(name,0,0,i)), (I3D_l(name,0,0,0)));      \
    }                                                                         \
    __syncthreads();                                                          \
  } if(!lk && !lj && !li) {store = I3D_l(name, 0, 0, 0);}}
 

#define CUDA_CHECK_LAST_CALL(msg)           __cutilCheckMsg(msg, __FILE__, __LINE__)

inline void __cutilCheckMsg(const char *errorMessage, const char *file,
    const int line)
{
  cudaError_t err = cudaGetLastError();
  if (cudaSuccess != err)
  {
    fprintf(stderr, "%s(%i) : cutilCheckMsg() CUTIL CUDA error : %s : %s.\n",
        file, line, errorMessage, cudaGetErrorString(err));
    exit(-1);
  }
  err = cudaDeviceSynchronize();
  if (cudaSuccess != err)
  {
    fprintf(stderr,
        "%s(%i) : cutilCheckMsg cudaDeviceSynchronize error: %s : %s.\n", file,
        line, errorMessage, cudaGetErrorString(err));
    exit(-1);
  }
}

#ifdef __CUDACC__
}
#endif

#define SYNC_BLOCK() __syncthreads()


/* MPI Util */

#ifdef CCTK_MPI

#define MPI_SAFE_CALL( call )                                              \
  {                                                                        \
    int err = call;                                                        \
    if (err != MPI_SUCCESS)                                                \
    {                                                                      \
      char mpi_error_string[MPI_MAX_ERROR_STRING+1];                       \
      int resultlen;                                                       \
      MPI_Error_string (err, mpi_error_string, &resultlen);                \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,   \
            "MPI error: %s", mpi_error_string);                            \
      exit(-1);                                                            \
     }                                                                     \
   }
#endif

/* Malloc Util */

#define MALLOC_SAFE_CALL( call )                                           \
  {                                                                        \
    void *err = call;                                                      \
    if(err == NULL)                                                        \
    {                                                                      \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,   \
            "Malloc error: %s", "failed to allocate memory !");            \
      exit(-1);                                                            \
    }                                                                      \
  }

#ifdef CCTK_REAL_PRECISION_4
  #define COPYSIGN copysignf
#else
  #define COPYSIGN copysign
#endif

/* util functions used in Kerner launcher */

int inline iDivUp(int a, int b){
  return (a + b - 1) / b;
}


/* CaCUDA parameters to lauch CaCUDA kernel */

struct CaCUDA_Kernel_Launch_Parameters
{
  CCTK_INT cagh_it;
  CCTK_INT cagh_ni;
  CCTK_INT cagh_nj;
  CCTK_INT cagh_nk;
  CCTK_INT cagh_blocky;
  CCTK_REAL cagh_dx;
  CCTK_REAL cagh_dy;
  CCTK_REAL cagh_dz;
  CCTK_REAL cagh_dt;
  CCTK_REAL cagh_xmin;
  CCTK_REAL cagh_ymin;
  CCTK_REAL cagh_zmin;
  CCTK_REAL cagh_time;
  union{
    CCTK_INT cagh_boundWidth[6];
    struct{
      CCTK_INT cagh_boundWidthxn;
      CCTK_INT cagh_boundWidthxp;
      CCTK_INT cagh_boundWidthyn;
      CCTK_INT cagh_boundWidthyp;
      CCTK_INT cagh_boundWidthzn;
      CCTK_INT cagh_boundWidthzp;
    };
  };
  CCTK_INT cagh_boundsPhys;
  CaCUDA_Kernel_Launch_Parameters ():cagh_it(0), cagh_ni(0), cagh_nj(0),
    cagh_nk (0), cagh_blocky(0),
    cagh_dx (0), cagh_dy (0), cagh_dz (0), cagh_dt (0), cagh_xmin (0), cagh_ymin (0),
    cagh_zmin (0), cagh_time (0), cagh_boundWidthxp(0), cagh_boundWidthxn(0), 
    cagh_boundWidthyp(0), cagh_boundWidthyn(0), cagh_boundWidthzp(0), cagh_boundWidthzn(0), 
    cagh_boundsPhys(0)
  {
  }

  CaCUDA_Kernel_Launch_Parameters(int _cagh_it, int _cagh_ni, int _cagh_nj, int _cagh_nk,
          int _cagh_blocky,
          CCTK_REAL _cagh_dx, CCTK_REAL _cagh_dy, CCTK_REAL _cagh_dz, CCTK_REAL _cagh_dt,
          CCTK_REAL _cagh_xmin, CCTK_REAL _cagh_ymin, CCTK_REAL _cagh_zmin, CCTK_REAL _cagh_time,
          CCTK_INT _cagh_boundWidthxp, CCTK_INT _cagh_boundWidthxn, CCTK_INT _cagh_boundWidthyp, 
          CCTK_INT _cagh_boundWidthyn, CCTK_INT _cagh_boundWidthzp, CCTK_INT _cagh_boundWidthzn,
          CCTK_INT _cagh_boundPhys) :
    cagh_it (_cagh_it), cagh_ni (_cagh_ni), cagh_nj (_cagh_nj), cagh_nk (_cagh_nk),
    cagh_blocky(_cagh_blocky), cagh_dx (_cagh_dx), cagh_dy (_cagh_dy), cagh_dz (_cagh_dz),
    cagh_dt (_cagh_dt), cagh_xmin (_cagh_xmin), cagh_ymin (_cagh_ymin), cagh_zmin (_cagh_zmin),
    cagh_time (_cagh_time), 
    cagh_boundWidthxp(_cagh_boundWidthxp), cagh_boundWidthxn(_cagh_boundWidthxn), 
    cagh_boundWidthyp(_cagh_boundWidthyp), cagh_boundWidthyn(_cagh_boundWidthyn), 
    cagh_boundWidthzp(_cagh_boundWidthzp), cagh_boundWidthzn(_cagh_boundWidthzn), 
    cagh_boundsPhys(_cagh_boundPhys)
  {
  }
};


#define DECLARE_CACUDA_PARAMS                                                 \
  const int blocky = 0;                                                       \
  CaCUDA_Kernel_Launch_Parameters prms                                        \
    (cctk_iteration, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],                   \
     blocky,                                                                  \
     CCTK_DELTA_SPACE(0), CCTK_DELTA_SPACE(1), CCTK_DELTA_SPACE(2),           \
     CCTK_DELTA_TIME,                                                         \
     CCTK_ORIGIN_SPACE(0), CCTK_ORIGIN_SPACE(1), CCTK_ORIGIN_SPACE(2),        \
     cctk_time, 0, 0, 0, 0, 0, 0, 0);                                         \
                                                                              \
  int imin[3], imax[3], is_symbnd[6],                                         \
      is_physbnd[6], is_ipbnd[6];                                             \
                                                                              \
  assert(!CaKernel_GetBoundaryInfo(cctkGH, cctk_lsh,/*cctk_lssh,*/ cctk_bbox, \
                                   cctk_nghostzones, imin, imax, is_symbnd,   \
                                   is_physbnd, is_ipbnd));                    \
                                                                              \
  int bounds = 0;                                                             \
  for(int dir = 0; dir < 3; dir++){                                           \
    if  (is_physbnd [dir * 2    ]) {                                          \
      bounds|= 1 << (dir * 2);                                                \
      prms.cagh_boundWidth[dir * 2] = imin[dir];                              \
    } else if(!exterior[dir * 2])                                             \
      prms.cagh_boundWidth[dir * 2] = cctk_nghostzones[dir];                  \
    if (is_physbnd [dir * 2 + 1]) {                                           \
      bounds|= 1 << (dir * 2 + 1);                                            \
      prms.cagh_boundWidth[dir * 2 + 1] = cctk_lsh[dir] - imax[dir];          \
    } else if(!exterior[dir * 2 + 1])                                         \
      prms.cagh_boundWidth[dir * 2 + 1] = cctk_nghostzones[dir];              \
  }                                                                           \
  prms.cagh_boundsPhys = bounds;



  struct P3 {
   CCTK_REAL x, y, z;
   inline P3(CCTK_REAL _x, CCTK_REAL _y, CCTK_REAL _z):x(_x), y(_y), z(_z){}
   inline P3():x(0.0f), y(0.0f), z(0.0f){}
 };
 typedef CCTK_REAL P;

#ifndef T3Dx
# define T3Dx(z, y, x) (((z) * DIMY + (y)) * DIMX + (x))
# define T3Dy(z, y, x) (T3Dx(z, y, x) + (DIMX * DIMY * DIMZ))
# define T3Dz(z, y, x) (T3Dx(z, y, x) + (DIMX * DIMY * DIMZ * 2))
#endif

#define T3Dx_val(ptr, z, y, x, pitchx, pitchy)             (((__typeof__(ptr))((char *)(ptr) + (z) * (pitchy)  + (y) * (pitchx)))[(x)])
#define T3Dy_val(ptr, z, y, x, pitchx, pitchy, next_table) (__typeof__(ptr)(((char *)&T3Dx_val(ptr, z, y, x, pitchx, pitchy)) + (next_table)))[0]
#define T3Dz_val(ptr, z, y, x, pitchx, pitchy, next_table) (__typeof__(ptr)(((char *)&T3Dx_val(ptr, z, y, x, pitchx, pitchy)) + (next_table) * 2))[0]



#endif                          /* _CACUDAUTIL_H_ */
