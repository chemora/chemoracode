/* Assume Piraha will generate this file and this file will be pushed here as well */

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "CaKernel.h"

#ifdef __CUDACC__ 
#include <cuda.h> 
#include <cuda_runtime.h> 
#endif

#include <assert.h>
#include <algorithm>
using namespace std;
%evaluate(macros) 


static Vars __attribute__((unused)) d_vars;
Pars d_%{thornname}_pars;

static void CaKernel_AllocDevVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' != 'GF' ]%) > 0 ]%, %[
  
  int numvars = %macro(num_vars_sepinout);
  size_t sizes[%macro(num_vars_sepinout) + 1] = {
    %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      sizeof (%gvar("%vname", CCTK_TYPE)), ]%
    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      sizeof (%gvar("%vname", CCTK_TYPE)), ]%
    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      sizeof (%gvar("%vname", CCTK_TYPE)), ]%
      16 // to make sure that the "structure" is aligned to 16 bytes
    };   

  size_t elements[%macro(num_vars_sepinout) + 1] = {
    %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)

    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)

    %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ %macro(var_numelements), ]%)
      0
    };   

  int offsets[%macro(num_vars_sepinout) + 1];   
  
  offsets[0] = 0;
  for (int i = 1; i <= numvars; i++){
    int oldoffset = offsets[i - 1] + sizes[i - 1] * elements[i - 1];
    for(int b = 2; b <= 16 && b <= sizes[i]; b <<= 1)
      oldoffset += (b - (oldoffset % b)) % b;
    offsets[i] =  oldoffset;
  }

  d_vars.sepinout_out = d_vars.sepinout = d_vars.all = offsets[numvars];
  %set(tmpi, 0)
  %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    d_vars.%{vname}_offset = offsets[(int)%inc(tmpi)]; ]%)

  %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    d_vars.sepinout = min(offsets[(int) %var(tmpi)], d_vars.sepinout);
    d_vars.%{vname}_offset = offsets[(int)%inc(tmpi)]; ]%)

  %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%, %[ 
    d_vars.sepinout_out = min(offsets[(int) %var(tmpi)], d_vars.sepinout);
    d_vars.%{vname}_offset = offsets[(int)%inc(tmpi)]; ]%)

  CUDA_SAFE_CALL (cudaMalloc ((void **) &(d_vars.ptr), offsets[numvars]));
  ]%)
}

static void CaKernel_FreeDevVars(){
  %ifthen(%[ %var_loop_ifno(%[ '%gvar("%vname", type)' != 'GF' ]%) > 0 ]%, %[
  CUDA_SAFE_CALL (cudaFree ((void *) (d_vars.ptr)));
  ]%)
}

static void CaKernel_AllocDevPars(CCTK_ARGUMENTS)
{
  %ifthen(%[ %par_loop_ifno(true) > 0 ]%, %[
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  int numvars = %macro(num_pars);
  size_t sizes[%macro(num_pars) + 1] = {
    %par_loop() %[
      sizeof (%gpar("%vname", CCTK_TYPE)), ]%
      16 // to make sure that the "structure" is aligned to 16 bytes
    };   

  size_t elements[%macro(num_pars) + 1] = {
    %par_loop(%[ %macro(par_numelements), ]%)
      0
    };   

  int offsets[%macro(num_pars) + 1];   
  
  offsets[0] = 0;
  for (int i = 1; i <= numvars; i++){
    int oldoffset = offsets[i - 1] + sizes[i - 1] * elements[i - 1];
    for(int b = 2; b <= 16 && b <= sizes[i]; b <<= 1)
      oldoffset += (b - (oldoffset % b)) % b;
    offsets[i] =  oldoffset;
  }  
  %set(tmpi, 0)
  %par_loop(%[ 
    d_%{thornname}_pars.%{vname}_offset = offsets[(int)%setpold(tmpi, "%eval(%[ %var(tmpi) + 1 ]%)")]; ]%)

//  d_pars = pars;
  char * tmpptr = (char *)malloc (offsets[numvars]);
  CUDA_SAFE_CALL (cudaMalloc ((void **) &(d_%{thornname}_pars.ptr), offsets[numvars]));
  %set(tmpi, 0)
  %par_loop(%[ 
  { 
    %gpar("%vname", CCTK_TYPE) * ptr = (%gpar("%vname", CCTK_TYPE) *) &tmpptr[d_%{thornname}_pars.%{vname}_offset];
    %ifelse("%gpar('%vname', isVector)",%[%com%[ 
]%for(int i = 0; i < elements[%inc(tmpi)]; i++)
      ptr[i] = %vname[i]; ]%, %[*ptr = %vname; %t%[%inc(tmpi)]% ]%)  
  } ]%)
  
  CUDA_SAFE_CALL (cudaMemcpy(d_%{thornname}_pars.ptr, tmpptr, offsets[numvars], cudaMemcpyHostToDevice));
  free(tmpptr);
  ]%)
}

static void CaKernel_FreeDevPars()
{
  %ifthen(%[ %par_loop_ifno(true) > 0 ]%, %[
//  free(pars.ptr);
  CUDA_SAFE_CALL(cudaFree ((void *) d_%{thornname}_pars.ptr));
  ]%)
}

extern "C" 
int Device_GetDevice(const CCTK_POINTER_TO_CONST cctkGH);

void %{thornname}_%{path_parent_lower}_AllocDevMem (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const int cak__devidx = Device_GetDevice(cctkGH);

  if(!(cak__devidx >= 0)){
    CCTK_VInfo(CCTK_THORNSTRING, "device not assigned because Accelerator told me not to. ");
  }

  int cak__num_tl = 1;
  int cak__vi = -1;
  %var_loop %[
    %ifelse("%gvar('%vname', isVector)",%[
    cak__vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname[0]");]%,%[
    cak__vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname");]%)
    if(%tlevel == 0) {
      assert(cak__vi >= 0);
      cak__num_tl = CCTK_DeclaredTimeLevelsVI(cak__vi);
      Device_RegisterMem(CCTK_PASS_CTOC, cak__vi, cak__num_tl); 
    }
  ]%

  CaKernel_AllocDevPars(CCTK_PASS_CTOC);
  
//Just to aviod warnings
  if(0) CaKernel_AllocDevVars(CCTK_PASS_CTOC);
//  CaKernel_AllocDevVars(CCTK_PASS_CTOC);
}

/**
 * Free the memory of grid variables on GPU devices.
 */

int %{thornname}_%{path_parent_lower}_FreeDevMem () {
  
  CaKernel_FreeDevPars();

//Just to avoid warnings  
  if(0) CaKernel_FreeDevVars();
  return 0;
}

%macro(schedule_in_init)

