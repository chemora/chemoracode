#include "CaKernel.h" 

%evaluate(macros)
%macro(run1_undefpars)

#include<assert.h>

extern "C"
void CAKERNEL_Launch_%{name}(CCTK_ARGUMENTS)
{
  /// ... following line is required because of using *.a archives for thorns,
  ///     the variables are not initializied at all, unless used somewhere...
  %{thornname}::%{path_parent_lower}_ignoreme+=1;

    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    
    CHECK_THORN_INIT

    int vi;
    %var_loop %[
      %ifelse("%gvar('%vname', isVector)",%[ 
      vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname[0]");]%,%[
      vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname");]%)
      assert(vi >= 0);
      void * d_%{vname} = Device_GetVarI(cctkGH, vi, 0); 
      %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)")%[ %set(arg1,"%var(fi)")
      void * d_%{vname}%macro(append_p) = Device_GetVarI(cctkGH, vi, %var(fi)); ]% ]%
    ]%

#define TS_ERROR(d)                                                           \
    if ( CAKERNEL_Tile##d <= stncl_##d##n + stncl_##d##p )                    \
      CCTK_VWarn                                                              \
       (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,                 \
        "Tile size for %{name} along %s axis (%d) too small for stencil %d -- %d\n",\
        #d, CAKERNEL_Tile##d, stncl_##d##n, stncl_##d##p);

    TS_ERROR(x); TS_ERROR(y); TS_ERROR(z);
#   undef TS_ERROR

    //
    // Prepare grid hierarchy values.
    //

    const int blocky = 0;
    CaCUDA_Kernel_Launch_Parameters prms
      (cctk_iteration,
       cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
       blocky,
       CCTK_DELTA_SPACE(0), CCTK_DELTA_SPACE(1), CCTK_DELTA_SPACE(2),
       CCTK_DELTA_TIME,
       CCTK_ORIGIN_SPACE(0), CCTK_ORIGIN_SPACE(1), CCTK_ORIGIN_SPACE(2),
       cctk_time, 1, 1, 1, 1, 1, 1, -1);

    static cudaStream_t * streams = 0;
    if(!streams){
      streams = (cudaStream_t *)malloc (sizeof(cudaStream_t) * 6);
      for(int i = 0; i < 6; i++)
      	CUDA_SAFE_CALL(cudaStreamCreate(&streams[i]));
    }

    

    CUDA_SAFE_CALL(cudaDeviceSynchronize());

    if(cctkGH->cctk_bbox[1] == 1){
      CAKERNEL_%{name}<1,0,0><<<
dim3(iDivUp(prms.cagh_nj, CAKERNEL_Tiley), iDivUp(prms.cagh_nk, CAKERNEL_Tilez)),
dim3(CAKERNEL_Tiley, CAKERNEL_Tilez),0,streams[1]>>>(  
          %macro(run1_pass_args_d) prms);
    }

    if(cctkGH->cctk_bbox[0] == 1){
    CAKERNEL_%{name}<-1,0,0><<<
dim3(iDivUp(prms.cagh_nj, CAKERNEL_Tiley), iDivUp(prms.cagh_nk, CAKERNEL_Tilez)),
dim3(CAKERNEL_Tiley, CAKERNEL_Tilez),0,streams[0]>>>(  
          %macro(run1_pass_args_d) prms);
    }

    if(cctkGH->cctk_bbox[3] == 1){
    CAKERNEL_%{name}<0,1,0><<<
dim3(iDivUp(prms.cagh_ni, CAKERNEL_Tilex), iDivUp(prms.cagh_nk, CAKERNEL_Tilez)),
dim3(CAKERNEL_Tilex, CAKERNEL_Tilez),0,streams[3]>>>(  
          %macro(run1_pass_args_d) prms);
    }

    if(cctkGH->cctk_bbox[2] == 1){
    CAKERNEL_%{name}<0,-1,0><<<
dim3(iDivUp(prms.cagh_ni, CAKERNEL_Tilex), iDivUp(prms.cagh_nk, CAKERNEL_Tilez)),
dim3(CAKERNEL_Tilex, CAKERNEL_Tilez),0,streams[2]>>>(  
          %macro(run1_pass_args_d) prms);
    }

    if(cctkGH->cctk_bbox[5] == 1){
    CAKERNEL_%{name}<0,0,1><<<
dim3(iDivUp(prms.cagh_ni, CAKERNEL_Tilex), iDivUp(prms.cagh_nj, CAKERNEL_Tiley)),
dim3(CAKERNEL_Tilex, CAKERNEL_Tiley),0,streams[5]>>>(  
          %macro(run1_pass_args_d) prms);
     }

    if(cctkGH->cctk_bbox[4] == 1){
    CAKERNEL_%{name}<0,0,-1><<<
dim3(iDivUp(prms.cagh_ni, CAKERNEL_Tilex), iDivUp(prms.cagh_nj, CAKERNEL_Tiley)),
dim3(CAKERNEL_Tilex, CAKERNEL_Tiley),0,streams[4]>>>(  
          %macro(run1_pass_args_d) prms);
    }

    CUDA_CHECK_LAST_CALL("Failed while executing the boundary update."); 
}

