%t%[
%evaluate(macros_group)
%set_macro(run1_debug, %[  
#if CACTUS_DEBUG
    CCTK_VInfo(CCTK_THORNSTRING, "Printing the velocity table BEFORE any kernel update FROM THE HOST");
    printPartOfTheTable3D(stdout, (CCTK_REAL *) vx, cctk_lsh[0] * sizeof(CCTK_REAL), cctk_lsh[0] * sizeof(CCTK_REAL) * cctk_lsh[1], (CCTK_REAL *)0, 0, 0, 0, 0, cctk_lsh[2] / 2, cctk_lsh[0], cctk_lsh[1], 1);
    
    static CCTK_REAL * tmp_buff = 0;
    if (!tmp_buff) tmp_buff = (CCTK_REAL *) malloc(datasize);

    cudaMemcpy(tmp_buff, d_vx, datasize, cudaMemcpyDeviceToHost);
    CCTK_VInfo(CCTK_THORNSTRING, "Printing the velocity from the GPU before pressure updating");
    printPartOfTheTable3D(stdout, (CCTK_REAL *) tmp_buff, cctk_lsh[0] * sizeof(CCTK_REAL), cctk_lsh[0] * sizeof(CCTK_REAL) * cctk_lsh[1], (CCTK_REAL *)0, 0, 0, 0, 0, cctk_lsh[2] / 2, cctk_lsh[0], cctk_lsh[1], 1);

    cudaMemcpy(tmp_buff, d_p, datasize, cudaMemcpyDeviceToHost);
    CCTK_VInfo(CCTK_THORNSTRING, "Printing the velocity from the GPU before updating");
    printPartOfTheTable3D(stdout, (CCTK_REAL *) tmp_buff, cctk_lsh[0] * sizeof(CCTK_REAL), cctk_lsh[0] * sizeof(CCTK_REAL) * cctk_lsh[1], (CCTK_REAL *)0, 0, 0, 0, 0, cctk_lsh[2] / 2, cctk_lsh[0], cctk_lsh[1], 1);
    CCTK_VInfo(CCTK_THORNSTRING, "Updating VELOCITY");
#endif /*CACTUS_DEBUG*/
]%)


%set_macro(run1_3dindexing_specific, %[  
 %ifthen(%[ %var_loop_ifno(%[ '%varAttr("%vname",intent)' == 'separateinout' ]%) > 0 ]%, 
  '#error "separateinout variables are not supported in this type of templates"')

 //This macros should be defined in runtime changing the state of being cached or not 
  %var_loop_if(%[ ('%varAttr("%vname", intent)' == 'inout' || '%varAttr("%vname", intent)' == 'in') && '%gvar("%vname", type)'=='gf' ]%)%[
    %ifelse(%[ '%varAttr("%vname", cached)' == 'yes' ]%, %[ #define CAKERNEL_VAR_%{vname}_CACHED 1 ]%, 
    %[ #define CAKERNEL_VAR_%{vname}_CACHED 0 ]%)
  ]%

  %var_loop_if(%['%gvar("%vname", type)'=='gf']%) %[

#if CAKERNEL_VAR_%{vname}_CACHED       

%ifthen( %[ '%kerAttr("",nameShort)'=='3dblock' ]%, %[ %macro(run1_3dblock_indexing_spec_cached) 
#define CAKERNEL_VAR_%{vname}_DECLARE_CACHE             \
    %macro(run1_3dblock_dec_cache_spec) ]%)

%ifthen( %[ '%kerAttr("",nameShort)'=='3dstencil' ]%, %[ %macro(run1_3dstencil_indexing_spec_cached) 
#define CAKERNEL_VAR_%{vname}_DECLARE_CACHE                                   \
  %macro(run1_3dstencil_dec_cache_spec) ]%)

#define CAKERNEL_VAR_%{vname}_FETCH_TO_CACHE                                  \
  %macro(run1_fetch_to_cache_spec)

#define CAKERNEL_VAR_%{vname}_ITERATE_CACHE                                   \
  %macro(run1_iterate_cache_spec)

#define CAKERNEL_VAR_%{vname}_FETCH_FRONT_TILE_CACHE                          \
  %macro(run1_fetch_front_tile_to_cache_spec)

#else
%ifthen( %[ '%kerAttr("",nameShort)'=='3dblock' ]%, %[ %macro(run1_3dblock_indexing_spec) ]%)
%ifthen( %[ '%kerAttr("",nameShort)'=='3dstencil' ]%, %[ %macro(run1_3dstencil_indexing_spec) ]%)

  #define CAKERNEL_VAR_%{vname}_DECLARE_CACHE             
  #define CAKERNEL_VAR_%{vname}_FETCH_TO_CACHE                                  
  #define CAKERNEL_VAR_%{vname}_ITERATE_CACHE                                   
  #define CAKERNEL_VAR_%{vname}_FETCH_FRONT_TILE_CACHE                          
#endif
  ]%

  %macro(run1_3dcommon_indexing_spec)
]%)


%set_macro(run1_iterate_cache, %[                                             \
%var_loop_if(%['%varAttr("%vname", cached)'=='yes' && '%gvar("%vname", type)'=='gf']%,%[\
  %macro(run1_iterate_cache_spec)]%)                                          \
]%)

%set_macro(run1_iterate_cache_spec, %[                                        \
  %for_loop(tmpi,'-%{stencil_zn}','%{stencil_zp}') %[                     \
    I3D_l(%vname, 0, 0, %var(tmpi)) = I3D_l(%vname, 0, 0, %var(tmpi) + 1);]%    \
]%)

%set_macro(run1_fetch_to_cache, %[                                            \
%var_loop_if(%['%varAttr("%vname", cached)'=='yes' && '%gvar("%vname", type)'=='gf']%,%[ \
     %macro(run1_fetch_to_cache_spec)]%)                                      \
]%)

%set_macro(run1_fetch_to_cache_spec, %[                                       \
%for_loop(tmpi,'-%{stencil_zn}','%{stencil_zp}')%[                            \
    I3D_l(%vname, 0, 0, %var(tmpi) + 1) = I3D(%vname, 0, 0, %var(tmpi));]%    \
]%)

%set_macro(run1_fetch_front_tile_to_cache, %[                                 \
%var_loop_if(%['%varAttr("%vname", cached)'=='yes' && '%gvar("%vname", type)'=='gf']%,%[ \
     %macro(run1_fetch_front_tile_to_cache_spec)]%)                           \
]%)

%set_macro(run1_fetch_front_tile_to_cache_spec, %[                            \
    I3D_l(%vname, 0, 0, stncl_zp) = I3D(%vname, 0, 0, stncl_zp);              \
]%)

%set_macro(run1_3dstencil_indexing, %[  
/** Element  */

template<int i, int j, int k, typename t, typename t2>
__device__ inline t2 & cctk_cuda_gfindex3d_%{name}_lhelper
         (t ptr_sh, const int & li, const int & lj, const int & lk
           %for_loop(tmpi,'1','%{stencil_zp}+1',',t2 & v_p%var(tmpi)')
           %for_loop(tmpi,'1','%{stencil_zn}+1',',t2 & v_n%var(tmpi)')
         )
{
    if(k == 0) return ptr_sh[j + lj][i + li];
    %for_loop(i,'1','%{stencil_zp}+1','if(k == %var(i)) return v_p%var(i);')
    %for_loop(i,'1','%{stencil_zn}+1','if(k ==-%var(i)) return v_n%var(i);')
    return ptr_sh[0][0];  // Unreachable.
}

// Static derefernce of the variables 
#define CAKERNEL_GFINDEX3D_%{name}_l(ptr, i, j, k)                     \
    cctk_cuda_gfindex3d_%{name}_lhelper<i, j, k>(ptr##_sh, li, lj, lk       \
  %for_loop(tmpi,'1','%{stencil_zp}+1',', ptr##_p%var(tmpi)')               \
  %for_loop(tmpi,'1','%{stencil_zn}+1',', ptr##_n%var(tmpi)')               \
    )

// Dynamic derefernce of the variables (necessary to use this macro )
#  define CAKERNEL_GFINDEX3D_%{name}_ld(ptr, i, j, k)                  \
    ((k == 0) ? ptr##_sh[j + lj][i + li] : (k > 0) ?                        \
    ptr##_v[k + lk - 1] : ptr##_v[k + lk])

#  define CAKERNEL_GFINDEX3D_%{name}_l2(ptr, i, j, k)                  \
    ((k == 0) ? ptr##_sh[j + lj][i + li] : (k == 1) ?                        \
    ptr##_p1 : ptr##_n1)

#define I3D_l CAKERNEL_GFINDEX3D_%{name}_l
#define I3D_ld CAKERNEL_GFINDEX3D_%{name}_ld

]%)

%set_macro(run1_3dstencil_indexing_spec_cached, %[  
/** Element  */
template<int i, int j, int k, typename t, typename t2>
__device__ inline t2 & cctk_cuda_gfindex3d_%{name}_%{vname}_lhelper
         (t ptr_sh, const int & li, const int & lj, const int & lk
           %for_loop(tmpi,'1','%{stencil_zp}+1',',t2 & v_p%var(tmpi)')
           %for_loop(tmpi,'1','%{stencil_zn}+1',',t2 & v_n%var(tmpi)')
         )
{
    if(k == 0) return ptr_sh[j + lj][i + li];
    %for_loop(i,'1','%{stencil_zp}+1','if(k == %var(i)) return v_p%var(i);')
    %for_loop(i,'1','%{stencil_zn}+1','if(k ==-%var(i)) return v_n%var(i);')
    return ptr_sh[0][0];  // Unreachable.
}

// Static derefernce of the variables 
#define CAKERNEL_GFINDEX3D_%{name}_%{vname}_l(ptr, i, j, k)                     \
    cctk_cuda_gfindex3d_%{name}_%{vname}_lhelper<i, j, k>(ptr##_sh, li, lj, lk  \
  %for_loop(tmpi,'1','%{stencil_zp}+1',', ptr##_p%var(tmpi)')               \
  %for_loop(tmpi,'1','%{stencil_zn}+1',', ptr##_n%var(tmpi)')               \
    )

//#define I3D_l(ptr, i, j, k) CAKERNEL_GFINDEX3D_%{name}_ ## ptr ## _l (ptr, i, j, k)
]%)

%set_macro(run1_3dstencil_indexing_spec, %[  
#define CAKERNEL_GFINDEX3D_%{name}_%{vname}_l                                 \
    I3D
//#define I3D_l(ptr, i, j, k) CAKERNEL_GFINDEX3D_%{name}_ ## ptr ## _l(ptr, i, j, k)
]%)


%set_macro(run1_3dblock_dec_cache, %[                                         \
  %var_loop_if(%[ '%varAttr("%vname", cached)'=='yes' && '%gvar("%vname", type)'=='gf'  ]%,'%macro(run1_3dblock_dec_cache_spec)')\
]%)


%set_macro(run1_3dstencil_dec_cache, %[                                       \
  %var_loop_if(%[ '%varAttr("%vname", cached)'=='yes' && '%gvar("%vname", type)'=='gf'  ]%,'%macro(run1_3dstencil_dec_cache_spec)')\
]%)

%set_macro(run1_3dblock_dec_cache_spec, %[                                         \
  volatile __shared__ %gvar("%vname", CCTK_TYPE) %{vname}_sh[CAKERNEL_Threadsz + stncl_zn + stncl_zp][CAKERNEL_Tiley][CAKERNEL_Tilex];\
]%)

%set_macro(run1_3dstencil_dec_cache_spec, %[                                       \
  __shared__ %gvar("%vname", CCTK_TYPE) %{vname}_sh[CAKERNEL_Tiley][CAKERNEL_Tilex];\
  %for_loop(tmpi,'1','%{stencil_zp}+1',%[%gvar('%vname', CCTK_TYPE) %{vname}_p%var(tmpi);]%)\
  %for_loop(tmpi,'1','%{stencil_zn}+1',%[%gvar('%vname', CCTK_TYPE) %{vname}_n%var(tmpi);]%)\
]%)


%set_macro(run1_pass_args_d, %[%clean_lines%[
%var_loop_if(%['%gvar("%vname",type)'=='GF']%, %[ 
  (typeof(%{vname})) d_%{vname},
  %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)")%[ %set(arg1,"%var(fi)") (typeof(%{vname})) d_%{vname}%macro(append_p), ]% ]% ]% )
%var_loop_if(%['%gvar("%vname",type)'!='GF']%,'(typeof(%{vname})) d_%{vname}, ')
%ifthen(%[ %par_loop_ifno(true) > 0 ]%, %[ d_%{thornname}_pars.ptr, ]%)
%par_loop('d_%{thornname}_pars.%{vname}_offset, ')
]% ]%)

%set_macro(run1_pass_args2_d, %[%clean_lines%[
%var_loop_if(%['%gvar("%vname",type)'=='GF']%, %[
  %{vname},
  %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)")%[ %set(arg1,"%var(fi)") %{vname}%macro(append_p), ]% ]% ]% )
%var_loop_if(%['%gvar("%vname",type)'!='GF']%,'%{vname}, ')
%ifthen(%[ %par_loop_ifno(true) > 0 ]%, %[ d_%{thornname}_pars.ptr, ]%)
%par_loop('d_%{thornname}_pars.%{vname}_offset, ')
]%]%)

%set_macro(run1_declare_args_d, %[                                            \
%var_loop_if(%['%gvar("%vname",type)'=='GF']%, %[                             \
  %gvar("%vname",CCTK_TYPE) * %{vname},                                       \
  %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)", %[ %set(arg1,"%var(fi)") %gvar("%vname",CCTK_TYPE) * %{vname}%macro(append_p), ]% ) ]% ]% ) \
%var_loop_if(%['%gvar("%vname",type)'!='GF']%,'%gvar("%vname",CCTK_TYPE) * %{vname}, ')\
%ifthen(%[ %par_loop_ifno(true) > 0 ]%, %[const  char * pars_ptr, ]%)         \
%par_loop('const unsigned int %{vname}_offset, ')                             \
]%)


]%
