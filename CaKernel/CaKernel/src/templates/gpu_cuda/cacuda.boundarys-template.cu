#include "CaKernel.h" 

%evaluate(macros)
%macro(run1_undefvars)
%macro(run1_undefpars)

#include<assert.h>
#include<cctk.h>

#define STREAMS_NO 8
static cudaStream_t * streams = 0;
static int stream_next = 0;

/**
 * This is a function that gets the information about boundaries;
 * It is declared here, because otherwise, we would have to modify 
 * the interface.ccl file
 */
extern "C" 
int CaKernel_GetBoundaryInfo(const void * cctkGH_, const int * cctk_lsh, /*const int * cctk_lssh, */
                             const int * cctk_bbox, const int * cctk_nghostzones, int * imin,
                             int * imax, int * is_symbnd, int * is_physbnd, int * is_ipbnd);


template<int bound_x, int bound_y, int bound_z>
void CAKERNEL_Launch_%{name}_s (CCTK_ARGUMENTS, 
                                %macro(run1_declare_args_d) //int * const ghost_width, 
                                CaCUDA_Kernel_Launch_Parameters * prms)
{
  DECLARE_CCTK_PARAMETERS;

  unsigned int width[] = {8, 8, 8};
  if(bound_x < 0) width[0] = prms->cagh_boundWidthxn;
  if(bound_x > 0) width[0] = prms->cagh_boundWidthxp;
  if(bound_y < 0) width[1] = prms->cagh_boundWidthyn;
  if(bound_y > 0) width[1] = prms->cagh_boundWidthyp;
  if(bound_z < 0) width[2] = prms->cagh_boundWidthzn;
  if(bound_z > 0) width[2] = prms->cagh_boundWidthzp;
   
  unsigned int blocks[] = {1, 1, 1};
  if(bound_x == 0) blocks[0] = iDivUp(prms->cagh_ni - (prms->cagh_boundWidthxn + prms->cagh_boundWidthxp), width[0]);
  if(bound_y == 0) blocks[1] = iDivUp(prms->cagh_nj - (prms->cagh_boundWidthyn + prms->cagh_boundWidthyp), width[1]);
  if(bound_z == 0) blocks[2] = iDivUp(prms->cagh_nk - (prms->cagh_boundWidthzn + prms->cagh_boundWidthzp), width[2]);
  prms->cagh_blocky = blocks[1];

  %e%[ %t%[%gpar(verbose,dataType)]% 
  if(verbose)
    CCTK_VInfo(CCTK_THORNSTRING, "Lanching boundary(%2d,%2d,%2d) update, with blocks(%2d,%2d,%2d), stream(%d) and grid(%2d,%2d,%2d)",
                                  bound_x, bound_y, bound_z, width[0], width[1], width[2], stream_next, blocks[0], blocks[1], blocks[2]); ]%

  CAKERNEL_%{name}<bound_x, bound_y, bound_z>
    <<<dim3(blocks[0], blocks[1] * blocks[2], 1), *((dim3 *) width),0,streams[stream_next]>>>
    (%macro(run1_pass_args2_d) *prms);

  stream_next = (stream_next + 1) % STREAMS_NO;
}


extern "C"
void CAKERNEL_Launch_%{name}(CCTK_ARGUMENTS)
{
  /// ... following line is required because of using *.a archives for thorns,
  ///     the variables are not initializied at all, unless used somewhere...
  %{thornname}::%{path_parent_lower}_ignoreme+=1;

    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;

    CHECK_THORN_INIT

    int cak__vi;
    %var_loop %[
      %ifelse("%gvar('%vname', isVector)",%[ 
      cak__vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname[0]");]%,%[
      cak__vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname");]%)
      assert(cak__vi >= 0);
      void * d_%{vname} = Device_GetVarI(cctkGH, cak__vi, 0); 
      %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)")%[ %set(arg1,"%var(fi)")
      void * d_%{vname}%macro(append_p) = Device_GetVarI(cctkGH, cak__vi, %var(fi)); ]% ]%
    ]%

    stream_next=0;

#define TS_ERROR(d)                                                           \
    if ( CAKERNEL_Tile##d <= stncl_##d##n + stncl_##d##p )                    \
      CCTK_VWarn                                                              \
       (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,                 \
        "Tile size for %{name} along %s axis (%d) too small for stencil %d -- %d\n",\
        #d, CAKERNEL_Tile##d, stncl_##d##n, stncl_##d##p);

    TS_ERROR(x); TS_ERROR(y); TS_ERROR(z);
#   undef TS_ERROR

    //
    // Prepare grid hierarchy values.
    //

    DECLARE_CACUDA_PARAMS;
    if(verbose)
      printf("Update'ing boundaries; boundphys: %x, b=(%d,%d,%d,%d,%d,%d), bb=(%d,%d,%d,%d,%d,%d) ", 
                                   prms.cagh_boundsPhys,
                                   prms.cagh_boundWidthxn, prms.cagh_boundWidthxp,
                                   prms.cagh_boundWidthyn, prms.cagh_boundWidthyp,
                                   prms.cagh_boundWidthzn, prms.cagh_boundWidthzp,
                                   cctkGH->cctk_bbox[0], cctkGH->cctk_bbox[1], cctkGH->cctk_bbox[2],
                                   cctkGH->cctk_bbox[3], cctkGH->cctk_bbox[4], cctkGH->cctk_bbox[5]);

    if(!streams){
      streams = (cudaStream_t *)malloc (sizeof(cudaStream_t) * STREAMS_NO);
      for(int i = 0; i < STREAMS_NO; i++)
      	CUDA_SAFE_CALL(cudaStreamCreate(&streams[i]));
    }
  
  %set(v, 1) %while_loop("%var(v) < 27")%[ 
    { const int bx = (%var(v)+1)%3-1, by=(%var(v)/3+1)%3-1, bz=(%inc(v)/9+1)%3-1;  
      if((!bx || prms.cagh_boundsPhys & (1 << ((bx + 1)/2))) &&
         (!by || prms.cagh_boundsPhys & (1 << ((by + 1)/2 + 2))) &&
         (!bz || prms.cagh_boundsPhys & (1 << ((bz + 1)/2 + 4)))){
         CAKERNEL_Launch_%{name}_s <bx, by, bz> (CCTK_PASS_CTOC, %macro(run1_pass_args_d) &prms);
      }}
  ]% 

  CUDA_CHECK_LAST_CALL("Failed while executing the boundary update.");
}

