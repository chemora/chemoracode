#ifndef %{name_upper}
#define %{name_upper}

/* definition of CCTK_REAL */
#include "cctk.h"

//#warning "TODO: This is a bad hack. It should be done via interface.ccl files but I don't know how to do it" \
//         "without modifying interface.ccl each time"
extern "C" void * Device_GetVarI(void *cctkGH, int vi, int tl);
extern "C" int Device_RegisterMem(void * cctkGH_, int vi, int tl);
extern "C" int Device_UnRegisterMem(void * cctkGH_, int vi);

#ifdef __CUDACC__

/* device pointers */
/* All the external variables will be declared here.
 * We will generate the this file from the parser directly but we
 * need to generate the memory allocation routines as well.
 */



struct Vars {
  char  * ptr;
  union
  {
    struct
    {
      %var_loop_if(%['%varAttr("%vname", "intent")'!='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      unsigned int %{vname}_offset; ]%
   
      %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      unsigned int %{vname}_offset; ]%
    
      %var_loop_if(%['%varAttr("%vname", "intent")'=='separateinout' && '%gvar("%vname", type)'!='GF']%) %[
      unsigned int %{vname}_out_offset; ]%
    
      unsigned int all;
      unsigned int sepinout;
      unsigned int sepinout_out;
    };
    unsigned int field[];
  };
};

//extern Vars d_vars;

struct Pars {
  char  * ptr;

  %par_loop(%[
  unsigned int %{vname}_offset; ]%)
  unsigned int all;
} ;

extern Pars d_%{thornname}_pars;

namespace %{thornname} {
  extern int %{path_parent_lower}_ignoreme;
}

#endif

#endif /* %{name_upper} */
