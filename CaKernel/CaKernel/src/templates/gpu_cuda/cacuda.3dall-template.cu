#include "CaKernel.h" 

%evaluate(macros)
%macro(run1_undefpars)

#include<assert.h>

extern "C"
void CAKERNEL_Launch_%{name}(CCTK_ARGUMENTS)
{
  /// ... following line is required because of using *.a archives for thorns,
  ///     the variables are not initializied at all, unless used somewhere...
  %{thornname}::%{path_parent_lower}_ignoreme+=1;

    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;

    CHECK_THORN_INIT

    int vi = 0;

#define TS_ERROR(d)                                                           \
    if ( CAKERNEL_Tile##d <= stncl_##d##n + stncl_##d##p )                    \
      CCTK_VWarn                                                              \
       (CCTK_WARN_ABORT, __LINE__,__FILE__, CCTK_THORNSTRING,                 \
        "Tile size for %{name} along %s axis (%d) too small for stencil %d -- %d\n",\
        #d, CAKERNEL_Tile##d, stncl_##d##n, stncl_##d##p);

    TS_ERROR(x); TS_ERROR(y); TS_ERROR(z);
#   undef TS_ERROR

    
    %var_loop %[
      %ifelse("%gvar('%vname', isVector)",%[ 
      vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname[0]");]%,%[
      vi = CCTK_VarIndex("%gvar('%vname',impl)::%vname");]%)
      assert(vi >= 0);
      void * d_%{vname} = Device_GetVarI(cctkGH, vi, 0); 
      %e%[%for_loop(fi, 1, "%varAttr('%vname', timelevels)")%[ %set(arg1,"%var(fi)")
      void * d_%{vname}%macro(append_p) = Device_GetVarI(cctkGH, vi, %var(fi)); ]% ]%
      %com%[//void * d_%{vname}_out = Device_GetVarI(cctkGH, vi, 1);]%
    ]%

    //
    // Prepare grid hierarchy values.
    //

    int exterior[6] = {0};
    for(int i = 0; i < 6; ++i)
      exterior[i] = !::exterior[i] ? cctk_nghostzones[i / 2] : stencil[i];
    
//    printf("%s exteriors are: %2d,%2d,%2d,%2d,%2d,%2d\n", __FILE__, exterior[0], exterior[1], exterior[2], exterior[3], exterior[4], exterior[5]);

    const int blocky = iDivUp(cctk_lsh[1] - exterior[2] - exterior[3],
                               CAKERNEL_Tiley - stncl_yn - stncl_yp);


    CaCUDA_Kernel_Launch_Parameters prms 
      (cctk_iteration,
       cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
//       cctk_nghostzones[0], cctk_nghostzones[1], cctk_nghostzones[2],
       blocky,
       CCTK_DELTA_SPACE(0), CCTK_DELTA_SPACE(1), CCTK_DELTA_SPACE(2),
       CCTK_DELTA_TIME,
       CCTK_ORIGIN_SPACE(0), CCTK_ORIGIN_SPACE(1), CCTK_ORIGIN_SPACE(2),
       cctk_time, 
       exterior[0], exterior[1], exterior[2], exterior[3], exterior[4], exterior[5],
       -1);

     

    CAKERNEL_%{name}<<<                                                    
 dim3(iDivUp(prms.cagh_ni - exterior[0] - exterior[1], CAKERNEL_Tilex - stncl_xn - stncl_xp), 
      iDivUp(prms.cagh_nk - exterior[4] - exterior[5], CAKERNEL_Tilez - stncl_zn - stncl_zp) 
          * blocky),
 dim3(CAKERNEL_Tilex, CAKERNEL_Threadsy, CAKERNEL_Threadsz)>>>(
      %macro(run1_pass_args_d) prms);

    CUDA_CHECK_LAST_CALL("Failed while executing the function.");
}
