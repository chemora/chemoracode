#ifndef CAKERNEL_H_
#define CAKERNEL_H_

#include<cctk.h>

#define CHECK_THORN_INIT                                                      \
  if(!%{thornname}::thorn_initialized)                                        \
    CCTK_VWarn(CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,         \
    "The CaKernel in thorn %s has not been initialized!\n"                    \
    "You need to schedule a init function in the schedule.ccl file.\n"        \
    "It should look like this:\n"                                             \
    "schedule %" "s_Init in CCTK_BASEGRID after "                             \
    "Accelerator_SetDevice\n{\n"                                              \
    "  LANG: C\n"                                                             \
    "} \"Initialize CUDA Device\"", CCTK_THORNSTRING, CCTK_THORNSTRING);


#ifdef CCTK_REAL_PRECISION_4
# define SIN sinf
# define COS cosf
# define ASIN asinf
# define ACOS acosf
# define TAN tanf
# define SQRT sqrtf
# define pow powf
#else
# define SIN sin
# define COS cos
# define ASIN asin
# define ACOS acos
# define TAN tan
# define SQRT sqrt
#endif
  
namespace %{thornname} {
  typedef void (*thorn_fun_t)(CCTK_ARGUMENTS);

  int push_back(thorn_fun_t);
  int push_back_for_compilation(thorn_fun_t);
  extern bool thorn_initialized;
}

/// SOME DEFINITIONS
#ifdef __CUDACC__

#  define CUDA_SAFE_CALL( call) {                                             \
    cudaError err = call;                                                     \
    if( cudaSuccess != err) {                                                 \
        fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",         \
                __FILE__, __LINE__, cudaGetErrorString( err) );               \
        assert(0);                                                            \
    } }

#endif /* __CUDACC__ */

#endif
