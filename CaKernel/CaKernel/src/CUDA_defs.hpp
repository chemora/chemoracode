
 /*@@
   @file      CUDA_defs.h
   @date      Thu Jan 24 16:16:25 CET 2013
   @author    Marek Blazewicz
   @desc 
   Some common CUDA definitions.
   @enddesc 
 @@*/

#pragma once


#  define CUDA_SAFE_CALL( call) {                                             \
    cudaError err = call;                                                     \
    if( cudaSuccess != err) {                                                 \
        fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",         \
                __FILE__, __LINE__, cudaGetErrorString( err) );               \
        assert(0);                                                            \
    } }

