
#include "MoL_interface.hpp"

__global__
void OpSetVar_dev(CCTK_REAL *restrict const var,
              CCTK_REAL const val,
              CCTK_INT const npoints)
{
  int dim = gridDim.x * TILE;
  for (int i  = blockIdx.x * TILE + threadIdx.x; i < npoints; i += dim) {
    var[i] = val;
  }
}


__global__
void OpCopyVar_dev(CCTK_REAL *restrict const var,
               CCTK_REAL const *restrict const src,
               CCTK_INT const npoints)
{
  int dim = gridDim.x * TILE;
  for (int i  = blockIdx.x * TILE + threadIdx.x; i < npoints; i += dim) {
    var[i] = src[i];
  }
}

__global__
void OpUpdateVar_dev(CCTK_REAL *restrict const var,
                 CCTK_REAL const *restrict const src,
                 CCTK_REAL const fact,
                 CCTK_INT const npoints)
{
  int dim = gridDim.x * TILE;
  for (int i  = blockIdx.x * TILE + threadIdx.x; i < npoints; i += dim) {
    var[i] /*+*/= fact * src[i];
  }
}

__global__
void OpUpdateScaleVar_dev(CCTK_REAL *restrict const var,
                      CCTK_REAL const scale,
                      CCTK_REAL const *restrict const src,
                      CCTK_REAL const fact,
                      CCTK_INT const npoints)
{
  int dim = gridDim.x * TILE;
  for (int i  = blockIdx.x * TILE + threadIdx.x; i < npoints; i += dim) {
    var[i] = scale * var[i] + fact * src[i];
  }
}

__global__
void OpUpdateVar2_dev(CCTK_REAL *restrict const var,
                  CCTK_REAL const *restrict const src1,
                  CCTK_REAL const fact1,
                  CCTK_REAL const *restrict const src2,
                  CCTK_REAL const fact2,
                  CCTK_INT const npoints)
{
  int dim = gridDim.x * TILE;
  for (int i  = blockIdx.x * TILE + threadIdx.x; i < npoints; i += dim) {
    var[i] /*+*/= fact1 * src1[i] + fact2 * src2[i];
  }
}

__global__
void OpUpdateScaleVar2_dev(CCTK_REAL *restrict const var,
                       CCTK_REAL const scale,
                       CCTK_REAL const *restrict const src1,
                       CCTK_REAL const fact1,
                       CCTK_REAL const *restrict const src2,
                       CCTK_REAL const fact2,
                       CCTK_INT const npoints)
{
  int dim = gridDim.x * TILE;
  for (int i  = blockIdx.x * TILE + threadIdx.x; i < npoints; i += dim) {
    var[i] = scale * var[i] + fact1 * src1[i] + fact2 * src2[i];
  }
}

__global__
void OpUpdateVar3_dev(CCTK_REAL *restrict const var,
                  CCTK_REAL const *restrict const src1,
                  CCTK_REAL const fact1,
                  CCTK_REAL const *restrict const src2,
                  CCTK_REAL const fact2,
                  CCTK_REAL const *restrict const src3,
                  CCTK_REAL const fact3,
                  CCTK_INT const npoints)
{
  int dim = gridDim.x * TILE;
  for (int i  = blockIdx.x * TILE + threadIdx.x; i < npoints; i += dim) {
    var[i] /*+*/= fact1 * src1[i] + fact2 * src2[i] + fact3 * src3[i];
  }
}

__global__
void OpUpdateScaleVar3_dev(CCTK_REAL *restrict const var,
                       CCTK_REAL const scale,
                       CCTK_REAL const *restrict const src1,
                       CCTK_REAL const fact1,
                       CCTK_REAL const *restrict const src2,
                       CCTK_REAL const fact2,
                       CCTK_REAL const *restrict const src3,
                       CCTK_REAL const fact3,
                       CCTK_INT const npoints)
{
  int dim = gridDim.x * TILE;
  for (int i  = blockIdx.x * TILE + threadIdx.x; i < npoints; i += dim) {
    var[i] =
      scale * var[i] + fact1 * src1[i] + fact2 * src2[i] + fact3 * src3[i];
  }
}
