package edu.lsu.cct.cakernel;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


class KernelData extends Stringificator {
	final String name;
	public KernelData(String name, String thornName) {
		this.name = name;
		this.thornName = thornName;
	}	
	
	public String thornName="";
	
	Map<String,String> attrs = new LinkedHashMap<String,String>();
	Map<String,VarData> vars = new LinkedHashMap<String,VarData>();
	Set<String> parameters = new HashSet<String>();
	int exterior[] = {1,1,1,1,1,1};
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("CCTK_CUDA_KERNEL ");
		sb.append(name);
		sb.append("\n ATTRS:");
		for(Map.Entry<String,String> e : attrs.entrySet()){
			sb.append(' ');
			sb.append(e.getKey());
			sb.append('=');
			sb.append('"');
			sb.append(e.getValue());
			sb.append('"');
		}
		sb.append("\n PARAMS:");
		for(String p : parameters) {
			sb.append(' ');
			sb.append(p);
		}
		sb.append("\n VARS:\n");
		for(Map.Entry<String, VarData> e : vars.entrySet()){
			sb.append("  ");
			sb.append(e.getValue().toString());
		}
		return sb.toString();
	}
	
	public String nameShort(String dummy1){
		if(attrs.containsKey("TYPE")){
			String ret = attrs.get("TYPE");
			if(ret.contains("/"))
				ret = ret.replaceFirst("^.*/", "");
			return ret;
		}
		return null;
	}
	
	String getProperty(String name, String prop){
		try{ return super.getProperty(name, prop); } catch(Exception e){}
		if(attrs.containsKey(prop))
			return attrs.get(prop);
		throw new RuntimeException("Cannot find a property: " + prop + ", in object named: " + name + "; Class named: " + this.getClass().getName());
	}	
}
