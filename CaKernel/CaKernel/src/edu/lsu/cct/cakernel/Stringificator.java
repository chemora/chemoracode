package edu.lsu.cct.cakernel;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class Stringificator {
	String getProperty(String name, String prop){
		Class<?> propClass = this.getClass();
		Object ret = null;
		Integer i = null;
		
		if(prop.contains(":")){
			String strs[] = prop.split(":");
			if (strs.length != 2){
				throw new RuntimeException("Wrong number of arguments using \":\". Number of arguments: " + 
						strs.length + "; Expression: " + prop);
			}
			i = (int)Float.parseFloat(strs[1]);
			prop = strs[0];
		}
		
		try{ 
			Field f = propClass.getField(prop);
			ret = f.get(this);
		} catch (Exception e){} 
		
		if(ret == null){
			try{ 
				Method m = propClass.getMethod(prop, new Class[] {String.class});
				ret = m.invoke(this, new Object[] {name});
			} catch (Exception e){}
		}
		
		if(ret != null){
			if(i != null){
				return (((Object[])ret)[i]).toString();
			}else{
				return ret.toString();
			}
		}
		
		throw new RuntimeException("Cannot find a property: " + prop + ", in object named: " + name + "; Class named: " + this.getClass().getName());
	}	
}
