package edu.lsu.cct.cakernel;

import java.util.LinkedHashMap;
import java.util.Map;

class VarData extends Stringificator {
	final String name;
	final int stencil[];
	public VarData(String name, int stencil[]) {
		this.name = name;
		this.stencil = stencil;
	}
	Map<String,String> attrs = new LinkedHashMap<String,String>();
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append(" :");
		for(Map.Entry<String,String> e : attrs.entrySet()) {
			sb.append(' ');
			sb.append(e.getKey());
			sb.append('=');
			sb.append('"');
			sb.append(e.getValue());
			sb.append('"');
		}
		sb.append('\n');
		return sb.toString();
	}
	
	
	String getProperty(String name, String prop){
		try{return super.getProperty(name, prop); } catch (Exception e){}
		if(prop != null) prop = prop.toUpperCase();
		if(attrs.containsKey(prop))
			return attrs.get(prop);
		throw new RuntimeException("Cannot find a property: " + prop + ", in object named: " + name + "; Class named: " + this.getClass().getName());
	}	
}
