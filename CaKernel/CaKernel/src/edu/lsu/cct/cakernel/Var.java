package edu.lsu.cct.cakernel;

import java.util.LinkedList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;


import edu.lsu.cct.piraha.Group;

public class Var extends Stringificator {
	
	public enum Access {Public, Private, Protected};
	public enum DataType {Char, Byte, Int, Real, Complex};
	public enum Type {Scalar, GF, Array};
	public enum Distribution {Default, Constant};
	public enum Stagger {M, C, P};
	
	public List<String> varMembers;
	
	public Type type;
	public DataType dataType;
	public Access access;
	public Distribution distribution;
	
	public int dim;
	public int timelevels;
	
	public String name;
	public String description;
	public String vectorExpression;
	public boolean isVector;
	public String size[];
	public String ghostSize[];
	public Stagger stagger[];
	public String tags;
	public String impl;

  public String shortName;
  public int timeLevel;

  public Var(Var v) {
    for(java.lang.reflect.Field f : Var.class.getFields()) {
      try {
        f.set(this,f.get(v));
      } catch(Exception ex) {
        throw new Error(ex);
      }
    }
  }
		
	public static String getDefProperty(String name, String prop){
//		String ret = "/* PROVIDED INFORMATION IS DEFAULT NOT OBTAINED FROM CONFIGURATION [" + name + ", " + prop + "] */";
		String ret = "";
		Var v = new Var(name);
		
		return ret + v.getProperty(name, prop);
	}
	
	public void setDefault(){
		name = null;
		description = "";
		type = Type.Scalar;
		access = Access.Private;
		dim = 3;
		timelevels = 1;
		tags = "";
		varMembers = new LinkedList<String>();
		vectorExpression = "";
		isVector = false;
		size = null;
		ghostSize = null;
		stagger = null;
		distribution = Distribution.Default;
		dataType = DataType.Real;
		type = Type.GF;
		impl = null;
    timeLevel = 0;
    shortName = name;
	}
	
	public void setFromGroup(Group g){
		String str;
		
		if(!"GROUP_VARS".equals(g.getPatternName()))
			throw new RuntimeException("Wrong pattern group name: " + g.getPatternName());
		int i = -1;
		if(!"vtype".equals(g.group(++i).getPatternName()) || !"gname".equals(g.group(++i).getPatternName()))
			throw new RuntimeException("Wrong pattern group name: " + g.group(i).getPatternName());
		
		name = g.group(1).group(0).substring();
		str =  g.group(0).substring().toLowerCase();
		
		if("char".equals(str)) dataType = DataType.Char;
		else if("byte".equals(str)) dataType = DataType.Byte;
		else if("int".equals(str)) dataType = DataType.Int;
		else if("real".equals(str)) dataType = DataType.Real;
		else if("complex".equals(str)) dataType = DataType.Complex;
		else throw new RuntimeException("Wrong datatype name: " + str);
		
		for (i = 2; i < g.groupCount(); i++){
			Group g2 = g.group(i);
			str = g2.getPatternName();
			if("expr".equals(str)){
				isVector = true;
				vectorExpression = g2.substring();
			}else if("gtype".equals(str)){
				str = g2.substring().toLowerCase();
				if("gf".equals(str)) type = Type.GF;
				else if("array".equals(str)) type = Type.Array;
				else if("scalar".equals(str)) type = Type.Scalar;
				else throw new RuntimeException("Wrong group type name: " + str);
			}else if("dim".equals(str)){
				dim = Integer.parseInt(g2.substring());
			}else if("timelevels".equals(str)){
				timelevels = Integer.parseInt(g2.substring());;
			}else if("size".equals(str)){
				size = g2.substring().split("\\s*,\\s*");
				for (int j = 0; j < size.length; j++)
					size[j] = size[j].trim();
			}else if("distrib".equals(str)){
				str = g2.substring().toLowerCase();
				if("constant".equals(str)) distribution = Distribution.Constant;
				else if("constant".equals(str)) distribution = Distribution.Default;
			}else if("ghostsize".equals(str)){
				ghostSize = g2.substring().split("\\s*,\\s*");
				for (int j = 0; j < ghostSize.length; j++)
					ghostSize[j] = ghostSize[j].trim();				
			}else if("stagger".equals(str)){
				String tmp[] = g2.substring().toLowerCase().split("\\s*,\\s*");
				stagger = new Stagger[tmp.length];
				for (int j = 0; j < tmp.length; j++){
					if("m".equals(tmp[j])) stagger[j] = Stagger.M;
					else if("C".equals(tmp[j])) stagger[j] = Stagger.C;
					else if("P".equals(tmp[j])) stagger[j] = Stagger.P;
					else throw new RuntimeException("Wrong stagger: " + tmp[j]);
				}									
			}else if("tags".equals(str)){
				str = g2.substring();
				char quote = str.charAt(0);
				str = str.substring(1, str.length() - 1);
				tags = str.replaceAll("\\\\(" + quote + ")", "\\1");
			}else if("group_comment".equals(str)){
                if(g2.groupCount() == 0) {
                    str = "";
                    description = "";
                } else {
				    str = g2.group(0).substring();
				    description = str.substring(1, str.length() - 1);				
                }
			}else if("VARS".equals(str)){
//				String vars[] = g2.substring().split("\\s*,\\s*");
				for(int j = 0; j < g2.groupCount(); j++){
					String str2 = g2.group(j).substring();
					varMembers.add(str2);
				}
      } else if("desc".equals(str)) {
        // do nothing
			}else throw new RuntimeException("Wrong pattern name: " + str);
		}
		if(varMembers.isEmpty())
			varMembers.add(name);
		
	}
	
	public Var(String name){
		this.name=name;
		setDefault();
	}
	
	public Var(Group g){
		setDefault();
		setFromGroup(g);
	}
	
	public Var(Group g, Access a){
		setDefault();
		access = a;
		setFromGroup(g);
	}
	
	public Var(Group g, String impl){
		setDefault();
		setFromGroup(g);
		this.impl = impl;
	}
	
	public Var(Group g, Access a, String impl){
		setDefault();
		access = a;
		setFromGroup(g);
		this.impl = impl;
	}
	
	public boolean isGroup(String name){
		return name.equals(this.name);
	}
	
	public static String eval(String defs, String var) {
		if(var == null || "".equals(var.trim())){
			System.err.println("It is pointless to evaluate void var...");
			return null;
		}
		String in = defs + var, out = null;
		ScriptEngineManager mgr = new ScriptEngineManager();
		ScriptEngine engine = mgr.getEngineByName("JavaScript");
		try{ out = engine.eval(in).toString(); }catch (Exception e){}
		return out;
	}
	
	public String CCTK_TYPE(String name){
		return "CCTK_" + dataType.toString().toUpperCase();
	}
	
	
}

