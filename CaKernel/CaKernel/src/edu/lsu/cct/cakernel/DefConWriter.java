package edu.lsu.cct.cakernel;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;

public class DefConWriter extends Writer
{
  final Writer w;
  public DefConWriter(Writer w) {
    this.w = w;
  }
  
  @Override
  public void close() throws IOException
  {
    w.close();
  }

  @Override
  public void flush() throws IOException
  {
    w.flush();
  }

  @Override
  public void write(char[] str, int start, int end) throws IOException
  {
    for(int i=start;i<end;i++)
      w(str[i]);
  }
  
  StringBuilder sb = new StringBuilder();
  private void w(char c) throws IOException {
    if(c == '\n') {
      int len = 0;
      if((sb.length()>0 && sb.charAt(sb.length()- ++len) == '\\') || // if terminal backslash
          (sb.length()> 3 && sb.charAt(sb.length()-len) == '"' &&    // if terminal sequence is \n"
           sb.charAt(sb.length()- ++len) == 'n' && sb.charAt(sb.length()- ++len) == '\\' ))	  
      {
        // len == 3 means termainl sequence was \n"
        if(len == 3 && sb.length() > 5 && 
            sb.charAt(sb.length()-4) == '\\' && sb.charAt(sb.length()-5) == '\\') 
          len += 2; // true if terminal sequence was \\\n"
        String endString = sb.substring(sb.length() - len, sb.length()); 
        sb.setLength(sb.length()-len); // remove terminal seqeunce

        // remove trailing white space
        while(sb.length()>0 && " \t\r\b".indexOf(sb.charAt(sb.length()-1)) >= 0) {
          sb.setLength(sb.length()-1);
        }

        // pad out to 80 characters
        int n = (80 - len) - sb.length();
        if(n < 1) n = 1;
        while(n-- > 0) {
          sb.append(' ');
        }
        
        // put the terminal sequence back
        sb.append(endString);
      }    
    }
    sb.append(c);
    if(c == '\n') {
      w.write(sb.toString());
      sb.setLength(0);
    }
  }
  
  /** Test and demonstration */
  public static void main(String[] args) throws Exception {
    OutputStreamWriter osw = new OutputStreamWriter(System.out);
    DefConWriter dcw = new DefConWriter(osw);
    PrintWriter pw = new PrintWriter(dcw);
    pw.println("      hello\\");
    pw.println("world\\");
    pw.println("  and    \\");
    pw.println("goodbye             \\");
    pw.println("moon");
    pw.println("foo\\n\"");
    StringBuilder sb = new StringBuilder();
    for(int i=0;i<80;i++) {
      sb.append((char)('0' + (i % 10)));
    }
    pw.println(sb+"\\");
    sb.append('x');
    pw.println(sb+"\\");
    pw.flush();
  }
}
