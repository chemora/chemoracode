package edu.lsu.cct.cakernel;

import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;

import edu.lsu.cct.piraha.Group;
import edu.lsu.cct.piraha.DebugOutput;

import java.io.PrintWriter;

public class Param extends Stringificator {
	enum Type { Int, Real, Keyword, String, Boolean };
	enum Steerable { Never, Always, Recover, Default };
	enum Access { Global, Restricted, Private, Shares };
	public Type dataType;
	public Access access;
	public Steerable steerable;
	public String name;
	public String alias;
	public String vectorExpression;
	public boolean isVector;
	public String accumulator;
	public String accumulatorBase;
	public String defaultVal;
	public String range;
	public String description;
	
	void setDefault(){
		dataType = Type.Int;
		steerable = Steerable.Default;
		name = "";
		alias = name;
		vectorExpression = "";
		isVector = false;
		accumulator = "";
		accumulatorBase = "";
		defaultVal = "";
		range = "";
		access = Access.Global;
	}
	
    void setFromGroup(Group m, Access a){
        access = a;
        setDefault();
        String patname = m.getPatternName();
        if("intpar".equals(patname)) dataType = Type.Int;			
        else if("realpar".equals(patname)) dataType = Type.Real;
        else if("keywordpar".equals(patname)) dataType = Type.Keyword;
        else if("stringpar".equals(patname)) dataType = Type.String;
        else if("boolpar".equals(patname)) dataType = Type.Boolean;
        else throw new RuntimeException("Cannot parse a pattern group named: " + patname);

        Group m2 = m.group(0);
        if((m2.getPatternName()).endsWith("guts")){
            if("name_num".equals(m2.group(0).getPatternName()))
                name = m2.group(0).group(0).substring();
            else
                throw new RuntimeException("Cannot parse a pattern group named: {" + m2.group(0).getPatternName() + "}, should be {name}");
            List<Group> m2grps = new ArrayList<Group>();
            for(int i=0; i < m2.groupCount(); i++) {
                if("gutpars".equals(m2.group(i).getPatternName())) {
                    Group gg = m2.group(i);
                    for(int j=0; j<gg.groupCount();j++)
                        m2grps.add(gg.group(j));
                } else {
                    m2grps.add(m2.group(i));
                }
            }
            for(int i = 1; i < m2grps.size(); i++){
                Group m3 = m2grps.get(i); 
                patname = m3.getPatternName();
                if("name".equals(patname)) alias = m3.substring();
                else if("num".equals(patname)){
                    isVector = true;
                    vectorExpression = m3.substring();
                }else if("steerable".equals(patname)){
                    String sub = m3.substring().toLowerCase();
                    if("never".equals(sub)) steerable = Steerable.Never;
                    else if("always".equals(sub)) steerable = Steerable.Always;
                    else if("recover".equals(sub)) steerable = Steerable.Recover;
                    else throw new RuntimeException("Wrong value of steerable attribute: " + sub);
                }else if("accumexpr".equals(patname)){
                    accumulator = m3.substring();
                }else if("accname".equals(patname)){
                    accumulator = m3.substring();
                }else if("quote".equals(patname)||"description".equals(patname)){
                    description = m3.substring();
                    description = description.substring(1, description.length() - 1);
                }else throw new RuntimeException("Wrong pattern name: " + patname);
            }
        }

        if(isVector){
            if(Pattern.matches("^\\s*1\\s*$", vectorExpression)){
                isVector = false;
            }
        }

        if(dataType == Type.Real || dataType == Type.Int){
            PrintWriter pw = new PrintWriter(System.err,true);
            //m.dumpMatches(new DebugOutput(pw));
            m2 = m.group(m.groupCount() - 1);
            String name = "num"; if(dataType == Type.Real) name = "real";
            if(name.equals(m2.getPatternName())) defaultVal = m2.substring();
            else if(m.get("uses_or_extends") != null) {
               this.name = m.group(1).group(0).substring(); 
            } else pw.printf("Can't set the default value. Last group: {%s}, should be: {%s}\n", new Object[] {m2.getPatternName(), name});
        }
    }
	
	public Param(Group m) {
		setFromGroup(m, Access.Global);
	}
	
	public Param(Group m, Access a) {
		setFromGroup(m, a);
	}
	
	String getParEqVal(){
		if(steerable == steerable.Never){
			return String.format("%s=%s;\n", new Object[] {name, defaultVal});
		}
		return null;
	}
	
	public String CCTK_TYPE(String name){
		return "CCTK_" + dataType.toString().toUpperCase();
	}
	
	public String toString() {
		return this.dataType+" "+this.name+" = "+this.defaultVal;
	}
}
