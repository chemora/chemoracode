package edu.lsu.cct.cakernel; /*xxx*/

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import edu.lsu.cct.piraha.Grammar;
import edu.lsu.cct.piraha.Matcher;
import edu.lsu.cct.piraha.examples.Calc;

public final class CCLFormater {
	private final KernelData kd;
	private final int[] tile;
	private final File outFile;
	private final int[] stencil;
	private final Grammar g;
	public OutputGen outputgen;
	Calc calc = new Calc();
	String vname;
	String thornName;
	Template template;
	private HashMap<String, Var> gvars;
	private HashMap<String, Param> gpars;
	Map<String, String> vars = new HashMap<String, String>();
	Map<String, String> macros = new HashMap<String, String>();

//	public Map<String, Integer> forLoopVars = new HashMap<String, Integer>();

	public CCLFormater(KernelData kd, HashMap<String, Var> Vars,
			HashMap<String, Param> Pars, int[] tile, File outFile,
                           int[] stencil, Grammar g, Template template, String thornName) {
		this.kd = kd;
		this.tile = tile;
		this.outFile = outFile;
		this.stencil = stencil;
		this.g = g;
		this.template = template;
                this.thornName = thornName;
		gvars = Vars;
		gpars = Pars;
	}

	public String file() {
		return outFile.getName();
	}

	public String date() {
		return new Date().toString();
	}

	public String name_upper() {
		return outFile.getName().replace('.', '_').toUpperCase();
	}

	public String include_as_str(String what) throws IOException {
		String names[] = { Node.getPartOfName(what, 0),
				Node.getPartOfName(what, 1) };
		if (names[0].isEmpty())
			names[0] = template.getPartOfName(0);
		String name = names[0];
		if (!names[1].isEmpty())
			name += "$" + names[1];

		Node group = template.parent;
		Node toInclude = group.find(group.name + "/" + name);
		if (toInclude != null) {
			File inputFile = null;
			try {
				inputFile = new File(outFile.getParentFile(),
						((Template) toInclude).produceFilenames(kd,
								toInclude.scope).second);
			} catch (Exception e) {
			}
			;
			if (inputFile != null && inputFile.isFile()) {
				BufferedReader br = new BufferedReader(
						new FileReader(inputFile));
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = br.readLine()) != null)
					sb.append("\"")
							.append(line.replaceAll("\\\\", "\\\\\\\\")
									.replaceAll("\\\"", "\\\\\""))
							.append("\\n\"\n");
				br.close();
				return sb.toString();
			}
		}
		return "";
	}

	public String include(String what) throws IOException {
		String names[] = { Node.getPartOfName(what, 0),
				Node.getPartOfName(what, 1) };
		if (names[0].isEmpty())
			names[0] = template.getPartOfName(0);
		String name = names[0];
		if (!names[1].isEmpty())
			name += "$" + names[1];

		Node group = template.parent;
		Node toInclude = group.find(group.name + "/" + name);
		if (toInclude != null) {
			File inputFile = null;
			try {
				inputFile = new File(outFile.getParentFile(),
						((Template) toInclude).produceFilenames(kd,
								toInclude.scope).second);
			} catch (Exception e) {
			}
			;
			if (inputFile != null && inputFile.isFile()) {
				BufferedReader br = new BufferedReader(
						new FileReader(inputFile));
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = br.readLine()) != null)
					sb.append(line).append("\n");
				br.close();
				return sb.toString();
			}
		}
		return "";
	}

	public String evaluate(String what) throws IOException {
		String names[] = { Node.getPartOfName(what, 0),
				Node.getPartOfName(what, 1) };
		if (names[0].isEmpty())
			names[0] = template.getPartOfName(0);
		String name = names[0];
		if (!names[1].isEmpty())
			name += "$" + names[1];

		Node group = template.parent;
		Node toInclude = group.find(group.name + "/" + name);
		FileInputStream fr = new FileInputStream(toInclude.file);
		byte buff[] = new byte[(int) toInclude.file.length()];
		fr.read(buff);
		return outputgen.replaceAll(this, new String(buff));
	}

	public String path_lower() {
		return template.abspath.replaceAll("\\$[^/]*$", "").replaceAll("/",
				"__");
	}
	
	public String path_lower_parent() {
		String tmp = template.abspath.replaceAll("\\$[^/]*$", "");
		if(tmp.contains("/")){
			return tmp.replaceAll("/[^/]*$","").
						replaceAll("/", "__");
		}
		return "";
	}

	public String path_parent_lower() {
		return template.abspath.replaceAll("/[^/]*$", "").replaceAll("/", "__");
	}

	public String name() {
		return kd.name;
	}

	public String tile_x() {
		return Integer.toString(tile[0]);
	}

	public String tile_y() {
		return Integer.toString(tile[1]);
	}

	public String tile_z() {
		return Integer.toString(tile[2]);
	}

	public String stencil(String name, String ind) {
		int i = Integer.parseInt(ind);
		int ret = stencil[i];
		final int stencil2[]=kd.vars.get(name).stencil;
		if (stencil2 != null)
		  ret = stencil2[i];
		return Integer.toString(ret);
	}
	
	public String stencil_xn(String name) {
		return stencil(name, "0");
	}
	
	public String stencil_xp(String name) {
		return stencil(name, "1");
	}

	public String stencil_yn(String name) {
		return stencil(name, "2");
	}
	
	public String stencil_yp(String name) {
		return stencil(name, "3");
	}

	public String stencil_zn(String name) {
		return stencil(name, "4");
	}
	
	public String stencil_zp(String name) {
		return stencil(name, "5");
	}
	
	public String stencil_xn() {
		return Integer.toString(stencil[0]);
	}

	public String stencil_xp() {
		return Integer.toString(stencil[1]);
	}

	public String stencil_yn() {
		return Integer.toString(stencil[2]);
	}

	public String stencil_yp() {
		return Integer.toString(stencil[3]);
	}

	public String stencil_zn() {
		return Integer.toString(stencil[4]);
	}

	public String stencil_zp() {
		return Integer.toString(stencil[5]);
	}

	public String exterior_xn() {
		return Integer.toString(kd.exterior[0]);
	}

	public String exterior_xp() {
		return Integer.toString(kd.exterior[1]);
	}

	public String exterior_yn() {
		return Integer.toString(kd.exterior[2]);
	}

	public String exterior_yp() {
		return Integer.toString(kd.exterior[3]);
	}

	public String exterior_zn() {
		return Integer.toString(kd.exterior[4]);
	}

	public String exterior_zp() {
		return Integer.toString(kd.exterior[5]);
	}

	
	public String upper(String str) {
		return outputgen.replaceAll(this, str).toUpperCase();
	}

	public String lower(String str) {
		return outputgen.replaceAll(this, str).toLowerCase();
	}

	public String capital(String str) {
		str = outputgen.replaceAll(this, str);
		return str.substring(0, 1).toUpperCase()
				+ str.substring(1).toLowerCase();
	}

	public String conn() {
		return "\\\n";
	}

	public String con() {
		return "\\";
	}

	void parse_var(String par, Map<String, String> parMap) {
		Matcher m = g.matcher("par", par);
		if (m.matches()) {
			parMap.put(m.group(0).substring(), m.group(1).substring());
		} else {
			throw new RuntimeException("Error in " + par + " " + m.near());
		}
	}

	public String var_loop(String p1, String p2, String p3, String body) {
		Map<String, String> parMap = new HashMap<String, String>();
		parse_var(p1, parMap);
		parse_var(p2, parMap);
		parse_var(p3, parMap);
		return var_loop(parMap, body);
	}

	public String var_loop(String p1, String p2, String body) {
		Map<String, String> parMap = new HashMap<String, String>();
		parse_var(p1, parMap);
		parse_var(p2, parMap);
		return var_loop(parMap, body);
	}

	public String var_loop(String p1, String body) {
		Map<String, String> parMap = new HashMap<String, String>();
		parse_var(p1, parMap);
		return var_loop(parMap, body);
	}

	public String var_loop(String body) {
		Map<String, String> parMap = new HashMap<String, String>();
		return var_loop(parMap, body);
	}

	public String var_loop(Map<String, String> parMap, String body) {
		StringBuilder sb = new StringBuilder();
		String delimit = null;
		for (VarData vd : kd.vars.values()) {
			if (parMap.containsKey("intent")) {
				if (!parMap.get("intent").equals(
						vd.getProperty("", "intent").toLowerCase())) {
					continue;
				}
			}
			if (parMap.containsKey("cached")) {
				if (!parMap.get("cached").equals(
						vd.getProperty("", "cached").toLowerCase())) {
					continue;
				}
			}

			if (gvars.containsKey(vd.name)
					&& getVar(vd.name).isGroup(vd.name)) {
				Var v = getVar(vd.name);
				for (String tmpvname : v.varMembers) {
					vname = tmpvname;
					if (delimit != null)
						sb.append(delimit);
					sb.append(outputgen.replaceAll(this, body));
					if (delimit == null && parMap.containsKey("delimit"))
						delimit = parMap.get("delimit");
				}
			} else {
				vname = vd.name;
				if (delimit != null)
					sb.append(delimit);
				sb.append(outputgen.replaceAll(this, body));
				if (delimit == null && parMap.containsKey("delimit"))
					delimit = parMap.get("delimit");
			}
		}
		vname = null;
		return sb.toString();
	}

	public String par_loop_if(String ifstat, String body) {
		StringBuilder sb = new StringBuilder();
		for (String p : kd.parameters) {
			vname = p;
			if (evalTrue(ifstat))
				sb.append(outputgen.replaceAll(this, body));
		}
		vname = null;
		return sb.toString();
	}

	public String var_loop_if(String ifstat, String body) {
		StringBuilder sb = new StringBuilder();
		for (VarData vd : kd.vars.values()) {
			if (gvars.containsKey(vd.name)
					&& getVar(vd.name).isGroup(vd.name)) {
				Var v = getVar(vd.name);
				for (String tmpvname : v.varMembers) {
					vname = tmpvname;
					if (evalTrue(ifstat))
						sb.append(outputgen.replaceAll(this, body));
				}
			} else {
				vname = vd.name;
				if (evalTrue(ifstat))
					sb.append(outputgen.replaceAll(this, body));
			}
		}
		vname = null;
		return sb.toString();
	}

	public String par_loop_ifno(String ifstat) {
		int count = 0;
		for (String p : kd.parameters) {
			vname = p;
			if (evalTrue(ifstat))
				count++;
		}
		return "" + count;
	}

	public String var_loop_ifno(String ifstat) {
		int count = 0;
		for (VarData vd : kd.vars.values()) {
			if (gvars.containsKey(vd.name)
					&& getVar(vd.name).isGroup(vd.name)) {
				Var v = getVar(vd.name);
				for (String tmpvname : v.varMembers) {
					vname = tmpvname;
					if (evalTrue(ifstat))
						count++;
				}
			} else {
				vname = vd.name;
				if (evalTrue(ifstat))
					count++;
			}
		}
		return Integer.toString(count);
	}

	public String par_loop(String p1, String p2, String body) {
		Map<String, String> parMap = new HashMap<String, String>();
		parse_var(p1, parMap);
		parse_var(p2, parMap);
		return par_loop(parMap, body);
	}

	public String par_loop(String p1, String body) {
		Map<String, String> parMap = new HashMap<String, String>();
		parse_var(p1, parMap);
		return par_loop(parMap, body);
	}

	public String par_loop(String body) {
		Map<String, String> parMap = new HashMap<String, String>();
		return par_loop(parMap, body);
	}

	public String par_loop(Map<String, String> parMap, String body) {
		StringBuilder sb = new StringBuilder();
		String delimit = null;
		for (String str : kd.parameters) {
			vname = str;
			if (delimit != null)
				sb.append(delimit);
			sb.append(outputgen.replaceAll(this, body));
			if (delimit == null && parMap.containsKey("delimit"))
				delimit = parMap.get("delimit");
		}
		return sb.toString();
	}

	public String for_loop(String var, String lo, String hi, String body) {
		int lov = (int) calc.eval(outputgen.replaceAll(this, lo));
		int hiv = (int) calc.eval(outputgen.replaceAll(this, hi));
		int del = lov < hiv ? 1 : -1;
		StringBuilder sb = new StringBuilder();
		for (int i = lov; i != hiv; i += del) {
//			forLoopVars.put(var, i);
			vars.put(var, Integer.toString(i));
			sb.append(outputgen.replaceAll(this, body));
		}
		// forLoopVars.remove(var);
		return sb.toString();
	}

	public String eval(String body) {
		String body2 = outputgen.replaceAll(this, body);
		return Var.eval("", body2.toLowerCase());
	}

	public Boolean evalTrue(String body) {
		String eval = eval(body);
		if (eval.matches("^\\s*$")) {
			System.err
					.println("Got \"\" String from evauation. Probable error during evaluation of the expression: "
							+ body);
			return false;
		}
		if (eval.matches("^\\s*((?i:f(alse|)|0(\\.0*|))|('\\s*')|(\"\\s*\"))\\s*$"))
			return false;
		return true;
	}

	public String while_loop(String eval, String body) {
		StringBuilder sb = new StringBuilder();
		while (evalTrue(eval)) {
			String out = outputgen.replaceAll(this, body);
			sb.append(out);
		}
		// forLoopVars.remove(var);
		return sb.toString();
	}

	public String ifelse(String ifstat, String thenbody, String elsebody) {
		if (evalTrue(ifstat))
			return outputgen.replaceAll(this, thenbody);
		return outputgen.replaceAll(this, elsebody);
	}

	public String ifthen(String ifstat, String thenbody) {
		if (evalTrue(ifstat))
			return outputgen.replaceAll(this, thenbody);
		return "";
	}

  private Var getVar(String vname) {
    Var v = gvars.get(vname);
    Var v2 = v;
    if(v == null && vname.endsWith("Opast")) {
      String shortName = vname.substring(0,vname.length()-5);
      System.err.println("Creating variable: "+vname+" => "+shortName);
      v = gvars.get(shortName);
      v2 = new Var(v);
      v2.shortName = shortName;
      v2.timeLevel = 1;
      gvars.put(vname,v2);
    }
    return v2;
  }

	public String gvar(String name, String param) {
		String name2 = outputgen.replaceAll(this, name);
		String param2 = outputgen.replaceAll(this, param);
		Var v = getVar(name2);
		if (v == null) {
			System.err.println("Cannot find var: " + name2
					+ "; Providing default params....");
			return Var.getDefProperty(name2, param2);
		}
		String ret = v.getProperty(name2, param2);
		return ret;
	}

	public String varAttr(String name, String param) {
		String name2 = outputgen.replaceAll(this, name);
		String param2 = outputgen.replaceAll(this, param);
		VarData v = kd.vars.get(name2);
		// /if the @attr name is a group:
		if (v == null){
			v = kd.vars.get(getVar(name2).name);
		}
		String ret = v.getProperty(name2, param2);
		return ret;
	}

	public String kerAttr(String name, String param) {
		String name2 = outputgen.replaceAll(this, name);
		String param2 = outputgen.replaceAll(this, param);
		String ret = kd.getProperty(name2, param2);
		return ret;
	}

	public String gpar(String name, String param) {
		String name2 = outputgen.replaceAll(this, name);
		String param2 = outputgen.replaceAll(this, param);
		Param p = gpars.get(name2);
		if (p == null)
			System.err.println("Error: Cannot find parameter: " + name2);
		String str = p.getProperty(name2, param2);
		return str;
	}

	public String set(String name, String val) {
		setp(name, val);
		return "";
	}

	public String e(String bodyIfNoExeption, String el) {
		try {
			return outputgen.replaceAll(this, bodyIfNoExeption);
		} catch (Exception e) {
		}
		;
		return outputgen.replaceAll(this, el);
	}

	public String e(String bodyIfNoExeption) {
		try {
			return outputgen.replaceAll(this, bodyIfNoExeption);
		} catch (Exception e) {
		}
		;
		return "";
	}

	public String setp(String name, String val) {
		String out = outputgen.replaceAll(this, val);
		vars.put(name, out);
		return out;
	}

	public String inc(String name) {
		String out = vars.get(name);
		int outi = ((int) Double.parseDouble(out)) + 1;
		vars.put(name, "" + outi);
		return out;
	}

	public String setpold(String name, String val) {
		String out = outputgen.replaceAll(this, val);
		return vars.put(name, out);
	}

	public String set_macro(String name, String val) {
		macros.put(name, val);
		return "";
	}

	public String macro(String name) {
		String ret = macros.get(name);
		return outputgen.replaceAll(this, ret);
	}
	
	public String clean(String body){
		String out = outputgen.replaceAll(this, body);
		String out2 = "";
		Pattern p1 = Pattern.compile("\\s*$\n^\\s*$\n", Pattern.MULTILINE);
		Pattern p2 = Pattern.compile("^\\s*$\n", Pattern.MULTILINE);
		while (!out2.equals(out)){
			out2 = out;
			out = p1.matcher(out).replaceAll(" \n");
			out = p2.matcher(out).replaceAll(" ");
		}
		return out;
	}
	
	public String clean_lines(String body){
		String out = outputgen.replaceAll(this, body);
		String out2 = "";
		Pattern p1 = Pattern.compile("\\s+");
		while (!out2.equals(out)){
			out2 = out;
			out = p1.matcher(out).replaceAll(" ");
		}
		return out;
	}

	public String var(String name) {
//		Integer val = forLoopVars.get(name);
//		if (val != null)
//			return val.toString();
		String valStr = vars.get(name);
		if (valStr != null) {
			return valStr;
		}
		System.err
				.println("Warning: No value available for var '" + name + "'");
		return "";
	}

	/**
	 * comment (don't evaluate)
	 * 
	 * @return
	 */
	public String com(String body) {
		return "";
	}

	/**
	 * trim and eval
	 * 
	 * @return
	 */
	public String t(String body) {
		outputgen.replaceAll(this, body);
		return "";
	}

	public String unset(String name) {
		vars.remove(name);
		return "";
	}

	/**
	 * This is the temporary variable used by the var_loop
	 * 
	 * @return
	 */
	public String vname() {
		if (vname == null) {
			System.err.println("Warning %vname used outside of var_loop");
			return null;
		}
		return vname;
	}
	public String svname() {
		if (vname == null) {
			System.err.println("Warning %vname used outside of var_loop");
			return null;
		}
    Var v = getVar(vname);
    if(v.shortName == null)
      return vname;
    else
		  return v.shortName;
	}
	public String tlevel() {
		if (vname == null) {
			System.err.println("Warning %vname used outside of var_loop");
			return null;
		}
    Var v = getVar(vname);
    if(v == null) throw new NullPointerException("No such var "+vname);
		return Integer.toString(v.timeLevel);
	}
	public String thornname() {
          return thornName;
	}
}
