package edu.lsu.cct.cakernel;

import java.util.Date;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import edu.lsu.cct.cakernel.Var.Access;
import edu.lsu.cct.piraha.Grammar;
import edu.lsu.cct.piraha.Group;
import edu.lsu.cct.piraha.Matcher;

public class CCLParser {
  Grammar g = new Grammar();
  Grammar g_deps = new Grammar();
  Grammar g_parm = new Grammar();
  Grammar g_intr = new Grammar();
  Grammar g_schd = new Grammar();
  long lastModified = Long.MIN_VALUE;
  Map<File,Set<File>> dependencies = new HashMap<File,Set<File>>();
  Map<File,File> outDirs = new HashMap<File,File>();

  Matcher m = null;
  Matcher m_deps = null;
  Matcher m_parm = null;
  Matcher m_intr = null;
  File src = null;
  File scratchBuild = null;
  File arrangementDir = null;
  File cakernelCcl = null;
  File cctkHome = null;
  File depsCcl = null;
  File thornList = null;
  File targetFile = null;

  TemplateGroup root = null;
  String thornName = "";
  private OutputGen out;
  private File templateDir;

  private HashMap<String, Var> vars;
  private HashMap<String, Param> pars = null;

  public CCLParser(File cctkHome) throws IOException {

    g.compileFile(CCLParser.class.getResourceAsStream("/edu/lsu/cct/pegs/cakernel.peg"));
    g_deps.compileFile(CCLParser.class.getResourceAsStream("/edu/lsu/cct/pegs/deps.peg"));

    g_parm.compileFile(new File(cctkHome,"src/piraha/pegs/param.peg"));
    g_schd.compileFile(new File(cctkHome,"src/piraha/pegs/schedule.peg"));
    g_intr.compileFile(new File(cctkHome,"src/piraha/pegs/interface.peg"));
  }

  public static void main(String[] args) throws Exception {
    long start = System.currentTimeMillis();
    try {
      // String str = Grammar.readContents(new
      // File("/home/sbrandt/GBSC12/code/CaCUDA/CaCUDACFD3D/cacuda.ccl"));
      if (args.length != 6 && args.length != 7)
        usage();
      File cclFile = new File(args[0]);

      // String names[] = args[0].split("/");
      // parser.thornName = names[names.length - 2];
      File thornList = new File(args[2]);
      File arrangementDir = new File(args[3]);
      File cctkHome = new File(args[4]);
      File depsCclFile = new File(args[1]);
      File templateDir = depsCclFile.getParentFile();
      File scratchBuild = new File(args[5]);

      File intrCclFile = new File(cclFile.getParentFile(), "interface.ccl");
      File parmCclFile = new File(cclFile.getParentFile(), "param.ccl");

      if (!(cclFile.exists() && templateDir.exists() && depsCclFile.exists()
            && parmCclFile.exists() && intrCclFile.exists())) {
        if(!cclFile.exists()) System.err.println("Missing file: "+cclFile);
        if(!cctkHome.exists()) System.err.println("Missing file: "+cctkHome);
        if(!templateDir.exists()) System.err.println("Missing file: "+templateDir);
        if(!depsCclFile.exists()) System.err.println("Missing file: "+depsCclFile);
        if(!parmCclFile.exists()) System.err.println("Missing file: "+parmCclFile);
        if(!intrCclFile.exists()) System.err.println("Missing file: "+intrCclFile);
        if(!scratchBuild.exists()) System.err.println("Missing file: "+scratchBuild);
        usage();
      }

      CCLParser parser = new CCLParser(cctkHome);
      parser.scratchBuild = scratchBuild;
      parser.arrangementDir = arrangementDir;
      parser.cakernelCcl = cclFile;
      parser.depsCcl = depsCclFile;
      parser.thornList = thornList;
      parser.cctkHome = cctkHome;
      if(args.length == 7) {
        parser.targetFile = new File(args[6]);
      }

      parser.modified(cclFile);
      parser.thornName = cclFile.getParentFile().getName();
      List<String> thorns = getThorns(thornList);
      boolean found = false;
      for(String thorn : thorns) {
        File thornDir = new File(arrangementDir,thorn);
        File cak = new File(thornDir,"cakernel.ccl");
        if(cak.getCanonicalPath().equals(cclFile.getCanonicalPath())) {
          found = true;
          break;
        }
      }
      if(!found) {
        return;
      }

      parser.modified(intrCclFile);
      parser.modified(parmCclFile);
      //parser.modified(depsCclFile);

      // Parse out the public data from the whole build
      for (String thorn : thorns)
        parser.parseParam(new File(new File(arrangementDir, thorn),
              "param.ccl"), true);

      parser.parseParam(parmCclFile, false);
      parser.parseInterface(intrCclFile, arrangementDir, thorns);
      parser.parseFiles(cclFile, depsCclFile, templateDir);
      parser.generateParserDeps();
    } finally {
      long end = System.currentTimeMillis();
      System.out.printf("CCLParser time: %.2f\n",0.001*(end-start));
    }
  }

  void modified(File f) {
    long n = f.lastModified();
    if(n > lastModified) {
      lastModified = n;
    }
  }

  private static List<String> getThorns(File thornList) throws IOException {
    FileReader fr = new FileReader(thornList);
    BufferedReader br = new BufferedReader(fr);
    List<String> thorns = new ArrayList<String>();
    for (String thorn = br.readLine(); thorn != null; thorn = br.readLine()) {
      thorn = thorn.replaceAll("#.*", "").trim();
      if (thorn.startsWith("#"))
        continue;
      if (thorn.startsWith("!"))
        continue;
      if (thorn.equals(""))
        continue;
      thorns.add(thorn);
    }
    return thorns;
  }

  public static void usage() {
    throw new Error("Usage: CCLParser cclFile templateCclFile thornList arrangementDir cctkHome");
  }

  static class GroupInfo {
    final Group g;
    final File f;
    public GroupInfo(Group g,File f) {
      this.g = g;
      this.f = f;
    }
  }

  public void parseInterface(final File intrCclFile,final File arrangementDir,
      final List<String> thorns) throws Exception {
    final Map<String, GroupInfo> interfaces = new HashMap<String, GroupInfo>();
    for (final String thorn : thorns) {
      File thornDir = new File(arrangementDir, thorn);
      File iface = new File(thornDir, "interface.ccl");
      if (intrCclFile.equals(iface))
        continue;
      Matcher m = g_intr.matcher(Grammar.readContents(iface));
      if (!m.matches())
        throw new RuntimeException("File: " + iface + " " + m.near());
      Group impl = m.get("FUNC_GROUP", "IMPLEMENTS", "name");
      interfaces.put(impl.substring().toLowerCase(), new GroupInfo(m.group(),iface));
    }
    vars = new LinkedHashMap<String, Var>();
    m_intr = g_intr.matcher("intr", Grammar.readContents(intrCclFile));
    if (!m_intr.match(0))
      throw new RuntimeException("File: "+intrCclFile+" Error near line "
          + m_intr.near());

    processInterface(m_intr, interfaces, false);
  }

  // TODO: Handle protected
  private void processInterface(Group ifaceGrp,
      Map<String, GroupInfo> interfaces, boolean publicOnly)
    throws IOException {
      String impl = ifaceGrp.get("FUNC_GROUP", "IMPLEMENTS", "name")
        .substring();
      Group inherits = ifaceGrp.get("FUNC_GROUP", "INHERITS");

      if (inherits != null) {
        for (int i = 0; i < inherits.groupCount(); i++) {
          String inheritsName = inherits.group(i).substring()
            .toLowerCase();
          GroupInfo gi = interfaces.get(inheritsName);
          modified(gi.f);
          processInterface(gi.g, interfaces, true);
        }
      }
      Var.Access vaccess = null;
      Group g2 = null;
      for (int j = 0; j < ifaceGrp.groupCount(); j++) {
        g2 = ifaceGrp.group(j);
        if (!"FUNC_GROUP".equals(g2.getPatternName()))
          continue;

        for (int i = 0; i < g2.groupCount(); i++) {
          Group g = g2.group(i);
          if ("access".equals(g.getPatternName())) {
            String str = g.substring().toLowerCase();
            if ("protected".equals(str))
              vaccess = Access.Protected;
            else if ("private".equals(str))
              vaccess = Access.Private;
            else if ("public".equals(str))
              vaccess = Access.Public;
            else
              throw new RuntimeException("Wrong access type: " + str);
          } else if ("GROUP_VARS".equals(g.getPatternName())) {
            Var var;
            if (vaccess != null)
              var = new Var(g, vaccess, impl);
            else
              var = new Var(g, impl);
            // System.err.println("FOUND GROUP VAR" + var.name +
            // " vaccess: "+ vaccess);
            // for (String name : var.varMembers) {
            // System.err.println("FOUND VAR (" + name + ":" + var.name
            // + ") " );
            // }
            if ((publicOnly && vaccess == Access.Public) || !publicOnly) {
              vars.put(var.name, var);
              for (String name : var.varMembers) {
                vars.put(name, var);
                // System.err.println("PUTING VAR (" + name + ":" +
                // var.name + ") " );
              }
            }
          }
        }
      }

    }

  // TODO: Take into account shares/restricted
  public void parseParam(File parmCclFile, boolean globalOnly)
    throws Exception {
      m_parm = g_parm.matcher("pars", Grammar.readContents(parmCclFile));
      if (!m_parm.match(0))
        throw new RuntimeException("File: "+parmCclFile+" Error near line "
            + m_parm.near());

      if (pars == null) {
        pars = new LinkedHashMap<String, Param>();
      }

      Param.Access paccess = null;
      for (int i = 0; i < m_parm.groupCount(); i++) {
        Group g = m_parm.group(i);
        if ("access".equals(g.getPatternName())) {
          String str = g.substring().toLowerCase();
          str = str.replaceFirst(":.*", "").trim();
          if (str.startsWith("global"))
            paccess = Param.Access.Global;
          else if (str.startsWith("private"))
            paccess = Param.Access.Private;
          else if (str.startsWith("restricted"))
            paccess = Param.Access.Restricted;
          else if (str.startsWith("shares"))
            paccess = Param.Access.Shares;
          else
            throw new RuntimeException("Wrong access type: " + str);
        } else {
          Param par;
          if (paccess != null)
            par = new Param(g, paccess);
          else
            par = new Param(g);
          if (globalOnly) {
            if (paccess == Param.Access.Global)
              pars.put(par.name, par);
          } else {
            pars.put(par.name, par);
          }
        }
      }

    }

  public void parseFiles(File cclFile, File depsCclFile, File templateDir)
    throws Exception {
      src = new File(cclFile.getParentFile(), "src");
      this.templateDir = templateDir;
      root = new TemplateGroup("root");
      root.file = templateDir;
      match(cclFile, Grammar.readContents(depsCclFile));
    }

  List<KernelData> kernels = new LinkedList<KernelData>();

  public void match(File cclFile, String str_deps) throws Exception {
    String str = Grammar.readContents(cclFile);
    kernels = new LinkedList<KernelData>();
    m = g.matcher("KERNELS", str);
    m_deps = g_deps.matcher("TEMPLATE_GROUPS", str_deps);

    if (m_deps.match(0)) {
      root.parseInput(m_deps);
    } else
      throw new RuntimeException("File: "+cclFile+" Error in deps file, near line "
          + m_deps.near());

    if (m.match(0)) {
      processMatch();
      generateFiles();
    } else
      throw new RuntimeException("File: "+cclFile+" Error near line " + m.near());
  }

  private void insertGroupScope(
      Map<String, Pair<LinkedHashSet<Node>, LinkedHashSet<KernelData>>> groupScope,
      Pair<Node, KernelData> p, String path) {
    String groupPath = path.replaceAll("/.*", "");
    Pair<LinkedHashSet<Node>, LinkedHashSet<KernelData>> pair = groupScope
      .get(groupPath);
    if (pair == null)
      pair = new Pair<LinkedHashSet<Node>, LinkedHashSet<KernelData>>(
          new LinkedHashSet<Node>(), new LinkedHashSet<KernelData>());
    pair.first.add(p.first);
    pair.second.add(p.second);
    groupScope.put(groupPath, pair);
  }

  // private void insertThornScope(Map<String, Pair<LinkedHashSet<Node>,
  // LinkedHashSet<KernelData>>> thornScope, Pair<Node, KernelData> p, String
  // path){
  // String name = p.first.name;
  // Pair<LinkedHashSet<Node>, LinkedHashSet<KernelData>> pair =
  // thornScope.get(name);
  // if(pair == null) pair =
  // new Pair<LinkedHashSet<Node>, LinkedHashSet<KernelData>>(new
  // LinkedHashSet<Node>(), new LinkedHashSet<KernelData>());
  // pair.first.add(p.first);
  // pair.second.add(p.second);
  // thornScope.put(p.first.name, pair);
  // }

  private void generateFiles() throws Exception {
    // Map<String, Pair<LinkedHashSet<Node>, LinkedHashSet<KernelData>>>
    // thornScope =
    // new LinkedHashMap<String,
    // Pair<LinkedHashSet<Node>,LinkedHashSet<KernelData>>>();
    Set<Node> thornNodes = new LinkedHashSet<Node>();
    Map<String, Pair<LinkedHashSet<Node>, LinkedHashSet<KernelData>>> groupScope = new LinkedHashMap<String, Pair<LinkedHashSet<Node>, LinkedHashSet<KernelData>>>();
    Map<String, Node> fileScope = new LinkedHashMap<String, Node>();
    Map<String, Pair<Node, KernelData>> evaluatedTemplates = new LinkedHashMap<String, Pair<Node, KernelData>>();

    StringBuffer scheduleBuffer = new StringBuffer();

    for (KernelData kd : kernels) {
      LinkedHashMap<String, Node> pathToNodes = new LinkedHashMap<String, Node>();
      String type = kd.attrs.get("TYPE");
      root.findAll(type, "", pathToNodes);
      if (pathToNodes.isEmpty())
        throw new RuntimeException(
            "Cannot find proper path to the template: " + type);

      for (Map.Entry<String, Node> entry : pathToNodes.entrySet()) {
        Template template = null;
        try {
          template = (Template) (entry.getValue());
        } catch (Exception e) {
          throw new RuntimeException(
              "Only template files can be pointed in CCL file so far.");
        }

        switch (entry.getValue().scope) {
          case group:
            insertGroupScope(groupScope, new Pair<Node, KernelData>(
                  entry.getValue(), kd), entry.getKey());
            break;
          case thorn:
            // insertThornScope(thornScope, new Pair<Node,
            // KernelData>(entry.getValue(), kd), entry.getKey());
            thornNodes.add(entry.getValue());
            break;
          default:
            fileScope.put(entry.getKey(), entry.getValue());
        }

        int len = fileScope.size(), newlen = 0;

        while (newlen != len) {
          len = fileScope.size();
          LinkedHashMap<String, Node> tmpmap = new LinkedHashMap<String, Node>();
          for (Map.Entry<String, Node> entry2 : fileScope.entrySet()) {
            Node n = entry2.getValue();
            for (Node tmpn : n.triggers.values()) {
              if (tmpn.scope == Node.Scope.group)
                insertGroupScope(groupScope,
                    new Pair<Node, KernelData>(tmpn, kd),
                    entry.getKey());
              else if (tmpn.scope == Node.Scope.thorn)
                // insertThornScope(thornScope, new Pair<Node,
                // KernelData>(tmpn, kd), entry.getKey());
                thornNodes.add(entry.getValue());
              else
                tmpmap.put(tmpn.abspath, tmpn);
            }
          }
          fileScope.putAll(tmpmap);
          tmpmap.clear();
          newlen = fileScope.size();
        }

        Iterator<Map.Entry<String, Node>> it = fileScope.entrySet()
          .iterator();
        while (it.hasNext()) {
          Map.Entry<String, Node> entry2 = it.next();
          Node n = entry2.getValue();
          Pair<String, String> filePaths = ((Template) n)
            .produceFilenames(kd);
          // since one template may be evaluated for several
          evaluatedTemplates.put(filePaths.second,
              new Pair<Node, KernelData>(n, kd));
          generateFile(kd, filePaths.first, filePaths.second,
              (Template) n, scheduleBuffer,
              !"no".equals(n.attrs.get("replace")));
          it.remove();
        }
      }
    }

    for (Map.Entry<String, Pair<LinkedHashSet<Node>, LinkedHashSet<KernelData>>> entry2 : groupScope
        .entrySet()) {
      int len = groupScope.size(), newlen = 0;
      while (newlen != len) {
        len = entry2.getValue().first.size();
        LinkedHashSet<Node> tmpset = new LinkedHashSet<Node>();
        for (Node n : entry2.getValue().first)
          for (Node tmpn : n.triggers.values()) {
            if (tmpn.scope == Node.Scope.group)
              tmpset.add(tmpn);
            else if (tmpn.scope == Node.Scope.thorn)
              // insertThornScope(thornScope, new Pair<Node,
              // KernelData>(tmpn, kd), entry.getKey());
              thornNodes.add(tmpn);
            else
              throw new RuntimeException(
                  "Cannot schedule file-scope template from group-scope");
          }
        entry2.getValue().first.addAll(tmpset);
        tmpset.clear();
        newlen = entry2.getValue().first.size();
      }
    }

    // evaluate group templates; either scheduled or triggered
    KernelData thornkd = new KernelData(this.thornName, this.thornName);
    for (Map.Entry<String, Pair<LinkedHashSet<Node>, LinkedHashSet<KernelData>>> entry : groupScope
        .entrySet()) {
      KernelData groupkd = new KernelData(entry.getKey(), this.thornName);
      for (KernelData kd : entry.getValue().second) {
        for (VarData vd : kd.vars.values()) {
          if (groupkd.vars.containsKey(vd.name)) {
            VarData vdtmp = groupkd.vars.get(vd.name);
            if (!"separateinout".equals(vdtmp.getProperty(vd.name,
                    "intent").toUpperCase()))
              groupkd.vars.put(vd.name, vd);
          } else
            groupkd.vars.put(vd.name, vd);
          if (thornkd.vars.containsKey(vd.name)) {
            VarData vdtmp = thornkd.vars.get(vd.name);
            if (!"separateinout".equals(vdtmp.getProperty(vd.name,
                    "intent").toUpperCase()))
              thornkd.vars.put(vd.name, vd);
          } else
            thornkd.vars.put(vd.name, vd);
        }
        groupkd.parameters.addAll(kd.parameters);
        thornkd.parameters.addAll(kd.parameters);
      }
      for (Node n : entry.getValue().first) {
        Pair<String, String> filePaths = ((Template) n)
          .produceFilenames(groupkd, Node.Scope.group);
        generateFile(groupkd, filePaths.first, filePaths.second,
            (Template) n, scheduleBuffer,
            !"no".equals(n.attrs.get("replace")));
        evaluatedTemplates.put(filePaths.second,
            new Pair<Node, KernelData>(n, groupkd));
      }
    }

    int len = thornNodes.size(), newlen = 0;
    List<Node> tmpnodes = new LinkedList<Node>();
    while (newlen != len) {
      for (Node n : thornNodes) {
        for (Node tmpn : n.triggers.values()) {
          if (tmpn.scope == Node.Scope.thorn)
            tmpnodes.add(tmpn);
          //            thornNodes.add(tmpn);
          else
            throw new RuntimeException(
                "From thorn scope only the thorn group might be scheduled");
        }
      }
      thornNodes.addAll(tmpnodes);
      tmpnodes.clear();
      newlen = len;
      len = thornNodes.size();
    }

    for (Node n : thornNodes) {
      Pair<String, String> filePaths = ((Template) n).produceFilenames(
          thornkd, Node.Scope.thorn);
      generateFile(thornkd, filePaths.first, filePaths.second,
          (Template) n, scheduleBuffer,
          !"no".equals(n.attrs.get("replace")));
      evaluatedTemplates.put(filePaths.second,
          new Pair<Node, KernelData>(n, thornkd));
    }
  }

  private KernelData makeMasterKernel(String type) {
    KernelData master = new KernelData("master", this.thornName);
    master.attrs.put("TYPE", type);
    for (KernelData kd : kernels) {
      for (VarData vd : kd.vars.values()) {
        if (master.vars.containsKey(vd.name)) {
          VarData vdtmp = master.vars.get(vd.name);
          if (!"separateinout".equals(vdtmp.getProperty(vd.name,
                  "intent").toLowerCase())) {
            master.vars.put(vd.name, vd);
          }
        } else
          master.vars.put(vd.name, vd);
      }
    }
    return master;
  }

  private int[] parseArray(String in, int[] def) {
    if (in == null)
      return def;
    Matcher m = g.matcher("digit", in);
    int pos = 0;
    int[] array = new int[def.length];
    int index = 0;

    while (m.find(pos)) {
      array[index++] = Integer.parseInt(m.substring());
      pos = m.getEnd();
    }
    return array;
  }

  private void generateFile(final KernelData kd, final String templatePath,
      final String dst, Template template, StringBuffer scheduleBuffer)
    throws IOException {
      generateFile(kd, templatePath, dst, template, scheduleBuffer, true);
    }

  private void generateFile(final KernelData kd, final String templatePath,
      final String dst, Template template, StringBuffer scheduleBuffer,
      boolean replace) throws IOException {
    final String type = kd.attrs.get("TYPE");
    out = new OutputGen();
    File inFile = new File(templatePath);
    if (!inFile.exists()) {
      System.err.println("Error: file " + inFile.getAbsolutePath()
          + " does not exist");
      usage();
    }

    File userFile = new File(src,dst);

    File bindings = new File(scratchBuild,"bindings");
    File include = new File(bindings,"include");

    File thornDir = src.getParentFile();
    String thorn = thornDir.getName();

    File outDir = new File(new File(include,thorn),"AUX");

    final File outFile = new File(outDir, dst);

    if(targetFile != null && !targetFile.equals(outFile))
        return;

    outDir.mkdirs();
    out.readSource(inFile);

    File parserDeps = new File(outDir,outFile.getName()+".parser.deps");

    Set<File> deps = dependencies.get(inFile);
    if(deps == null) {
      deps = new HashSet<File>();
      dependencies.put(inFile,deps);
    }
    deps.add(outFile);
    outDirs.put(inFile,outDir);

    mkAux(userFile,outFile);
    if(userFile.exists() && !replace) {
      return;
    }

    if(outFile.exists()) {
      Date outfileDate = new Date(outFile.lastModified());
      Date inFileDate = new Date(inFile.lastModified());
      Date globalDate = new Date(lastModified);
      if(outfileDate.compareTo(globalDate) > 0 && outfileDate.compareTo(inFileDate) > 0) {
        return;
      }
    }

    // System.out.println(" Using Data: " + kd);

    final int[] tile = parseArray(kd.attrs.get("TILE"), new int[] { 16,
        16, 16 });
    final int[] stencil = parseArray(kd.attrs.get("STENCIL"),
        new int[] { 1, 1, 1, 1, 1, 1 });

    // BufferedWriter bw = new BufferedWriter(fw);
    Object f = new CCLFormater(kd, vars, pars, tile, outFile, stencil,
        g, template, thornName);

    StringWriter sw = new StringWriter();
    // FileWriter fw = new FileWriter(outFile);
    // BufferedWriter bw = new BufferedWriter(fw);
    DefConWriter dcw = new DefConWriter(sw);

    for (Pair<String, Node> p : template.include) {
      Node n = p.second;
      Pair<String, String> files = ((Template) n).produceFilenames(
          kd, n.scope);
      sw.write("#include\"");
      sw.write(files.second);
      sw.write("\"\n");
    }

    out.output(dcw, OutputGen.DEFAULT_TEMPLATE, f);

    String outstr = sw.toString();
    boolean write = false;

    if(outFile.exists()) {
      CharBuffer tmpbuff = CharBuffer.allocate((int)outFile.length());
      FileReader tmpreader = new FileReader(outFile);
      tmpreader.read(tmpbuff); tmpreader.close();
      String oldstr = tmpbuff.rewind().toString();
      if(!outstr.equals(oldstr)) {
        write = true;
      }
    } else {
      write = true;
    }

    if(write) {
      System.err.println("Creating file " + outFile);
      FileWriter fw = new FileWriter(outFile);
      fw.write(outstr);
      fw.close();
    } else {
      touch(outFile);
    }
  }

  private void generateParserDeps() throws IOException {
    for(Map.Entry<File,Set<File>> entry : dependencies.entrySet()) {
      File inFile = entry.getKey();
      File parserDeps = new File(outDirs.get(inFile),inFile.getName()+".parser.deps");
      FileWriter parserDepsF = new FileWriter(parserDeps);
      BufferedWriter parserDepsB = new BufferedWriter(parserDepsF);
      PrintWriter parserDepsPW = new PrintWriter(parserDepsB);

      for(File outFile : entry.getValue()) {
        parserDepsPW.print(outFile);
        parserDepsPW.print(' ');
      }

      parserDepsPW.print(":: ");
      parserDepsPW.print(inFile);
      parserDepsPW.println();

      parserDepsPW.println("\techo INVOKING CCLParser FOR $@");
      parserDepsPW.print("\tjava -cp ");
      parserDepsPW.print(System.getProperty("java.class.path"));
      parserDepsPW.println(" edu.lsu.cct.cakernel.CCLParser \\");

      parserDepsPW.print("\t\t");
      parserDepsPW.print(cakernelCcl);
      parserDepsPW.println(" \\");

      parserDepsPW.print("\t\t");
      parserDepsPW.print(depsCcl);
      parserDepsPW.println(" \\");

      parserDepsPW.print("\t\t");
      parserDepsPW.print(thornList);
      parserDepsPW.println(" \\");

      parserDepsPW.print("\t\t");
      parserDepsPW.print(arrangementDir);
      parserDepsPW.println(" \\");

      parserDepsPW.print("\t\t");
      parserDepsPW.print(cctkHome);
      parserDepsPW.println(" \\");

      parserDepsPW.print("\t\t");
      parserDepsPW.print(scratchBuild);
      parserDepsPW.println(" \\");

      parserDepsPW.println("\t\t$@");

      parserDepsPW.close();
    }
  }

  private static boolean touch(File f) {
    if(f.exists()) {
      f.setLastModified(System.currentTimeMillis());
    } else {
      try {
        FileWriter fw = new FileWriter(f,true);
        fw.close();
      } catch(IOException ioe) {
        return false;
      }
    }
    return true;
  }

  private void processVar(KernelData kd, Group m) {
    Map<String, String> attrs = new HashMap<String, String>();
    int stencil[] = null; 
    for (int i = 0; i < m.groupCount(); i++) {
      String p = m.group(i).getPatternName();

      if ("key".equals(p)) {
        if(m.group(i).substring().toUpperCase().equals("STENCIL"))
          stencil = parseArray(m.group(i + 1).substring(), new int[]{1000,1000,1000,1000,1000,1000});
        attrs.put(m.group(i).substring().toUpperCase(), m.group(i + 1)
            .substring());
      } else if ("name".equals(p)) {
        VarData vd = new VarData(m.group(i).substring(), stencil);

        vd.attrs = attrs;
        kd.vars.put(vd.name, vd);
      }
    }
  }

  void processMatch() {
    for (int i = 0; i < m.groupCount(); i++) {
      processKernel(m.group(i));
    }
  }

  private void processKernel(Group m) {
    boolean typeSet = false;
    List<String> types = new LinkedList<String>();
    do {
      typeSet = false;
      KernelData kd = null;
      for (int i = 0; i < m.groupCount(); i++) {
        String p = m.group(i).getPatternName();
        if ("name".equals(p)) {
          kd = new KernelData(m.group(i).substring(), this.thornName);
        } else if ("key".equals(p)) {
          if ("type".equals(m.group(i).substring().toLowerCase())) {
            if (typeSet) {
              System.err
                .println("Only one template per kernel permited, skipping: "
                    + m.group(i).substring()
                    + "="
                    + m.group(i + 1).substring());
              continue;
            }
            if (m.group(i + 1).group(0).getPatternName()
                .equals("mvalue")) {
              if (types.size() == 0) {
                Group m2 = m.group(i + 1).group(0);
                for (int j = 0; j < m2.groupCount(); j++)
                  types.add(m2.group(j).substring());
              }
              if (types.size() != 0) {
                kd.attrs.put(m.group(i).substring(),
                    types.remove(0));
                typeSet = true;
              }
            }
            if (!typeSet) {
              kd.attrs.put(m.group(i).substring(), m.group(i + 1)
                  .substring());
              typeSet = true;
            }
          } else if ("exterior".equals(m.group(i).substring().toLowerCase())){ 
            kd.exterior = parseArray(m.group(i + 1).substring(), new int[] {1,1,1,1,1,1});
          } else
            kd.attrs.put(m.group(i).substring(), m.group(i + 1)
                .substring());
        } else if ("PAR".equals(p)) {
          processPar(kd, m.group(i));
        } else if ("VAR".equals(p)) {
          processVar(kd, m.group(i));
        }
      }
      kernels.add(kd);
      // System.err.println("Pushed kernel: " + kd.name + ":" +
      // kd.getProperty("", "TYPE"));
    } while (types.size() > 0);
  }

  private void processPar(KernelData kd, Group m) {
    for (int i = 0; i < m.groupCount(); i++) {
      if (m.group(i).getPatternName().equals("name")) {
        kd.parameters.add(m.group(i).substring());
      }
    }
  }

  private void mkAux(File userFile,File outFile) throws IOException {
    File auxFileDir = outFile.getParentFile();
    File auxMk = new File(auxFileDir,"aux.mk");
    if(!auxMk.exists()) {
      FileWriter fw = new FileWriter(auxMk);
      PrintWriter pw = new PrintWriter(fw);
      pw.println("THORN_SRC_DIR = "+userFile.getParent());
      pw.println("include $(MAKE_CONFIG_DEPS_PART2_PATH)");
      pw.close();
    }
  }
}
