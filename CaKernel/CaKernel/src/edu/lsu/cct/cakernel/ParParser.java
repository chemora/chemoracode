package edu.lsu.cct.cakernel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import edu.lsu.cct.piraha.Grammar;
import edu.lsu.cct.piraha.Group;
import edu.lsu.cct.piraha.Matcher;

/**
 * This class provides a list of all parameters available by thorn name
 * given a ThornList and an arrangements directory.
 * @author sbrandt
 *
 */
public class ParParser {
	public final static URL paramPEG =
			CCLParser.class.getResource("/edu/lsu/cct/pegs/paramccl.peg");
	public final static URL interfacePEG =
			CCLParser.class.getResource("/edu/lsu/cct/pegs/intrfccl.peg");
	
	Grammar paramGrammar = new Grammar();
	Grammar interfaceGrammar = new Grammar();
	
	public ParParser() throws IOException {
		paramGrammar.compileFile(paramPEG.openStream());
		interfaceGrammar.compileFile(interfacePEG.openStream());
	}
	
	public Map<String,Map<String,Param>> parseParameters(File thornFile,File arrangementDir) throws IOException {
		Map<String,Group> paramFiles = new HashMap<String,Group>();
		Map<String,List<String>> inherits = new HashMap<String,List<String>>();
		Map<String,String> impls = new HashMap<String,String>();
		
		Map<String,Map<String,Param>> visibleParametersByThorn = new HashMap<String,Map<String,Param>>();
		FileReader fr = new FileReader(thornFile);
		BufferedReader br = new BufferedReader(fr);
		for(String thornName=br.readLine();thornName!=null;thornName=br.readLine()) {
			thornName = thornName.trim();
			if("".equals(thornName))
				continue;
			if(thornName.startsWith("#"))
				continue;
			if(thornName.startsWith("!"))
				continue;
			visibleParametersByThorn.put(thornName, new HashMap<String,Param>());
			File thornDir = new File(arrangementDir,thornName);
			File paramCCL = new File(thornDir,"param.ccl");
			String paramCCLContents = Grammar.readContents(paramCCL);
			Matcher m = paramGrammar.matcher(paramCCLContents);
			if(!m.matches()) {
				System.out.println(m.near());
				break;
			}
			paramFiles.put(thornName, m.group());
			
			for(int i=0;i<m.groupCount();i++) {
				if(m.group(i).getPatternName().endsWith("par")) {
					if(m.group(i).group(0).getPatternName().equals("uses_or_extends"))
						;
					else {
						Map<String,Param> params = visibleParametersByThorn.get(thornName);
						params.put(m.group(i).group(0).group(0).substring(),new Param(m.group(i)));
					}
				}
			}
			
			File interfaceCCL = new File(thornDir,"interface.ccl");
			String interfaceContents = Grammar.readContents(interfaceCCL);
			m = interfaceGrammar.matcher(interfaceContents);
			if(!m.matches()) {
				System.out.println(m.near());
				break;
			}
			Group impl = m.get("entries","HEADER","IMPLEMENTS","name");
			if(impl == null) {
				m.dumpMatches();
				break;
			}
			
			// assume all implementations are equivalent
			impls.put(impl.substring().toLowerCase(),thornName);
			
			Group inherit = m.get("entries","HEADER","INHERITS");
			if(inherit != null) {
				List<String> inheritList = null;
				if(!inherits.containsKey(thornName))
					inherits.put(thornName, inheritList = new ArrayList<String>());
				else
					inheritList = inherits.get(thornName);
				for(int i=0;i<inherit.groupCount();i++) {
					inheritList.add(inherit.group(i).substring().toLowerCase());
				}
			}
		}
		for(Map.Entry<String, List<String>> inheritorList : inherits.entrySet()) {
			Map<String,Param> baseParams = visibleParametersByThorn.get(inheritorList.getKey());
			for(String inheritor : inheritorList.getValue()) {
				String thornName = impls.get(inheritor);
				Map<String,Param> moreParams = visibleParametersByThorn.get(thornName);
				if(moreParams == null) {
					throw new Error("missing "+thornName+" from "+inheritor);
				}
				for(Entry<String, Param> param : moreParams.entrySet()) {
					baseParams.put(param.getKey(),param.getValue());
				}
			}
		}
		
		return visibleParametersByThorn;
	}
	// debugging code
//	public static void main(String[] args) throws IOException {
//		File arrangements = new File("/home/sbrandt/workspace/.CactusET.mojave/Cactus/arrangements/");
//		File thornFile = new File("/home/sbrandt/workspace/.CactusET.mojave/Cactus/configs/Cfg1/ThornList");
//		ParParser pp = new ParParser();
//		System.out.println(pp.parseParameters(thornFile, arrangements));
//	}
}