#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include<assert.h>
#include "Common_mem.hpp"

//#include <algorithm>
//#include <cassert>
//#include <cmath>
//#include <cstdlib>
//#include <iostream>


namespace CaKernel {

#define SRCIND3(i,j,k)                                  \
  (i) + (srcipadext) * ((j) + (srcjpadext) * (k))
#define DSTIND3(i,j,k)                                  \
  (i) + (dstipadext) * ((j) + (dstjpadext) * (k))
 
  
  __global__
  void DEVICE_Restrict(
      CCTK_REAL const * restrict src, int srcipadext, int srcjpadext, //int srckpadext,
      CCTK_REAL       * restrict dst, int dstipadext, int dstjpadext, //int dstkpadext,
                                            int ni, int nj, int nk){
    const int gdi = threadIdx.x;
    const int gdk = blockIdx.y ;

    const int gsi = threadIdx.x * 2;
    const int gsk = blockIdx.y * 2;

    src = &src[SRCIND3(gsi, 0, gsk)];
    dst = &dst[DSTIND3(gdi, 0, gdk)];

    for (int j = 0; j < nj; ++j)
      dst[j * dstipadext] = src[j * srcipadext * 2];
  } 
  
  
 
  extern "C"
  CCTK_INT
  Device_RestrictCaKernel (const CCTK_POINTER_TO_CONST cctkGH_,
                           const CCTK_INT * restrict const vars,
                           const CCTK_INT rl,
                           const CCTK_INT nvars,
                           const CCTK_INT * restrict const srcbbox,
                           const CCTK_INT * restrict const dstbbox,
                           const CCTK_INT * restrict const regbbox)
  {
//    const cGH * restrict const cctkGH = static_cast<const cGH*> (cctkGH_);
    
    // Restrict nvars variables.
    // The variable indices are in the array vars.
    // The source refinement level is rl+1, the destination refinement
    // level is rl.

    //TODO Erik where from to get tl? 
#warning the value of tl should be set up from somewhere else...
    int tl = 0;

    for(int var = 0; var < nvars; var++){
      assert(rl >= 0);
      assert(rl + 1>= 0);
      
      const ptr_t * src_p = CaKernel_GetVarPtrI_rl((void *)cctkGH_, vars[var], rl + 1, tl); 
        assert(src_p);
      const ptr_t * dst_p = CaKernel_GetVarPtrI_rl((void *)cctkGH_, vars[var], rl, tl); 
        assert(dst_p);
      
      CCTK_REAL * restrict src = (CCTK_REAL *) src_p -> ptr;
      CCTK_REAL * restrict dst = (CCTK_REAL *) dst_p -> ptr;
      
      // Shape (including padding) of the source variables
      ptrdiff_t const srcipadext = src_p->ash[0];
      ptrdiff_t const srcjpadext = src_p->ash[1];
  //  ptrdiff_t const srckpadext = src_p->ash[2];
      
      // Shape (including padding) of the destination variables
      ptrdiff_t const dstipadext = dst_p->ash[0];
      ptrdiff_t const dstjpadext = dst_p->ash[1];
  //  ptrdiff_t const dstkpadext = dst_p->ash[2];
      
      // Region size in the destination variables that should be set
      ptrdiff_t const regiext = (regbbox[3] - regbbox[0]) / regbbox[6] + 1;
      ptrdiff_t const regjext = (regbbox[4] - regbbox[1]) / regbbox[7] + 1;
      ptrdiff_t const regkext = (regbbox[5] - regbbox[2]) / regbbox[8] + 1;
      
      ptrdiff_t const srcioff = (regbbox[0] - srcbbox[0]) / regbbox[6];
      ptrdiff_t const srcjoff = (regbbox[1] - srcbbox[1]) / regbbox[7];
      ptrdiff_t const srckoff = (regbbox[2] - srcbbox[2]) / regbbox[8];
      
      ptrdiff_t const dstioff = (regbbox[0] - dstbbox[0]) / regbbox[6];
      ptrdiff_t const dstjoff = (regbbox[1] - dstbbox[1]) / regbbox[7];
      ptrdiff_t const dstkoff = (regbbox[2] - dstbbox[2]) / regbbox[8];
      
      
      assert (1 == SRCIND3(1,0,0) - SRCIND3(0,0,0));
      assert (1 == DSTIND3(1,0,0) - DSTIND3(0,0,0));
  
      dst = &dst[DSTIND3(dstioff, dstjoff, dstkoff)];
      src = &src[SRCIND3(srcioff, srcjoff, srckoff)];
  
      DEVICE_Restrict<<<regiext, regkext>>> (src, srcipadext, srcjpadext, dst, dstipadext, dstjpadext, regiext, regjext, regkext);
    }

    return 0;
  }
  
} // namespace CaKernel
