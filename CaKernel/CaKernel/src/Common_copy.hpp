#pragma once

#include"Common_mem.hpp"
#include"CUDA_mem.hpp"
#include"CUDA_copy.hpp"

int CaKernel_CopyDir
(const cGH* cctkGH, int vi, int rl, int tl, int todev, pointer_t type);

int CaKernel_CopyInDev
(const cGH* cctkGH, int srci, int srcrl, int srctl,
 int dsti, int dstrl, int dsttl, pointer_t type);

// int CaKernel_CopyToDev( CCTK_ARGUMENTS, int vi, int tl, pointer_t type);

// int CaKernel_CopyFromDev(CCTK_ARGUMENTS, int vi, int tl, pointer_t type);

// int CaKernel_CopyToDev_Boundary( CCTK_ARGUMENTS, int vi, int tl, pointer_t type);

// int CaKernel_CopyFromDev_Boundary( CCTK_ARGUMENTS, int vi, int tl, pointer_t type);
