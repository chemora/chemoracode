
#include "Common_copy.hpp"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<set>
#include <Timer.hh>
using namespace std;
/**********************************************************************
 * These functions are called (via function-aliasing) from Carpet at
 * certain points in the execution.
 **********************************************************************/

// If true scan for non-finite FP in some key places.
static bool nanscan = false;

extern "C"
int
CaKernel_CreateVariables(CCTK_POINTER_TO_CONST const cctkGH_,
                              CCTK_INT const vis[],
                              CCTK_INT const rls[],
                              CCTK_INT const tls[],
                              CCTK_INT const nvars)
{
  return 0;
}

extern "C"
int CaKernel_CopyToDev2(CCTK_POINTER_TO_CONST const cctkGH_,
                       CCTK_INT const vis[],
                       CCTK_INT const rls[],
                       CCTK_INT const tls[],
                       CCTK_INT const nvars,
                       CCTK_INT *const moved)
{
  DECLARE_CCTK_PARAMETERS;

  if (verbose && nvars || veryverbose){
    printf("INFO (%s): Copying %d variables to device", CCTK_THORNSTRING, nvars);
  }

  for (int i = 0; i < nvars; i++)
  {
    CaKernel_CopyDir((const cGH *) cctkGH_, vis[i], rls[i], tls[i], 1, cuda);
  }
  *moved = 0;
  if(verbose && nvars || veryverbose) printf ("\n");
  return 0;
} 

extern "C"
int CaKernel_CopyFromDev2(CCTK_POINTER_TO_CONST const cctkGH_,
                         CCTK_INT const vis[],
                         CCTK_INT const rls[],
                         CCTK_INT const tls[],
                         CCTK_INT const nvars,
                         CCTK_INT *const moved)
{
  DECLARE_CCTK_PARAMETERS;

  if (verbose && nvars || veryverbose){
    printf("INFO (%s): Copying %d variables to host", CCTK_THORNSTRING, nvars);
  }

  for (int i = 0; i < nvars; i++)
  {
    CaKernel_CopyDir((const cGH *) cctkGH_, vis[i], rls[i], tls[i], 0, cuda);
  }
  *moved = 0;
  if(verbose && nvars || veryverbose) printf ("\n");
  return 0;
} 

 
extern "C"
CCTK_INT
CaKernel_CopyCycle(CCTK_POINTER_TO_CONST const cctkGH_,
                   CCTK_INT const vis[],
                   CCTK_INT const rls[],
                   CCTK_INT const tls[],
                   CCTK_INT const nvars)
{
 cGH * cctkGH = (cGH *) cctkGH_;
 DECLARE_CCTK_PARAMETERS;

 if (cycle_by_pointer_rotation) {
   // Cycle by pointer rotation
   set<int> toprocess;
   if(veryverbose) printf("Cycling: %d\n", nvars) ;

   for(int i = 0; i< nvars; i++) assert( rls[i] == 0 );

   for(int i = 0; i< nvars; i++) 
     toprocess.insert(vis[i]);
  
   for(set<int>::iterator i = toprocess.begin(); i!= toprocess.end(); ++i)  {
     if(veryverbose) printf ("%d:%s ", *i, CCTK_VarName(vis[*i])); 
     CaKernel_Cycle((cGH *) cctkGH_, *i);
   }
   if(veryverbose) printf("\n");
 }
 else {
   // Cycle by memory copy
   for(int i = 0; i < nvars; i++){
     assert(tls[i]);
     int ierr = CaKernel_CopyInDev((cGH *)cctkGH_, vis[i], rls[i], tls[i]-1,
                                   vis[i], rls[i], tls[i], cuda);
     assert(!ierr);
   }
 }
 return 0;
}

extern "C"
CCTK_INT
CaKernel_CopyFromPast(CCTK_POINTER_TO_CONST const cctkGH_,
                      CCTK_INT const vis[],
                      CCTK_INT const rls[],
                      CCTK_INT const tls[],
                      CCTK_INT const nvars)
{
  cGH * cctkGH = (cGH *) cctkGH_;
  DECLARE_CCTK_PARAMETERS;
  if(veryverbose) printf("Copying from past: %d\n", nvars) ;
  for(int i = 0; i < nvars; i++){
    assert(!tls[i]);
    int ierr = CaKernel_CopyInDev((cGH *)cctkGH_,
                                  vis[i], rls[i], tls[i],
                                  vis[i], rls[i], 0, cuda);
    assert(!ierr);
  }
  return 0;
}

/**********************************************************************
 * These functions are internal to CaKernel
 **********************************************************************/

int CaKernel_CopyInDev
(const cGH* cctkGH, int srci, int srcrl, int srctl,
 int dsti, int dstrl, int dsttl, pointer_t type)
{
  assert(srci>=0);
  assert(dsti>=0);

  //printf (" %d:%d", vi, tl);
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const ptr_t * ptr = CaKernel_GetVarPtrI_rl((void *)cctkGH, srci, srcrl, srctl);
  const ptr_t * ptr2= CaKernel_GetVarPtrI_rl((void *)cctkGH, dsti, dstrl, dsttl);

  assert(ptr->np == ptr2->np); 
  assert(ptr->var_size == ptr2->var_size); 
  assert(ptr->vector_length == ptr2->vector_length);

  int not_first = (ptr->ptr != ptr->gptr && ptr->type != CCTK_GF) + 
                  (ptr2->ptr != ptr2->gptr && ptr2->type != CCTK_GF);
  assert(not_first == 0 || not_first == 2);
  if(not_first == 2){
    if(veryverbose) CCTK_VInfo(CCTK_THORNSTRING, "Copy from vi=%d to vi=%d skipped beacuse there are not first variables in group", srci, dsti);
    return 0;
  }

  int ret = -1;
  assert(ptr->ptr && ptr2->ptr);
  size_t vl_gf = ptr->type != CCTK_GF ? ptr->vector_length : 1;

  switch(type) {
    case cuda:
#if HAVE_CUDA == 1
      ret = CaCUDA_CopyInDev(ptr->ptr, ptr2->ptr, ptr->np * ptr->var_size * vl_gf);
#endif
      break;
  }
  return 0;
}

void CaKernel_CopyGroupPointerDir
(const cGH* cctkGH, void * ptr, int rl, int tl, 
              int first_vi, int np, int var_size, int vector_length, 
              int vars_in_group, int topointer){

  size_t single_copy_size = np * var_size * vector_length;
  void * hptr;
  int const c = CCTK_MyProc(cctkGH);
  for(int i = 0; i < vars_in_group; i++){
//    assert(hptr = CCTK_VarDataPtrI(cctkGH, tl, first_vi + i));
    hptr = VarDataPtrI(cctkGH, 0, rl, c, tl, first_vi + i);
    assert(hptr);
    if(topointer)
      memcpy(&(((char *)ptr)[single_copy_size * i]), hptr, single_copy_size);
    else
      memcpy(hptr, &(((char *)ptr)[single_copy_size * i]), single_copy_size);
  }
}

int CaKernel_CopyDir
(const cGH* cctkGH, int vi, int rl, int tl, int todev, pointer_t type)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
    
  static void * tmpptr = 0;
  static size_t pointer_size = 0;

//  int const rl_current = GetRefinementLevel(cctkGH);
//  assert( rl == rl_current ); // Assuming np, etc can vary with rl.

  assert(vi>=0);
  if(verbose) printf (" %s:%d[%d:%d]", CCTK_FullName(vi), vi, tl, rl);
  
//  size_t np, var_size, vector_length = 1, max_tl;
//  size_t offset[dim], length[dim];
//  
//  int gtype, gi, vars_in_group, first_vi;
//
//  assert(!CaKernel_VarTypeHelper(CCTK_PASS_CTOC, vi, &np, &var_size, offset, length, 
//              &vector_length, &max_tl, &gtype,  &gi,  &vars_in_group,  &first_vi));

  const ptr_t * ptr = CaKernel_GetVarPtrI_rl((void *)cctkGH, vi, rl, tl);
  int not_first = (ptr->ptr != ptr->gptr && ptr->type != CCTK_GF);
  if(not_first == 1){
    if(verbose) 
      CCTK_VInfo(CCTK_THORNSTRING, "Copy from/to device vi=%d skipped beacuse it is not first variables in group", vi);
    return 0;
  }

  size_t vl_gf = ptr->type != CCTK_GF ? ptr->vector_length : 1;
  size_t copy_size = ptr->np * ptr->var_size * ptr->vars_in_group * vl_gf;

  void *       hptr = 0;
  if(ptr->type == CCTK_GF){
    int const c = CCTK_MyProc(cctkGH);
    hptr = VarDataPtrI(cctkGH, 0, rl, c, tl, vi);
  }else {
    if(copy_size > pointer_size){
      pointer_size = copy_size * 2;
      tmpptr = realloc(tmpptr, pointer_size);
    }
    hptr = tmpptr;
    if(todev)
      CaKernel_CopyGroupPointerDir
        (cctkGH, tmpptr, rl, tl, vi,
   /* Should be first_vi, but we are coping only if vi==first_vi */
         ptr->np, ptr->var_size, ptr->vector_length, ptr->vars_in_group, 1);
  }

  int ret = -1;
  assert(hptr);
  assert(ptr->ptr);
  switch(type) {
    case cuda:
#if HAVE_CUDA == 1
      if(todev) {
        //if(veryverbose) printf("(from %ld to %ld)", hptr, tmpptr);
        ret = CaCUDA_CopyToDev(hptr, ptr->ptr, copy_size);
      }else{
        //if(veryverbose) printf("(from %ld to %ld)", tmpptr, hptr);
        ret = CaCUDA_CopyFromDev(ptr->ptr, hptr, copy_size);
      }
#endif
      break;
  }
  if(ptr->type != CCTK_GF && !todev){
    CaKernel_CopyGroupPointerDir
      (cctkGH, tmpptr, rl, tl, vi, 
   /* Should be first_vi, but we are coping only if vi==first_vi */
       ptr->np, ptr->var_size, ptr->vector_length, ptr->vars_in_group, 0);
  }
}

static int CaKernel_CopyToDev_Boundary(CCTK_ARGUMENTS, 
                                       int vi, int rl, int tl, pointer_t type)
{
  assert(vi >= 0);
  assert(tl >= 0);

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int const c = CCTK_MyProc(cctkGH);
  void *const hptr = VarDataPtrI(cctkGH, 0, rl, c, tl, vi);
  const ptr_t * ptr = CaKernel_GetVarPtrI_rl(CCTK_PASS_CTOC, vi, rl, tl);
  assert(ptr->ptr && hptr);

  bool is_empty[dim][2];
  int fmin[dim][2][dim];
  int fmax[dim][2][dim];

  // Domain size
  int dmin[dim];
  int dmax[dim];
  for (int d=0; d<dim; ++d) {
    // Whole domain
    dmin[d] = 0;
    dmax[d] = ptr->lsh[d];
  }

  // Regions for each boundary face (empty if not a ghost boundary)
  for (int dir=0; dir<3; ++dir) {
    for (int face=0; face<2; ++face) {
      if (ptr->bbox[2*dir+face]) {
        // Non-ghost boundary: empty region
        is_empty[dir][face] = true;
      } else {
        is_empty[dir][face] = false;
        // Whole domain
        for (int d=0; d<dim; ++d) {
          fmin[dir][face][d] = dmin[d];
          fmax[dir][face][d] = dmax[d];
        }
        // Exclude points that are included in earlier regions
        for (int d=0; d<dim; ++d) {
          if (d > dir) {
            if (!ptr->bbox[2*d+0]) {
              fmin[dir][face][d] += ptr->nghostzones[d];
            }
            if (!ptr->bbox[2*d+1]) {
              fmax[dir][face][d] -= ptr->nghostzones[d];
            }
          }
        }
        // Set extent for this face
        if (face==0) {
          fmin[dir][face][dir] = 0;
          fmax[dir][face][dir] = ptr->nghostzones[dir];
        } else {
          fmax[dir][face][dir] = ptr->lsh[dir];
          fmin[dir][face][dir] = ptr->lsh[dir] - ptr->nghostzones[dir];
        }
        // Check whether region is empty
        for (int d=0; d<dim; ++d) {
          is_empty[dir][face] |= fmax[dir][face][d] <= fmin[dir][face][d];
        }
      }
    }
    // Combine two faces if they are adjacent
    if (!is_empty[dir][0] && !is_empty[dir][1] &&
        fmax[dir][0][dir] >= fmin[dir][1][dir])
    {
      for (int d=0; d<dim; ++d) {
        if (d!=dir) {
          assert(fmin[dir][0][d] == fmin[dir][1][d]);
          assert(fmax[dir][0][d] == fmax[dir][1][d]);
        }
      }
      fmax[dir][0][dir] = fmax[dir][1][dir];
      is_empty[dir][1] = true;
    }
  }

  // Indexing offsets (in bytes)
  int const di = ptr->var_size;
  int const dj = di * ptr->ash[0];
  int const dk = dj * ptr->ash[1];
  size_t pitch[dim-1] = {size_t(dj),size_t(dk)};
  
  // Copy all non-empty regions
  for (int dir=0; dir<3; ++dir) {
    for (int face=0; face<2; ++face) {
      if (!is_empty[dir][face]) {
        
        size_t offset[dim];
        size_t length[dim];
        for (int d=0; d<dim; ++d) {
          offset[d] = fmin[dir][face][d];
          length[d] = fmax[dir][face][d] - fmin[dir][face][d];
        }
        offset[0] *= di;
        length[0] *= di;
        
        int ret = -1;
        switch (type) {
        case cuda:
#if HAVE_CUDA == 1
          ret = CaCUDA_CopyToDev_Rect(hptr, ptr->ptr, pitch, pitch, offset, offset, length);

        if ( nanscan )
          {
            CCTK_REAL* const src = (CCTK_REAL*) hptr;
            for ( int i=0; i<length[0]; i+=di )
              for ( int j=0; j<length[1]; j++ )
                for ( int k=0; k<length[2]; k++ )
                  {
                    const int idx_byte = offset[0] + i
                      + ( offset[1] + j ) * pitch[0]
                      + ( offset[2] + k ) * pitch[1];
                    const int idx = idx_byte / di;
                    assert( idx >= 0 );
                    CCTK_REAL elt = src[idx];
                    assert( isfinite(elt) );
                  }
          }

#endif
          break;
        }
        assert(!ret);
        
      } // if bbox
    }   // if face
  }     // if dir

  return 0;
}

static int CaKernel_CopyFromDev_Boundary
(CCTK_ARGUMENTS, int vi, int rl, int tl, pointer_t type)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int const c = CCTK_MyProc(cctkGH);
  void *const hptr = VarDataPtrI(cctkGH, 0, rl, c, tl, vi);
  const ptr_t * ptr = CaKernel_GetVarPtrI_rl(CCTK_PASS_CTOC, vi, rl, tl);
  assert(ptr->ptr && hptr);

  bool is_empty[dim][2];
  int fmin[dim][2][dim];
  int fmax[dim][2][dim];

  // Domain size without ghost zones
  int dmin[dim];
  int dmax[dim];
  for (int d=0; d<dim; ++d) {
    // Whole domain
    dmin[d] = 0;
    dmax[d] = ptr->lsh[d];
    // Skip ghost points
    if (not ptr->bbox[2*d+0]) {
      dmin[d] += ptr->nghostzones[d];
    }
    if (not ptr->bbox[2*d+1]) {
      dmax[d] -= ptr->nghostzones[d];
    }
  }

  // Regions for each face (empty if at non-ghost boundary)
  for (int dir=0; dir<3; ++dir) {
    for (int face=0; face<2; ++face) {
      if (ptr->bbox[2*dir+face]) {
        // Non-ghost boundary: empty region
        is_empty[dir][face] = true;
      } else {
        is_empty[dir][face] = false;
        // Whole domain
        for (int d=0; d<dim; ++d) {
          fmin[dir][face][d] = dmin[d];
          fmax[dir][face][d] = dmax[d];
        }
        // Exclude points that are included in earlier regions
        for (int d=0; d<dim; ++d) {
          if (d > dir) {
            if (!ptr->bbox[2*d+0]) {
              fmin[dir][face][d] += ptr->nghostzones[d];
            }
            if (!ptr->bbox[2*d+1]) {
              fmax[dir][face][d] -= ptr->nghostzones[d];
            }
          }
        }
        // Set extent for this face
        if (face==0) {
          fmin[dir][face][dir] = ptr->nghostzones[dir];
          fmax[dir][face][dir] = fmin[dir][face][dir] + ptr->nghostzones[dir];
        } else {
          fmax[dir][face][dir] = ptr->lsh[dir] - ptr->nghostzones[dir];
          fmin[dir][face][dir] = fmax[dir][face][dir] - ptr->nghostzones[dir];
        }
        // Check whether region is empty
        for (int d=0; d<dim; ++d) {
          is_empty[dir][face] |= fmax[dir][face][d] <= fmin[dir][face][d];
        }
      }
    }
    // Combine two faces if they are adjacent
    if (!is_empty[dir][0] && !is_empty[dir][1] &&
        fmax[dir][0][dir] >= fmin[dir][1][dir])
    {
      for (int d=0; d<dim; ++d) {
        if (d!=dir) {
          assert(fmin[dir][0][d] == fmin[dir][1][d]);
          assert(fmax[dir][0][d] == fmax[dir][1][d]);
        }
      }
      fmax[dir][0][dir] = fmax[dir][1][dir];
      is_empty[dir][1] = true;
    }
  }

  // Indexing offsets (in bytes)
  int const di = ptr->var_size;
  int const dj = di * ptr->ash[0];
  int const dk = dj * ptr->ash[1];
  size_t pitch[dim-1] = {size_t(dj),size_t(dk)};
  
  // Copy all non-empty regions
  for (int dir=0; dir<3; ++dir) {
    for (int face=0; face<2; ++face) {
      if (!is_empty[dir][face]) {
        
        size_t offset[dim];
        size_t length[dim];
        for (int d=0; d<dim; ++d) {
          offset[d] = fmin[dir][face][d];
          length[d] = fmax[dir][face][d] - fmin[dir][face][d];
        }
        offset[0] *= di;
        length[0] *= di;
        
        int ret = -1;
        switch (type) {
        case cuda:
#if HAVE_CUDA == 1
          ret = CaCUDA_CopyFromDev_Rect
            (ptr->ptr, hptr, pitch, pitch, offset, offset, length);

          if ( nanscan )
            {
              CCTK_REAL* const src = (CCTK_REAL*) hptr;
              for ( int i=0; i<length[0]; i+=di )
                for ( int j=0; j<length[1]; j++ )
                  for ( int k=0; k<length[2]; k++ )
                    {
                      const int idx_byte = offset[0] + i
                        + ( offset[1] + j ) * pitch[0]
                        + ( offset[2] + k ) * pitch[1];
                      const int idx = idx_byte / di;
                      assert( idx >= 0 );
                      CCTK_REAL elt = src[idx];
                      assert( isfinite(elt) );
                    }
            }

#endif
          break;
        }
        assert(!ret);
        
      } // if bbox
    }   // if face
  }     // if dir

  return 0;
}

//void tmpdebug(CCTK_ARGUMENTS, const char * str){
//  DECLARE_CCTK_PARAMETERS
//  DECLARE_CCTK_ARGUMENTS
//  CCTK_REAL * vx = (CCTK_REAL *) CCTK_VarDataPtrB(cctkGH, 0, -1, "cacudacfd3dwomol::vx"); 
//  if(vx){
//    long x = cctk_lsh[0] / 2; 
//    long y = cctk_lsh[1] - 2; 
//    long z = 0;
//    long z_end = 0;
//    if(cctk_bbox[5] != 1){
//      z = cctk_lsh[2] - 2 * cctk_nghostzones[2];
//      z_end = cctk_lsh[2]; 
//    }
//    else if(cctk_bbox[4] != 1)
//      z_end = 2 * cctk_nghostzones[2]; 
//    if(z_end){
//      printf("%s", str);
//      for(long i = z; i < z_end; i++)
//        printf("%f ", vx[CCTK_GFINDEX3D(cctkGH,x,y,i)])
////          printf("(%ld,%ld,%ld) ", x,y,i)
//        ;
//      printf("\n");
//    }
//  }
//}

extern "C"
CCTK_INT
CaKernel_CopyPreSync(CCTK_POINTER_TO_CONST const cctkGH_,
                     CCTK_INT const vis[],
                     CCTK_INT const rls[],
                     CCTK_INT const tls[],
                     CCTK_INT const nvars)
{
  DECLARE_CCTK_PARAMETERS
  static Timers::Timer * timer = 0; 
  if(add_pre_post_sync_timer){
    if(!timer) timer = new Timers::Timer("CaKernel_PreSync");
    timer->start();
  }

  cGH * cctkGH = (cGH *) cctkGH_;
  DECLARE_CCTK_ARGUMENTS

  if(verbose) printf ("INFO (%s): PreSync %d vars: ", CCTK_THORNSTRING, nvars);
  
  for(int i = 0; i < nvars; i++){
    CaKernel_CopyFromDev_Boundary((cGH*)cctkGH_, vis[i], rls[i], tls[i], cuda);
    if(verbose) printf (" %s:%d[%d]", CCTK_VarName(vis[i]), tls[i], rls[i]);
  }

  if(verbose) printf("\n");
  if(add_pre_post_sync_timer){
    timer->stop();
  }

  return 0;
}

extern "C"
CCTK_INT
CaKernel_CopyPostSync(CCTK_POINTER_TO_CONST const cctkGH_,
                      CCTK_INT const vis[],
                      CCTK_INT const rls[],
                      CCTK_INT const tls[],
                      CCTK_INT const nvars)
{
  DECLARE_CCTK_PARAMETERS
  static Timers::Timer * timer = 0; 
  if(add_pre_post_sync_timer){
    if(!timer) timer = new Timers::Timer("CaKernel_PostSync");
    timer->start();
  }

  cGH * cctkGH = (cGH *) cctkGH_;
  DECLARE_CCTK_ARGUMENTS
  
  if(verbose) printf ("INFO (%s): PostSync %d vars: ", CCTK_THORNSTRING, nvars);
//  if(veryverbose){
//    tmpdebug(cctkGH, "Printing vx on boundary (before postsync): ");
//  }

  for(int i = 0; i < nvars; i++){
    CaKernel_CopyToDev_Boundary((cGH*)cctkGH_, vis[i], rls[i], tls[i], cuda);
    if(verbose) printf (" %s:%d[%d]", CCTK_VarName(vis[i]), tls[i],rls[i]);
  }

  if(verbose) printf("\n");
//  if(veryverbose){
//    tmpdebug(cctkGH, "Printing vx on boundary (after postsync): ");
//  }
  if(add_pre_post_sync_timer){
    timer->stop();
  }
  return 0;
}
 
