#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <iostream>

using namespace std;



namespace CaKernel {
  
  
  
  static inline
  ptrdiff_t
  index3 (ptrdiff_t const i, ptrdiff_t const j, ptrdiff_t const k,
          ptrdiff_t const padexti, ptrdiff_t const padextj, ptrdiff_t const padextk,
          ptrdiff_t const exti, ptrdiff_t const extj, ptrdiff_t const extk)
  {
    return i + padexti * (j + padextj * k);
  }
  
#define SRCIND3(i,j,k)                          \
  index3 (i, j, k,                              \
          srcipadext, srcjpadext, srckpadext,   \
          srciext, srcjext, srckext)
#define DSTIND3(i,j,k)                          \
  index3 (i, j, k,                              \
          dstipadext, dstjpadext, dstkpadext,   \
          dstiext, dstjext, dstkext)
  
  
  
  namespace {
    
    // 1D interpolation coefficients
    
    struct coeffs1d {
      static const CCTK_REAL coeffs[];
      
      static ptrdiff_t const ncoeffs = 5+1;
      static ptrdiff_t const imin    = - ncoeffs/2 + 1;
      static ptrdiff_t const imax    = imin + ncoeffs;
      
      static inline
      CCTK_REAL const&
      get (ptrdiff_t const i)
      {
        static_assert (ncoeffs == sizeof coeffs / sizeof *coeffs,
                       "coefficient array has wrong size");
#ifdef CARPET_DEBUG
        assert (i>=imin and i<imax);
#endif
        ptrdiff_t const j = i - imin;
#ifdef CARPET_DEBUG
        assert (j>=0 and j<ncoeffs);
#endif
        return coeffs[j];
      }
    };
    
    const CCTK_REAL coeffs1d::coeffs[] = {
      +  3 / CCTK_REAL(256.0),
      - 25 / CCTK_REAL(256.0),
      +150 / CCTK_REAL(256.0),
      +150 / CCTK_REAL(256.0),
      - 25 / CCTK_REAL(256.0),
      +  3 / CCTK_REAL(256.0)
    };
    
  } // namespace
  
  
  
  // 0D "interpolation"
  static inline
  CCTK_REAL const&
  interp0 (CCTK_REAL const * restrict const p)
  {
    return * p;
  }
  
  // 1D interpolation
  template <int di>
  static inline
  CCTK_REAL
  interp1 (CCTK_REAL const * restrict const p,
           ptrdiff_t const d1)
  {
    typedef coeffs1d coeffs;
    if (di == 0) {
      return interp0 (p);
    } else {
      assert(d1 == 1);         // why would d1 have a non-unit stride?
      CCTK_REAL res = 0.0;
      for (ptrdiff_t i=coeffs::imin; i<coeffs::imax; ++i) {
        res += coeffs::get(i) * interp0 (p + i);
      }
      return res;
    }
  }
  
  // 2D interpolation
  template <int di, int dj>
  static inline
  CCTK_REAL
  interp2 (CCTK_REAL const * restrict const p,
           ptrdiff_t const d1,
           ptrdiff_t const d2)
  {
    typedef coeffs1d coeffs;
    if (dj == 0) {
      return interp1<di> (p, d1);
    } else {
      CCTK_REAL res = 0.0;
      for (ptrdiff_t i=coeffs::imin; i<coeffs::imax; ++i) {
        res += coeffs::get(i) * interp1<di> (p + i*d2, d1);
      }
      return res;
    }
  }
  
  // 3D interpolation
  template <int di, int dj, int dk>
  static inline
  CCTK_REAL
  interp3 (CCTK_REAL const * restrict const p,
           ptrdiff_t const d1,
           ptrdiff_t const d2,
           ptrdiff_t const d3)
  {
    typedef coeffs1d coeffs;
    if (dk == 0) {
      return interp2<di,dj> (p, d1, d2);
    } else {
      CCTK_REAL res = 0.0;
      for (ptrdiff_t i=coeffs::imin; i<coeffs::imax; ++i) {
        res += coeffs::get(i) * interp2<di,dj> (p + i*d3, d1, d2);
      }
      return res;
    }
  }
  
  
  
  extern "C"
  void
  Device_ProlongateCaKernel (const CCTK_POINTER_TO_CONST cctkGH_,
                             const CCTK_INT * restrict const vars,
                             const CCTK_INT rl,
                             const CCTK_INT nvars,
                             const CCTK_REAL * restrict const tfacts,
                             const CCTK_INT num_tls,
                             const CCTK_INT * restrict const srcbbox,
                             const CCTK_INT * restrict const dstbbox,
                             const CCTK_INT * restrict const regbbox)
  {
    const cGH * restrict const cctkGH = static_cast<const cGH*> (cctkGH_);
    
    // Interpolate nvars variables.
    // The variable indices are in the array vars.
    // The source refinement level is rl-1, the destination refinement
    // level is rl.
    // Of the source variables, timelevels 0 ... num_tls-1 are used.
    
    const CCTK_REAL * restrict srcs[num_tls];
    for (int tl=0; tl<num_tls; ++tl) {
      srcs[tl] = TODO;
    }
    CCTK_REAL * restrict const dst = TODO;
    
    // Shape (including padding) of the source variables
    ptrdiff_t const srcipadext = TODO srcpadext[0];
    ptrdiff_t const srcjpadext = TODO srcpadext[1];
    ptrdiff_t const srckpadext = TODO srcpadext[2];
    
    // Shape (including padding) of the destination variables
    ptrdiff_t const dstipadext = TODO dstpadext[0];
    ptrdiff_t const dstjpadext = TODO dstpadext[1];
    ptrdiff_t const dstkpadext = TODO dstpadext[2];
    
    // Extent (excluding padding) of the source variables
    ptrdiff_t const srciext = (srcbbox[3] - srcbbox[0]) / srcbbox[6] + 1;
    ptrdiff_t const srcjext = (srcbbox[4] - srcbbox[1]) / srcbbox[7] + 1;
    ptrdiff_t const srckext = (srcbbox[5] - srcbbox[2]) / srcbbox[8] + 1;
    
    // Extent (excluding padding) of the destination variables
    ptrdiff_t const dstiext = (dstbbox[3] - dstbbox[0]) / dstbbox[6] + 1;
    ptrdiff_t const dstjext = (dstbbox[4] - dstbbox[1]) / dstbbox[7] + 1;
    ptrdiff_t const dstkext = (dstbbox[5] - dstbbox[2]) / dstbbox[8] + 1;
    
    // Region size in the destination variables that should be set
    ptrdiff_t const regiext = (regbbox[3] - regbbox[0]) / regbbox[6] + 1;
    ptrdiff_t const regjext = (regbbox[4] - regbbox[1]) / regbbox[7] + 1;
    ptrdiff_t const regkext = (regbbox[5] - regbbox[2]) / regbbox[8] + 1;
    
    ptrdiff_t const srcioff = (regbbox[0] - srcbbox[0]) / regbbox[6];
    ptrdiff_t const srcjoff = (regbbox[1] - srcbbox[1]) / regbbox[7];
    ptrdiff_t const srckoff = (regbbox[2] - srcbbox[2]) / regbbox[8];
    
    ptrdiff_t const dstioff = (regbbox[0] - dstbbox[0]) / regbbox[6];
    ptrdiff_t const dstjoff = (regbbox[1] - dstbbox[1]) / regbbox[7];
    ptrdiff_t const dstkoff = (regbbox[2] - dstbbox[2]) / regbbox[8];
    
    
    
    ptrdiff_t const fi = srcioff % 2;
    ptrdiff_t const fj = srcjoff % 2;
    ptrdiff_t const fk = srckoff % 2;
    
    ptrdiff_t const i0 = srcioff / 2;
    ptrdiff_t const j0 = srcjoff / 2;
    ptrdiff_t const k0 = srckoff / 2;
    
    
    
    // ptrdiff_t const srcdi = SRCIND3(1,0,0) - SRCIND3(0,0,0);
    ptrdiff_t const srcdi = 1;
    assert (srcdi == SRCIND3(1,0,0) - SRCIND3(0,0,0));
    ptrdiff_t const srcdj = SRCIND3(0,1,0) - SRCIND3(0,0,0);
    ptrdiff_t const srcdk = SRCIND3(0,0,1) - SRCIND3(0,0,0);
    
    
    
    // Loop over fine region
    for (ptrdiff_t k = 0; k < regkext; ++k) {
      ptrdiff_t ks = k0;
      ptrdiff_t kd = dstkoff;
      for (ptrdiff_t j = 0; j < regjext; ++j) {
        ptrdiff_t js = j0;
        ptrdiff_t jd = dstjoff;
        for (ptrdiff_t i = 0; i < regiext; ++i) {
          ptrdiff_t is = i0;
          ptrdiff_t id = dstioff;
          
          // Note: num_tls is either 1 or 3; provide special cases for
          // this.
          CCTK_REAL res = 0.0;
          for (int tl=0; tl<num_tls; ++tl) {
            
            CCTK_REAL val = 0;
            // Note: Unroll the loops above to avoid these if
            // statements
            if (k % 2 == fk) {
              if (j % 2 == fj) {
                if (i % 2 == fi) {
                  val =
                    interp3<0,0,0>
                    (& srcs[tl][SRCIND3(is,js,ks)], srcdi,srcdj,srcdk);
                } else {
                  val =
                    interp3<1,0,0>
                    (& srcs[tl][SRCIND3(is,js,ks)], srcdi,srcdj,srcdk);
                }
              } else {
                if (i % 2 == fi) {
                  val =
                    interp3<0,1,0>
                    (& srcs[tl][SRCIND3(is,js,ks)], srcdi,srcdj,srcdk);
                } else {
                  val =
                    interp3<1,1,0>
                    (& srcs[tl][SRCIND3(is,js,ks)], srcdi,srcdj,srcdk);
                }
              }
            } else {
              if (j % 2 == fj) {
                if (i % 2 == fi) {
                  val =
                    interp3<0,0,1>
                    (& srcs[tl][SRCIND3(is,js,ks)], srcdi,srcdj,srcdk);
                } else {
                  val =
                    interp3<1,0,1>
                    (& srcs[tl][SRCIND3(is,js,ks)], srcdi,srcdj,srcdk);
                }
              } else {
                if (i % 2 == fi) {
                  val =
                    interp3<0,1,1>
                    (& srcs[tl][SRCIND3(is,js,ks)], srcdi,srcdj,srcdk);
                } else {
                  val =
                    interp3<1,1,1>
                    (& srcs[tl][SRCIND3(is,js,ks)], srcdi,srcdj,srcdk);
                }
              }
            }
            
            res += val * tfacs[tl];
            
          } // for tl
          
          dst[DSTIND3(id,jd,kd)] = res;
          
          ++id;
          if (i % 2 != fi) ++is;
        } // for i
        ++jd;
        if (j % 2 != fj) ++js;
      }   // for j
      ++kd;
      if (k % 2 != fk) ++ks;
    }     // for k
  }
  
} // namespace CaKernel
