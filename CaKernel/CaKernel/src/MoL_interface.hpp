#pragma once

#include"cctk.h"
//#define restrict __restrict
#define TILE 256

#if HAVE_CUDA == 1

  #ifdef __CUDACC__
  
  __global__
  void OpSetVar_dev(CCTK_REAL *restrict const var, CCTK_REAL const val, CCTK_INT const npoints);
  
  
  __global__
  void OpCopyVar_dev(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src,
                 CCTK_INT const npoints);
  
  __global__
  void OpUpdateVar_dev(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src,
                   CCTK_REAL const fact, CCTK_INT const npoints);
  
  __global__
  void OpUpdateScaleVar_dev(CCTK_REAL *restrict const var, CCTK_REAL const scale,
                        CCTK_REAL const *restrict const src, CCTK_REAL const fact,
                        CCTK_INT const npoints);
  
  __global__
  void OpUpdateVar2_dev(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src1,
                    CCTK_REAL const fact1, CCTK_REAL const *restrict const src2,
                    CCTK_REAL const fact2, CCTK_INT const npoints);
  
  __global__
  void OpUpdateScaleVar2_dev(CCTK_REAL *restrict const var, CCTK_REAL const scale,
                         CCTK_REAL const *restrict const src1, CCTK_REAL const fact1,
                         CCTK_REAL const *restrict const src2, CCTK_REAL const fact2,
                         CCTK_INT const npoints);
  
  __global__
  void OpUpdateVar3_dev(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src1,
                    CCTK_REAL const fact1, CCTK_REAL const *restrict const src2,
                    CCTK_REAL const fact2, CCTK_REAL const *restrict const src3,
                    CCTK_REAL const fact3, CCTK_INT const npoints);
  
  __global__
  void OpUpdateScaleVar3_dev(CCTK_REAL *restrict const var, CCTK_REAL const scale,
                         CCTK_REAL const *restrict const src1, CCTK_REAL const fact1,
                         CCTK_REAL const *restrict const src2, CCTK_REAL const fact2,
                         CCTK_REAL const *restrict const src3, CCTK_REAL const fact3,
                         CCTK_INT const npoints);
  
  #endif /* __CUDACC__ */

  #ifdef __cplusplus
  extern "C" {
  #endif
  
  void OpSetVar(CCTK_REAL *restrict const var, CCTK_REAL const val, CCTK_INT const npoints);
  
  
  
  void OpCopyVar(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src, 
                 CCTK_INT const npoints);
  
  
  void OpUpdateVar(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src,
                   CCTK_REAL const fact, CCTK_INT const npoints);
  
  
  void OpUpdateScaleVar(CCTK_REAL *restrict const var, CCTK_REAL const scale,
                        CCTK_REAL const *restrict const src, CCTK_REAL const fact, 
                        CCTK_INT const npoints);
  
  
  void OpUpdateVar2(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src1,
                    CCTK_REAL const fact1, CCTK_REAL const *restrict const src2,
                    CCTK_REAL const fact2, CCTK_INT const npoints);
  
  
  void OpUpdateScaleVar2(CCTK_REAL *restrict const var, CCTK_REAL const scale,
                         CCTK_REAL const *restrict const src1, CCTK_REAL const fact1,
                         CCTK_REAL const *restrict const src2, CCTK_REAL const fact2,
                         CCTK_INT const npoints);
  
  
  void OpUpdateVar3(CCTK_REAL *restrict const var, CCTK_REAL const *restrict const src1,
                    CCTK_REAL const fact1, CCTK_REAL const *restrict const src2,
                    CCTK_REAL const fact2, CCTK_REAL const *restrict const src3,
                    CCTK_REAL const fact3, CCTK_INT const npoints);
  
  
  void OpUpdateScaleVar3(CCTK_REAL *restrict const var, CCTK_REAL const scale,
                         CCTK_REAL const *restrict const src1, CCTK_REAL const fact1,
                         CCTK_REAL const *restrict const src2, CCTK_REAL const fact2,
                         CCTK_REAL const *restrict const src3, CCTK_REAL const fact3,
                         CCTK_INT const npoints);


    
  #ifdef __cplusplus
  }
  #endif


#endif /* HAVE_CUDA */
