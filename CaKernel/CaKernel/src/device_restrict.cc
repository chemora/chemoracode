#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <iostream>

using namespace std;



namespace CaKernel {
  
  
  
  static inline
  ptrdiff_t
  index3 (ptrdiff_t const i, ptrdiff_t const j, ptrdiff_t const k,
          ptrdiff_t const padexti, ptrdiff_t const padextj, ptrdiff_t const padextk,
          ptrdiff_t const exti, ptrdiff_t const extj, ptrdiff_t const extk)
  {
    return i + padexti * (j + padextj * k);
  }
  
#define SRCIND3(i,j,k)                                  \
  index3 (srcioff + (i), srcjoff + (j), srckoff + (k),  \
          srcipadext, srcjpadext, srckpadext,           \
          srciext, srcjext, srckext)
#define DSTIND3(i,j,k)                                  \
  index3 (dstioff + (i), dstjoff + (j), dstkoff + (k),  \
          dstipadext, dstjpadext, dstkpadext,           \
          dstiext, dstjext, dstkext)
  
  
  
  extern "C"
  void
  Device_RestrictCaKernel (const CCTK_POINTER_TO_CONST cctkGH_,
                           const CCTK_INT * restrict const vars,
                           const CCTK_INT rl,
                           const CCTK_INT nvars,
                           const CCTK_INT * restrict const srcbbox,
                           const CCTK_INT * restrict const dstbbox,
                           const CCTK_INT * restrict const regbbox)
  {
    const cGH * restrict const cctkGH = static_cast<const cGH*> (cctkGH_);
    
    // Restrict nvars variables.
    // The variable indices are in the array vars.
    // The source refinement level is rl+1, the destination refinement
    // level is rl.
    
    const CCTK_REAL * restrict const src = TODO;
    CCTK_REAL * restrict const dst = TODO;
    
    // Shape (including padding) of the source variables
    ptrdiff_t const srcipadext = TODO srcpadext[0];
    ptrdiff_t const srcjpadext = TODO srcpadext[1];
    ptrdiff_t const srckpadext = TODO srcpadext[2];
    
    // Shape (including padding) of the destination variables
    ptrdiff_t const dstipadext = TODO dstpadext[0];
    ptrdiff_t const dstjpadext = TODO dstpadext[1];
    ptrdiff_t const dstkpadext = TODO dstpadext[2];
    
    // Extent (excluding padding) of the source variables
    ptrdiff_t const srciext = (srcbbox[3] - srcbbox[0]) / srcbbox[6] + 1;
    ptrdiff_t const srcjext = (srcbbox[4] - srcbbox[1]) / srcbbox[7] + 1;
    ptrdiff_t const srckext = (srcbbox[5] - srcbbox[2]) / srcbbox[8] + 1;
    
    // Extent (excluding padding) of the destination variables
    ptrdiff_t const dstiext = (dstbbox[3] - dstbbox[0]) / dstbbox[6] + 1;
    ptrdiff_t const dstjext = (dstbbox[4] - dstbbox[1]) / dstbbox[7] + 1;
    ptrdiff_t const dstkext = (dstbbox[5] - dstbbox[2]) / dstbbox[8] + 1;
    
    // Region size in the destination variables that should be set
    ptrdiff_t const regiext = (regbbox[3] - regbbox[0]) / regbbox[6] + 1;
    ptrdiff_t const regjext = (regbbox[4] - regbbox[1]) / regbbox[7] + 1;
    ptrdiff_t const regkext = (regbbox[5] - regbbox[2]) / regbbox[8] + 1;
    
    ptrdiff_t const srcioff = (regbbox[0] - srcbbox[0]) / regbbox[6];
    ptrdiff_t const srcjoff = (regbbox[1] - srcbbox[1]) / regbbox[7];
    ptrdiff_t const srckoff = (regbbox[2] - srcbbox[2]) / regbbox[8];
    
    ptrdiff_t const dstioff = (regbbox[0] - dstbbox[0]) / regbbox[6];
    ptrdiff_t const dstjoff = (regbbox[1] - dstbbox[1]) / regbbox[7];
    ptrdiff_t const dstkoff = (regbbox[2] - dstbbox[2]) / regbbox[8];
    
    
    
    ptrdiff_t const fi = srcioff % 2;
    ptrdiff_t const fj = srcjoff % 2;
    ptrdiff_t const fk = srckoff % 2;
    
    ptrdiff_t const i0 = srcioff / 2;
    ptrdiff_t const j0 = srcjoff / 2;
    ptrdiff_t const k0 = srckoff / 2;
    
    
    
    // ptrdiff_t const srcdi = SRCIND3(1,0,0) - SRCIND3(0,0,0);
    ptrdiff_t const srcdi = 1;
    assert (srcdi == SRCIND3(1,0,0) - SRCIND3(0,0,0));
    ptrdiff_t const srcdj = SRCIND3(0,1,0) - SRCIND3(0,0,0);
    ptrdiff_t const srcdk = SRCIND3(0,0,1) - SRCIND3(0,0,0);
    
    
    
    // Loop over coarse region
    for (ptrdiff_t k = 0; k < regkext; ++k) {
      for (ptrdiff_t j = 0; j < regjext; ++j) {
        for (ptrdiff_t i = 0; i < regiext; ++i) {
          dst [DSTIND3(i, j, k)] = src [SRCIND3(2*i, 2*j, 2*k)];
        } // for i
      }   // for j
    }     // for k
  }
  
} // namespace CaKernel
