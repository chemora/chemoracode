#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include<assert.h>
#include "Common_mem.hpp"

//#include <algorithm>
//#include <cassert>
//#include <cmath>
//#include <cstdlib>
//#include <iostream>


namespace CaKernel {
  
  int _index3 (const int i, const int j, const int k, const int padexti, const int padextj) {
    return i + padexti * (j + padextj * k);
  }
  
  __device__  int index3 (const int i, const int j, const int k, const int padexti, const int padextj) {
    return i + padexti * (j + padextj * k);
  }

  
#define SRCIND3(i,j,k)                                                        \
  index3 (i, j, k, srcipadext, srcjpadext)
#define DSTIND3(i,j,k)                                                        \
  index3 (i, j, k, dstipadext, dstjpadext)
  
    
    // 1D interpolation coefficients
    
    struct coeffs1d {
//      static const CCTK_REAL coeffs[];
      
      static int const ncoeffs = 5+1;
      static int const imin    = - ncoeffs/2 + 1;
      static int const imax    = imin + ncoeffs;
      
      static inline __device__
      CCTK_REAL get (int const i)
      {
        const CCTK_REAL coeffs[] = {
          +  3 / CCTK_REAL(256.0),
          - 25 / CCTK_REAL(256.0),
          +150 / CCTK_REAL(256.0),
          +150 / CCTK_REAL(256.0),
          - 25 / CCTK_REAL(256.0),
          +  3 / CCTK_REAL(256.0)
        };
#ifdef CARPET_DEBUG
        static_assert (ncoeffs == sizeof (coeffs) / sizeof (*coeffs),
                       "coefficient array has wrong size");
        assert (i>=imin and i<imax);
#endif
        int const j = i - imin;
#ifdef CARPET_DEBUG
        assert (j>=0 and j<ncoeffs);
#endif
        return coeffs[j];
      }
    };
    
  
  
  // 0D "interpolation"
//  static inline
//  __device__
//  CCTK_REAL const&
//  interp0 (CCTK_REAL const * restrict const p)
//  {
//    return * p;
//  }
  
  // 1D interpolation
  template <int di>
  static inline
  __device__
  CCTK_REAL
  interp1 (CCTK_REAL const * restrict const p)
  {
    typedef coeffs1d coeffs;
    if (di == 0) {
//      return interp0 (p);
      return *p;
    } else {
      CCTK_REAL res = 0.0;
#pragma unroll 
      for (int i=coeffs::imin; i<coeffs::imax; ++i) {
//      for (int i=-2; i<4; ++i) {
//        res += coeffs::get(i) * interp0 (p + i);
        res += coeffs::get(i) * *(p + i);
      }
      return res;
    }
  }
  
  // 2D interpolation
  template <int di, int dj>
  static inline
  __device__
  CCTK_REAL
  interp2 (CCTK_REAL const * restrict const p, int const d1)
  {
    typedef coeffs1d coeffs;
    if (dj == 0) {
      return interp1<di> (p);
    } else {
      CCTK_REAL res = 0.0;
#pragma unroll
      for (int i=coeffs::imin; i<coeffs::imax; ++i) {
//      for (int i=-2; i<4; ++i) {
        res += coeffs::get(i) * interp1<di> (p + i*d1);
      }
      return res;
    }
  }
  
  // 3D interpolation
  template <int di, int dj, int dk>
  static inline
  __device__
  CCTK_REAL
  interp3 (CCTK_REAL const * restrict const p,
           int const d1,
           int const d2)
  {
    typedef coeffs1d coeffs;
    if (dk == 0) {
      return interp2<di,dj> (p, d1);
    } else {
      CCTK_REAL res = 0.0;
#pragma unroll 
      for (int i=coeffs::imin; i<coeffs::imax; ++i) {
//      for (int i=-2; i<4; ++i) {
        res += coeffs::get(i) * interp2<di,dj> (p + i*d2, d1);
      }
      return res;
    }
  }
  
  
//for the point i=0, j=0, k=0:
//di = k % 2 == fk
//dj = j % 2 == fj
//dk = i % 2 == fi
  template <int di, int dj, int dk>
  __global__
  void DEVICE_Prolongate(
      CCTK_REAL const * restrict _src, int srcipadext, int srcjpadext, //int srckpadext,
      CCTK_REAL       * restrict _dst, int dstipadext, int dstjpadext, //int dstkpadext,
                                            int ni, int nj, int nk){
    const int gdi = threadIdx.x * 2;
    const int gdj = blockIdx.x * 2;
    const int gdk = blockIdx.y * 2;

    const int gsi = threadIdx.x;
    const int gsj = blockIdx.x ;
    const int gsk = blockIdx.y ;

    const int iadd = 1 - di;
    const int jadd = 1 - dj;
    const int kadd = 1 - dk;

    __shared__ CCTK_REAL const * restrict src; 
    if(gdi==0) src = &src[srcipadext * (gsj + srcjpadext * gsk)];
    __shared__ CCTK_REAL       * restrict dst; 
    if(gdi==1) dst = &dst[dstipadext * (gdj + dstjpadext * gdk)];
    __syncthreads();

//#pragma unroll
//    for (int k = 0; k < 2; ++k) if (k && gdk + k < nk){
//#pragma unroll
//      for (int j = 0; j < 2; ++j) if (j && gdj + j < nj){
//          dst[DSTIND3(gdi,j,k)]     = interp3<di, (dj + j) % 2, (dk + k) % 2>
//            (&src[SRCIND3(gsi, jadd * j, kadd * k)], srcipadext, srcjpadext);
//        if(gdi + 1 < ni){
//          dst[DSTIND3(gdi + 1,j,k)] = interp3<di, (dj + j) % 2, (dk + k) % 2>
//            (&src[SRCIND3(gsi + iadd, jadd * j, kadd * k)], srcipadext, srcjpadext);
//        }
//      }
//    }
    
    dst[DSTIND3(gdi,0,0)] = interp3<di, dj, dk>
                     (&src[SRCIND3(gsi, 0, 0)], srcipadext, srcjpadext);

    if(gdi + 1 < ni)
      dst[DSTIND3(gdi + 1,0,0)] = interp3<(di + 1) % 2, dj, dk>
                     (&src[SRCIND3(gsi + iadd, 0, 0)], srcipadext, srcjpadext);

    if(gdj + 1 < nj){
      dst[DSTIND3(gdi,1,0)] = interp3<di, (dj + 1) % 2, dk>
                     (&src[SRCIND3(gsi, jadd, 0)], srcipadext, srcjpadext);

      if(gdi + 1 < ni)
        dst[DSTIND3(gdi + 1,1,0)] = interp3<(di + 1) % 2, (dj + 1) % 2, dk>
                     (&src[SRCIND3(gsi + iadd, jadd, 0)], srcipadext, srcjpadext);
    }

    if(gdk + 1 < nk){
      dst[DSTIND3(gdi,0,1)] = interp3<di, dj, (dk + 1) % 2>
                     (&src[SRCIND3(gsi, 0, kadd)], srcipadext, srcjpadext);
     
      if(gdi + 1 < ni)
        dst[DSTIND3(gdi + 1,0,1)] = interp3<(di + 1) % 2, dj, (dk + 1) % 2>
                     (&src[SRCIND3(gsi + iadd, 0, kadd)], srcipadext, srcjpadext);
     
      if(gdj + 1 < nj){
        dst[DSTIND3(gdi,1,1)] = interp3<di, (dj + 1) % 2, (dk + 1) % 2>
                     (&src[SRCIND3(gsi, jadd, kadd)], srcipadext, srcjpadext);
     
        if(gdi + 1 < ni)
          dst[DSTIND3(gdi + 1,1,1)] = interp3<(di + 1) % 2, (dj + 1) % 2, (dk + 1) % 2>
                     (&src[SRCIND3(gsi + iadd, jadd, kadd)], srcipadext, srcjpadext);
      }
    }
  }

#undef SRCIND3
#undef DSTIND3
#define SRCIND3(i,j,k)                                                        \
  _index3 (i, j, k, srcipadext, srcjpadext)
#define DSTIND3(i,j,k)                                                        \
  _index3 (i, j, k, dstipadext, dstjpadext)
  
  extern "C"
  int
  Device_ProlongateCaKernel (const CCTK_POINTER_TO_CONST cctkGH_,
                             const CCTK_INT * restrict const vars,
                             const CCTK_INT rl,
                             const CCTK_INT nvars,
                             const CCTK_REAL * restrict const tfacts,
                             const CCTK_INT num_tls,
                             const CCTK_INT * restrict const srcbbox,
                             const CCTK_INT * restrict const dstbbox,
                             const CCTK_INT * restrict const regbbox)
  {
//    const cGH * restrict const cctkGH = static_cast<const cGH*> (cctkGH_);
    
    // Interpolate nvars variables.
    // The variable indices are in the array vars.
    // The source refinement level is rl-1, the destination refinement
    // level is rl.
    // Of the source variables, timelevels 0 ... num_tls-1 are used.

    assert(rl>=0);
    assert(rl - 1>=0);

    for(int var = 0; var < nvars; ++var){
      for(int tl = 0; tl < num_tls; ++tl){
       
        const ptr_t * src_p = CaKernel_GetVarPtrI_rl((void *)cctkGH_, vars[var], rl - 1, tl); 
          assert(src_p);
        const ptr_t * dst_p = CaKernel_GetVarPtrI_rl((void *)cctkGH_, vars[var], rl, tl); 
          assert(dst_p);
    
        CCTK_REAL * restrict src = (CCTK_REAL *) src_p -> ptr;
        CCTK_REAL * restrict dst = (CCTK_REAL *) dst_p -> ptr;
        
        // Shape (including padding) of the source variables
        ptrdiff_t const srcipadext = src_p->ash[0];
        ptrdiff_t const srcjpadext = src_p->ash[1];
//        ptrdiff_t const srckpadext = src_p->ash[2];
        
        // Shape (including padding) of the destination variables
        ptrdiff_t const dstipadext = dst_p->ash[0];
        ptrdiff_t const dstjpadext = dst_p->ash[1];
//        ptrdiff_t const dstkpadext = dst_p->ash[2];
        
        // Region size in the destination variables that should be set
        ptrdiff_t const regiext = (regbbox[3] - regbbox[0]) / regbbox[6] + 1;
        ptrdiff_t const regjext = (regbbox[4] - regbbox[1]) / regbbox[7] + 1;
        ptrdiff_t const regkext = (regbbox[5] - regbbox[2]) / regbbox[8] + 1;
        
        ptrdiff_t const srcioff = (regbbox[0] - srcbbox[0]) / regbbox[6];
        ptrdiff_t const srcjoff = (regbbox[1] - srcbbox[1]) / regbbox[7];
        ptrdiff_t const srckoff = (regbbox[2] - srcbbox[2]) / regbbox[8];
        
        ptrdiff_t const dstioff = (regbbox[0] - dstbbox[0]) / regbbox[6];
        ptrdiff_t const dstjoff = (regbbox[1] - dstbbox[1]) / regbbox[7];
        ptrdiff_t const dstkoff = (regbbox[2] - dstbbox[2]) / regbbox[8];
        
        ptrdiff_t const fi = srcioff % 2;
        ptrdiff_t const fj = srcjoff % 2;
        ptrdiff_t const fk = srckoff % 2;
        
        ptrdiff_t const i0 = srcioff / 2;
        ptrdiff_t const j0 = srcjoff / 2;
        ptrdiff_t const k0 = srckoff / 2;
        
        assert (1 == SRCIND3(1,0,0) - SRCIND3(0,0,0));
        
        assert (1 == DSTIND3(1,0,0) - DSTIND3(0,0,0));

        dst = &dst[DSTIND3(dstioff, dstjoff, dstkoff)];
        src = &src[SRCIND3(i0,      j0,      k0)];

        int dimx = iDivUp(regiext, 2);
        int dimy = iDivUp(regjext, 2);
        int dimz = iDivUp(regkext, 2);

        
        const int dec = (fk << 2) + (fj << 1) + fi;
        switch (dec){
        case 0: DEVICE_Prolongate<0,0,0><<<dimx, dim3(dimy, dimz)>>> (src, srcipadext, srcjpadext, dst, dstipadext, dstjpadext, regiext, regjext, regkext); break;
        case 1: DEVICE_Prolongate<1,0,0><<<dimx, dim3(dimy, dimz)>>> (src, srcipadext, srcjpadext, dst, dstipadext, dstjpadext, regiext, regjext, regkext); break;
        case 2: DEVICE_Prolongate<0,1,0><<<dimx, dim3(dimy, dimz)>>> (src, srcipadext, srcjpadext, dst, dstipadext, dstjpadext, regiext, regjext, regkext); break;
        case 3: DEVICE_Prolongate<1,1,0><<<dimx, dim3(dimy, dimz)>>> (src, srcipadext, srcjpadext, dst, dstipadext, dstjpadext, regiext, regjext, regkext); break;
        case 4: DEVICE_Prolongate<0,0,1><<<dimx, dim3(dimy, dimz)>>> (src, srcipadext, srcjpadext, dst, dstipadext, dstjpadext, regiext, regjext, regkext); break;
        case 5: DEVICE_Prolongate<1,0,1><<<dimx, dim3(dimy, dimz)>>> (src, srcipadext, srcjpadext, dst, dstipadext, dstjpadext, regiext, regjext, regkext); break;
        case 6: DEVICE_Prolongate<0,1,1><<<dimx, dim3(dimy, dimz)>>> (src, srcipadext, srcjpadext, dst, dstipadext, dstjpadext, regiext, regjext, regkext); break;
        case 7: DEVICE_Prolongate<1,1,1><<<dimx, dim3(dimy, dimz)>>> (src, srcipadext, srcjpadext, dst, dstipadext, dstjpadext, regiext, regjext, regkext); break;
        default: assert(0);
        }
      
      }
    }    
  return 0;
  }
  
} // namespace CaKernel
