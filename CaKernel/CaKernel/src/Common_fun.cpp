#include<cctk.h>
#include <util_Table.h>
#include"Common_fun.h"

/* 
  Function copy/pasted from GenericFD.c file. The reason of copying it is that we don't
  want CaKernel to be dependent of GenericFD.
*/
int CaKernel_GetBoundaryInfo(const void * cctkGH_, const int * cctk_lsh,
                               const int * cctk_bbox, const int * cctk_nghostzones, int * imin, 
        		                         int * imax, int * is_symbnd, int * is_physbnd, int * is_ipbnd)
{
  cGH * cctkGH = (cGH *) cctkGH_;
  int bbox[6];
  int is_internal[6];
  int is_staggered[6];
  int shiftout[6];
  int symbnd[6];
  int nboundaryzones[6];

  int symtable = 0;
  int dir = 0;
  int face = 0;
  int npoints = 0;
  int iret = 0;
  int ierr = 0;

  if (CCTK_IsFunctionAliased ("MultiPatch_GetBbox")) {
    ierr = MultiPatch_GetBbox (cctkGH, 6, bbox);
    if (ierr != 0)
      CCTK_WARN(0, "Could not obtain bbox specification");
  } else {
    for (dir = 0; dir < 6; dir++)
    {
      bbox[dir] = 0;
    }
  }

  if (CCTK_IsFunctionAliased ("MultiPatch_GetBoundarySpecification")) {
    int const map = MultiPatch_GetMap (cctkGH);
    if (map < 0)
      CCTK_WARN(0, "Could not obtain boundary specification");
    ierr = MultiPatch_GetBoundarySpecification
      (map, 6, nboundaryzones, is_internal, is_staggered, shiftout);
    if (ierr != 0)
      CCTK_WARN(0, "Could not obtain boundary specification");
  } else if (CCTK_IsFunctionAliased ("GetBoundarySpecification")) {
    ierr = GetBoundarySpecification
      (6, nboundaryzones, is_internal, is_staggered, shiftout);
    if (ierr != 0)
      CCTK_WARN(0, "Could not obtain boundary specification");
  } else {
    CCTK_WARN(0, "Could not obtain boundary specification");
  }

  symtable = SymmetryTableHandleForGrid(cctkGH);
  if (symtable < 0) 
  {
    CCTK_WARN(0, "Could not obtain symmetry table");
  }  
  
  iret = Util_TableGetIntArray(symtable, 6, symbnd, "symmetry_handle");
  if (iret != 6) CCTK_WARN (0, "Could not obtain symmetry information");

  for (dir = 0; dir < 6; dir++)
  {
    is_ipbnd[dir] = (!cctk_bbox[dir]);
    is_symbnd[dir] = (!is_ipbnd[dir] && symbnd[dir] >= 0 && !bbox[dir]);
    is_physbnd[dir] = (!is_ipbnd[dir] && !is_symbnd[dir]);
  }

  for (dir = 0; dir < 3; dir++)
  {
    for (face = 0; face < 2; face++)
    {
      int index = dir*2 + face;
      if (is_ipbnd[index])
      {
        /* Inter-processor boundary */
        npoints = cctk_nghostzones[dir];
      }
      else
      {
        /* Symmetry or physical boundary */
        npoints = nboundaryzones[index];
             
        if (is_symbnd[index])
        {
          /* Ensure that the number of symmetry zones is the same
             as the number of ghost zones */
          if (npoints != cctk_nghostzones[dir])
          {
            CCTK_WARN (1, "The number of symmetry points is different from the number of ghost points; this is probably an error");
          }
        }
      }

      switch(face)
      {
      case 0: /* Lower boundary */
        imin[dir] = npoints;
        break;
      case 1: /* Upper boundary */
        imax[dir] = cctk_lsh[dir] - npoints;
        break;
      default:
        CCTK_WARN(0, "internal error");
      }
    }
  }
  return 0;
}
