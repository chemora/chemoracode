#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <carpet.hh>

#include <cassert>
#include <vector>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "util.hh"
#include "device.hh"

using namespace std;



pui_t *pui;

void Accelerator_SetDevice(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_PARAMETERS;
  DECLARE_CCTK_ARGUMENTS;

  // My MPI process
  const int myproc = CCTK_MyProc(cctkGH);
  
  // Number of devices on this host
  int ndev;
  cudaError_t err = cudaGetDeviceCount(&ndev);
  if(err != cudaSuccess) CCTK_WARN(CCTK_WARN_ALERT, cudaGetErrorString(err));
  if (ndev == 0) {
    CCTK_WARN(CCTK_WARN_ALERT,
              "There are no devices available. Make sure you active the CUDA thorn in your parameter file!");
  }
  
  // List of MPI processes running on this host
  int const myhost = CCTK_MyHost(cctkGH);
  int const mynprocs = CCTK_nProcsOnHost(cctkGH, myhost);
  vector<int> myprocs(mynprocs);
  CCTK_ProcsOnHost(cctkGH, myhost, &myprocs[0], mynprocs);
  int myprocid = -1;
  for (int i=0; i<mynprocs; ++i) {
    if (myprocs[i] == myproc) myprocid = i;
  }
  assert(myprocid != -1);
  
  // pui always has a size of mynprocs
  MALLOC_SAFE_CALL(pui = (pui_t*) malloc(mynprocs * sizeof(pui_t)));
  
  CCTK_VInfo(CCTK_THORNSTRING,
             "This host has %d device(s) and %d MPI process(es)",
             ndev, mynprocs);
  
  // Assign GPUs and cores to the MPI processes. First assign one GPU
  // and one core to each MPI process, then divide the remaining cores
  // among the remaining MPI processes.
  
  // Don't know how to handle this
//  assert(not allow_multiple_process_on_gpu);
  
  for (int i=0; i<mynprocs; i++) {
    // Set up pui structure
    pui[i].host = myhost;
    pui[i].nprocs = mynprocs;
    pui[i].dev = -1;
    pui[i].proc = -1;
    
    if (myproc == myprocs[i]) {
      pui[i].proc = myproc;
    }
    
    // Does this MPI process use a GPU?
    bool const isdev = not allow_no_process_on_gpu and 
                        (i<ndev or allow_multiple_process_on_gpu);
    if (isdev) {
      pui[i].dev = i % ndev;
      CUDA_SAFE_CALL(cudaGetDeviceProperties(&(pui[i].devprop), pui[i].dev));
    }
  }
  
  // Testing Accelerator_WhichDevice
  int const devid = AcceleratorThorn_WhichDevice(cctkGH);
  if (devid != -1) {
    CCTK_VInfo(CCTK_THORNSTRING,
               "Host ID: %d\tProcess ID: %d\tDevice ID: %d",
               myhost, myproc, devid);
  } else {
    CCTK_VInfo(CCTK_THORNSTRING,
               "Host ID: %d\tProcess ID: %d\tNo device, using CPU cores",
               myhost, myproc);
  }
  CCTK_VInfo(CCTK_THORNSTRING,
             "This is process #%d on this host", myprocid);
  *device_process = devid != -1;
  *host_process   = not *device_process;
  
  if (set_cpu_affinity) {
#ifdef HAVE_SCHED_GETAFFINITY
    // Set CPU affinities
    // TODO: check this
    int const num_cores_on_host = mynprocs * dist::num_threads();
    int num_available_cores = num_cores_on_host;
    if (verbose_core_assignments) {
      CCTK_VInfo(CCTK_THORNSTRING,
                 "   There are %d cores available", num_available_cores);
    }
    vector<bool> available_cores(num_cores_on_host, true);
    vector<vector<bool> > cores_for_process(mynprocs);
    // First assign cores to GPUs
    for (int i=0; i<mynprocs; i++) {
      int const dev = pui[i].dev;
      if (dev != -1) {
        // Running on a GPU -- need a nearby core
        assert(dev>=0 and dev<16);
        int core = gpu_core[dev];
        if (core == -1) {
          // Find a free core
          for (int c=0; c<num_cores_on_host; ++c) {
            if (available_cores[c]) {
              core = c;
              break;
            }
          }
        }
        assert(core != -1);
        if (verbose_core_assignments) {
          CCTK_VInfo(CCTK_THORNSTRING,
                     "   Process %d [GPU]: starting with core %d",
                     myproc, core);
        }
        cores_for_process.at(i).resize(num_cores_on_host, false);
        for (int c=core; c<core+gpu_num_cores[dev]; ++c) {
          if (c>=num_cores_on_host) break;
          if (verbose_core_assignments) {
            CCTK_VInfo(CCTK_THORNSTRING,
                       "   Process %d [GPU]: using core %d",
                       myproc, c);
          }
          cores_for_process.at(i).at(c) = true;
          if (not oversubscribe_gpu_cores) {
            available_cores.at(c) = false;
            --num_available_cores;
          }
        }
      }
    }
    if (verbose_core_assignments) {
      CCTK_VInfo(CCTK_THORNSTRING,
                 "   There are %d cores left", num_available_cores);
    }
    // Then assign the left-over cores to CPUs
    int const num_cpu_procs = mynprocs - ndev >= 0 ? mynprocs - ndev : 0;
    assert(num_available_cores >= num_cpu_procs);
    int const num_cores_per_proc =
      num_cpu_procs == 0 ? 0 : num_cpu_procs / num_available_cores;
    for (int i=0; i<mynprocs; i++) {
      int const dev = pui[i].dev;
      if (dev == -1) {
        // Running on a CPU -- need some cores
        cores_for_process.at(i).resize(num_cores_on_host, false);
        int num_cores = 0;
        int proc_numa_node = -1;
        // Find free cores
        for (int c=0; c<num_cores_on_host; ++c) {
          if (available_cores.at(c)) {
            int const numa_node = c / cores_in_numa_node;
            if (proc_numa_node == -1) {
              proc_numa_node = numa_node;
              if (verbose_core_assignments) {
                CCTK_VInfo(CCTK_THORNSTRING,
                           "   Process %d [CPU]: using NUMA node %d",
                           myproc, numa_node);
              }
            }
            // Only choose this core if it is on the same NUMA node
            if (numa_node == proc_numa_node) {
              if (verbose_core_assignments) {
                CCTK_VInfo(CCTK_THORNSTRING,
                           "   Process %d [CPU]: using core %d",
                           myproc, c);
              }
              cores_for_process.at(i).at(c) = true;
              available_cores.at(c) = false;
              --num_available_cores;
              ++num_cores;
              if (num_cores == num_cores_per_proc) break;
            }
          }
        }
        // Ensure this process has cores
        assert(num_cores>0);
      }
    }
    // Set CPU affinity
    {
      // Count available cores
      int num_cores = 0;
      cout << "Process " << myproc << ": running on cores";
      for (int c=0; c<num_cores_on_host; ++c) {
        if (cores_for_process.at(myprocid).at(c)) {
          cout << " " << c;
          ++num_cores;
        }
      }
      cout << " [" << num_cores << " cores ]\n";
      // Set number of OpenMP threads
#ifdef _OPENMP
      omp_set_num_threads(num_cores);
#endif
      // Assign cores to threads
#pragma omp parallel
      {
        cpu_set_t cpumask;
        CPU_ZERO(&cpumask);
        for (int c=0; c<num_cores_on_host; ++c) {
          if (cores_for_process.at(myprocid).at(c)) {
            CPU_SET(c, &cpumask);
          }
        }
        int const ierr = sched_setaffinity(0, sizeof(cpumask), &cpumask);
        assert(not ierr);
      }
    }
#else
    CCTK_WARN(CCTK_WARN_ALERT,
              "Setting the CPU affinity is not supported on this system");
#endif
  }
}



// @return  -1     is not device
// something else: device id
int AcceleratorThorn_WhichDevice(const CCTK_POINTER_TO_CONST _cctkGH)
{
  const cGH *  cctkGH = (cGH *) _cctkGH;
  const int myhost = CCTK_MyHost(cctkGH);
  const int myproc = CCTK_MyProc(cctkGH);
  const int mynprocs = CCTK_nProcsOnHost(cctkGH, myhost);
  extern pui_t *pui;

  for (int i = 0; i < mynprocs; i++)
  {
    if ((myproc == pui[i].proc) && (pui[i].dev != -1))
      return pui[i].dev;
  }
  return -1;
}
