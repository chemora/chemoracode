#include <stdio.h>

#define STRING(x)                               \
  "#define ANTIPARENS(x) x\n"                   \
  "ANTIPARENS " #x "\n"

int main (int argc, char **argv)
{
  char const* const code =
    STRING((
            __kernel double f(double a, double b)
      {
        int x,y;
        return a+b;
      }
            ));
  printf ("Code:\n%s", code);
  return 0;
}
