// -*-C-*-

// mad
// pown
// __builtin_expect
// Boost, <boost/preprocessor/...>

#pragma OPENCL EXTENSION cl_khr_fp64  : enable
#pragma OPENCL EXTENSION cl_amd_printf: enable

// #include <assert.h>
// #define assert(x) ((x) ? 0 : sin(*(double*)0))
#define assert(x) (0)



// doubleV           vector of double
// longV             vector of long (same size as double)
// convert_doubleV   convert to vector of double
// indicesV          vector of long containing (0,1,2,...)
// vloadV            load unaligned vector
// vstoreaV_masked   partially store aligned vector

// Assumption: all grid points [0,j,k] are aligned, arrays are padded
// as necessary

#if VECTOR_SIZE_I==1
#  define doubleV         double
#  define convert_doubleV convert_double
#  define longV           long
#  define vloadV(i,p)     ((p)[i])
#  define indicesV        ((longV)(0))
#elif VECTOR_SIZE_I==2
#  define doubleV         double2
#  define convert_doubleV convert_double2
#  define longV           long2
#  define vloadV          vload2
#  define indicesV        ((longV)(0,1))
#elif VECTOR_SIZE_I==4
#  define doubleV  double4
#  define convert_doubleV convert_double4
#  define longV           long4
#  define vloadV          vload4
#  define indicesV        ((longV)(0,1,2,3))
#elif VECTOR_SIZE_I==8
#  define doubleV         double8
#  define convert_doubleV convert_double8
#  define longV           long8
#  define vloadV          vload8
#  define indicesV        ((longV)(0,1,2,3,4,5,6,7))
#elif VECTOR_SIZE_I==16
#  define doubleV         double16
#  define convert_doubleV convert_double16
#  define longV           long16
#  define vloadV          vload16
#  define indicesV        ((longV)(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15))
#else
#  error
#endif
#if VECTOR_SIZE_J!=1 || VECTOR_SIZE_K!=1
#  error
#endif



#define vloadV_offset(offset, p, d)                     \
  vloadV(offset, (__global double const*)(p) + (d))



#if VECTOR_SIZE_I == 1

#  define vstoreaV_masked(data, offset, p)      \
  do {                                          \
    if (vec_any_I && vec_any_J && vec_any_K) {  \
      (p)[offset] = (data);                     \
    }                                           \
  } while(0)

#elif VECTOR_SIZE_I == 2

#  define vstoreaV_masked(data_, offset_, p_)           \
  do {                                                  \
    if (vec_any_I && vec_any_J && vec_any_K) {          \
      doubleV const data = (data_);                     \
      size_t const offset = (offset_);                  \
      __global doubleV* restrict const p = (p_);        \
      if (vec_all_I) {                                  \
        p[offset] = data;                               \
      } else {                                          \
        if (vec_all_lo_I) {                             \
          ((__global double*)&p[offset])[0] = data.s0;  \
        } else {                                        \
          ((__global double*)&p[offset])[1] = data.s1;  \
        }                                               \
      }                                                 \
    }                                                   \
  } while(0)

#else

#  define vstoreaV_masked(data_, offset_, p_)                   \
  do {                                                          \
    if (vec_any_I && vec_any_J && vec_any_K) {                  \
      doubleV const data = (data_);                             \
      size_t const offset = (offset_);                          \
      __global doubleV* restrict const p = (p_);                \
      if (vec_all_I) {                                          \
        p[offset] = data;                                       \
      } else {                                                  \
        /* select(a,b,c) = MSB(c) ? b : a */                    \
        p[offset] = select (p[offset], data, vec_mask_I);       \
      }                                                         \
    }                                                           \
  } while(0)

#endif



/* uint const dim = 3; */
#define dim 3



typedef struct {
  size_t gsh[dim];
  size_t lbnd[dim];
  size_t lssh[dim];
  size_t lsh[dim];
  double origin_space[dim];
  double delta_space[dim];
  double time;
  double delta_time;
} grid_t;



#define EXECUTE_KERNEL(CMD)                             \
  do {                                                  \
    size_t const ind3d = dI*indI + dJ*indJ + dK*indK;   \
    { CMD; }                                            \
  } while(0)

#define vecVI indicesV
#define vecVJ ((longV)0)
#define vecVK ((longV)0)

#define EXECUTE_VECTOR(D, CMD)                                          \
  do {                                                                  \
    /* TODO: Distribute this over the different EXECUTE macros */       \
    size_t const ind##D =                                               \
      (unr##D + UNROLL_SIZE_##D *                                       \
       (grp##D + GROUP_SIZE_##D *                                       \
        (til##D + TILE_SIZE_##D * grd##D)));                            \
    size_t const D = VECTOR_SIZE_##D * ind##D;                          \
    bool const vec_trivial_##D = VECTOR_SIZE_##D * UNROLL_SIZE_##D == 1; \
    bool const vec_any_##D =                                            \
      vec_trivial_##D || (D + VECTOR_SIZE_##D - 1 >= D##min && D < D##max); \
    bool const vec_all_lo_##D =                                         \
      vec_trivial_##D || D >= D##min;                                   \
    bool const vec_all_hi_##D =                                         \
      vec_trivial_##D || D+VECTOR_SIZE_##D-1 < D##max;                  \
    bool const vec_all_##D    =                                         \
      vec_trivial_##D || (vec_all_lo_##D && vec_all_hi_##D);            \
    longV const vec_mask_##D =                                          \
      vec_trivial_##D ?                                                 \
      (longV)0 == (longV)0 :                                            \
      (longV)D+vecV##D >= (longV)D##min &&                              \
      (longV)D+vecV##D <  (longV)D##max;                                \
    { CMD; }                                                            \
  } while(0)

#define EXECUTE_UNROLL(D, CMD)                                          \
  do {                                                                  \
    switch (UNROLL_SIZE_##D) {                                          \
    /*                                                                  \
    case 8: { size_t const unr##D = UNROLL_SIZE_##D - 8; { CMD; } }     \
    case 7: { size_t const unr##D = UNROLL_SIZE_##D - 7; { CMD; } }     \
    case 6: { size_t const unr##D = UNROLL_SIZE_##D - 6; { CMD; } }     \
    case 5: { size_t const unr##D = UNROLL_SIZE_##D - 5; { CMD; } }     \
    */                                                                  \
    case 4: { size_t const unr##D = UNROLL_SIZE_##D - 4; { CMD; } }     \
    case 3: { size_t const unr##D = UNROLL_SIZE_##D - 3; { CMD; } }     \
    case 2: { size_t const unr##D = UNROLL_SIZE_##D - 2; { CMD; } }     \
    case 1: { size_t const unr##D = UNROLL_SIZE_##D - 1; { CMD; } }     \
    }                                                                   \
  } while(0)

#define EXECUTE_TILE(D, CMD)                                            \
  do {                                                                  \
    for (size_t til##D = 0; til##D < TILE_SIZE_##D; ++til##D) {         \
      size_t const tind##D =                                            \
        (grp##D + GROUP_SIZE_##D *                                      \
         (til##D + TILE_SIZE_##D * grd##D));                            \
      size_t const D = VECTOR_SIZE_##D * UNROLL_SIZE_##D * tind##D;     \
      if (D + VECTOR_SIZE_##D * UNROLL_SIZE_##D - 1 >= D##min && D < D##max) { \
        CMD;                                                            \
      }                                                                 \
    }                                                                   \
  } while (0)



#define BEGIN_LOOP(imin_,jmin_,kmin_, imax_,jmax_,kmax_)                \
  do {                                                                  \
    typedef int loop_begin_t;                                           \
    size_t const imin = (imin_);                                        \
    size_t const jmin = (jmin_);                                        \
    size_t const kmin = (kmin_);                                        \
    size_t const imax = (imax_);                                        \
    size_t const jmax = (jmax_);                                        \
    size_t const kmax = (kmax_);                                        \
    size_t const grpi = get_local_id(0); /* index in group */           \
    size_t const grpj = get_local_id(1);                                \
    size_t const grpk = get_local_id(2);                                \
    size_t const grdi = get_group_id(0); /* index in grid */            \
    size_t const grdj = get_group_id(1);                                \
    size_t const grdk = get_group_id(2);                                \
    size_t const di = 1;                                                \
    size_t const dj = (grid->lsh[0] / VECTOR_SIZE_I) * di;              \
    size_t const dk = (grid->lsh[1] / VECTOR_SIZE_J) * dj;              \
                                                                        \
    for (size_t tilk = 0; tilk < TILE_SIZE_K; ++ tilk) {                \
      size_t const indk =                                               \
        grpk + GROUP_SIZE_K * (tilk + TILE_SIZE_K * grdk);              \
      size_t const k = VECTOR_SIZE_K * indk;                            \
      if (k+VECTOR_SIZE_K-1 >= kmin && k < kmax) {                      \
        for (size_t tilj = 0; tilj < TILE_SIZE_J; ++ tilj) {            \
          size_t const indj =                                           \
            grpj + GROUP_SIZE_J * (tilj + TILE_SIZE_J * grdj);          \
          size_t const j = VECTOR_SIZE_J * indj;                        \
          if (j+VECTOR_SIZE_J-1 >= jmin && j < jmax) {                  \
            for (size_t tili = 0; tili < TILE_SIZE_I; ++ tili) {        \
              size_t const indi =                                       \
                grpi + GROUP_SIZE_I * (tili + TILE_SIZE_I * grdi);      \
              size_t const i = VECTOR_SIZE_I * indi;                    \
              if (i+VECTOR_SIZE_I-1 >= imin && i < imax) {              \
                                                                        \
                size_t const ind3d = indi*di + indj*dj + indk*dk;       \
                bool const vec_all_lo = i >= imin;                      \
                bool const vec_all_hi = i+VECTOR_SIZE_I-1 < imax;       \
                bool const vec_all = vec_all_lo && vec_all_hi;          \
                longV const vec_mask =                                  \
                  (longV)i+indicesV >= (longV)imin &&                   \
                  (longV)i+indicesV <  (longV)imax;                     \
                {
#define END_LOOP                                \
                }                               \
              }                                 \
              if (TILE_SIZE_I == 1) break;      \
            }                                   \
          }                                     \
          if (TILE_SIZE_J == 1) break;          \
        }                                       \
      }                                         \
      if (TILE_SIZE_K == 1) break;              \
    }                                           \
    typedef loop_begin_t loop_end_t;            \
  } while (0)

#define BEGIN_LOOP_ALL                                          \
  BEGIN_LOOP(0,0,0,                                             \
             grid->lssh[0],grid->lssh[1],grid->lssh[2])         \
  {                                                             \
    typedef int begin_loop_all_t;                               \
    {
#define END_LOOP_ALL                            \
    }                                           \
    typedef begin_loop_all_t end_loop_all_t;    \
  }                                             \
  END_LOOP

#define BEGIN_LOOP_INTERIOR                     \
  BEGIN_LOOP(GHOST_SIZE_I,                      \
             GHOST_SIZE_J,                      \
             GHOST_SIZE_K,                      \
             grid->lssh[0]-GHOST_SIZE_I,        \
             grid->lssh[1]-GHOST_SIZE_J,        \
             grid->lssh[2]-GHOST_SIZE_K)        \
  {                                             \
    typedef int begin_loop_interior_t;          \
    {
#define END_LOOP_INTERIOR                               \
    }                                                   \
    typedef begin_loop_interior_t end_loop_interior_t;  \
  }                                                     \
  END_LOOP



// #define printf3(name, func)                                             \
//   printf ("%s: [%d,%d,%d]\n", name, (int)func(0), (int)func(1), (int)func(2))
#define printf3(name, func) (0)



#define vec_any_I (1)
#define vec_any_J (1)
#define vec_any_K (1)
#define vec_all_I vec_all
#define vec_all_lo_I vec_all_lo
#define vec_all_hi_I vec_all_hi

__kernel
__attribute__((vec_type_hint(doubleV)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void info (void)
{
  if (get_global_id(0)==0 && get_global_id(1)==0 && get_global_id(2)==0) {
    printf3 ("global_size", get_global_size);
    printf3 ("global_id  ", get_global_id  );
    printf3 ("local_size ", get_local_size );
    printf3 ("local_id   ", get_local_id   );
    printf3 ("num_groups ", get_num_groups );
    printf3 ("group_id   ", get_group_id   );
  }
}



__kernel
__attribute__((vec_type_hint(doubleV)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void init (__constant grid_t  *restrict const grid,
           __global   doubleV *restrict const b,
           __global   doubleV *restrict const c,
           __global   doubleV *restrict const d)
{
  BEGIN_LOOP_ALL {
    size_t const ind = i + grid->lssh[0] * (j + grid->lssh[1] * k);
    b[ind3d] = convert_doubleV((longV)(ind  ) + indicesV);
    c[ind3d] = convert_doubleV((longV)(ind+1) + indicesV);
    d[ind3d] = 0.0;
  } END_LOOP_ALL;
}



__kernel
__attribute__((vec_type_hint(doubleV)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void add (__constant grid_t        *restrict const grid,
          __global   doubleV       *restrict const a,
          __global   doubleV const *restrict const b,
          __global   doubleV const *restrict const c)
{
  BEGIN_LOOP_ALL {
    doubleV const f = 2.0;
    a[ind3d] = b[ind3d]+f*c[ind3d];
  } END_LOOP_ALL;
}



__kernel
__attribute__((vec_type_hint(doubleV)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void div (__constant grid_t        *restrict const grid,
          __global   doubleV       *restrict const d,
          __global   doubleV const *restrict const b)
{
  // scanf("");
  // unsigned long eax, edx;
  // __asm__ volatile ("rdtsc" : "=a" (eax), "=d" (edx));
  // unsigned long long const ts = (unsigned long long) edx << 32 | eax;
  BEGIN_LOOP_INTERIOR {
    doubleV const ax =
      - 0.5 * vloadV_offset(ind3d, b, -di)
      + 0.5 * vloadV_offset(ind3d, b, +di);
    doubleV const ay = - 0.5 * b[ind3d-dj] + 0.5 * b[ind3d+dj];
    doubleV const az = - 0.5 * b[ind3d-dk] + 0.5 * b[ind3d+dk];
    doubleV const diva = ax + ay + az;
    vstoreaV_masked (diva, ind3d, d);
  } END_LOOP_INTERIOR;
}



__kernel
__attribute__((vec_type_hint(doubleV)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void wave_init (__constant grid_t        *restrict const grid,
                __global   doubleV       *restrict const u)
{
  size_t const ni = grid->gsh[0];
  size_t const nj = grid->gsh[1];
  size_t const nk = grid->gsh[2];
  size_t const i0 = grid->lbnd[0];
  size_t const j0 = grid->lbnd[1];
  size_t const k0 = grid->lbnd[2];
  double const x0 = grid->origin_space[0];
  double const y0 = grid->origin_space[1];
  double const z0 = grid->origin_space[2];
  double const dx = grid->delta_space[0];
  double const dy = grid->delta_space[1];
  double const dz = grid->delta_space[2];
  double const kx = M_PI / (dx * (ni - 1));
  double const ky = M_PI / (dy * (nj - 1));
  double const kz = M_PI / (dz * (nk - 1));
  double const t = grid->time;
  double const omega = sqrt(pow(kx,2) + pow(ky,2) + pow(kz,2));
  BEGIN_LOOP_ALL {
    doubleV const x = (doubleV)x0 + ((longV)(i0 + i) + indicesV) * (doubleV)dx;
    double  const y = y0 + (j0 + j) * dy;
    double  const z = z0 + (k0 + k) * dz;
    u[ind3d] = sin(kx*x) * sin(ky*y) * sin(kz*z) * sin(omega*t);
  } END_LOOP_ALL;
}

__kernel
__attribute__((vec_type_hint(doubleV)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void wave_step0 (__constant grid_t        *restrict const grid,
                 __global   doubleV const *restrict const u_p_p,
                 __global   doubleV const *restrict const u_p,
                 __global   doubleV       *restrict const u)
{
  double const idx2 = 1.0 / pow(grid->delta_space[0], 2);
  double const idy2 = 1.0 / pow(grid->delta_space[1], 2);
  double const idz2 = 1.0 / pow(grid->delta_space[2], 2);
  double const dt2 = pow(grid->delta_time, 2);
  BEGIN_LOOP_INTERIOR {
    doubleV const ux =                                                  \
      + 1.0 * vloadV_offset(ind3d, u_p, -di)                            \
      - 2.0 * u_p[ind3d]                                                \
      + 1.0 * vloadV_offset(ind3d, u_p, +di);                           \
    doubleV const uy =                                                  \
      1.0 * u_p[ind3d-dj] - 2.0 * u_p[ind3d] + 1.0 * u_p[ind3d+dj];     \
    doubleV const uz =                                                  \
      1.0 * u_p[ind3d-dk] - 2.0 * u_p[ind3d] + 1.0 * u_p[ind3d+dk];     \
    doubleV const divu = idx2 * ux + idy2 * uy + idz2 * uz;             \
    vstoreaV_masked(2.0 * u_p[ind3d] - u_p_p[ind3d] + dt2 * divu, ind3d, u); \
  } END_LOOP_INTERIOR;
}

#undef vec_any_I
#undef vec_any_J
#undef vec_any_K
#undef vec_all_I
#undef vec_all_lo_I
#undef vec_all_hi_I

__kernel
__attribute__((vec_type_hint(doubleV)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void wave_step (__constant grid_t        *restrict const grid,
                __global   doubleV const *restrict const u_p_p,
                __global   doubleV const *restrict const u_p,
                __global   doubleV       *restrict const u)
{
  double const idx2 = 1.0 / pow(grid->delta_space[0], 2);
  double const idy2 = 1.0 / pow(grid->delta_space[1], 2);
  double const idz2 = 1.0 / pow(grid->delta_space[2], 2);
  double const dt2 = pow(grid->delta_time, 2);
  
  size_t const Imin = GHOST_SIZE_I;
  size_t const Jmin = GHOST_SIZE_J;
  size_t const Kmin = GHOST_SIZE_K;
  size_t const Imax = grid->lssh[0] - GHOST_SIZE_I;
  size_t const Jmax = grid->lssh[1] - GHOST_SIZE_J;
  size_t const Kmax = grid->lssh[2] - GHOST_SIZE_K;
  size_t const grpI = get_local_id(0); /* index in group */
  size_t const grpJ = get_local_id(1);
  size_t const grpK = get_local_id(2);
  size_t const grdI = get_group_id(0); /* index in grid */
  size_t const grdJ = get_group_id(1);
  size_t const grdK = get_group_id(2);
  size_t const dI = 1;
  size_t const dJ = (grid->lsh[0] / VECTOR_SIZE_I) * dI;
  size_t const dK = (grid->lsh[1] / VECTOR_SIZE_J) * dJ;
  
#define KERNEL                                                          \
  do {                                                                  \
    doubleV const ux =                                                  \
      + 1.0 * vloadV_offset(ind3d, u_p, -dI)                            \
      - 2.0 * u_p[ind3d]                                                \
      + 1.0 * vloadV_offset(ind3d, u_p, +dI);                           \
    doubleV const uy =                                                  \
      1.0 * u_p[ind3d-dJ] - 2.0 * u_p[ind3d] + 1.0 * u_p[ind3d+dJ];     \
    doubleV const uz =                                                  \
      1.0 * u_p[ind3d-dK] - 2.0 * u_p[ind3d] + 1.0 * u_p[ind3d+dK];     \
    doubleV const divu = idx2 * ux + idy2 * uy + idz2 * uz;             \
    vstoreaV_masked(2.0 * u_p[ind3d] - u_p_p[ind3d] + dt2 * divu, ind3d, u); \
  } while (0)
  
  EXECUTE_TILE(K,
  EXECUTE_TILE(J,
  EXECUTE_TILE(I,
  EXECUTE_UNROLL(K,
  EXECUTE_UNROLL(J,
  EXECUTE_UNROLL(I,
  EXECUTE_VECTOR(K,
  EXECUTE_VECTOR(J,
  EXECUTE_VECTOR(I,
  EXECUTE_KERNEL(
  KERNEL
  ))))))))));
  
#undef KERNEL
  
}
