#include <string>

namespace WaveCL {
  
#define STRING(x)                               \
  "#define ANTIPARENS(x) x\n"                   \
  "ANTIPARENS " #x "\n"
  
  std::string get_source_wave ()
  {
    return STRING((
#include "wave.cl"
            ));
  }
  
}
