#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <sys/types.h>
#include <unistd.h>

#ifdef __APPLE__
#  include <OpenCL/opencl.h>
#else
#  include <CL/opencl.h>
#endif	

using namespace std;

#define restrict __restrict__



namespace OpenCL {
  
  // grid point   1                         n/a
  // vector       VECTOR_SIZE               n/a
  // unrolled     UNROLL_SIZE               ???
  // work group   GROUP_SIZE                local_id
  // loop tile    TILE_SIZE (local_size)    ???
  // grid         (GRID_SIZE) grid_size     group_id
  
  
  
  string get_kernel_opencl ();
  
  
  
#define checkErr(cmd) checkErr1(cmd, #cmd, __FILE__, __LINE__)
  void checkErr1 (cl_int const errcode, char const* const cmd,
                  char const* const file, int const line)
  {
    if (errcode == CL_SUCCESS) return;
    CCTK_VWarn (CCTK_WARN_ABORT, line, file, CCTK_THORNSTRING,
                "%s\nError %d",
                cmd, int(errcode));
  }
  
#define checkWarn(cmd) checkWarn1(cmd, #cmd, __FILE__, __LINE__)
  void checkWarn1 (cl_int const errcode, char const* const cmd,
                   char const* const file, int const line)
  {
    if (errcode == CL_SUCCESS) return;
    CCTK_VWarn (CCTK_WARN_ALERT, __LINE__, __FILE__, CCTK_THORNSTRING,
                "%s\nError %d",
                cmd, int(errcode));
  }
  
  
  
  // round up
  size_t ru (size_t const a, size_t const b)
  {
    return (a+b-1)/b;
  }
  
  
  
  typedef char char_arr[10000];
  struct size_t_arr {
    size_t arr[3];
  };
  ostream& operator<< (ostream& os, size_t_arr const& arr)
  {
    for (size_t i=0; i<sizeof(size_t_arr)/sizeof(size_t); ++i) {
      if (i) os << " ";
      os << arr.arr[i];
    }
    return os;
  }
  
  struct device_type_t { cl_device_type t; };
  ostream& operator<< (ostream& os, device_type_t const device_type)
  {
    cout << "{";
    if (device_type.t & CL_DEVICE_TYPE_CPU        ) cout << "DEVICE_TYPE_CPU,";
    if (device_type.t & CL_DEVICE_TYPE_GPU        ) cout << "DEVICE_TYPE_GPU,";
    if (device_type.t & CL_DEVICE_TYPE_ACCELERATOR) cout << "DEVICE_TYPE_ACCELERATOR,";
    cout << "}";
    return os;
  }
  
  struct device_fp_config_t { cl_device_fp_config t; };
  ostream& operator<< (ostream& os, device_fp_config_t const device_fp_config)
  {
    cout << "{";
    if (device_fp_config.t & CL_FP_DENORM          ) cout << "FP_DENORM,";
    if (device_fp_config.t & CL_FP_INF_NAN         ) cout << "FP_INF_NAN,";
    if (device_fp_config.t & CL_FP_ROUND_TO_NEAREST) cout << "FP_ROUND_TO_NEAREST,";
    if (device_fp_config.t & CL_FP_ROUND_TO_ZERO   ) cout << "FP_ROUND_TO_ZERO,";
    if (device_fp_config.t & CL_FP_ROUND_TO_INF    ) cout << "FP_ROUND_TO_INF,";
    cout << "}";
    return os;
  }
  
#define PRINT_INFO(device, FUNCTION, MACRO, NAME, OFFSET, LENGTH, TYPE) \
  do {                                                                  \
    string const spaces = "                                        ";   \
    string const macro = string(NAME) + ": " + spaces;                  \
    size_t const begin = macro.find('_', macro.find('_',0)+1)+1;        \
    TYPE val;                                                           \
    size_t size;                                                        \
    cl_int const errcode1 = FUNCTION(device, MACRO, sizeof val, &val, &size); \
    if (errcode1 == CL_SUCCESS) {                                       \
      assert (sizeof val >= size);                                      \
      cout << spaces.substr(0, OFFSET)                                  \
           << macro.substr(begin, LENGTH+2) << val << endl;             \
    } else {                                                            \
      cout << spaces.substr(0, OFFSET)                                  \
           << macro.substr(begin, LENGTH+2)                             \
           << "[error " << errcode1 << "]" << endl;                     \
    }                                                                   \
  } while (0)
  
  
  
  // Read a file into a string
  string read_file (string const filename)
  {
    ifstream file (filename.c_str());
    stringstream buf;
    buf << file.rdbuf();
    return buf.str();
  }
  
  
  
  // Output a disassembled listing to stdout
  void disassemble_kernels (vector<string> const kernels)
  {
    if (CCTK_MyProc(NULL) != 0) return;
    
    pid_t const cpid = fork();
    if (cpid > 0) {
      cout << "Disassembling kernels in process " << cpid << "\n";
      return;
    }
    
    DECLARE_CCTK_PARAMETERS;
    
    if (verbose) {
      cout << "================================================================================\n";
    }
    
    char* const filename = tempnam(NULL,NULL);
    ofstream cmds (filename);
    for (vector<string>::const_iterator
           kerneli = kernels.begin(); kerneli != kernels.end(); ++kerneli)
    {
      cmds << "disassemble " << *kerneli << "_wrapper\n";
    }
    cmds.close();
    if (verbose) {
      cout << "gdb commands are:\n"
           << "----------\n"
           << read_file(filename)
           << "----------\n";
    }
    
    char **argv;
    int const argc = CCTK_CommandLine(&argv);
    assert (argc >= 1);
    pid_t const pid = getpid();
    
    stringstream gdb;
    gdb << "time gdb -batch -x " << filename << " " << argv[0] << " " << pid
        << " > " << out_dir << "/opencl.dis 2>&1";
    if (verbose) {
      cout << "system command is:\n"
           << gdb.str() << "\n";
      flush (cout);
    }
    system (gdb.str().c_str());
    
    remove (filename);
    // free (filename);
    
    // Exit the hard way, not via exit(), so that MPI etc. don't get
    // confused. Note that we don't need to clean up anything.
    _exit(0);
  }
  
  void disassemble_kernel (string const kernel)
  {
    vector<string> kernels;
    kernels.push_back(kernel);
    disassemble_kernels (kernels);
  }
  
  
  
  extern "C"
  void HelloCL_Init (CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    
    cl_int errcode;
    
    // Display some information about the available compute devices
    cl_uint num_platforms;
    checkErr (clGetPlatformIDs(0, NULL, &num_platforms));
    cout << "num_platforms: " << num_platforms << endl;
    vector<cl_platform_id> platforms(num_platforms);
    vector<vector<cl_device_id> > devices(num_platforms);
    checkErr (clGetPlatformIDs(num_platforms, &platforms[0], &num_platforms));
    for (cl_uint plat_id=0; plat_id<num_platforms; ++plat_id) {
      cout << "PLATFORM_INDEX: " << plat_id << "\n";
      cl_platform_id const platform = platforms[plat_id];
      cout << "   PLATFORM: " << platform << "\n";
      
      if (verbose) {
#define PRINT_PLATFORM_INFO(MACRO, TYPE)                                \
        PRINT_INFO (platform, clGetPlatformInfo, MACRO, #MACRO, 3, 10, TYPE);
        PRINT_PLATFORM_INFO (CL_PLATFORM_PROFILE   , char_arr);
        PRINT_PLATFORM_INFO (CL_PLATFORM_VERSION   , char_arr);
        PRINT_PLATFORM_INFO (CL_PLATFORM_NAME      , char_arr);
        PRINT_PLATFORM_INFO (CL_PLATFORM_VENDOR    , char_arr);
        PRINT_PLATFORM_INFO (CL_PLATFORM_EXTENSIONS, char_arr);
      }
      
      cl_uint num_devices;
      checkErr (clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL,
                               0, NULL, &num_devices));
      cout << "   NUM_DEVICES:   " << num_devices << endl;
      devices[plat_id].resize(num_devices);
      checkErr (clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL,
                               num_devices,
                               &devices[plat_id][0], &num_devices));
      for (cl_uint dev_id=0; dev_id<num_devices; ++dev_id) {
        cout << "   DEVICE INDEX: " << dev_id << "\n";
        cl_device_id const device = devices[plat_id][dev_id];
        cout << "      DEVICE: " << device << "\n";
        
        if (verbose) {
#define PRINT_DEVICE_INFO(MACRO, TYPE)                                  \
          PRINT_INFO (device, clGetDeviceInfo, MACRO, #MACRO, 6, 29, TYPE);
          PRINT_DEVICE_INFO (CL_DEVICE_TYPE                         , device_type_t);
          PRINT_DEVICE_INFO (CL_DEVICE_VENDOR_ID                    , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_COMPUTE_UNITS            , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS     , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_WORK_ITEM_SIZES          , size_t_arr);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_WORK_GROUP_SIZE          , size_t);
          PRINT_DEVICE_INFO (CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR  , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT   , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG  , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF  , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR     , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT    , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_NATIVE_VECTOR_WIDTH_INT      , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG     , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT    , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE   , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF     , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_CLOCK_FREQUENCY          , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_ADDRESS_BITS                 , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_MEM_ALLOC_SIZE           , cl_ulong);
          PRINT_DEVICE_INFO (CL_DEVICE_IMAGE_SUPPORT                , cl_bool);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_READ_IMAGE_ARGS          , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_WRITE_IMAGE_ARGS         , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_IMAGE2D_MAX_WIDTH            , size_t);
          PRINT_DEVICE_INFO (CL_DEVICE_IMAGE2D_MAX_HEIGHT           , size_t);
          PRINT_DEVICE_INFO (CL_DEVICE_IMAGE3D_MAX_WIDTH            , size_t);
          PRINT_DEVICE_INFO (CL_DEVICE_IMAGE3D_MAX_HEIGHT           , size_t);
          PRINT_DEVICE_INFO (CL_DEVICE_IMAGE3D_MAX_DEPTH            , size_t);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_SAMPLERS                 , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_PARAMETER_SIZE           , size_t);
          PRINT_DEVICE_INFO (CL_DEVICE_MEM_BASE_ADDR_ALIGN          , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE     , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_SINGLE_FP_CONFIG             , device_fp_config_t);
#ifdef CL_DEVICE_DOUBLE_FP_CONFIG
          PRINT_DEVICE_INFO (CL_DEVICE_DOUBLE_FP_CONFIG             , device_fp_config_t);
#endif
#ifdef CL_DEVICE_HALF_FP_CONFIG
          PRINT_DEVICE_INFO (CL_DEVICE_HALF_FP_CONFIG               , device_fp_config_t);
#endif
          PRINT_DEVICE_INFO (CL_DEVICE_GLOBAL_MEM_CACHE_TYPE        , cl_device_mem_cache_type);
          PRINT_DEVICE_INFO (CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE    , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_GLOBAL_MEM_CACHE_SIZE        , cl_ulong);
          PRINT_DEVICE_INFO (CL_DEVICE_GLOBAL_MEM_SIZE              , cl_ulong);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE     , cl_ulong);
          PRINT_DEVICE_INFO (CL_DEVICE_MAX_CONSTANT_ARGS            , cl_uint);
          PRINT_DEVICE_INFO (CL_DEVICE_LOCAL_MEM_TYPE               , cl_device_local_mem_type);
          PRINT_DEVICE_INFO (CL_DEVICE_LOCAL_MEM_SIZE               , cl_ulong);
          PRINT_DEVICE_INFO (CL_DEVICE_ERROR_CORRECTION_SUPPORT     , cl_bool);
          PRINT_DEVICE_INFO (CL_DEVICE_HOST_UNIFIED_MEMORY          , cl_bool);
          PRINT_DEVICE_INFO (CL_DEVICE_PROFILING_TIMER_RESOLUTION   , size_t);
          PRINT_DEVICE_INFO (CL_DEVICE_ENDIAN_LITTLE                , cl_bool);
          PRINT_DEVICE_INFO (CL_DEVICE_AVAILABLE                    , cl_bool);
          PRINT_DEVICE_INFO (CL_DEVICE_COMPILER_AVAILABLE           , cl_bool);
          PRINT_DEVICE_INFO (CL_DEVICE_EXECUTION_CAPABILITIES       , cl_device_exec_capabilities);
          PRINT_DEVICE_INFO (CL_DEVICE_QUEUE_PROPERTIES             , cl_command_queue_properties);
          PRINT_DEVICE_INFO (CL_DEVICE_PLATFORM                     , cl_platform_id);
          PRINT_DEVICE_INFO (CL_DEVICE_NAME                         , char_arr);
          PRINT_DEVICE_INFO (CL_DEVICE_VENDOR                       , char_arr);
          PRINT_DEVICE_INFO (CL_DRIVER_VERSION                      , char_arr);
          PRINT_DEVICE_INFO (CL_DEVICE_PROFILE                      , char_arr);
          PRINT_DEVICE_INFO (CL_DEVICE_VERSION                      , char_arr);
          PRINT_DEVICE_INFO (CL_DEVICE_OPENCL_C_VERSION             , char_arr);
          PRINT_DEVICE_INFO (CL_DEVICE_EXTENSIONS                   , char_arr);
        }
        
      } // for dev_id
      
    } // for plat_id
    
    // Choose the platform
    cl_platform_id const platform = platforms.front();
    cout << "Choosing platform: " << platform << "\n";
    
    // Choose a context (basically a device)
    cl_context_properties const cprops[] =
      {CL_CONTEXT_PLATFORM, (cl_context_properties)platform,
       0};
    cl_context const context =
      clCreateContextFromType (cprops, CL_DEVICE_TYPE_CPU, NULL, NULL,
                               &errcode);
    checkErr (errcode);
    
    cl_device_id device;
    size_t size;
    checkErr (clGetContextInfo (context, CL_CONTEXT_DEVICES,
                                sizeof device, &device, &size));
    assert (sizeof device >= size);
    cout << "Choosing device: " << device << "\n";
    
    ////////////////////////////////////////////////////////////////////////////
    
    int const dim = 3;
    
    // point  (smallest unit)
    // vector (same execution path)
    // unroll (unrolled kernel loop)
    // group  (closely coupled threads, sharing cache, "CUDA thread block")
    // tile   (explicit kernel loop)
    // grid   (largest unit, loosely coupled threads, separate caches,
    //         "CUDA grid")
    
    // Choose vector size
    cl_uint vector_size[dim] = {1,1,1};
    checkErr (clGetDeviceInfo (device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE,
                               sizeof vector_size[0], &vector_size[0], &size));
    assert (sizeof vector_size[0] >= size);
    if (vector_size[0] == 0) {
      // If double vectors are not supported, try long instead
      checkErr (clGetDeviceInfo (device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG,
                                 sizeof vector_size[0], &vector_size[0], &size));
      assert (sizeof vector_size[0] >= size);
    }
    if (vector_size[0] == 0) {
      vector_size[0] = 1;
    }
    cout << "Vector size: " << vector_size[0] << " " << vector_size[1] << " " << vector_size[2] << endl;
    
    // unrolled loop
    cl_uint const unroll_size[dim] = {unroll_size_x, unroll_size_y, unroll_size_z};
    cout << "Unroll size: " << unroll_size[0] << " " << unroll_size[1] << " " << unroll_size[2] << endl;
    
    // closely coupled threads
    cl_uint const group_size[dim] = {group_size_x, group_size_y, group_size_z};
    cout << "Group size:  " << group_size[0] << " " << group_size[1] << " " << group_size[2] << endl;
    
    // explicit kernel loops
    cl_uint const tile_size[dim] = {tile_size_x, tile_size_y, tile_size_z};
    cout << "Tile size:   " << tile_size[0] << " " << tile_size[1] << " " << tile_size[2] << endl;
    
    ostringstream defines;
    defines << "#define VECTOR_SIZE_I " << vector_size[0] << "\n"
            << "#define VECTOR_SIZE_J " << vector_size[1] << "\n"
            << "#define VECTOR_SIZE_K " << vector_size[2] << "\n"
            << "#define UNROLL_SIZE_I " << unroll_size[0] << "\n"
            << "#define UNROLL_SIZE_J " << unroll_size[1] << "\n"
            << "#define UNROLL_SIZE_K " << unroll_size[2] << "\n"
            << "#define GROUP_SIZE_I  " << group_size[0] << "\n"
            << "#define GROUP_SIZE_J  " << group_size[1] << "\n"
            << "#define GROUP_SIZE_K  " << group_size[2] << "\n"
            << "#define TILE_SIZE_I   " << tile_size[0] << "\n"
            << "#define TILE_SIZE_J   " << tile_size[1] << "\n"
            << "#define TILE_SIZE_K   " << tile_size[2] << "\n"
            << "\n"
            << "#define GHOST_SIZE_I  " << cctk_nghostzones[0] << "\n"
            << "#define GHOST_SIZE_J  " << cctk_nghostzones[1] << "\n"
            << "#define GHOST_SIZE_K  " << cctk_nghostzones[2] << "\n"
            << "\n"
      ;
    
    // Read the kernel source code and create a program
    string const source = defines.str() + get_kernel_opencl();
    if (verbose) {
      cout << "================================================================================\n"
           << source
           << "================================================================================\n";
    }
    char const* c_source = source.c_str();
    size_t const c_length = strlen(c_source);
    cl_program const program =
      clCreateProgramWithSource (context, 1, &c_source, &c_length, &errcode);
    checkErr (errcode);
    
    // Compile the program
    // (ignore build errors)
    checkWarn (clBuildProgram (program, 1, &device, opencl_options,
                               NULL, NULL));
    checkErr (clGetProgramBuildInfo (program, device, CL_PROGRAM_BUILD_LOG,
                                     0, NULL, &size));
    vector<char> build_log(size);
    checkErr (clGetProgramBuildInfo (program, device, CL_PROGRAM_BUILD_LOG,
                                     build_log.size(), &build_log[0], &size));
    cout << "Build log:" << endl
         << &build_log[0] << endl;
    cl_build_status build_status;
    checkErr (clGetProgramBuildInfo (program, device, CL_PROGRAM_BUILD_STATUS,
                                     sizeof build_status, &build_status,
                                     &size));
    cout << "Build status: ";
    switch (build_status) {
    case CL_BUILD_NONE:        cout << "none";        break;
    case CL_BUILD_ERROR:       cout << "error";       break;
    case CL_BUILD_SUCCESS:     cout << "success";     break;
    case CL_BUILD_IN_PROGRESS: cout << "in_progress"; break;
    default: assert(0);
    }
    cout << endl;
#if 1
    // Dump the program's object code to a file
    size_t binary_size;
    checkErr (clGetProgramInfo (program, CL_PROGRAM_BINARY_SIZES,
                                sizeof binary_size, &binary_size, &size));
    assert (sizeof binary_size >= size);
    cout << "program_binary_size: " << binary_size << "\n";
    vector<unsigned char> binary(binary_size);
    unsigned char* binary_ptr = &binary[0];
    checkErr (clGetProgramInfo (program, CL_PROGRAM_BINARIES,
                                sizeof binary_ptr, &binary_ptr, &size));
    assert (sizeof binary_ptr >= size);
    stringstream filename;
    filename << out_dir << "/opencl.bin";
    ofstream file (filename.str().c_str(), ofstream::binary);
    file.write((char *)(binary_ptr), binary.size());
    file.close();
#endif
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Set up input data
    typedef struct {
      size_t gsh[dim];
      size_t lbnd[dim];
      size_t lssh[dim];
      size_t lsh[dim];
      double origin_space[dim];
      double delta_space[dim];
      double time;
      double delta_time;
    } grid_t;
    grid_t grid;
    grid.gsh[0] = cctk_gsh[0];
    grid.gsh[1] = cctk_gsh[1];
    grid.gsh[2] = cctk_gsh[2];
    grid.lbnd[0] = cctk_lbnd[0];
    grid.lbnd[1] = cctk_lbnd[1];
    grid.lbnd[2] = cctk_lbnd[2];
    grid.lssh[0] = cctk_lssh[0];
    grid.lssh[1] = cctk_lssh[1];
    grid.lssh[2] = cctk_lssh[2];
    grid.lsh[0] = cctk_lsh[0];
    grid.lsh[1] = cctk_lsh[1];
    grid.lsh[2] = cctk_lsh[2];
    grid.origin_space[0] = cctk_origin_space[0];
    grid.origin_space[1] = cctk_origin_space[1];
    grid.origin_space[2] = cctk_origin_space[2];
    grid.delta_space[0] = cctk_delta_space[0];
    grid.delta_space[1] = cctk_delta_space[1];
    grid.delta_space[2] = cctk_delta_space[2];
    grid.time = cctk_time;
    grid.delta_time = cctk_delta_time;
    int const np = grid.lsh[0] * grid.lsh[1] * grid.lsh[2];
    // Ensure alignment
    for (int d=0; d<dim; ++d) {
      assert (grid.lsh[d] % vector_size[d] == 0);
    }
    // Calculate number of thread groups and number of thread groups
    size_t local_work_size[dim];
    for (int d=0; d<dim; ++d) {
      local_work_size[d] = group_size[d];
    }
    cout << "Local work group size:  " << local_work_size[0] << " " << local_work_size[1] << " " << local_work_size[2] << endl;
    size_t global_work_size[dim];
    for (int d=0; d<dim; ++d) {
      global_work_size[d] =
        ru (ru (ru (ru (grid.lssh[d],
                        vector_size[d]),
                    unroll_size[d]),
                group_size[d]),
            tile_size[d]) * group_size[d];
    }
    cout << "Global work group size: " << global_work_size[0] << " " << global_work_size[1] << " " << global_work_size[2] << endl;
    
    // Describe data locations
    cl_mem const mem_grid =
      clCreateBuffer (context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,
                      sizeof grid, &grid, &errcode);
    checkErr (errcode);
    cl_mem const mem_a =
      clCreateBuffer (context, CL_MEM_USE_HOST_PTR,
                      np*sizeof a[0], &a[0], &errcode);
    checkErr (errcode);
    cl_mem const mem_b =
      clCreateBuffer (context, CL_MEM_USE_HOST_PTR,
                      np*sizeof b[0], &b[0], &errcode);
    checkErr (errcode);
    cl_mem const mem_c =
      clCreateBuffer (context, CL_MEM_USE_HOST_PTR,
                      np*sizeof c[0], &c[0], &errcode);
    checkErr (errcode);
    cl_mem const mem_d =
      clCreateBuffer (context, CL_MEM_USE_HOST_PTR,
                      np*sizeof d[0], &d[0], &errcode);
    checkErr (errcode);
    
    // Create an execution queue
    cl_command_queue const queue =
      clCreateCommandQueue (context, device, CL_QUEUE_PROFILING_ENABLE,
                            &errcode);
    checkErr (errcode);
    
    // Choose a kernel from the program
    cl_kernel const kernel_info = clCreateKernel (program, "info", &errcode);
    checkErr (errcode);
    
    // Queue a single execution of the kernel
    cl_event event_info;
    checkErr (clEnqueueNDRangeKernel (queue, kernel_info, dim,
                                      NULL, global_work_size, local_work_size,
                                      0, NULL,
                                      &event_info));
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Choose a kernel from the program
    cl_kernel const kernel_init = clCreateKernel (program, "init", &errcode);
    checkErr (errcode);
    
    // Set up arguments for calling the kernel
    checkErr (clSetKernelArg (kernel_init, 0, sizeof mem_grid, &mem_grid));
    checkErr (clSetKernelArg (kernel_init, 1, sizeof mem_b, &mem_b));
    checkErr (clSetKernelArg (kernel_init, 2, sizeof mem_c, &mem_c));
    checkErr (clSetKernelArg (kernel_init, 3, sizeof mem_d, &mem_d));
    
    // Queue a single execution of the kernel
    cl_event event_init;
    checkErr (clEnqueueNDRangeKernel (queue, kernel_init, dim,
                                      NULL, global_work_size, local_work_size,
                                      0, NULL,
                                      &event_init));
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Choose a kernel from the program
    cl_kernel const kernel_add = clCreateKernel (program, "add", &errcode);
    checkErr (errcode);
    
    // Set up arguments for calling the kernel
    checkErr (clSetKernelArg (kernel_add, 0, sizeof mem_grid, &mem_grid));
    checkErr (clSetKernelArg (kernel_add, 1, sizeof mem_a, &mem_a));
    checkErr (clSetKernelArg (kernel_add, 2, sizeof mem_b, &mem_b));
    checkErr (clSetKernelArg (kernel_add, 3, sizeof mem_c, &mem_c));
    
    // Queue a single execution of the kernel
    cl_event event_add;
    checkErr (clEnqueueNDRangeKernel (queue, kernel_add, dim,
                                      NULL, global_work_size, local_work_size,
                                      1, &event_init,
                                      &event_add));
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Choose a kernel from the program
    cl_kernel const kernel_div = clCreateKernel (program, "div", &errcode);
    checkErr (errcode);
    
    // Set up arguments for calling the kernel
    checkErr (clSetKernelArg (kernel_div, 0, sizeof mem_grid, &mem_grid));
    checkErr (clSetKernelArg (kernel_div, 1, sizeof mem_d, &mem_d));
    checkErr (clSetKernelArg (kernel_div, 2, sizeof mem_b, &mem_b));
    
    // Queue a single execution of the kernel
    cl_event event_div;
    checkErr (clEnqueueNDRangeKernel (queue, kernel_div, dim,
                                      NULL, global_work_size, local_work_size,
                                      1, &event_init,
                                      &event_div));
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Choose a kernel from the program
    cl_kernel const kernel_wave_init =
      clCreateKernel (program, "wave_init", &errcode);
    checkErr (errcode);
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Choose a kernel from the program
    cl_kernel const kernel_wave_step =
      clCreateKernel (program, "wave_step", &errcode);
    checkErr (errcode);
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Produce disassembled listings
    vector<string> kernels;
    kernels.push_back ("init");
    kernels.push_back ("add");
    kernels.push_back ("div");
    kernels.push_back ("wave_init");
    kernels.push_back ("wave_step");
    disassemble_kernels (kernels);
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Wait for the kernel
    checkErr (clWaitForEvents (1, &event_add));
    checkErr (clWaitForEvents (1, &event_div));
    
    // Look at the result
    CCTK_REAL sum = 0.0;
    for (int i=0; i<np; ++i) {
      sum += a[i];
    }
    cout << "result: " << sum << " (should be " << 1.5005e+6 << ")\n";
    
    sum = 0.0;
    for (int i=0; i<np; ++i) {
      sum += d[i];
    }
    cout << "result: " << sum << " (should be " << 56832 << ")\n";
    
    ////////////////////////////////////////////////////////////////////////////
    
    cl_event const events[] = {event_info, event_init, event_add, event_div};
    for (int k=0; k<4; ++k) {
      cl_ulong queued, submit, start, end;
      checkErr (clGetEventProfilingInfo (events[k], CL_PROFILING_COMMAND_QUEUED,
                                         sizeof queued, &queued, &size));
      checkErr (clGetEventProfilingInfo (events[k], CL_PROFILING_COMMAND_SUBMIT,
                                         sizeof submit, &submit, &size));
      checkErr (clGetEventProfilingInfo (events[k], CL_PROFILING_COMMAND_START,
                                         sizeof start, &start, &size));
      checkErr (clGetEventProfilingInfo (events[k], CL_PROFILING_COMMAND_END,
                                         sizeof end, &end, &size));
      cl_ulong const wait_time    = submit - queued;
      cl_ulong const startup_time = start  - submit;
      cl_ulong const run_time     = end    - start;
      cout << "   wait: " << wait_time
           << "   startup: " << startup_time
           << "   run: " << run_time << endl;
    }
  }
  
} // namespace OpenCL
