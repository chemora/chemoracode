// -*-C-*-

// pown; note that Apple optimises pow(,2) but not pown(,2)
// __builtin_expect
// Boost, <boost/preprocessor/...>

_Pragma ("OPENCL EXTENSION cl_khr_fp64    : enable")
_Pragma ("OPENCL EXTENSION cl_amd_printf  : enable")
_Pragma ("OPENCL EXTENSION cl_intel_printf: enable")



// doubleV           vector of double
// longV             vector of long (same size as double)
// convert_doubleV   convert to vector of double
// indicesV          vector of long containing (0,1,2,...)
// vloadaV           load aligned vector
// vloadV_offset     load unaligned vector
// vstoreaV          store aligned vector
// vstoreaV_masked   store aligned vector partially

// Assumption: all grid points [0,j,k] are aligned, arrays are padded
// as necessary

#if VECTOR_SIZE_I==1
#  define doubleV         double
#  define convert_doubleV convert_double
#  define longV           long
#  define vloadV(i,p)     ((p)[i])
#  define indicesV        ((longV)(0))
#elif VECTOR_SIZE_I==2
#  define doubleV         double2
#  define convert_doubleV convert_double2
#  define longV           long2
#  define vloadV          vload2
#  define indicesV        ((longV)(0,1))
#elif VECTOR_SIZE_I==4
#  define doubleV  double4
#  define convert_doubleV convert_double4
#  define longV           long4
#  define vloadV          vload4
#  define indicesV        ((longV)(0,1,2,3))
#elif VECTOR_SIZE_I==8
#  define doubleV         double8
#  define convert_doubleV convert_double8
#  define longV           long8
#  define vloadV          vload8
#  define indicesV        ((longV)(0,1,2,3,4,5,6,7))
#elif VECTOR_SIZE_I==16
#  define doubleV         double16
#  define convert_doubleV convert_double16
#  define longV           long16
#  define vloadV          vload16
#  define indicesV        ((longV)(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15))
#else
#  error
#endif
#if VECTOR_SIZE_J!=1 || VECTOR_SIZE_K!=1
#  error
#endif



inline doubleV cosV(doubleV const x);
#ifndef __APPLE__
inline doubleV cosV(doubleV const x)
{
  return cos(x);
}
#else
// Apple's OpenCL compiler segfaults when calling cos on a vector, so
// we serialise this operation explicitly
#  if VECTOR_SIZE_I==1
inline doubleV cosV(doubleV const x)
{
  return cos(x);
}
#  elif VECTOR_SIZE_I==2
inline doubleV cosV(doubleV const x)
{
  // return (doubleV)(cos(x.s0), cos(x.s1));
  doubleV r;
  r.s0 = cos(x.s0);
  r.s1 = cos(x.s1);
  return r;
}
#  else
#    error
#  endif
#endif

#ifdef __APPLE__
// Apple's pow implementation is much better than their pown
#  define pown pow
#endif



#define vloadaV(offset, p)                      \
  ((p)[offset])
#define vloadV_offset(offset, p, d)                     \
  vloadV(offset, (__global double const*)(p) + (d))

#define vstoreaV(data, offset, p)               \
  do {                                          \
    if (vec_any_I && vec_any_J && vec_any_K) {  \
      (p)[offset] = (data);                     \
    }                                           \
  } while(0)



#if VECTOR_SIZE_I == 1

#  define vstoreaV_masked(data, offset, p)      \
  do {                                          \
    if (vec_any_I && vec_any_J && vec_any_K) {  \
      (p)[offset] = (data);                     \
    }                                           \
  } while(0)

#elif VECTOR_SIZE_I == 2

#  define vstoreaV_masked(data_, offset_, p_)           \
  do {                                                  \
    if (vec_any_I && vec_any_J && vec_any_K) {          \
      doubleV const data = (data_);                     \
      cl_ptrdiff_t const offset = (offset_);            \
      __global doubleV* restrict const p = (p_);        \
      if (vec_all_I) {                                  \
        p[offset] = data;                               \
      } else {                                          \
        if (vec_all_lo_I) {                             \
          ((__global double*)&p[offset])[0] = data.s0;  \
        } else {                                        \
          ((__global double*)&p[offset])[1] = data.s1;  \
        }                                               \
      }                                                 \
    }                                                   \
  } while(0)

#else

#  define vstoreaV_masked(data_, offset_, p_)                   \
  do {                                                          \
    if (vec_any_I && vec_any_J && vec_any_K) {                  \
      doubleV const data = (data_);                             \
      cl_ptrdiff_t const offset = (offset_);                    \
      __global doubleV* restrict const p = (p_);                \
      if (vec_all_I) {                                          \
        p[offset] = data;                                       \
      } else {                                                  \
        /* select(a,b,c) = MSB(c) ? b : a */                    \
        p[offset] = select (p[offset], data, vec_mask_I);       \
      }                                                         \
    }                                                           \
  } while(0)

#endif



#define dim 3

#if 0
// Could/should use 64 bits on 64-bit platforms
typedef unsigned int cl_size_t;
// typedef int cl_ptrdiff_t;
#else
// NOTE: Must use 32 bits on 32-bit platforms
// typedef unsigned long cl_size_t;
typedef long cl_ptrdiff_t;
#endif



typedef struct {
  cl_ptrdiff_t gsh[dim];
  cl_ptrdiff_t lbnd[dim];
  cl_ptrdiff_t lssh[dim];
  cl_ptrdiff_t lsh[dim];
  double origin_space[dim];
  double delta_space[dim];
  double time;
  double delta_time;
} grid_t;



#define DECLARE_CONSTANTS(imin,jmin,kmin, imax,jmax,kmax)               \
  cl_ptrdiff_t const Imin = (imin);                                     \
  cl_ptrdiff_t const Jmin = (jmin);                                     \
  cl_ptrdiff_t const Kmin = (kmin);                                     \
  cl_ptrdiff_t const Imax = (imax);                                     \
  cl_ptrdiff_t const Jmax = (jmax);                                     \
  cl_ptrdiff_t const Kmax = (kmax);                                     \
  cl_ptrdiff_t const grpI = get_local_id(0); /* index in group */       \
  cl_ptrdiff_t const grpJ = get_local_id(1);                            \
  cl_ptrdiff_t const grpK = get_local_id(2);                            \
  cl_ptrdiff_t const grdI = get_group_id(0); /* index in grid */        \
  cl_ptrdiff_t const grdJ = get_group_id(1);                            \
  cl_ptrdiff_t const grdK = get_group_id(2);                            \
  cl_ptrdiff_t const dI = 1;                                            \
  cl_ptrdiff_t const dJ = (grid->lsh[0] / VECTOR_SIZE_I) * dI;          \
  cl_ptrdiff_t const dK = (grid->lsh[1] / VECTOR_SIZE_J) * dJ;



#define EXECUTE_KERNEL(CMD)                                     \
  do {                                                          \
    cl_ptrdiff_t const ind3d = dI*indI + dJ*indJ + dK*indK;     \
    { CMD; }                                                    \
  } while(0)

#define vecVI indicesV
#define vecVJ ((longV)0)
#define vecVK ((longV)0)

#define EXECUTE_VECTOR(D, CMD)                                          \
  do {                                                                  \
    /* TODO: Distribute this over the different EXECUTE macros */       \
    cl_ptrdiff_t const ind##D __attribute__((__unused__)) =             \
      (unr##D + UNROLL_SIZE_##D *                                       \
       (grp##D + GROUP_SIZE_##D *                                       \
        (til##D + TILE_SIZE_##D * grd##D)));                            \
    cl_ptrdiff_t const D __attribute__((__unused__)) =                  \
      VECTOR_SIZE_##D * ind##D;                                         \
    bool const vec_trivial_##D __attribute__((__unused__)) =            \
      VECTOR_SIZE_##D * UNROLL_SIZE_##D == 1;                           \
    bool const vec_any_##D __attribute__((__unused__)) =                \
      vec_trivial_##D || (D + VECTOR_SIZE_##D - 1 >= D##min && D < D##max); \
    bool const vec_all_lo_##D __attribute__((__unused__)) =             \
      vec_trivial_##D || D >= D##min;                                   \
    bool const vec_all_hi_##D __attribute__((__unused__)) =             \
      vec_trivial_##D || D+VECTOR_SIZE_##D-1 < D##max;                  \
    bool const vec_all_##D __attribute__((__unused__)) =                \
      vec_trivial_##D || (vec_all_lo_##D && vec_all_hi_##D);            \
    longV const vec_mask_##D __attribute__((__unused__)) =              \
      vec_trivial_##D ?                                                 \
      (longV)0 == (longV)0 :                                            \
      (longV)D+vecV##D >= (longV)D##min &&                              \
      (longV)D+vecV##D <  (longV)D##max;                                \
    { CMD; }                                                            \
  } while(0)

#define EXECUTE_UNROLL(D, CMD)                                          \
  do {                                                                  \
    switch (UNROLL_SIZE_##D) {                                          \
    /*                                                                  \
    case 8: { cl_ptrdiff_t const unr##D = UNROLL_SIZE_##D - 8; { CMD; } } \
    case 7: { cl_ptrdiff_t const unr##D = UNROLL_SIZE_##D - 7; { CMD; } } \
    case 6: { cl_ptrdiff_t const unr##D = UNROLL_SIZE_##D - 6; { CMD; } } \
    case 5: { cl_ptrdiff_t const unr##D = UNROLL_SIZE_##D - 5; { CMD; } } \
    */                                                                  \
    case 4: { cl_ptrdiff_t const unr##D = UNROLL_SIZE_##D - 4; { CMD; } } \
    case 3: { cl_ptrdiff_t const unr##D = UNROLL_SIZE_##D - 3; { CMD; } } \
    case 2: { cl_ptrdiff_t const unr##D = UNROLL_SIZE_##D - 2; { CMD; } } \
    case 1: { cl_ptrdiff_t const unr##D = UNROLL_SIZE_##D - 1; { CMD; } } \
    }                                                                   \
  } while(0)

#define EXECUTE_TILE(D, CMD)                                            \
  do {                                                                  \
    for (cl_ptrdiff_t til##D = 0; til##D < TILE_SIZE_##D; ++til##D) {   \
      cl_ptrdiff_t const tind##D =                                      \
        (grp##D + GROUP_SIZE_##D *                                      \
         (til##D + TILE_SIZE_##D * grd##D));                            \
      cl_ptrdiff_t const D = VECTOR_SIZE_##D * UNROLL_SIZE_##D * tind##D; \
      if (D + VECTOR_SIZE_##D * UNROLL_SIZE_##D - 1 >= D##min && D < D##max) { \
        CMD;                                                            \
      }                                                                 \
    }                                                                   \
  } while (0)



__kernel
__attribute__((vec_type_hint(doubleV)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void wave_init (__constant grid_t  *restrict const grid,
                __global   doubleV *restrict const u)
{
  cl_ptrdiff_t const ni = grid->gsh[0];
  cl_ptrdiff_t const nj = grid->gsh[1];
  cl_ptrdiff_t const nk = grid->gsh[2];
  cl_ptrdiff_t const i0 = grid->lbnd[0];
  cl_ptrdiff_t const j0 = grid->lbnd[1];
  cl_ptrdiff_t const k0 = grid->lbnd[2];
  double const x0 = grid->origin_space[0];
  double const y0 = grid->origin_space[1];
  double const z0 = grid->origin_space[2];
  double const dx = grid->delta_space[0];
  double const dy = grid->delta_space[1];
  double const dz = grid->delta_space[2];
  double const x1 = x0 + convert_double(i0) * dx;
  double const y1 = y0 + convert_double(j0) * dy;
  double const z1 = z0 + convert_double(k0) * dz;
  double const t0 = grid->time;
  double const kx = M_PI / (dx * convert_double(ni - 1));
  double const ky = M_PI / (dy * convert_double(nj - 1));
  double const kz = M_PI / (dz * convert_double(nk - 1));
  double const omega = sqrt(pown(kx,2) + pown(ky,2) + pown(kz,2));
  double const cos_t = cos(omega*t0);
  
  doubleV const x1V = x1 + convert_doubleV(indicesV) * dx;
  
  DECLARE_CONSTANTS(0,
                    0,
                    0,
                    grid->lssh[0],
                    grid->lssh[1],
                    grid->lssh[2]);
  
#define KERNEL                                          \
  do {                                                  \
    doubleV const x = x1V + convert_double(I) * dx;     \
    double  const y = y1  + convert_double(J) * dy;     \
    double  const z = z1  + convert_double(K) * dz;     \
    /* doubleV const cos_x = cos(kx*x); */              \
    doubleV const cos_x = cosV(kx*x);                   \
    double  const cos_y = cos (ky*y);                   \
    double  const cos_z = cos (kz*z);                   \
    doubleV const uval = cos_x * cos_y * cos_z * cos_t; \
    vstoreaV(uval, ind3d, u);                           \
  } while(0)
  
  EXECUTE_TILE(K,
  EXECUTE_TILE(J,
  EXECUTE_TILE(I,
  EXECUTE_UNROLL(K,
  EXECUTE_UNROLL(J,
  EXECUTE_UNROLL(I,
  EXECUTE_VECTOR(K,
  EXECUTE_VECTOR(J,
  EXECUTE_VECTOR(I,
  EXECUTE_KERNEL(
  KERNEL
  ))))))))));
  
#undef KERNEL
}



__kernel
__attribute__((vec_type_hint(doubleV)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void wave_evol (__constant grid_t        *restrict const grid,
                __global   doubleV const *restrict const u_p_p,
                __global   doubleV const *restrict const u_p,
                __global   doubleV       *restrict const u)
{
  double const idx2 = 1.0 / pown(grid->delta_space[0], 2);
  double const idy2 = 1.0 / pown(grid->delta_space[1], 2);
  double const idz2 = 1.0 / pown(grid->delta_space[2], 2);
  double const dt2 = pown(grid->delta_time, 2);
  
  //   cl_ptrdiff_t const interior_min_I = grid->bbox[0] ? NBOUNDARYZONES_I : 0;
  //   cl_ptrdiff_t const interior_min_J = grid->bbox[2] ? NBOUNDARYZONES_J : 0;
  //   cl_ptrdiff_t const interior_min_K = grid->bbox[4] ? NBOUNDARYZONES_K : 0;
  //   cl_ptrdiff_t const interior_max_I =
  //     grid->lssh[0] - (grid->bbox[1] ? NBOUNDARYZONES_I : 0);
  //   cl_ptrdiff_t const interior_max_J =
  //     grid->lssh[0] - (grid->bbox[3] ? NBOUNDARYZONES_J : 0);
  //   cl_ptrdiff_t const interior_max_K =
  //     grid->lssh[0] - (grid->bbox[5] ? NBOUNDARYZONES_K : 0);
  //   
  //   DECLARE_CONSTANTS(grid->bbox[0] ? 0 : NGHOSTZONES_I,
  //                     grid->bbox[2] ? 0 : NGHOSTZONES_J,
  //                     grid->bbox[4] ? 0 : NGHOSTZONES_K,
  //                     grid->lssh[0] - (grid->bbox[1] ? 0 : NGHOSTZONES_I),
  //                     grid->lssh[1] - (grid->bbox[3] ? 0 : NGHOSTZONES_J),
  //                     grid->lssh[2] - (grid->bbox[5] ? 0 : NGHOSTZONES_K));
  //     
  // #define KERNEL                                                     \
  //  do {                                                              \
  //    doubleV uval;                                                   \
  //    bool const is_interior_I = I>=interior_min_I && I<interior_max_I; \
  //    bool const is_interior_J = J>=interior_min_J && J<interior_max_J; \
  //    bool const is_interior_K = K>=interior_min_K && K<interior_max_K; \
  //    if (is_interior_I && is_interior_J && is_interior_K) {          \
  //      /* Interior */                                                \
  //      doubleV const dxu =                                           \
  //        idx2 * (+ 1.0 * vloadV_offset(ind3d, u_p, -dI)              \
  //                - 2.0 * vloadaV(ind3d, u_p)                         \
  //                + 1.0 * vloadV_offset(ind3d, u_p, +dI));            \
  //      doubleV const dyu =                                           \
  //        idy2 * (+ 1.0 * vloadaV(ind3d-dJ, u_p)                      \
  //                - 2.0 * vloadaV(ind3d, u_p)                         \
  //                + 1.0 * vloadaV(ind3d+dJ, u_p));                    \
  //      doubleV const dzu =                                           \
  //        idz2 * (+ 1.0 * vloadaV(ind3d-dK, u_p)                      \
  //                - 2.0 * vloadaV(ind3d, u_p)                         \
  //                + 1.0 * vloadaV(ind3d+dK, u_p));                    \
  //      uval =                                                        \
  //        2.0 * u_p[ind3d] - u_p_p[ind3d] + dt2 * (dxu + dyu + dzu);  \
  //    } else {                                                        \
  //      /* Boundary */                                                \
  //      uval = 0.0;                                                   \
  //    }                                                               \
  //    vstoreaV_masked(uval, ind3d, u);                                \
  //  } while (0)
  
  DECLARE_CONSTANTS(NGHOSTZONES_I,
                    NGHOSTZONES_J,
                    NGHOSTZONES_K,
                    grid->lssh[0] - NGHOSTZONES_I,
                    grid->lssh[1] - NGHOSTZONES_J,
                    grid->lssh[2] - NGHOSTZONES_K);
  
#define KERNEL                                                  \
 do {                                                           \
   doubleV const dxu =                                          \
     idx2 * (+ 1.0 * vloadV_offset(ind3d, u_p, -dI)             \
             - 2.0 * vloadaV(ind3d, u_p)                        \
             + 1.0 * vloadV_offset(ind3d, u_p, +dI));           \
   doubleV const dyu =                                          \
     idy2 * (+ 1.0 * vloadaV(ind3d-dJ, u_p)                     \
             - 2.0 * vloadaV(ind3d, u_p)                        \
             + 1.0 * vloadaV(ind3d+dJ, u_p));                   \
   doubleV const dzu =                                          \
     idz2 * (+ 1.0 * vloadaV(ind3d-dK, u_p)                     \
             - 2.0 * vloadaV(ind3d, u_p)                        \
             + 1.0 * vloadaV(ind3d+dK, u_p));                   \
   doubleV const uval =                                         \
     2.0 * u_p[ind3d] - u_p_p[ind3d] + dt2 * (dxu + dyu + dzu); \
   vstoreaV_masked(uval, ind3d, u);                             \
 } while (0)
 
 EXECUTE_TILE(K,
 EXECUTE_TILE(J,
 EXECUTE_TILE(I,
 EXECUTE_UNROLL(K,
 EXECUTE_UNROLL(J,
 EXECUTE_UNROLL(I,
 EXECUTE_VECTOR(K,
 EXECUTE_VECTOR(J,
 EXECUTE_VECTOR(I,
 EXECUTE_KERNEL(
 KERNEL
 ))))))))));
 
#undef KERNEL
}


  
#define KERNEL                                  \
 do {                                           \
   doubleV const uval = 0.0;                    \
   vstoreaV_masked(uval, ind3d, u);             \
 } while (0)

#define FUNCTION(DIR, FACE)                                             \
__kernel                                                                \
__attribute__((vec_type_hint(doubleV)))                                 \
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K))) \
void wave_boundary_##DIR##_##FACE (__constant grid_t  *restrict const grid, \
                                   __global   doubleV *restrict const u) \
{                                                                       \
  cl_ptrdiff_t Ibmin = 0;                                               \
  cl_ptrdiff_t Jbmin = 0;                                               \
  cl_ptrdiff_t Kbmin = 0;                                               \
  cl_ptrdiff_t Ibmax = grid->lssh[0];                                   \
  cl_ptrdiff_t Jbmax = grid->lssh[1];                                   \
  cl_ptrdiff_t Kbmax = grid->lssh[2];                                   \
                                                                        \
  if (DIR==0) {                                                         \
    if (FACE==0) {                                                      \
      Ibmax = Ibmin + NGHOSTZONES_I;                                    \
    } else {                                                            \
      Ibmin = Ibmax - NGHOSTZONES_I;                                    \
    }                                                                   \
  }                                                                     \
  if (DIR==1) {                                                         \
    Ibmin += NGHOSTZONES_I;                                             \
    Ibmax -= NGHOSTZONES_I;                                             \
    if (FACE==0) {                                                      \
      Jbmax = Jbmin + NGHOSTZONES_J;                                    \
    } else {                                                            \
      Jbmin = Jbmax - NGHOSTZONES_J;                                    \
    }                                                                   \
  }                                                                     \
  if (DIR==2) {                                                         \
    Ibmin += NGHOSTZONES_I;                                             \
    Ibmax -= NGHOSTZONES_I;                                             \
    Jbmin += NGHOSTZONES_J;                                             \
    Jbmax -= NGHOSTZONES_J;                                             \
    if (FACE==0) {                                                      \
      Kbmax = Kbmin + NGHOSTZONES_K;                                    \
    } else {                                                            \
      Kbmin = Kbmax - NGHOSTZONES_K;                                    \
    }                                                                   \
  }                                                                     \
                                                                        \
  DECLARE_CONSTANTS(Ibmin,Jbmin,Kbmin, Ibmax,Jbmax,Kbmax);              \
                                                                        \
  EXECUTE_TILE(K,                                                       \
  EXECUTE_TILE(J,                                                       \
  EXECUTE_TILE(I,                                                       \
  EXECUTE_UNROLL(K,                                                     \
  EXECUTE_UNROLL(J,                                                     \
  EXECUTE_UNROLL(I,                                                     \
  EXECUTE_VECTOR(K,                                                     \
  EXECUTE_VECTOR(J,                                                     \
  EXECUTE_VECTOR(I,                                                     \
  EXECUTE_KERNEL(                                                       \
  KERNEL                                                                \
  ))))))))));                                                           \
}

FUNCTION(0,0)
FUNCTION(0,1)
FUNCTION(1,0)
FUNCTION(1,1)
FUNCTION(2,0)
FUNCTION(2,1)

#undef FUNCTION
#undef KERNEL
