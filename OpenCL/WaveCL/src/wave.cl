// -*-C-*-



kernel
__attribute__((vec_type_hint(CCTK_REAL_VEC)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void wave_init (grid_t constant  *restrict const grid,
                CCTK_REAL global *restrict const u)
{
  cl_ptrdiff_t const ni = grid->gsh[0];
  cl_ptrdiff_t const nj = grid->gsh[1];
  cl_ptrdiff_t const nk = grid->gsh[2];
  cl_ptrdiff_t const i0 = grid->lbnd[0];
  cl_ptrdiff_t const j0 = grid->lbnd[1];
  cl_ptrdiff_t const k0 = grid->lbnd[2];
  cl_ptrdiff_t const di = 1;
  cl_ptrdiff_t const dj = di * grid->lsh[0];
  cl_ptrdiff_t const dk = dj * grid->lsh[1];
  CCTK_REAL const x0 = grid->origin_space[0];
  CCTK_REAL const y0 = grid->origin_space[1];
  CCTK_REAL const z0 = grid->origin_space[2];
  CCTK_REAL const dx = grid->delta_space[0];
  CCTK_REAL const dy = grid->delta_space[1];
  CCTK_REAL const dz = grid->delta_space[2];
  CCTK_REAL const x1 = x0 + convert_double(i0) * dx;
  CCTK_REAL const y1 = y0 + convert_double(j0) * dy;
  CCTK_REAL const z1 = z0 + convert_double(k0) * dz;
  CCTK_REAL const t0 = grid->time;
  CCTK_REAL const kx = M_PI / (dx * convert_double(ni - 1));
  CCTK_REAL const ky = M_PI / (dy * convert_double(nj - 1));
  CCTK_REAL const kz = M_PI / (dz * convert_double(nk - 1));
  CCTK_REAL const omega = sqrt(pown(kx,2) + pown(ky,2) + pown(kz,2));
  CCTK_REAL const cos_t = mycos(omega*t0);
  
  CCTK_REAL_VEC const x1V = x1 + convert_real_vec(indices_vec) * dx;
  
  CCTK_LOOP3(wave_init,
             i,j,k,
             grid->imin[0],grid->imin[1],grid->imin[2],
             grid->imax[0],grid->imax[1],grid->imax[2],
             grid->lsh[0],grid->lsh[1],grid->lsh[2])
  {
    cl_ptrdiff_t const ind3d = di*i + dj*j + dk*k;
    
    CCTK_REAL_VEC const x = x1V + convert_double(i) * dx;
    CCTK_REAL     const y = y1  + convert_double(j) * dy;
    CCTK_REAL     const z = z1  + convert_double(k) * dz;
    // CCTK_REAL_VEC const cos_x = (CCTK_REAL_VEC)(cos(kx*x.s0), cos(kx*x.s1));
    // CCTK_REAL_VEC const cos_x = kcos(kx*x);
    CCTK_REAL_VEC const cos_x = kcos(kx*x);
    CCTK_REAL     const cos_y = mycos (ky*y);
    CCTK_REAL     const cos_z = mycos (kz*z);
    CCTK_REAL_VEC const uval = dx;//cos_x * cos_y * cos_z * cos_t;
    vec_store_nta(&u[ind3d], uval);
    
  }
  CCTK_ENDLOOP3(wave_init);
}



kernel
__attribute__((vec_type_hint(CCTK_REAL_VEC)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void wave_evol (grid_t constant        *restrict const grid,
                CCTK_REAL const global *restrict const u_p_p,
                CCTK_REAL const global *restrict const u_p,
                CCTK_REAL       global *restrict const u)
{
  cl_ptrdiff_t const di = 1;
  cl_ptrdiff_t const dj = di * grid->lsh[0];
  cl_ptrdiff_t const dk = dj * grid->lsh[1];
  CCTK_REAL const idx2 = 1.0 / pown(grid->delta_space[0], 2);
  CCTK_REAL const idy2 = 1.0 / pown(grid->delta_space[1], 2);
  CCTK_REAL const idz2 = 1.0 / pown(grid->delta_space[2], 2);
  CCTK_REAL const dt2 = pown(grid->delta_time, 2);
  
  CCTK_LOOP3(wave_evol,
             i,j,k,
             grid->imin[0],grid->imin[1],grid->imin[2],
             grid->imax[0],grid->imax[1],grid->imax[2],
             grid->lsh[0],grid->lsh[1],grid->lsh[2])
  {
    cl_ptrdiff_t const ind3d = di*i + dj*j + dk*k;
    
    CCTK_REAL_VEC const dxu =
      idx2 * (+ 1.0 * vec_loadu_maybe3(-di,0,0, &u_p[ind3d-di])
              - 2.0 * vec_loadu_maybe3(  0,0,0, &u_p[ind3d   ])
              + 1.0 * vec_loadu_maybe3(+di,0,0, &u_p[ind3d+di]));
    CCTK_REAL_VEC const dyu =
      idy2 * (+ 1.0 * vec_loadu_maybe3(0,-dj,0, &u_p[ind3d-dj])
              - 2.0 * vec_loadu_maybe3(0,  0,0, &u_p[ind3d   ])
              + 1.0 * vec_loadu_maybe3(0,+dj,0, &u_p[ind3d+dj]));
    CCTK_REAL_VEC const dzu =
      idz2 * (+ 1.0 * vec_loadu_maybe3(0,0,-dk, &u_p[ind3d-dk])
              - 2.0 * vec_loadu_maybe3(0,0,  0, &u_p[ind3d   ])
              + 1.0 * vec_loadu_maybe3(0,0,+dk, &u_p[ind3d+dk]));
    CCTK_REAL_VEC const uval =
      + 2.0 * vec_load(&u_p[ind3d]) - vec_load(&u_p_p[ind3d])
      + dt2 * (dxu + dyu + dzu);
    vec_store_nta_partial(&u[ind3d], uval);
    
  }
  CCTK_ENDLOOP3(wave_evol);
}



kernel
__attribute__((vec_type_hint(CCTK_REAL_VEC)))
__attribute__((reqd_work_group_size(GROUP_SIZE_I, GROUP_SIZE_J, GROUP_SIZE_K)))
void wave_boundary (grid_t constant  *restrict const grid,
                    CCTK_REAL global *restrict const u)
{
  cl_ptrdiff_t const di = 1;
  cl_ptrdiff_t const dj = di * grid->lsh[0];
  cl_ptrdiff_t const dk = dj * grid->lsh[1];
  
  CCTK_LOOP3(wave_boundary,
             i,j,k,
             grid->imin[0],grid->imin[1],grid->imin[2],
             grid->imax[0],grid->imax[1],grid->imax[2],
             grid->lsh[0],grid->lsh[1],grid->lsh[2])
  {
    cl_ptrdiff_t const ind3d = di*i + dj*j + dk*k;
    
    CCTK_REAL_VEC const uval = 0.0;
    vec_store_nta_partial(&u[ind3d], uval);
    
  }
  CCTK_ENDLOOP3(wave_boundary);
}
