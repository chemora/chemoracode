// -*-C-*-

// pown; note that Apple optimises pow(,2) but not pown(,2)
// __builtin_expect
// Boost, <boost/preprocessor/...>

#pragma OPENCL EXTENSION cl_khr_fp64    : enable
#pragma OPENCL EXTENSION cl_amd_printf  : enable
#pragma OPENCL EXTENSION cl_intel_printf: enable



////////////////////////////////////////////////////////////////////////////////



// CCTK_REAL_VEC           vector of CCTK_REAL
// CCTK_LONG_VEC           vector of long (same size as CCTK_REAL)
// convert_real_vec        convert to CCTK_REAL_VEC
// indices_vec             CCTK_LONG_VEC containing (0,1,2,...)
// vec_load                load aligned vector
// vec_loadu               load unaligned vector
// vec_store_nta           store aligned vector
// vec_store_nta_partial   store aligned vector partially

// Assumption: all grid points [0,j,k] are aligned, arrays are padded
// as necessary

#define CCTK_REAL double
#define CCTK_LONG long

#define CCTK_REAL_VEC_SIZE VECTOR_SIZE_I
#if VECTOR_SIZE_J!=1 || VECTOR_SIZE_K!=1
#  error
#endif

#if CCTK_REAL_VEC_SIZE == 1
#  define CCTK_REAL_VEC      double
#  define convert_real_vec   convert_double
#  define CCTK_LONG_VEC      long
#  define indices_vec        ((CCTK_LONG_VEC)(0))
#  define vloadV(i,p)        ((p)[i])
#  define vstoreV(x,i,p)     ((p)[i]=(x))
#elif CCTK_REAL_VEC_SIZE == 2
#  define CCTK_REAL_VEC      double2
#  define convert_real_vec   convert_double2
#  define CCTK_LONG_VEC      long2
#  define indices_vec        ((CCTK_LONG_VEC)(0,1))
#  define vloadV             vload2
#  define vstoreV            vstore2
#elif CCTK_REAL_VEC_SIZE == 4
#  define CCTK_REAL_VEC      double4
#  define convert_real_vec   convert_double4
#  define CCTK_LONG_VEC      long4
#  define indices_vec        ((CCTK_LONG_VEC)(0,1,2,3))
#  define vloadV             vload4
#  define vstoreV            vstore4
#elif CCTK_REAL_VEC_SIZE == 8
#  define CCTK_REAL_VEC      double8
#  define convert_real_vec   convert_double8
#  define CCTK_LONG_VEC      long8
#  define indices_vec        ((CCTK_LONG_VEC)(0,1,2,3,4,5,6,7))
#  define vloadV             vload8
#  define vstoreV            vstore8
#elif CCTK_REAL_VEC_SIZE == 16
#  define CCTK_REAL_VEC      double16
#  define convert_real_vec   convert_double16
#  define CCTK_LONG_VEC      long16
#  define indices_vec        ((CCTK_LONG_VEC)(0,1,2,3,4,5,6,7,          \
                                              8,9,10,11,12,13,14,15))
#  define vloadV             vload16
#  define vstoreV            vstore16
#else
#  error
#endif



#if VECTORISE_ALIGNED_ARRAYS

#  define vec_load(p) (* (CCTK_REAL_VEC const global *) p)

#  define vec_loadu_maybe3(off1,off2,off3, p_)                          \
  ({                                                                    \
    CCTK_REAL const global * const pp = (p_);                           \
    CCTK_REAL const global * const p = pp;                              \
    (off1) % CCTK_REAL_VEC_SIZE == 0 ? vec_load(p) : vloadV(0, p);      \
  })

#else

#  define vec_load(p) vloadV(0, p)
#  define vec_loadu_maybe3(off1,off2,off3, p) vloadV(0, p)

#endif



#if VECTORISE_ALIGNED_ARRAYS

#  define vec_store_nta(p, x)                   \
  do {                                          \
    * (CCTK_REAL_VEC global *) (p) = (x);       \
  } while(0)

#else

#  define vec_store_nta(p, x)                   \
  do {                                          \
    vstoreV(x, 0, p);                           \
  } while(0)

#endif

#if CCTK_REAL_VEC_SIZE == 1

#  define vec_store_nta_partial(p, x)                                   \
  do {                                                                  \
    if (__builtin_expect(lc_vec_any_I && lc_vec_any_J && lc_vec_any_K, 1)) { \
      vec_store_nta ((p), (x));                                         \
    }                                                                   \
  } while(0)

#elif CCTK_REAL_VEC_SIZE == 2

#  define vec_store_nta_partial(p_, x_)                                 \
  do {                                                                  \
    if (__builtin_expect(lc_vec_any_I && lc_vec_any_J && lc_vec_any_K, 1)) { \
      CCTK_REAL global * const pp = (p_);                               \
      CCTK_REAL global * const p = pp;                                  \
      CCTK_REAL_VEC const xx = (x_);                                    \
      CCTK_REAL_VEC const x = xx;                                       \
      if (__builtin_expect(lc_vec_all_I, 1)) {                          \
        vec_store_nta (p, x);                                           \
      } else {                                                          \
        if (lc_vec_lo_I) {                                              \
          p[0] = x.s0;                                                  \
        } else {                                                        \
          p[1] = x.s1;                                                  \
        }                                                               \
      }                                                                 \
    }                                                                   \
  } while(0)

#else

#  define vec_store_nta_partial(p_, x_)                                 \
  do {                                                                  \
    if (__builtin_expect(lc_vec_any_I && lc_vec_any_J && lc_vec_any_K, 1)) { \
      CCTK_REAL global * const pp = (p_);                               \
      CCTK_REAL global * const p = pp;                                  \
      CCTK_REAL_VEC const xx = (x_);                                    \
      CCTK_REAL_VEC const x = xx;                                       \
      if (__builtin_expect(lc_vec_all_I, 1)) {                          \
        vec_store_nta (p, x);                                           \
      } else {                                                          \
        /* select(a,b,c) = MSB(c) ? b : a */                            \
        CCTK_REAL_VEC const y = select (vec_load(p), x, lc_vec_mask_I); \
        vec_store_nta (p, y);                                           \
      }                                                                 \
    }                                                                   \
  } while(0)

#endif



inline CCTK_REAL myfabs (CCTK_REAL x);
inline CCTK_REAL myfabs (CCTK_REAL x)
{
  return x>=0 ? x : -x;
}

inline CCTK_REAL mycos1 (CCTK_REAL x);
inline CCTK_REAL mycos1 (CCTK_REAL x)
{
  // 0<=x<=pi/2
  CCTK_REAL const c1 = +1.0;
  CCTK_REAL const c2 = -1.0/2.0;
  CCTK_REAL const c3 = +1.0/24.0;
  CCTK_REAL const c4 = -1.0/720.0;
  CCTK_REAL const c5 = +1.0/40320.0;
  CCTK_REAL const c6 = -1.0/3628800.0;
  CCTK_REAL const c7 = +1.0/479001600.0;
  CCTK_REAL const c8 = -1.0/87178291200.0;
  CCTK_REAL const x2 = pown(x,2);
  return c1 + x2 * (c2 + x2 * (c3 + x2 * (c4 + x2 * (c5 + x2 * (c6 + x2 * (c7 + x2 * c8))))));
}

inline CCTK_REAL mycos (CCTK_REAL x);
inline CCTK_REAL mycos (CCTK_REAL x)
{
  x = myfabs(x);
  x = fmod(x,2*M_PI);
  if (x>M_PI) x=M_PI-x;
  bool const isneg = x>M_PI/2;
  if (isneg) x=M_PI/2-x;
  CCTK_REAL y = mycos1(x);
  if (isneg) y=-y;
  return y;
}



inline CCTK_REAL_VEC kcos (CCTK_REAL_VEC const x);
#ifndef __APPLE__
inline CCTK_REAL_VEC kcos (CCTK_REAL_VEC const x)
{
  return cos(x);
}
#else
// Apple's OpenCL compiler segfaults when calling cos on a vector, so
// we serialise this operation explicitly
#  if CCTK_REAL_VEC_SIZE==1
inline CCTK_REAL_VEC kcos (CCTK_REAL_VEC const x)
{
  // return cos(x);
  return mycos(x);
}
#  elif CCTK_REAL_VEC_SIZE==2
inline CCTK_REAL_VEC kcos (CCTK_REAL_VEC const x)
{
  // return (CCTK_REAL_VEC)(cos(x.s0), cos(x.s1));
  return (CCTK_REAL_VEC)(mycos(x.s0), mycos(x.s1));
  // CCTK_REAL_VEC r;
  // r.s0 = cos(x.s0);
  // r.s1 = cos(x.s1);
  // return r;
}
#  else
#    error
#  endif
#endif

#ifdef __APPLE__
// Apple's pow implementation is much better than their pown
#  undef pown
#  define pown pow
#endif



////////////////////////////////////////////////////////////////////////////////



#define dim 3

#if SIZEOF_PTRDIFF_T == 4
typedef unsigned int cl_size_t;
typedef int          cl_ptrdiff_t;
#elif SIZEOF_PTRDIFF_T == 8
typedef unsigned long cl_size_t;
typedef long          cl_ptrdiff_t;
#else
#  error
#endif



typedef struct {
  cl_ptrdiff_t gsh[dim];
  cl_ptrdiff_t lbnd[dim];
  cl_ptrdiff_t lssh[dim];
  cl_ptrdiff_t lsh[dim];
  cl_ptrdiff_t imin[dim];
  cl_ptrdiff_t imax[dim];
  CCTK_REAL origin_space[dim];
  CCTK_REAL delta_space[dim];
  CCTK_REAL time;
  CCTK_REAL delta_time;
} grid_t;



#define LC_SET_GROUP_VARS(D)                                            \
  cl_ptrdiff_t const ind##D __attribute__((__unused__)) =               \
    (VECTOR_SIZE_##D * UNROLL_SIZE_##D *                                \
     (lc_grp##D + GROUP_SIZE_##D *                                      \
      (lc_til##D + TILE_SIZE_##D * lc_grd##D)));                        \
  bool const lc_grp_any_##D __attribute__((__unused__)) =               \
    ind##D + VECTOR_SIZE_##D * UNROLL_SIZE_##D - 1 >= lc_##D##min &&    \
    ind##D < lc_##D##max;

#define vecVI indices_vec
#define vecVJ ((CCTK_LONG_VEC)0)
#define vecVK ((CCTK_LONG_VEC)0)

#define LC_SET_VECTOR_VARS(IND,D)                                       \
  cl_ptrdiff_t const IND __attribute__((__unused__)) =                  \
    (VECTOR_SIZE_##D *                                                  \
     (lc_unr##D + UNROLL_SIZE_##D *                                     \
      (lc_grp##D + GROUP_SIZE_##D *                                     \
       (lc_til##D + TILE_SIZE_##D * lc_grd##D))));                      \
  bool const lc_vec_trivial_##D __attribute__((__unused__)) =           \
    VECTOR_SIZE_##D * UNROLL_SIZE_##D == 1;                             \
  bool const lc_vec_any_##D __attribute__((__unused__)) =               \
    lc_vec_trivial_##D ||                                               \
    (IND+VECTOR_SIZE_##D-1 >= lc_##D##min && IND < lc_##D##max);        \
  bool const lc_vec_lo_##D __attribute__((__unused__)) =                \
    lc_vec_trivial_##D ||                                               \
    IND >= lc_##D##min;                                                 \
  bool const lc_vec_hi_##D __attribute__((__unused__)) =                \
    lc_vec_trivial_##D ||                                               \
    IND+VECTOR_SIZE_##D-1 < lc_##D##max;                                \
  bool const lc_vec_all_##D __attribute__((__unused__)) =               \
    lc_vec_trivial_##D ||                                               \
    lc_vec_lo_##D && lc_vec_hi_##D;                                     \
  CCTK_LONG_VEC const lc_vec_mask_##D __attribute__((__unused__)) =     \
    lc_vec_trivial_##D ?                                                \
    (CCTK_LONG_VEC)0 == (CCTK_LONG_VEC)0 :                              \
    (CCTK_LONG_VEC)IND+vecV##D >= (CCTK_LONG_VEC)lc_##D##min &&         \
    (CCTK_LONG_VEC)IND+vecV##D <  (CCTK_LONG_VEC)lc_##D##max;

#define CCTK_LOOP3(name,                                                \
                   i,j,k,                                               \
                   imin,jmin,kmin,                                      \
                   imax,jmax,kmax,                                      \
                   ilsh,jlsh,klsh)                                      \
  do {                                                                  \
    typedef int lc_loop3_##name;                                        \
                                                                        \
    cl_ptrdiff_t const lc_Imin = (imin);                                \
    cl_ptrdiff_t const lc_Jmin = (jmin);                                \
    cl_ptrdiff_t const lc_Kmin = (kmin);                                \
    cl_ptrdiff_t const lc_Imax = (imax);                                \
    cl_ptrdiff_t const lc_Jmax = (jmax);                                \
    cl_ptrdiff_t const lc_Kmax = (kmax);                                \
    cl_ptrdiff_t const lc_grpI = get_local_id(0); /* index in group */  \
    cl_ptrdiff_t const lc_grpJ = get_local_id(1);                       \
    cl_ptrdiff_t const lc_grpK = get_local_id(2);                       \
    cl_ptrdiff_t const lc_grdI = get_group_id(0); /* index in grid */   \
    cl_ptrdiff_t const lc_grdJ = get_group_id(1);                       \
    cl_ptrdiff_t const lc_grdK = get_group_id(2);                       \
                                                                        \
    for (cl_ptrdiff_t lc_tilK = 0; lc_tilK < TILE_SIZE_K; ++lc_tilK) {  \
    for (cl_ptrdiff_t lc_tilJ = 0; lc_tilJ < TILE_SIZE_J; ++lc_tilJ) {  \
    for (cl_ptrdiff_t lc_tilI = 0; lc_tilI < TILE_SIZE_I; ++lc_tilI) {  \
                                                                        \
      LC_SET_GROUP_VARS(I);                                             \
      LC_SET_GROUP_VARS(J);                                             \
      LC_SET_GROUP_VARS(K);                                             \
      if (lc_grp_any_K) {                                               \
      if (lc_grp_any_J) {                                               \
      if (lc_grp_any_I) {                                               \
                                                                        \
        _Pragma("unroll")                                               \
          for (cl_ptrdiff_t lc_unrK = 0; lc_unrK < UNROLL_SIZE_K; ++lc_unrK) { \
        _Pragma("unroll")                                               \
          for (cl_ptrdiff_t lc_unrJ = 0; lc_unrJ < UNROLL_SIZE_J; ++lc_unrJ) { \
        _Pragma("unroll")                                               \
          for (cl_ptrdiff_t lc_unrI = 0; lc_unrI < UNROLL_SIZE_I; ++lc_unrI) { \
                                                                        \
          LC_SET_VECTOR_VARS(i,I);                                      \
          LC_SET_VECTOR_VARS(j,J);                                      \
          LC_SET_VECTOR_VARS(k,K);                                      \
                                                                        \
          {
          
#define CCTK_ENDLOOP3(name)                             \
          }                                             \
        }                                               \
        }                                               \
        }                                               \
      }                                                 \
      }                                                 \
      }                                                 \
    }                                                   \
    }                                                   \
    }                                                   \
    typedef lc_loop3_##name lc_ensure_proper_nesting;   \
  } while(0)
