#ifndef DEFS_H
#define DEFS_H

// Global definitions

#include "OpenCLRunTime.h"

using namespace std;

#ifdef CCTK_CXX_RESTRICT
#  define restrict CCTK_CXX_RESTRICT
#endif

#endif  // #ifndef DEFS_H
