#include "MoLCL.h"

#include <cctk.h>
#include <cctk_Arguments.h>



extern "C"
void MoLCL_InitialCopy(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  
  Accelerator_CopyFromPast
    (cctkGH, EvolvedVariableIndex, MoLNumEvolvedVariables);
}
