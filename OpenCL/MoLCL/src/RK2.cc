#include "MoLCL.h"

#include <cctk.h>
#include <cctk_Arguments.h>

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <vector>

#include <OpenCLRunTime.h>

using namespace std;



extern "C"
void MoLCL_RK2Add(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  
  CCTK_INT *const MoL_Intermediate_Step =
    static_cast<CCTK_INT*>
    (CCTK_VarDataPtr(cctkGH, 0, "MoL::MoL_Intermediate_Step"));
  assert(MoL_Intermediate_Step);
  
  char const    *this_stepper = NULL;
  char const    *this_name    = NULL;
  OpenCLKernel **this_kernel  = NULL;
  
  switch (*MoL_Intermediate_Step) {
  case 2: {
    
    char const *const stepper =
      "void do_step(cGH constant           *restrict const cctkGH,\n"
      "             CCTK_REAL global       *restrict const u,\n"
      "             CCTK_REAL global const *restrict const u_p,\n"
      "             CCTK_REAL global const *restrict const urhs)\n"
      "{\n"
      "  DECLARE_CCTK_ARGUMENTS;\n"
      "\n"
      "  ptrdiff_t const di = 1;\n"
      "  ptrdiff_t const dj = CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);\n"
      "  ptrdiff_t const dk = CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);\n"
      "  \n"
      "  CCTK_REAL_VEC const dt = ToReal(CCTK_DELTA_TIME);\n"
      "  \n"
      "  LC_LOOP3VEC(MoLCL_RK2Add_1,\n"
      "    i,j,k, imin[0],imin[1],imin[2], imax[0],imax[1],imax[2],\n"
      "    cctk_lsh[0],cctk_lsh[1],cctk_lsh[2],\n"
      "    CCTK_REAL_VEC_SIZE)\n"
      "  {\n"
      "    ptrdiff_t const index = di*i + dj*j + dk*k;\n"
      "    CCTK_REAL_VEC uL    = vec_load(u[index]);\n"
      "    CCTK_REAL_VEC urhsL = vec_load(urhs[index]);\n"
      "    uL += dt * urhsL;\n"
      "    vec_store_nta_partial(u[index], uL);\n"
      "  }\n"
      "  LC_ENDLOOP3VEC(MoLCL_RK2Add_1);\n"
      "}\n"
      ;
    
    static OpenCLKernel *kernel = NULL;
    this_name    = "MoLCL_RK2Add_1";
    this_stepper = stepper;
    this_kernel  = &kernel;
    
    break;
  }
  case 1: {
    
    char const *const stepper =
      "void do_step(cGH       constant     *restrict const cctkGH,\n"
      "             CCTK_REAL global       *restrict const u,\n"
      "             CCTK_REAL global const *restrict const u_p,\n"
      "             CCTK_REAL global const *restrict const urhs)\n"
      "{\n"
      "  DECLARE_CCTK_ARGUMENTS;\n"
      "\n"
      "  ptrdiff_t const di = 1;\n"
      "  ptrdiff_t const dj = CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);\n"
      "  ptrdiff_t const dk = CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);\n"
      "  \n"
      "  CCTK_REAL_VEC const dt = ToReal(CCTK_DELTA_TIME);\n"
      "  \n"
      "  LC_LOOP3VEC(MoLCL_RK2Add_2,\n"
      "    i,j,k, imin[0],imin[1],imin[2], imax[0],imax[1],imax[2],\n"
      "    cctk_lsh[0],cctk_lsh[1],cctk_lsh[2],\n"
      "    CCTK_REAL_VEC_SIZE)\n"
      "  {\n"
      "    ptrdiff_t const index = di*i + dj*j + dk*k;\n"
      "    CCTK_REAL_VEC uL    = vec_load(u[index]);\n"
      "    CCTK_REAL_VEC u_pL  = vec_load(u_p[index]);\n"
      "    CCTK_REAL_VEC urhsL = vec_load(urhs[index]);\n"
      "    uL = ToReal(0.5) * (u_pL + uL) + dt * urhsL;\n"
      "    vec_store_nta_partial(u[index], uL);\n"
      "  }\n"
      "  LC_ENDLOOP3VEC(MoLCL_RK2Add_2);\n"
      "}\n"
      ;
    
    static OpenCLKernel *kernel = NULL;
    this_name    = "MoLCL_RK2Add_2";
    this_stepper = stepper;
    this_kernel  = &kernel;
    
    break;
  }
  default:
    assert(0);
  }
  
  static string sourcestr;
  static char const *source = NULL;
  static vector<int> varindices, timelevels;
  static vector<string> aliasstrs;
  static vector<char const*> aliases;
  
  if (not source) {
    stringstream sourcebuf;
    for (int var=0; var<MoLNumEvolvedVariables; ++var) {
      int const vi    = EvolvedVariableIndex[var];
      int const rhsvi = RHSVariableIndex[var];
      char const *const varname = CCTK_VarName(vi);
      char const *const rhsname = CCTK_VarName(rhsvi);
      string const varname_p = string(varname) + "_p";
      
      varindices.push_back(vi);
      timelevels.push_back(0);
      aliasstrs.push_back(varname);
      varindices.push_back(vi);
      timelevels.push_back(1);
      aliasstrs.push_back(varname_p);
      varindices.push_back(rhsvi);
      timelevels.push_back(0);
      aliasstrs.push_back(rhsname);
      
      sourcebuf << "  do_step(cctkGH, " << varname << ", " << varname_p << ", "
                << rhsname << ");\n";
    }    
    sourcestr = sourcebuf.str();
    source = sourcestr.c_str();
    
    aliases.resize(aliasstrs.size());
    for (size_t n=0; n<aliases.size(); ++n) {
      aliases.at(n) = aliasstrs.at(n).c_str();
    }
  }
  
  char const *const sources[] = {this_stepper, source, NULL};
  int const imin[] = {0,0,0};
  int const imax[] = {CCTK_LSSH(0,0),CCTK_LSSH(0,1),CCTK_LSSH(0,2)};
  
  OpenCLRunTime_CallKernel
    (cctkGH, CCTK_THORNSTRING, this_name, sources,
     NULL, &varindices.front(), &timelevels.front(), &aliases.front(),
     varindices.size(),
     imin, imax,
     this_kernel);
}
