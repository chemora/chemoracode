#ifndef MoLCL_H
#define MoLCL_H

#include <cctk.h>

// Secretly accessing MoL's internal variables

extern CCTK_INT *EvolvedVariableIndex;
extern CCTK_INT *RHSVariableIndex;

extern CCTK_INT MoLNumEvolvedVariables;

#endif  // #ifndef MoLCL_H
